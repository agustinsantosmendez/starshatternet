﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      DisplayView.h/DisplayView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    View class for Radio Communications HUD Overlay
*/
using System.Collections.Generic;
using StarshatterNet.Gen.Views;
using UnityEngine;
using System;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Graphics;

namespace StarshatterNet.Stars.Views
{
    public class DisplayView : View
    {

        public DisplayView(Window c, string name = "DisplayView") : base(c, name)
        {
            width = 0; height = 0; xcenter = 0; ycenter = 0;

            display_view = this;
            base.OnWindowMove();
        }
        //public virtual ~DisplayView();

        // Operations:
        public override void Refresh()
        {
            foreach (DisplayElement elem in elements)
            {
                // convert relative rect to window rect:
                Rect elem_rect = elem.rect;
                if (elem_rect.x == 0 && elem_rect.y == 0 && elem_rect.w == 0 && elem_rect.h == 0)
                {
                    // stretch to fit
                    elem_rect.w = width;
                    elem_rect.h = height;
                }
                else if (elem_rect.w < 0 && elem_rect.h < 0)
                {
                    // center image in window
                    elem_rect.w *= -1;
                    elem_rect.h *= -1;

                    elem_rect.x = (width - elem_rect.w) / 2;
                    elem_rect.y = (height - elem_rect.h) / 2;
                }
                else
                {
                    // offset from right or bottom
                    if (elem_rect.x < 0) elem_rect.x += width;
                    if (elem_rect.y < 0) elem_rect.y += height;
                }

                // compute current fade,
                // assumes fades are 1 second or less:
                double fade = 0;
                if (elem.fade_in > 0) fade = 1 - elem.fade_in;
                else if (elem.hold > 0) fade = 1;
                else if (elem.fade_out > 0) fade = elem.fade_out;

                // draw text:
                if (!string.IsNullOrEmpty(elem.text) && elem.font != null)
                {
                    elem.font.SetColor(elem.color);
                    elem.font.SetAlpha((int)fade);
                    window.SetFont(elem.font);
                    window.DrawText(elem.text, elem.text.Length, ref elem_rect, TextFormat.DT_WORDBREAK);
                }

                // draw image:
                else if (elem.image != null)
                {
                     window.FadeBitmap(elem_rect.x,
                                        elem_rect.y,
                                        elem_rect.x + elem_rect.w,
                                        elem_rect.y + elem_rect.h,
                                        elem.image,
                                        elem.color * (float)fade,
                                        elem.blend);
                 }
            }
        }
        public override void OnWindowMove()
        {
            if (windowObj)
            {
                width = window.Width();
                height = window.Height();
                xcenter = (width / 2.0) - 0.5;
                ycenter = (height / 2.0) + 0.5;
            }
        }

        public virtual void ExecFrame()
        {
            double seconds = Game.GUITime();

            for (int i = elements.Count - 1; i >= 0; i--)
            {
                DisplayElement elem = elements[i];

                if (elem.fade_in > 0)
                    elem.fade_in -= seconds;

                else if (elem.hold > 0)
                    elem.hold -= seconds;

                else if (elem.fade_out > 0)
                    elem.fade_out -= seconds;

                else
                    elements.RemoveAt(i);
            }
        }
        public virtual void ClearDisplay()
        {
            elements.Destroy();
        }


        public virtual void AddText(string txt,
                                    FontItem font,
                                    Color color,
                                    Rect rect,
                                    double hold = 1e9,
                                    double fade_in = 0,
                                    double fade_out = 0)
        {
            DisplayElement elem = new DisplayElement();

            if (fade_in == 0 && fade_out == 0 && hold == 0)
                hold = 300;

            elem.text = txt;
            elem.font = font;
            elem.color = color;
            elem.rect = rect;
            elem.hold = hold;
            elem.fade_in = fade_in;
            elem.fade_out = fade_out;

            elements.Add(elem);
        }



        public virtual void AddImage(Bitmap bmp,
                                Color color,
                                Video.BLEND_TYPE blend,
                                Rect rect,
                                double hold = 1e9,
                                double fade_in = 0,
                                double fade_out = 0)
        {
            DisplayElement elem = new DisplayElement();

            if (fade_in == 0 && fade_out == 0 && hold == 0)
                hold = 300;

            elem.image = bmp;
            elem.rect = rect;
            elem.color = color;
            elem.blend = blend;
            elem.hold = hold;
            elem.fade_in = fade_in;
            elem.fade_out = fade_out;

            elements.Add(elem);
        }

        public static DisplayView GetOrCreateInstance(Window win)
        {
            if (display_view == null)
                display_view = new DisplayView(win);

            return display_view;
        }



        protected int width, height;
        protected double xcenter, ycenter;

        protected List<DisplayElement> elements = new List<DisplayElement>();
        private static DisplayView display_view = null;

    }
    public class DisplayElement
    {
        public DisplayElement()
        {
            image = null; font = null;
            blend = 0; hold = 0; fade_in = 0; fade_out = 0;
        }

        public string text;
        public Bitmap image;
        public FontItem font;
        public Color color;
        public Rect rect;
        public Video.BLEND_TYPE blend;
        public double hold;
        public double fade_in;
        public double fade_out;
    }

}