﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      SeekerAI.h/SeekerAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Seeker Missile (low-level) Artifical Intelligence class
*/
using System;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.AI;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars
{
    public class SeekerAI : SteerAI
    {
        public SeekerAI(SimObject s) : base(s)
        {
            shot = (Shot)s; orig_target = null;
            pursuit = 1; delay = 0; overshot = false;
            ai_type = SteerType.SEEKER;

            seek_gain = 25;
            seek_damp = 0.55;
        }
        // ~SeekerAI();

        public override SteerAI.SteerType Type() { return (SteerAI.SteerType)1001; }
        public override bool Subframe() { return true; }

        public override void ExecFrame(double seconds)
        {
            // setup:
            FindObjective();

            // adaptive behavior:
            Navigator();
        }

        public override void FindObjective()
        {
            if (shot == null || target == null) return;

            if (target.Life() == 0)
            {
                if (target != orig_target)
                    SetTarget(orig_target, null);
                else
                    SetTarget(null, null);

                return;
            }

            Point tloc = target.Location();
            tloc = Transform(tloc);

            // seeker head limit of 45 degrees:
            if (tloc.Z < 0 || tloc.Z < Math.Abs(tloc.X) || tloc.Z < Math.Abs(tloc.Y))
            {
                overshot = true;
                SetTarget(null, null);
                return;
            }

            // distance from self to target:
            distance = (target.Location() - self.Location()).Length;

            // are we being spoofed?
            CheckDecoys(distance);

            Point cv = ClosingVelocity();

            // time to reach target:
            double time = distance / cv.Length;
            double predict = time;
            if (predict > 15)
                predict = 15;

            // pure pursuit:
            if (pursuit == 1 || time < 0.1)
            {
                obj_w = target.Location();
            }

            // lead pursuit:
            else
            {
                // where the target will be when we reach it:
                Point run_vec = target.Velocity();
                obj_w = target.Location() + (run_vec * (float)predict);
            }

            // subsystem offset:
            if (subtarget != null)
            {
                Point offset = target.Location() - subtarget.MountLocation();
                obj_w -= offset;
            }
            else if (target.Type() == (int)SimObject.TYPES.SIM_SHIP)
            {
                Ship tgt_ship = (Ship)target;

                if (tgt_ship.IsGroundUnit())
                    obj_w += new Point(0, 150, 0);
            }


            distance = (obj_w - self.Location()).Length;
            time = distance / cv.Length;

            // where we will be when the target gets there:
            if (predict > 0.1 && predict < 15)
            {
                Point self_dest = self.Location() + cv * (float)predict;
                Point err = obj_w - self_dest;

                obj_w += err;
            }

            // transform into camera coords:
            objective = Transform(obj_w);
            objective.Normalize();

            shot.SetEta((int)time);

            if (shot.Owner() != null)
                ((Ship)shot.Owner()).SetMissileEta(shot.Identity(), (int)time);
        }

        public override void SetTarget(SimObject targ, ShipSystem sub = null)
        {
            if (orig_target == null && targ != null && targ.Type() == (int)SimObject.TYPES.SIM_SHIP)
            {
                orig_target = (Ship)targ;
                Observe(orig_target);
            }

            base.SetTarget(targ, sub);

            if (target == null)
            {
                shot.SetEta(0);

                if (shot.Owner() != null)
                    ((Ship)shot.Owner()).SetMissileEta(shot.Identity(), 0);
            }
        }

        public virtual bool Overshot()
        {
            return overshot;
        }


        public virtual void SetPursuit(int p) { pursuit = p; }
        public virtual int GetPursuit() { return pursuit; }

        public virtual void SetDelay(double d) { delay = d; }
        public virtual double GetDelay() { return delay; }

        public override bool Update(SimObject obj)
        {
            if (obj == target)
            {
                if (obj.Type() == (int)SimObject.TYPES.SIM_SHOT && orig_target != null)
                    target = orig_target;
            }

            if (obj == orig_target)
                orig_target = null;

            return base.Update(obj);
        }

        public override string GetObserverName()
        {
            string name = string.Format("SeekerAI({0})", self.Name());
            return name;
        }



        // behaviors:
        protected virtual Steer AvoidCollision()
        {
            return new Steer();
        }

        protected virtual Steer SeekTarget()
        {
            if (self == null || target == null || Overshot())
                return new Steer();

            return Seek(objective);
        }


        // accumulate behaviors:
        protected override void Navigator()
        {
            if (delay > 0)
            {
                delay -= SimulationTime.FrameTime();
            }
            else
            {
                Steer s = SeekTarget();
                self.ApplyYaw((float)s.yaw);
                self.ApplyPitch((float)s.pitch);
            }
        }


        protected virtual void CheckDecoys(double target_distance)
        {
            // if the assigned target has the burner lit,
            // ignore the decoys:
            if (orig_target != null && orig_target.Augmenter())
            {
                SetTarget(orig_target);
                return;
            }

            if (target != null &&
                    target == orig_target &&
                    orig_target.GetActiveDecoys().Count != 0)
            {

                foreach (Shot decoy in orig_target.GetActiveDecoys())
                {
                    double decoy_distance = (decoy.Location() - self.Location()).Length;

                    if (decoy_distance < target_distance)
                    {
                        if (RandomHelper.Rand() < 1600)
                        {
                            SetTarget(decoy, null);
                            return;
                        }
                    }
                }
            }
        }

        protected Ship orig_target;
        protected Shot shot;
        protected int pursuit;    // type of pursuit curve
                                  // 1: pure pursuit
                                  // 2: lead pursuit

        protected double delay;      // don't start seeking until then
        protected bool overshot;
    }
}