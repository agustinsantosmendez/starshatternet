﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CombatZone.h/CombatZone.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CombatZone is used by the dynamic campaign strategy
    and logistics algorithms to assign forces to locations
    within the campaign.  A CombatZone is a collection of
    closely related sectors, and the assets contained
    within them.
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning CombatZone class is still in development and is not recommended for production.
    public class CombatZone
    {
        // ~CombatZone();

        //int operator ==(const CombatZone& g)  const { return this == &g; }

        public string Name() { return name; }
        public string System() { return system; }
        public void AddGroup(CombatGroup group)
        {
            if (group != null)
            {
                int iff = group.GetIFF();
                ZoneForce f = FindForce(iff);
                f.AddGroup(group);
                group.SetCurrentZone(this);
            }
        }

        public void RemoveGroup(CombatGroup group)
        {
            if (group != null)
            {
                int iff = group.GetIFF();
                ZoneForce f = FindForce(iff);
                f.RemoveGroup(group);
                group.SetCurrentZone(null);
            }
        }

        public bool HasGroup(CombatGroup group)
        {
            if (group != null)
            {
                int iff = group.GetIFF();
                ZoneForce f = FindForce(iff);
                return f.HasGroup(group);
            }

            return false;
        }

        public void AddRegion(string rgn)
        {
            if (!string.IsNullOrEmpty(rgn))
            {
                regions.Add(rgn);

                if (string.IsNullOrEmpty(name))
                    name = rgn;
            }
        }

        public bool HasRegion(string rgn)
        {
            if (!string.IsNullOrEmpty(rgn) && regions.Count != 0)
            {
                return regions.Contains(rgn);
            }

            return false;
        }

        public List<string> GetRegions() { return regions; }
        public List<ZoneForce> GetForces() { return forces; }

        public ZoneForce FindForce(int iff)
        {
            foreach (ZoneForce f in forces)
            {
                if (f.GetIFF() == iff)
                    return f;
            }

            return MakeForce(iff);
        }

        public ZoneForce MakeForce(int iff)
        {
            ZoneForce f = new ZoneForce(iff);
            forces.Add(f);
            return f;
        }

        public void Clear()
        {
            forces.Clear();
        }

        public static List<CombatZone> Load(string filename)
        {
            List<CombatZone> zonelist = new List<CombatZone>();
            var czl = ReaderSupport.DeserializeAsset<CombatZoneList>(filename);

            if (czl == null)
                return null;

            foreach (CombatZoneInfo czi in czl.zones)
            {
                CombatZone zone = new CombatZone()
                {
                    system = czi.system,
                };
                foreach (string r in czi.regions)
                    zone.AddRegion(r);
                zonelist.Add(zone);
            }

            return zonelist;
        }


        // attributes:
        private string name;
        private string system;
        private List<string> regions = new List<string>();
        private List<ZoneForce> forces = new List<ZoneForce>();
    }

    [Serializable]
    public class CombatZoneInfo
    {
        public string system;
        public List<string> regions;
    }

    [Serializable]
    public class CombatZoneList
    {
        public List<CombatZoneInfo> zones;
    }

    public class ZoneForce
    {
        public ZoneForce(int i)
        {
            iff = i;

            for (int n = 0; n < 8; n++)
                need[n] = 0;
        }


        public int GetIFF() { return iff; }
        public List<CombatGroup> GetGroups() { return groups; }
        public List<CombatGroup> GetTargetList() { return target_list; }
        public List<CombatGroup> GetDefendList() { return defend_list; }

        public void AddGroup(CombatGroup group)
        {
            if (group != null)
                groups.Add(group);
        }
        public void RemoveGroup(CombatGroup group)
        {
            if (group != null)
                groups.Remove(group);
        }
        public bool HasGroup(CombatGroup group)
        {
            if (group != null)
                return groups.Contains(group);

            return false;
        }

        public int GetNeed(CombatGroup.GROUP_TYPE group_type)
        {
            switch (group_type)
            {
                case CombatGroup.GROUP_TYPE.CARRIER_GROUP: return need[0];
                case CombatGroup.GROUP_TYPE.BATTLE_GROUP: return need[1];
                case CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON: return need[2];
                case CombatGroup.GROUP_TYPE.ATTACK_SQUADRON: return need[3];
                case CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON: return need[4];
                case CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON: return need[5];
            }

            return 0;
        }

        public void SetNeed(CombatGroup.GROUP_TYPE group_type, int needed)
        {
            switch (group_type)
            {
                case CombatGroup.GROUP_TYPE.CARRIER_GROUP: need[0] = needed; break;
                case CombatGroup.GROUP_TYPE.BATTLE_GROUP: need[1] = needed; break;
                case CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON: need[2] = needed; break;
                case CombatGroup.GROUP_TYPE.ATTACK_SQUADRON: need[3] = needed; break;
                case CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON: need[4] = needed; break;
                case CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON: need[5] = needed; break;
            }
        }

        public void AddNeed(CombatGroup.GROUP_TYPE group_type, int needed)
        {
            switch (group_type)
            {
                case CombatGroup.GROUP_TYPE.CARRIER_GROUP: need[0] += needed; break;
                case CombatGroup.GROUP_TYPE.BATTLE_GROUP: need[1] += needed; break;
                case CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON: need[2] += needed; break;
                case CombatGroup.GROUP_TYPE.ATTACK_SQUADRON: need[3] += needed; break;
                case CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON: need[4] += needed; break;
                case CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON: need[5] += needed; break;
            }
        }


        // attributes:
        private int iff;
        private List<CombatGroup> groups = new List<CombatGroup>();
        private List<CombatGroup> defend_list= new List<CombatGroup>();
        private List<CombatGroup> target_list=new List<CombatGroup>();
        private int[] need = new int[8];
    }
}