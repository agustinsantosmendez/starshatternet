﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      EngDlg.h/EngDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Engineering (Power/Maint) Dialog Active Window class
*/
using System.Collections.Generic;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.SimElements;
using UnityEngine;
using Component = StarshatterNet.Stars.SimElements.Component;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class EngDlg : FormWindow
    {
        public EngDlg(Screen s, Camera c, FormDef def, GameScreen mgr, GameObject p) :
            base(s, 0, 0, s.Width(), s.Height(), c, p, "EngDlg")
        {
            manager = mgr;
            ship = null; route_source = null; selected_source = null;
            selected_repair = null; selected_component = null;
            Init(def);
        }
        //public virtual ~EngDlg();
        public override void RegisterControls()
        {
            for (int i = 0; i < 4; i++)
            {
                sources[i] = (Button)FindControl(201 + i);
                REGISTER_CLIENT(ETD.EID_CLICK, sources[i], OnSource);

                source_levels[i] = (Slider)FindControl(211 + i);

                clients[i] = (ListBox)FindControl(301 + i);

                if (clients[i] != null)
                {
                    clients[i].SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_FILLED_BOX);
                    clients[i].SetSortColumn(0);

                    REGISTER_CLIENT(ETD.EID_SELECT, clients[i], OnClient);
                    REGISTER_CLIENT(ETD.EID_DRAG_START, clients[i], OnRouteStart);
                    REGISTER_CLIENT(ETD.EID_DRAG_DROP, clients[i], OnRouteComplete);
                }
            }

            close_btn = (Button)FindControl(1);
            selected_name = (ActiveWindow)FindControl(401);
            power_off = (Button)FindControl(402);
            power_on = (Button)FindControl(403);
            override_ = (Button)FindControl(410);
            power_level = (Slider)FindControl(404);
            capacity = (Slider)FindControl(405);

            if (close_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, close_btn, OnClose);

            if (power_off != null)
                REGISTER_CLIENT(ETD.EID_CLICK, power_off, OnPowerOff);

            if (power_on != null)
                REGISTER_CLIENT(ETD.EID_CLICK, power_on, OnPowerOn);

            if (override_ != null)
                REGISTER_CLIENT(ETD.EID_CLICK, override_, OnOverride);

            if (power_level != null)
                REGISTER_CLIENT(ETD.EID_CLICK, power_level, OnPowerLevel);

            components = (ListBox)FindControl(501);

            if (components != null)
            {
                components.SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_FILLED_BOX);
                components.SetSortColumn(0);
                REGISTER_CLIENT(ETD.EID_SELECT, components, OnComponent);
            }

            auto_repair = (Button)FindControl(700);
            repair = (Button)FindControl(502);
            replace = (Button)FindControl(503);
            repair_time = FindControl(512);
            replace_time = FindControl(513);
            priority_increase = (Button)FindControl(602);
            priority_decrease = (Button)FindControl(603);

            if (auto_repair != null)
                REGISTER_CLIENT(ETD.EID_CLICK, auto_repair, OnAutoRepair);

            if (repair != null)
                REGISTER_CLIENT(ETD.EID_CLICK, repair, OnRepair);

            if (replace != null)
                REGISTER_CLIENT(ETD.EID_CLICK, replace, OnReplace);

            if (repair_time != null)
                repair_time.Hide();

            if (replace_time != null)
                replace_time.Hide();

            if (priority_increase != null)
            {
                string up_arrow = char.ConvertFromUtf32(0x2191); // Font.ARROW_UP;
                priority_increase.SetText(up_arrow);

                REGISTER_CLIENT(ETD.EID_CLICK, priority_increase, OnPriorityIncrease);
            }

            if (priority_decrease != null)
            {
                string dn_arrow = char.ConvertFromUtf32(0x2193); // Font.ARROW_DOWN;
                priority_decrease.SetText(dn_arrow);

                REGISTER_CLIENT(ETD.EID_CLICK, priority_decrease, OnPriorityDecrease);
            }

            repair_queue = (ListBox)FindControl(601);

            if (repair_queue != null)
            {
                repair_queue.SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_FILLED_BOX);
                REGISTER_CLIENT(ETD.EID_SELECT, repair_queue, OnQueue);
            }
        }
        public override void Show()
        {
            base.Show();

            if (ship != null)
            {
                int nsources = ship.Reactors().Count;

                for (int i = 0; i < 4; i++)
                {
                    if (i >= nsources)
                    {
                        sources[i].Hide();
                        source_levels[i].Hide();
                        clients[i].Hide();
                    }
                }
            }
        }

        public override void Hide()
        {
            base.Hide();
        }

        // Operations:
        public virtual void OnSource(ActiveWindow obj, AWEvent evnt)
        {
            selected_source = null;
            selected_clients.Clear();
            selected_component = null;

            if (ship == null) return;

            int source_index = -1;

            for (int i = 0; i < 4; i++)
                if (evnt.window == sources[i])
                    source_index = i;

            // found the source list:
            if (source_index >= 0)
            {
                selected_source = ship.Reactors()[source_index];
            }

            for (int i = 0; i < 4; i++)
            {
                clients[i].ClearSelection();
            }

            if (components != null)
            {
                components.ClearItems();

                if (repair != null) repair.SetEnabled(false);
                if (replace != null) replace.SetEnabled(false);
                if (repair_time != null) repair_time.Hide();
                if (replace_time != null) replace_time.Hide();

                if (selected_source != null && selected_source.GetComponents().Count != 0)
                {
                    int index = 0;
                    foreach (Component comp in selected_source.GetComponents())
                        components.AddItemWithData(Game.GetText(comp.Abbreviation()), index++);
                }
            }
        }
        public virtual void OnClient(ActiveWindow obj, AWEvent evnt)
        {
            selected_source = null;
            selected_clients.Clear();
            selected_component = null;

            if (ship == null) return;

            int source_index = -1;

            for (int i = 0; i < 4; i++)
            {
                if (evnt.window == clients[i])
                    source_index = i;
                else
                    clients[i].ClearSelection();
            }

            // found the source list:
            if (source_index >= 0)
            {
                // find the power source:
                PowerSource src = ship.Reactors()[source_index];

                // build a list of the clients to be manipulated:
                List<ShipSystem> client_list = src.Clients();
                for (int i = 0; i < clients[source_index].NumItems(); i++)
                {
                    if (clients[source_index].IsSelected(i))
                    {
                        int index = (int)clients[source_index].GetItemData(i);
                        selected_clients.Add(client_list[index]);
                    }
                }
            }

            if (components != null)
            {
                components.ClearItems();

                if (repair != null) repair.SetEnabled(false);
                if (replace != null) replace.SetEnabled(false);
                if (repair_time != null) repair_time.Hide();
                if (replace_time != null) replace_time.Hide();

                if (selected_clients.Count == 1)
                {
                    ShipSystem sink = selected_clients[0];

                    if (sink.GetComponents().Count != 0)
                    {
                        int index = 0;
                        foreach (Component comp in sink.GetComponents())
                            components.AddItemWithData(Game.GetText(comp.Abbreviation()), index++);
                    }
                }
            }
        }
        public virtual void OnRouteStart(ActiveWindow obj, AWEvent evnt)
        {
            if (ship == null) return;

            int source_index = -1;

            for (int i = 0; i < 4; i++)
                if (evnt.window == clients[i])
                    source_index = i;

            // found the source list:
            if (source_index >= 0)
            {
                // remember the power source:
                route_source = ship.Reactors()[source_index];
                route_list.Clear();

                // build a list of the clients to be moved:
                List<ShipSystem> rsc = route_source.Clients();
                for (int i = 0; i < clients[source_index].NumItems(); i++)
                {
                    if (clients[source_index].IsSelected(i))
                    {
                        int index = (int)clients[source_index].GetItemData(i);
                        route_list.Add(rsc[index]);
                    }
                }
            }
        }
        public virtual void OnRouteComplete(ActiveWindow obj, AWEvent evnt)
        {
            if (ship == null || route_source == null) return;

            int dest_index = -1;

            for (int i = 0; i < 4; i++)
                if (evnt.window == clients[i])
                    dest_index = i;

            // found the destination list, copy the clients over:
            if (dest_index >= 0)
            {
                PowerSource route_dest = ship.Reactors()[dest_index];

                if (route_dest == null)
                    return;

                foreach (ShipSystem client in route_list)
                {
                    route_source.RemoveClient(client);
                    route_dest.AddClient(client);
                }
            }
        }
        public virtual void OnPowerOff(ActiveWindow obj, AWEvent evnt)
        {
            if (selected_source != null)
            {
                selected_source.PowerOff();

                power_off.SetButtonState(1);
                power_on.SetButtonState(0);
                override_.SetButtonState(0);
            }

            else if (selected_clients.Count > 0)
            {
                foreach (ShipSystem iter in selected_clients)
                    iter.PowerOff();

                power_off.SetButtonState(1);
                power_on.SetButtonState(0);
                override_.SetButtonState(0);
            }
        }

        public virtual void OnPowerOn(ActiveWindow obj, AWEvent evnt)
        {
            if (selected_source != null)
            {
                selected_source.PowerOn();

                power_off.SetButtonState(0);
                power_on.SetButtonState(1);
                override_.SetButtonState(0);
            }

            else if (selected_clients.Count > 0)
            {
                foreach (ShipSystem iter in selected_clients)
                    iter.PowerOn();

                power_off.SetButtonState(0);
                power_on.SetButtonState(1);
                override_.SetButtonState(0);
            }
        }
        public virtual void OnOverride(ActiveWindow obj, AWEvent evnt)
        {
            bool over = false;

            if (override_.GetButtonState() > 0)
                over = true;

            if (selected_source != null)
            {
                selected_source.SetOverride(over);
            }

            else if (selected_clients.Count > 0)
            {
                foreach (ShipSystem iter in selected_clients)
                    iter.SetOverride(over);
            }

            if (over)
            {
                power_off.SetButtonState(0);
                power_on.SetButtonState(1);
            }
        }
        public virtual void OnPowerLevel(ActiveWindow obj, AWEvent evnt)
        {
            int level = power_level.GetValue();

            if (level < 0)
                level = 0;
            else if (level > 100)
                level = 100;

            if (selected_source != null)
                selected_source.SetPowerLevel(level);

            else if (selected_clients.Count > 0)
            {
                foreach (ShipSystem iter in selected_clients)
                    iter.SetPowerLevel(level);
            }

            if (override_ != null)
                override_.SetButtonState(0);
        }
        public virtual void OnComponent(ActiveWindow obj, AWEvent evnt)
        {
            selected_component = null;

            if (repair != null) repair.SetEnabled(false);
            if (replace != null) replace.SetEnabled(false);
            if (repair_time != null) repair_time.Hide();
            if (replace_time != null) replace_time.Hide();

            // find index of selected component:
            int component_index = -1;

            if (components != null)
            {
                int n = components.GetSelection();
                if (n >= 0)
                    component_index = (int)components.GetItemData(n);
            }

            // now examine the component info:
            if (component_index >= 0)
            {
                repair.SetEnabled(true);

                if (selected_source != null)
                {
                    List<Component> comps = selected_source.GetComponents();
                    selected_component = comps[component_index];

                    if (repair_time != null)
                    {
                        string t;
                        int s = (int)(selected_component.RepairTime() /
                        ship.RepairSpeed());
                        FormatUtil.FormatTime(out t, s);
                        repair_time.SetText(t);
                        repair_time.Show();
                    }

                    if (selected_component.SpareCount() > 0)
                    {
                        if (replace != null) replace.SetEnabled(true);

                        if (replace_time != null)
                        {
                            string t;
                            int s = (int)(selected_component.ReplaceTime() /
                            ship.RepairSpeed());
                            FormatUtil.FormatTime(out t, s);
                            replace_time.SetText(t);
                            replace_time.Show();
                        }
                    }
                }
                else if (selected_clients.Count > 0)
                {
                    List<Component> comps = selected_clients[0].GetComponents();
                    selected_component = comps[component_index];

                    if (repair_time != null)
                    {
                        string t;
                        int s = (int)(selected_component.RepairTime() /
                        ship.RepairSpeed());
                        FormatUtil.FormatTime(out t, s);
                        repair_time.SetText(t);
                        repair_time.Show();
                    }

                    if (selected_component.SpareCount() > 0)
                    {
                        if (replace != null) replace.SetEnabled(true);

                        if (replace_time != null)
                        {
                            string t;
                            int s = (int)(selected_component.ReplaceTime() /
                            ship.RepairSpeed());
                            FormatUtil.FormatTime(out t, s);
                            replace_time.SetText(t);
                            replace_time.Show();
                        }
                    }
                }
            }
        }
        public virtual void OnAutoRepair(ActiveWindow obj, AWEvent evnt)
        {
            if (ship != null)
                ship.EnableRepair(auto_repair.GetButtonState() > 0);
        }
        public virtual void OnRepair(ActiveWindow obj, AWEvent evnt)
        {
            if (selected_component != null)
            {
                selected_component.Repair();

                if (ship != null)
                    ship.RepairSystem(selected_component.GetSystem());
            }
        }
        public virtual void OnReplace(ActiveWindow obj, AWEvent evnt)
        {
            if (selected_component != null)
            {
                selected_component.Replace();

                if (ship != null)
                    ship.RepairSystem(selected_component.GetSystem());
            }
        }
        public virtual void OnQueue(ActiveWindow obj, AWEvent evnt)
        {
            selected_repair = null;

            if (priority_increase != null) priority_increase.SetEnabled(false);
            if (priority_decrease != null) priority_decrease.SetEnabled(false);

            if (repair_queue != null)
            {
                int n = repair_queue.GetSelection();

                if (n >= 0)
                    selected_repair = ship.RepairQueue()[n];
            }

            if (selected_repair == null)
                return;

            selected_clients.Clear();
            selected_clients.Add(selected_repair);

            if (components != null)
            {
                components.ClearItems();

                if (repair != null) repair.SetEnabled(false);
                if (replace != null) replace.SetEnabled(false);
                if (repair_time != null) repair_time.Hide();
                if (replace_time != null) replace_time.Hide();

                if (selected_repair.GetComponents().Count != 0)
                {
                    int index = 0;
                    foreach (Component comp in selected_repair.GetComponents())
                        components.AddItemWithData(Game.GetText(comp.Abbreviation()), index++);
                }
            }

            if (priority_increase != null) priority_increase.SetEnabled(true);
            if (priority_decrease != null) priority_decrease.SetEnabled(true);
        }
        public virtual void OnPriorityIncrease(ActiveWindow obj, AWEvent evnt)
        {
            if (ship != null && repair_queue != null)
                ship.IncreaseRepairPriority(repair_queue.GetSelection());
        }
        public virtual void OnPriorityDecrease(ActiveWindow obj, AWEvent evnt)
        {
            if (ship != null && repair_queue != null)
                ship.DecreaseRepairPriority(repair_queue.GetSelection());
        }
        public virtual void OnClose(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.CloseTopmost();
        }

        public virtual void ExecFrame()
        {
            if (IsShown())
                UpdateSelection();
        }

        public void UpdateRouteTables()
        {
            for (int i = 0; i < 4; i++)
                clients[i].ClearSelection();

            if (components != null)
                components.ClearItems();

            if (priority_increase != null)
                priority_increase.SetEnabled(false);

            if (priority_decrease != null)
                priority_decrease.SetEnabled(false);

            if (ship != null)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (sources[i] != null)
                        sources[i].Hide();

                    if (source_levels[i] != null)
                        source_levels[i].Hide();

                    if (clients[i] != null)
                    {
                        clients[i].ClearItems();
                        clients[i].Hide();
                    }
                }

                int reactor_index = 0;
                foreach (PowerSource reactor in ship.Reactors())
                {
                    if (sources[reactor_index] != null && clients[reactor_index] != null)
                    {
                        sources[reactor_index].SetText(Game.GetText(reactor.Abbreviation()));
                        sources[reactor_index].Show();

                        source_levels[reactor_index].Show();
                        source_levels[reactor_index].SetValue((int)reactor.GetPowerLevel());

                        clients[reactor_index].Show();

                        int index = 0;
                        foreach (ShipSystem client in reactor.Clients())
                        {
                            string abrv, num;
                            FormatUtil.FormatNumber(out num, client.GetPowerLevel());
                            abrv = Game.GetText(client.Name());

                            clients[reactor_index].AddItemWithData(Game.GetText(abrv), index);
                            clients[reactor_index].SetItemText(index, 1, num);
                            clients[reactor_index].SetItemData(index, 1, (int)client.GetPowerLevel());

                            index++;
                        }

                        clients[reactor_index].SortItems();
                    }

                    reactor.RouteScanned();
                    reactor_index++;
                }
            }
        }
        public void UpdateSelection()
        {
            if (ship == null) return;

            string num;
            int nsources = ship.Reactors().Count;

            // update the route tables:
            for (int source_index = 0; source_index < nsources; source_index++)
            {
                PowerSource reac = ship.Reactors()[source_index];

                if (reac.RouteChanged())
                    UpdateRouteTables();

                Color c = new Color32(62, 106, 151, 255);

                if (reac.IsPowerOn())
                {
                    switch (reac.Status())
                    {
                        default:
                        case ShipSystem.STATUS.NOMINAL: break;
                        case ShipSystem.STATUS.DEGRADED: c = Color.yellow; break;
                        case ShipSystem.STATUS.CRITICAL: c = new Color32(255, 0, 0, 255) /*Color.BrightRed*/; break;
                        case ShipSystem.STATUS.DESTROYED: c = new Color32(128, 0, 0, 255) /*Color.DarkRed*/; break;
                    }
                }
                else
                {
                    c = Color.gray;
                }

                sources[source_index].SetBackColor(c);
                source_levels[source_index].SetEnabled(reac.IsPowerOn());
                source_levels[source_index].SetValue((int)reac.GetPowerLevel());

                ListBox client_list = clients[source_index];

                for (int i = 0; i < client_list.NumItems(); i++)
                {
                    int index = (int)client_list.GetItemData(i);

                    ShipSystem client = reac.Clients()[index];
                    FormatUtil.FormatNumber(out num, client.GetPowerLevel());
                    client_list.SetItemText(i, 1, num);
                    client_list.SetItemData(i, 1, (int)client.GetPowerLevel());

                    if (client.IsPowerOn())
                    {
                        Color c2;

                        switch (client.Status())
                        {
                            default:
                            case ShipSystem.STATUS.NOMINAL: c2 = Color.white; break;
                            case ShipSystem.STATUS.DEGRADED: c2 = Color.yellow; break;
                            case ShipSystem.STATUS.CRITICAL: c2 = new Color32(255, 0, 0, 255) /*Color.BrightRed*/; break;
                            case ShipSystem.STATUS.DESTROYED: c2 = new Color32(128, 0, 0, 255) /*Color.DarkRed*/; break;
                        }

                        client_list.SetItemColor(i, c2);
                    }
                    else
                    {
                        client_list.SetItemColor(i, Color.gray);
                    }

                }
            }

            // update the detail info:
            if (selected_source != null)
            {
                selected_name.SetText(Game.GetText(selected_source.Name()));
                power_off.SetEnabled(true);
                power_on.SetEnabled(true);
                override_.SetEnabled(true);
                if (override_.GetButtonState() != 2)
                    override_.SetButtonState(selected_source.GetPowerLevel() > 100 ? (short)1 : (short)0);
                power_level.SetEnabled(selected_source.IsPowerOn());
                power_level.SetValue((int)selected_source.GetPowerLevel());

                if (selected_source.Safety() < 100)
                {
                    power_level.SetMarker((int)selected_source.Safety(), 0);
                    power_level.SetMarker((int)selected_source.Safety(), 1);
                }
                else
                {
                    power_level.SetMarker(-1, 0);
                    power_level.SetMarker(-1, 1);
                }

                capacity.SetEnabled(true);
                capacity.SetValue((int)selected_source.Charge());

                if (selected_source.IsPowerOn())
                {
                    if (power_on.GetButtonState() != 2)
                        power_on.SetButtonState(1);
                    if (power_off.GetButtonState() != 2)
                        power_off.SetButtonState(0);
                }
                else
                {
                    if (power_on.GetButtonState() != 2)
                        power_on.SetButtonState(0);
                    if (power_off.GetButtonState() != 2)
                        power_off.SetButtonState(1);
                }

                if (components != null)
                {
                    for (int i = 0; i < components.NumItems(); i++)
                    {
                        int index = (int)components.GetItemData(i);

                        Component comp = selected_source.GetComponents()[index];

                        string stat = "OK";
                        Color c = Color.black;

                        switch (comp.Status())
                        {
                            case SimElements.Component.STATUS.DESTROYED:
                            case SimElements.Component.STATUS.CRITICAL: stat = "FAIL"; c = new Color32(255, 0, 0, 255) /*Color.BrightRed*/; break;
                            case SimElements.Component.STATUS.DEGRADED: stat = "WARN"; c = Color.yellow; break;
                            case SimElements.Component.STATUS.NOMINAL: stat = "OK"; c = Color.white; break;
                            case SimElements.Component.STATUS.REPLACE:
                            case SimElements.Component.STATUS.REPAIR: stat = "MAINT"; c = Color.cyan; break;
                        }

                        stat = Game.GetText("EngDlg." + stat);
                        components.SetItemText(i, 1, stat);
                        components.SetItemData(i, 1, (int)comp.Status());

                        FormatUtil.FormatNumber(out num, comp.SpareCount());
                        components.SetItemText(i, 2, num);
                        components.SetItemData(i, 2, comp.SpareCount());
                        components.SetItemColor(i, c);
                    }
                }
            }

            else if (selected_clients.Count == 1)
            {
                ShipSystem sink = selected_clients[0];

                selected_name.SetText(Game.GetText(sink.Name()));
                power_off.SetEnabled(true);
                power_on.SetEnabled(true);
                override_.SetEnabled(true);
                if (override_.GetButtonState() != 2)
                    override_.SetButtonState(sink.GetPowerLevel() > 100 ? (short)1 : (short)0);
                power_level.SetEnabled(sink.IsPowerOn());
                power_level.SetValue((int)sink.GetPowerLevel());

                if (sink.Safety() < 100)
                {
                    power_level.SetMarker((int)sink.Safety(), 0);
                    power_level.SetMarker((int)sink.Safety(), 1);
                }
                else
                {
                    power_level.SetMarker(-1, 0);
                    power_level.SetMarker(-1, 1);
                }

                capacity.SetEnabled(true);
                capacity.SetValue((int)sink.Charge());

                if (sink.IsPowerOn())
                {
                    if (power_on.GetButtonState() != 2)
                        power_on.SetButtonState(1);
                    if (power_off.GetButtonState() != 2)
                        power_off.SetButtonState(0);
                }
                else
                {
                    if (power_on.GetButtonState() != 2)
                        power_on.SetButtonState(0);
                    if (power_off.GetButtonState() != 2)
                        power_off.SetButtonState(1);
                }

                if (components != null)
                {
                    for (int i = 0; i < components.NumItems(); i++)
                    {
                        int index = (int)components.GetItemData(i);

                        SimElements.Component comp = sink.GetComponents()[index];

                        string stat = "OK";
                        Color c = Color.black;

                        switch (comp.Status())
                        {
                            case SimElements.Component.STATUS.DESTROYED:
                            case SimElements.Component.STATUS.CRITICAL: stat = "FAIL"; c = new Color32(255, 0, 0, 255) /*Color.BrightRed*/; break;
                            case SimElements.Component.STATUS.DEGRADED: stat = "WARN"; c = Color.yellow; break;
                            case SimElements.Component.STATUS.NOMINAL: stat = "OK"; c = Color.white; break;
                            case SimElements.Component.STATUS.REPLACE:
                            case SimElements.Component.STATUS.REPAIR: stat = "MAINT"; c = Color.cyan; break;
                        }

                        stat = Game.GetText( "EngDlg."  + stat);
                        components.SetItemText(i, 1, stat);
                        components.SetItemData(i, 1, (int)comp.Status());

                        FormatUtil.FormatNumber(out num, comp.SpareCount());
                        components.SetItemText(i, 2, num);
                        components.SetItemData(i, 2, comp.SpareCount());
                        components.SetItemColor(i, c);
                    }
                }
            }

            else if (selected_clients.Count > 1)
            {
                ShipSystem sink = selected_clients[0];

                selected_name.SetText(Game.GetText("[Multiple]"));
                power_off.SetEnabled(true);
                power_on.SetEnabled(true);
                override_.SetEnabled(true);
                if (override_.GetButtonState() != 2)
                    override_.SetButtonState(sink.GetPowerLevel() > 100 ? (short)1 : (short)0);
                power_level.SetEnabled(true);
                power_level.SetValue((int)sink.GetPowerLevel());

                if (sink.Safety() < 100)
                {
                    power_level.SetMarker((int)sink.Safety(), 0);
                    power_level.SetMarker((int)sink.Safety(), 1);
                }
                else
                {
                    power_level.SetMarker(-1, 0);
                    power_level.SetMarker(-1, 1);
                }

                capacity.SetEnabled(true);
                capacity.SetValue((int)sink.Charge());

                if (sink.IsPowerOn())
                {
                    if (power_on.GetButtonState() != 2)
                        power_on.SetButtonState(1);
                    if (power_off.GetButtonState() != 2)
                        power_off.SetButtonState(0);
                }
                else
                {
                    if (power_on.GetButtonState() != 2)
                        power_on.SetButtonState(0);
                    if (power_off.GetButtonState() != 2)
                        power_off.SetButtonState(1);
                }
            }

            else
            {
                selected_name.SetText(Game.GetText("No Selection"));
                power_off.SetEnabled(false);
                power_off.SetButtonState(0);
                power_on.SetEnabled(false);
                power_on.SetButtonState(0);
                override_.SetEnabled(false);
                override_.SetButtonState(0);
                power_level.SetEnabled(false);
                power_level.SetValue(0);
                power_level.SetMarker(-1, 0);
                power_level.SetMarker(-1, 1);
                capacity.SetEnabled(false);
                capacity.SetValue(0);
            }

            // display the repair queue:
            if (repair_queue != null)
            {
                // if adding to the queue, dump and reload:
                if (repair_queue.NumItems() < ship.RepairQueue().Count)
                {
                    repair_queue.ClearItems();

                    int i = 0;
                    foreach (ShipSystem sys in ship.RepairQueue())
                    {
                        double time_remaining = 0;
                        string etr;

                        foreach (Component c in sys.GetComponents())
                        {
                            if (c.TimeRemaining() > time_remaining)
                                time_remaining = c.TimeRemaining();
                        }

                        FormatUtil.FormatTime(out etr, (int)(time_remaining / ship.RepairSpeed()));
                        repair_queue.AddItem(Game.GetText(sys.Name()));
                        repair_queue.SetItemText(i, 1, etr);
                        i++;
                    }
                }

                // otherwise, update in place:
                else
                {
                    while (repair_queue.NumItems() > ship.RepairQueue().Count)
                        repair_queue.RemoveItem(0);

                    bool found = false;

                    for (int i = 0; i < repair_queue.NumItems(); i++)
                    {
                        double time_remaining = 0;
                        string etr;

                        ShipSystem sys = ship.RepairQueue()[i];

                        foreach (Component c in sys.GetComponents())
                        {
                            if (c.TimeRemaining() > time_remaining)
                                time_remaining = c.TimeRemaining();
                        }

                        FormatUtil.FormatTime(out etr, (int)time_remaining);
                        repair_queue.SetItemText(i, sys.Name());
                        repair_queue.SetItemText(i, 1, etr);

                        if (sys == selected_repair)
                        {
                            found = true;

                            if (Mouse.LButton() == 0)
                                repair_queue.SetSelected(i);
                        }
                    }

                    if (!found)
                        selected_repair = null;
                }
            }

            if (auto_repair != null && auto_repair.GetButtonState() != 2)
                auto_repair.SetButtonState(ship.AutoRepair() ? (short)1 : (short)0);
        }


        public void SetShip(Ship s)
        {
            if (IsShown() && ship != s)
            {
                selected_source = null;
                selected_repair = null;
                selected_component = null;
                selected_clients.Clear();

                ship = s;

                UpdateRouteTables();
                ExecFrame();
            }

            // make sure we clear the ship, even if not shown:
            else if (s == null)
            {
                ship = null;
            }
        }


        protected Ship ship;
        protected GameScreen manager;

        protected Button close_btn;
        protected Button[] sources = new Button[4];
        protected Slider[] source_levels = new Slider[4];
        protected ListBox[] clients = new ListBox[4];
        protected ListBox components;
        protected ListBox repair_queue;
        protected ActiveWindow selected_name;
        protected Button power_off;
        protected Button power_on;
        protected Button override_;
        protected Slider power_level;
        protected Slider capacity;
        protected Button auto_repair;
        protected Button repair;
        protected Button replace;
        protected ActiveWindow repair_time;
        protected ActiveWindow replace_time;
        protected Button priority_increase;
        protected Button priority_decrease;

        protected PowerSource route_source;
        protected List<ShipSystem> route_list;

        protected PowerSource selected_source;
        protected List<ShipSystem> selected_clients;

        protected ShipSystem selected_repair;
        protected SimElements.Component selected_component;
    }
}