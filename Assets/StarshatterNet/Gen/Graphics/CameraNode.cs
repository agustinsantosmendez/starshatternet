﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    FILE:         Camera.h/Camera.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Camera class - Position and Point of View
*/
using System;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Graphics.General;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;


namespace StarshatterNet.Gen.Graphics
{
    // Creates and controls a 3D camera node. (The camera node is not automatically
    // set as the active camera in any graphics screen. This needs to be done by the
    // sample.)
    public class CameraNode
    {
        public CameraNode(GameObject camera, double x = 0.0, double y = 0.0, double z = 0.0)
        {
            pos = new Point(x, y, z);
            gameObj = camera;
            cam = gameObj.GetComponent<Camera>();
            if (cam ==null)
                cam = gameObj.AddComponent<Camera>();
            cam.clearFlags = CameraClearFlags.Color;
            cam.backgroundColor = Color.black;

            UpdateGameObject(true);
        }
        public CameraNode(double x = 0.0, double y = 0.0, double z = 0.0)
        {
            pos = new Point(x, y, z);
            gameObj = new GameObject("MainCamera");
            //cameraGameObject.transform.SetParent(sceneTrasnform);
            cam = gameObj.AddComponent<Camera>();
            cam.clearFlags = CameraClearFlags.Color;
            cam.backgroundColor = Color.black;

            UpdateGameObject(true);
        }

        public void Aim(double roll, double pitch, double yaw) {
            orientation.Rotate(roll, pitch, yaw);
            gameObj.transform.Rotate((float)roll, (float)pitch, (float)yaw);
        }
        public void Roll(double roll) { orientation.Roll(roll); }
        public void Pitch(double pitch) { orientation.Pitch(pitch); }
        public void Yaw(double yaw) { orientation.Yaw(yaw); }

        public void MoveTo(double x, double y, double z)
        {
            pos.X = x;
            pos.Y = y;
            pos.Z = z;

            UpdateGameObject();
        }

        public void MoveTo(Point p)
        {
            pos.X = p.X;
            pos.Y = p.Y;
            pos.Z = p.Z;

            UpdateGameObject();
        }

        public void MoveBy(double dx, double dy, double dz)
        {
            pos.X += dx;
            pos.Y += dz;
            pos.Z += dy;

            UpdateGameObject();
        }

        public void MoveBy(Point p)
        {
            pos.X += p.X;
            pos.Y += p.Y;
            pos.Z += p.Z;

            UpdateGameObject();
        }

        public void Clone(CameraNode cam)
        {
            pos = cam.pos;
            orientation = cam.orientation;
        }
        public CameraNode Clone()
        {
            CameraNode cam = new CameraNode();
            cam.pos = this.pos;
            cam.orientation = this.orientation;
            return cam;
        }
        public void LookAt(Point target)
        {
            // No navel gazing:
            if (target == Pos())
                return;

            Point tgt, tmp = target - Pos();

            // Rotate into the view orientation:
            tgt.X = Point.Dot(tmp, vrt());
            tgt.Y = Point.Dot(tmp, vup());
            tgt.Z = Point.Dot(tmp, vpn());

            if (tgt.Z == 0)
            {
                Pitch(0.5);
                Yaw(0.5);
                LookAt(target);
                return;
            }

            double az = Math.Atan(tgt.X / tgt.Z);
            double el = Math.Atan(tgt.Y / tgt.Z);

            // if target is behind, offset by 180 degrees:
            if (tgt.Z < 0)
                az -= Math.PI;

            Pitch(-el);
            Yaw(az);

            // roll to upright position:
            double deflection = vrt().Y;
            while (Math.Abs(deflection) > 0.001)
            {
                double theta = Math.Asin(deflection / vrt().Length);
                Roll(-theta);

                deflection = vrt().Y;
            }
        }

        public void LookAt(Point target, Point eye, Point up)
        {
            Point direction = target - eye;
            orient = Quaternion.LookRotation(new Vector3((float)direction.X, (float)direction.Y, (float)direction.Z));

            Point zaxis = target - eye; zaxis.Normalize();
            Point xaxis = Vector3D.Cross(up, zaxis); xaxis.Normalize();
            Point yaxis = Vector3D.Cross(zaxis, xaxis); yaxis.Normalize();

            orientation[0, 0] = xaxis.X;
            orientation[0, 1] = xaxis.Y;
            orientation[0, 2] = xaxis.Z;

            orientation[1, 0] = yaxis.X;
            orientation[1, 1] = yaxis.Y;
            orientation[1, 2] = yaxis.Z;

            orientation[2, 0] = zaxis.X;
            orientation[2, 1] = zaxis.Z;

            pos = eye;
            UpdateGameObject(true);
        }
        public bool Padlock(Point target, double alimit = -1, double e_lo = -1, double e_hi = -1)
        {
            // No navel gazing:
            if (target == Pos())
                return false;

            Point tgt, tmp = target - Pos();

            // Rotate into the view orientation:
            tgt.X = Point.Dot(tmp, vrt());
            tgt.Y = Point.Dot(tmp, vup());
            tgt.Z = Point.Dot(tmp, vpn());

            if (tgt.Z == 0)
            {
                Yaw(0.1);

                tgt.X = Point.Dot(tmp, vrt());
                tgt.Y = Point.Dot(tmp, vup());
                tgt.Z = Point.Dot(tmp, vpn());

                if (tgt.Z == 0)
                    return false;
            }

            bool locked = true;
            double az = Math.Atan(tgt.X / tgt.Z);
            double orig = az;

            // if target is behind, offset by 180 degrees:
            if (tgt.Z < 0)
                az -= Math.PI;

            while (az > Math.PI) az -= 2 * Math.PI;
            while (az < -Math.PI) az += 2 * Math.PI;

            if (alimit > 0)
            {
                if (az < -alimit)
                {
                    az = -alimit;
                    locked = false;
                }
                else if (az > alimit)
                {
                    az = alimit;
                    locked = false;
                }
            }

            Yaw(az);

            // Rotate into the new view orientation:
            tgt.X = Point.Dot(tmp, vrt());
            tgt.Y = Point.Dot(tmp, vup());
            tgt.Z = Point.Dot(tmp, vpn());

            double el = Math.Atan(tgt.Y / tgt.Z);

            if (e_lo > 0 && el < -e_lo)
            {
                el = -e_lo;
                locked = false;
            }

            else if (e_hi > 0 && el > e_hi)
            {
                el = e_hi;
                locked = false;
            }

            Pitch(-el);

            return locked;
        }

        public virtual Scene GetScene() { return scene; }
        public virtual void SetScene(Scene s) { scene = s; }

        public Point Pos() { return pos; }
        public Point vrt() { return new Point(orientation[0, 0], orientation[0, 1], orientation[0, 2]); }
        public Point vup() { return new Point(orientation[1, 0], orientation[1, 1], orientation[1, 2]); }
        public Point vpn() { return new Point(orientation[2, 0], orientation[2, 1], orientation[2, 2]); }

        public Matrix33D Orientation() { return orientation; }

        protected void UpdateGameObject(bool updateOrientation = false)
        {
            //update the position
            gameObj.transform.position = new Vector3((float)pos.X, (float)pos.Y, (float)pos.Z);
            if (updateOrientation)
            {
                gameObj.transform.rotation = orient;
            }
        }

        protected Point pos;
        protected Matrix33D orientation;
        protected Quaternion orient;
        // to get the angles just access eulerAngles on the quaternion which LookRotation returned:
        // Vector3 angles = q.eulerAngles;
        //protected double roll, pitch, yaw;

        internal protected GameObject gameObj;
        protected Scene scene = null;
        private Camera cam;

        public Camera Camera { get => cam; set => cam = value; }
    }
}
