﻿
/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      GameScreen.h/GameScreen.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

*/

using System;
using StarshatterNet.Audio;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class GameScreen : BaseScreen
    {
        public GameScreen()
        {
            screen = null; gamewin = null;
            navdlg = null; wep_view = null; engdlg = null; fltdlg = null;
            HUDfont = null; GUIfont = null; GUI_small_font = null; title_font = null; cam_view = null;
            isShown = false; disp_view = null; hud_view = null; tac_view = null; radio_view = null;
            quantum_view = null; quit_view = null; ctldlg = null; joydlg = null; keydlg = null; auddlg = null;
            viddlg = null; moddlg = null; modInfoDlg = null;
            flare1 = null; flare2 = null; flare3 = null; flare4 = null;
            optdlg = null; cam_dir = null; sim = null;

            cam_dir = new CameraDirector();
            sim = Sim.GetSim();
            //loader = DataLoader.GetLoader();

            // find the fonts:
            HUDfont = FontMgr.Find("HUD");
            GUIfont = FontMgr.Find("GUI");
            GUI_small_font = FontMgr.Find("GUIsmall");
            title_font = FontMgr.Find("Title");

            flare1 = new Bitmap("flare0+.pcx", Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            flare2 = new Bitmap("flare2.pcx", Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            flare3 = new Bitmap("flare3.pcx", Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            flare4 = new Bitmap("flare4.pcx", Bitmap.BMP_TYPES.BMP_TRANSLUCENT);

            mouse_con = MouseController.GetInstance();
            game_screen = this;
        }
        //public virtual ~GameScreen();

        public virtual void Setup(Screen s, Camera camera, GameObject gameObj)
        {
            if (camera == null)
                return;

            screen = s;

            //loader.UseFileSystem(true);

            // create windows
            gamewin = new Window(screen, 0, 0, screen.Width(), screen.Height());
            gamewin.CreateCanvas();
            // attach views to windows (back to front)
            // fade in:
            gamewin.AddView(new FadeView(gamewin, "FadeView", 1, 0, 0));

            // camera:
            cam_dir = CameraDirector.GetInstance();
            CameraNode cam = new CameraNode(camera.gameObject);
            cam_dir.SetCamera(cam);
            Scene scene = new Scene();
            scene.AddCamera(cam_dir.GetCamera().Camera.gameObject);
            cam_view = new CameraView(gamewin, scene, null);

            if (cam_view != null)
                gamewin.AddView(cam_view);

            // HUD:
            hud_view = new HUDView(gamewin);
            gamewin.AddView(hud_view);

            wep_view = new WepView(gamewin);
            gamewin.AddView(wep_view);

            quantum_view = new QuantumView(gamewin);
            gamewin.AddView(quantum_view);

            radio_view = new RadioView(gamewin);
            gamewin.AddView(radio_view);

            tac_view = new TacticalView(gamewin, "TacticalView");
            gamewin.AddView(tac_view);

            disp_view = DisplayView.GetOrCreateInstance(gamewin);

            // note: quit view must be last in chain 
            // so it can release window surface
            quit_view = new QuitView(gamewin, "QuitView");
            gamewin.AddView(quit_view);

            Starshatter stars = Starshatter.GetInstance();

            // initialize lens flare bitmaps:
            if (stars.LensFlare() != 0)
            {
                cam_view.LensFlareElements(flare1, flare4, flare2, flare3);
                cam_view.LensFlare(true);
            }

            // if lens flare disabled, just create the corona:
            else if (stars.Corona() != 0)
            {
                cam_view.LensFlareElements(flare1, null, null, null);
                cam_view.LensFlare(true);
            }

            screen.AddWindow(gamewin);

            FormDef aud_def = new FormDef("AudDlg", 0);
            aud_def.Load("AudDlg");
            auddlg = new AudDlg(screen, camera, aud_def, this, gameObj);

            FormDef ctl_def = new FormDef("CtlDlg", 0);
            ctl_def.Load("CtlDlg");
            ctldlg = new CtlDlg(screen, camera, ctl_def, this, gameObj);

            FormDef opt_def = new FormDef("OptDlg", 0);
            opt_def.Load("OptDlg");
            optdlg = new OptDlg(screen, camera, opt_def, this, gameObj);

            FormDef vid_def = new FormDef("VidDlg", 0);
            vid_def.Load("VidDlg");
            viddlg = new VidDlg(screen, camera, vid_def, this, gameObj);

            FormDef mod_def = new FormDef("ModDlg", 0);
            mod_def.Load("ModDlg");
            moddlg = new ModDlg(screen, camera, mod_def, this, gameObj);

            FormDef joy_def = new FormDef("JoyDlg", 0);
            joy_def.Load("JoyDlg");
            joydlg = new JoyDlg(screen, camera, joy_def, this, gameObj);

            FormDef key_def = new FormDef("KeyDlg", 0);
            key_def.Load("KeyDlg");
            keydlg = new KeyDlg(screen, camera, key_def, this, gameObj);

            FormDef mod_info_def = new FormDef("ModInfoDlg", 0);
            mod_info_def.Load("ModInfoDlg");
            modInfoDlg = new ModInfoDlg(screen, camera, mod_info_def, this, gameObj);

            FormDef nav_def = new FormDef("NavDlg", 0);
            nav_def.Load("NavDlg");
            navdlg = new NavDlg(screen,   camera, nav_def, this, gameObj);

            FormDef eng_def = new FormDef("EngDlg", 0);
            eng_def.Load("EngDlg");
            engdlg = new EngDlg(screen, camera, eng_def, this, gameObj);

            FormDef flt_def = new FormDef("FltDlg", 0);
            flt_def.Load("FltDlg");
            fltdlg = new FltDlg(screen, camera, flt_def, this, gameObj);

            if (engdlg != null) engdlg.Hide();
            if (fltdlg != null) fltdlg.Hide();
            if (navdlg != null) navdlg.Hide();
            if (auddlg != null) auddlg.Hide();
            if (viddlg != null) viddlg.Hide();
            if (optdlg != null) optdlg.Hide();
            if (ctldlg != null) ctldlg.Hide();
            if (keydlg != null) keydlg.Hide();
            if (joydlg != null) joydlg.Hide();
            if (moddlg != null) moddlg.Hide();
            if (modInfoDlg != null) modInfoDlg.Hide();

            //TODO screen.SetBackgroundColor(Color.black);

            frame_rate = 0;

            //loader.UseFileSystem(Starshatter.UseFileSystem());
        }
        public virtual void TearDown()
        {
            if (gamewin != null && disp_view != null)
                gamewin.DelView(disp_view);

            if (screen != null)
            {
                screen.DelWindow(engdlg);
                screen.DelWindow(fltdlg);
                screen.DelWindow(navdlg);
                screen.DelWindow(modInfoDlg);
                screen.DelWindow(auddlg);
                screen.DelWindow(viddlg);
                screen.DelWindow(optdlg);
                screen.DelWindow(moddlg);
                screen.DelWindow(ctldlg);
                screen.DelWindow(keydlg);
                screen.DelWindow(joydlg);
                screen.DelWindow(gamewin);
            }

            //delete engdlg;
            //delete fltdlg;
            //delete navdlg;
            //delete modInfoDlg;
            //delete auddlg;
            //delete viddlg;
            //delete optdlg;
            //delete moddlg;
            //delete ctldlg;
            //delete keydlg;
            //delete joydlg;
            //delete gamewin;
            //delete cam_dir;

            engdlg = null;
            fltdlg = null;
            navdlg = null;
            modInfoDlg = null;
            auddlg = null;
            viddlg = null;
            optdlg = null;
            moddlg = null;
            ctldlg = null;
            keydlg = null;
            joydlg = null;
            gamewin = null;
            screen = null;
            cam_dir = null;
            cam_view = null;
            disp_view = null;
        }
        public virtual bool CloseTopmost()
        {
            bool processed = false;

            if (gamewin == null) return processed;

            if (navdlg != null && navdlg.IsShown())
            {
                HideNavDlg();
                processed = true;
            }

            else if (engdlg != null && engdlg.IsShown())
            {
                HideEngDlg();
                processed = true;
            }

            else if (fltdlg != null && fltdlg.IsShown())
            {
                HideFltDlg();
                processed = true;
            }

            else if (modInfoDlg != null && modInfoDlg.IsShown())
            {
                HideModInfoDlg();
                processed = true;
            }

            else if (keydlg != null && keydlg.IsShown())
            {
                ShowCtlDlg();
                processed = true;
            }

            else if (joydlg != null && joydlg.IsShown())
            {
                ShowCtlDlg();
                processed = true;
            }

            else if (auddlg != null && auddlg.IsShown())
            {
                CancelOptions();
                processed = true;
            }

            else if (viddlg != null && viddlg.IsShown())
            {
                CancelOptions();
                processed = true;
            }

            else if (optdlg != null && optdlg.IsShown())
            {
                CancelOptions();
                processed = true;
            }

            else if (moddlg != null && moddlg.IsShown())
            {
                CancelOptions();
                processed = true;
            }

            else if (ctldlg != null && ctldlg.IsShown())
            {
                CancelOptions();
                processed = true;
            }

            else if (quantum_view != null && quantum_view.IsMenuShown())
            {
                quantum_view.CloseMenu();
                processed = true;
            }

            else if (quit_view != null && quit_view.IsMenuShown())
            {
                quit_view.CloseMenu();
                processed = true;
            }

            else if (radio_view != null && radio_view.IsMenuShown())
            {
                radio_view.CloseMenu();
                processed = true;
            }

            return processed;
        }

        public virtual bool IsShown() { return isShown; }
        static Window old_disp_win = null;
        public virtual void Show()
        {
            if (!isShown)
            {
                screen.AddWindow(gamewin);
                isShown = true;

                if (disp_view != null)
                {
                    old_disp_win = disp_view.GetWindow();

                    disp_view.SetWindow(gamewin);
                    gamewin.AddView(disp_view);
                }
            }
        }
        public virtual void Hide()
        {
            if (isShown)
            {
                HideAll();

                if (disp_view != null && gamewin != null)
                {
                    gamewin.DelView(disp_view);
                    disp_view.SetWindow(old_disp_win);
                }

                if (engdlg != null) engdlg.SetShip(null);
                if (fltdlg != null) fltdlg.SetShip(null);
                if (navdlg != null) navdlg.SetShip(null);

                HUDSounds.StopSound(HUDSounds.SOUNDS.SND_RED_ALERT);

                screen.DelWindow(gamewin);
                isShown = false;
            }
        }

        public virtual bool IsFormShown()
        {
            bool form_shown = false;

            if (navdlg != null && navdlg.IsShown())
                form_shown = true;

            else if (engdlg != null && engdlg.IsShown())
                form_shown = true;

            else if (fltdlg != null && fltdlg.IsShown())
                form_shown = true;

            else if (auddlg != null && auddlg.IsShown())
                form_shown = true;

            else if (viddlg != null && viddlg.IsShown())
                form_shown = true;

            else if (optdlg != null && optdlg.IsShown())
                form_shown = true;

            else if (moddlg != null && moddlg.IsShown())
                form_shown = true;

            else if (ctldlg != null && ctldlg.IsShown())
                form_shown = true;

            else if (keydlg != null && keydlg.IsShown())
                form_shown = true;

            else if (joydlg != null && joydlg.IsShown())
                form_shown = true;

            return form_shown;
        }
        public virtual void ShowExternal()
        {
            if (gamewin == null) return;

            if ((navdlg != null && navdlg.IsShown()) ||
                    (engdlg != null && engdlg.IsShown()) ||
                    (fltdlg != null && fltdlg.IsShown()) ||
                    (auddlg != null && auddlg.IsShown()) ||
                    (viddlg != null && viddlg.IsShown()) ||
                    (optdlg != null && optdlg.IsShown()) ||
                    (moddlg != null && moddlg.IsShown()) ||
                    (ctldlg != null && ctldlg.IsShown()) ||
                    (keydlg != null && keydlg.IsShown()) ||
                    (joydlg != null && joydlg.IsShown()))
                return;

            gamewin.MoveTo(new Rect(0, 0, (int)screen.Width(), (int)screen.Height()));
            screen.AddWindow(gamewin);
        }

        public override void ShowNavDlg()
        {
            if (gamewin == null) return;

            if (navdlg != null && !navdlg.IsShown())
            {
                HideAll();

                navdlg.SetSystem(sim.GetStarSystem());
                navdlg.SetShip(sim.GetPlayerShip());
                navdlg.Show();

                if (mouse_con != null)
                {
                    mouse_active = mouse_con.Active();
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
            else
            {
                HideNavDlg();
            }
        }
        public override void HideNavDlg()
        {
            if (gamewin == null) return;

            if (navdlg != null && navdlg.IsShown())
            {
                navdlg.Hide();

                if (mouse_con != null)
                    mouse_con.SetActive(mouse_active);

                Mouse.Show(false);
                screen.AddWindow(gamewin);
            }
        }

        public override bool IsNavShown()
        {
            return gamewin != null && navdlg != null && navdlg.IsShown();
        }
        public override NavDlg GetNavDlg() { return navdlg; }

        public virtual void ShowEngDlg()
        {
            if (gamewin == null) return;

            if (engdlg != null && !engdlg.IsShown())
            {
                HideAll();

                engdlg.SetShip(sim.GetPlayerShip());
                engdlg.Show();

                if (mouse_con != null)
                {
                    mouse_active = mouse_con.Active();
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
            else
            {
                HideEngDlg();
            }
        }

        public virtual void HideEngDlg()
        {
            if (gamewin == null) return;

            if (engdlg != null && engdlg.IsShown())
            {
                engdlg.Hide();

                if (mouse_con != null)
                    mouse_con.SetActive(mouse_active);

                Mouse.Show(false);
                screen.AddWindow(gamewin);
            }
        }
        public virtual bool IsEngShown()
        {
            return gamewin != null && engdlg != null && engdlg.IsShown();
        }
        public virtual EngDlg GetEngDlg() { return engdlg; }

        public virtual void ShowFltDlg()
        {
            if (gamewin == null) return;

            if (fltdlg != null && !fltdlg.IsShown())
            {
                HideAll();

                fltdlg.SetShip(sim.GetPlayerShip());
                fltdlg.Show();

                if (mouse_con != null)
                {
                    mouse_active = mouse_con.Active();
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
            else
            {
                HideFltDlg();
            }
        }
        public virtual void HideFltDlg()
        {
            if (gamewin == null) return;

            if (fltdlg != null && fltdlg.IsShown())
            {
                fltdlg.Hide();

                if (mouse_con != null)
                    mouse_con.SetActive(mouse_active);

                Mouse.Show(false);
                screen.AddWindow(gamewin);
            }
        }
        public virtual bool IsFltShown()
        {
            return gamewin != null && fltdlg != null && fltdlg.IsShown();
        }
        public virtual FltDlg GetFltDlg() { return fltdlg; }

        public override void ShowCtlDlg()
        {
            if (ctldlg != null)
            {
                if (quit_view != null)
                {
                    quit_view.CloseMenu();
                    Starshatter.GetInstance().Pause(true);
                }

                HideAll();

                ctldlg.Show();
                ctldlg.SetTopMost(true);

                if (mouse_con != null)
                {
                    mouse_active = mouse_con.Active();
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
        }
        public virtual void HideCtlDlg()
        {
            if (ctldlg != null && ctldlg.IsShown())
            {
                ctldlg.Hide();
                Mouse.Show(false);
                screen.AddWindow(gamewin);

                if (quit_view != null)
                    quit_view.ShowMenu();
            }
        }
        public virtual bool IsCtlShown()
        {
            return ctldlg != null && ctldlg.IsShown();
        }

        public override void ShowJoyDlg()
        {
            if (joydlg != null)
            {
                if (quit_view != null)
                {
                    quit_view.CloseMenu();
                    Starshatter.GetInstance().Pause(true);
                }

                HideAll();

                if (ctldlg != null)
                {
                    ctldlg.Show();
                    ctldlg.SetTopMost(false);
                }

                if (joydlg != null) joydlg.Show();

                if (mouse_con != null)
                {
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
        }

        public virtual bool IsJoyShown()
        {
            return joydlg != null && joydlg.IsShown();
        }

        public override void ShowKeyDlg()
        {
            if (keydlg != null)
            {
                if (quit_view != null)
                {
                    quit_view.CloseMenu();
                    Starshatter.GetInstance().Pause(true);
                }

                HideAll();

                if (ctldlg != null)
                {
                    ctldlg.Show();
                    ctldlg.SetTopMost(false);
                }

                if (keydlg != null) keydlg.Show();

                if (mouse_con != null)
                {
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
        }
        public virtual bool IsKeyShown()
        {
            return keydlg != null && keydlg.IsShown();
        }


        public override AudDlg GetAudDlg() { return auddlg; }
        public override VidDlg GetVidDlg() { return viddlg; }
        public override OptDlg GetOptDlg() { return optdlg; }
        public override CtlDlg GetCtlDlg() { return ctldlg; }
        public override JoyDlg GetJoyDlg() { return joydlg; }
        public override KeyDlg GetKeyDlg() { return keydlg; }
        public override ModDlg GetModDlg() { return moddlg; }
        public override ModInfoDlg GetModInfoDlg() { return modInfoDlg; }

        public override void ShowAudDlg()
        {
            if (auddlg != null)
            {
                if (quit_view != null)
                {
                    quit_view.CloseMenu();
                    Starshatter.GetInstance().Pause(true);
                }

                HideAll();

                auddlg.Show();
                auddlg.SetTopMost(true);

                if (mouse_con != null)
                {
                    mouse_active = mouse_con.Active();
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
        }
        public virtual void HideAudDlg()
        {
            if (auddlg != null && auddlg.IsShown())
            {
                auddlg.Hide();
                Mouse.Show(false);
                screen.AddWindow(gamewin);

                if (quit_view != null)
                    quit_view.ShowMenu();
            }
        }
        public virtual bool IsAudShown()
        {
            return auddlg != null && auddlg.IsShown();
        }

        public override void ShowVidDlg()
        {
            if (viddlg != null)
            {
                if (quit_view != null)
                {
                    quit_view.CloseMenu();
                    Starshatter.GetInstance().Pause(true);
                }

                HideAll();

                viddlg.Show();
                viddlg.SetTopMost(true);

                if (mouse_con != null)
                {
                    mouse_active = mouse_con.Active();
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
        }
        public virtual void HideVidDlg()
        {
            if (viddlg != null && viddlg.IsShown())
            {
                viddlg.Hide();
                Mouse.Show(false);
                screen.AddWindow(gamewin);

                if (quit_view != null)
                    quit_view.ShowMenu();
            }
        }
        public virtual bool IsVidShown()
        {
            return viddlg != null && viddlg.IsShown();
        }


        public override void ShowOptDlg()
        {
            if (optdlg != null)
            {
                if (quit_view != null)
                {
                    quit_view.CloseMenu();
                    Starshatter.GetInstance().Pause(true);
                }

                HideAll();

                optdlg.Show();
                optdlg.SetTopMost(true);

                if (mouse_con != null)
                {
                    mouse_active = mouse_con.Active();
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
        }
        public virtual void HideOptDlg()
        {
            if (optdlg != null && optdlg.IsShown())
            {
                optdlg.Hide();
                Mouse.Show(false);
                screen.AddWindow(gamewin);

                if (quit_view != null)
                    quit_view.ShowMenu();
            }
        }
        public virtual bool IsOptShown()
        {
            return optdlg != null && optdlg.IsShown();
        }

        public override void ShowModDlg()
        {
            if (moddlg != null)
            {
                if (quit_view != null)
                {
                    quit_view.CloseMenu();
                    Starshatter.GetInstance().Pause(true);
                }

                HideAll();

                moddlg.Show();
                moddlg.SetTopMost(true);

                if (mouse_con != null)
                {
                    mouse_active = mouse_con.Active();
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
        }
        public virtual void HideModDlg()
        {
            if (moddlg != null && moddlg.IsShown())
            {
                moddlg.Hide();
                Mouse.Show(false);
                screen.AddWindow(gamewin);

                if (quit_view != null)
                    quit_view.ShowMenu();
            }
        }
        public virtual bool IsModShown()
        {
            return moddlg != null && moddlg.IsShown();
        }

        public override void ShowModInfoDlg()
        {
            if (moddlg != null && modInfoDlg != null)
            {
                if (quit_view != null)
                {
                    quit_view.CloseMenu();
                    Starshatter.GetInstance().Pause(true);
                }

                HideAll();

                moddlg.Show();
                moddlg.SetTopMost(false);

                modInfoDlg.Show();
                modInfoDlg.SetTopMost(true);

                if (mouse_con != null)
                {
                    mouse_active = mouse_con.Active();
                    mouse_con.SetActive(false);
                }

                Mouse.Show(true);
            }
        }
        public virtual void HideModInfoDlg()
        {
            ShowModDlg();
        }

        public virtual bool IsModInfoShown()
        {
            return modInfoDlg != null && modInfoDlg.IsShown();
        }

        public override void ApplyOptions()
        {
            if (ctldlg != null) ctldlg.Apply();
            if (optdlg != null) optdlg.Apply();
            if (auddlg != null) auddlg.Apply();
            if (viddlg != null) viddlg.Apply();

            if (engdlg != null) engdlg.Hide();
            if (fltdlg != null) fltdlg.Hide();
            if (navdlg != null) navdlg.Hide();
            if (ctldlg != null) ctldlg.Hide();
            if (auddlg != null) auddlg.Hide();
            if (viddlg != null) viddlg.Hide();
            if (optdlg != null) optdlg.Hide();
            if (moddlg != null) moddlg.Hide();
            if (keydlg != null) keydlg.Hide();
            if (joydlg != null) joydlg.Hide();

            Mouse.Show(false);
            screen.AddWindow(gamewin);
            Starshatter.GetInstance().Pause(false);
        }
        public override void CancelOptions()
        {
            if (ctldlg != null) ctldlg.Cancel();
            if (optdlg != null) optdlg.Cancel();
            if (auddlg != null) auddlg.Cancel();
            if (viddlg != null) viddlg.Cancel();

            if (engdlg != null) engdlg.Hide();
            if (fltdlg != null) fltdlg.Hide();
            if (navdlg != null) navdlg.Hide();
            if (ctldlg != null) ctldlg.Hide();
            if (auddlg != null) auddlg.Hide();
            if (viddlg != null) viddlg.Hide();
            if (optdlg != null) optdlg.Hide();
            if (moddlg != null) moddlg.Hide();
            if (keydlg != null) keydlg.Hide();
            if (joydlg != null) joydlg.Hide();

            Mouse.Show(false);
            screen.AddWindow(gamewin);
            Starshatter.GetInstance().Pause(false);
        }

        public virtual void ShowWeaponsOverlay()
        {
            if (wep_view != null)
                wep_view.CycleOverlayMode();
        }
        public virtual void HideWeaponsOverlay()
        {
            if (wep_view != null)
                wep_view.SetOverlayMode(0);
        }

        public void SetFieldOfView(double fov)
        {
            cam_view.SetFieldOfView((float)fov);
        }
        public double GetFieldOfView()
        {
            return cam_view.GetFieldOfView();
        }
        public void CycleMFDMode(MFD.Modes mfd)
        {
            if (hud_view != null)
                hud_view.CycleMFDMode(mfd);
        }

        public void CycleHUDMode()
        {
            if (hud_view != null)
                hud_view.CycleHUDMode();
        }
        public void CycleHUDColor()
        {
            if (hud_view != null)
                hud_view.CycleHUDColor();
        }
        public void CycleHUDWarn()
        {
            if (hud_view != null)
                hud_view.CycleHUDWarn();
        }
        public void FrameRate(double f)
        {
            frame_rate = f;
        }
        public void ExecFrame()
        {
            sim = Sim.GetSim();

            if (sim != null)
            {
                cam_view.UseCamera(CameraDirector.GetInstance().GetCamera().Camera);
                cam_view.UseScene(sim.GetScene());

                Ship player = sim.GetPlayerShip();

                if (player != null)
                {
                    bool dialog_showing = false;

                    if (hud_view != null)
                    {
                        hud_view.UseCameraView(cam_view);
                        hud_view.ExecFrame();
                    }

                    if (quit_view != null && quit_view.IsMenuShown())
                    {
                        quit_view.ExecFrame();
                        dialog_showing = true;
                    }

                    if (navdlg != null && navdlg.IsShown())
                    {
                        navdlg.SetShip(player);
                        navdlg.ExecFrame();
                        dialog_showing = true;
                    }

                    if (engdlg != null && engdlg.IsShown())
                    {
                        engdlg.SetShip(player);
                        engdlg.ExecFrame();
                        dialog_showing = true;
                    }

                    if (fltdlg != null && fltdlg.IsShown())
                    {
                        fltdlg.SetShip(player);
                        fltdlg.ExecFrame();
                        dialog_showing = true;
                    }

                    if (auddlg != null && auddlg.IsShown())
                    {
                        auddlg.ExecFrame();
                        dialog_showing = true;
                    }

                    if (viddlg != null && viddlg.IsShown())
                    {
                        viddlg.ExecFrame();
                        dialog_showing = true;
                    }

                    if (optdlg != null && optdlg.IsShown())
                    {
                        optdlg.ExecFrame();
                        dialog_showing = true;
                    }

                    if (ctldlg != null && ctldlg.IsShown())
                    {
                        ctldlg.ExecFrame();
                        dialog_showing = true;
                    }

                    if (keydlg != null && keydlg.IsShown())
                    {
                        keydlg.ExecFrame();
                        dialog_showing = true;
                    }

                    if (joydlg != null && joydlg.IsShown())
                    {
                        joydlg.ExecFrame();
                        dialog_showing = true;
                    }

                    if (moddlg != null && moddlg.IsShown())
                    {
                        moddlg.ExecFrame();
                        dialog_showing = true;
                    }

                    if (quantum_view != null && !dialog_showing)
                    {
                        quantum_view.ExecFrame();
                    }

                    if (radio_view != null && !dialog_showing)
                    {
                        radio_view.ExecFrame();
                    }

                    if (wep_view != null && !dialog_showing)
                    {
                        wep_view.ExecFrame();
                    }

                    if (tac_view != null && !dialog_showing)
                    {
                        if (cam_view != null)
                            tac_view.UseProjector(cam_view.GetProjector());
                        tac_view.ExecFrame();
                    }
                }

                if (disp_view != null)
                {
                    disp_view.ExecFrame();
                }
            }

            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                if (stars.LensFlare() != 0)
                {
                    cam_view.LensFlareElements(flare1, flare4, flare2, flare3);
                    cam_view.LensFlare(true);
                }

                else if (stars.Corona() != 0)
                {
                    cam_view.LensFlareElements(flare1, null, null, null);
                    cam_view.LensFlare(true);
                }

                else
                {
                    cam_view.LensFlare(false);
                }
            }
        }

        public static GameScreen GetInstance() { return game_screen; }
        public CameraView GetCameraView() { return cam_view; }
        public Bitmap GetLensFlare(int index)
        {
            switch (index)
            {
                case 0: return flare1;
                case 1: return flare2;
                case 2: return flare3;
                case 3: return flare4;
            }

            return null;
        }

        protected void HideAll()
        {
            screen.DelWindow(gamewin);

            if (engdlg != null) engdlg.Hide();
            if (fltdlg != null) fltdlg.Hide();
            if (navdlg != null) navdlg.Hide();
            if (auddlg != null) auddlg.Hide();
            if (viddlg != null) viddlg.Hide();
            if (optdlg != null) optdlg.Hide();
            if (moddlg != null) moddlg.Hide();
            if (modInfoDlg != null) modInfoDlg.Hide();
            if (ctldlg != null) ctldlg.Hide();
            if (keydlg != null) keydlg.Hide();
            if (joydlg != null) joydlg.Hide();
        }

        protected Sim sim;
        protected Screen screen;

        protected Window gamewin;
        protected NavDlg navdlg;
        protected EngDlg engdlg;
        protected FltDlg fltdlg;
        protected CtlDlg ctldlg;
        protected KeyDlg keydlg;
        protected JoyDlg joydlg;
        protected AudDlg auddlg;
        protected VidDlg viddlg;
        protected OptDlg optdlg;
        protected ModDlg moddlg;
        protected ModInfoDlg modInfoDlg;

        protected FontItem HUDfont;
        protected FontItem GUIfont;
        protected FontItem GUI_small_font;
        protected FontItem title_font;

        protected Bitmap flare1;
        protected Bitmap flare2;
        protected Bitmap flare3;
        protected Bitmap flare4;

        protected CameraDirector cam_dir;
        protected DisplayView disp_view;
        protected HUDView hud_view;
        protected WepView wep_view;
        protected QuantumView quantum_view;
        protected QuitView quit_view;
        protected TacticalView tac_view;
        protected RadioView radio_view;
        protected CameraView cam_view;
        //DataLoader loader;

        protected double frame_rate;
        protected bool isShown;

        protected static GameScreen game_screen;
        static MouseController mouse_con = null;
        static bool mouse_active = false;
    }
}