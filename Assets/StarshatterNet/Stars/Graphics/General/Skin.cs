﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      Skin.h/Skin.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Classes for managing run-time selectable skins on solid objects
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Stars.Graphics.General
{
    public class Skin
    {
        public Skin(string name = null)
        {
            this.name = name;
            this.path = null;
        }

        //~Skin();

        // operations
        public void ApplyTo(Model model)
        {
            if (model != null)
            {
                foreach (SkinCell s in cells)
                {
                    if (s.Skin != null)
                    {
                        s.Orig = model.ReplaceMaterial(s.Skin);
                    }
                }
            }
        }

        public void Restore(Model model)
        {
            if (model != null)
            {
                foreach (SkinCell s in cells)
                {
                    if (s.Orig != null)
                    {
                        model.ReplaceMaterial(s.Orig);
                        s.Orig = null;
                    }
                }
            }
        }

        // accessors / mutators
        public string Name() { return name; }
        public string Path() { return path; }
        public int NumCells() { return cells.Count; }

        public void SetName(string n)
        {
            if (!string.IsNullOrWhiteSpace(n))
            {
                name = n;
            }
        }

        public void SetPath(string n)
        {
            if (!string.IsNullOrWhiteSpace(n))
            {
                path = n;
            }
            else
            {
                path = "";
            }
        }

        public void AddMaterial(Material mtl)
        {
            if (mtl == null) return;

            SkinCell s = cells.Find(c => c.Skin.name == mtl.name);
            if (s != null)
            {
                s.Skin = mtl;
            }
            else
            {
                SkinCell newskin = new SkinCell(mtl);
                cells.Add(newskin);
            }
        }


        protected string name;
        protected string path;
        protected List<SkinCell> cells;
    }

    public class SkinCell
    {
        public SkinCell(Material mtl = null)
        {
            skin = mtl;
            orig = null;
        }
        //~SkinCell() { }

        //public int operator ==(  SkinCell  other)  ;

        public string Name()
        {
            if (skin != null)
                return skin.name;

            return "Invalid Skin Cell";
        }

        public Material Skin
        {
            get { return skin; }
            set { skin = value; }
        }
        public Material Orig
        {
            get { return orig; }
            set { orig = value; }
        }

        public void SetSkin(Material mtl)
        {
            Skin = mtl;
        }

        public void SetOrig(Material mtl)
        {
            Orig = mtl;
        }

        private Material skin;
        private Material orig;
    }
}
