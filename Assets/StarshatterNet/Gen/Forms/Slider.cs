﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      Slider.h/Slider.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Slider/Gauge ActiveWindow class
*/
using System;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.Int32;

namespace StarshatterNet.Gen.Forms
{
    public class Slider : ActiveWindow
    {
        public const int MAX_VAL = 8;
        public const int ORIENT_HORIZONTAL = 0;
        public const int ORIENT_VERTICAL = 1;

        public Slider(ActiveWindow p, int ax, int ay, int aw, int ah, DWORD aid) :
            base(p.GetScreen(), ax, ay, aw, ah, aid, 0, p)
        {
            captured = false;
            dragging = false;

            border_color = Color.black;
            fill_color = new Color32(0, 0, 128, 255);// DarkBlue;

            active = true;
            border = true;
            num_leds = 1;
            orientation = 0;

            range_min = 0;
            range_max = 100;
            step_size = 10;
            show_thumb = false;
            thumb_size = -1;

            nvalues = 1;
            //ZeroMemory(value, sizeof(value));
            value.Initialize();

            marker[0] = -1;
            marker[1] = -1;

            desc = "Slider " + id;
        }
        //public Slider(Screen* s, int ax, int ay, int aw, int ah, DWORD aid);
        //public virtual ~Slider();

        // Operations:
        public virtual void Draw() { throw new NotImplementedException(); }


        // Event Target Interface:
        public override int OnMouseMove(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonDown(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonUp(int x, int y) { throw new NotImplementedException(); }
        public override int OnClick() { throw new NotImplementedException(); }

        // Property accessors:
        public bool GetActive()
        {
            return active;
        }
        public void SetActive(bool bNewValue)
        {
            active = bNewValue;
            SetActive();
        }

        protected void SetActive()
        {
            if (handleSlideArea != null)
                handleSlideArea.SetActive(active);
        }
        public Color GetActiveColor()
        {
            return active_color;
        }

        public void SetActiveColor(Color c)
        {
            active_color = c;
            SetActiveColor();
        }
        public void SetActiveColor()
        {
            if (bck != null)
            {
                var img = bck.GetComponent<Image>();
                img.color = fill_color;
            }
        }

        public Color GetFillColor()
        {
            return fill_color;
        }

        public void SetFillColor(Color c)
        {
            fill_color = c;
            SetFillColor();
        }
        public void SetFillColor()
        {
            if (bck != null)
            {
                var img = fillArea.GetComponent<Image>();
                img.color = fill_color;
            }

        }
        public override void SetBackColor(Color c)
        {
            back_color = c;
            SetBackColor();
        }

        public void SetBackColor()
        {
            if (bck != null)
            {
                var img = bck.GetComponent<Image>();
                img.color = back_color;
            }
        }

        public bool GetBorder()
        {
            return border;
        }
        public void SetBorder(bool bNewValue)
        {
            border = bNewValue;
        }
        public Color GetBorderColor()
        {
            return border_color;
        }
        public void SetBorderColor(Color c)
        {
            border_color = c;
        }

        public int GetNumLeds()
        {
            return num_leds;
        }
        public void SetNumLeds(int nNewValue)
        {
            if (nNewValue >= 0)
            {
                num_leds = nNewValue;
            }
        }
        public int GetOrientation()
        {
            return orientation;
        }
        public void SetOrientation(int nNewValue)
        {
            if (nNewValue == 0 || nNewValue == 1)
            {
                orientation = nNewValue;
            }
        }

        public int GetRangeMin()
        {
            return range_min;
        }
        public void SetRangeMin(int nNewValue)
        {
            range_min = nNewValue;
            SetRanges();
        }
        public int GetRangeMax()
        {
            return range_max;
        }
        public void SetRangeMax(int nNewValue)
        {
            range_max = nNewValue;
            SetRanges();
        }
        public void SetRanges()
        {
            if (slider == null) return;
            slider.minValue = range_min;
            slider.maxValue = range_max;
        }
        public int GetStepSize()
        {
            return step_size;
        }
        public void SetStepSize(int nNewValue)
        {
            if (nNewValue < range_max - range_min)
                step_size = nNewValue;
        }

        public int GetThumbSize()
        {
            return thumb_size;
        }
        public void SetThumbSize(int nNewValue)
        {
            if (nNewValue < range_max - range_min)
                thumb_size = nNewValue;
        }
        public bool GetShowThumb()
        {
            return show_thumb;
        }
        public void SetShowThumb(bool bNewValue)
        {
            show_thumb = bNewValue;
        }

        public int NumValues()
        {
            return nvalues;
        }

        public int GetValue(int index = 0)
        {
            if (index >= 0 && index < nvalues)
                return value[index];

            return 0;
        }
        public void SetValue(int nNewValue, int index = 0)
        {
            if (index >= 0 && index < MAX_VAL)
            {
                value[index] = nNewValue;

                if (index >= nvalues)
                    nvalues = index + 1;

                SetValue();
            }
        }
        public void SetValue()
        {
            if (slider == null) return;
            slider.value = value[0];
        }
        public double FractionalValue(int index = 0)
        {
            if (index >= 0 && index < nvalues)
                return ((double)(value[index] - range_min)) / ((double)(range_max - range_min));

            return 0;
        }


        public void SetMarker(int nNewValue, int index = 0)
        {
            if (index >= 0 && index < 2)
            {
                marker[index] = nNewValue;
            }
        }

        // Methods:
        public void StepUp(int index = 0)
        {
            if (index >= 0 && index < nvalues)
            {
                value[index] += step_size;

                if (value[index] > range_max)
                    value[index] = range_max;
            }
        }
        public void StepDown(int index = 0)
        {
            if (index >= 0 && index < nvalues)
            {
                value[index] -= step_size;

                if (value[index] < range_min)
                    value[index] = range_min;
            }
        }
        public override void MoveTo(Stars.Rect r)
        {
            Stars.Rect ctrl_rect = new Stars.Rect(r.x, r.y, r.w, r.h);
            ctrl_rect.Inflate(0, 10);

            base.MoveTo(ctrl_rect);
        }

        protected internal override void BuildGamebjects()
        {
            base.BuildGamebjects();

            DefaultControls.Resources uiResources = new DefaultControls.Resources();
            obj = DefaultControls.CreateSlider(uiResources);
            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            rectTransform.SetParent(this.parentObj.transform);
            obj.name = desc;
            obj.SetActive(true);
            slider = obj.GetComponent<UnityEngine.UI.Slider>();

            bck = obj.transform.Find("Background").gameObject;
            fillArea = obj.transform.Find("Fill Area/Fill").gameObject;
            handleSlideArea = obj.transform.Find("Handle Slide Area").gameObject;
            //handleSlideArea.SetActive(active);

            SetActive();
            SetActiveColor();
            SetFillColor();
            SetBackColor();
            SetRanges();
            SetValue();
        }


        protected bool captured;
        protected bool dragging;
        protected int mouse_x;
        protected int mouse_y;

        protected bool active;        // true => slider; false => gauge
        protected bool border;
        protected Color border_color;
        protected Color fill_color;    // default: dark blue
        protected Color active_color;

        protected int num_leds;      // default: 1
        protected int orientation;   // 0 => horizontal; !0 => vertical

        protected int range_min;
        protected int range_max;
        protected int step_size;
        protected bool show_thumb;
        protected int thumb_size;
        protected int thumb_pos;

        protected int nvalues;
        protected int[] value = new int[MAX_VAL];
        protected int[] marker = new int[2];

        protected GameObject bck;
        protected GameObject fillArea;
        protected GameObject handleSlideArea;
        protected UnityEngine.UI.Slider slider;
    }
}
