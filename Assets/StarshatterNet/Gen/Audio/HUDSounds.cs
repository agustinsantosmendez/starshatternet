﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      HUDSounds.h/HUDSounds.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    HUDSounds singleton class utility
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Stars;

namespace StarshatterNet.Audio
{
    public class HUDSounds
    {
        public enum SOUNDS
        {
            SND_MFD_MODE,
            SND_NAV_MODE,
            SND_WEP_MODE,
            SND_WEP_DISP,
            SND_HUD_MODE,
            SND_HUD_WIDGET,
            SND_SHIELD_LEVEL,
            SND_RED_ALERT,
            SND_TAC_ACCEPT,
            SND_TAC_REJECT
        };

        public static void Initialize()
        {
            // loader.SetDataPath("Sounds/");

            LoadInterfaceSound("mfd_mode.wav", out mfd_mode);
            LoadInterfaceSound("nav_mode.wav", out nav_mode);
            LoadInterfaceSound("wep_mode.wav", out wep_mode);
            LoadInterfaceSound("wep_disp.wav", out wep_disp);
            LoadInterfaceSound("hud_mode.wav", out hud_mode);
            LoadInterfaceSound("hud_widget.wav", out hud_widget);
            LoadInterfaceSound("shield_level.wav", out shield_level);
            LoadInterfaceSound("alarm.wav", out red_alert);
            LoadInterfaceSound("tac_accept.wav", out tac_accept);
            LoadInterfaceSound("tac_reject.wav", out tac_reject);

            if (red_alert != null)
                red_alert.SetFlags(Sound.FlagEnum.AMBIENT | Sound.FlagEnum.LOOP | Sound.FlagEnum.LOCKED);

        }
        public static void Close()
        {
            mfd_mode = null;
            nav_mode = null;
            wep_mode = null;
            wep_disp = null;
            hud_mode = null;
            hud_widget = null;
            shield_level = null;
            red_alert = null;
            tac_accept = null;
            tac_reject = null;
        }


        public static void PlaySound(SOUNDS n)
        {
            Sound sound = null;

            switch (n)
            {
                default:
                case SOUNDS.SND_MFD_MODE: if (mfd_mode != null) sound = mfd_mode.Duplicate(); break;
                case SOUNDS.SND_NAV_MODE: if (nav_mode != null) sound = nav_mode.Duplicate(); break;
                case SOUNDS.SND_WEP_MODE: if (wep_mode != null) sound = wep_mode.Duplicate(); break;
                case SOUNDS.SND_WEP_DISP: if (wep_disp != null) sound = wep_disp.Duplicate(); break;
                case SOUNDS.SND_HUD_MODE: if (hud_mode != null) sound = hud_mode.Duplicate(); break;
                case SOUNDS.SND_HUD_WIDGET: if (hud_widget != null) sound = hud_widget.Duplicate(); break;
                case SOUNDS.SND_SHIELD_LEVEL: if (shield_level != null) sound = shield_level.Duplicate(); break;
                case SOUNDS.SND_TAC_ACCEPT: if (tac_accept != null) sound = tac_accept.Duplicate(); break;
                case SOUNDS.SND_TAC_REJECT: if (tac_reject != null) sound = tac_reject.Duplicate(); break;

                // RED ALERT IS A SPECIAL CASE!
                case SOUNDS.SND_RED_ALERT:
                    if (red_alert != null)
                    {
                        sound = red_alert;
                    }
                    break;
            }

            if (sound != null && !sound.IsPlaying())
            {
                int gui_volume = AudioConfig.GuiVolume();
                sound.SetVolume(gui_volume);
                sound.Play();
            }
        }
        public static void StopSound(SOUNDS n)
        {
            if (n == SOUNDS.SND_RED_ALERT && red_alert != null)
            {
                red_alert.Stop();
            }
        }

        protected static void LoadInterfaceSound(string wave, out Sound s)
        {
            s = Sound.CreateStream("Sounds/" + wave);
        }


        protected static Sound mfd_mode = null;
        protected static Sound nav_mode = null;
        protected static Sound wep_mode = null;
        protected static Sound wep_disp = null;
        protected static Sound hud_mode = null;
        protected static Sound hud_widget = null;
        protected static Sound shield_level = null;
        protected static Sound red_alert = null;
        protected static Sound tac_accept = null;
        protected static Sound tac_reject = null;
    }
}
