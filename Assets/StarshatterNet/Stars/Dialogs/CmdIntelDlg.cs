﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmdIntelDlg.h/CmdIntelDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Operational Command Dialog (Intel/Newsfeed Tab)
*/
using System.Collections.Generic;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmdIntelDlg : CmdDlg
    {
        public CmdIntelDlg(Screen s, Camera c, FormDef def, CmpnScreen mgr, GameObject p) :
              base(s, mgr, 0, 0, s.Width(), s.Height(), c, p, "CmdIntelDlg")
        {
            update_time = 0; start_scene = 0;
            cam_view = null; dsp_view = null;

            if (campaign != null)
                update_time = campaign.GetUpdateTime();

            Init(def);
        }
        //public virtual ~CmdIntelDlg();
        public override void RegisterControls()
        {
            lst_news = (ListBox)FindControl(401);
            txt_news = (RichTextBox)FindControl(402);
            img_news = (ImageBox)FindControl(403);
            mov_news = FindControl(404);
            btn_play = (Button)FindControl(405);

            RegisterCmdControls(this);

            if (btn_save != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_save, OnSave);

            if (btn_exit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_exit, OnExit);

            if (btn_play != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_play, OnPlay);

            for (int i = 0; i < 5; i++)
            {
                if (btn_mode[i] != null)
                    REGISTER_CLIENT(ETD.EID_CLICK, btn_mode[i], OnMode);
            }

            if (lst_news != null)
            {
                REGISTER_CLIENT(ETD.EID_SELECT, lst_news, OnNews);
            }

            if (img_news != null)
            {
#if TODO
                img_news.GetPicture(  bmp_default.Texture);
#endif
            }

            if (mov_news != null)
            {
#if TODO
                CameraDirector cam_dir = CameraDirector.GetInstance();
                cam_view = new CameraView(mov_news, cam_dir.GetCamera(), 0);
                if (cam_view != null)
                    mov_news.AddView(cam_view);

                dsp_view = DisplayView.GetInstance();
                if (dsp_view != null)
                {
                    dsp_view.SetWindow(mov_news);
                    mov_news.AddView(dsp_view);
                }
#endif
                ErrLogger.PrintLine("WARNING: CmdIntelDlg.RegisterControls is not yet implemented.");

                mov_news.Hide();
            }
        }
        public override void Show()
        {
            mode = MODE.MODE_INTEL;

            base.Show();
            ShowCmdDlg();

            if (btn_play != null)
                btn_play.Hide();

            if (mov_news != null)
                mov_news.Hide();
        }
        public override void ExecFrame()
        {
            base.ExecFrame();

            if (campaign != Campaign.GetCampaign() || campaign.GetUpdateTime() != update_time)
            {
                campaign = Campaign.GetCampaign();
                update_time = campaign.GetUpdateTime();

                lst_news.ClearItems();
                txt_news.SetText("");

                if (img_news != null)
                    img_news.SetPicture(bmp_default.Texture);
            }

            if (campaign != null)
            {
                List<CombatEvent> events = campaign.GetEvents();
                bool auto_scroll = false;

                if (events.Count > lst_news.NumItems())
                {
                    while (events.Count > lst_news.NumItems())
                    {
                        CombatEvent info = events[lst_news.NumItems()];

                        string unread = info.Visited() ? " " : "*";
                        int i = lst_news.AddItemWithData(unread, info) - 1;

                        string dateline;
                        FormatUtil.FormatDayTime(out dateline, info.Time());
                        lst_news.SetItemText(i, 1, dateline);
                        lst_news.SetItemText(i, 2, info.Title());
                        lst_news.SetItemText(i, 3, info.Region());
                        lst_news.SetItemText(i, 4, Game.GetText(info.SourceName()));

                        if (!info.Visited())
                            auto_scroll = true;
                    }

                    if (lst_news.GetSortColumn() > 0)
                        lst_news.SortItems();
                }

                else if (events.Count < lst_news.NumItems())
                {
                    lst_news.ClearItems();

                    for (int i = 0; i < events.Count; i++)
                    {
                        CombatEvent info = events[i];

                        string unread = info.Visited() ? " " : "*";
                        int j = lst_news.AddItemWithData(unread, info) - 1;

                        string dateline;
                        FormatUtil.FormatDayTime(out dateline, info.Time());
                        lst_news.SetItemText(j, 1, dateline);
                        lst_news.SetItemText(j, 2, info.Title());
                        lst_news.SetItemText(j, 3, info.Region());
                        lst_news.SetItemText(j, 4, Game.GetText(info.SourceName()));

                        if (!info.Visited())
                            auto_scroll = true;
                    }

                    if (lst_news.GetSortColumn() > 0)
                        lst_news.SortItems();

                    txt_news.SetText("");

                    if (img_news != null)
                        img_news.SetPicture(bmp_default.Texture);
                }

                if (auto_scroll)
                {
                    int first_unread = -1;

                    for (int i = 0; i < lst_news.NumItems(); i++)
                    {
                        if (lst_news.GetItemText(i, 0) == "*")
                        {
                            first_unread = i;
                            break;
                        }
                    }

                    if (first_unread >= 0)
                        lst_news.ScrollTo(first_unread);
                }
            }

            Starshatter stars = Starshatter.GetInstance();

            if (start_scene > 0)
            {
#if TODO
                ShowMovie();

                start_scene--;

                if (start_scene == 0)
                {
                    if (stars != null && campaign != null)
                    {
                        stars.ExecCutscene(event_scene, campaign.Path());

                        if (stars.InCutscene())
                        {
                            Sim sim = Sim.GetSim();

                            if (sim != null)
                            {
                                cam_view.UseCamera(CameraDirector.GetInstance().GetCamera());
                                cam_view.UseScene(sim.GetScene());
                            }
                        }
                    }

                    event_scene = "";
                }
#endif
                ErrLogger.PrintLine("WARNING: CmdIntelDlg.ExecFrame is not yet implemented.");
            }
            else
            {
#if TODO
                if (dsp_view != null)
                    dsp_view.ExecFrame();
#endif
                ErrLogger.PrintLine("WARNING: CmdIntelDlg.ExecFrame is not yet implemented.");

                if (stars.InCutscene())
                    ShowMovie();
                else
                    HideMovie();
            }
        }

        // Operations:
        public override void OnMode(ActiveWindow obj, AWEvent evnt)
        {
            base.OnMode(obj, evnt);
        }
        public override void OnSave(ActiveWindow obj, AWEvent evnt)
        {
            base.OnSave(obj, evnt);
        }
        public override void OnExit(ActiveWindow obj, AWEvent evnt)
        {
            base.OnExit(obj, evnt);
        }

        public virtual void OnNews(ActiveWindow obj, AWEvent evnt)
        {
            CombatEvent cevent = null;
            int index = lst_news.GetSelection();

            if (index >= 0)
            {
                cevent = (CombatEvent)lst_news.GetItemData(index, 0);
            }

            if (cevent != null)
            {
                FontItem Limerick = FontMgr.Find("Limerick12");
                int limerickSize = 14;
                if (Limerick != null)
                    limerickSize = Limerick.size;

                FontItem Verdana = FontMgr.Find("Verdana");
                int verdanaSize = 12;
                if (Limerick != null)
                    verdanaSize = Verdana.size;

                string info = "<size=" + limerickSize + "><color=#ffff80>";
                info += cevent.Title();
                info += "</color></size><size=" + verdanaSize + "><color=#ffffff>\n\n";
                info += cevent.Information();
                info += "</color></size>";

                txt_news.SetText(info);
                txt_news.EnsureVisible(0);

                lst_news.SetItemText(index, 0, " ");

                if (img_news != null)
                {
#if TODO
                    if (cevent.Image().Width() >= 64)
                        img_news.SetPicture(cevent.Image().Texture);
                    else
                        img_news.SetPicture(bmp_default.Texture);
#endif
                    ErrLogger.PrintLine("WARNING: CmdIntelDlg.ShowMovie is not yet implemented.");

                }

                if (btn_play != null)
                {
                    if (!string.IsNullOrEmpty(cevent.SceneFile()))
                        btn_play.Show();
                    else
                        btn_play.Hide();
                }

                if (!cevent.Visited() && btn_play.IsEnabled())
                    OnPlay(null, null);

                cevent.SetVisited(true);
            }
            else
            {
                txt_news.SetText("");

                if (img_news != null)
                    img_news.SetPicture(bmp_default.Texture);

                if (btn_play != null)
                    btn_play.Hide();
            }
        }
        public virtual void OnPlay(ActiveWindow obj, AWEvent evnt)
        {
            CombatEvent cevent = null;
            int index = lst_news.GetSelection();

            if (index >= 0)
            {
                cevent = (CombatEvent)lst_news.GetItemData(index, 0);
            }

            if (mov_news != null && cam_view != null && cevent != null && !string.IsNullOrEmpty(cevent.SceneFile()))
            {
                event_scene = cevent.SceneFile();
                start_scene = 2;
                ShowMovie();
            }
        }



        protected virtual void ShowMovie()
        {
            if (mov_news != null)
            {
                mov_news.Show();
#if TODO
                dsp_view.SetWindow(mov_news);
#endif
                ErrLogger.PrintLine("WARNING: CmdIntelDlg.ShowMovie is not yet implemented.");

                if (img_news != null) img_news.Hide();
                if (txt_news != null) txt_news.Hide();
                if (btn_play != null) btn_play.Hide();
            }
        }
        protected virtual void HideMovie()
        {
            CombatEvent cevent = null;
            int index = lst_news.GetSelection();
            bool play = false;

            if (index >= 0)
            {
                cevent = (CombatEvent)lst_news.GetItemData(index, 0);

                if (cevent != null && !string.IsNullOrEmpty(cevent.SceneFile()))
                    play = true;
            }

            if (mov_news != null)
            {
                mov_news.Hide();

                if (img_news != null) img_news.Show();
                if (txt_news != null) txt_news.Show();
                if (btn_play != null)
                {
                    if (play)
                        btn_play.Show();
                    else
                        btn_play.Hide();
                }
            }
        }

        protected ListBox lst_news;
        protected RichTextBox txt_news;
        protected ImageBox img_news;
        protected Button btn_play;
        protected ActiveWindow mov_news;
        protected CameraView cam_view;
        protected DisplayView dsp_view;

        protected Bitmap bmp_default;

        protected double update_time;
        protected int start_scene;
        protected string event_scene;

    }
}