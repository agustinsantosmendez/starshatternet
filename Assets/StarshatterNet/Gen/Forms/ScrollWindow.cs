﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      ScrollWindow.h/ScrollWindow.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    ScrollWindow base class for List, Edit, and Rich Text controls
*/
using System;
using StarshatterNet.Gen.Misc;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.Int32;

namespace StarshatterNet.Gen.Forms
{
    public class ScrollWindow : ActiveWindow
    {
        public enum POLICY
        {
            SCROLL_NEVER,
            SCROLL_AUTO,
            SCROLL_ALWAYS
        }

        public enum SCROLL
        {
            SCROLL_NONE,
            SCROLL_UP,
            SCROLL_PAGE_UP,
            SCROLL_DOWN,
            SCROLL_PAGE_DOWN,
            SCROLL_THUMB
        }

        public enum MISC
        {
            BORDER_WIDTH = 2,
            BORDER_HEIGHT = 10,
            EXTRA_WIDTH = 4,
            SCROLL_WIDTH = 16,
            SCROLL_HEIGHT = 6,
            SCROLL_TRACK = SCROLL_WIDTH + 1,
            TRACK_START = BORDER_WIDTH + SCROLL_HEIGHT,
            THUMB_HEIGHT = SCROLL_WIDTH,
            HEADING_EXTRA = BORDER_WIDTH + EXTRA_WIDTH
        }

        public ScrollWindow(int ax, int ay, int aw, int ah, DWORD aid, ActiveWindow p) :
            base(p.GetScreen(), ax, ay, aw, ah, aid, 0, p)
        {
            captured = false;
            dragging = false;
            selecting = false;
            scrolling = SCROLL.SCROLL_NONE;

            leading = 0;
            scroll_bar = POLICY.SCROLL_AUTO;
            dragdrop = false;
            scroll_count = 0;
            line_count = 0;
            page_count = 0;
            page_size = 1;
            top_index = 0;
            line_height = 0;
            mouse_x = 0;
            mouse_y = 0;

            smooth_scroll = false;
            smooth_offset = 0;

            track_length = ah - 2 * (int)MISC.TRACK_START - (int)MISC.THUMB_HEIGHT;
            thumb_pos = (int)MISC.TRACK_START;

            desc = "ScrollWindow " + id;

        }
        //public ScrollWindow(Screen* s, int ax, int ay, int aw, int ah, DWORD aid, DWORD style = 0, ActiveWindow* parent = 0);
        // public virtual ~ScrollWindow();

        // Operations:
        public virtual void Paint() { throw new NotImplementedException(); }
        public virtual void Draw() { throw new NotImplementedException(); }
        public virtual void DrawTransparent() { throw new NotImplementedException(); }
        public virtual void DrawContent(Rect ctrl_rect) { throw new NotImplementedException(); }
        public virtual void DrawTransparentContent(Rect ctrl_rect) { throw new NotImplementedException(); }
        public virtual void DrawScrollBar() { throw new NotImplementedException(); }
        public override void MoveTo(Stars.Rect r)
        {
            Stars.Rect ctrl_rect = new Stars.Rect(r.x, r.y, r.w, r.h);
            if (r.w > 2 * (int)MISC.BORDER_WIDTH && r.h > 2.5 * (int)MISC.BORDER_HEIGHT)
                ctrl_rect.Deflate((int)MISC.BORDER_WIDTH, (int)MISC.BORDER_HEIGHT);

            base.MoveTo(ctrl_rect);

            track_length = rect.h - 2 * (int)MISC.TRACK_START - (int)MISC.THUMB_HEIGHT;
            thumb_pos = (int)MISC.TRACK_START;
        }


        // Event Target Interface:
        public override int OnMouseMove(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonDown(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonUp(int x, int y) { throw new NotImplementedException(); }
        public override int OnMouseWheel(int wheel) { throw new NotImplementedException(); }
        public override int OnClick() { return base.OnClick(); }

        public virtual int OnKeyDown(int vk, int flags) { throw new NotImplementedException(); }

        // pseudo-events:
        public virtual int OnDragStart(int x, int y) { throw new NotImplementedException(); }
        public virtual int OnDragDrop(int x, int y, ActiveWindow source) { throw new NotImplementedException(); }

        // Property accessors:
        public virtual int GetLineHeight()
        {
            return line_height;
        }
        public virtual void SetLineHeight(int h)
        {
            if (h >= 0)
                line_height = h;
        }
        protected void SetLineHeight()
        {
            if (line_height < 1 && font != null)
                line_height = GetFont().size;
            page_size = rect.h / (line_height + leading);
        }
        public virtual int GetLeading()
        {
            return leading;
        }

        public virtual void SetLeading(int nNewValue)
        {
            if (leading != nNewValue && nNewValue >= 0)
            {
                leading = nNewValue;
            }
        }
        public virtual POLICY GetScrollBarVisible()
        {
            return scroll_bar;
        }
        public virtual void SetScrollBarVisible(POLICY nNewValue)
        {
            if (scroll_bar != nNewValue)
            {
                scroll_bar = nNewValue;
                SetScrollBarVisible();
            }
        }
        protected void SetScrollBarVisible()
        {
            if (scrollRect == null) return;
            scrollRect.vertical = true;
            switch (scroll_bar)
            {
                case POLICY.SCROLL_NEVER:
                    scrollRect.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;
                    break;
                case POLICY.SCROLL_AUTO:
                    scrollRect.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHide;
                    break;
                case POLICY.SCROLL_ALWAYS:
                    scrollRect.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.Permanent;
                    break;
            }
            scrollRect.horizontal = false;
            scrollRect.horizontalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;

        }
        public virtual bool GetDragDrop()
        {
            return dragdrop;
        }
        public virtual void SetDragDrop(bool nNewValue)
        {
            if (dragdrop != nNewValue)
            {
                dragdrop = nNewValue;
            }
        }
        public virtual bool GetSmoothScroll()
        {
            return smooth_scroll;
        }
        public virtual void SetSmoothScroll(bool bNewValue)
        {
            if (smooth_scroll != bNewValue)
            {
                smooth_scroll = bNewValue;
                smooth_offset = top_index;
            }
        }

        public virtual bool IsScrollVisible()
        {
            bool vis = false;

            if (scroll_bar == POLICY.SCROLL_ALWAYS ||
                    (scroll_bar == POLICY.SCROLL_AUTO &&
                        line_count > page_size))
            {
                vis = true;
            }

            return vis;
        }
        public virtual bool CanScroll(int direction, int nlines = 1)
        {
            return false;
        }

        public virtual void EnsureVisible(int index)
        {
            if (index < top_index)
                ScrollTo(index);

            else if (index > top_index + page_size)
                ScrollTo(index - page_size);
        }
        public virtual void Scroll(SCROLL direction, int nlines = 1)
        {
            if (nlines != 0)
            {
                scrolling = direction;

                if (direction == SCROLL.SCROLL_UP || direction == SCROLL.SCROLL_PAGE_UP)
                {
                    top_index--;

                    if (top_index < 0)
                        top_index = 0;

                    else
                        scroll_count = nlines - 1;
                }

                else if (direction == SCROLL.SCROLL_DOWN || direction == SCROLL.SCROLL_PAGE_DOWN)
                {
                    top_index++;

                    if (top_index >= line_count)
                        top_index = line_count - 1;

                    else
                        scroll_count = nlines - 1;
                }

                smooth_offset = top_index;
                thumb_pos = (int)MISC.TRACK_START + (int)(track_length * (double)top_index / (line_count - 1));

                if (scroll_count < 1)
                    scrolling = SCROLL.SCROLL_NONE;
            }
        }
        public virtual void SmoothScroll(SCROLL direction, double nlines)
        {
            if (!smooth_scroll)
            {
                Scroll(direction, (int)nlines);
                return;
            }

            if (direction == SCROLL.SCROLL_UP || direction == SCROLL.SCROLL_PAGE_UP)
            {
                smooth_offset -= nlines;

                if (smooth_offset < 0)
                    smooth_offset = 0;
            }

            else if (direction == SCROLL.SCROLL_DOWN || direction == SCROLL.SCROLL_PAGE_DOWN)
            {
                smooth_offset += nlines;

                if (smooth_offset >= line_count)
                    smooth_offset = line_count - 1;
            }

            top_index = (int)smooth_offset;
            thumb_pos = (int)MISC.TRACK_START + (int)(track_length * smooth_offset / (line_count - 1));
            scrolling = SCROLL.SCROLL_NONE;
        }
        public virtual void ScrollTo(int index)
        {
            if (index >= 0 && index < line_count)
            {
                top_index = index;
                smooth_offset = index;

                thumb_pos = (int)MISC.TRACK_START + (int)(track_length * smooth_offset / (line_count - 1));
            }
        }

        // read-only:
        public virtual int GetTopIndex()
        {
            return top_index;
        }

        public virtual int GetLineCount()
        {
            return line_count;
        }
        public virtual int GetPageCount()
        {
            return line_count / GetPageSize();
        }
        public virtual int GetPageSize()
        {
            return page_size;
        }
        public virtual int GetScrollTrack()
        {
            return rect.w - (int)MISC.SCROLL_TRACK;
        }

        public bool IsDragging() { return dragging; }
        public bool IsSelecting() { return selecting; }
        public SCROLL IsScrolling() { return scrolling; }
        public override void SetForeColor(Color c)
        {
            base.SetForeColor(c);
            SetColors();
        }
        public override void SetBackColor(Color c)
        {
            base.SetBackColor(c);
            SetColors();
        }

        protected void SetColors()
        {
            if (obj == null) return;
            Color scFullHighlight = ColorExtensions.ShadeColor(this.GetBackColor(), 1.6);
            Color scSoftHighlight = ColorExtensions.ShadeColor(this.GetBackColor(), 1.3);
            Color scFullShadow = ColorExtensions.ShadeColor(this.GetBackColor(), 0.3);
            Color scSoftShadow = ColorExtensions.ShadeColor(this.GetBackColor(), 0.6);

            var imgc = obj.GetComponent<Image>();
            if (transparent)
            {
                imgc.color = ColorExtensions.ToTransparent(this.GetBackColor());
            }
            else
                imgc.color = this.GetBackColor();

            ColorBlock cb = new ColorBlock();
            cb.normalColor = scSoftShadow;
            cb.highlightedColor = scSoftShadow;
            cb.pressedColor = scSoftShadow;
            cb.selectedColor = scSoftShadow;
            cb.disabledColor = scFullShadow;
            cb.colorMultiplier = 1;
            var hs = obj.transform.Find("Scrollbar Horizontal").gameObject;
            var sc = hs.GetComponent<Scrollbar>();
            sc.colors = cb;
            imgc = hs.GetComponent<Image>();
            imgc.color = this.GetBackColor();

            var vs = obj.transform.Find("Scrollbar Vertical").gameObject;
            sc = vs.GetComponent<Scrollbar>();
            sc.colors = cb;
            imgc = vs.GetComponent<Image>();
            imgc.color = this.GetBackColor();
        }

        protected internal override void BuildGamebjects()
        {
            base.BuildGamebjects();

            DefaultControls.Resources uiResources = new DefaultControls.Resources();
            obj = DefaultControls.CreateScrollView(uiResources);
            obj.name = desc;
            content = obj.transform.Find("Viewport/Content").gameObject;

            hlg = content.AddComponent<HorizontalLayoutGroup>();
            hlg.childForceExpandHeight = true;
            hlg.childForceExpandWidth = true;
            hlg.childControlHeight = true;
            hlg.childControlWidth = true;
            hlg.padding.left = LeftPadding;
            hlg.childAlignment = TextAnchor.UpperLeft;

            var csf = content.AddComponent<ContentSizeFitter>();
            csf.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            csf.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

            objRectTransform = obj.GetComponent<RectTransform>();
            objRectTransform.SetParent(this.parentObj.transform);

            scrollRect = obj.GetComponent<ScrollRect>();

            vsb = obj.transform.Find("Scrollbar Vertical").gameObject;
            vsb.GetComponent<RectTransform>().sizeDelta = new Vector2(10, 0);
            Sprite handleArea = Resources.Load<Sprite>("Screens/Handler");
            vsb.GetComponent<Image>().sprite = handleArea;

            GameObject sah = obj.transform.Find("Scrollbar Vertical/Sliding Area/Handle").gameObject;
            Sprite handler = Resources.Load<Sprite>("Screens/Btn_17x17_0");
            sah.GetComponent<Image>().sprite = handler;
            RectTransform rectTransform = sah.GetComponent<RectTransform>();
            rectTransform.offsetMin = new Vector2(-3, rectTransform.offsetMin.y);
            rectTransform.offsetMax = new Vector2(-3, rectTransform.offsetMax.y);

            GameObject sa = obj.transform.Find("Scrollbar Vertical/Sliding Area").gameObject;
            rectTransform = sa.GetComponent<RectTransform>();
            rectTransform.offsetMin = new Vector2(0, rectTransform.offsetMin.y);
            rectTransform.offsetMax = new Vector2(0, rectTransform.offsetMax.y);

            SetColors();
            SetScrollBarVisible();
            //SetLineHeight();
        }

        protected bool captured;
        protected bool dragging;
        protected bool selecting;
        protected SCROLL scrolling;
        protected int scroll_count;
        protected int mouse_x;
        protected int mouse_y;
        protected int track_length;
        protected int thumb_pos;

        protected int leading;
        protected POLICY scroll_bar;
        protected bool dragdrop;
        protected int line_count;
        protected int page_count;
        protected int page_size;
        protected int top_index;
        protected int line_height;

        protected bool smooth_scroll;
        protected double smooth_offset;

        protected const int LeftPadding = 10;

        protected GameObject content;
        protected ScrollRect scrollRectCmp;
        protected HorizontalLayoutGroup hlg;
        protected ScrollRect scrollRect;
        protected GameObject vsb;
        protected RectTransform objRectTransform;
    }
}
