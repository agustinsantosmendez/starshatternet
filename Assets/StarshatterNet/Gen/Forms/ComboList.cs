﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      ComboList.h/ComboList.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    ComboList class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using UnityEngine;
using UnityEngine.UI;

namespace StarshatterNet.Gen.Forms
{
    public class ComboList : ScrollWindow
    {
        public ComboList(ComboBox ctrl, ActiveWindow p, int ax, int ay, int aw, int ah, int maxentries) :
            base(ax, ay, aw, ah + (ah - 2) * maxentries + 2, 0, p)
        {
            combo_box = ctrl; 
            button_state = 0;
            button_height = ah;
            captured = false;
            seln = -1;
            max_entries = maxentries;
            scroll = 0;
            scrolling = false;
            border = true;
            animated = true;
            bevel_width = 5;

            if (ctrl != null)
                CopyStyle(ctrl);

            rect.h = (ah - 2) * maxentries + 2;

            int screen_height = Screen.height;

            if (rect.h > screen_height)
            {
                rect.y = 0;
                rect.h = screen_height;
            }

            else if (rect.y + rect.h > screen_height)
            {
                rect.y = screen_height - rect.h;
            }
        }
        // public ComboList(ComboBox* ctrl, Screen* s, int ax, int ay, int aw, int ah, int maxentries);
        // public virtual ~ComboList();

        // Operations:
        public override void Draw() { throw new NotImplementedException(); }
        public override void Show() { throw new NotImplementedException(); }
        public override void Hide() { throw new NotImplementedException(); }

        // Event Target Interface:
        public override int OnMouseMove(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonDown(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonUp(int x, int y) { throw new NotImplementedException(); }
        public override int OnClick() { throw new NotImplementedException(); }
        public override int OnMouseEnter(int x, int y) { throw new NotImplementedException(); }
        public override int OnMouseExit(int x, int y) { throw new NotImplementedException(); }
        public virtual void KillFocus() { throw new NotImplementedException(); }

        // Property accessors:
        public virtual void ClearItems()
        {
            items.Destroy();
        }
        public virtual void AddItem(string item)
        {
            items.Add( item);
        }
        public virtual void AddItems(List<string> item_list)
        {
                 items.AddRange(item_list);
        }
        public virtual void SetItems(List<string> item_list) { throw new NotImplementedException(); }

        public virtual string GetItem(int index)
        {
            if (index >= 0 && index < items.Count)
                return items[index];
            else
                return null;
        }
        public virtual void SetItem(int index, string item)
        {
            if (index >= 0 && index < items.Count)
                items[index] = item;
            else
                items.Add( item);
        }

        public virtual int GetCount()
        {
            return items.Count;
        }
        public virtual string GetSelectedItem()
        {
            if (seln >= 0 && seln < items.Count)
                return items[seln] ;
            else
                return null;
        }
        public virtual int GetSelectedIndex()
        {
            if (seln >= 0 && seln < items.Count)
                return seln;
            else
                return -1;
        }
        public virtual void SetSelection(int index)
        {
            if (index >= 0 && index < items.Count)
                seln = index;
        }


        protected void DrawRectSimple(Rect rect, int stat) { throw new NotImplementedException(); }
        protected void DrawItem(Text label, Rect btn_rect, int state) { throw new NotImplementedException(); }
        protected Rect CalcLabelRect(Rect btn_rect) { throw new NotImplementedException(); }
        protected int CalcSeln(int x, int y) { throw new NotImplementedException(); }
        protected void CopyStyle(ComboBox ctrl)
        {
            active_color = ctrl.GetActiveColor();
            border_color = ctrl.GetBorderColor();
            fore_color = ctrl.GetForeColor();
            back_color = ctrl.GetBackColor();

            animated = ctrl.GetAnimated();
            bevel_width = ctrl.GetBevelWidth();
            border = ctrl.GetBorder();
        }


        protected ComboBox combo_box;
        protected List<string> items = new List<string>();
        protected bool animated;
        protected bool border;
        protected int seln;
        //protected int captured;
        protected int bevel_width;
        protected int button_state;
        protected int button_height;
        protected int max_entries;
        protected int scroll;
        protected bool scrolling;

        protected Color active_color;
        protected Color border_color;
    }
}
