﻿using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;

public class GalaxyTesting : MonoBehaviour
{
    public Scene scene;

    private Camera cam;

    // Use this for initialization
    void Start()
    {
        Galaxy.Initialize();

        // This code is similar to TacRefDlg.cpp, lines 133-160
        scene.SetAmbient(new Color(60f / 255f, 60f / 255f, 60f / 255f));

        CameraNode camera = new CameraNode();
        scene.AddCamera(camera.gameObj);
        cam = camera.gameObj.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
