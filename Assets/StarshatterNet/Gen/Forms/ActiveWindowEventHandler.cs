﻿using System.Collections;
using System.Collections.Generic;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using UnityEngine.EventSystems;

public class ActiveWindowEventHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public ActiveWindow activewindow;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (activewindow != null)
        {
            //Debug.Log("OnPointerEnter, ID:" + activewindow.GetID());
            activewindow.OnMouseEnter((int)eventData.position.x, (int)eventData.position.y);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (activewindow != null)
        {
            //Debug.Log("OnPointerExit, ID:" + activewindow.GetID());
            activewindow.OnMouseExit((int)eventData.position.x, (int)eventData.position.y);
        }
    }
}
