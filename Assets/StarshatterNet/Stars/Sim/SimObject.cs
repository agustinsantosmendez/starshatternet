﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      SimObject.h/SimObject.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Object and Observer classes
 */
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using System;
using System.Collections.Generic;
using DWORD = System.UInt32;

namespace StarshatterNet.Stars.Simulator
{
#warning SimObject class is still in development and is not recommended for production.
    public class SimObject : Physical
    {
        public enum TYPES
        {
            SIM_SHIP = 100,
            SIM_SHOT,
            SIM_DRONE,
            SIM_EXPLOSION,
            SIM_DEBRIS,
            SIM_ASTEROID
        };

        public SimObject() { }
        public SimObject(string n, SimObject.TYPES t = 0) : base(n, (int)t) { }
        ~SimObject()
        {
            Notify();
        }

        public virtual SimRegion GetRegion() { return region; }
        public virtual void SetRegion(SimRegion rgn) { region = rgn; }

        public virtual void Notify()
        {
            if (!notifying)
            {
                notifying = true;

                int nobservers = observers.Count;
                int nupdate = 0;

                if (nobservers > 0)
                {
                    foreach (ISimObserver observer in observers)
                    {
                        observer.Update(this);
                        nupdate++;
                    }

                    observers.Clear();
                }

                if (nobservers != nupdate)
                {
                    ErrLogger.PrintLine("WARNING: incomplete notify sim object '{0}' - {1} of {2} notified ",
                                         Name(), nupdate, nobservers);
                }

                notifying = false;
            }
            else
            {
                ErrLogger.PrintLine("WARNING: double notify on sim object '{0}'\n", Name());
            }
        }

        public virtual void Register(ISimObserver obs)
        {
            if (!notifying && !observers.Contains(obs))
                observers.Add(obs);
        }

        public virtual void Unregister(ISimObserver obs)
        {
            if (!notifying)
                observers.Remove(obs);
        }

        public virtual void Activate(Scene scene)
        {
            if (rep != null)
                scene.AddGraphic(rep);
            if (light != null)
                scene.AddLight(light);

            active = true;
        }

        public virtual void Deactivate(Scene scene)
        {
            if (rep != null)
                scene.DelGraphic(rep);
            if (light != null)
                scene.DelLight(light);

            active = false;
        }


        public virtual DWORD GetObjID() { return objid; }
        public virtual void SetObjID(DWORD id) { objid = id; }

        public virtual bool IsHostileTo(SimObject o)
        { return false; }


        protected SimRegion region = null;
        protected List<ISimObserver> observers = new List<ISimObserver>();
        protected DWORD objid;
        protected bool active = false;
        protected bool notifying = false;
    }
}
