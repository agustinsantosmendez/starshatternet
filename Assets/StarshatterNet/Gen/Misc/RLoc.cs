﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      RLoc.h/RLoc.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Relative Location (RLoc) class declaration
*/
using System;
using DigitalRune.Mathematics.Algebra;

namespace StarshatterNet.Gen.Misc
{
    public class RLoc
    {
        public RLoc() { }
        public RLoc(Vector3D l, double d, double dv = 5e3)
        {
            loc = l;
            base_loc = l;
            rloc = null;
            dex = (float)d;
            dex_var = (float)dv;

        }
        public RLoc(RLoc l, double d, double dv = 5e3)
        {
            rloc = l;
            dex = (float)d;
            dex_var = (float)dv;
        }
        public RLoc(RLoc r)
        {
            loc =r.loc;
            base_loc=r.base_loc;
            rloc=r.rloc;
            dex=r.dex;
            dex_var=r.dex_var;
            az=r.az;
            az_var=r.az_var;
            el=r.el;
            el_var=r.el_var;
        }

        // accessors:
        public Vector3D Location()
        {
            if (rloc != null || dex > 0) Resolve();
            return loc;
        }
        public Vector3D BaseLocation() { return base_loc; }
        public RLoc ReferenceLoc() { return rloc; }
        public double Distance() { return dex; }
        public double DistanceVar() { return dex_var; }
        public double Azimuth() { return az; }
        public double AzimuthVar() { return az_var; }
        public double Elevation() { return el; }
        public double ElevationVar() { return el_var; }

        public void Resolve()
        {
            if (rloc != null)
            {
                base_loc = rloc.Location();
                rloc = null;
            }

            if (dex > 0)
            {
                double d = dex + RandomHelper.Random(-dex_var, dex_var);
                double a = az + RandomHelper.Random(-az_var, az_var);
                double e = el + RandomHelper.Random(-el_var, el_var);

                Vector3F p = new Vector3F((float)(d * Math.Sin(a)),
                (float)(d * -Math.Cos(a)),
                (float)(d * Math.Sin(e)));

                loc = base_loc + p;
                dex = 0;
            }
            else
            {
                loc = base_loc;
            }
        }
        // mutators:
        public void SetBaseLocation(Vector3D l)
        {
            base_loc = l;
            loc = l;
        }
        public void SetReferenceLoc(RLoc r) { rloc = r; }
        public void SetDistance(double d) { dex = (float)d; }
        public void SetDistanceVar(double dv) { dex_var = (float)dv; }
        public void SetAzimuth(double a) { az = (float)a; }
        public void SetAzimuthVar(double av) { az_var = (float)av; }
        public void SetElevation(double e) { el = (float)e; }
        public void SetElevationVar(double ev) { el_var = (float)ev; }

        private Vector3D loc;
        private Vector3D base_loc;
        private RLoc rloc;

        private float dex = 0;
        private float dex_var = 5.0e3f;
        private float az = 0;
        private float az_var = 3.1415f;
        private float el = 0;
        private float el_var = 0.1f;

    }
}
