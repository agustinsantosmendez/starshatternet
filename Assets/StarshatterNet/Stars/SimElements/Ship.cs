﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Ship.h/Ship.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Starship (or space/ground station) class
*/
using System;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Simulator;
using System.Collections.Generic;
using StarshatterNet.Stars.SimElements;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using DWORD = System.UInt32;
using BYTE = System.Byte;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.AI;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using StarshatterNet.Stars.StarSystems;
 using StarshatterNet.Audio;
using StarshatterNet.Stars.Network;
using StarshatterNet.Config;
using DigitalRune.Mathematics;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.Views;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars
{
#warning Ship class is still in development and is not recommended for production.
    public class Ship : SimObject, ISimObserver
    {
        // CONSTRUCTORS:
        public Ship(string ship_name, string reg_num, ShipDesign ship_dsn, int IFF = 0, int cmd_ai = 0, int[] load = null)
        {
            IFF_code = IFF; killer = null; throttle = 0; augmenter = false; throttle_request = 0;
            shield = null; shieldRep = null; main_drive = null; quantum_drive = null; farcaster = null;
            check_fire = false; probe = null; sensor_drone = null; primary = 0; secondary = 1;
            cmd_chain_index = 0; target = null; subtarget = null; radio_orders = null; launch_point = null;
            g_force = 0.0f; sensor = null; navsys = null; flcs = null; hangar = null; respawns = 0; invulnerable = false;
            thruster = null; decoy = null; ai_mode = 2; command_ai_level = cmd_ai; flcs_mode = FLCS_MODE.FLCS_AUTO; loadout = null;
            emcon = 3; old_emcon = 3; master_caution = false; cockpit = null; gear = null; skin = null;
            auto_repair = true; last_repair_time = 0; last_eval_time = 0; last_beam_time = 0; last_bolt_time = 0;
            warp_fov = 1; flight_phase = OP_MODE.LAUNCH; launch_time = 0; carrier = null; dock = null; ff_count = 0;
            inbound = null; element = null; director_info = "Init"; combat_unit = null; net_control = null;
            track = null; ntrack = 0; track_time = 0; helm_heading = 0.0f; helm_pitch = 0.0f;
            altitude_agl = -1.0e6f; transition_time = 0.0f; transition_type = TRAN_TYPE.TRANSITION_NONE;
            friendly_fire_time = 0; ward = null; net_observer_mode = false; orig_elem_index = -1;

            sim = Sim.GetSim();

            name = ship_name;
            if (!string.IsNullOrEmpty(reg_num))
                regnum = reg_num;
            else regnum = null;

            design = ship_dsn;

            if (design == null)
            {
                string msg = string.Format("No ship design found for '{0}'\n", ship_name);
                ErrLogger.PrintLine(msg);
            }

            obj_type = (int)TYPES.SIM_SHIP;

            radius = design.radius;
            mass = design.mass;
            integrity = design.integrity;
            vlimit = design.vlimit;

            agility = design.agility;
            wep_mass = 0.0f;
            wep_resist = 0.0f;

            CL = design.CL;
            CD = design.CD;
            stall = design.stall;

            chase_vec = design.chase_vec;
            bridge_vec = design.bridge_vec;

            acs = design.acs;
            pcs = design.acs;

            auto_repair = design.repair_auto;

            while (base_contact_id == 0)
                base_contact_id = RandomHelper.Rand() % 1000;

            contact_id = base_contact_id++;
            int sys_id = 0;

            for (int i = 0; i < design.reactors.Count; i++)
            {
                PowerSource reactor = new PowerSource(design.reactors[i]);
                reactor.SetShip(this);
                reactor.SetID(sys_id++);
                reactors.Add(reactor);
                systems.Add(reactor);
            }

            for (int i = 0; i < design.drives.Count; i++)
            {
                Drive drive = new Drive(design.drives[i]);
                drive.SetShip(this);
                drive.SetID(sys_id++);

                int src_index = drive.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(drive);

                drives.Add(drive);
                systems.Add(drive);
            }

            if (design.quantum_drive != null)
            {
                quantum_drive = new QuantumDrive(design.quantum_drive);
                quantum_drive.SetShip(this);
                quantum_drive.SetID(sys_id++);

                int src_index = quantum_drive.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(quantum_drive);

                quantum_drive.SetShip(this);
                systems.Add(quantum_drive);
            }

            if (design.farcaster != null)
            {
                farcaster = new Farcaster(design.farcaster);
                farcaster.SetShip(this);
                farcaster.SetID(sys_id++);

                int src_index = farcaster.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(farcaster);

                farcaster.SetShip(this);
                systems.Add(farcaster);
            }

            if (design.thruster != null)
            {
                thruster = new Thruster(design.thruster);
                thruster.SetShip(this);
                thruster.SetID(sys_id++);

                int src_index = thruster.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(thruster);

                thruster.SetShip(this);
                systems.Add(thruster);
            }

            if (design.shield != null)
            {
                shield = new Shield(design.shield);
                shield.SetShip(this);
                shield.SetID(sys_id++);

                int src_index = shield.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(shield);

                if (design.shield_model != null)
                {
                    shieldRep = new ShieldRep();
                    shieldRep.UseModel(design.shield_model);
                }

                systems.Add(shield);
            }

            for (int i = 0; i < design.flight_decks.Count; i++)
            {
                FlightDeck deck = new FlightDeck(design.flight_decks[i]);
                deck.SetShip(this);
                deck.SetCarrier(this);
                deck.SetID(sys_id++);
                deck.SetIndex(i);

                int src_index = deck.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(deck);

                flight_decks.Add(deck);
                systems.Add(deck);
            }

            if (design.flight_decks.Count > 0)
            {
                if (hangar == null)
                {
                    hangar = new Hangar();
                    hangar.SetShip(this);
                }
            }

            if (design.squadrons.Count > 0)
            {
                if (hangar == null)
                {
                    hangar = new Hangar();
                    hangar.SetShip(this);
                }

                for (int i = 0; i < design.squadrons.Count; i++)
                {
                    ShipSquadron s = design.squadrons[i];
                    hangar.CreateSquadron(s.name, null, s.design, s.count, GetIFF(), null, 0, s.avail);
                }
            }

            if (design.gear != null)
            {
                gear = new LandingGear(design.gear);
                gear.SetShip(this);
                gear.SetID(sys_id++);

                int src_index = gear.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(gear);

                systems.Add(gear);
            }

            if (design.sensor != null)
            {
                sensor = new Sensor(design.sensor);
                sensor.SetShip(this);
                sensor.SetID(sys_id++);

                int src_index = sensor.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(sensor);

                if (IsStarship() || IsStatic() || design.name.StartsWith("Camera"))
                    sensor.SetMode(Sensor.Mode.CST);

                systems.Add(sensor);
            }

            int wep_index = 1;

            for (int i = 0; i < design.weapons.Count; i++)
            {
                Weapon gun = new Weapon(design.weapons[i]);
                gun.SetID(sys_id++);
                gun.SetOwner(this);
                gun.SetIndex(wep_index++);

                int src_index = gun.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(gun);

                WeaponGroup group = FindWeaponGroup(gun.Group());
                group.AddWeapon(gun);
                group.SetAbbreviation(gun.Abbreviation());

                systems.Add(gun);

                if (IsDropship() && gun.GetTurret() != null)
                    gun.SetFiringOrders(Weapon.Orders.POINT_DEFENSE);
                else
                    gun.SetFiringOrders(Weapon.Orders.MANUAL);
            }

            int loadout_size = design.hard_points.Count;

            if (load != null && loadout_size > 0)
            {
                loadout = new int[loadout_size];

                for (int i = 0; i < loadout_size; i++)
                {
                    int mounted_weapon = loadout[i] = load[i];

                    if (mounted_weapon < 0)
                        continue;

                    Weapon missile = design.hard_points[i].CreateWeapon(mounted_weapon);

                    if (missile != null)
                    {
                        missile.SetID(sys_id++);
                        missile.SetOwner(this);
                        missile.SetIndex(wep_index++);

                        WeaponGroup group = FindWeaponGroup(missile.Group());
                        group.AddWeapon(missile);
                        group.SetAbbreviation(missile.Abbreviation());

                        systems.Add(missile);
                    }
                }
            }

            if (weapons.Count > 1)
            {
                primary = -1;
                secondary = -1;

                for (int i = 0; i < weapons.Count; i++)
                {
                    WeaponGroup group = weapons[i];
                    if (group.IsPrimary() && primary < 0)
                    {
                        primary = i;

                        // turrets on fighters are set to point defense by default,
                        // this forces the primary turret back to manual control
                        group.SetFiringOrders(Weapon.Orders.MANUAL);
                    }

                    else if (group.IsMissile() && secondary < 0)
                    {
                        secondary = i;
                    }
                }

                if (primary < 0) primary = 0;
                if (secondary < 0) secondary = 1;

                if (weapons.Count > 4)
                {
                    ErrLogger.PrintLine("WARNING: Ship '%s' type '%s' has %d wep groups (max=4)\n",
                    Name(), DesignName(), weapons.Count);
                }
            }

            if (design.decoy != null)
            {
                decoy = new Weapon(design.decoy);
                decoy.SetOwner(this);
                decoy.SetID(sys_id++);
                decoy.SetIndex(wep_index++);

                int src_index = decoy.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(decoy);

                systems.Add(decoy);
            }

            for (int i = 0; i < design.navlights.Count; i++)
            {
                NavLight navlight = new NavLight(design.navlights[i]);
                navlight.SetShip(this);
                navlight.SetID(sys_id++);
                navlight.SetOffset(((DWORD)this.GetHashCode()) << 2);
                navlights.Add(navlight);
                systems.Add(navlight);
            }

            if (design.navsys != null)
            {
                navsys = new NavSystem(design.navsys);
                navsys.SetShip(this);
                navsys.SetID(sys_id++);

                int src_index = navsys.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(navsys);

                systems.Add(navsys);
            }

            if (design.probe != null)
            {
                probe = new Weapon(design.probe);
                probe.SetOwner(this);
                probe.SetID(sys_id++);
                probe.SetIndex(wep_index++);

                int src_index = probe.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(probe);

                systems.Add(probe);
            }

            for (int i = 0; i < design.computers.Count; i++)
            {
                Computer comp = null;

                if (design.computers[i].Subtype() == (int)Computer.CompType.FLIGHT)
                {
                    flcs = new FlightComp(design.computers[i]);

                    flcs.SetShip(this);
                    flcs.SetMode(flcs_mode);
                    flcs.SetVelocityLimit(vlimit);

                    if (thruster != null)
                        flcs.SetTransLimit(thruster.TransXLimit(),
                        thruster.TransYLimit(),
                        thruster.TransZLimit());
                    else
                        flcs.SetTransLimit(design.trans_x,
                        design.trans_y,
                        design.trans_z);

                    comp = flcs;
                }
                else
                {
                    comp = new Computer(design.computers[i]);
                }

                comp.SetShip(this);
                comp.SetID(sys_id++);
                int src_index = comp.GetSourceIndex();
                if (src_index >= 0 && src_index < reactors.Count)
                    reactors[src_index].AddClient(comp);

                computers.Add(comp);
                systems.Add(comp);
            }

            radio_orders = new Instruction("", new Point(0, 0, 0));

            // Load Detail Set:
            for (int i = 0; i < DetailSet.MAX_DETAIL; i++)
            {
                if (design.models[i].Count > 0)
                {
                    Solid solid = new ShipSolid(this);
                    solid.UseModel(design.models[i][0]);
                    solid.CreateShadows(1);

                    Point offset = new Point();
                    Point spin = new Point();

                    if (design.offsets[i].Count > 0)
                        offset = new Point(design.offsets[i][0]);

                    if (design.spin_rates.Count > 0)
                        spin = new Point(design.spin_rates[0]);

                    detail_level = detail.DefineLevel(design.feature_size[i], solid, offset, spin);
                }

                if (design.models[i].Count > 1)
                {
                    for (int n = 1; n < design.models[i].Count; n++)
                    {
                        Solid solid = new ShipSolid(this); //Solid;
                        solid.UseModel(design.models[i][n]);
                        solid.CreateShadows(1);

                        Point offset = new Point();
                        Point spin = new Point();

                        if (design.offsets[i].Count > n)
                            offset = new Point(design.offsets[i][n]);

                        if (design.spin_rates.Count > n)
                            spin = new Point(design.spin_rates[n]);

                        detail.AddToLevel(detail_level, solid, offset, spin);
                    }
                }
            }

            // start with lowest available detail:
            detail_level = 0; // this is highest . detail.NumLevels()-1);
            rep = detail.GetRep(detail_level);

            if (design.cockpit_model != null)
            {
                cockpit = new Solid();
                cockpit.UseModel(design.cockpit_model);
                cockpit.SetForeground(true);
            }

            if (design.main_drive >= 0 && design.main_drive < drives.Count)
                main_drive = drives[design.main_drive];

            // only use light from drives:
            light = null;

            // setup starship helm stuff:
            if (IsStarship())
            {
                flcs_mode = FLCS_MODE.FLCS_HELM;
            }

            // initialize the AI:
            dir = null;
            SetControls(null);

            for (int i = 0; i < 4; i++)
            {
                missile_id[i] = 0;
                missile_eta[i] = 0;
                trigger[i] = false;
            }
        }
        //~Ship();

        public static void Initialize()
        {
            ShipDesign.Initialize();
            Thruster.Initialize();
        }
        public static void Close()
        {
            ShipDesign.Close();
            Thruster.Close();
        }


        public override void ExecFrame(double seconds)
        {
            //ZeroMemory(trigger, sizeof(trigger));
            trigger.Initialize();
            altitude_agl = -1.0e6f;

            if (flight_phase < OP_MODE.LAUNCH)
            {
                DockFrame(seconds);
                return;
            }

            if (flight_phase == OP_MODE.LAUNCH ||
                    (flight_phase == OP_MODE.TAKEOFF && AltitudeAGL() > Radius()))
            {
                SetFlightPhase(OP_MODE.ACTIVE);
            }

            if (transition_time > 0)
            {
                transition_time -= (float)seconds;

                if (transition_time <= 0)
                {
                    CompleteTransition();
                    return;
                }

                if (rep != null && IsDying() && killer != null)
                {
                    killer.ExecFrame(seconds);
                }
            }

            // observers do not run out of power:
            if (IsNetObserver())
            {
                for (int i = 0; i < reactors.Count; i++)
                    reactors[i].SetFuelRange(1e6);
            }

            if (IsStatic())
            {
                StatFrame(seconds);
                return;
            }

            CheckFriendlyFire();
            ExecNavFrame(seconds);
            ExecEvalFrame(seconds);

            if (IsAirborne())
            {
                // are we trying to make orbit?
                if (Location().Y >= TerrainRegion.TERRAIN_ALTITUDE_LIMIT)
                    MakeOrbit();
            }

            if (!InTransition())
            {
                ExecSensors(seconds);
                ExecThrottle(seconds);
            }

            else if (IsDropping() || IsAttaining() || IsSkipping())
            {
                throttle = 100;
            }

            if (target != null && target.Life() == 0)
            {
                DropTarget();
            }

            ExecPhysics(seconds);

            if (!InTransition())
            {
                UpdateTrack();
            }

            // are we docking?
            if (IsDropship())
            {
                foreach (Ship carrier_target in GetRegion().Carriers())
                {

                    double range = (Location() - carrier_target.Location()).Length;
                    if (range > carrier_target.Radius() * 1.5)
                        continue;

                    if (carrier_target.GetIFF() == GetIFF() || carrier_target.GetIFF() == 0)
                    {
                        for (int i = 0; i < carrier_target.NumFlightDecks(); i++)
                        {
                            if (carrier_target.GetFlightDeck(i).Recover(this))
                                break;
                        }
                    }
                }
            }

            ExecSystems(seconds);
            ExecMaintFrame(seconds);

            if (flight_decks.Count > 0)
            {
                CameraNode global_cam = CameraDirector.GetInstance().GetCamera();
                Point global_cam_loc = global_cam.Pos();
                bool disable_shadows = false;

                for (int i = 0; i < flight_decks.Count; i++)
                {
                    if (flight_decks[i].ContainsPoint(global_cam_loc))
                        disable_shadows = true;
                }

                EnableShadows(!disable_shadows);
            }

            if (double.IsInfinity(Location().X))
            {
                DropTarget();
            }

            if (!IsStatic() && !IsGroundUnit() && (int)GetFlightModel() < 2)
                CalcFlightPath();
        }
        public override void AeroFrame(double seconds)
        {
            float g_save = g_accel;

            if (Class() == CLASSIFICATION.LCA)
            {
                lat_thrust = true;
                SetGravity(0.0f);
            }

            if (AltitudeAGL() < Radius())
            {
                SetGravity(0.0f);

                // on the ground/runway?
                double bottom = 1e9;
                double tlevel = Location().Y - AltitudeAGL();

                // taking off or landing?
                if (flight_phase < OP_MODE.ACTIVE || flight_phase > OP_MODE.APPROACH)
                {
                    if (dock != null)
                        tlevel = dock.MountLocation().Y;
                }

                if (tlevel < 0)
                    tlevel = 0;

                if (gear != null)
                    bottom = gear.GetTouchDown() - 1;
                else
                    bottom = Location().Y - 6;

                if (bottom < tlevel)
                    TranslateBy(new Point(0, bottom - tlevel, 0));
            }

            // MODEL 2: ARCADE
            if ((int)flight_model >= 2)
            {
                base.ArcadeFrame(seconds);
            }

            // MODEL 1: RELAXED
            else if ((int)flight_model == 1)
            {
                base.ExecFrame(seconds);
            }

            // MODEL 0: STANDARD
            else
            {
                // apply drag-torque (i.e. turn ship into
                // velocity vector to minimize drag):

                Point vnrm = velocity;
                double v = vnrm.Normalize();
                double pitch_deflection = Point.Dot(vnrm, cam.vup());
                double yaw_deflection = Point.Dot(vnrm, cam.vrt());

                if (lat_thrust && v < 250)
                {
                }

                else
                {
                    if (v < 250)
                    {
                        double factor = 1.2 + (250 - v) / 100;

                        ApplyPitch(pitch_deflection * -factor);
                        ApplyYaw(yaw_deflection * factor);

                        dp += (float)(dp_acc * seconds);
                        dy += (float)(dy_acc * seconds);
                    }

                    else
                    {
                        if (Math.Abs(pitch_deflection) > stall)
                        {
                            ApplyPitch(pitch_deflection * -1.2);
                            dp += (float)(dp_acc * seconds);
                        }

                        ApplyYaw(yaw_deflection * 2);
                        dy += (float)(dy_acc * seconds);
                    }
                }

                // compute rest of physics:
                base.AeroFrame(seconds);
            }

            SetGravity(g_save);
        }
        public virtual void StatFrame(double seconds)
        {
            if (flight_phase != OP_MODE.ACTIVE)
            {
                flight_phase = OP_MODE.ACTIVE;
                launch_time = (uint)(Game.GameTime() + 1);

                if (element != null)
                    element.SetLaunchTime(launch_time);
            }

            if (IsGroundUnit())
            {
                // glue buildings to the terrain:
                Point loc = Location();
                Terrain terrain = region.GetTerrain();

                if (terrain != null)
                {
                    loc.Y = terrain.Height(loc.X, loc.Z);
                    MoveTo(loc);
                }
            }

            if (rep != null)
            {
                rep.MoveTo(cam.Pos());
                rep.SetOrientation(cam.Orientation());
            }

            if (light != null)
            {
                light.MoveTo(cam.Pos());
            }

            ExecSensors(seconds);

            if (target != null && target.Life() == 0)
            {
                DropTarget();
            }

            if (dir != null) dir.ExecFrame(seconds);

            SelectDetail(seconds);

            int i = 0;

            if (rep != null)
            {
                foreach (ShipSystem iter in systems)
                    iter.Orient(this);

                for (i = 0; i < reactors.Count; i++)
                    reactors[i].ExecFrame(seconds);

                for (i = 0; i < navlights.Count; i++)
                    navlights[i].ExecFrame(seconds);

                for (i = 0; i < weapons.Count; i++)
                    weapons[i].ExecFrame(seconds);

                if (farcaster != null)
                {
                    farcaster.ExecFrame(seconds);

                    if (navlights.Count == 2)
                    {
                        if (farcaster.Charge() > 99)
                        {
                            navlights[0].Enable();
                            navlights[1].Disable();
                        }
                        else
                        {
                            navlights[0].Disable();
                            navlights[1].Enable();
                        }
                    }
                }

                if (shield != null)
                    shield.ExecFrame(seconds);

                if (hangar != null)
                    hangar.ExecFrame(seconds);

                if (flight_decks.Count > 0)
                {
                    CameraNode global_cam = CameraDirector.GetInstance().GetCamera();
                    Point global_cam_loc = global_cam.Pos();
                    bool disable_shadows = false;

                    for (i = 0; i < flight_decks.Count; i++)
                    {
                        flight_decks[i].ExecFrame(seconds);

                        if (flight_decks[i].ContainsPoint(global_cam_loc))
                            disable_shadows = true;
                    }

                    EnableShadows(!disable_shadows);
                }
            }

            if (shieldRep != null)
            {
                Solid solid = (Solid)rep;
                shieldRep.MoveTo(solid.Location());
                shieldRep.SetOrientation(solid.Orientation());

                bool bubble = false;
                if (shield != null)
                    bubble = shield.ShieldBubble();

                if (shieldRep.ActiveHits() != 0)
                {
                    shieldRep.Energize(seconds, bubble);
                    shieldRep.Show();
                }
                else
                {
                    shieldRep.Hide();
                }
            }

            if (double.IsInfinity(Location().X))
            {
                DropTarget();
            }
        }

        public virtual void DockFrame(double seconds)
        {
            SelectDetail(seconds);

            if (sim.GetPlayerShip() == this)
            {
                // Make sure the thruster sound is diabled
                // when the player is on the runway or catapult
                if (thruster != null)
                {
                    thruster.ExecTrans(0, 0, 0);
                }
            }

            if (rep != null)
            {
                // Update the graphic rep and light sources:
                // (This is usually done by the physics class,
                // but when the ship is in dock, we skip the
                // standard physics processing):
                rep.MoveTo(cam.Pos());
                rep.SetOrientation(cam.Orientation());

                if (light != null)
                    light.MoveTo(cam.Pos());

                foreach (ShipSystem iter in systems)
                    iter.Orient(this);

                double spool = 75 * seconds;

                if (flight_phase == OP_MODE.DOCKING)
                {
                    throttle_request = 0;
                    throttle = 0;
                }

                else if (throttle < throttle_request)
                    if (throttle_request - throttle < spool)
                        throttle = throttle_request;
                    else
                        throttle += spool;

                else if (throttle > throttle_request)
                    if (throttle - throttle_request < spool)
                        throttle = throttle_request;
                    else
                        throttle -= spool;

                // make sure there is power to run the drive:
                for (int i = 0; i < reactors.Count; i++)
                    reactors[i].ExecFrame(seconds);

                // count up weapon ammo for status mfd:
                for (int i = 0; i < weapons.Count; i++)
                    weapons[i].ExecFrame(seconds);

                // show drive flare while on catapult:
                if (main_drive != null)
                {
                    main_drive.SetThrottle(throttle);

                    if (throttle > 0)
                        main_drive.Thrust(seconds);  // show drive flare
                }
            }

            if (cockpit != null && !cockpit.Hidden())
            {
                Solid solid = (Solid)rep;

                Point cpos = cam.Pos() +
                cam.vrt() * bridge_vec.X +
                cam.vpn() * bridge_vec.Y +
                cam.vup() * bridge_vec.Z;

                cockpit.MoveTo(cpos);
                cockpit.SetOrientation(solid.Orientation());
            }
        }
        public override void LinearFrame(double seconds)
        {
            base.LinearFrame(seconds);

            if (!IsAirborne() || Class() != CLASSIFICATION.LCA)
                return;

            // damp lateral movement in atmosphere:

            // side-to-side
            if (trans_x == 0)
            {
                Point transvec = cam.vrt();
                transvec *= (transvec * velocity) * seconds * 0.5;
                velocity -= transvec;
            }

            // fore-and-aft
            if (trans_y == 0 && Math.Abs(thrust) < 1.0f)
            {
                Point transvec = cam.vpn();
                transvec *= (transvec * velocity) * seconds * 0.25;
                velocity -= transvec;
            }

            // up-and-down
            if (trans_z == 0)
            {
                Point transvec = cam.vup();
                transvec *= (transvec * velocity) * seconds * 0.5;
                velocity -= transvec;
            }
        }
        public virtual void ExecSensors(double seconds)
        {
            // how visible are we?
            DoEMCON();

            // what can we see?
            if (sensor != null)
                sensor.ExecFrame(seconds);

            // can we still see our target?   
            if (target != null)
            {
                bool target_found = false;
                foreach (Contact c in ContactList())
                {
                    if (target == c.GetShip() || target == c.GetShot())
                    {
                        target_found = true;

                        bool vis = c.Visible(this) || c.Threat(this);

                        if (!vis && !c.PasLock() && !c.ActLock())
                            DropTarget();
                    }
                }

                if (!target_found)
                    DropTarget();
            }
        }

        public void ExecNavFrame(double seconds)
        {
            bool auto_pilot = false;

            // update director info string:
            SetFLCSMode(flcs_mode);

            if (navsys != null)
            {
                navsys.ExecFrame(seconds);

                if (navsys.AutoNavEngaged())
                {
                    if (dir != null && (int)dir.Type() == NavAI.DIR_TYPE)
                    {
                        NavAI navai = (NavAI)dir;

                        if (navai.Complete())
                        {
                            navsys.DisengageAutoNav();
                            SetControls(sim.GetControls());
                        }
                        else
                        {
                            auto_pilot = true;
                        }
                    }
                }
            }

            // even if we are not on auto pilot,
            // have we completed the next navpoint?

            Instruction navpt = GetNextNavPoint();
            if (navpt != null && !auto_pilot)
            {
                if (navpt.Region() == GetRegion())
                {
                    double distance = 0;

                    Point npt = navpt.Location();

                    if (navpt.Region() != null)
                        npt += navpt.Region().Location();

                    Sim sim = Sim.GetSim();
                    if (sim.GetActiveRegion() != null)
                        npt -= sim.GetActiveRegion().Location();

                    npt = npt.OtherHand();

                    // distance from self to navpt:
                    distance = new Point(npt - Location()).Length;

                    if (distance < 10 * Radius())
                        SetNavptStatus(navpt, Instruction.STATUS.COMPLETE);
                }
            }
        }
        public void ExecPhysics(double seconds)
        {
            if (net_control != null)
            {
                net_control.ExecFrame(seconds);
                Thrust(seconds);  // drive flare
            }
            else
            {
                thrust = (float)Thrust(seconds);
                SetupAgility();

                if (seconds > 0)
                {
                    g_force = 0.0f;
                }

                if (IsAirborne())
                {
                    Point v1 = velocity;
                    AeroFrame(seconds);
                    Point v2 = velocity;
                    Point dv = v2 - v1 + new Point(0, g_accel * seconds, 0);

                    if (seconds > 0)
                    {
                        g_force = (float)(Point.Dot(dv, cam.vup()) / seconds) / 9.8f;
                    }
                }

                else if (IsDying() || (int)flight_model < 2)
                { // standard and relaxed modes
                    base.ExecFrame(seconds);
                }

                else
                {                                    // arcade mode
                    base.ArcadeFrame(seconds);
                }
            }
        }
        public void ExecThrottle(double seconds)
        {
            double spool = 75 * seconds;

            if (throttle < throttle_request)
                if (throttle_request - throttle < spool)
                    throttle = throttle_request;
                else
                    throttle += spool;

            else if (throttle > throttle_request)
                if (throttle - throttle_request < spool)
                    throttle = throttle_request;
                else
                    throttle -= spool;
        }
        public void ExecSystems(double seconds)
        {
            if (rep == null)
                return;

            int i;

            foreach (ShipSystem sys in systems)
            {
                sys.Orient(this);

                // sensors have already been executed,
                // they can not be run twice in a frame!
                if (sys.Type() != ShipSystem.CATEGORY.SENSOR)
                    sys.ExecFrame(seconds);
            }

            // hangars and weapon groups are not systems
            // they must be executed separately from above
            if (hangar != null)
                hangar.ExecFrame(seconds);

            wep_mass = 0.0f;
            wep_resist = 0.0f;

            bool winchester_cycle = false;

            for (i = 0; i < weapons.Count; i++)
            {
                WeaponGroup w_group = weapons[i];
                w_group.ExecFrame(seconds);

                if (w_group.GetTrigger() && w_group.GetFiringOrders() == (int)Weapon.Orders.MANUAL)
                {

                    Weapon gun = w_group.GetSelected();

                    SimObject gun_tgt = gun.GetTarget();

                    // if no target has been designated for this
                    // weapon, let it guide on the contact closest
                    // to its boresight.  this must be done before
                    // firing the weapon.

                    if (sensor != null && gun.Guided() != 0 && !gun.Design().beam && gun_tgt == null)
                    {
                        gun.SetTarget(sensor.AcquirePassiveTargetForMissile(), null);
                    }

                    gun.Fire();

                    w_group.SetTrigger(false);
                    w_group.CycleWeapon();
                    w_group.CheckAmmo();

                    // was that the last shot from this missile group?
                    if (w_group.IsMissile() && w_group.Ammo() < 1)
                    {

                        // is this the current secondary weapon group?
                        if (weapons[secondary] == w_group)
                        {
                            winchester_cycle = true;
                        }
                    }
                }

                wep_mass += w_group.Mass();
                wep_resist += w_group.Resistance();
            }

            // if we just fired the last shot in the current secondary
            // weapon group, auto cycle to another secondary weapon:
            if (winchester_cycle)
            {
                int old_secondary = secondary;

                CycleSecondary();

                // do not winchester-cycle to an A2G missile type, 
                // or a missile that is also out of ammo,
                // keep going!

                while (secondary != old_secondary)
                {
                    Weapon missile = GetSecondary();
                    if (missile != null && missile.CanTarget(CLASSIFICATION.GROUND_UNITS))
                        CycleSecondary();

                    else if (weapons[secondary].Ammo() < 1)
                        CycleSecondary();

                    else
                        break;
                }
            }

            mass = (float)design.mass + wep_mass;

            if (IsDropship())
                agility = (float)design.agility - wep_resist;

            if (shieldRep != null)
            {
                Solid solid = (Solid)rep;
                shieldRep.MoveTo(solid.Location());
                shieldRep.SetOrientation(solid.Orientation());

                bool bubble = false;
                if (shield != null)
                    bubble = shield.ShieldBubble();

                if (shieldRep.ActiveHits() != 0)
                {
                    shieldRep.Energize(seconds, bubble);
                    shieldRep.Show();
                }
                else
                {
                    shieldRep.Hide();
                }
            }

            if (cockpit != null)
            {
                Solid solid = (Solid)rep;

                Point cpos = cam.Pos() +
                cam.vrt() * bridge_vec.X +
                cam.vpn() * bridge_vec.Y +
                cam.vup() * bridge_vec.Z;

                cockpit.MoveTo(cpos);
                cockpit.SetOrientation(solid.Orientation());
            }
        }

        public override void Activate(Scene scene)
        {
            int i = 0;
            base.Activate(scene);

            for (i = 0; i < detail.NumModels(detail_level); i++)
            {
                Graphic g = detail.GetRep(detail_level, i);
                scene.AddGraphic(g);
            }

            for (i = 0; i < flight_decks.Count; i++)
                scene.AddLight(flight_decks[i].GetLight());

            if (shieldRep != null)
                scene.AddGraphic(shieldRep);

            if (cockpit != null)
            {
                scene.AddForeground(cockpit);
                cockpit.Hide();
            }

            Drive drive = GetDrive();
            if (drive != null)
            {
                for (i = 0; i < drive.NumEngines(); i++)
                {
                    Graphic flare = drive.GetFlare(i);
                    if (flare != null)
                    {
                        scene.AddGraphic(flare);
                    }

                    Graphic trail = drive.GetTrail(i);
                    if (trail != null)
                    {
                        scene.AddGraphic(trail);
                    }
                }
            }

            Thruster thruster = GetThruster();
            if (thruster != null)
            {
                for (i = 0; i < thruster.NumThrusters(); i++)
                {
                    Graphic flare = thruster.Flare(i);
                    if (flare != null)
                    {
                        scene.AddGraphic(flare);
                    }

                    Graphic trail = thruster.Trail(i);
                    if (trail != null)
                    {
                        scene.AddGraphic(trail);
                    }
                }
            }

            for (int n = 0; n < navlights.Count; n++)
            {
                NavLight navlight = navlights[n];
                for (i = 0; i < navlight.NumBeacons(); i++)
                {
                    Graphic beacon = navlight.Beacon(i);
                    if (beacon != null)
                        scene.AddGraphic(beacon);
                }
            }

            foreach (WeaponGroup g in weapons)
            {
                foreach (Weapon w in g.GetWeapons())
                {
                    Solid turret = w.GetTurret();
                    if (turret != null)
                    {
                        scene.AddGraphic(turret);

                        Solid turret_base = w.GetTurretBase();
                        if (turret_base != null)
                            scene.AddGraphic(turret_base);
                    }
                    if (w.IsMissile())
                    {
                        for (i = 0; i < w.Ammo(); i++)
                        {
                            Solid store = w.GetVisibleStore(i);
                            if (store != null)
                                scene.AddGraphic(store);
                        }
                    }
                }
            }

            if (gear != null && gear.GetState() != LandingGear.GEAR_STATE.GEAR_UP)
            {
                for (i = 0; i < gear.NumGear(); i++)
                {
                    scene.AddGraphic(gear.GetGear(i));
                }
            }
        }
        public override void Deactivate(Scene scene)
        {
            int i = 0;
            base.Deactivate(scene);

            for (i = 0; i < detail.NumModels(detail_level); i++)
            {
                Graphic g = detail.GetRep(detail_level, i);
                scene.DelGraphic(g);
            }

            for (i = 0; i < flight_decks.Count; i++)
                scene.DelLight(flight_decks[i].GetLight());

            if (shieldRep != null)
                scene.DelGraphic(shieldRep);

            if (cockpit != null)
                scene.DelForeground(cockpit);

            Drive drive = GetDrive();
            if (drive != null)
            {
                for (i = 0; i < drive.NumEngines(); i++)
                {
                    Graphic flare = drive.GetFlare(i);
                    if (flare != null)
                    {
                        scene.DelGraphic(flare);
                    }

                    Graphic trail = drive.GetTrail(i);
                    if (trail != null)
                    {
                        scene.DelGraphic(trail);
                    }
                }
            }

            Thruster thruster = GetThruster();
            if (thruster != null)
            {
                for (i = 0; i < thruster.NumThrusters(); i++)
                {
                    Graphic flare = thruster.Flare(i);
                    if (flare != null)
                    {
                        scene.DelGraphic(flare);
                    }

                    Graphic trail = thruster.Trail(i);
                    if (trail != null)
                    {
                        scene.DelGraphic(trail);
                    }
                }
            }

            for (int n = 0; n < navlights.Count; n++)
            {
                NavLight navlight = navlights[n];
                for (i = 0; i < navlight.NumBeacons(); i++)
                {
                    Graphic beacon = navlight.Beacon(i);
                    if (beacon != null)
                        scene.DelGraphic(beacon);
                }
            }

            foreach (WeaponGroup g in weapons)
            {
                foreach (Weapon w in g.GetWeapons())
                {
                    Solid turret = w.GetTurret();
                    if (turret != null)
                    {
                        scene.DelGraphic(turret);

                        Solid turret_base = w.GetTurretBase();
                        if (turret_base != null)
                            scene.DelGraphic(turret_base);
                    }
                    if (w.IsMissile())
                    {
                        for (i = 0; i < w.Ammo(); i++)
                        {
                            Solid store = w.GetVisibleStore(i);
                            if (store != null)
                                scene.DelGraphic(store);
                        }
                    }
                }
            }

            if (gear != null)
            {
                for (i = 0; i < gear.NumGear(); i++)
                {
                    scene.DelGraphic(gear.GetGear(i));
                }
            }
        }
        public virtual void SelectDetail(double seconds)
        {
            detail.ExecFrame(seconds);
            detail.SetLocation(GetRegion(), Location());

            int new_level = detail.GetDetailLevel();

            if (detail_level != new_level)
            {
                Scene scene = null;

                // remove current rep from scene (if necessary):
                for (int i = 0; i < detail.NumModels(detail_level); i++)
                {
                    Graphic g = detail.GetRep(detail_level, i);
                    if (g != null)
                    {
                        scene = g.GetScene();
                        if (scene!= null)
                            scene.DelGraphic(g);
                    }
                }

                // switch to new rep:
                detail_level = new_level;
                rep = detail.GetRep(detail_level);

                // add new rep to scene (if necessary):
                if (scene != null)
                {
                    for (int i = 0; i < detail.NumModels(detail_level); i++)
                    {
                        Graphic g = detail.GetRep(detail_level, i);
                        Point s = detail.GetSpin(detail_level, i);
                        Matrix33D m = cam.Orientation();

                        m.Pitch(s.X);
                        m.Yaw(s.Z);
                        m.Roll(s.Y);

                        scene.AddGraphic(g);
                        g.MoveTo(cam.Pos() + detail.GetOffset(detail_level, i));
                        g.SetOrientation(m);
                    }

                    // show/hide external stores and landing gear...
                    if (detail.NumLevels() > 0)
                    {
                        if (gear != null && (gear.GetState() != LandingGear.GEAR_STATE.GEAR_UP))
                        {
                            for (int i = 0; i < gear.NumGear(); i++)
                            {
                                Solid g = gear.GetGear(i);

                                if (g != null)
                                {
                                    if (detail_level == 0)
                                        scene.DelGraphic(g);
                                    else
                                        scene.AddGraphic(g);
                                }
                            }
                        }

                        foreach (WeaponGroup g in weapons)
                        {
                            foreach (Weapon w in g.GetWeapons())
                            {
                                Solid turret = w.GetTurret();
                                if (turret != null)
                                {
                                    if (detail_level == 0)
                                        scene.DelGraphic(turret);
                                    else
                                        scene.AddGraphic(turret);

                                    Solid turret_base = w.GetTurretBase();
                                    if (turret_base != null)
                                    {
                                        if (detail_level == 0)
                                            scene.DelGraphic(turret_base);
                                        else
                                            scene.AddGraphic(turret_base);
                                    }
                                }
                                if (w.IsMissile())
                                {
                                    for (int i = 0; i < w.Ammo(); i++)
                                    {
                                        Solid store = w.GetVisibleStore(i);
                                        if (store != null)
                                        {
                                            if (detail_level == 0)
                                                scene.DelGraphic(store);
                                            else
                                                scene.AddGraphic(store);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            else
            {
                int nmodels = detail.NumModels(detail_level);

                if (nmodels > 1)
                {
                    for (int i = 0; i < nmodels; i++)
                    {
                        Graphic g = detail.GetRep(detail_level, i);
                        Point s = detail.GetSpin(detail_level, i);
                        Matrix33D m = cam.Orientation();

                        m.Pitch(s.X);
                        m.Yaw(s.Z);
                        m.Roll(s.Y);

                        g.MoveTo(cam.Pos() + detail.GetOffset(detail_level, i));
                        g.SetOrientation(m);
                    }
                }
            }
        }
        public override void SetRegion(SimRegion rgn)
        {
            base.SetRegion(rgn);

            const double GRAV = 6.673e-11;

            if (IsGroundUnit())
            {
                // glue buildings to the terrain:
                Point loc = Location();
                Terrain terrain = region.GetTerrain();

                if (terrain != null)
                {
                    loc.Y = terrain.Height(loc.X, loc.Z);
                    MoveTo(loc);
                }
            }

            else if (IsAirborne())
            {
                Orbital primary = GetRegion().GetOrbitalRegion().Primary();

                double m0 = primary.Mass();
                double r = primary.Radius();

                SetGravity((float)(GRAV * m0 / (r * r)));
                SetBaseDensity(1.0f);
            }

            else
            {
                SetGravity(0.0f);
                SetBaseDensity(0.0f);

                if (IsStarship())
                    flcs_mode = FLCS_MODE.FLCS_HELM;
                else
                    flcs_mode = FLCS_MODE.FLCS_AUTO;
            }
        }
        public virtual int GetTextureList(List<Bitmap> textures)
        {
            textures.Clear();

            for (int d = 0; d < detail.NumLevels(); d++)
            {
                for (int i = 0; i < detail.NumModels(d); i++)
                {
                    Graphic g = detail.GetRep(d, i);

                    if (g.IsSolid())
                    {
                        Solid solid = (Solid)g;
                        Model model = solid.GetModel();

                        if (model != null)
                        {
                            for (int n = 0; n < model.NumMaterials(); n++)
                            {
                                //textures.Add(model.textures[n]);
                            }
                        }
                    }
                }
            }

            return textures.Count;
        }

        // DIRECTION:
        public virtual void SetControls(MotionController m)
        {
            if (IsDropping() || IsAttaining())
            {
                if (dir != null && (int)dir.Type() != DropShipAI.DIR_TYPE)
                {
                    //delete dir;
                    dir = new DropShipAI(this);
                }

                return;
            }

            else if (IsSkipping())
            {
                if (navsys != null && sim.GetPlayerShip() == this)
                    navsys.EngageAutoNav();
            }

            else if (IsDying() || IsDead())
            {
                if (dir != null)
                {
                    //delete dir;
                    dir = null;
                }

                if (navsys != null && navsys.AutoNavEngaged())
                {
                    navsys.DisengageAutoNav();
                }

                return;
            }

            else if (life == 0)
            {
                if (dir != null || navsys != null)
                {
                    ErrLogger.PrintLine("Warning: dying ship '%' still has not been destroyed!\n", name);
                    //delete dir;
                    dir = null;

                    if (navsys != null && navsys.AutoNavEngaged())
                        navsys.DisengageAutoNav();
                }

                return;
            }

            if (navsys != null && navsys.AutoNavEngaged())
            {
                NavAI nav = null;

                if (dir != null)
                {
                    if ((int)dir.Type() != NavAI.DIR_TYPE)
                    {
                        // delete dir;
                        dir = null;
                    }
                    else
                    {
                        nav = (NavAI)dir;
                    }
                }

                if (nav == null)
                {
                    nav = new NavAI(this);
                    dir = nav;
                    return;
                }

                else if (!nav.Complete())
                {
                    return;
                }
            }

            if (dir != null)
            {
                //delete dir;
                dir = null;
            }

            if (m != null)
            {
                Keyboard.FlushKeys();
                m.Acquire();
                dir = new ShipCtrl(this, m);
                director_info = Game.GetText("flcs.auto");
            }
            else if (GetIFF() < 100)
            {
                if (IsStatic())
                    dir = SteerAI.Create(this, SteerAI.SteerType.GROUND);

                else if (IsStarship() && !IsAirborne())
                    dir = SteerAI.Create(this, SteerAI.SteerType.STARSHIP);

                else
                    dir = SteerAI.Create(this, SteerAI.SteerType.FIGHTER);
            }
        }
        public virtual void SetNetworkControl(IDirector net = null)
        {
            net_control = net;

            // delete dir;
            dir = null;

            if (net_control == null && GetIFF() < 100)
            {
                if (IsStatic())
                    dir = null;
                else if (IsStarship())
                    dir = SteerAI.Create(this, SteerAI.SteerType.STARSHIP);
                else
                    dir = SteerAI.Create(this, SteerAI.SteerType.FIGHTER);
            }
        }

        public void SetDirectorInfo(string msg) { director_info = msg; }
        public string GetDirectorInfo() { return director_info; }
        public void SetAIMode(int n) { ai_mode = (BYTE)n; }
        public int GetAIMode() { return (int)ai_mode; }
        public void SetCommandAILevel(int n) { command_ai_level = (BYTE)n; }
        public int GetCommandAILevel() { return command_ai_level; }
        public virtual OP_MODE GetFlightPhase() { return flight_phase; }
        public virtual void SetFlightPhase(OP_MODE phase)
        {
            if (phase == OP_MODE.ACTIVE && launch_time == 0)
            {
                launch_time = (DWORD)(Game.GameTime() + 1);
                dock = null;

                if (element != null)
                    element.SetLaunchTime(launch_time);
            }

            flight_phase = phase;

            if (flight_phase == OP_MODE.ACTIVE)
                dock = null;
        }
        public bool IsNetObserver() { return net_observer_mode; }
        public void SetNetObserver(bool n) { net_observer_mode = n; }

        public bool IsInvulnerable() { return invulnerable; }
        public void SetInvulnerable(bool n) { invulnerable = n; }

        public double GetHelmHeading() { return helm_heading; }
        public double GetHelmPitch() { return helm_pitch; }
        public void SetHelmHeading(double h)
        {
            while (h < 0)
                h += 2 * Math.PI;

            while (h >= 2 * Math.PI)
                h -= 2 * Math.PI;

            helm_heading = (float)h;
        }
        public void SetHelmPitch(double p)
        {
            const double PITCH_LIMIT = 80 * ConstantsF.DEGREES;

            if (p < -PITCH_LIMIT)
                p = -PITCH_LIMIT;

            else if (p > PITCH_LIMIT)
                p = PITCH_LIMIT;

            helm_pitch = (float)p;
        }


        public virtual void ApplyHelmYaw(double y)
        {
            // rotate compass into helm-relative orientation:
            double compass = CompassHeading() - helm_heading;
            double turn = y * Math.PI / 4;

            if (compass > Math.PI)
                compass -= 2 * Math.PI;
            else if (compass < -Math.PI)
                compass += 2 * Math.PI;

            // if requested turn is more than 170, reject it:
            if (Math.Abs(compass + turn) > 170 * ConstantsF.DEGREES)
                return;

            SetHelmHeading(helm_heading + turn);
        }
        public virtual void ApplyHelmPitch(double p)
        {
            SetHelmPitch(helm_pitch - p * Math.PI / 4);
        }
        public override void ApplyPitch(double p)   // override for G limiter
        {
            if (flight_model == 0)
            { // standard flight model
                if (IsAirborne())
                    p *= 0.5;

                // command for pitch up is negative
                if (p < 0)
                {
                    if (alpha > Math.PI / 6)
                    {
                        p *= 0.05;
                    }
                    else if (g_force > 12.0)
                    {
                        double limit = 0.5 - (g_force - 12.0) / 10.0;

                        if (limit < 0)
                            p = 0;
                        else
                            p *= limit;
                    }
                }

                // command for pitch down is positive
                else if (p > 0)
                {
                    if (alpha < -Math.PI / 8)
                    {
                        p *= 0.05;
                    }
                    else if (g_force < -3)
                    {
                        p *= 0.1;
                    }
                }
            }

            base.ApplyPitch(p);
        }

        public void ArcadeStop() { arcade_velocity *= 0; }

        // CAMERA:
        public Point BridgeLocation() { return bridge_vec; }
        public Point ChaseLocation() { return chase_vec; }
        public Point TransitionLocation() { return transition_loc; }

        // FLIGHT DECK:
        public Ship GetController()
        {
            Ship controller = null;

            if (carrier != null)
            {
                // are we in same region as carrier?
                if (carrier.GetRegion() == GetRegion())
                {
                    return carrier;
                }

                // if not, figure out who our control unit is:
                else
                {
                    double distance = 10e6;

                    foreach (Ship test in GetRegion().Carriers())
                    {
                        if (test.GetIFF() == GetIFF())
                        {
                            double d = new Point(Location() - test.Location()).Length;
                            if (d < distance)
                            {
                                controller = test;
                                distance = d;
                            }
                        }
                    }
                }
            }

            if (controller == null)
            {
                if (element != null && element.GetCommander() != null)
                    controller = element.GetCommander().GetShip(1);
            }

            return controller;
        }
        public int NumInbound()
        {
            int result = 0;

            for (int i = 0; i < flight_decks.Count; i++)
            {
                result += flight_decks[i].GetRecoveryQueue().Count;
            }

            return result;
        }
        public int NumFlightDecks()
        {
            return flight_decks.Count;
        }
        public FlightDeck GetFlightDeck(int i = 0)
        {
            if (i >= 0 && i < flight_decks.Count)
                return flight_decks[i];

            return null;
        }
        public Ship GetCarrier() { return carrier; }
        public FlightDeck GetDock() { return dock; }
        public void SetCarrier(Ship c, FlightDeck d)
        {
            carrier = c;
            dock = d;

            if (carrier != null)
                Observe(carrier);
        }
        public void Stow()
        {
            if (carrier != null && carrier.GetHangar() != null)
                carrier.GetHangar().Stow(this);
        }
        public InboundSlot GetInbound() { return inbound; }
        public void SetInbound(InboundSlot s)
        {
            inbound = s;

            if (inbound != null && flight_phase == OP_MODE.ACTIVE)
            {
                flight_phase = OP_MODE.APPROACH;

                SetCarrier((Ship)inbound.GetDeck().GetCarrier(), inbound.GetDeck());

                HUDView hud = HUDView.GetInstance();

                if (hud != null && hud.GetShip() == this)
                    hud.SetHUDMode(HUDView.HUDModes.HUD_MODE_ILS);
            }
        }

        // DRIVE SYSTEMS:
        public int GetFuelLevel()  // (0-100) percent of full tank
        {
            if (reactors.Count > 0)
            {
                PowerSource reactor = reactors[0];
                if (reactor != null)
                    return reactor.Charge();
            }

            return 0;
        }
        public void SetThrottle(double percent)
        {
            throttle_request = percent;

            if (throttle_request < 0) throttle_request = 0;
            else if (throttle_request > 100) throttle_request = 100;

            if (throttle_request < 50)
                augmenter = false;
        }
        public void SetAugmenter(bool enable)
        {
            if (throttle <= 50)
                enable = false;

            if (main_drive != null && main_drive.MaxAugmenter() <= 0)
                enable = false;

            augmenter = enable;
        }
        public double Thrust(double seconds)
        {
            double total_thrust = 0;

            if (main_drive != null)
            {
                // velocity limiter:
                Point H = Heading();
                Point V = Velocity();
                double vmag = V.Normalize();
                double eff_throttle = throttle;
                double thrust_factor = 1;
                double vfwd = Point.Dot(H, V);
                bool aug_on = main_drive.IsAugmenterOn();

                if (vmag > vlimit && vfwd > 0)
                {
                    double vmax = vlimit;
                    if (aug_on)
                        vmax *= 1.5;

                    vfwd = 0.5 * vfwd + 0.5;

                    // reduce drive efficiency at high fwd speed:
                    thrust_factor = (vfwd * Math.Pow(vmax, 3) / Math.Pow(vmag, 3)) + (1 - vfwd);
                }

                if (flcs != null)
                    eff_throttle = flcs.Throttle();

                // square-law throttle curve to increase sensitivity
                // at lower throttle settings:
                if ((int)flight_model > 1)
                {
                    eff_throttle /= 100;
                    eff_throttle *= eff_throttle;
                    eff_throttle *= 100;
                }

                main_drive.SetThrottle(eff_throttle, augmenter);
                total_thrust += thrust_factor * main_drive.Thrust(seconds);

                if (aug_on && shake < 1.5)
                    ((Ship)this).shake = 1.5f;
            }

            return total_thrust;
        }
        public double VelocityLimit() { return vlimit; }
        public Drive GetDrive() { return main_drive; }
        public double Throttle() { return throttle; }
        public bool Augmenter() { return augmenter; }
        public QuantumDrive GetQuantumDrive() { return quantum_drive; }
        public Farcaster GetFarcaster() { return farcaster; }

        public bool IsAirborne()
        {
            if (region != null)
                return region.Type() == SimRegion.TYPES.AIR_SPACE;

            return false;
        }

        public bool IsDropCam() { return transition_type == TRAN_TYPE.TRANSITION_DROP_CAM; }
        public bool IsDropping() { return transition_type == TRAN_TYPE.TRANSITION_DROP_ORBIT; }
        public bool IsAttaining() { return transition_type == TRAN_TYPE.TRANSITION_MAKE_ORBIT; }
        public bool IsSkipping() { return transition_type == TRAN_TYPE.TRANSITION_TIME_SKIP; }
        public bool IsDying() { return transition_type == TRAN_TYPE.TRANSITION_DEATH_SPIRAL; }
        public bool IsDead() { return transition_type == TRAN_TYPE.TRANSITION_DEAD; }
        public bool InTransition() { return transition_type != TRAN_TYPE.TRANSITION_NONE; }
        public void DropOrbit()
        {
            if (IsDropship() && transition_type == TRAN_TYPE.TRANSITION_NONE && !IsAirborne())
            {
                SimRegion dst_rgn = sim.FindNearestTerrainRegion(this);

                if (dst_rgn != null &&
                        dst_rgn.GetOrbitalRegion().Primary() ==
                        GetRegion().GetOrbitalRegion().Primary())
                {

                    transition_time = 10.0f;
                    transition_type = TRAN_TYPE.TRANSITION_DROP_ORBIT;
                    transition_loc = Location() + Heading() * (-2 * Radius());

                    RadioTraffic.SendQuickMessage(this, RadioMessage.ACTION.BREAK_ORBIT);
                    SetControls(null);
                }
            }
        }
        public void MakeOrbit()
        {
            if (IsDropship() && transition_type == TRAN_TYPE.TRANSITION_NONE && IsAirborne())
            {
                transition_time = 5.0f;
                transition_type = TRAN_TYPE.TRANSITION_MAKE_ORBIT;
                transition_loc = Location() + Heading() * (-2 * Radius());

                RadioTraffic.SendQuickMessage(this, RadioMessage.ACTION.MAKE_ORBIT);
                SetControls(null);
            }
        }
        public bool CanTimeSkip()
        {
            bool go = false;
            Instruction navpt = GetNextNavPoint();

            if (MissionClock() < 10000 || NetGame.IsNetGame())
                return go;

            if (navpt != null)
            {
                go = true;

                if (navpt.Region() != GetRegion())
                    go = false;

                else if (new Point(navpt.Location().OtherHand() - Location()).Length < 30e3)
                    go = false;
            }

            if (go)
                go = !IsInCombat();

            return go;
        }
        public bool IsInCombat()
        {
            if (IsRogue())
                return true;

            bool combat = false;

            foreach (Contact c in ContactList())
            {
                Ship cship = c.GetShip();
                int ciff = c.GetIFF(this);
                Point delta = c.Location() - Location();
                double dist = delta.Length;

                if (c.Threat(this) && cship == null)
                {
                    if (IsStarship())
                        combat = dist < 120e3;
                    else
                        combat = dist < 60e3;
                }

                else if (cship != null && ciff > 0 && ciff != GetIFF())
                {
                    if (IsStarship() && cship.IsStarship())
                        combat = dist < 120e3;
                    else
                        combat = dist < 60e3;
                }
            }

            return combat;
        }

        public void TimeSkip()
        {
            if (CanTimeSkip())
            {
                // go back to regular time before performing the skip:
                Game.SetTimeCompression(1);

                transition_time = 7.5f;
                transition_type = TRAN_TYPE.TRANSITION_TIME_SKIP;
                transition_loc = Location() + Heading() * (Velocity().Length * 4);
                // 2500; //(8*Radius());

                if (RandomHelper.Rand() < 16000)
                    transition_loc += BeamLine() * (2.5 * Radius());
                else
                    transition_loc += BeamLine() * (-2 * Radius());

                if (RandomHelper.Rand() < 8000)
                    transition_loc += LiftLine() * (-1 * Radius());
                else
                    transition_loc += LiftLine() * (1.8 * Radius());

                SetControls(null);
            }

            else if (sim.GetPlayerShip() == this)
            {
                SetAutoNav(true);
            }
        }
        public void DropCam(double time = 10, double range = 0)
        {
            transition_type = TRAN_TYPE.TRANSITION_DROP_CAM;

            if (time > 0)
                transition_time = (float)time;
            else
                transition_time = 10.0f;

            Point offset = Heading() * (Velocity().Length * 5);
            double lateral_offset = 2 * Radius();
            double vertical_offset = Radius();

            if (vertical_offset > 300)
                vertical_offset = 300;

            if (RandomHelper.Rand() < 16000)
                lateral_offset *= -1;

            if (RandomHelper.Rand() < 8000)
                vertical_offset *= -1;

            offset += BeamLine() * lateral_offset;
            offset += LiftLine() * vertical_offset;

            if (range > 0)
                offset *= range;

            transition_loc = Location() + offset;
        }

        public void DeathSpiral()
        {
            if (killer == null)
                killer = new ShipKiller(this);

            foreach (ShipSystem iter in systems)
                iter.PowerOff();

            // transfer arcade velocity to newtonian velocity:
            if ((int)flight_model >= 2)
            {
                velocity += arcade_velocity;
            }

            if (GetIFF() < 100 && !IsGroundUnit())
            {
                RadioTraffic.SendQuickMessage(this, RadioMessage.ACTION.DISTRESS);
            }

            transition_type = TRAN_TYPE.TRANSITION_DEATH_SPIRAL;

            killer.BeginDeathSpiral();

            transition_time = killer.TransitionTime();
            transition_loc = killer.TransitionLoc();
        }
        public void CompleteTransition()
        {
            TRAN_TYPE old_type = transition_type;
            transition_time = 0.0f;
            transition_type = TRAN_TYPE.TRANSITION_NONE;

            switch (old_type)
            {
                case TRAN_TYPE.TRANSITION_NONE:
                case TRAN_TYPE.TRANSITION_DROP_CAM:
                default:
                    return;

                case TRAN_TYPE.TRANSITION_DROP_ORBIT:
                    {
                        SetControls(null);
                        SimRegion dst_rgn = sim.FindNearestTerrainRegion(this);
                        Point dst_loc = Location().OtherHand() * 0.20;
                        dst_loc.X += 6000 * GetElementIndex();
                        dst_loc.Z = TerrainRegion.TERRAIN_ALTITUDE_LIMIT * 0.95;
                        dst_loc += RandomHelper.RandomDirection() * 2e3;

                        sim.RequestHyperJump(this, dst_rgn, dst_loc, TRAN_TYPE.TRANSITION_DROP_ORBIT);

                        ShipStats stats = ShipStats.Find(Name());
                        stats.AddEvent(SimEvent.EVENT.BREAK_ORBIT, dst_rgn.Name());
                    }
                    break;

                case TRAN_TYPE.TRANSITION_MAKE_ORBIT:
                    {
                        SetControls(null);
                        SimRegion dst_rgn = sim.FindNearestSpaceRegion(this);
                        double dist = 200.0e3 + 10.0e3 * GetElementIndex();
                        Point esc_vec = dst_rgn.GetOrbitalRegion().Location() -
                        dst_rgn.GetOrbitalRegion().Primary().Location();

                        esc_vec.Z = -100 * GetElementIndex();
                        esc_vec.Normalize();
                        esc_vec *= -dist;
                        esc_vec += RandomHelper.RandomDirection() * 2e3;

                        sim.RequestHyperJump(this, dst_rgn, esc_vec, TRAN_TYPE.TRANSITION_MAKE_ORBIT);

                        ShipStats stats = ShipStats.Find(Name());
                        stats.AddEvent(SimEvent.EVENT.MAKE_ORBIT, dst_rgn.Name());
                    }
                    break;

                case TRAN_TYPE.TRANSITION_TIME_SKIP:
                    {
                        Instruction navpt = GetNextNavPoint();

                        if (navpt != null)
                        {
                            Point delta = navpt.Location().OtherHand() - Location();
                            Point unit = delta; unit.Normalize();
                            Point trans = delta + unit * -20e3;
                            double dist = trans.Length;
                            double speed = navpt.Speed();

                            if (speed < 50) speed = 500;

                            double etr = dist / speed;

                            sim.ResolveTimeSkip(etr);
                        }
                    }
                    break;

                case TRAN_TYPE.TRANSITION_DEATH_SPIRAL:
                    SetControls(null);
                    transition_type = TRAN_TYPE.TRANSITION_DEAD;
                    break;
            }

        }
        public void SetTransition(double trans_time, TRAN_TYPE trans_type, Point trans_loc)
        {
            transition_time = (float)trans_time;
            transition_type = trans_type;
            transition_loc = trans_loc;
        }

        public double CompassHeading()
        {
            Point heading = Heading();
            double compass_heading = Math.Atan2(Math.Abs(heading.X), heading.Z);

            if (heading.X < 0)
                compass_heading *= -1;

            double result = compass_heading + Math.PI;

            if (result >= 2 * Math.PI)
                result -= 2 * Math.PI;

            return result;
        }

        public double CompassPitch()
        {
            Point heading = Heading();
            return Math.Asin(heading.Y);
        }
        public double AltitudeMSL()
        {
            return Location().Y;
        }
        public double AltitudeAGL()
        {
            if (altitude_agl < -1000)
            {
                Ship pThis = (Ship)this; // cast-away const
                Point loc = Location();

                Terrain terrain = region.GetTerrain();

                if (terrain != null)
                    pThis.altitude_agl = (float)(loc.Y - terrain.Height(loc.X, loc.Z));

                else
                    pThis.altitude_agl = (float)loc.Y;

                if (float.IsInfinity(altitude_agl))
                {
                    pThis.altitude_agl = 0.0f;
                }
            }

            return altitude_agl;
        }
        public double GForce()
        {
            return g_force;
        }

        public virtual void SetupAgility()
        {
            const float ROLL_SPEED = (float)(Math.PI * 0.1500);
            const float PITCH_SPEED = (float)(Math.PI * 0.0250);
            const float YAW_SPEED = (float)(Math.PI * 0.0250);

            drag = design.drag;
            dr_drg = design.roll_drag;
            dp_drg = design.pitch_drag;
            dy_drg = design.yaw_drag;

            if (IsDying())
            {
                drag = 0.0f;
                dr_drg *= 0.25f;
                dp_drg *= 0.25f;
                dy_drg *= 0.25f;
            }

            if (flight_model > 0)
            {
                drag = design.arcade_drag;
                thrust *= 10.0f;
            }

            float yaw_air_factor = 1.0f;

            if (IsAirborne())
            {
                bool grounded = AltitudeAGL() < Radius() / 2;

                if (flight_model > 0)
                {
                    drag *= 2.0f;

                    if (gear != null && gear.GetState() != LandingGear.GEAR_STATE.GEAR_UP)
                        drag *= 2.0f;

                    if (grounded)
                        drag *= 3.0f;
                }

                else
                {
                    if (Class() != CLASSIFICATION.LCA)
                        yaw_air_factor = 0.3f;

                    double rho = GetDensity();
                    double speed = Velocity().Length;

                    agility = design.air_factor * rho * speed - wep_resist;

                    if (grounded && agility < 0)
                        agility = 0;

                    else if (!grounded && agility < 0.5 * design.agility)
                        agility = 0.5 * design.agility;

                    else if (agility > 2 * design.agility)
                        agility = 2 * design.agility;

                    // undercarriage aerodynamic drag
                    if (gear != null && gear.GetState() != LandingGear.GEAR_STATE.GEAR_UP)
                        drag *= 5.0f;

                    // wheel rolling friction
                    if (grounded)
                        drag *= 10.0f;

                    // dead engine drag ;-)
                    if (thrust < 10)
                        drag *= 5.0f;
                }
            }

            else
            {
                agility = design.agility - wep_resist;

                if (agility < 0.5 * design.agility)
                    agility = 0.5 * design.agility;

                if (flight_model == 0)
                    drag = 0.0f;
            }

            float rr = (float)(design.roll_rate * Math.PI / 180);
            float pr = (float)(design.pitch_rate * Math.PI / 180);
            float yr = (float)(design.yaw_rate * Math.PI / 180);

            if (rr == 0) rr = (float)agility * ROLL_SPEED;
            if (pr == 0) pr = (float)agility * PITCH_SPEED;
            if (yr == 0) yr = (float)agility * YAW_SPEED * yaw_air_factor;

            SetAngularRates(rr, pr, yr);
        }

        // FLIGHT CONTROL SYSTEM (FLCS):
        public void ExecFLCSFrame()
        {
            if (flcs != null)
                flcs.ExecSubFrame();
        }
        public void CycleFLCSMode()
        {
            switch (flcs_mode)
            {
                case FLCS_MODE.FLCS_MANUAL: SetFLCSMode(FLCS_MODE.FLCS_HELM); break;
                case FLCS_MODE.FLCS_AUTO: SetFLCSMode(FLCS_MODE.FLCS_MANUAL); break;
                case FLCS_MODE.FLCS_HELM: SetFLCSMode(FLCS_MODE.FLCS_AUTO); break;

                default:
                    if (IsStarship())
                        flcs_mode = FLCS_MODE.FLCS_HELM;
                    else
                        flcs_mode = FLCS_MODE.FLCS_AUTO;
                    break;
            }

            // reset helm heading to compass heading when switching
            // back to helm mode from manual mode:

            if (flcs_mode == FLCS_MODE.FLCS_HELM)
            {
                if (IsStarship())
                {
                    SetHelmHeading(CompassHeading());
                    SetHelmPitch(CompassPitch());
                }
                else
                {
                    flcs_mode = FLCS_MODE.FLCS_AUTO;
                }
            }
        }
        public void SetFLCSMode(FLCS_MODE mode)
        {
            flcs_mode = mode;

            if (IsAirborne())
                flcs_mode = FLCS_MODE.FLCS_MANUAL;

            if (dir != null && dir.Type() < SteerAI.SteerType.SEEKER)
            {
                switch (flcs_mode)
                {
                    case FLCS_MODE.FLCS_MANUAL: director_info = Game.GetText("flcs.manual"); break;
                    case FLCS_MODE.FLCS_AUTO: director_info = Game.GetText("flcs.auto"); break;
                    case FLCS_MODE.FLCS_HELM: director_info = Game.GetText("flcs.helm"); break;
                    default: director_info = Game.GetText("flcs.fault"); break;
                }

                if (flcs == null || !flcs.IsPowerOn())
                    director_info = Game.GetText("flcs.offline");

                else if (IsAirborne())
                    director_info = Game.GetText("flcs.atmospheric");
            }

            if (flcs != null)
                flcs.SetMode(mode);
        }
        public int GetFLCSMode()
        {
            return (int)flcs_mode;
        }
        public override void SetTransX(double t)
        {
            float limit = design.trans_x;

            if (thruster != null)
                limit = (float)thruster.TransXLimit();

            trans_x = (float)t;

            if (trans_x != 0)
            {
                if (trans_x > limit)
                    trans_x = limit;
                else if (trans_x < -limit)
                    trans_x = -limit;

                // reduce thruster efficiency at high fwd speed:
                double vfwd = Point.Dot(cam.vrt(), Velocity());
                double vmag = Math.Abs(vfwd);
                if (vmag > vlimit)
                {
                    if (trans_x > 0 && vfwd > 0 || trans_x < 0 && vfwd < 0)
                        trans_x *= (float)(Math.Pow(vlimit, 4) / Math.Pow(vmag, 4));
                }
            }
        }
        public override void SetTransY(double t)
        {
            float limit = design.trans_y;

            if (thruster != null)
                limit = (float)thruster.TransYLimit();

            trans_y = (float)t;

            if (trans_y != 0)
            {
                double vmag = Velocity().Length;

                if (trans_y > limit)
                    trans_y = limit;
                else if (trans_y < -limit)
                    trans_y = -limit;

                // reduce thruster efficiency at high fwd speed:
                if (vmag > vlimit)
                {
                    double vfwd = Point.Dot(cam.vpn(), Velocity());

                    if (trans_y > 0 && vfwd > 0 || trans_y < 0 && vfwd < 0)
                        trans_y *= (float)(Math.Pow(vlimit, 4) / Math.Pow(vmag, 4));
                }
            }
        }
        public override void SetTransZ(double t)
        {
            float limit = design.trans_z;

            if (thruster != null)
                limit = (float)thruster.TransZLimit();

            trans_z = (float)t;

            if (trans_z != 0)
            {

                if (trans_z > limit)
                    trans_z = limit;
                else if (trans_z < -limit)
                    trans_z = -limit;

                // reduce thruster efficiency at high fwd speed:
                double vfwd = Point.Dot(cam.vup(), Velocity());
                double vmag = Math.Abs(vfwd);
                if (vmag > vlimit)
                {
                    if (trans_z > 0 && vfwd > 0 || trans_z < 0 && vfwd < 0)
                        trans_z *= (float)(Math.Pow(vlimit, 4) / Math.Pow(vmag, 4));
                }
            }
        }

        public bool IsGearDown()
        {
            if (gear != null && gear.GetState() == LandingGear.GEAR_STATE.GEAR_DOWN)
                return true;

            return false;
        }
        public void LowerGear()
        {
            if (gear != null && gear.GetState() != LandingGear.GEAR_STATE.GEAR_DOWN)
            {
                gear.SetState(LandingGear.GEAR_STATE.GEAR_LOWER);
                Scene scene = null;

                if (rep != null)
                    scene = rep.GetScene();

                if (scene != null)
                {
                    for (int i = 0; i < gear.NumGear(); i++)
                    {
                        Solid g = gear.GetGear(i);
                        if (g != null)
                        {
                            if (detail_level == 0)
                                scene.DelGraphic(g);
                            else
                                scene.AddGraphic(g);
                        }
                    }
                }
            }
        }
        public void RaiseGear()
        {
            if (gear != null && gear.GetState() != LandingGear.GEAR_STATE.GEAR_UP)
                gear.SetState(LandingGear.GEAR_STATE.GEAR_RAISE);
        }
        public void ToggleGear()
        {
            if (gear != null)
            {
                if (gear.GetState() == LandingGear.GEAR_STATE.GEAR_UP ||
                        gear.GetState() == LandingGear.GEAR_STATE.GEAR_RAISE)
                {
                    LowerGear();
                }
                else
                {
                    RaiseGear();
                }
            }
        }
        public void ToggleNavlights()
        {
            bool enable = false;

            for (int i = 0; i < navlights.Count; i++)
            {
                if (i == 0)
                    enable = !navlights[0].IsEnabled();

                if (enable)
                    navlights[i].Enable();
                else
                    navlights[i].Disable();
            }
        }

        // WEAPON SYSTEMS:
        public virtual void CheckFriendlyFire()
        {
            // if no weapons, there is no worry about friendly fire...
            if (weapons.Count < 1)
                return;

            // only check once each second
            if (Game.GameTime() - friendly_fire_time < 1000)
                return;

            List<Weapon> w_list = new List<Weapon>();
            int i, j;

            // clear the FF blocked flag on all weapons
            for (i = 0; i < weapons.Count; i++)
            {
                WeaponGroup g = weapons[i];

                for (j = 0; j < g.NumWeapons(); j++)
                {
                    Weapon w = g.GetWeapon(j);
                    w_list.Add(w);
                    w.SetBlockedFriendly(false);
                }
            }

            // for each friendly ship within some kind of weapons range,
            foreach (Contact c in ContactList())
            {
                Ship cship = c.GetShip();
                Shot cshot = c.GetShot();

                if (cship != null && cship != this && (cship.GetIFF() == 0 || cship.GetIFF() == GetIFF()))
                {
                    double range = (cship.Location() - Location()).Length;

                    if (range > 100e3)
                        continue;

                    // check each unblocked weapon to see if it is blocked by that ship
                    foreach (Weapon w in w_list)
                    {
                        if (!w.IsBlockedFriendly())
                            w.SetBlockedFriendly(IsWeaponBlockedFriendly(w, cship));
                    }
                }

                else if (cshot != null && cshot.GetIFF() == GetIFF())
                {
                    double range = (cshot.Location() - Location()).Length;

                    if (range > 30e3)
                        continue;

                    // check each unblocked weapon to see if it is blocked by that shot
                    foreach (Weapon w in w_list)
                    {
                        if (!w.IsBlockedFriendly())
                            w.SetBlockedFriendly(IsWeaponBlockedFriendly(w, cshot));
                    }
                }
            }

            friendly_fire_time = (DWORD)(Game.GameTime() + RandomHelper.Random(0, 500));
        }
        public virtual void CheckFire(bool c) { check_fire = c; }
        public virtual bool CheckFire() { return (check_fire || net_observer_mode) ? true : false; }
        public virtual void SelectWeapon(int n, int w)
        {
            if (n < weapons.Count)
                weapons[n].SelectWeapon(w);
        }
        public virtual bool FireWeapon(int n)
        {
            bool fired = false;

            if (n >= 0 && !CheckFire())
            {
                if (n < 4)
                    trigger[n] = true;

                if (n < weapons.Count)
                {
                    weapons[n].SetTrigger(true);
                    fired = weapons[n].GetTrigger();
                }
            }

            if (!fired && sim.GetPlayerShip() == this)
                Button.PlaySound(Button.SOUNDS.SND_REJECT);

            return fired;
        }
        public virtual bool FirePrimary() { return FireWeapon(primary); }
        public virtual bool FireSecondary() { return FireWeapon(secondary); }
        public virtual bool FireDecoy()
        {
            Shot drone = null;

            if (decoy != null && !CheckFire())
            {
                drone = decoy.Fire();

                if (drone != null)
                {
                    Observe(drone);
                    decoy_list.Add(drone);
                }
            }

            if (sim.GetPlayerShip() == this)
            {
                if (NetGame.IsNetGame())
                {
                    if (decoy != null && decoy.Ammo() < 1)
                        Button.PlaySound(Button.SOUNDS.SND_REJECT);
                }

                else if (drone == null)
                {
                    Button.PlaySound(Button.SOUNDS.SND_REJECT);
                }
            }

            return drone != null;
        }
        public virtual void CyclePrimary()
        {
            if (weapons.IsEmpty())
                return;

            if (IsDropship() && primary < weapons.Count)
            {
                WeaponGroup p = weapons[primary];
                Weapon w = p.GetSelected();

                if (w != null && w.GetTurret() != null)
                {
                    p.SetFiringOrders(Weapon.Orders.POINT_DEFENSE);
                }
            }

            int n = primary + 1;
            while (n != primary)
            {
                if (n >= weapons.Count)
                    n = 0;

                if (weapons[n].IsPrimary())
                {
                    weapons[n].SetFiringOrders(Weapon.Orders.MANUAL);
                    break;
                }

                n++;
            }

            primary = n;
        }
        public virtual void CycleSecondary()
        {
            if (weapons.IsEmpty())
                return;

            int n = secondary + 1;
            while (n != secondary)
            {
                if (n >= weapons.Count)
                    n = 0;

                if (weapons[n].IsMissile())
                    break;

                n++;
            }

            secondary = n;

            // automatically switch sensors to appropriate mode:
            if (IsAirborne())
            {
                Weapon missile = GetSecondary();
                if (missile != null && missile.CanTarget(CLASSIFICATION.GROUND_UNITS))
                    SetSensorMode(Sensor.Mode.GM);
                else if (sensor != null && sensor.GetMode() == Sensor.Mode.GM)
                    SetSensorMode(Sensor.Mode.STD);
            }
        }
        public virtual Weapon GetPrimary()
        {
            if (weapons.Count > primary)
                return weapons[primary].GetSelected();
            return null;
        }
        public virtual Weapon GetSecondary()
        {
            if (weapons.Count > secondary)
                return weapons[secondary].GetSelected();
            return null;
        }

        public virtual Weapon GetWeaponByIndex(int n)
        {
            for (int i = 0; i < weapons.Count; i++)
            {
                WeaponGroup g = weapons[i];

                List<Weapon> wlist = g.GetWeapons();
                for (int j = 0; j < wlist.Count; j++)
                {
                    Weapon w = wlist[j];

                    if (w.GetIndex() == n)
                    {
                        return w;
                    }
                }
            }

            return null;
        }
        public virtual WeaponGroup GetPrimaryGroup()
        {
            if (weapons.Count > primary)
                return weapons[primary];
            return null;
        }
        public virtual WeaponGroup GetSecondaryGroup()
        {
            if (weapons.Count > secondary)
                return weapons[secondary];
            return null;
        }
        public virtual Weapon GetDecoy()
        {
            return decoy;
        }
        public virtual List<Shot> GetActiveDecoys()
        {
            return decoy_list;
        }
        public virtual void AddActiveDecoy(Drone drone)
        {
            if (drone != null)
            {
                Observe(drone);
                decoy_list.Add(drone);
            }
        }
        public virtual int[] GetLoadout() { return loadout; }

        public List<Shot> GetThreatList()
        {
            return threat_list;
        }
        public void AddThreat(Shot s)
        {
            if (!threat_list.Contains(s))
            {
                Observe(s);
                threat_list.Add(s);
            }
        }
        public void DropThreat(Shot s)
        {
            if (threat_list.Contains(s))
            {
                threat_list.Remove(s);
            }
        }

        public virtual bool Update(SimObject obj)
        {
            if (obj == ward)
                ward = null;

            if (obj == target)
            {
                target = null;
                subtarget = null;
            }

            if (obj == carrier)
            {
                carrier = null;
                dock = null;
                inbound = null;
            }

            if (obj.Type() == (int)SimObject.TYPES.SIM_SHOT ||
                    obj.Type() == (int)SimObject.TYPES.SIM_DRONE)
            {
                Shot s = (Shot)obj;

                if (sensor_drone == s)
                    sensor_drone = null;

                if (decoy_list.Contains(s))
                    decoy_list.Remove(s);

                if (threat_list.Contains(s))
                    threat_list.Remove(s);
            }

            return thisSimObserver.Update(obj);
        }
        public virtual string GetObserverName() { return name; }

        public virtual int GetMissileEta(int index)
        {
            if (index >= 0 && index < 4)
                return missile_eta[index];

            return 0;
        }

        public virtual void SetMissileEta(int id, int eta)
        {
            int index = -1;

            // are we tracking this missile's eta?
            for (int i = 0; i < 4; i++)
                if (id == missile_id[i])
                    index = i;

            // if not, can we find an open slot to track it in?
            if (index < 0)
            {
                for (int i = 0; i < 4 && index < 0; i++)
                {
                    if (missile_eta[i] == 0)
                    {
                        index = i;
                        missile_id[i] = id;
                    }
                }
            }

            // track the eta:
            if (index >= 0 && index < 4)
            {
                if (eta > 3599)
                    eta = 3599;

                missile_eta[index] = (BYTE)eta;
            }
        }

        public virtual WeaponDesign GetPrimaryDesign()
        {
            if (weapons.Count > primary)
                return (WeaponDesign)weapons[primary].GetSelected().Design();
            return null;
        }
        public virtual WeaponDesign GetSecondaryDesign()
        {
            if (weapons.Count > secondary)
                return (WeaponDesign)weapons[secondary].GetSelected().Design();
            return null;
        }

        public virtual void SetTarget(SimObject targ, ShipSystem sub = null, bool from_net = false)
        {
            if (targ != null && targ.Type() == (int)SimObject.TYPES.SIM_SHIP)
            {
                Ship targ_ship = (Ship)targ;

                if (targ_ship != null && targ_ship.IsNetObserver())
                    return;
            }

            if (target != targ)
            {
                // DON'T IGNORE TARGET, BECAUSE IT MAY BE IN THREAT LIST
                target = targ;
                if (target != null) Observe(target);

                if (sim != null && target != null)
                    sim.ProcessEventTrigger(MissionEvent.EVENT_TRIGGER.TRIGGER_TARGET, 0, target.Name());
            }

            subtarget = sub;

            foreach (WeaponGroup weapon in weapons)
            {
                if (weapon.GetFiringOrders() != Weapon.Orders.POINT_DEFENSE)
                {
                    weapon.SetTarget(target, subtarget);

                    if (sub != null || !IsStarship())
                        weapon.SetSweep(Weapon.Sweep.SWEEP_NONE);
                    else
                        weapon.SetSweep(Weapon.Sweep.SWEEP_TIGHT);
                }
            }

            if (!from_net && NetGame.GetInstance() != null)
                NetUtil.SendObjTarget(this);

            // track engagement:
            if (target != null && target.Type() == (int)SimObject.TYPES.SIM_SHIP)
            {
                Element elem = GetElement();
                Element tgt_elem = ((Ship)target).GetElement();

                if (elem != null)
                    elem.SetAssignment(tgt_elem);
            }
        }
        public virtual SimObject GetTarget() { return target; }
        public virtual ShipSystem GetSubTarget() { return subtarget; }
        public virtual void CycleSubTarget(int dir = 1)
        {
            if (target == null || target.Type() != (int)SimObject.TYPES.SIM_SHIP)
                return;

            Ship tgt = (Ship)target;

            if (tgt.IsDropship())
                return;

            ShipSystem subtgt = null;

            if (dir > 0)
            {
                bool latch = (subtarget == null);
                foreach (ShipSystem sys in tgt.Systems())
                {
                    if (sys.Type() == ShipSystem.CATEGORY.COMPUTER || // computers are not targetable
                            sys.Type() == ShipSystem.CATEGORY.SENSOR)     // sensors   are not targetable
                        continue;

                    if (sys == subtarget)
                    {
                        latch = true;
                    }

                    else if (latch)
                    {
                        subtgt = sys;
                        break;
                    }
                }
            }
            else
            {
                ShipSystem prev = null;

                foreach (ShipSystem sys in tgt.Systems())
                {
                    if (sys.Type() == ShipSystem.CATEGORY.COMPUTER || // computers are not targetable
                            sys.Type() == ShipSystem.CATEGORY.SENSOR)     // sensors   are not targetable
                        continue;

                    if (sys == subtarget)
                    {
                        subtgt = prev;
                        break;
                    }

                    prev = sys;
                }

                if (subtarget == null)
                    subtgt = prev;
            }

            SetTarget(tgt, subtgt);
        }
        public virtual void DropTarget()
        {
            target = null;
            subtarget = null;

            SetTarget(target, subtarget);
        }
        public virtual void LockTarget(TYPES type = TYPES.SIM_SHIP, bool closest = false, bool hostile = false)
        {
            if (sensor != null)
                SetTarget(sensor.LockTarget(type, closest, hostile));
        }



        public virtual void LockTarget(SimObject candidate)
        {
            if (sensor != null)
                SetTarget(sensor.LockTarget(candidate));
            else
                SetTarget(candidate);
        }
        public virtual bool IsTracking(SimObject tgt)
        {
            if (tgt != null && sensor != null)
                return sensor.IsTracking(tgt);

            return false;
        }
        public virtual bool GetTrigger(int i)
        {
            if (i >= 0)
            {
                if (i < 4)
                    return trigger[i];

                else if (i < weapons.Count)
                    return weapons[i].GetTrigger();
            }

            return false;
        }
        public virtual void SetTrigger(int i)
        {
            if (i >= 0 && !CheckFire())
            {
                if (i < 4)
                    trigger[i] = true;

                if (i < weapons.Count)
                    weapons[i].SetTrigger();
            }
        }

        public Ship GetWard() { return ward; }
        public void SetWard(Ship s)
        {
            if (ward == s)
                return;

            ward = s;

            if (ward != null)
                Observe(ward);
        }

        // SHIELD SYSTEMS:
        public virtual double InflictDamage(double damage,
                                          Shot shot = null,
                                          int hit_type = 3,
                                          Point impact = new Point())
        {
            double damage_applied = 0;

            if (Game.Paused() || IsNetObserver() || IsInvulnerable())
                return damage_applied;

            if (Integrity() == 0) // already dead?
                return damage_applied;

            const double MAX_SHAKE = 7;
            double hull_damage = damage;
            bool hit_shield = (hit_type & HIT_SHIELD) != 0;
            bool hit_hull = (hit_type & HIT_HULL) != 0;
            bool hit_turret = (hit_type & HIT_TURRET) != 0;

            if (impact == new Point(0, 0, 0))
                impact = Location();

            if (hit_shield && ShieldStrength() > 0)
            {
                hull_damage = shield.DeflectDamage(shot, damage);

                if (shot != null)
                {
                    if (shot.IsBeam())
                    {
                        if (design.beam_hit_sound_resource != null)
                        {
                            if (Game.RealTime() - last_beam_time > 400)
                            {
                                Sound s = design.beam_hit_sound_resource.Duplicate();
                                s.SetLocation(impact);
                                s.SetVolume(AudioConfig.EfxVolume());
                                s.Play();

                                last_beam_time = Game.RealTime();
                            }
                        }
                    }

                    else
                    {
                        if (design.bolt_hit_sound_resource != null)
                        {
                            if (Game.RealTime() - last_bolt_time > 400)
                            {
                                Sound s = design.bolt_hit_sound_resource.Duplicate();
                                s.SetLocation(impact);
                                s.SetVolume(AudioConfig.EfxVolume());
                                s.Play();

                                last_bolt_time = Game.RealTime();
                            }
                        }
                    }
                }
            }

            if (hit_hull)
            {
                hull_damage = InflictSystemDamage(hull_damage, shot, impact);

                WeaponDesign.DAMAGE damage_type = WeaponDesign.DAMAGE.DMG_NORMAL;

                if (shot != null && shot.Design() != null)
                    damage_type = shot.Design().damage_type;

                if (damage_type == WeaponDesign.DAMAGE.DMG_NORMAL)
                {
                    damage_applied = hull_damage;
                    base.InflictDamage(damage_applied, 0);
                    NetUtil.SendObjDamage(this, damage_applied, shot);
                }
            }

            else if (hit_turret)
            {
                hull_damage = InflictSystemDamage(hull_damage, shot, impact) * 0.3;

                WeaponDesign.DAMAGE damage_type = WeaponDesign.DAMAGE.DMG_NORMAL;

                if (shot != null && shot.Design() != null)
                    damage_type = shot.Design().damage_type;

                if (damage_type == WeaponDesign.DAMAGE.DMG_NORMAL)
                {
                    damage_applied = hull_damage;
                    base.InflictDamage(damage_applied, 0);
                    NetUtil.SendObjDamage(this, damage_applied, shot);
                }
            }

            // shake by percentage of maximum damage
            double newshake = 50 * damage / design.integrity;

            if (shake < MAX_SHAKE) shake += (float)newshake;
            if (shake > MAX_SHAKE) shake = (float)MAX_SHAKE;

            // start fires as needed:
            if ((IsStarship() || IsGroundUnit() || RandomHelper.RandomChance(1, 3)) && hit_hull && damage_applied > 0)
            {
                int old_integrity = (int)((integrity + damage_applied) / design.integrity * 10);
                int new_integrity = (int)((integrity) / design.integrity * 10);

                if (new_integrity < 5 && new_integrity < old_integrity)
                {
                    // need accurate hull impact for starships,
                    if (rep != null)
                    {
                        Point detonation = impact * 2 - Location();
                        Point direction = Location() - detonation;
                        double distance = direction.Normalize() * 3;
                        rep.CheckRayIntersection(detonation, direction, distance, impact);

                        // pull fire back into hull a bit:
                        direction = Location() - impact;
                        impact += direction * 0.2;

                        float scale = (float)design.scale;

                        if (IsDropship())
                            sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.SMOKE_TRAIL, 0.01f * scale, 0.5f * scale, region, this);
                        else
                            sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.HULL_FIRE, 0.10f * scale, scale, region, this);
                    }
                }
            }

            return damage_applied;
        }


        public virtual double InflictSystemDamage(double damage, Shot shot, Point impact)
        {
            if (IsNetObserver())
                return 0;

            // find the system that is closest to the impact point:
            ShipSystem system = null;
            double distance = 1e6;
            double blast_radius = 0;
            WeaponDesign.DAMAGE dmg_type = 0;

            if (shot != null)
                dmg_type = shot.Design().damage_type;

            bool dmg_normal = dmg_type == WeaponDesign.DAMAGE.DMG_NORMAL;
            bool dmg_power = dmg_type == WeaponDesign.DAMAGE.DMG_POWER;
            bool dmg_emp = dmg_type == WeaponDesign.DAMAGE.DMG_EMP;
            double to_level = 0;

            if (dmg_power)
            {
                to_level = 1 - damage / 1e4;

                if (to_level < 0)
                    to_level = 0;
            }

            // damage caused by weapons applies to closest system:
            if (shot != null)
            {
                if (shot.IsMissile())
                    blast_radius = 300;

                foreach (ShipSystem candidate in systems)
                {
                    double sysrad = candidate.Radius();

                    if (dmg_power)
                        candidate.DrainPower(to_level);

                    if (sysrad > 0 || dmg_emp && candidate.IsPowerCritical())
                    {
                        double test_distance = (impact - candidate.MountLocation()).Length;

                        if ((test_distance - blast_radius) < sysrad || dmg_emp && candidate.IsPowerCritical())
                        {
                            if (test_distance < distance)
                            {
                                system = candidate;
                                distance = test_distance;
                            }
                        }
                    }
                }

                // if a system was in range of the blast, assess the damage:
                if (system != null)
                {
                    double hull_damage = damage * system.HullProtection();
                    double sys_damage = damage - hull_damage;
                    double avail = system.Availability();

                    if (dmg_normal || system.IsPowerCritical() && dmg_emp)
                    {
                        system.ApplyDamage(sys_damage);
                        NetUtil.SendSysDamage(this, system, sys_damage);

                        master_caution = true;

                        if (dmg_normal)
                        {
                            if (sys_damage < 100)
                                damage -= sys_damage;
                            else
                                damage -= 100;
                        }

                        if (system.GetExplosionType() != 0 && (avail - system.Availability()) >= 50)
                        {
                            float scale = design.explosion_scale;
                            if (scale <= 0)
                                scale = design.scale;

                            sim.CreateExplosion(system.MountLocation(), Velocity() * 0.7f, system.GetExplosionType(), 0.2f * scale, scale, region, this, system);
                        }
                    }
                }
            }

            // damage caused by collision applies to all systems:
            else
            {
                // ignore incidental bumps:
                if (damage < 100)
                    return damage;

                foreach (ShipSystem sys in systems)
                {
                    if (RandomHelper.Rand() > 24000)
                    {
                        double base_damage = 33.0 + RandomHelper.Rand() / 1000.0;
                        double sys_damage = base_damage * (1.0 - sys.HullProtection());
                        sys.ApplyDamage(sys_damage);
                        NetUtil.SendSysDamage(this, system, sys_damage);
                        damage -= sys_damage;

                        master_caution = true;
                    }
                }

                // just in case this ship has lots of systems...
                if (damage < 0)
                    damage = 0;
            }

            // return damage remaining
            return damage;
        }

        public virtual void InflictNetDamage(double damage, Shot shot = null) { throw new NotImplementedException(); }
        public virtual void InflictNetSystemDamage(ShipSystem system, double damage, BYTE type) { throw new NotImplementedException(); }
        public virtual void SetNetSystemStatus(ShipSystem system, ShipSystem.STATUS status, int power, int reactor, double avail)
        {
            if (system != null && !IsNetObserver())
            {
                if (system.GetPowerLevel() != power)
                    system.SetPowerLevel(power);

                if (system.GetSourceIndex() != reactor)
                {
                    ShipSystem s = GetSystem(reactor);

                    if (s != null && s.Type() == ShipSystem.CATEGORY.POWER_SOURCE)
                    {
                        PowerSource reac = (PowerSource)s;
                        reac.AddClient(system);
                    }
                }

                if (system.Status() != status)
                {
                    if (status == ShipSystem.STATUS.MAINT)
                    {
                        foreach (SimElements.Component c in system.GetComponents())
                        {
                            if (c.Status() < SimElements.Component.STATUS.NOMINAL && c.Availability() < 75)
                            {
                                if (c.SpareCount() != 0 &&
                                        c.ReplaceTime() <= 300 &&
                                        (c.Availability() < 50 ||
                                            c.ReplaceTime() < c.RepairTime()))
                                {

                                    c.Replace();
                                }

                                else if (c.Availability() >= 50 || c.NumJerried() < 5)
                                {
                                    c.Repair();
                                }
                            }
                        }

                        RepairSystem(system);
                    }
                }

                if (system.Availability() < avail)
                {
                    system.SetNetAvail(avail);
                }
                else
                {
                    system.SetNetAvail(-1);
                }
            }
        }
        public virtual void SetIntegrity(float n) { integrity = n; }

        public virtual void Destroy() { throw new NotImplementedException(); }
        public virtual int ShieldStrength()
        {
            if (shield == null) return 0;

            return (int)shield.ShieldLevel();
        }
        public virtual int HullStrength()
        {
            if (design != null)
                return (int)(Integrity() / design.integrity * 100);

            return 10;
        }

        static DWORD ff_warn_time = 0;
        public virtual int HitBy(Shot shot, ref Point impact)
        {
            if (shot.Owner() == this || IsNetObserver())
                return HIT_NOTHING;

            if (shot.IsFlak())
                return HIT_NOTHING;

            if (InTransition())
                return HIT_NOTHING;

            Point shot_loc = shot.Location();
            Point delta = shot_loc - Location();
            double dlen = delta.Length;

            Point hull_impact = new Point();
            int hit_type = HIT_NOTHING;
            double dscale = 1;
            float scale = design.explosion_scale;
            Weapon wep = null;

            if (!shot.IsMissile() && !shot.IsBeam())
            {
                if (dlen > Radius() * 2)
                    return HIT_NOTHING;
            }

            if (scale <= 0)
                scale = design.scale;

            if (shot.Owner() != null)
            {
                ShipDesign owner_design = shot.Owner().Design();
                if (owner_design != null && owner_design.scale < scale)
                    scale = (float)owner_design.scale;
            }


            // MISSILE PROCESSING ------------------------------------------------

            if (shot.IsMissile() && rep != null)
            {
                if (dlen < rep.Radius())
                {
                    hull_impact = impact = shot_loc;

                    hit_type = CheckShotIntersection(shot, out impact, out hull_impact, out wep);

                    if (hit_type != 0)
                    {
                        if (shot.Damage() > 0)
                        {
                            Explosion.ExplosionType flash = Explosion.ExplosionType.HULL_FLASH;

                            if ((hit_type & HIT_SHIELD) != 0)
                                flash = Explosion.ExplosionType.SHIELD_FLASH;

                            sim.CreateExplosion(impact, Velocity(), flash, 0.3f * scale, scale, region);
                            sim.CreateExplosion(impact, new Point(), Explosion.ExplosionType.SHOT_BLAST, 2.0f, scale, region);
                        }
                    }
                }

                if (hit_type == HIT_NOTHING && shot.IsArmed())
                {
                    SeekerAI seeker = (SeekerAI)shot.GetDirector();

                    // if the missile overshot us, take damage proportional to distance
                    double damage_radius = shot.Design().lethal_radius;
                    if (dlen < (damage_radius + Radius()))
                    {
                        if (seeker != null && seeker.Overshot())
                        {
                            dscale = 1.0 - (dlen / (damage_radius + Radius()));

                            if (dscale > 1)
                                dscale = 1;

                            if (ShieldStrength() > 5)
                            {
                                hull_impact = impact = shot_loc;

                                if (shot.Damage() > 0)
                                {
                                    if (shieldRep != null)
                                        shieldRep.Hit(impact, shot, shot.Damage() * dscale);
                                    sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.SHIELD_FLASH, 0.20f * scale, scale, region);
                                    sim.CreateExplosion(impact, new Point(), Explosion.ExplosionType.SHOT_BLAST, 20.0f * scale, scale, region);
                                }

                                hit_type = HIT_BOTH;
                            }
                            else
                            {
                                hull_impact = impact = shot_loc;

                                if (shot.Damage() > 0)
                                {
                                    sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.HULL_FLASH, 0.30f * scale, scale, region);
                                    sim.CreateExplosion(impact, new Point(), Explosion.ExplosionType.SHOT_BLAST, 20.0f * scale, scale, region);
                                }

                                hit_type = HIT_HULL;
                            }
                        }
                    }
                }
            }

            // ENERGY WEP PROCESSING ---------------------------------------------

            else
            {
                hit_type = CheckShotIntersection(shot, out impact, out hull_impact, out wep);

                // impact:
                if (hit_type != 0)
                {

                    if ((hit_type & HIT_SHIELD) != 0)
                    {
                        if (shieldRep != null)
                            shieldRep.Hit(impact, shot, shot.Damage());
                        sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.SHIELD_FLASH, 0.20f * scale, scale, region);
                    }

                    else
                    {
                        if (shot.IsBeam())
                            sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.BEAM_FLASH, 0.30f * scale, scale, region);
                        else
                            sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.HULL_FLASH, 0.30f * scale, scale, region);

                        if (IsStarship())
                        {
                            Point burst_vel = hull_impact - Location();
                            burst_vel.Normalize();
                            burst_vel *= Radius() * 0.5;
                            burst_vel += Velocity();

                            sim.CreateExplosion(hull_impact, burst_vel, Explosion.ExplosionType.HULL_BURST, 0.50f * scale, scale, region, this);
                        }
                    }
                }
            }

            // DAMAGE RESOLUTION -------------------------------------------------

            if (hit_type != HIT_NOTHING && shot.IsArmed())
            {
                double effective_damage = shot.Damage() * dscale;

                // FRIENDLY FIRE --------------------------------------------------

                if (shot.Owner() != null)
                {
                    Ship s = (Ship)shot.Owner();

                    if (!IsRogue() && s.GetIFF() == GetIFF() &&
                            s.GetDirector() != null && (int)s.GetDirector().Type() < 1000)
                    {
                        bool was_rogue = s.IsRogue();

                        // only count beam hits once
                        if (shot.Damage() != 0 && !shot.HitTarget() && GetFriendlyFireLevel() > 0)
                        {
                            int penalty = 1;

                            if (shot.IsBeam()) penalty = 5;
                            else if (shot.IsDrone()) penalty = 7;

                            if (s.GetTarget() == this) penalty *= 3;

                            s.IncFriendlyFire(penalty);
                        }

                        effective_damage *= GetFriendlyFireLevel();

                        if (Class() > CLASSIFICATION.DRONE && s.Class() > CLASSIFICATION.DRONE)
                        {
                            if (s.IsRogue() && !was_rogue)
                            {
                                RadioMessage warn = new RadioMessage(s, this, RadioMessage.ACTION.DECLARE_ROGUE);
                                RadioTraffic.Transmit(warn);
                            }
                            else if (!s.IsRogue() && (Game.GameTime() - ff_warn_time) > 5000)
                            {
                                ff_warn_time = (DWORD)Game.GameTime();

                                RadioMessage warn = null;
                                if (s.GetTarget() == this)
                                    warn = new RadioMessage(s, this, RadioMessage.ACTION.WARN_TARGETED);
                                else
                                    warn = new RadioMessage(s, this, RadioMessage.ACTION.WARN_ACCIDENT);

                                RadioTraffic.Transmit(warn);
                            }
                        }
                    }
                }

                if (effective_damage > 0)
                {
                    if (!shot.IsBeam() && shot.Design().damage_type == WeaponDesign.DAMAGE.DMG_NORMAL)
                    {
                        ApplyTorque(shot.Velocity() * (float)effective_damage * 1e-6f);
                    }

                    if (!NetGame.IsNetGameClient())
                    {
                        InflictDamage(effective_damage, shot, hit_type, hull_impact);
                    }
                }
            }

            return hit_type;
        }
        public override bool CollidesWith(Physical o)
        {
            // bounding spheres test:
            Point delta_loc = Location() - o.Location();
            if (delta_loc.Length > radius + o.Radius())
                return false;

            if (o.Rep() == null)
                return true;

            for (int i = 0; i < detail.NumModels(detail_level); i++)
            {
                Graphic g = detail.GetRep(detail_level, i);

                if (o.Type() == (int)SimObject.TYPES.SIM_SHIP)
                {
                    Ship o_ship = (Ship)o;
                    int o_det = o_ship.detail_level;

                    for (int j = 0; j < o_ship.detail.NumModels(o_det); j++)
                    {
                        Graphic o_g = o_ship.detail.GetRep(o_det, j);

                        if (g.CollidesWith(o_g))
                            return true;
                    }
                }
                else
                {
                    // representation collision test (will do bounding spheres first):
                    if (g.CollidesWith(o.Rep()))
                        return true;
                }
            }

            return false;
        }

        // SENSORS AND VISIBILITY:
        public virtual int GetContactID() { return contact_id; }
        public virtual int GetIFF() { return IFF_code; }
        public virtual void SetIFF(int iff)
        {
            IFF_code = iff;

            if (hangar != null)
                hangar.SetAllIFF(iff);

            DropTarget();

            if (dir != null && (int)dir.Type() >= 1000)
            {
                SteerAI ai = (SteerAI)dir;
                ai.DropTarget();
            }
        }
        public virtual Color MarkerColor()
        {
            return IFFColor(IFF_code);
        }

        public static Color IFFColor(int iff)
        {
            Color c;

            switch (iff)
            {
                case 0:  // NEUTRAL, NON-COMBAT
                    c = new Color32(192, 192, 192, 255);
                    break;

                case 1:  // TERELLIAN ALLIANCE
                    c = new Color32(70, 70, 220, 255);
                    break;

                case 2:  // MARAKAN HEGEMONY
                    c = new Color32(220, 20, 20, 255);
                    break;

                case 3:  // BROTHERHOOD OF IRON
                    c = new Color32(200, 180, 20, 255);
                    break;

                case 4:  // ZOLON EMPIRE
                    c = new Color32(20, 200, 20, 255);
                    break;

                case 5:
                    c = new Color32(128, 0, 128, 255);
                    break;

                case 6:
                    c = new Color32(40, 192, 192, 255);
                    break;

                default:
                    c = new Color32(128, 128, 128, 255);
                    break;
            }

            return c;
        }
        public virtual void DoEMCON()
        {
            foreach (ShipSystem s in systems)
            {
                s.DoEMCON(emcon);
            }

            old_emcon = emcon;
        }
        public virtual double PCS()
        {
            double e_factor = design.e_factor[emcon - 1];

            if (IsAirborne() && !IsGroundUnit())
            {
                if (AltitudeAGL() < 40)
                    return 0;

                if (AltitudeAGL() < 200)
                {
                    double clutter = AltitudeAGL() / 200;
                    return clutter * e_factor;
                }
            }

            return e_factor * pcs;
        }
        public virtual double ACS()
        {
            if (IsAirborne() && !IsGroundUnit())
            {
                if (AltitudeAGL() < 40)
                    return 0;

                if (AltitudeAGL() < 200)
                {
                    double clutter = AltitudeAGL() / 200;
                    return clutter * acs;
                }
            }

            return acs;
        }
        public int NumContacts()   // actual sensor contacts
        {
            // cast-away const:
            return ((Ship)this).ContactList().Count;
        }
        public List<Contact> ContactList()
        {
            if (region != null)
                return region.TrackList(GetIFF());

            return new List<Contact>();
        }
        public virtual Sensor.Mode GetSensorMode()
        {
            if (sensor != null)
                return sensor.GetMode();

            return 0;
        }
        public virtual void SetSensorMode(Sensor.Mode mode)
        {
            if (sensor != null)
                sensor.SetMode((Sensor.Mode)mode);
        }

        public virtual void LaunchProbe()
        {
            if (net_observer_mode)
                return;

            if (sensor_drone != null)
            {
                sensor_drone = null;
            }

            if (probe != null)
            {
                sensor_drone = (Drone)probe.Fire();

                if (sensor_drone != null)
                    Observe(sensor_drone);

                else if (sim.GetPlayerShip() == this)
                    Button.PlaySound(Button.SOUNDS.SND_REJECT);
            }
        }
        public virtual Weapon GetProbeLauncher() { return probe; }
        public virtual Drone GetProbe() { return sensor_drone; }
        public virtual void SetProbe(Drone d)
        {
            if (sensor_drone != d)
            {
                sensor_drone = d;

                if (sensor_drone != null)
                    Observe(sensor_drone);
            }
        }
        public virtual int GetEMCON() { return emcon; }
        public virtual void SetEMCON(int e, bool from_net = false)
        {
            if (e < 1) emcon = 1;
            else if (e > 3) emcon = 3;
            else emcon = (BYTE)e;

            if (emcon != old_emcon && !from_net && NetGame.GetInstance() != null)
                NetUtil.SendObjEmcon(this);
        }
        public virtual Contact FindContact(SimObject s)
        {
            if (s == null) return null;

            foreach (Contact c in ((Ship)this).ContactList())
            {

                if (c.GetShip() == s)
                    return c;

                if (c.GetShot() == s)
                    return c;
            }

            return null;
        }
        public override bool IsHostileTo(SimObject o)
        {
            if (o != null)
            {
                if (IsRogue())
                    return true;

                if (o.Type() == (int)SimObject.TYPES.SIM_SHIP)
                {
                    Ship s = (Ship)o;

                    if (s.IsRogue())
                        return true;

                    if (GetIFF() == 0)
                    {
                        if (s.GetIFF() > 1)
                            return true;
                    }
                    else
                    {
                        if (s.GetIFF() > 0 && s.GetIFF() != GetIFF())
                            return true;
                    }
                }

                else if (o.Type() == (int)SimObject.TYPES.SIM_SHOT || o.Type() == (int)SimObject.TYPES.SIM_DRONE)
                {
                    Shot s = (Shot)o;

                    if (GetIFF() == 0)
                    {
                        if (s.GetIFF() > 1)
                            return true;
                    }
                    else
                    {
                        if (s.GetIFF() > 0 && s.GetIFF() != GetIFF())
                            return true;
                    }
                }
            }

            return false;
        }

        // GENERAL ACCESSORS:
        public string Registry() { return regnum; }
        public void SetName(string ident) { name = ident; }
        public ShipDesign Design() { return design; }
        public string Abbreviation()
        {
            return design.abrv;
        }
        public string DesignName()
        {
            return design.DisplayName();
        }
        public string DesignFileName()
        {
            return design.filename;
        }
        public static string ClassName(CLASSIFICATION c)
        {
            return ShipDesign.ClassName((int)c);
        }
        public static CLASSIFICATION ClassForName(string name)
        {
            return ShipDesign.ClassForName(name);
        }
        public string ClassName()
        {
            return ShipDesign.ClassName((int)design.type);
        }
        public CLASSIFICATION Class() { throw new NotImplementedException(); }
        public bool IsGroundUnit()
        {
            return (design.type & CLASSIFICATION.GROUND_UNITS) != 0 ? true : false;
        }
        public bool IsStarship()
        {
            return (design.type & CLASSIFICATION.STARSHIPS) != 0 ? true : false;
        }
        public bool IsDropship()
        {
            return (design.type & CLASSIFICATION.DROPSHIPS) != 0 ? true : false;
        }
        public bool IsStatic()
        {
            return design.type >= CLASSIFICATION.STATION;
        }
        public bool IsRogue()
        {
            return ff_count >= 50;
        }
        public void SetRogue(bool r = true)
        {
            bool rogue = IsRogue();

            ff_count = r ? 1000 : 0;

            if (!rogue && IsRogue())
            {
                ErrLogger.PrintLine("Ship '%s' has been made rogue\n", Name());
            }
            else if (rogue && !IsRogue())
            {
                ErrLogger.PrintLine("Ship '%s' is no longer rogue\n", Name());
            }
        }
        public int GetFriendlyFire() { return ff_count; }
        public void SetFriendlyFire(int f)
        {
            bool rogue = IsRogue();

            ff_count = f;

            if (!rogue && IsRogue())
            {
                ErrLogger.PrintLine("Ship '%s' has been made rogue with ff_count = %d\n", Name(), ff_count);
            }
            else if (rogue && !IsRogue())
            {
                ErrLogger.PrintLine("Ship '%s' is no longer rogue\n", Name());
            }
        }
        public void IncFriendlyFire(int f = 1)
        {
            if (f > 0)
            {
                bool rogue = IsRogue();

                ff_count += f;

                if (!rogue && IsRogue())
                {
                    ErrLogger.PrintLine("Ship '%s' has been made rogue with ff_count = %d\n", Name(), ff_count);
                }
            }
        }
        public double Agility() { return agility; }
        public DWORD MissionClock()
        {
            if (launch_time > 0)
                return (DWORD)(Game.GameTime() + 1 - launch_time);

            return 0;
        }

        public Graphic Cockpit()
        {
            return cockpit;
        }
        public void ShowCockpit()
        {
            if (cockpit != null)
            {
                cockpit.Show();

                foreach (WeaponGroup g in weapons)
                {
                    foreach (Weapon w in g.GetWeapons())
                    {
                        Solid turret = w.GetTurret();
                        if (turret != null)
                        {
                            turret.Show();

                            Solid turret_base = w.GetTurretBase();
                            if (turret_base != null)
                                turret_base.Show();
                        }

                        if (w.IsMissile())
                        {
                            for (int i = 0; i < w.Ammo(); i++)
                            {
                                Solid store = w.GetVisibleStore(i);
                                if (store != null)
                                {
                                    store.Show();
                                }
                            }
                        }
                    }
                }
            }
        }
        public void HideCockpit()
        {
            if (cockpit != null)
                cockpit.Hide();
        }
        public int Value()
        {
            return Value(design.type);
        }
        public double AIValue()
        {
            int i = 0;
            double value = 0;

            for (i = 0; i < reactors.Count; i++)
            {
                PowerSource r = reactors[i];
                value += r.Value();
            }

            for (i = 0; i < drives.Count; i++)
            {
                Drive d = drives[i];
                value += d.Value();
            }

            for (i = 0; i < weapons.Count; i++)
            {
                WeaponGroup w = weapons[i];
                value += w.Value();
            }

            return value;
        }
        public static int Value(CLASSIFICATION type)
        {
            int value = 0;

            switch (type)
            {
                case CLASSIFICATION.DRONE: value = 10; break;
                case CLASSIFICATION.FIGHTER: value = 20; break;
                case CLASSIFICATION.ATTACK: value = 40; break;
                case CLASSIFICATION.LCA: value = 50; break;

                case CLASSIFICATION.COURIER: value = 100; break;
                case CLASSIFICATION.CARGO: value = 100; break;
                case CLASSIFICATION.CORVETTE: value = 100; break;
                case CLASSIFICATION.FREIGHTER: value = 250; break;
                case CLASSIFICATION.FRIGATE: value = 200; break;
                case CLASSIFICATION.DESTROYER: value = 500; break;
                case CLASSIFICATION.CRUISER: value = 800; break;
                case CLASSIFICATION.BATTLESHIP: value = 1000; break;
                case CLASSIFICATION.CARRIER: value = 1500; break;
                case CLASSIFICATION.SWACS: value = 500; break;
                case CLASSIFICATION.DREADNAUGHT: value = 1500; break;

                case CLASSIFICATION.STATION: value = 2500; break;
                case CLASSIFICATION.FARCASTER: value = 5000; break;

                case CLASSIFICATION.MINE: value = 20; break;
                case CLASSIFICATION.COMSAT: value = 200; break;
                case CLASSIFICATION.DEFSAT: value = 300; break;

                case CLASSIFICATION.BUILDING: value = 100; break;
                case CLASSIFICATION.FACTORY: value = 250; break;
                case CLASSIFICATION.SAM: value = 100; break;
                case CLASSIFICATION.EWR: value = 200; break;
                case CLASSIFICATION.C3I: value = 500; break;
                case CLASSIFICATION.STARBASE: value = 2000; break;

                default: value = 100; break;
            }

            return value;
        }

        public Skin GetSkin() { return skin; }
        public void UseSkin(Skin s) { skin = s; }
        public void ShowRep()
        {
            for (int i = 0; i < detail.NumModels(detail_level); i++)
            {
                Graphic g = detail.GetRep(detail_level, i);
                g.Show();
            }

            if (gear != null && (gear.GetState() != LandingGear.GEAR_STATE.GEAR_UP))
            {
                for (int i = 0; i < gear.NumGear(); i++)
                {
                    Solid g = gear.GetGear(i);
                    if (g != null) g.Show();
                }
            }

            foreach (WeaponGroup g in weapons)
            {
                foreach (Weapon w in g.GetWeapons())
                {
                    Solid turret = w.GetTurret();
                    if (turret != null)
                    {
                        turret.Show();

                        Solid turret_base = w.GetTurretBase();
                        if (turret_base != null)
                            turret_base.Show();
                    }

                    if (w.IsMissile())
                    {
                        for (int i = 0; i < w.Ammo(); i++)
                        {
                            Solid store = w.GetVisibleStore(i);
                            if (store != null)
                            {
                                store.Show();
                            }
                        }
                    }
                }
            }
        }
        public void HideRep()
        {
            for (int i = 0; i < detail.NumModels(detail_level); i++)
            {
                Graphic g = detail.GetRep(detail_level, i);
                g.Hide();
            }

            if (gear != null && (gear.GetState() != LandingGear.GEAR_STATE.GEAR_UP))
            {
                for (int i = 0; i < gear.NumGear(); i++)
                {
                    Solid g = gear.GetGear(i);
                    if (g != null) g.Hide();
                }
            }

            foreach (WeaponGroup g in weapons)
            {
                foreach (Weapon w in g.GetWeapons())
                {
                    Solid turret = w.GetTurret();
                    if (turret != null)
                    {
                        turret.Hide();

                        Solid turret_base = w.GetTurretBase();
                        if (turret_base != null)
                            turret_base.Hide();
                    }

                    if (w.IsMissile())
                    {
                        for (int i = 0; i < w.Ammo(); i++)
                        {
                            Solid store = w.GetVisibleStore(i);
                            if (store != null)
                            {
                                store.Hide();
                            }
                        }
                    }
                }
            }
        }

        public void EnableShadows(bool enable)
        {
#if TODO
            for (int i = 0; i < detail.NumModels(detail_level); i++)
            {
                Graphic g = detail.GetRep(detail_level, i);

                if (g.IsSolid())
                {
                    Solid s = (Solid)g;

                    foreach (Shadow shadow in s.GetShadows())
                    {
                        shadow.SetEnabled(enable);
                    }
                }
            }
#endif 
            throw new NotImplementedException();
        }


        public int RespawnCount() { return respawns; }
        public void SetRespawnCount(int r) { respawns = r; }
        public Point RespawnLoc() { return respawn_loc; }
        public void SetRespawnLoc(Point rl) { respawn_loc = rl; }

        public double WarpFactor() { return warp_fov; }
        public void SetWarp(double w) { warp_fov = (float)w; }

        public void MatchOrientation(Ship s)
        {
            Point pos = cam.Pos();
            cam.Clone(s.cam);
            cam.MoveTo(pos);

            if (rep != null)
                rep.SetOrientation(cam.Orientation());

            if (cockpit != null)
                cockpit.SetOrientation(cam.Orientation());
        }


        // ORDERS AND NAVIGATION:
        static DWORD last_eval_frame = 0;      // one ship per game frame
        public void ExecEvalFrame(double seconds)
        {
            // is it already too late?
            if (life == 0 || integrity < 1) return;

            const DWORD EVAL_FREQUENCY = 1000;   // once every second

            if (element != null && element.NumObjectives() > 0 &&
                    Game.GameTime() - last_eval_time > EVAL_FREQUENCY &&
                    last_eval_frame != Game.Frame())
            {

                last_eval_time = (DWORD)Game.GameTime();
                last_eval_frame = (DWORD)Game.Frame();

                for (int i = 0; i < element.NumObjectives(); i++)
                {
                    Instruction obj = element.GetObjective(i);

                    if (obj.Status() <= Instruction.STATUS.ACTIVE)
                    {
                        obj.Evaluate(this);
                    }
                }
            }
        }
        public void SetLaunchPoint(Instruction pt)
        {
            if (pt != null && launch_point == null)
                launch_point = pt;
        }
        public void AddNavPoint(Instruction pt, Instruction after = null)
        {
            if (GetElementIndex() == 1)
                element.AddNavPoint(pt, after);
        }
        public void DelNavPoint(Instruction pt)
        {
            if (GetElementIndex() == 1)
                element.DelNavPoint(pt);
        }
        public void ClearFlightPlan()
        {
            if (GetElementIndex() == 1)
                element.ClearFlightPlan();
        }
        public Instruction GetNextNavPoint()
        {
            if (launch_point != null && launch_point.Status() <= Instruction.STATUS.ACTIVE)
                return launch_point;

            if (element != null)
                return element.GetNextNavPoint();

            return null;
        }
        public int GetNavIndex(Instruction n)
        {
            if (element != null)
                return element.GetNavIndex(n);

            return 0;
        }
        public double RangeToNavPoint(Instruction n)
        {
            double distance = 0;

            if (n != null && n.Region() != null)
            {
                Point npt = n.Region().Location() + n.Location();
                npt -= GetRegion().Location();
                npt = npt.OtherHand(); // convert from map to sim coords

                distance = new Point(npt - Location()).Length;
            }

            return distance;
        }
        public void SetNavptStatus(Instruction navpt, Instruction.STATUS status)
        {
            if (navpt != null && navpt.Status() != status)
            {
                if (status == Instruction.STATUS.COMPLETE)
                {
                    if (navpt.Action() == Instruction.ACTION.ASSAULT)
                        ErrLogger.PrintLine("Completed Assault\n");

                    else if (navpt.Action() == Instruction.ACTION.STRIKE)
                        ErrLogger.PrintLine("Completed Strike\n");
                }

                navpt.SetStatus(status);

                if (status == Instruction.STATUS.COMPLETE)
                    sim.ProcessEventTrigger(MissionEvent.EVENT_TRIGGER.TRIGGER_NAVPT, 0, Name(), GetNavIndex(navpt));

                if (element != null)
                {
                    int index = element.GetNavIndex(navpt);

                    if (index >= 0)
                        NetUtil.SendNavData(false, element, index - 1, navpt);
                }
            }
        }
        public List<Instruction> GetFlightPlan()
        {
            if (element != null)
                return element.GetFlightPlan();

            return new List<Instruction>();
        }

        public int FlightPlanLength()
        {
            if (element != null)
                return element.FlightPlanLength();

            return 0;
        }
        public CombatUnit GetCombatUnit() { return combat_unit; }
        public Element GetElement() { return element; }
        public Ship GetLeader()
        {
            if (element != null)
                return element.GetShip(1);

            return (Ship)this;
        }
        public int GetElementIndex()
        {
            if (element != null)
                return element.FindIndex(this);

            return 0;
        }
        public int GetOrigElementIndex()
        {
            return orig_elem_index;
        }
        public void SetElement(Element e)
        {
            element = e;

            if (element != null)
            {
                combat_unit = element.GetCombatUnit();

                if (combat_unit != null)
                {
                    integrity = (float)(design.integrity - combat_unit.GetSustainedDamage());
                }

                orig_elem_index = element.FindIndex(this);
            }
        }

        public Instruction GetRadioOrders()
        {
            return radio_orders;
        }
        public void ClearRadioOrders()
        {
            if (radio_orders != null)
            {
                radio_orders.SetAction(0);
                radio_orders.ClearTarget();
                radio_orders.SetLocation(new Point());
            }
        }
        private static RadioHandler rh = new RadioHandler();
        public void HandleRadioMessage(RadioMessage msg)
        {
            if (msg == null) return;

            if (rh.ProcessMessage(msg, this))
                rh.AcknowledgeMessage(msg, this);
        }
        public bool IsAutoNavEngaged()
        {
            if (navsys != null && navsys.AutoNavEngaged())
                return true;

            return false;
        }
        public void SetAutoNav(bool engage = true)
        {
            if (navsys != null)
            {
                if (navsys.AutoNavEngaged())
                {
                    if (!engage)
                        navsys.DisengageAutoNav();
                }
                else
                {
                    if (engage)
                        navsys.EngageAutoNav();
                }

                if (sim != null)
                    SetControls(sim.GetControls());
            }
        }
        public void CommandMode()
        {
            if (dir == null || (int)dir.Type() != ShipCtrl.DIR_TYPE)
            {
                string msg = "Captain on the bridge";
                RadioVox vox = new RadioVox(0, "1", msg);

                if (vox != null)
                {
                    vox.AddPhrase(msg);

                    if (!vox.Start())
                    {
                        RadioView.Message(RadioTraffic.TranslateVox(msg));
                        //delete vox;
                        vox = null;
                    }
                }

                SetControls(sim.GetControls());
            }

            else
            {
                string msg = "Exec, you have the conn";
                RadioVox vox = new RadioVox(0, "1", msg);

                if (vox != null)
                {
                    vox.AddPhrase(msg);

                    if (!vox.Start())
                    {
                        RadioView.Message(RadioTraffic.TranslateVox(msg));
                        //delete vox;
                        vox = null;
                    }
                }

                SetControls(null);
            }
        }

        public void ClearTrack()
        {
            const int DEFAULT_TRACK_LENGTH = 20; // 10 seconds

            if (track == null)
            {
                track = new Point[DEFAULT_TRACK_LENGTH];
            }

            track[0] = Location();
            ntrack = 1;
            track_time = (DWORD)Game.GameTime();
        }

        public void UpdateTrack()
        {
            const int DEFAULT_TRACK_UPDATE = 500; // milliseconds
            const int DEFAULT_TRACK_LENGTH = 20; // 10 seconds

            DWORD time = (DWORD)Game.GameTime();

            if (track == null)
            {
                track = new Point[DEFAULT_TRACK_LENGTH];
                track[0] = Location();
                ntrack = 1;
                track_time = time;
            }

            else if (time - track_time > DEFAULT_TRACK_UPDATE)
            {
                if (Location() != track[0])
                {
                    for (int i = DEFAULT_TRACK_LENGTH - 2; i >= 0; i--)
                        track[i + 1] = track[i];

                    track[0] = Location();
                    if (ntrack < DEFAULT_TRACK_LENGTH) ntrack++;
                }

                track_time = time;
            }
        }
        public int TrackLength() { return ntrack; }
        public Point TrackPoint(int i)
        {
            if (track != null && i < ntrack)
                return track[i];

            return new Point();
        }


        // DAMAGE CONTROL AND ENGINEERING:
        public List<ShipSystem> RepairQueue() { return repair_queue; }
        public double RepairSpeed()
        {
            return design.repair_speed;
        }
        public int RepairTeams()
        {
            return design.repair_teams;
        }
        public void RepairSystem(ShipSystem sys)
        {
            if (!repair_queue.Contains(sys))
            {
                repair_queue.Add(sys);
                sys.Repair();
            }
        }
        public void IncreaseRepairPriority(int task_index)
        {
            if (task_index > 0 && task_index < repair_queue.Count)
            {
                ShipSystem task1 = repair_queue[task_index - 1];
                ShipSystem task2 = repair_queue[task_index];

                repair_queue[task_index - 1] = task2;
                repair_queue[task_index] = task1;
            }
        }
        public void DecreaseRepairPriority(int task_index)
        {
            if (task_index >= 0 && task_index < repair_queue.Count - 1)
            {
                ShipSystem task1 = repair_queue[task_index];
                ShipSystem task2 = repair_queue[task_index + 1];

                repair_queue[task_index] = task2;
                repair_queue[task_index + 1] = task1;
            }
        }
        static DWORD last_repair_frame = 0;     // one ship per game frame
        public void ExecMaintFrame(double seconds)
        {
            // is it already too late?
            if (life == 0 || integrity < 1) return;

            const DWORD REPAIR_FREQUENCY = 5000;  // once every five seconds

            if (auto_repair &&
                    Game.GameTime() - last_repair_time > REPAIR_FREQUENCY &&
                    last_repair_frame != Game.Frame())
            {

                last_repair_time = (DWORD)Game.GameTime();
                last_repair_frame = (DWORD)Game.Frame();

                foreach (ShipSystem sys in systems)
                {

                    if (sys.Status() != ShipSystem.STATUS.NOMINAL)
                    {
                        bool started_repairs = false;

                        // emergency power routing:
                        if (sys.Type() == ShipSystem.CATEGORY.POWER_SOURCE && sys.Availability() < 33)
                        {
                            PowerSource src = (PowerSource)sys;
                            PowerSource dst = null;

                            for (int i = 0; i < reactors.Count; i++)
                            {
                                PowerSource pwr = reactors[i];

                                if (pwr != src && pwr.Availability() > src.Availability())
                                {
                                    if (dst == null ||
                                            (pwr.Availability() > dst.Availability() &&
                                                pwr.Charge() > dst.Charge()))
                                        dst = pwr;
                                }
                            }

                            if (dst != null)
                            {
                                while (src.Clients().Count > 0)
                                {
                                    ShipSystem s = src.Clients()[0];
                                    src.RemoveClient(s);
                                    dst.AddClient(s);
                                }
                            }
                        }

                        foreach (SimElements.Component c in sys.GetComponents())
                        {

                            if (c.Status() < SimElements.Component.STATUS.NOMINAL && c.Availability() < 75)
                            {
                                if (c.SpareCount() != 0 &&
                                        c.ReplaceTime() <= 300 &&
                                        (c.Availability() < 50 ||
                                            c.ReplaceTime() < c.RepairTime()))
                                {

                                    c.Replace();
                                    started_repairs = true;
                                }

                                else if (c.Availability() >= 50 || c.NumJerried() < 5)
                                {
                                    c.Repair();
                                    started_repairs = true;
                                }
                            }
                        }

                        if (started_repairs)
                            RepairSystem(sys);
                    }
                }
            }

            if (repair_queue.Count > 0 && RepairTeams() > 0)
            {
                int team = 0;
                for (int i = repair_queue.Count - 1; i >= 0; i--)
                {
                    ShipSystem sys = repair_queue[i];
                    if (!(team < RepairTeams())) break;

                    sys.ExecMaintFrame(seconds * RepairSpeed());
                    team++;

                    if (sys.Status() != ShipSystem.STATUS.MAINT)
                    {
                        repair_queue.RemoveAt(i);

                        // emergency power routing (restore):
                        if (sys.Type() == ShipSystem.CATEGORY.POWER_SOURCE &&
                                sys.Status() == ShipSystem.STATUS.NOMINAL)
                        {
                            PowerSource src = (PowerSource)sys;
                            int isrc = reactors.IndexOf(src);

                            for (i = 0; i < reactors.Count; i++)
                            {
                                PowerSource pwr = reactors[i];

                                if (pwr != src)
                                {
                                    List<ShipSystem> xfer = new List<ShipSystem>();

                                    for (int j = 0; j < pwr.Clients().Count; j++)
                                    {
                                        ShipSystem s = pwr.Clients()[j];

                                        if (s.GetSourceIndex() == isrc)
                                        {
                                            xfer.Add(s);
                                        }
                                    }

                                    for (int j = 0; j < xfer.Count; j++)
                                    {
                                        ShipSystem s = xfer[j];
                                        pwr.RemoveClient(s);
                                        src.AddClient(s);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public bool AutoRepair() { return auto_repair; }
        public void EnableRepair(bool e) { auto_repair = e; }
        public bool MasterCaution() { return master_caution; }
        public void ClearCaution() { master_caution = false; }

        // SYSTEM ACCESSORS:
        public List<ShipSystem> Systems() { return systems; }
        public List<WeaponGroup> Weapons() { return weapons; }
        public List<Drive> Drives() { return drives; }
        public List<Computer> Computers() { return computers; }
        public List<FlightDeck> FlightDecks() { return flight_decks; }
        public List<PowerSource> Reactors() { return reactors; }
        public List<NavLight> NavLights() { return navlights; }
        public Shield GetShield() { return shield; }
        public Solid GetShieldRep() { return (Solid)shieldRep; }
        public Sensor GetSensor() { return sensor; }
        public NavSystem GetNavSystem() { return navsys; }
        public FlightComp GetFLCS() { return flcs; }
        public Thruster GetThruster() { return thruster; }
        public Hangar GetHangar() { return hangar; }
        public LandingGear GetGear() { return gear; }

        public ShipSystem GetSystem(int sys_id)
        {
            ShipSystem s = null;

            if (sys_id >= 0)
            {
                if (sys_id < systems.Count)
                {
                    s = systems[sys_id];
                    if (s.GetID() == sys_id)
                        return s;
                }

                foreach (ShipSystem iter in systems)
                {
                    s = iter;

                    if (s.GetID() == sys_id)
                        return s;
                }
            }

            return null;
        }

        public static int GetControlModel() { return control_model; }
        public static void SetControlModel(int n) { control_model = n; }
        public static FLIGHT_MODEL GetFlightModel() { return flight_model; }
        public static void SetFlightModel(FLIGHT_MODEL f) { flight_model = f; }
        public static LANDING_MODEL GetLandingModel() { return landing_model; }
        public static void SetLandingModel(LANDING_MODEL f) { landing_model = f; }
        public static double GetFriendlyFireLevel() { return friendly_fire_level; }
        public static void SetFriendlyFireLevel(double f) { friendly_fire_level = f; }


        protected int CheckShotIntersection(Shot shot, out Point ipt, out Point hpt, out Weapon wep)
        {
            wep = null;
            ipt = new Point();
            hpt = new Point();
            int hit_type = HIT_NOTHING;
            Point shot_loc = shot.Location();
            Point shot_org = shot.Origin();
            Point shot_vpn = shot_loc - shot_org;
            double shot_len = shot_vpn.Normalize();
            double blow_len = shot_len;
            bool hit_hull = false;
            bool easy = false;

            if (shot_len <= 0)
                return hit_type;

            if (shot_len < 1000)
                shot_len = 1000;

            Point hull_impact = new Point();
            Point shield_impact = new Point();
            Point turret_impact = new Point();
            Point closest = new Point();
            double d0 = 1e9;
            double d1 = 1e9;
            double ds = 1e9;

            if (dir != null && dir.Type() == SteerAI.SteerType.FIGHTER)
            {
                ShipAI shipAI = (ShipAI)dir;
                easy = shipAI.GetAILevel() < 2;
            }

            if (shieldRep != null && ShieldStrength() > 5)
            {
                if (shieldRep.CheckRayIntersection(shot_org, shot_vpn, shot_len, shield_impact))
                {
                    hit_type = HIT_SHIELD;
                    closest = shield_impact;
                    d0 = new Point(closest - shot_org).Length;
                    ds = d0;

                    ipt = shield_impact;
                }
            }

            if (shieldRep != null && hit_type == HIT_SHIELD && !shot.IsBeam())
                blow_len = shieldRep.Radius() * 2;

            for (int i = 0; i < detail.NumModels(detail_level) && !hit_hull; i++)
            {
                Solid s = (Solid)detail.GetRep(detail_level, i);
                if (s != null)
                {
                    if (easy)
                    {
                        hit_hull = CheckRaySphereIntersection(s.Location(), s.Radius(), shot_org, shot_vpn, shot_len);
                    }
                    else
                    {
                        hit_hull = s.CheckRayIntersection(shot_org, shot_vpn, blow_len, hull_impact) ? true : false;
                    }
                }
            }

            if (hit_hull)
            {
                if (ShieldStrength() > 5 && shieldRep == null)
                    hit_type = HIT_SHIELD;

                hit_type = hit_type | HIT_HULL;
                hpt = hull_impact;

                d1 = new Point(hull_impact - shot_org).Length;

                if (d1 < d0)
                {
                    closest = hull_impact;
                    d0 = d1;
                }
            }

            if (IsStarship() || IsStatic())
            {
                foreach (WeaponGroup g in Weapons())
                {
                    if (g.GetDesign() != null && g.GetDesign().turret_model != null)
                    {
                        double tsize = g.GetDesign().turret_model.Radius();

                        foreach (Weapon w in g.GetWeapons())
                        {
                            Point tloc = w.GetTurret().Location();

                            if (CheckRaySphereIntersection(tloc, tsize, shot_org, shot_vpn, shot_len))
                            {
                                Point delta = tloc - shot_org;
                                d1 = delta.Length;

                                if (d1 < d0)
                                {
                                    if (wep != null) wep = w;
                                    hit_type = hit_type | HIT_TURRET;
                                    turret_impact = tloc;

                                    d0 = d1;

                                    closest = turret_impact;
                                    hull_impact = turret_impact;
                                    hpt = turret_impact;

                                    if (d1 < ds)
                                        ipt = turret_impact;
                                }
                            }
                        }
                    }
                }
            }

            // trim beam shots to closest impact point:
            if (hit_type != 0 && shot.IsBeam())
            {
                shot.SetBeamPoints(shot_org, closest);
            }

            return hit_type;
        }
        protected WeaponGroup FindWeaponGroup(string name)
        {
            WeaponGroup group = null;

            foreach (WeaponGroup iter in weapons)
            {
                if (iter.Name() == name)
                {
                    group = iter;
                    break;
                }
            }
            if (group == null)
            {
                group = new WeaponGroup(name);
                weapons.Add(group);
            }

            return group;
        }

        public void Observe(SimObject obj)
        {
            throw new NotImplementedException();
        }

        public void Ignore(SimObject obj)
        {
            throw new NotImplementedException();
        }
        private bool IsWeaponBlockedFriendly(Weapon w, SimObject test)
        {
            if (w.GetTarget() != null)
            {
                Point tgt = w.GetTarget().Location();
                Point obj = test.Location();
                Point wep = w.MountLocation();

                Point dir = tgt - wep;
                double d = dir.Normalize();
                Point rho = obj - wep;
                double r = rho.Normalize();

                // if target is much closer than obstacle,
                // don't worry about friendly fire...
                if (d < 1.5 * r)
                    return false;

                Point dst = dir * r + wep;
                double err = (obj - dst).Length;

                if (err < test.Radius() * 1.5)
                    return true;
            }

            return false;
        }
        static bool CheckRaySphereIntersection(Point loc, double radius, Point Q, Point w, double len)
        {
            Point d0 = loc - Q;
            Point d1 = Point.Cross(d0, w);
            double dlen = d1.Length;          // distance of point from line

            if (dlen > radius)                  // clean miss
                return false;                    // (no impact)

            // possible collision course...
            // find the point on the ray that is closest
            // to the sphere's location:
            Point closest = Q + w * (d0 * w);

            // find the leading edge, and it's distance from the location:
            Point leading_edge = Q + w * len;
            Point leading_delta = leading_edge - loc;
            double leading_dist = leading_delta.Length;

            // if the leading edge is not within the sphere,
            if (leading_dist > radius)
            {
                // check to see if the closest point is between the
                // ray's endpoints:
                Point delta1 = closest - Q;
                Point delta2 = leading_edge - Q; // this is w*len

                // if the closest point is not between the leading edge
                // and the origin, this ray does not intersect:
                if (Point.Dot(delta1, delta2) < 0 || delta1.Length > len)
                {
                    return false;
                }
            }

            return true;
        }


        protected string regnum; //16
        protected ShipDesign design;
        protected ShipKiller killer;
        protected DetailSet detail;
        protected int detail_level;
        protected Sim sim;
        protected double vlimit;
        protected double agility;
        protected double throttle;
        protected double throttle_request;
        protected bool augmenter;
        protected float wep_mass;
        protected float wep_resist;

        protected int IFF_code;
        protected int cmd_chain_index;
        protected int ff_count;
        protected OP_MODE flight_phase;

        protected SimObject target;
        protected ShipSystem subtarget;
        protected Ship ward;
        protected bool check_fire;
        protected int primary;
        protected int secondary;

        protected Skin skin;
        protected Solid cockpit;
        protected Drive main_drive;
        protected QuantumDrive quantum_drive;
        protected Farcaster farcaster;
        protected Shield shield;
        protected ShieldRep shieldRep;
        protected NavSystem navsys;
        protected FlightComp flcs;
        protected Sensor sensor;
        protected LandingGear gear;
        protected Thruster thruster;
        protected Weapon decoy;
        protected Weapon probe;
        protected Drone sensor_drone;
        protected Hangar hangar;
        protected List<Shot> decoy_list;
        protected List<Shot> threat_list;

        protected List<ShipSystem> systems;
        protected List<PowerSource> reactors;
        protected List<WeaponGroup> weapons;
        protected List<Drive> drives;
        protected List<Computer> computers;
        protected List<FlightDeck> flight_decks;
        protected List<NavLight> navlights;
        protected List<ShipSystem> repair_queue;

        protected CombatUnit combat_unit;
        protected Element element;
        protected int orig_elem_index;
        protected Instruction radio_orders;
        protected Instruction launch_point;

        protected Point chase_vec;
        protected Point bridge_vec;

        protected string director_info;
        protected BYTE ai_mode;
        protected int command_ai_level;
        protected FLCS_MODE flcs_mode;
        protected bool net_observer_mode;

        protected float pcs;  // passive sensor cross section
        protected float acs;  // active  sensor cross section
        protected BYTE emcon;
        protected BYTE old_emcon;
        protected bool invulnerable;

        protected ulong launch_time;
        protected ulong friendly_fire_time;

        protected Ship carrier;
        protected FlightDeck dock;
        protected InboundSlot inbound;

        protected IDirector net_control;

        protected Point[] track;
        protected int ntrack;
        protected DWORD track_time;

        protected float helm_heading;
        protected float helm_pitch;

        protected float altitude_agl;
        protected float g_force;

        protected float warp_fov;

        protected float transition_time;
        protected TRAN_TYPE transition_type;
        protected Point transition_loc;
        protected Point respawn_loc;
        protected int respawns;

        protected bool master_caution;
        protected bool auto_repair;
        protected ulong last_repair_time;
        protected ulong last_eval_time;
        protected ulong last_beam_time;
        protected ulong last_bolt_time;

        protected int[] missile_id = new int[4];
        protected BYTE[] missile_eta = new BYTE[4];
        protected bool[] trigger = new bool[4];
        protected int[] loadout;

        protected int contact_id;
        protected ISimObserver thisSimObserver = new SimObserver();

        protected static int control_model;
        protected static FLIGHT_MODEL flight_model;
        protected static LANDING_MODEL landing_model;
        protected static double friendly_fire_level;

        private static int base_contact_id = 0;
        private static double range_min = 0;
        private static double range_max = 250e3;

        private const int HIT_NOTHING = 0;
        private const int HIT_HULL = 1;
        private const int HIT_SHIELD = 2;
        private const int HIT_BOTH = 3;
        private const int HIT_TURRET = 4;

    }
}
