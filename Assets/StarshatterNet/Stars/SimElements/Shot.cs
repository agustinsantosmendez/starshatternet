﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Shot.h/Shot.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Laser and Missile class
*/
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Audio;
using StarshatterNet.Stars.Graphics.General;
using System;
using UnityEngine;
using BYTE = System.Byte;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars.SimElements
{
#warning Shot class is still in development and is not recommended for production.
    public class Shot : SimObject, ISimObserver
    {
        public Shot(Point pos, CameraNode shot_cam, WeaponDesign dsn, Ship ship = null)
        {
#if TODO
            first_frame = true; owner = ship; flash = null; flare = null; trail = null; sound = null; eta = 0;
            charge = 1.0f; design = dsn; offset = 1.0e5f; altitude_agl = -1.0e6f; hit_target = false;
            obj_type = (int)SimObject.TYPES.SIM_SHOT;
            type = design.type;
            primary = design.primary;
            beam = design.beam;
            base_damage = design.damage;
            armed = false;

            radius = 10.0f;

            if (primary || design.decoy_type  != 0||  design.guided == 0)
            {
                straight = true;
                armed = true;
            }

            cam=shot_cam.Clone();

            life = design.life;
            velocity = cam.vpn() *  design.speed;

            MoveTo(pos);

            if (beam)
                origin = pos + (shot_cam.vpn() * -design.length);

            switch (design.graphic_type)
            {
                case Graphic.TYPE.BOLT:
                    {
                        Bolt  s = new Bolt(design.length, design.width, design.shot_img, true);
                        s.SetDirection(cam.vpn());
                        rep = s;
                    }
                    break;

                case Graphic.TYPE.SPRITE:
                    {
                        Sprite  s = null;

                        if (design.animation != null)
                            s = new DriveSprite(design.animation, design.anim_length);
                        else
                            s = new DriveSprite(design.shot_img);

                        s.Scale((double)design.scale);
                        rep = s;
                    }
                    break;

                case Graphic.TYPE.SOLID:
                    {
                        Solid  s = new Solid();
                        s.UseModel(design.shot_model);
                        rep = s;

                        radius = rep.Radius();
                    }
                    break;
            }

            if (rep != null)
                rep.MoveTo(pos);

            light = null;

            if (design.light > 0)
            {
                light = new Light(design.light);
                light.SetColor(design.light_color);
            }

            mass = design.mass;
            drag = design.drag;
            thrust = 0.0f;

            dr_drg = design.roll_drag;
            dp_drg = design.pitch_drag;
            dy_drg = design.yaw_drag;

            SetAngularRates((float)design.roll_rate, (float)design.pitch_rate, (float)design.yaw_rate);

            if (design.flash_img != 0)
            {
                flash = new Sprite(design.flash_img);
                flash.Scale((double)design.flash_scale);
                flash.MoveTo(pos - cam.vpn() * design.length);
                flash.SetLuminous(true);
            }

            if (design.flare_img != 0)
            {
                flare = new DriveSprite(design.flare_img);
                flare.Scale((double)design.flare_scale);
                flare.MoveTo(pos);
            }

            if (owner != null)
            {
                iff_code = (BYTE)owner.GetIFF();
                Observe((SimObject )owner);
            }

             name = string.Format("Shot({0})", design.name );
#endif
            throw new NotImplementedException();
        }
        //~Shot();

        public virtual void SeekTarget(SimObject target, ShipSystem sub = null)
        {
            if (dir != null && !primary)
            {
                SeekerAI seeker = (SeekerAI)dir;
                SimObject old_target = seeker.GetTarget();

                if ((TYPES)old_target.Type() == SimObject.TYPES.SIM_SHIP)
                {
                    Ship tgt_ship = (Ship)old_target;
                    tgt_ship.DropThreat(this);
                }
            }

            //delete dir;
            dir = null;

            if (target != null)
            {
                SeekerAI seeker = new SeekerAI(this);
                seeker.SetTarget(target, sub);
                seeker.SetPursuit(design.guided);
                seeker.SetDelay(1);

                dir = seeker;

                if (!primary && (TYPES)target.Type() == SimObject.TYPES.SIM_SHIP)
                {
                    Ship tgt_ship = (Ship)target;
                    tgt_ship.AddThreat(this);
                }
            }
        }

        public override void ExecFrame(double seconds)
        {
#if TODO
            altitude_agl = -1.0e6f;

            // add random flickering effect:
            double flicker = 0.75 + (double)RandomHelper.Rand() / 8e4;
            if (flicker > 1) flicker = 1;

            if (flare != null)
            {
                flare.SetShade(flicker);
            }
            else if (beam)
            {
                Bolt blob = (Bolt)rep;
                blob.SetShade(flicker);
                offset -= (float)(seconds * 10);
            }

            if (SimTime.IsPaused )
                return;

            if (beam)
            {
                if (!first_frame)
                {
                    if (life > 0)
                    {
                        life -= seconds;

                        if (life < 0)
                            life = 0;
                    }
                }
            }
            else
            {
                origin = Location();

                if (!first_frame)
                    base.ExecFrame(seconds);
                else
                    base.ExecFrame(0);

                double len = design.length;
                if (len < 50) len = 50;

                if (trail == null && life > 0 && design.life - life > 0.2)
                {
                    if (design.trail.length())
                    {
                        trail = new Trail(design.trail_img, design.trail_length);

                        if (design.trail_width > 0)
                            trail.SetWidth(design.trail_width);

                        if (design.trail_dim > 0)
                            trail.SetDim(design.trail_dim);

                        trail.AddPoint(Location() + Heading() * -100);

                        Scene scene = null;

                        if (rep)
                            scene = rep.GetScene();

                        if (scene != null)
                            scene.AddGraphic(trail);
                    }
                }

                if (trail != null)
                    trail.AddPoint(Location());

                if (!armed)
                {
                    SeekerAI seeker = (SeekerAI)dir;

                    if (seeker && seeker.GetDelay() <= 0)
                        armed = true;
                }

                // handle submunitions:
                else if (design.det_range > 0 && design.det_count > 0)
                {
                    if (dir != null && !primary)
                    {
                        SeekerAI seeker = (SeekerAI)dir;
                        SimObject target = seeker.GetTarget();

                        if (target != null)
                        {
                            double range = new Point(Location() - target.Location()).length();

                            if (range < design.det_range)
                            {
                                life = 0;

                                Sim sim = Sim.GetSim();
                                WeaponDesign child_design = WeaponDesign.Find(design.det_child);

                                if (sim && child_design)
                                {
                                    double spread = design.det_spread;

                                    Camera aim_cam;
                                    aim_cam.Clone(Cam());
                                    aim_cam.LookAt(target.Location());

                                    for (int i = 0; i < design.det_count; i++)
                                    {
                                        Shot child = sim.CreateShot(Location(), aim_cam, child_design,
                                        owner, owner.GetRegion());

                                        child.SetCharge(child_design.charge);

                                        if (child_design.guided != 0)
                                            child.SeekTarget(target, seeker.GetSubTarget());

                                        if (child_design.beam)
                                            child.SetBeamPoints(Location(), target.Location());

                                        if (i) aim_cam.LookAt(target.Location());
                                        aim_cam.Pitch(Random(-spread, spread));
                                        aim_cam.Yaw(Random(-spread, spread));
                                    }
                                }
                            }
                        }
                    }
                }

                if (flash != null && !first_frame)
                    GRAPHIC_DESTROY(flash);

                if (thrust < design.thrust)
                    thrust += (float)(seconds * 5.0e3);
                else
                    thrust = design.thrust;
            }

            first_frame = 0;

            if (flare != null)
                flare.MoveTo(Location());
#endif
            throw new NotImplementedException();
        }

        public static void Initialize()
        {
        }

        public static void Close()
        {
        }

        public void Activate(Scene scene)
        {
#if TODO
            base.Activate(scene);

            if (trail != null)
                scene.AddGraphic(trail);

            if (flash != null)
                scene.AddGraphic(flash);

            if (flare != null)
                scene.AddGraphic(flare);

            if (first_frame)
            {
                if (design.sound_resource)
                {
                    sound = design.sound_resource.Duplicate();

                    if (sound != null)
                    {
                        long max_vol = AudioConfig.EfxVolume();
                        long volume = -1000;

                        if (volume > max_vol)
                            volume = max_vol;

                        if (beam)
                        {
                            sound.SetLocation(origin);
                            sound.SetVolume(volume);
                            sound.Play();
                        }
                        else
                        {
                            sound.SetLocation(Location());
                            sound.SetVolume(volume);
                            sound.Play();
                            sound = null;  // fire and forget:
                        }
                    }
                }
            }
#endif
            throw new NotImplementedException();
        }

        public void Deactivate(Scene scene)
        {
#if TODO
            base.Deactivate(scene);

            if (trail != null)
                scene.DelGraphic(trail);

            if (flash != null)
                scene.DelGraphic(flash);

            if (flare != null)
                scene.DelGraphic(flare);
#endif
            throw new NotImplementedException();
        }


        public Ship Owner() { return owner; }
        public double Damage()
        {
#if TODO
            double damage = 0;

            // beam damage based on length:
            if (beam)
            {
                double fade = 1;

                if (design != null)
                {
                    // linear fade with distance:
                    double len = new Point(origin - Location()).length();

                    if (len > design.min_range)
                        fade = (design.length - len) / (design.length - design.min_range);
                }

                damage = base_damage * charge * fade * Game.FrameTime();
            }

            // energy wep damage based on time:
            else if (primary)
            {
                damage = base_damage * charge * life;
            }

            // missile damage is constant:
            else
            {
                damage = base_damage * charge;
            }

            return damage;
#endif
            throw new NotImplementedException();
        }

        public int ShotType() { return (int)type; }
        public virtual SimObject GetTarget()
        {
#if TODO
            if (dir != null)
            {
                SeekerAI seeker = (SeekerAI)dir;

                if (seeker.GetDelay() <= 0)
                    return seeker.GetTarget();
            }

            return null;
#endif
            throw new NotImplementedException();
        }

        public virtual bool IsPrimary() { return primary; }
        public virtual bool IsDrone() { return false; }
        public virtual bool IsDecoy() { return false; }
        public virtual bool IsProbe() { return false; }
        public virtual bool IsMissile() { return !primary; }
        public virtual bool IsArmed() { return armed; }
        public virtual bool IsBeam() { return beam; }
        public virtual bool IsFlak()
        {
            return design != null && design.flak;
        }

        public override bool IsHostileTo(SimObject o)
        {
            if (o != null)
            {
                if ((TYPES)o.Type() == TYPES.SIM_SHIP)
                {
                    Ship s = (Ship)o;

                    if (s.IsRogue())
                        return true;

                    if (s.GetIFF() > 0 && s.GetIFF() != GetIFF())
                        return true;
                }

                else if ((TYPES)o.Type() == TYPES.SIM_SHOT || (TYPES)o.Type() == TYPES.SIM_DRONE)
                {
                    Shot s = (Shot)o;

                    if (s.GetIFF() > 0 && s.GetIFF() != GetIFF())
                        return true;
                }
            }

            return false;
        }

        public bool HitTarget() { return hit_target; }
        public void SetHitTarget(bool h) { hit_target = h; }

        public virtual bool IsTracking(Ship tgt)
        {
            return tgt != null && (GetTarget() == tgt);
        }

        public virtual double PCS() { return 0; }
        public virtual double ACS() { return 0; }
        public virtual int GetIFF()
        {
            return iff_code;
        }

        public virtual Color MarkerColor()
        {
            return Ship.IFFColor(GetIFF());
        }

        public Point Origin() { return origin; }
        public float Charge() { return charge; }
        public void SetCharge(float c)
        {
            charge = c;

            // trim beam life to amount of energy available:
            if (beam)
                life = design.life * charge / design.charge;
        }

        public double Length()
        {
            if (design != null)
                return design.length;

            return 500;
        }

        public Graphic GetTrail() { return (Graphic)trail; }
        public void SetFuse(double seconds)
        {
            if (seconds > 0 && !beam)
                life = seconds;
        }


        public void SetBeamPoints(Point from, Point to)
        {
#if TODO
            if (beam)
            {
                MoveTo(to);
                origin = from;

                if (sound != null)
                {
                    sound.SetLocation(from);
                }

                if (rep != null)
                {
                    Bolt s = (Bolt)rep;
                    s.SetEndPoints(from, to);

                    double len = new Point(to - from).length() / 500;
                    s.SetTextureOffset(offset, offset + len);
                }
            }

            if (flash != null)
            {
                flash.MoveTo(origin);
            }
#endif
            throw new NotImplementedException();
        }

        public virtual void Disarm()
        {
            if (armed && !primary)
            {
                armed = false;
                //delete dir;
                dir = null;
            }
        }

        public virtual void Destroy()
        {
            life = 0;
        }

        public WeaponDesign Design() { return design; }
        public string DesignName()
        {
            return design.name;
        }

        public int GetEta() { return eta; }
        public void SetEta(int t) { eta = t; }

        public double AltitudeMSL()
        {
            return Location().Y;
        }

        public double AltitudeAGL()
        {
#if TODO
            if (altitude_agl < -1000)
            {
                Shot pThis = (Shot)this; // cast-away const
                Point loc = Location();
                Terrain terrain = region.GetTerrain();

                if (terrain != null)
                    pThis.altitude_agl = (float)(loc.Y - terrain.Height(loc.x, loc.z));

                else
                    pThis.altitude_agl = (float)loc.Y;

                if (!_finite(altitude_agl))
                {
                    pThis.altitude_agl = 0.0f;
                }
            }

            return altitude_agl;
#endif
            throw new NotImplementedException();
        }

        // SimObserver interface:
        public virtual bool Update(SimObject obj)
        {
            if (obj == (SimObject)owner)
                owner = null;

            return simObserver.Update(obj);
        }

        public virtual string GetObserverName()
        {
            return name;
        }

        public void Observe(SimObject obj)
        {
            simObserver.Observe(obj);
        }

        public void Ignore(SimObject obj)
        {
            simObserver.Ignore(obj);
        }

        // public int operator ==(const Shot& s) { return id == s.id; }

        protected Ship owner;

        protected WeaponDesign.WeaponType type;
        protected float base_damage;
        protected float charge;
        protected float offset;
        protected float altitude_agl;
        protected int eta;
        protected BYTE iff_code;
        protected bool first_frame;
        protected bool primary;
        protected bool beam;
        protected bool armed;
        protected bool hit_target;

        protected Sprite flash;   // muzzle flash
        protected Sprite flare;   // drive flare
        protected Trail trail;   // exhaust trail

        protected Sound sound;
        protected WeaponDesign design;

        // for beam weapons:
        protected Point origin;
        protected ISimObserver simObserver = new SimObserver();
    }
}
