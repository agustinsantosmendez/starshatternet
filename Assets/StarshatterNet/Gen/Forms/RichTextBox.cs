﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      RichTextBox.h/RichTextBox.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Rich Text Window - an HTML-like control
*/
using System;
using StarshatterNet.Stars.Graphics;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.Int32;

namespace StarshatterNet.Gen.Forms
{
    public class RichTextBox : ScrollWindow
    {

        public RichTextBox(ActiveWindow p, int ax, int ay, int aw, int ah, DWORD aid = 0, DWORD astyle = 0) :
             base(ax, ay, aw, ah, aid, p)
        {
            leading = 2;
            desc = "RichTextBox " + id;
        }

        // public virtual ~RichTextBox();

        //public int operator ==(const RichTextBox& w) const { return id == w.id; }

        // Operations:
        public virtual void DrawContent(Stars.Rect ctrl_rect) { throw new NotImplementedException(); }

        // Event Target Interface:
        public override int OnMouseMove(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonDown(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonUp(int x, int y) { throw new NotImplementedException(); }
        public override int OnMouseWheel(int wheel) { throw new NotImplementedException(); }
        public override int OnClick()
        {
            int fire_click = scrolling == 0 ? 1 : 0;

            if (scrolling == SCROLL.SCROLL_THUMB)
                scrolling = SCROLL.SCROLL_NONE;

            if (fire_click != 0)
                return base.OnClick();

            return 0;
        }

        public override int OnKeyDown(int vk, int flags) { throw new NotImplementedException(); }


        protected virtual void DrawTabbedText() { throw new NotImplementedException(); }
        protected virtual void DrawRichText(Stars.Rect text_rect) { throw new NotImplementedException(); }
        protected int GetNextTab(int xpos) { throw new NotImplementedException(); }

        //protected virtual int find_next_word_start(string text, int index) { throw new NotImplementedException(); }
        //protected virtual int find_next_word_end(string text, int index) { throw new NotImplementedException(); }
        //protected virtual int parse_hex_digit(char c) { throw new NotImplementedException(); }
        //protected virtual int process_tag(string text, int index, Font font) { throw new NotImplementedException(); }
        public override void SetText(string t)
        {
            base.SetText(t);
            ScrollTo(0);
            SetText();
        }
        public override void SetFont(FontItem f)
        {
            base.SetFont(f);
            SetFont();
        }
        public override void SetFontSize(int s)
        {
            base.SetFontSize(s);
            SetFont();
        }

        protected void SetText()
        {
            if (textCmp == null) return;
            textCmp.text = this.GetText();
        }
        protected void SetFont()
        {
            if (textCmp == null) return;
            textCmp.font = this.GetFont().font;
            textCmp.fontSize = this.GetFontSize();
        }

        public override void SetForeColor(Color c)
        {
            base.SetForeColor(c);
            SetForeColor();
        }
        protected void SetForeColor()
        {
            if (textCmp != null)
                textCmp.color = fore_color;
        }
        public override void MoveTo(Stars.Rect r)
        {
            base.MoveTo(r);
            if (layoutElement != null)
                layoutElement.preferredWidth = r.w - (int)MISC.SCROLL_WIDTH - 2 * LeftPadding;
        }

        protected internal override void BuildGamebjects()
        {
            base.BuildGamebjects();

            GameObject objTxt = new GameObject(); //Create the GameObject   
            layoutElement = objTxt.AddComponent<LayoutElement>();
            layoutElement.preferredWidth = objRectTransform.rect.width - (int)MISC.SCROLL_WIDTH - 2 * LeftPadding;

            RectTransform rectTransform = objTxt.GetComponent<RectTransform>();
            textCmp = objTxt.AddComponent<Text>(); //Add the Text Component script
            rectTransform.SetParent(content.transform); //Assign the newly created Image GameObject as a Child of the Parent Panel.
            objTxt.name = desc + " Txt";
            objTxt.SetActive(true); //Activate the GameObject

            SetText();
            SetFont();
            SetForeColor();
        }
        protected Text textCmp;
        protected LayoutElement layoutElement;
    }
}
