﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetPeer.h/NetPeer.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

      OVERVIEW
     ========
     One side of a UDP net link connection
*/
using System;
using System.Collections.Generic;
using DWORD = System.UInt32;
using WORD = System.UInt16;
using BYTE = System.Byte;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Stars.Network
{
    public class NetPeer
    {
        public enum STATUS { OK, SEND_OVERFLOW, RECV_OVERFLOW };

        public NetPeer(NetAddr a, uint id)
        {
            addr = a; netid = id; sequence = 0; pps = 0; bps = 0; max_qsize = 0;
            status = STATUS.OK; hist_indx = 0; send_size = 0; recv_size = 0;
            chunk_size = MULTIPART_CHUNKSIZE;
            {
                Array.Clear(hist_time, 0, hist_time.Length);
                Array.Clear(hist_size, 0, hist_size.Length);

                last_recv_time = NetLayer.GetUTC();
            }
        }

        //public int operator ==(NetPeer p) { return netid == p.netid; }

        public bool SendMessage(NetMsg msg)
        {
            if (msg != null)
            {
                if (max_qsize > 0 && msg.Length() + send_size > max_qsize)
                {
                    status = STATUS.SEND_OVERFLOW;
                    //delete msg;
                    return false;
                }

                // simple message
                if (msg.Length() <= (int)chunk_size)
                {
                    if (msg.IsPriority())
                        send_list.Insert(0, msg);
                    else
                        send_list.Add(msg);

                    send_size += msg.Length();
                }

                // multipart message
                else
                {
                    List<NetMsg> list = send_list;

                    if (msg.IsScatter())
                        list = multi_send_list;

                    uint nparts = (uint)(msg.Length() / chunk_size);
                    uint extra = (uint)(msg.Length() % chunk_size);

                    if (extra > 0) nparts++;

                    multi_part_buffer.type = NetMsg.TYPES.MULTIPART;
                    multi_part_buffer.msgid = multi_msg_sequence++;
                    multi_part_buffer.nparts = nparts;

                    uint header_size = NetMsg.NetMsgMultipart.HeaderSize();

                    byte[] p = msg.Data();
                    uint offset = 0;

                    for (uint i = 0; i < nparts; i++)
                    {
                        multi_part_buffer.partno = i;
                        NetMsg part = null;
                        uint part_size = chunk_size;

                        if (i == nparts - 1 && extra > 0) // last partial payload
                            part_size = extra;

                        Array.Copy(p, offset, multi_part_buffer.payload, 0, part_size);
                        //CopyMemory(multi_part_buffer.payload, p, part_size);
                        offset += part_size;
                        part = new NetMsg(msg.NetID(),
                                        multi_part_buffer,
                                        (int)(header_size + part_size),
                                        msg.Flags());

                        if (part != null)
                        {
                            list.Add(part);
                            send_size += part.Length();
                        }
                    }
                }

                return true;
            }

            return false;
        }
        public NetMsg GetMessage()
        {
            if (recv_list.Count > 0)
            {
                NetMsg msg = recv_list.RemoveIndex(0);
                recv_size -= msg.Length();
                return msg;
            }

            return null;
        }


        public NetGram ComposeGram()
        {
            NetGram g = null;

            if ((send_list.Count != 0 || multi_send_list.Count != 0) && OKtoSend())
            {
                lock (sync)
                {

                    int xmit_size = send_size;
                    int nmsg = send_list.Count;
                    int limit = NetGram.NET_GRAM_MAX_SIZE;
                    bool reliable = false;
                    bool is_multi = false;

                    NetMsg multi_msg = null;
                    List<NetMsg> list = send_list;

                    if (xmit_size > limit)
                    {
                        xmit_size = 0;
                        nmsg = 0;

                        if (send_list.Count > 0)
                        {

                            // if there is regular traffic, and multipart traffic
                            if (multi_send_list.Count != 0)
                            {
                                // just send one multipart message in this packet
                                multi_msg = multi_send_list.RemoveIndex(0);
                                limit -= multi_msg.Length();
                                reliable = true;
                                is_multi = true;
                            }

                            for (int i = 0; i < send_list.Count; i++)
                            {
                                NetMsg msg = send_list[i];

                                if (xmit_size + msg.Length() < limit)
                                {
                                    xmit_size += msg.Length();
                                    nmsg++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            // if there is only multipart traffic,
                            // send as many multipart messages as will fit:
                            list = multi_send_list;
                            reliable = true;
                            is_multi = true;

                            for (int i = 0; i < multi_send_list.Count; i++)
                            {
                                NetMsg msg = multi_send_list[i];

                                if (xmit_size + msg.Length() < limit)
                                {
                                    xmit_size += msg.Length();
                                    nmsg++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }

                    if (xmit_size > 0 && nmsg > 0)
                    {
                        byte[] buffer = new byte[xmit_size];
                        byte[] p = buffer;
                        int offset = 0;

                        if (multi_msg != null)
                        {
                            if (buffer != null)
                            {
                                //CopyMemory(p, multi_msg.Data(), multi_msg.Length());
                                //p[1] = multi_msg.Length();
                                //p += multi_msg.Length();
                                Array.Copy(multi_msg.Data(), 0, p, offset, multi_msg.Length());
                                Array.Copy(BitConverter.GetBytes((byte)multi_msg.Length()), 0, p, offset + 1, sizeof(byte));
                                offset += multi_msg.Length();
                            }
                            // delete multi_msg;
                        }

                        while (nmsg-- != 0 && offset < xmit_size)
                        {
                            NetMsg msg = list.RemoveIndex(0);

                            if (msg != null)
                            {
                                if (msg.IsReliable()) reliable = true;
                                if (buffer != null)
                                {
                                    //CopyMemory(p, msg.Data(), msg.Length());
                                    //p[1] = msg.Length();
                                    //p += msg.Length();
                                    Array.Copy(msg.Data(), 0, p, offset, msg.Length());
                                    Array.Copy(BitConverter.GetBytes((byte)msg.Length()), 0, p, offset + 1, sizeof(byte));
                                    offset += msg.Length();
                                }
                                // delete msg;
                            }
                        }

                        if (buffer != null)
                        {
                            string user_data = System.Text.Encoding.UTF8.GetString(buffer, 0, xmit_size);
                            int retries = 0;

                            if (reliable)
                                retries = 5;

                            if (is_multi)
                                retries = 10;

                            send_size -= xmit_size;

                            hist_size[hist_indx] = xmit_size + UDP_HEADER_SIZE;
                            hist_time[hist_indx] = NetLayer.GetTime();
                            hist_indx++;

                            if (hist_indx >= HIST_SIZE)
                                hist_indx = 0;

                            g = new NetGram(addr, user_data, retries);
                            //delete[] buffer;
                        }
                    }

                    // the next msg is too big to fit in a single packet
                    else
                    {
                        NetMsg m = send_list.RemoveIndex(0);
                        send_size -= m.Length();
                        //delete m;
                    }
                }
            }

            return g;
        }
        public bool ReceiveGram(NetGram g, List<NetMsg> q = null)
        {
            if (g != null)
            {
                if (max_qsize > 0 && recv_size + g.Size() > max_qsize)
                {
                    status = STATUS.RECV_OVERFLOW;
                    //delete g;
                    return false;
                }

                sequence = g.Sequence();
                recv_size += g.Size() - NetGram.NET_GRAM_HEADER_SIZE;

                // PARSE THE BLOCKS:
                byte[] p = g.UserData();
                int offset = 0;

                while (offset < g.Size())
                {
                    byte block_type = p[0];
                    byte block_size = p[1];

                    if (block_type == 0 || block_size == 0)
                        break;

                    NetMsg msg = new NetMsg(netid, p, block_size);

                    if (msg != null)
                    {
                        if (msg.Type() < NetMsg.TYPES.RESERVED)
                        {
                            msg.SetSequence(sequence);

                            recv_list.InsertIntoSortedList(msg);

                            if (q != null)
                                q.InsertIntoSortedList(msg);

                            offset += block_size;
                        }

                        else if (msg.Type() == NetMsg.TYPES.MULTIPART)
                        {
                            multi_recv_list.InsertIntoSortedList(msg);
                            offset += block_size;

                            CheckMultiRecv(q);
                        }
                    }
                }

                last_recv_time = NetLayer.GetUTC();

                //delete g;
                return true;
            }

            return false;
        }

        public NetAddr Address() { return addr; }
        public ulong NetID() { return netid; }
        public ulong Sequence() { return sequence; }

        public int GetMaxPPS() { return pps; }
        public void SetMaxPPS(int p) { pps = p; }
        public int GetMaxBPS() { return bps; }
        public void SetMaxBPS(int b) { bps = b; }
        public int GetMaxQSize() { return max_qsize; }
        public void SetMaxQSize(int q) { max_qsize = q; }

        public long GetChunkSize() { return chunk_size; }
        public void SetChunkSize(DWORD s) { chunk_size = s; }

        public long LastReceiveTime() { return last_recv_time; }
        public void SetLastReceiveTime(long t) { last_recv_time = t; }


        private bool OKtoSend()
        {
            if (pps != 0 || bps != 0)
            {
                long hist_total = 0;
                long hist_count = 0;
                long now = NetLayer.GetTime();
                long hist_oldest = now;
                long hist_newest = 0;

                for (int i = 0; i < HIST_SIZE; i++)
                {
                    if (hist_size[i] > 0)
                    {
                        hist_total += hist_size[i];
                        hist_count++;
                    }

                    if (hist_time[i] > 0)
                    {
                        if (hist_time[i] < hist_oldest)
                            hist_oldest = hist_time[i];

                        if (hist_time[i] > hist_newest)
                            hist_newest = hist_time[i];
                    }
                }

                if (now - hist_newest < (DWORD)pps)
                    return false;

                long delta = now - hist_oldest;
                DWORD avg_bps = (uint)(hist_total / delta);

                if (bps > 0 && avg_bps > (DWORD)bps)
                    return false;
            }

            return true;
        }

        private void CheckMultiRecv(List<NetMsg> q)
        {
            const int MAX_SIMULTANEOUS_MULTI_SEQUENCES = 8;

            PacketAssembly[] assy = new PacketAssembly[MAX_SIMULTANEOUS_MULTI_SEQUENCES];
            //ZeroMemory(assy, sizeof(assy));

            byte header_size = (byte)NetMsg.NetMsgMultipart.HeaderSize();

            // Catalog how much of each multipart sequence has been received:
            for (int i = 0; i < multi_recv_list.Count; i++)
            {
                NetMsg msg = multi_recv_list[i];
                NetMsg.NetMsgMultipart m = (NetMsg.NetMsgMultipart)msg.Data();

                for (int n = 0; n < MAX_SIMULTANEOUS_MULTI_SEQUENCES; n++)
                {
                    PacketAssembly a = assy[n];

                    if (a.msgid == 0 || (a.msgid == m.msgid && a.netid == msg.NetID()))
                    {
                        a.msgid = m.msgid;
                        a.netid = msg.NetID();
                        a.nreq = m.nparts;
                        a.nparts += 1;
                        a.nbytes += m.len - header_size;
                        break;
                    }
                    assy[n] = a;
                }
            }

            for (int n = 0; n < MAX_SIMULTANEOUS_MULTI_SEQUENCES; n++)
            {
                PacketAssembly a = assy[n];

                // is this sequence complete?
                if (a.msgid != 0 && a.nparts == a.nreq)
                {
                    BYTE[] buffer = new byte[a.nbytes];
                    //BYTE[] p = buffer;
                    int offset = 0;
                    WORD nid = 0;

                    foreach (NetMsg iter in multi_recv_list)
                    {
                        netid = iter.NetID();
                        NetMsg.NetMsgMultipart m = (NetMsg.NetMsgMultipart)iter.Data();

                        // found part of the sequence
                        if (m.msgid == a.msgid && netid == a.netid)
                        {
                            // copy it into the buffer
                            // CopyMemory(p, m.payload, m.len - header_size);
                            //p += m.len - header_size;
                            Array.Copy(m.payload, 0, buffer, offset, (int)(m.len - header_size));
                            offset += (int)(m.len - header_size);

                            // delete iter.removeItem();
                        }
                    }

                    NetMsg msg = new NetMsg(netid, buffer, a.nbytes, NetMsg.FLAGS.RELIABLE);
                    if (msg != null)
                    {
                        recv_list.InsertIntoSortedList(msg);

                        if (q != null)
                            q.InsertIntoSortedList(msg);
                    }
                }
            }
        }

        private NetAddr addr;          // remote network address
        private uint sequence;        // highest packet id received
        private uint netid;           // unique id for this peer
        private int pps;               // max packets per second
        private int bps;               // max bits per second
        private int max_qsize;         // max bytes in either queue
        private STATUS status;         // ok or error code
        private uint chunk_size;       // size of multipart message chunk

        private const int HIST_SIZE = 8;

        private long last_recv_time;         // time of last received packet        
        private long[] hist_time = new long[HIST_SIZE];   // history for pps check
        private long[] hist_size = new long[HIST_SIZE];   // history for bps check
        private int hist_indx;              // index into history

        private int send_size;     // total bytes in send list
        private int recv_size;     // total bytes in recv list
        private List<NetMsg> send_list;     // queue of messages waiting to be sent
        private List<NetMsg> recv_list;     // queue of messages waiting to be read

        private List<NetMsg> multi_send_list;
        private List<NetMsg> multi_recv_list;

        private Object sync = new object();

        private const int MULTIPART_CHUNKSIZE = 232;
        private const int MULTIPART_HEADER = 16;
        private const int UDP_HEADER_SIZE = 34;

        private static NetMsg.NetMsgMultipart multi_part_buffer = new NetMsg.NetMsgMultipart();
        private static uint multi_msg_sequence = 1;

        struct PacketAssembly
        {
            public DWORD msgid;
            public DWORD netid;
            public DWORD nreq;
            public int nparts;
            public int nbytes;
        };

    }
}