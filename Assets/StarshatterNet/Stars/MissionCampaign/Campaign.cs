﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Campaign.h/Campaign.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Campaign defines a strategic military scenario.  This class
    owns (or generates) the Mission list that defines the action
    in the campaign.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign.Planning;
using StarshatterNet.Stars.MissionCampaign.Serialization;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning Campaign class is still in development and is not recommended for production.
    public class Campaign : IComparer<Campaign>
    {
        public enum CONSTANTS
        {
            TRAINING_CAMPAIGN = 1,
            DYNAMIC_CAMPAIGN,
            MOD_CAMPAIGN = 100,
            SINGLE_MISSIONS = 1000,
            MULTIPLAYER_MISSIONS,
            CUSTOM_MISSIONS,
        };

        public enum STATUS
        {
            CAMPAIGN_INIT,
            CAMPAIGN_ACTIVE,
            CAMPAIGN_SUCCESS,
            CAMPAIGN_FAILED
        };

        public Campaign(int id, string name, string path = null)
        {
            this.campaign_id = id;
            this.name = name;
            this.mission_id = -1;
            this.mission = null;
            this.net_mission = null;
            this.scripted = false;
            this.sequential = false;
            this.time = 0;
            this.startTime = 0;
            this.loadTime = 0;
            this.player_group = null;
            this.player_unit = null;
            this.status = STATUS.CAMPAIGN_INIT;
            this.lockout = 0;
            this.loaded_from_savegame = false;
            this.path = null;

            Load();
        }



        // operations:
        public virtual void Load()
        {
            // first, unload any existing data:
            Unload();

            if (string.IsNullOrEmpty(path))
            {
                // then load the campaign from files:
                switch (campaign_id)
                {
                    case (int)CONSTANTS.SINGLE_MISSIONS:
                        path = "Missions/";
                        break;
                    case (int)CONSTANTS.CUSTOM_MISSIONS:
                        path = "Mods/Missions/";
                        break;
                    case (int)CONSTANTS.MULTIPLAYER_MISSIONS:
                        path = "Multiplayer/";
                        break;
                    default:
                        path = string.Format("Campaigns/{0:D2}/", campaign_id);
                        break;
                }
            }
            var zonesTmp = CombatZone.Load(path + "zones.json");
            if (zonesTmp != null)
                zones.AddRange(zonesTmp);

            for (int i = 0; i < zones.Count; i++)
            {
                string s = zones[i].System();
                bool found = false;

                for (int n = 0; !found && n < systems.Count; n++)
                {
                    if (s == systems[n].Name())
                        found = true;
                }

                if (!found)
                    systems.Add(Galaxy.GetInstance().GetSystem(s));
            }
            LoadCampaign(path);
            if (campaign_id == (int)CONSTANTS.CUSTOM_MISSIONS)
            {
                LoadCustomMissions(path);
            }
            else
            {
                bool found = false;

                if (LoadMissionList(path))
                    found = true;

                if (LoadTemplateList(path))
                    found = true;

                if (!found)
                {
                    LoadCustomMissions(path);
                }
            }

            // TODO This only works for campaign stored at assets. Not mods
            image[0] = Resources.Load<Texture2D>(path + "image");
            if (image[0] != null)
            {
                image[1] = Resources.Load<Texture2D>(path + "selected");
                image[2] = Resources.Load<Texture2D>(path + "unavail");
                image[3] = Resources.Load<Texture2D>(path + "banner");
            }
        }

        public virtual void Prep()
        {
            // if this is a new campaign,
            // create combatants from roster and template:
            if (IsDynamic() && combatants.Count == 0)
            {
                LoadCampaign(path, true);
            }

            StarSystem.SetBaseTime(loadTime);

            // load scripted missions:
            if (IsScripted() && actions.Count == 0)
            {
                LoadCampaign(path, true);

                foreach (var m in missions)
                {
                    GetMission(m.id);
                }
            }

            CheckPlayerGroup();
        }

        public virtual void Start()
        {
            ErrLogger.PrintLine("Campaign.Start()");

            Prep();

            // create planners:
            CreatePlanners();
            SetStatus(STATUS.CAMPAIGN_ACTIVE);
        }
        public virtual void ExecFrame()
        {
            if (InCutscene())
                return;

            time = Stardate() - startTime;

            if (status < STATUS.CAMPAIGN_ACTIVE)
                return;

            if (IsDynamic())
            {
                bool completed = false;
                foreach (var m in missions)
                {
                    if (m.mission != null && m.mission.IsComplete())
                    {
                        ErrLogger.PrintLine("Campaign.ExecFrame() completed mission {0} '{1}'", m.id, m.name);
                        completed = true;
                    }
                }

                if (completed)
                {
                    ErrLogger.PrintLine("Campaign.ExecFrame() destroying mission list after completion...");
                    missions.Clear();

                    if (player_group == null || player_group.IsFighterGroup())
                        time += 10 * 3600;
                    else
                        time += 20 * 3600;

                    StarSystem.SetBaseTime(startTime + time - Game.GameTime() / 1000.0);
                }
                else
                {
                    for (int i = missions.Count - 1; i >= 0; i--)
                    {
                        var m = missions[i];
                        if (m.start < time && !m.mission.IsActive())
                        {
                            missions.RemoveAt(i);
                            MissionInfo info = m;

                            if (info != null)
                                ErrLogger.PrintLine("Campaign.ExecFrame() deleting expired mission {0} start: {1} current: {2}",
                                                    info.id,
                                                    info.start,
                                                    (int)time);
                            info = null;
                            //delete info;
                        }
                    }
                }

                // PLAN EVENT MUST BE FIRST PLANNER:
                if (loaded_from_savegame && planners.Count > 0)
                {
                    CampaignPlanEvent plan_event = (CampaignPlanEvent)planners.First();
                    plan_event.ExecScriptedEvents();

                    loaded_from_savegame = false;
                }

                foreach (var plan in planners)
                {
                    CheckPlayerGroup();
                    plan.ExecFrame();
                }

                CheckPlayerGroup();

                // Auto save game AFTER planners have run!
                // This is done to ensure that campaign status
                // is properly recorded after winning or losing
                // the campaign.

                if (completed)
                {
                    CampaignSaveGame save = new CampaignSaveGame(this);
                    save.SaveAuto();
                }
            }
            else
            {
                // PLAN EVENT MUST BE FIRST PLANNER:
                if (planners.Count > 0)
                {
                    CampaignPlanEvent plan_event = (CampaignPlanEvent)planners.First();
                    plan_event.ExecScriptedEvents();
                }
            }
        }
        public virtual void Unload()
        {
            SetStatus(STATUS.CAMPAIGN_INIT);

            Game.ResetGameTime();
            StarSystem.SetBaseTime(0);

            startTime = Stardate();
            loadTime = startTime;
            lockout = 0;

            for (int i = 0; i < NUM_IMAGES; i++)
            {
                if (image[i] != null)
                    image[i] = null;
                //image[i].ClearImage();
            }
            Clear();

            zones.Clear();
        }

        public virtual void Clear()
        {
            missions.Clear();
            planners.Clear();
            combatants.Clear();
            events.Clear();
            actions.Clear();

            player_group = null;
            player_unit = null;

            updateTime = time;
        }

        public virtual void CommitExpiredActions()
        {
            foreach (CombatAction a in actions)
            {

                if (a.IsAvailable())
                    a.SetStatus(CombatAction.STATUS.COMPLETE);
            }

            updateTime = time;
        }
        public virtual void LockoutEvents(int seconds)
        {
            lockout = seconds;
        }
        public virtual void CheckPlayerGroup()
        {
            if (player_group == null || player_group.IsReserve() || player_group.CalcValue() < 1)
            {
                int player_iff = GetPlayerIFF();
                player_group = null;

                CombatGroup force = null;
                for (int i = 0; i < combatants.Count; i++)
                {
                    if (combatants[i].GetIFF() == player_iff)
                    {
                        force = combatants[i].GetForce();
                        break;
                    }
                }

                if (force != null)
                {
                    force.CalcValue();
                    SelectDefaultPlayerGroup(force, (int)CombatGroup.GROUP_TYPE.WING);

                    if (player_group == null)
                        SelectDefaultPlayerGroup(force, (int)CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON);
                }
            }

            if (player_unit != null && player_unit.GetValue() < 1)
                SetPlayerUnit(null);
        }
        public void CreatePlanners()
        {
            if (planners.Count > 0)
                planners.Clear();

            CampaignPlan p;

            // PLAN EVENT MUST BE FIRST PLANNER:
            p = new CampaignPlanEvent(this);
            if (p != null)
                planners.Add(p);

            p = new CampaignPlanStrategic(this);
            if (p != null)
                planners.Add(p);

            p = new CampaignPlanAssignment(this);
            if (p != null)
                planners.Add(p);

            p = new CampaignPlanMovement(this);
            if (p != null)
                planners.Add(p);

            p = new CampaignPlanMission(this);
            if (p != null)
                planners.Add(p);

            if (lockout > 0 && planners.Count > 0)
            {
                foreach (var plan in planners)
                    plan.SetLockout(lockout);
            }
        }

        // accessors:
        public string Name() { return name; }
        public string Description() { return description; }
        public string Path() { return path; }

        public string Situation() { return situation; }
        public string Orders() { return orders; }

        public void SetSituation(string s) { situation = s; }
        public void SetOrders(string o) { orders = o; }

        public int GetPlayerTeamScore()
        {
            int score_us = 0;
            int score_them = 0;

            if (player_group != null)
            {
                int iff = player_group.GetIFF();

                foreach (Combatant c in combatants)
                {

                    if (iff <= 1)
                    {
                        if (c.GetIFF() <= 1)
                            score_us += c.Score();
                        else
                            score_them += c.Score();
                    }

                    else
                    {
                        if (c.GetIFF() <= 1)
                            score_them += c.Score();
                        else
                            score_us += c.Score();
                    }
                }
            }

            return score_us - score_them;
        }
        public List<MissionInfo> GetMissionList() { return missions; }
        public List<Combatant> GetCombatants() { return combatants; }
        public List<CombatZone> GetZones() { return zones; }
        public List<StarSystem> GetSystemList() { return systems; }
        public List<CombatAction> GetActions() { return actions; }
        public List<CombatEvent> GetEvents() { return events; }
        public CombatEvent GetLastEvent()
        {
            CombatEvent result = null;

            if (events.Count > 0)
                result = events.Last();

            return result;
        }

        public CombatAction FindAction(int action_id)
        {
            foreach (CombatAction a in actions)
            {

                if (a.Identity() == action_id)
                    return a;
            }

            return null;
        }


        public int CountNewEvents()
        {
            int result = 0;

            for (int i = 0; i < events.Count; i++)
                if (/*events[i].Source() != CombatEvent::TACNET &&*/ !events[i].Visited())
                    result++;

            return result;
        }

        public int GetPlayerIFF()
        {
            int iff = 1;

            if (player_group != null)
                iff = player_group.GetIFF();

            return iff;
        }
        public CombatGroup GetPlayerGroup() { return player_group; }
        public void SetPlayerGroup(CombatGroup pg)
        {
            if (player_group != pg)
            {
                ErrLogger.PrintLine("Campaign::SetPlayerGroup({0})", pg != null ? pg.Name() : "0");

                // should verify that the player group is
                // actually part of this campaign, first!

                player_group = pg;
                player_unit = null;

                // need to regenerate missions when changing
                // player combat group:
                if (IsDynamic())
                {
                    ErrLogger.PrintLine("  destroying mission list...");
                    missions.Clear();
                }
            }
        }
        public CombatUnit GetPlayerUnit() { return player_unit; }
        public void SetPlayerUnit(CombatUnit unit)
        {
            if (player_unit != unit)
            {
                ErrLogger.PrintLine("Campaign::SetPlayerUnit({0})\n", unit != null ? unit.Name() : "0");

                // should verify that the player unit is
                // actually part of this campaign, first!

                player_unit = unit;

                if (unit != null)
                    player_group = unit.GetCombatGroup();

                // need to regenerate missions when changing
                // player combat unit:
                if (IsDynamic())
                {
                    ErrLogger.PrintLine("  destroying mission list...");
                    missions.Clear();
                }
            }
        }

        public Combatant GetCombatant(string cname)
        {
            foreach (Combatant c in combatants)
            {
                if (c.Name() == cname)
                    return c;
            }

            return null;
        }
        private static CombatGroup FindGroup(CombatGroup g, CombatGroup.GROUP_TYPE type, int id)
        {
            if (g.Type() == type && g.GetID() == id)
                return g;

            CombatGroup result = null;

            foreach (var subgroup in g.GetComponents())
            {
                result = FindGroup(subgroup, type, id);
                break;
            }
            return result;
        }
        public CombatGroup FindGroup(int iff, CombatGroup.GROUP_TYPE type, int id)
        {
            CombatGroup result = null;

            foreach (Combatant combatant in combatants)
            {
                if (combatant.GetIFF() == iff)
                {
                    result = FindGroup(combatant.GetForce(), type, id);
                    break;
                }
            }

            return result;
        }
        static void FindGroups(CombatGroup g, CombatGroup.GROUP_TYPE type, CombatGroup near_group, List<CombatGroup> groups)
        {
            if (g.Type() == type && g.IntelLevel() > Intel.INTEL_TYPE.RESERVE)
            {
                if (near_group == null || g.GetAssignedZone() == near_group.GetAssignedZone())
                    groups.Add(g);
            }

            foreach (CombatGroup subgroup in g.GetComponents())
                FindGroups(subgroup, type, near_group, groups);
        }
        public CombatGroup FindGroup(int iff, CombatGroup.GROUP_TYPE type, CombatGroup near_group = null)
        {
            CombatGroup result = null;
            List<CombatGroup> groups = new List<CombatGroup>();

            foreach (Combatant combatant in combatants)
            {
                if (combatant.GetIFF() == iff)
                {
                    FindGroups(combatant.GetForce(), type, near_group, groups);
                }
            }

            if (groups.Count > 0)
            {
                int index = (int)RandomHelper.Random(0, groups.Count);
                if (index >= groups.Count) index = groups.Count - 1;
                result = groups[index];
            }

            return result;
        }
        static void FindStrikeTargets(CombatGroup g, CombatGroup strike_group, List<CombatGroup> groups)
        {
            if (strike_group == null || strike_group.GetAssignedZone() == null) return;

            if (g.IsStrikeTarget() && g.IntelLevel() > Intel.INTEL_TYPE.RESERVE)
            {
                if (strike_group.GetAssignedZone() == g.GetAssignedZone() ||
                        strike_group.GetAssignedZone().HasRegion(g.GetRegion()))
                    groups.Add(g);
            }

            foreach (CombatGroup subgroup in g.GetComponents())
                FindStrikeTargets(subgroup, strike_group, groups);
        }
        public CombatGroup FindStrikeTarget(int iff, CombatGroup strike_group)
        {
            CombatGroup result = null;

            List<CombatGroup> groups = new List<CombatGroup>();

            foreach (Combatant combatant in GetCombatants())
            {
                if (combatant.GetIFF() != 0 && combatant.GetIFF() != iff)
                {
                    FindStrikeTargets(combatant.GetForce(), strike_group, groups);
                }
            }

            if (groups.Count > 0)
            {
                int index = RandomHelper.Rand() % groups.Count;
                result = groups[index];
            }

            return result;
        }

        public StarSystem GetSystem(string sys)
        {
            return Galaxy.GetInstance().GetSystem(sys);
        }
        public CombatZone GetZone(string rgn)
        {
            foreach (CombatZone z in zones)
            {
                if (z.HasRegion(rgn))
                    return z;
            }

            return null;
        }
        public MissionInfo CreateNewMission()
        {
            int id = 0;
            int maxid = 0;
            MissionInfo info = null;

            if (campaign_id == (int)CONSTANTS.MULTIPLAYER_MISSIONS)
                maxid = 10;

            for (int i = 0; info == null && i < missions.Count; i++)
            {
                MissionInfo m = missions[i];

                if (m.id > maxid)
                    maxid = m.id;
            }

            string filename = string.Format("custom{0:3}.def", maxid + 1);

            info = new MissionInfo();
            if (info != null)
            {
                info.id = maxid + 1;
                info.name = "New Custom Mission";
                info.script = filename;
                info.mission = new Mission(info.id, filename, path);
                info.mission.SetName(info.name);

                //info.script.setSensitive(false);

                missions.Add(info);
            }

            return info;
        }
        public void DeleteMission(int id)
        {
            if (id < 0)
            {
                ErrLogger.PrintLine("ERROR - Campaign::DeleteMission({0}) invalid mission id", id);
                return;
            }

            MissionInfo m = null;
            int index = -1;

            for (int i = 0; m == null && i < missions.Count; i++)
            {
                if (missions[i].id == id)
                {
                    m = missions[i];
                    index = i;
                }
            }

            if (m != null)
            {
                string full_path;

                if (path.EndsWith("/"))
                    full_path = path + m.script;
                else
                    full_path = path + "/" + m.script;

                DeleteFile(full_path);
                Load();
            }

            else
            {
                ErrLogger.PrintLine("ERROR - Campaign::DeleteMission({0}) could not find mission", id);
            }
        }

        private void DeleteFile(string full_path)
        {
            throw new NotImplementedException();
        }

        public Mission GetMission()
        {
            return GetMission(mission_id);
        }
        public Mission GetMission(int id)
        {
            if (id < 0)
            {
                ErrLogger.PrintLine("ERROR - Campaign::GetMission({0}) invalid mission id", id);
                return null;
            }

            if (mission != null && mission.Identity() == id)
            {
                return mission;
            }

            MissionInfo info = null;
            for (int i = 0; info == null && i < missions.Count; i++)
                if (missions[i].id == id)
                    info = missions[i];

            if (info != null)
            {
                if (info.mission == null)
                {
                    ErrLogger.PrintLine("Campaign::GetMission({0}) loading mission...", id);
                    info.mission = new Mission(id, info.script, path);
                    if (info.mission != null)
                        info.mission.Load();
                }

                if (IsDynamic())
                {
                    if (info.mission != null)
                    {
                        if (info.mission.Situation() == "Unknown")
                        {
                            ErrLogger.PrintLine("Campaign::GetMission({0}) generating sitrep...", id);
                            CampaignSituationReport sitrep = new CampaignSituationReport(this, info.mission);
                            sitrep.GenerateSituationReport();
                        }
                    }
                    else
                    {
                        ErrLogger.PrintLine("Campaign::GetMission(%d) could not find/load mission.\n", id);
                    }
                }

                return info.mission;
            }

            return null;
        }
        public Mission GetMissionByFile(string filename)
        {
            if (string.IsNullOrEmpty(filename))
            {
                ErrLogger.PrintLine("ERROR - Campaign::GetMissionByFile() invalid filename");
                return null;
            }

            int id = 0;
            int maxid = 0;
            MissionInfo info = null;

            for (int i = 0; info == null && i < missions.Count; i++)
            {
                MissionInfo m = missions[i];

                if (m.id > maxid)
                    maxid = m.id;

                if (m.script == filename)
                    info = m;
            }

            if (info != null)
            {
                id = info.id;

                if (info.mission == null)
                {
                    ErrLogger.PrintLine("Campaign::GetMission({0}) loading mission...", id);
                    info.mission = new Mission(id, info.script, path);
                    if (info.mission != null)
                        info.mission.Load();
                }

                if (IsDynamic())
                {
                    if (info.mission != null)
                    {
                        if (info.mission.Situation() == "Unknown")
                        {
                            ErrLogger.PrintLine("Campaign::GetMission({0}) generating sitrep...", id);
                            CampaignSituationReport sitrep = new CampaignSituationReport(this, info.mission);
                            sitrep.GenerateSituationReport();
                        }
                    }
                    else
                    {
                        ErrLogger.PrintLine("Campaign::GetMission({0}) could not find/load mission.", id);
                    }
                }
            }

            else
            {
                info = new MissionInfo();
                if (info != null)
                {
                    info.id = maxid + 1;
                    info.name = "New Custom Mission";
                    info.script = filename;
                    info.mission = new Mission(info.id, info.script, "Mods/Missions/");
                    info.mission.SetName(info.name);

                    //info.script.setSensitive(false);

                    missions.Add(info);
                }
            }

            return info.mission;
        }
        public MissionInfo GetMissionInfo(int id)
        {
            if (id < 0)
            {
                ErrLogger.PrintLine("ERROR - Campaign::GetMissionInfo({0}) invalid mission id", id);
                return null;
            }

            MissionInfo m = null;
            for (int i = 0; m == null && i < missions.Count; i++)
                if (missions[i].id == id)
                    m = missions[i];

            if (m != null)
            {
                if (m.mission == null)
                {
                    m.mission = new Mission(id, m.script);
                    if (m.mission != null)
                        m.mission.Load();
                }

                return m;
            }

            else
            {
                ErrLogger.PrintLine("ERROR - Campaign::GetMissionInfo({0}) could not find mission", id);
            }

            return null;
        }
        public MissionInfo FindMissionTemplate(Mission.TYPE mission_type, CombatGroup player_group)
        {
            MissionInfo info = null;

            if (player_group == null)
                return info;

            TemplateList templates = GetTemplateList(mission_type, (int)player_group.Type());

            if (templates == null || templates.missions.Count == 0)
                return info;

            int tries = 0;
            int msize = templates.missions.Count;

            while (info == null && tries < msize)
            {
                // get next template:
                int index = templates.index;

                if (index >= msize)
                    index = 0;

                info = templates.missions[index];
                templates.index = index + 1;
                tries++;

                // validate the template:
                if (info != null)
                {
                    if (info.action_id != 0)
                    {
                        CombatAction a = FindAction(info.action_id);

                        if (a != null && (int)a.Status() != info.action_status)
                            info = null;
                    }

                    if (info != null && !info.IsAvailable())
                    {
                        info = null;
                    }
                }
            }

            return info;
        }
        public void ReloadMission(int id)
        {
            if (mission != null && mission == net_mission)
            {
                //delete net_mission;
                net_mission = null;
            }

            mission = null;

            if (id >= 0 && id < missions.Count)
            {
                MissionInfo m = missions[id];
                //delete m.mission;
                m.mission = null;
            }
        }

        public void LoadNetMission(int id, string net_mission) { throw new System.NotImplementedException(); }
        public void StartMission()
        {
            Mission m = GetMission();

            if (m != null)
            {
                ErrLogger.PrintLine("\n\nCampaign Start Mission - {0}. '{1}'", m.Identity(), m.Name());

                if (!scripted)
                {
                    //FPU2Extended();

                    double gtime = (double)Game.GameTime() / 1000.0;
                    double base_ = startTime + m.Start() - 15 - gtime;

                    StarSystem.SetBaseTime(base_);

                    double current_time = Stardate() - startTime;

                    string buffer;
                    FormatUtil.FormatDayTime(out buffer, current_time);
                    ErrLogger.PrintLine("  current time:  {0}", buffer);

                    FormatUtil.FormatDayTime(out buffer, m.Start());
                    ErrLogger.PrintLine("  mission start: {0}", buffer);
                    ErrLogger.PrintLine("");
                }
            }
        }


        public void RollbackMission()
        {
            ErrLogger.PrintLine("Campaign::RollbackMission()");

            Mission m = GetMission();

            if (m != null)
            {
                if (!scripted)
                {
                    //FPU2Extended();

                    double gtime = (double)Game.GameTime() / 1000.0;
                    double base_ = startTime + m.Start() - 60 - gtime;

                    StarSystem.SetBaseTime(base_);

                    double current_time = Stardate() - startTime;
                    ErrLogger.PrintLine("  mission start: {0}", m.Start());
                    ErrLogger.PrintLine("  current time:  {0}", (int)current_time);
                }

                m.SetActive(false);
                m.SetComplete(false);
            }
        }

        public int GetCampaignId() { return campaign_id; }
        public void SetMissionId(int id)
        {
            ErrLogger.PrintLine("Campaign::SetMissionId({0})", id);

            if (id > 0)
                mission_id = id;
            else
                ErrLogger.PrintLine("   retaining mission id = {0}", mission_id);
        }
        public int GetMissionId() { return mission_id; }
        public Texture2D GetImage(int n) { return image[n]; }
        public double GetTime() { return time; }
        public double GetStartTime() { return startTime; }
        public void SetStartTime(double t) { startTime = t; }
        public double GetLoadTime() { return loadTime; }
        public void SetLoadTime(double t) { loadTime = t; }
        public double GetUpdateTime() { return updateTime; }
        public void SetUpdateTime(double t) { updateTime = t; }

        public bool InCutscene()
        {
            Starshatter stars = Starshatter.GetInstance();
            return stars != null ? stars.InCutscene() : false;
        }
        public bool IsDynamic()
        {
            return campaign_id >= (int)CONSTANTS.DYNAMIC_CAMPAIGN &&
            campaign_id < (int)CONSTANTS.SINGLE_MISSIONS;
        }
        public bool IsTraining()
        {
            return campaign_id == (int)CONSTANTS.TRAINING_CAMPAIGN;
        }
        public bool IsScripted()
        {
            return scripted;
        }
        public bool IsSequential()
        {
            return sequential;
        }
        public bool IsSaveGame() { return loaded_from_savegame; }
        public void SetSaveGame(bool s) { loaded_from_savegame = s; }

        public bool IsActive() { return status == STATUS.CAMPAIGN_ACTIVE; }
        public bool IsComplete() { return status == STATUS.CAMPAIGN_SUCCESS; }
        public bool IsFailed() { return status == STATUS.CAMPAIGN_FAILED; }

        public void SetStatus(STATUS s)
        {
            status = s;

            // record the win in player profile:
            if (status == STATUS.CAMPAIGN_SUCCESS)
            {
                Player player = Player.GetCurrentPlayer();

                if (player != null)
                    player.SetCampaignComplete(campaign_id);
            }

            if (status > STATUS.CAMPAIGN_ACTIVE)
            {
                ErrLogger.PrintLine("INFO: Campaign.SetStatus() destroying mission list at campaign end");
                missions.Clear();
            }
        }

        public STATUS GetStatus() { return status; }
        static void GetCombatUnits(CombatGroup g, List<CombatUnit> units)
        {
            if (g != null)
            {
                foreach (CombatUnit u in g.GetUnits())
                {

                    if (u.Count() - u.DeadCount() > 0)
                        units.Add(u);
                }

                foreach (CombatGroup g2 in g.GetComponents())
                {
                    if (!g2.IsReserve())
                        GetCombatUnits(g2, units);
                }
            }
        }
        public int GetAllCombatUnits(int iff, List<CombatUnit> units)
        {
            units.Clear();

            foreach (Combatant c in combatants)
            {
                if (iff < 0 || c.GetIFF() == iff)
                {
                    GetCombatUnits(c.GetForce(), units);
                }
            }

            return units.Count;
        }


        public static void Initialize()
        {
            const string assetsdir = "Assets/Resources/";
            const string cmpdir = "Campaigns/";
            Campaign c;
            string[] files = ReaderSupport.GetAssetFiles(assetsdir + cmpdir, "campaign.json").ToArray<string>();
            for (int i = 1; i < 100; i++)
            {
                var path = string.Format("Campaigns/{0:D2}/", i);
                if (files.Any(s => s.Contains(path)))
                {
                    var txt = string.Format("Dynamic Campaign {0:D2}", i);
                    c = new Campaign(i, txt);
                    if (c != null && c.campaign_id != -1)
                    {
                        ErrLogger.PrintLine("INFO: Parsing campaign directory {0}.", path);
                        campaigns.Add(c);
                    }
                }
            }
            c = new Campaign((int)CONSTANTS.SINGLE_MISSIONS, "Single Missions");
            if (c != null && c.campaign_id != -1)
            {
                campaigns.Add(c);
                current_campaign = c;
            }

            c = new Campaign((int)CONSTANTS.MULTIPLAYER_MISSIONS, "Multiplayer Missions");
            if (c != null && c.campaign_id != -1)
            {
                campaigns.Add(c);
            }

            c = new Campaign((int)CONSTANTS.CUSTOM_MISSIONS, "Custom Missions");
            if (c != null && c.campaign_id != -1)
            {
                campaigns.Add(c);
            }
        }

        public static void Close()
        {
            ErrLogger.PrintLine("INFO: Campaign.Close() - destroying all campaigns");
            current_campaign = null;
            campaigns.Clear();
        }


        public static Campaign GetCampaign()
        {
            return current_campaign;
        }

        public static List<Campaign> GetAllCampaigns()
        {
            return campaigns;
        }

        public static int GetLastCampaignId()
        {
            int result = 0;

            foreach (Campaign c in campaigns)
            {
                if (c.IsDynamic() && c.GetCampaignId() > result)
                {
                    result = c.GetCampaignId();
                }
            }

            return result;
        }

        public static Campaign SelectCampaign(string name)
        {
            foreach (Campaign c in campaigns)
            {
                if (c.name == name)
                {
                    ErrLogger.PrintLine("INFO: Campaign: Selected '{0}'", c.Name());
                    current_campaign = c;
                    return c;
                }
            }

            ErrLogger.PrintLine("ERROR: Campaign: could not find '{0}'", name);
            return null;
        }

        public static Campaign CreateCustomCampaign(string name, string path)
        {
            int id = 0;

            if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(path))
            {
                foreach (Campaign c in campaigns)
                {
                    if (c.GetCampaignId() >= id)
                        id = c.GetCampaignId() + 1;

                    if (c.Name() == name)
                    {
                        ErrLogger.PrintLine("Campaign: custom campaign '{0}' already exists.", name);
                        return null;
                    }
                }
            }

            if (id == 0)
                id = (int)CONSTANTS.CUSTOM_MISSIONS + 1;

            Campaign camp = new Campaign(id, name, path);
            ErrLogger.PrintLine("Campaign: created custom campaign {0} '{1}'\n", id, name);
            campaigns.Add(camp);

            return camp;
        }

        public static double Stardate()
        {
            return StarSystem.Stardate();
        }


        protected void LoadCampaign(string path, bool full = false)
        {
            string filename = path + CAMPAIGNFILENAME;

            var ci = ReaderSupport.DeserializeAsset<CampaignInfo>(filename);
            if (ci == null)
            {
                //ErrLogger.PrintLine("WARNING: Wrong campaign filename in '{0}/{1}'", path, filename);
                //this.campaign_id = -1;
                return;
            }
            if (string.IsNullOrEmpty(ci.name))
                ErrLogger.PrintLine("WARNING: name missing in '{0}/{1}'", path, filename);
            this.name = ContentBundle.Instance.GetText(ci.name);

            if (ci.desc == null || ci.desc.Count == 0)
                ErrLogger.PrintLine("WARNING: description missing in '{0}/{1}'", path, filename);
            this.description = ContentBundle.Instance.GetText(String.Join("", ci.desc.ToArray()));

            if (ci.situation == null || ci.situation.Count == 0)
                ErrLogger.PrintLine("WARNING: situation missing in '{0}/{1}'", path, filename);
            this.situation = ContentBundle.Instance.GetText(String.Join("", ci.situation.ToArray()));

            if (ci.orders == null || ci.orders.Count == 0)
                ErrLogger.PrintLine("WARNING: orders missing in '{0}/{1}'", path, filename);
            this.orders = ContentBundle.Instance.GetText(String.Join("", ci.orders.ToArray()));

            this.scripted = ci.scripted;
            this.sequential = ci.sequential;

            if (ci.combatants == null || ci.combatants.Count == 0)
                ErrLogger.PrintLine("WARNING: combatant struct missing in '{0}/{1}'", path, filename);

            foreach (CombatantInfo citem in ci.combatants)
            {
                CombatGroup clone = null;
                CombatGroup force = CombatRoster.GetInstance().GetForce(citem.name);
                if (force != null)
                    clone = force.Clone(false);
                if (citem.groups != null)
                    foreach (var g in citem.groups)
                        ParseGroup(g, force, clone);
                Combatant c = new Combatant(citem.name, clone);
                if (c != null)
                {
                    combatants.Add(c);
                }
                else
                {
                    Unload();
                    return;
                }
            }

            if (ci.actions == null || ci.actions.Count == 0)
                ErrLogger.PrintLine("WARNING: action struct missing in '{0}/{1}'", path, filename);
            foreach (var action in ci.actions)
                ParseAction(action);


        }

        private void ParseGroup(CombatantGroupInfo cgi, CombatGroup force, CombatGroup clone)
        {
            if (cgi.type != null && cgi.id != 0)
            {
                var type = CombatGroup.TypeFromName(cgi.type);
                CombatGroup g = force.FindGroup(type, cgi.id);

                // found original group, now clone it over
                if (g != null && g.GetParent() != null)
                {
                    CombatGroup parent = CloneOver(force, clone, g.GetParent());
                    parent.AddComponent(g.Clone());
                }
            }
        }

        private void ParseAction(ActionInfo val)
        {
            int id = 0;
            CombatAction.TYPE type = 0;
            int subtype = 0;
            int opp_type = -1;
            int team = 0;
            CombatEvent.EVENT_SOURCE source = 0;
            Point loc = new Point(0.0f, 0.0f, 0.0f);
            string system = null;
            string region = null;
            string file = null;
            string image = null;
            string scene = null;
            string text = null;
            int count = 1;
            int start_before = TIME_NEVER;
            int start_after = 0;
            int min_rank = 0;
            int max_rank = 100;
            int delay = 0;
            int probability = 100;

            int asset_type = 0;
            int asset_id = 0;
            int target_type = 0;
            int target_id = 0;
            int target_iff = 0;

            CombatAction action = null;

            if (val.id != 0)
                id = val.id;

            if (!string.IsNullOrWhiteSpace(val.type))
                type = CombatAction.TypeFromName(val.type);
            if (!string.IsNullOrWhiteSpace(val.subtype))
                if (!int.TryParse(val.subtype, out subtype))
                {
                    if (type == CombatAction.TYPE.MISSION_TEMPLATE)
                    {
                        subtype = (int)Mission.TypeFromName(val.subtype);
                    }
                    else if (type == CombatAction.TYPE.COMBAT_EVENT)
                    {
                        subtype = (int)CombatEvent.TypeFromName(val.subtype);
                    }
                    else if (type == CombatAction.TYPE.INTEL_EVENT)
                    {
                        subtype = (int)Intel.IntelFromName(val.subtype);
                    }
                }

            if (!string.IsNullOrWhiteSpace(val.opp_type))
                if (!int.TryParse(val.opp_type, out opp_type))
                {
                    opp_type = (int)Mission.TypeFromName(val.opp_type);
                }

            source = CombatEvent.SourceFromName(val.source);

            if (val.team != 0)
                team = val.team;
            if (val.iff != 0)
                team = val.iff;
            if (val.count != 0)
                count = val.count;

            if (!string.IsNullOrWhiteSpace(val.before))
                if (!int.TryParse(val.before, out start_before))
                {
                    start_before = ReaderSupport.GetDefTime(val.before);
                    start_before -= ONE_DAY;
                }

            if (!string.IsNullOrWhiteSpace(val.after))
                if (!int.TryParse(val.after, out start_after))
                {
                    start_after = ReaderSupport.GetDefTime(val.after);
                    start_after -= ONE_DAY;
                }

            if (!string.IsNullOrWhiteSpace(val.min_rank))
                if (!int.TryParse(val.min_rank, out min_rank))
                    min_rank = Player.RankFromName(val.min_rank);

            if (!string.IsNullOrWhiteSpace(val.max_rank))
                if (!int.TryParse(val.max_rank, out max_rank))
                    max_rank = Player.RankFromName(val.max_rank);

            if (val.delay != 0)
                delay = val.delay;
            if (val.probability != 0)
                delay = val.probability;

            if (!string.IsNullOrWhiteSpace(val.asset_type))
                asset_type = (int)CombatGroup.TypeFromName(val.asset_type);

            if (!string.IsNullOrWhiteSpace(val.target_type))
                target_type = (int)CombatGroup.TypeFromName(val.target_type);

            if (val.location != null)
                loc = val.location.ConvertToPoint();

            if (!string.IsNullOrWhiteSpace(val.system))
                system = val.system;

            if (!string.IsNullOrWhiteSpace(val.region))
                region = val.region;

            if (!string.IsNullOrWhiteSpace(val.file))
                file = val.file;
            if (!string.IsNullOrWhiteSpace(val.image))
                image = val.image;
            if (!string.IsNullOrWhiteSpace(val.scene))
                scene = val.scene;
            if (val.text != null && val.text.Count > 0)
                text = String.Join("", val.text.ToArray());

            if (val.asset_id != 0)
                asset_id = val.asset_id;
            if (val.target_id != 0)
                target_id = val.target_id;
            if (val.target_iff != 0)
                target_iff = val.target_iff;

            if (val.asset_kill != null && val.asset_kill.Count > 0)
            {
                if (action == null)
                    action = new CombatAction(id, type, subtype, team);
                if (action != null)
                    action.AssetKills().Add(String.Join("", val.asset_kill.ToArray()));
            }
            if (val.target_kill != null && val.target_kill.Count > 0)
            {
                if (action == null)
                    action = new CombatAction(id, type, subtype, team);
                if (action != null)
                    action.AssetKills().Add(String.Join("", val.target_kill.ToArray()));
            }
            if (val.reqs != null && val.reqs.Count != 0)
            {
                if (action == null)
                    action = new CombatAction(id, type, subtype, team);

                foreach (var elem in val.reqs)
                {
                    int act = 0;
                    CombatAction.STATUS stat = CombatAction.STATUS.COMPLETE;
                    bool not = false;

                    Combatant c1 = null;
                    Combatant c2 = null;
                    CombatActionReq.COMPARISON_OPERATOR comp = 0;
                    int score = 0;
                    int intel = 0;
                    CombatGroup.GROUP_TYPE gtype = 0;
                    int gid = 0;

                    if (elem.action != 0)
                        act = elem.action;
                    if (!string.IsNullOrWhiteSpace(elem.status))
                        stat = CombatAction.StatusFromName(elem.status);
                    not = elem.not;
                    if (!string.IsNullOrWhiteSpace(elem.c1))
                        c1 = GetCombatant(elem.c1);
                    if (!string.IsNullOrWhiteSpace(elem.c2))
                        c2 = GetCombatant(elem.c2);
                    score = elem.score;
                    if (!string.IsNullOrWhiteSpace(elem.intel))
                        if (!int.TryParse(elem.intel, out intel))
                            intel = (int)Intel.IntelFromName(elem.intel);
                    if (!string.IsNullOrWhiteSpace(elem.group_type))
                        gtype = CombatGroup.TypeFromName(elem.group_type);
                    gid = elem.group_id;

                    if (act != 0)
                        action.AddRequirement(act, stat, not);
                    else if (gtype != 0)
                        action.AddRequirement(c1, gtype, gid, comp, score, intel);
                    else
                        action.AddRequirement(c1, c2, comp, score);
                }
            }

            if (action == null)
                action = new CombatAction(id, type, subtype, team);
            if (action != null)
            {
                action.SetSource(source);
                action.SetOpposingType(opp_type);
                action.SetLocation(loc);
                action.SetSystem(system);
                action.SetRegion(region);
                action.SetFilename(file);
                action.SetImageFile(image);
                action.SetSceneFile(scene);
                action.SetCount(count);
                action.SetStartAfter(start_after);
                action.SetStartBefore(start_before);
                action.SetMinRank(min_rank);
                action.SetMaxRank(max_rank);
                action.SetDelay(delay);
                action.SetProbability(probability);

                action.SetAssetId(asset_id);
                action.SetAssetType(asset_type);
                action.SetTargetId(target_id);
                action.SetTargetIFF(target_iff);
                action.SetTargetType(target_type);
                action.SetText(text);

                actions.Add(action);
            }
        }

        protected bool LoadTemplateList(string fname)
        {
            ErrLogger.PrintLine("WARNING:Campaign.LoadTemplateList is not Implemented");
            return false;
        }
        public bool LoadMissionList(string path, string fname = "missions.json")
        {
            string filename = path + fname;
            ErrLogger.PrintLine("Loading MissionList: {0}", filename);

            //// ....
            var entryList = ReaderSupport.DeserializeAsset<MissionListEntry>(filename);

            if (entryList == null)
            {
                ErrLogger.PrintLine("INFO: No mission list file '{0}'", filename);
                return false;
            }
            foreach (var entry in entryList.MISSIONLIST)
            {
                MissionInfo info = new MissionInfo();
                info.id = entry.id;
                info.name = entry.name;
                info.description = entry.desc;
                if (!string.IsNullOrEmpty(entry.system))
                    info.system = entry.system;
                else
                    info.system = "Unknown";
                if (!string.IsNullOrEmpty(entry.region))
                    info.region = entry.region;
                else
                    info.region = "Unknown";
                info.script = entry.script;
                info.start = ReaderSupport.GetDefTime(entry.start);
                if (!string.IsNullOrEmpty(entry.type))
                    info.type = Mission.TypeFromName(entry.type);
                info.mission = null;

                missions.Add(info);
            }
            return true;
        }

        protected void LoadCustomMissions(string path)
        {
            ErrLogger.PrintLine("WARNING:Campaign.LoadMissionList is not Implemented");
            bool ok = true;
            string[] files = ReaderSupport.GetDataFileList(path, "*.json");
            if (files == null) return;
            foreach (var file in files)
            {
                ErrLogger.PrintLine("Custom Mission File detected: {0}", file);
            }
            if (!ok)
                Unload();
            else
                missions.Sort();
        }

        protected CombatGroup CloneOver(CombatGroup force, CombatGroup clone, CombatGroup group)
        {
            CombatGroup orig_parent = group.GetParent();

            if (orig_parent != null)
            {
                CombatGroup clone_parent = clone.FindGroup(orig_parent.Type(), orig_parent.GetID());

                if (clone_parent == null)
                    clone_parent = CloneOver(force, clone, orig_parent);

                CombatGroup new_clone = clone.FindGroup(group.Type(), group.GetID());

                if (new_clone == null)
                {
                    new_clone = group.Clone(false);
                    clone_parent.AddComponent(new_clone);
                }

                return new_clone;
            }
            else
            {
                return clone;
            }
        }
        protected void SelectDefaultPlayerGroup(CombatGroup g, int type)
        {
            if (player_group != null || g == null) return;

            if ((int)g.Type() == type && !g.IsReserve() && g.Value() > 0)
            {
                player_group = g;
                player_unit = null;
                return;
            }

            for (int i = 0; i < g.GetComponents().Count; i++)
                SelectDefaultPlayerGroup(g.GetComponents()[i], type);
        }
        protected TemplateList GetTemplateList(Mission.TYPE msn_type, int grp_type)
        {
            for (int i = 0; i < templates.Count; i++)
            {
                if (templates[i].mission_type == msn_type &&
                        templates[i].group_type == grp_type)
                    return templates[i];
            }

            return null;
        }



        public override bool Equals(object obj)
        {
            var campaign = obj as Campaign;
            return campaign != null &&
                   name == campaign.name;
        }

        public override int GetHashCode()
        {
            return 363513814 + EqualityComparer<string>.Default.GetHashCode(name);
        }
        public int Compare(Campaign x, Campaign y)
        {
            int compareRst = x.campaign_id - y.campaign_id;
            return compareRst;
        }

        protected const string CAMPAIGNFILENAME = "campaign.json";
        protected const int NUM_IMAGES = 6;

        // attributes:
        protected int campaign_id;
        protected STATUS status;
        protected string filename;
        protected string path;
        protected string name;
        protected string description;
        protected string situation;
        protected string orders;
        protected Texture2D[] image = new Texture2D[NUM_IMAGES];

        protected bool scripted;
        protected bool sequential;
        protected bool loaded_from_savegame;

        protected List<Combatant> combatants = new List<Combatant>();
        protected List<StarSystem> systems = new List<StarSystem>();
        protected List<CombatZone> zones = new List<CombatZone>();
        protected List<CampaignPlan> planners = new List<CampaignPlan>();
        protected List<MissionInfo> missions = new List<MissionInfo>();
        protected List<TemplateList> templates = new List<TemplateList>();
        protected List<CombatAction> actions = new List<CombatAction>();
        protected List<CombatEvent> events = new List<CombatEvent>();
        protected CombatGroup player_group;
        protected CombatUnit player_unit;

        protected int mission_id;
        protected Mission mission;
        protected Mission net_mission;

        protected double time;
        protected double loadTime;
        protected double startTime;
        protected double updateTime;
        protected int lockout;

        private static List<Campaign> campaigns = new List<Campaign>();
        private static Campaign current_campaign = null;
        private const int TIME_NEVER = (int)1e9;
        private const int ONE_DAY = (int)24 * 3600;

    }
}

namespace StarshatterNet.Stars.MissionCampaign.Serialization
{
    [Serializable]
    public class Localization
    {
        public double x;
        public double y;
        public double z;

        public Point ConvertToPoint()
        {
            return new Point((float)(x / 255f), (float)(y / 255f), (float)(z / 255f));
        }
    }

    [Serializable]
    public class CampaignInfo
    {
        public string name;
        public List<string> desc;
        public List<string> situation;
        public List<string> orders;
        public bool scripted;
        public bool sequential;
        public List<ActionInfo> actions;
        public List<CombatantInfo> combatants;
    }

    [Serializable]
    public class ActionInfo
    {
        public int id;
        public string type;
        public string subtype;
        public string opp_type;
        public string source;
        public int team;
        public int iff;
        public int count;
        public string before;
        public string after;
        public string min_rank;
        public string max_rank;
        public int delay;
        public int probability;
        public string asset_type;
        public string target_type;
        public Localization location;
        public string system;
        public string region;
        public string file;
        public string image;
        public string scene;
        public List<string> text;
        public int asset_id;
        public int target_id;
        public int target_iff;
        public List<string> asset_kill;
        public List<string> target_kill;
        public List<ReqInfo> reqs;
    }

    [Serializable]
    public class CombatantInfo
    {
        public string name;
        public List<CombatantGroupInfo> groups;
    }

    [Serializable]
    public class CombatantGroupInfo
    {
        public string type;
        public int id;
    }

    [Serializable]
    public class ReqInfo
    {
        public int action;
        public string status;
        public bool not;
        public string c1;
        public string c2;
        public string comp;
        public int score;
        public string intel;
        public string group_type;
        public int group_id;
    }

    [Serializable]
    public class MissionListEntry
    {
        public MissionEntry[] MISSIONLIST;
    }

    [Serializable]
    public class MissionEntry
    {
        public int id = 0;
        public string name;
        public string desc;
        public string script;
        public string system;
        public string region;
        public string start = null;
        public string type = null;
    }
}