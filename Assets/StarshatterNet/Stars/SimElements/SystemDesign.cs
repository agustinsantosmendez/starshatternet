﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      SystemDesign.h/SystemDesign.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Generic ship System Design class
*/
using StarshatterNet.Gen.Misc;
using System;
using System.Collections.Generic;

namespace StarshatterNet.Stars.SimElements
{
    [Serializable]
    public class SystemDesign
    {
        public const string DefaultFilename = "Sys.json";

        public static void Initialize(string filename = DefaultFilename)
        {
            ErrLogger.PrintLine("Loading System Designs '{0}'", filename);
            try
            {
                catalog = ReaderSupport.DeserializeAsset<SystemDesignCollection>(filename).Catalog;
            }
            catch (Exception e)
            {
                ErrLogger.PrintLine("ERROR: Exception while reading JSON '{0}' : '{1}'", filename, e);
            }
        }

        public static void Close()
        {
            catalog.Clear();
        }

        public static SystemDesign Find(string name)
        {
            return catalog.Find(sd => sd.name == name);
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            // TODO: write your implementation of Equals() here
            SystemDesign other = obj as SystemDesign;
            return this.name.Equals(other.name);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }

        // Unique ID:
        public string name;

        // Sub-components:
        public List<ComponentDesign> components;

        public static List<SystemDesign> catalog = new List<SystemDesign>();

    }

    [Serializable]
    public class SystemDesignCollection
    {
        public List<SystemDesign> Catalog;
    }
}
