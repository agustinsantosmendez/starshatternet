﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      ActiveWindow.h/ActiveWindow.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Active Window class (a window that knows how to draw itself)
*/
using System.Collections.Generic;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Views;
using UnityEngine;
using DWORD = System.Int32;

namespace StarshatterNet.Gen.Forms
{
    public class ActiveWindow : Window
    {
        public enum ETD
        {
            EID_CREATE,
            EID_DESTROY,
            EID_MOUSE_MOVE,
            EID_CLICK,
            EID_SELECT,
            EID_LBUTTON_DOWN,
            EID_LBUTTON_UP,
            EID_RBUTTON_DOWN,
            EID_RBUTTON_UP,
            EID_KEY_DOWN,
            EID_SET_FOCUS,
            EID_KILL_FOCUS,
            EID_MOUSE_ENTER,
            EID_MOUSE_EXIT,
            EID_MOUSE_WHEEL,
            EID_DRAG_START,
            EID_DRAG_DROP,

            EID_USER_1,
            EID_USER_2,
            EID_USER_3,
            EID_USER_4,

            EID_NUM_EVENTS
        }
        public ActiveWindow(Graphics.Screen screen, int ax, int ay, int aw, int ah,
                            DWORD id = 0, DWORD s = 0, ActiveWindow pParent = null) : base(screen, ax, ay, aw, ah)
        {
            rect = new Stars.Rect(ax, ay, aw, ah); shown = true;
            this.id = id; style = s; focus = false;
            enabled = true; text_align = TextFormat.DT_CENTER; single_line = false; alpha = 1;
            texture = null; back_color = sys_back_color; fore_color = sys_fore_color;
            parent = pParent; form = null; transparent = false; topmost = true;
            layout = null; rows = 1; cols = 1;
            fixed_width = 0; fixed_height = 0; hide_partial = true;

            //ZeroMemory(tab, sizeof(tab));
            tab.Initialize();
            font = sys_font;

            if (pParent != null)
            {
                parent.AddChild(this);
            }
            //else
            //{
            //    screen.AddWindow(this);
            //}

            shown = false;
            Show();

            desc = "ActiveWindow " + id;
        }

        public override void Show()
        {
            shown = true;
            if (obj != null)
                obj.Show();

        }
        public override void Hide()
        {
            shown = false;
            if (obj != null)
                obj.Hide();
        }

        public virtual void SetEnabled(bool e = true) { enabled = e; }
        public bool IsEnabled() { return enabled; }
        public bool IsVisible() { return shown; }

        public DWORD GetID() { return id; }
        public void SetStyle(DWORD s) { style = s; }
        public DWORD GetStyle() { return style; }

        public virtual void SetText(string t) { text = t; }
        public void AddText(string t)
        {
            if (!string.IsNullOrEmpty(t))
                text += t;
        }
        public virtual string GetText() { return text; }

        public void SetAltText(string t) { alt_text = t; }
        public string GetAltText() { return alt_text; }

        public void SetTexture(Texture2D bmp) { texture = bmp; }
        public Texture2D GetTexture() { return texture; }
        public void SetMargins(Insets m)
        {
            margins = m;
            CalcGrid();
        }
        public Insets GetMargins() { return margins; }
        public void SetTextInsets(Insets t)
        {
            text_insets = t;
        }
        public Insets GetTextInsets() { return text_insets; }

        public virtual ActiveWindow FindControl(int x, int y) { return null; }
        public virtual Stars.Rect TargetRect()
        {
            return rect;
        }

        public virtual ActiveWindow FindChild(DWORD id)
        {
            foreach (ActiveWindow w in children)
            {
                if (w.GetID() == id)
                    return w;

                ActiveWindow w2 = w.FindChild(id);
                if (w2 != null)
                    return w2;
            }

            return null;
        }

        public virtual ActiveWindow FindChild(int x, int y)
        {
            ActiveWindow mouse_tgt = null;

            foreach (ActiveWindow test in children)
            {
                if (test.TargetRect().Contains(x, y))
                    mouse_tgt = test;
            }

            return mouse_tgt;
        }

        public List<ActiveWindow> GetChildren() { return children; }
        public void SetCellInsets(Insets c)
        {
            cell_insets = c;
        }
        public Insets GetCellInsets() { return cell_insets; }
        public void SetCells(int cx, int cy, int cw = 1, int ch = 1)
        {
            cells.x = cx;
            cells.y = cy;
            cells.w = cw;
            cells.h = ch;

            if (cells.w < 1)
                cells.w = 1;

            if (cells.h < 1)
                cells.h = 1;
        }
        public void SetCells(Stars.Rect r) { cells = r; }
        public Stars.Rect GetCells() { return cells; }
        public void SetFixedWidth(int w) { fixed_width = w; }
        public int GetFixedWidth() { return fixed_width; }
        public void SetFixedHeight(int h) { fixed_height = h; }
        public int GetFixedHeight() { return fixed_height; }

        public void SetAlpha(double a)
        {
            if (alpha != a)
            {
                alpha = (float)a;

                Color c = Color.white;
                c.a = alpha;
            }
        }
        public double GetAlpha() { return alpha; }
        public virtual void SetBackColor(Color c) { back_color = c; }
        public Color GetBackColor() { return back_color; }
        public virtual void SetBaseColor(Color c) { base_color = c; }
        public Color GetBaseColor() { return base_color; }
        public virtual void SetForeColor(Color c) { fore_color = c; }
        public Color GetForeColor() { return fore_color; }
        public void SetSingleLine(bool a) { single_line = a; }
        public bool GetSingleLine() { return single_line; }
        public virtual void SetTextAlign(TextFormat a)
        {
            if (a == TextFormat.DT_LEFT || a == TextFormat.DT_RIGHT || a == TextFormat.DT_CENTER)
                text_align = a;
        }
        public TextFormat GetTextAlign() { return text_align; }
        public void SetTransparent(bool t) { transparent = t; }
        public bool GetTransparent() { return transparent; }
        public void SetHidePartial(bool a) { hide_partial = a; }
        public bool GetHidePartial() { return hide_partial; }

        public void SetTabStop(int n, int x)
        {
            if (n >= 0 && n < 10)
                tab[n] = x;
        }
        public int GetTabStop(int n)
        {
            if (n >= 0 && n < 10)
                return tab[n];

            return 0;
        }
        public override void MoveTo(Stars.Rect r)
        {
            if (rect.x == r.x &&
                rect.y == r.y &&
                rect.w == r.w &&
                rect.h == r.h)
                return;

            rect = r;
            CalcGrid();

            foreach (View v in view_list)
                v.OnWindowMove();

            if (layout != null && !layout.IsEmpty())
                layout.DoLayout(this);

            if (obj != null)
            {
                RectTransform rectTransform = obj.GetComponent<RectTransform>();
                if (rectTransform != null)
                {
                    rectTransform.pivot = new Vector2(0, 0);
                    rectTransform.anchorMin = new Vector2(0, 1);
                    rectTransform.anchorMax = new Vector2(0, 1);
                    rectTransform.localScale = new Vector3(1, 1, 1);
                    rectTransform.sizeDelta = new Vector2(rect.w, rect.h);
                    int x = rect.x;
                    int y = -(rect.y + rect.h);
                    if (parent != null && !parent.isModal)
                    {
                        x -= parent.rect.x;
                        y += parent.rect.y;
                    }
                    rectTransform.anchoredPosition = new Vector3(x, y, 0);
                }
            }
        }

        public virtual void UseLayout(List<DWORD> min_x, List<DWORD> min_y, List<float> weight_x, List<float> weight_y)
        {
            if (layout == null)
                layout = new Layout();

            if (layout != null)
                layout.SetConstraints(min_x, min_y, weight_x, weight_y);
        }
        public virtual void UseLayout(List<float> min_x, List<float> min_y, List<float> weight_x, List<float> weight_y)
        {
            if (layout == null)
                layout = new Layout();

            if (layout != null)
                layout.SetConstraints(min_x, min_y, weight_x, weight_y);
        }

        public virtual void UseLayout(int ncols, int nrows, List<DWORD> min_x, List<DWORD> min_y, List<float> weight_x, List<float> weight_y)
        {
            if (layout == null)
                layout = new Layout();

            if (layout != null)
                layout.SetConstraints(nrows, ncols, min_x, min_y, weight_x, weight_y);
        }

        public virtual void DoLayout()
        {
            if (layout != null && !layout.IsEmpty())
                layout.DoLayout(this);
        }

        // form context:
        public virtual ActiveWindow GetForm() { return form; }
        public virtual void SetForm(ActiveWindow f) { form = f; }

        public virtual bool IsTopMost() { return topmost; }
        public virtual void SetTopMost(bool t) { topmost = t; }

        // text methods:
        public virtual void SetFontSize(int s) { fontsize = s; }
        public int GetFontSize() { return fontsize; }

        public void RegisterClient(ETD eid, ActiveWindow client, PFVAWE callback)
        {
            AWMap map = new AWMap(eid, client, callback);

            if (map != null)
                clients.Add(map);
        }

        public void UnregisterClient(ETD eid, ActiveWindow client)
        {
            AWMap test = new AWMap(eid, client, null);
            int index = clients.IndexOf(test);

            if (index >= 0)
            {
                clients.RemoveAt(index);
            }
        }

        public virtual int OnMouseMove(int x, int y)
        {
            ClientEvent(ETD.EID_MOUSE_MOVE, x, y);
            return 0;
        }

        public virtual int OnMouseWheel(int wheel)
        {
            ClientEvent(ETD.EID_MOUSE_WHEEL, wheel, 0);
            return 0;
        }

        public virtual int OnLButtonDown(int x, int y)
        {
            ClientEvent(ETD.EID_LBUTTON_DOWN, x, y);
            return 0;
        }

        public virtual int OnLButtonUp(int x, int y)
        {
            ClientEvent(ETD.EID_LBUTTON_UP, x, y);
            return 0;
        }
        public virtual int OnSelect()
        {
            ClientEvent(ETD.EID_SELECT);
            return 0;
        }

        public virtual int OnClick()
        {
            ClientEvent(ETD.EID_CLICK);
            return 0;
        }
        public virtual int OnMouseEnter(int x, int y)
        {
            ClientEvent(ETD.EID_MOUSE_ENTER, x, y);
            return 0;
        }

        public virtual int OnMouseExit(int x, int y)
        {
            ClientEvent(ETD.EID_MOUSE_EXIT, x, y);
            return 0;
        }

        protected virtual void AddChild(ActiveWindow child)
        {
            if (child != null)
                children.Add(child);
        }

        public void ClientEvent(ETD aeid, int ax = 0, int ay = 0)
        {
            AWEvent evnt = new AWEvent(this, aeid, ax, ay);
            foreach (AWMap map in clients)
            {
                if (map.eid == aeid)
                    map.func(map.client, evnt);
            }
        }
        public bool AddView(IView v)
        {
            if (v == null) return false;

            if (!view_list.Contains(v))
                view_list.Add(v);

            return true;
        }

        public bool DelView(IView v)
        {
            if (v == null) return false;

            return view_list.Remove(v);
        }

        public override string ToString()
        {
            return desc;
        }
        public void DrawText(string txt, int len, StarshatterNet.Stars.Rect rect, TextFormat a)
        {
            // TODO
        }
        public bool IsFormActive()
        {
            throw new System.NotImplementedException();
        }
        protected virtual void CalcGrid()
        {
        }


        protected internal virtual void BuildGamebjects()
        {
            if (parent != null)
                this.parentObj = parent.obj;
        }

        protected FontItem sys_font = null;
        protected Color sys_back_color = new Color32(128, 128, 128, 255);
        protected Color sys_fore_color = new Color32(0, 0, 0, 255);

        protected void SetSystemFont(FontItem f) { sys_font = f; }
        protected void SetSystemBackColor(Color c) { sys_back_color = c; }
        protected void SetSystemForeColor(Color c) { sys_fore_color = c; }

        protected DWORD id;
        protected DWORD style;
        protected TextFormat text_align;
        protected bool single_line;
        protected bool focus;
        protected bool enabled;
        protected bool hide_partial;
        protected float alpha;
        protected Color back_color;
        protected Color base_color;
        protected Color fore_color;
        protected string text;
        protected string alt_text;
        protected string desc;
        protected Texture2D texture;
        protected Insets margins;
        protected Insets text_insets;
        protected Insets cell_insets;
        protected Stars.Rect cells;
        protected int fixed_width;
        protected int fixed_height;
        protected int[] tab = new int[10];

        protected bool isModal;

        protected int rows;
        protected int cols;

        protected int fontsize;
        protected Layout layout;

        protected ActiveWindow parent;
        protected internal GameObject parentObj;
        protected ActiveWindow form;
        protected bool transparent;
        protected bool topmost;

        protected List<AWMap> clients = new List<AWMap>();
        protected List<ActiveWindow> children = new List<ActiveWindow>();
        protected internal GameObject obj;
    }

    public delegate void PFVAWE(ActiveWindow obj, AWEvent data);

    public class AWEvent
    {
        public AWEvent(ActiveWindow w, ActiveWindow.ETD e, int ax = 0, int ay = 0) { window = w; eid = e; x = ax; y = ay; }

        public ActiveWindow window;
        public ActiveWindow.ETD eid;
        public int x;
        public int y;
    }
    public class AWMap
    {
        public AWMap(ActiveWindow.ETD e, ActiveWindow w, PFVAWE f) { eid = e; client = w; func = f; }

        public ActiveWindow.ETD eid;
        public ActiveWindow client;
        public PFVAWE func;
    }
}
