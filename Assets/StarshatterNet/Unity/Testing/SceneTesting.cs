﻿using System;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using UnityEngine;
using Point = UnityEngine.Vector3;
using StarPoint = DigitalRune.Mathematics.Algebra.Vector3D;

public class SceneTesting : MonoBehaviour
{
    public Scene scene;
    public bool invert = true;

    public float sensitivity = 20f;

    public bool visible = true;
    public float speed = 20f;
    public float modSpeed = 60f;
    public float zoomFov = 80.0f;

    private bool useModSpeed = false;

    private Camera cam;
    private float shift = 1f;
    private float lastX, lastY = 0f;
    private float x, y;
    private float startingFov;
    private float cubex;
    int mode;
    double radius = 1;

    double cam_zoom = 2.5;
    double cam_az = -Math.PI / 6;
    double cam_el = Math.PI / 7;
    float mouse_x = 0;
    float mouse_y = 0;

    Solid cube;

    // Use this for initialization
    void Start()
    {
        // This code is similar to TacRefDlg.cpp, lines 133-160
        scene.SetAmbient(new Color(60f / 255f, 60f / 255f, 60f / 255f));

        Point light_pos = new Point(3e6f, 5e6f, 4e6f);

        LightNode main_light = new LightNode(1.0f); //1.25f);
        main_light.MoveTo(light_pos);
        main_light.SetType(LightNode.TYPES.LIGHT_DIRECTIONAL);
        main_light.SetColor(Color.white);
        main_light.SetShadow(true);
        scene.AddLight(main_light);

        LightNode back_light = new LightNode(0.5f);
        back_light.MoveTo(light_pos * -1);
        back_light.SetType(LightNode.TYPES.LIGHT_DIRECTIONAL);
        back_light.SetColor(Color.white);
        back_light.SetShadow(false);
        scene.AddLight(back_light);

        CameraNode camera = new CameraNode();
        scene.AddCamera(camera.gameObj);
        cam = camera.gameObj.GetComponent<Camera>();
        startingFov = cam.fieldOfView;

        cube = new Solid();
        cube.MoveTo(0f, 0f, 5f);
        cube.Rescale(2);
        scene.AddGraphic(cube);
    }

    // Update is called once per frame
    void Update()
    {
        cubex += Time.deltaTime * 10;
        cube.SetOrientation(Quaternion.Euler(cubex, 0, 0));

        if (Input.GetMouseButtonDown(0))
        {
            visible = !visible;

            if (visible)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            useModSpeed = !useModSpeed;
        }
        if (cam != null)
        {
            float targetFov = startingFov;
            if (Input.GetMouseButton(2))
                targetFov = zoomFov;

            cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, targetFov, 10.0f * Time.deltaTime);
        }
        MouseLookMovement();
        //OnCamMove();
        //OnCamZoom();
        //UpdateCamera();

    }
    private void MouseLookMovement()
    {
        float trueSpeed = (useModSpeed) ? modSpeed : speed;

        if (!visible)
        {
            x = Input.GetAxis("Mouse X") + lastX * 0.9f;

            if (invert)
                y = -Input.GetAxis("Mouse Y") + lastY * 0.9f;
            else
                y = Input.GetAxis("Mouse Y") + lastY * 0.9f;

            float targetShift = (Input.GetKey(KeyCode.LeftShift)) ? 2f : 1f;
            shift = Mathf.Lerp(shift, targetShift, trueSpeed * Time.deltaTime);

            lastX = x;
            lastY = y;

            scene.Camera.Aim(sensitivity * y * Time.deltaTime, sensitivity * x * Time.deltaTime, 0f);
            //scene.Camera.gameObj.transform.Rotate(sensitivity * y * Time.deltaTime, sensitivity * x * Time.deltaTime, 0f);
        }

        Vector3 upright = scene.Camera.transform.eulerAngles;
        upright.z = 0f;
        scene.Camera.transform.eulerAngles = upright;

        //scene.Camera.gameObj.transform.Translate(trueSpeed * shift * Input.GetAxis("Horizontal") * Time.deltaTime,
        //                                        0f,
        //                                        trueSpeed * shift * Input.GetAxis("Vertical") * Time.deltaTime);
        scene.Camera.MoveBy(trueSpeed * shift * Input.GetAxis("Horizontal") * Time.deltaTime,
                            0f,
                            trueSpeed * shift * Input.GetAxis("Vertical") * Time.deltaTime);
    }
    private void UpdateAzimuth(double a)
    {
        cam_az += a;

        if (cam_az > Math.PI)
            cam_az = -2 * Math.PI + cam_az;

        else if (cam_az < -Math.PI)
            cam_az = 2 * Math.PI + cam_az;
    }
    private void UpdateElevation(double e)
    {
        cam_el += e;

        const double limit = (0.43 * Math.PI);

        if (cam_el > limit)
            cam_el = limit;
        else if (cam_el < -limit)
            cam_el = -limit;
    }
    private void UpdateZoom(double delta)
    {
        cam_zoom *= delta;

        if (cam_zoom < 1.2)
            cam_zoom = 1.2;

        else if (cam_zoom > 10)
            cam_zoom = 10;
    }

    private void OnCamZoom()
    {
        float w = Mouse.Wheel();
        if (w != 0)
            Debug.Log("Weel = " + w);
        if (w < 0)
        {
            while (w < 0)
            {
                UpdateZoom(1.25);
                w += 120;
            }
        }
        else
        {
            while (w > 0)
            {
                UpdateZoom(0.75f);
                w -= 120;
            }
        }

    }

    private void UpdateCamera()
    {
        double x = cam_zoom * radius * Math.Sin(cam_az) * Math.Cos(cam_el);
        double y = cam_zoom * radius * Math.Cos(cam_az) * Math.Cos(cam_el);
        double z = cam_zoom * radius * Math.Sin(cam_el);

        scene.Camera.LookAt(new StarPoint(0, 0, 0), new StarPoint((float)x, (float)z, (float)y), new StarPoint(0, 1, 0));
    }
    private void OnCamMove()
    {
        float mouse_dx = Mouse.x; // - mouse_x;
        float mouse_dy = Mouse.y; // - mouse_y;

        UpdateAzimuth(mouse_dx * 0.3 * DEGREES);
        UpdateElevation(mouse_dy * 0.3 * DEGREES);
        UpdateCamera();

        mouse_x = Mouse.x;
        mouse_y = Mouse.y;
    }
    const double DEGREES = (Math.PI / 180);
}
