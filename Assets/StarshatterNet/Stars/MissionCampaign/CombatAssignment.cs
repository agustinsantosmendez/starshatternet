﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CombatAssignment.h/CombatAssignment.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    High level assignment of one group to damage another
*/
namespace StarshatterNet.Stars.MissionCampaign
{
    public class CombatAssignment
    {
        public CombatAssignment(Mission.TYPE t, CombatGroup obj, CombatGroup rsc = null)
        {
            type = t;
            objective = obj;
            resource = rsc;
        }

        //~CombatAssignment();

        // +--------------------------------------------------------------------+
        // This is used to sort assignments into a priority list.
        // Higher priorities should come first in the list, so the
        // sense of the operator is "backwards" from the usual.
        //int operator <(const CombatAssignment& a) const;
        public static bool operator <(CombatAssignment l, CombatAssignment r)
        {
            if (l.objective == null)
                return false;

            if (r.objective == null)
                return true;

            return l.objective.GetPlanValue() > r.objective.GetPlanValue();
        }

        public static bool operator >(CombatAssignment l, CombatAssignment r)
        {
            if (l.objective == null)
                return true;

            if (r.objective == null)
                return false;

            return l.objective.GetPlanValue() < r.objective.GetPlanValue();
        }

        // operations:
        public void SetObjective(CombatGroup o) { objective = o; }
        public void SetResource(CombatGroup r) { resource = r; }

        // accessors:
        public Mission.TYPE Type() { return type; }
        public CombatGroup GetObjective() { return objective; }
        public CombatGroup GetResource() { return resource; }

        public string GetDescription()
        {
            string desc;

            if (resource == null)
                desc = string.Format("{0} {1}", Mission.RoleName(type), objective.Name());
            else
                desc = string.Format("{0} {1} {2}", resource.Name(), Mission.RoleName(type), objective.Name());

            return desc;
        }

        public bool IsActive() { return resource != null; }


        private Mission.TYPE type;
        private CombatGroup objective;
        private CombatGroup resource;
    }
}