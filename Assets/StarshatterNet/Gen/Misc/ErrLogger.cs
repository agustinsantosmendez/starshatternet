﻿using System;
using System.IO;
using UnityEngine;

namespace StarshatterNet.Gen.Misc
{
    public static class ErrLogger
    {
        private static StreamWriter ErrLog;
        private static ILogger logger = Debug.unityLogger;

        public static void Open(String name)
        {
            ErrLog = new StreamWriter(name);
        }
        public static void Close()
        { if (ErrLog != null) ErrLog.Close(); }

        public static void PrintLine(String fmt, params object[] args)
        {
            string errBuf = String.Format(fmt, args);
            PrintLine(errBuf);
        }
        public static void PrintLine(String msg)
        {
            if (ErrLog != null)
            {
                ErrLog.WriteLine(msg);
                ErrLog.Flush();
            }
             logger.Log(msg);
       }
        public static void PrintLine()
        {
            if (ErrLog != null)
            {
                ErrLog.WriteLine();
                ErrLog.Flush();
            }
        }
    }
}
