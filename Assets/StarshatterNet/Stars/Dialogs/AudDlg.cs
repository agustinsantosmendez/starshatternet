﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      AudDlg.h/AudDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class AudDlg : FormWindow
    {
        public AudDlg(Screen s, Camera c, FormDef def, BaseScreen mgr, GameObject p) :
                base(s, 0, 0, s.Width(), s.Height(), c, p, "AudDlg")
        {
            manager = mgr;
            apply = null; cancel = null; vid_btn = null; aud_btn = null; ctl_btn = null; opt_btn = null;
            mod_btn = null; closed = true;

            Init(def);
        }
        // public virtual ~AudDlg();

        public override void RegisterControls()
        {
            if (apply != null)
                return;

            efx_volume_slider = (Slider)FindControl(201);
            gui_volume_slider = (Slider)FindControl(202);
            wrn_volume_slider = (Slider)FindControl(203);
            vox_volume_slider = (Slider)FindControl(204);

            menu_music_slider = (Slider)FindControl(205);
            game_music_slider = (Slider)FindControl(206);

            apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, apply, OnApply);

            cancel = (Button)FindControl(2);
            REGISTER_CLIENT(ETD.EID_CLICK, cancel, OnCancel);

            vid_btn = (Button)FindControl(901);
            REGISTER_CLIENT(ETD.EID_CLICK, vid_btn, OnVideo);

            aud_btn = (Button)FindControl(902);
            REGISTER_CLIENT(ETD.EID_CLICK, aud_btn, OnAudio);

            ctl_btn = (Button)FindControl(903);
            REGISTER_CLIENT(ETD.EID_CLICK, ctl_btn, OnControls);

            opt_btn = (Button)FindControl(904);
            REGISTER_CLIENT(ETD.EID_CLICK, opt_btn, OnOptions);

            mod_btn = (Button)FindControl(905);
            if (mod_btn != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, mod_btn, OnMod);
            }
        }
        public override void Show()
        {
            base.Show();

            if (closed && AudioConfig.GetInstance() != null)
            {
                AudioConfig audio = AudioConfig.GetInstance();

                if (efx_volume_slider != null)
                    efx_volume_slider.SetValue(audio.GetEfxVolume());

                if (gui_volume_slider != null)
                    gui_volume_slider.SetValue(audio.GetGuiVolume());

                if (wrn_volume_slider != null)
                    wrn_volume_slider.SetValue(audio.GetWrnVolume());

                if (vox_volume_slider != null)
                    vox_volume_slider.SetValue(audio.GetVoxVolume());

                if (menu_music_slider != null)
                    menu_music_slider.SetValue(audio.GetMenuMusic());

                if (game_music_slider != null)
                    game_music_slider.SetValue(audio.GetGameMusic());
            }

            if (vid_btn != null) vid_btn.SetButtonState(0);
            if (aud_btn != null) aud_btn.SetButtonState(1);
            if (ctl_btn != null) ctl_btn.SetButtonState(0);
            if (opt_btn != null) opt_btn.SetButtonState(0);
            if (mod_btn != null) mod_btn.SetButtonState(0);

            closed = false;
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnApply(null, null);
            }
        }

        // Operations:
        public virtual void Apply()
        {
            if (!closed && AudioConfig.GetInstance() != null)
            {
                AudioConfig audio = AudioConfig.GetInstance();

                if (efx_volume_slider != null)
                    audio.SetEfxVolume(efx_volume_slider.GetValue());

                if (gui_volume_slider != null)
                    audio.SetGuiVolume(gui_volume_slider.GetValue());

                if (wrn_volume_slider != null)
                    audio.SetWrnVolume(wrn_volume_slider.GetValue());

                if (vox_volume_slider != null)
                    audio.SetVoxVolume(vox_volume_slider.GetValue());

                if (menu_music_slider != null)
                    audio.SetMenuMusic(menu_music_slider.GetValue());

                if (game_music_slider != null)
                    audio.SetGameMusic(game_music_slider.GetValue());

                audio.Save();
            }

            closed = true;
        }
        public virtual void Cancel()
        {
            closed = true;
        }

        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.ApplyOptions();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            manager.CancelOptions();
        }

        public virtual void OnAudio(ActiveWindow obj, AWEvent evnt) { manager.ShowAudDlg(); }
        public virtual void OnVideo(ActiveWindow obj, AWEvent evnt) { manager.ShowVidDlg(); }
        public virtual void OnOptions(ActiveWindow obj, AWEvent evnt) { manager.ShowOptDlg(); }
        public virtual void OnControls(ActiveWindow obj, AWEvent evnt) { manager.ShowCtlDlg(); }
        public virtual void OnMod(ActiveWindow obj, AWEvent evnt) { manager.ShowModDlg(); }


        protected BaseScreen manager;

        protected Slider efx_volume_slider;
        protected Slider gui_volume_slider;
        protected Slider wrn_volume_slider;
        protected Slider vox_volume_slider;

        protected Slider menu_music_slider;
        protected Slider game_music_slider;

        protected Button aud_btn;
        protected Button vid_btn;
        protected Button opt_btn;
        protected Button ctl_btn;
        protected Button mod_btn;

        protected Button apply;
        protected Button cancel;

        bool closed;
    }
}