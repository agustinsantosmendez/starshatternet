﻿using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Gen.Misc
{
    public static class PointExtensions
    {
        public static Point OtherHand(this Point p) { return new Point(-p.X, p.Z, p.Y); }
        public static void SwapYZ(this Point p) { double t = p.Y; p.Y = p.Z; p.Z = t; }

        public static bool IsNullOrEmpty(this Point p)
        {
            return (p == null || p == default(Point));
        }

    }
}
