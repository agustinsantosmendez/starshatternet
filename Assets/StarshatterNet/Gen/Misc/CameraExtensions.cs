﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Camera.h/Camera.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Camera class - Position and Point of View
*/
using System;
using DigitalRune.Mathematics.Algebra;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Gen.Misc
{
    public static class CameraExtensions
    {
        public static void Aim(this Camera cam, double roll, double pitch, double yaw)
        {
            cam.transform.Rotate((float)pitch, (float)yaw, (float)roll, Space.Self);
        }
        public static void Aim(this Camera cam, float roll, float pitch, float yaw)
        {
            cam.transform.Rotate(pitch, yaw, roll, Space.Self);
        }
        public static void Roll(this Camera cam, double roll)
        {
            cam.transform.Rotate(0f, 0f, (float)roll, Space.Self);
        }
        public static void Roll(this Camera cam, float roll)
        {
            cam.transform.Rotate(0f, 0f, roll, Space.Self);
        }
        public static void Pitch(this Camera cam, double pitch)
        {
            cam.transform.Rotate((float)pitch, 0f, 0f, Space.Self);
        }
        public static void Pitch(this Camera cam, float pitch)
        {
            cam.transform.Rotate(pitch, 0f, 0f, Space.Self);
        }
        public static void Yaw(this Camera cam, double yaw)
        {
            cam.transform.Rotate(0f, (float)yaw, 0f, Space.Self); ;
        }
        public static void Yaw(this Camera cam, float yaw)
        {
            cam.transform.Rotate(0f, yaw, 0f, Space.Self); ;
        }

        public static void MoveTo(this Camera cam, double x, double y, double z)
        {
            Vector3 newpos = new Vector3((float)x, (float)y, (float)z);
            cam.transform.position = newpos;
        }
        public static void MoveTo(this Camera cam, float x, float y, float z)
        {
            Vector3 newpos = new Vector3(x, y, z);
            cam.transform.position = newpos;
        }

        public static void MoveTo(this Camera cam, Point p)
        {
            Vector3 newpos = new Vector3((float)p.X, (float)p.Y, (float)p.Z);
            cam.transform.position = newpos;
        }

        public static void MoveBy(this Camera cam, double dx, double dy, double dz)
        {
            Vector3 dpos = new Vector3((float)dx, (float)dy, (float)dz);
            cam.transform.position += dpos;
        }
        public static void MoveBy(this Camera cam, float dx, float dy, float dz)
        {
            Vector3 dpos = new Vector3(dx, dy, dz);
            cam.transform.position += dpos;
        }

        public static void MoveBy(this Camera cam, Point dp)
        {
            Vector3 dpos = new Vector3((float)dp.X, (float)dp.Y, (float)dp.Z);
            cam.transform.position += dpos;
        }

        //public static void Clone(  Camera& cam);
        public static void LookAt(this Camera cam, Point target)
        {
            Vector3 worldPosition = new Vector3((float)target.X, (float)target.Y, (float)target.Z);
            cam.transform.LookAt(worldPosition);
        }
        public static void LookAt(this Camera cam, Point target, Point eye, Point up)
        {
            Vector3 eyePosition = new Vector3((float)eye.X, (float)eye.Y, (float)eye.Z);
            Vector3 worldTarget = new Vector3((float)target.X, (float)target.Y, (float)target.Z);
            Vector3 worldUp = new Vector3((float)up.X, (float)up.Y, (float)up.Z);
            cam.transform.position = eyePosition;
            cam.transform.LookAt(worldTarget, worldUp);
        }
        public static bool Padlock(this Camera cam, Point target, double alimit = -1, double e_lo = -1, double e_hi = -1)
        {
            //TODO
            return false;
        }

        public static Point Pos(this Camera cam)
        {
            Vector3 p = cam.transform.position;
            return new Point(p.x, p.y, p.z);
        }
        public static Point vrt(this Camera cam)
        {
            Vector3 f = cam.transform.rotation * Vector3.forward;
            return new Point(f.x, f.y, f.z);
        }
        public static Point vup(this Camera cam)
        {
            Vector3 u = cam.transform.rotation * Vector3.up;
            return new Point(u.x, u.y, u.z);
        }
        public static Point vpn(this Camera cam)
        {
            Vector3 r = cam.transform.rotation * Vector3.right;
            return new Point(r.x, r.y, r.z);
        }

        public static Matrix33D Orientation(this Camera cam)
        {
            Quaternion q = cam.transform.rotation;
            QuaternionD qD = new QuaternionD(q.w, q.x, q.y, q.z);
            return qD.ToRotationMatrix33();
        }
        public static Quaternion Rotation(this Camera cam)
        {
            return cam.transform.rotation;
        }

    }
}
