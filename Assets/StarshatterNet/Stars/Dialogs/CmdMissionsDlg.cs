﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmdMissionsDlg.h/CmdMissionsDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Operational Command Dialog (Mission List Tab)
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmdMissionsDlg : CmdDlg
    {
        public CmdMissionsDlg(Screen s, Camera c, FormDef def, CmpnScreen mgr, GameObject p) :
            base(s, mgr, 0, 0, s.Width(), s.Height(), c, p, "CmdMissionsDlg")
        {
            lst_missions = null; txt_desc = null; btn_accept = null;
            mission = null;

            Init(def);
        }
        //public virtual ~CmdMissionsDlg();
        public override void RegisterControls()
        {
            lst_missions = (ListBox)FindControl(401);
            txt_desc = FindControl(402);
            btn_accept = (Button)FindControl(403);

            RegisterCmdControls(this);

            if (btn_save != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_save, OnSave);

            if (btn_exit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_exit, OnExit);

            for (int i = 0; i < 5; i++)
            {
                if (btn_mode[i] != null)
                    REGISTER_CLIENT(ETD.EID_CLICK, btn_mode[i], OnMode);
            }

            if (lst_missions != null)
            {
                REGISTER_CLIENT(ETD.EID_SELECT, lst_missions, OnMission);
            }

            if (btn_accept != null)
            {
                btn_accept.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_accept, OnAccept);
            }
        }
        public override void Show()
        {
            mode = MODE.MODE_MISSIONS;

            base.Show();
            ShowCmdDlg();

            campaign = Campaign.GetCampaign();

            if (campaign != null)
            {
                if (lst_missions != null)
                {
                    lst_missions.ClearItems();

                    Player player = Player.GetCurrentPlayer();
                    List<MissionInfo> missions = campaign.GetMissionList();
                    for (int i = 0; i < missions.Count; i++)
                    {
                        MissionInfo info = missions[i];
                        lst_missions.AddItemWithData(info.name, info.id);

                        Mission m = info.mission;
                        if (m != null)
                        {
                            if (m.Type() == Mission.TYPE.TRAINING && player.HasTrained(m.Identity()))
                            {
                                lst_missions.SetItemText(i, 1, Game.GetText("CmdMissionsDlg.training"));
                            }
                            else
                            {
                                lst_missions.SetItemText(i, 1, m.TypeName());
                            }
                        }

                        string start_time;
                        FormatUtil.FormatDayTime(out start_time, info.start);
                        lst_missions.SetItemText(i, 2, start_time);
                    }
                }
            }
        }
        public override void ExecFrame()
        {
            base.ExecFrame();

            if (campaign != null)
            {
                List<MissionInfo> missions = campaign.GetMissionList();
                Player player = Player.GetCurrentPlayer();

                if (missions.Count > lst_missions.NumItems())
                {
                    while (missions.Count > lst_missions.NumItems())
                    {
                        MissionInfo info = missions[lst_missions.NumItems()];
                        int i = lst_missions.AddItemWithData(info.name, info.id) - 1;

                        Mission m = info.mission;
                        if (m != null)
                        {
                            if (m.Type() == Mission.TYPE.TRAINING && player.HasTrained(m.Identity()))
                            {
                                lst_missions.SetItemText(i, 1, Game.GetText("CmdMissionsDlg.training"));
                            }
                            else
                            {
                                lst_missions.SetItemText(i, 1, m.TypeName());
                            }
                        }

                        string start_time;
                        FormatUtil.FormatDayTime(out start_time, info.start);
                        lst_missions.SetItemText(i, 2, start_time);
                    }
                }

                else if (missions.Count < lst_missions.NumItems())
                {
                    lst_missions.ClearItems();

                    for (int i = 0; i < missions.Count; i++)
                    {
                        MissionInfo info = missions[i];
                        lst_missions.AddItemWithData(info.name, info.id);

                        Mission m = info.mission;
                        if (m != null)
                        {
                            if (m.Type() == Mission.TYPE.TRAINING && player.HasTrained(m.Identity()))
                            {
                                lst_missions.SetItemText(i, 1, Game.GetText("CmdMissionsDlg.training"));
                            }
                            else
                            {
                                lst_missions.SetItemText(i, 1, m.TypeName());
                            }
                        }

                        string start_time;
                        FormatUtil.FormatDayTime(out start_time, info.start);
                        lst_missions.SetItemText(i, 2, start_time);
                    }
                }

                else if (missions.Count > 0 && lst_missions.NumItems() > 0)
                {
                    int id = (int)lst_missions.GetItemData(0);
                    MissionInfo info = campaign.GetMissionInfo(id);

                    if (info == null)
                    {
                        int seln = -1;
                        int seln_id = 0;

                        for (int i = 0; i < lst_missions.NumItems(); i++)
                            if (lst_missions.IsSelected(i))
                                seln = i;

                        if (seln >= 0)
                            seln_id = (int)lst_missions.GetItemData(seln);

                        lst_missions.ClearItems();
                        seln = -1;

                        for (int i = 0; i < missions.Count; i++)
                        {
                            MissionInfo minfo = missions[i];
                            lst_missions.AddItemWithData(info.name, minfo.id);

                            Mission m = minfo.mission;
                            if (m != null)
                            {
                                if (m.Type() == Mission.TYPE.TRAINING && player.HasTrained(m.Identity()))
                                {
                                    lst_missions.SetItemText(i, 1, Game.GetText("CmdMissionsDlg.training"));
                                }
                                else
                                {
                                    lst_missions.SetItemText(i, 1, m.TypeName());
                                }
                            }

                            string start_time;
                            FormatUtil.FormatDayTime(out start_time, minfo.start);
                            lst_missions.SetItemText(i, 2, start_time);

                            if (minfo.id == seln_id)
                                seln = i;
                        }

                        if (seln >= 0)
                            lst_missions.SetSelected(seln);
                    }
                }

                bool found = false;

                for (int i = 0; i < missions.Count && !found; i++)
                {
                    MissionInfo info = missions[i];
                    if (info.mission == mission)
                        found = true;
                }

                if (!found)
                {
                    mission = null;
                    txt_desc.SetText("");
                    btn_accept.SetEnabled(false);
                }
            }
        }

        // Operations:
        public override void OnMode(ActiveWindow obj, AWEvent evnt)
        {
            base.OnMode(obj, evnt);
        }

        public override void OnSave(ActiveWindow obj, AWEvent evnt)
        {
            base.OnSave(obj, evnt);
        }

        public override void OnExit(ActiveWindow obj, AWEvent evnt)
        {
            base.OnExit(obj, evnt);
        }

        public virtual void OnMission(ActiveWindow obj, AWEvent evnt)
        {
            if (campaign != null && lst_missions != null)
            {
                MissionInfo info = null;
                mission = null;

                for (int i = 0; i < lst_missions.NumItems(); i++)
                {
                    if (lst_missions.IsSelected(i))
                    {
                        int id = (int)lst_missions.GetItemData(i);
                        info = campaign.GetMissionInfo(id);
                    }
                }

                btn_accept.SetEnabled((info != null) ? true : false);

                if (info != null)
                {
                    FontItem Limerick = FontMgr.Find("Limerick12");
                    int limerickSize = 14;
                    if (Limerick != null)
                        limerickSize = Limerick.size;

                    FontItem Verdana = FontMgr.Find("Verdana");
                    int verdanaSize = 12;
                    if (Limerick != null)
                        verdanaSize = Verdana.size;

                    string desc = "<size=" + limerickSize + "><color=#ffff80>";
                    desc += info.name;
                    desc += "</color></size><size=" + verdanaSize + "><color=#ffffff>\n\n";
                    desc += info.player_info;
                    desc += "\n\n";
                    desc += info.description;
                    desc += "</color></size>";

                    txt_desc.SetText(desc);
                    mission = info.mission;
                }
                else
                {
                    txt_desc.SetText(" ");
                }
            }
        }
        public virtual void OnAccept(ActiveWindow obj, AWEvent evnt)
        {
            if (campaign == null || mission == null)
            {
                ErrLogger.PrintLine("  ERROR CMD.Accept campaign=0x%08x mission=0x%08x", campaign, mission);
                return;
            }

            ErrLogger.PrintLine("  CMD.Accept Mission {0}", mission.Identity());

            Mouse.Show(false);
            campaign.SetMissionId(mission.Identity());
            campaign.StartMission();
            stars.SetGameMode(Starshatter.MODE.PREP_MODE);
        }




        protected ListBox lst_missions;
        protected ActiveWindow txt_desc;
        protected Button btn_accept;

        protected Mission mission;
    }
}