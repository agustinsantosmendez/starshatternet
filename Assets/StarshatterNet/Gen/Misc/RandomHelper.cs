﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx.lib
    ORIGINAL FILE:      Random.h/Random.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Utility functions for generating random numbers and locations.
*/
using DigitalRune.Mathematics.Algebra;
using System;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Gen.Misc
{
    /// <summary>
    /// TODO. Use DigitalRune.Mathematics.Statistics.RandomHelper instead.
    /// </summary>
    public static class RandomHelper
    {
        public static void RandomInit()
        {
            rand = new Random();
        }

        public static Point RandomDirection()
        {
            //Point p = Point(rand() - 16384, rand() - 16384, 0);
            Point p = new Point(rand.Next(short.MinValue, short.MaxValue), rand.Next(short.MinValue, short.MaxValue), 0);
            p.Normalize();
            return p;
        }
        public static Point RandomPoint()
        {
            //Point p = Point(rand() - 16384, rand() - 16384, 0);
            Point p = new Point(rand.Next(short.MinValue, short.MaxValue), rand.Next(short.MinValue, short.MaxValue), 0);
            p.Normalize();
            p *= 15e3f + rand.Next() / 3;
            return p;
        }

        public static Vector3F RandomVector(double radius)
        {
            //Vector3F v = new Vector3F(rand() - 16384, rand() - 16384, rand() - 16384);
            Vector3F v = new Vector3F(rand.Next(short.MinValue, short.MaxValue), rand.Next(short.MinValue, short.MaxValue), rand.Next(short.MinValue, short.MaxValue));
            v.Normalize();

            if (radius > 0)
                v *= (float)radius;
            else
                v *= (float)Random(radius / 3, radius);

            return v;
        }

        /// <summary>
        /// Get a random point in a sphere
        /// </summary>
        /// <param name="radius">the radius of the sphere</param>
        /// <returns></returns>
        public static UnityEngine.Vector3 GetRandomInSphere(float radius)
        {
            UnityEngine.Vector3 v = UnityEngine.Random.onUnitSphere;

            if (radius > 0)
                v *= (float)radius;
            else
                v *= (float)UnityEngine.Random.Range(radius / 3, radius);

            return v;
        }

        public static double Random(double min = 0, double max = 1)
        {
            double delta = max - min;
            double r = delta * rand.Next() / System.Int32.MaxValue;

            return min + r;
        }
        public static int RandomIndex()
        {
            int r = 1 + ((rand.Next() & 0x0700) >> 8);
            index += r;
            if (index > 1e7) index = 0;
            return table[index % 16];
        }
        public static int Rand()
        {
            return rand.Next();
        }

        public static bool RandomChance(int wins = 1, int tries = 2)
        {
            double fraction = 256.0 * wins / tries;
            double r = (rand.Next() >> 4) & 0xFF;

            return r < fraction;
        }
        public static int RandomSequence(int current, int range)
        {
            if (range > 1)
            {
                int step = (int)Random(1, range - 1);
                return (current + step) % range;
            }

            return current;
        }
        public static int RandomShuffle(int count)
        {
            if (count < 0 || count > 250)
                return 0;

            if (set_size != count)
            {
                set_size = count;
                indexShuffle = -1;
            }

            // need to reshuffle
            if (indexShuffle < 0 || indexShuffle > set_size - 1)
            {
                // set up the deck
                int[] tmp = new int[256];
                for (int i = 0; i < 256; i++)
                    tmp[i] = i;

                // shuffle the cards
                for (int i = 0; i < set_size; i++)
                {
                    int n = (int)Random(0, set_size);
                    int tries = set_size;
                    while (tmp[n] < 0 && tries-- != 0)
                    {
                        n = (n + 1) % set_size;
                    }

                    if (tmp[n] >= 0)
                    {
                        set[i] = (byte)tmp[n];
                        tmp[n] = -1;
                    }
                    else
                    {
                        set[i] = 0;
                    }
                }

                indexShuffle = 0;
            }

            return set[indexShuffle++];
        }

        private static int index = 0;
        private static int[] table = new int[16] { 0, 9, 4, 7, 14, 11, 2, 12, 1, 5, 13, 8, 6, 10, 3, 15 };

        private static int set_size = -1;
        private static byte[] set = new byte[256];
        private static int indexShuffle = -1;


        private static Random rand = new Random();
    }
}
