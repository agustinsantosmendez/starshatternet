﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    ORIGINAL FILE:         Encrypt.h/Encrypt.cpp
    ORIGINAL AUTHOR:       John DiCamillo
    .NET AUTHOR:        Agustin Santos

    OVERVIEW
    ========
    Simple Encryption / Decryption class
*/
using System;
using System.IO;
using System.Text;

namespace StarshatterNet.Gen.Misc
{
    /// <summary>
    /// The orignal Starshatter implements the "Tiny Encryption Algorithm".
    /// This algorithm is not as secure as AES or TripleDES, 
    /// but because of it's small footprint it's a good choise for small or embedded applications.
    /// </summary>
    public static class Encryption
    {
        /// <summary>
        /// Encrypts the provided data with XTEA
        /// </summary>
        /// <param name="data">The data to encrypt</param>
        /// <returns>Encrypted data as byte array</returns>        
        public static byte[] Encrypt(byte[] data)
        {
            var blockBuffer = new uint[2];
            int len = data.Length;
            // pad to eight byte chunks
            if ((len & 0x7) != 0)
            {
                len /= 8;
                len *= 8;
                len += 8;
            }
            var dataBuffer = new byte[len];

            Array.Copy(data, dataBuffer, data.Length);

            using (var memoryStream = new MemoryStream(dataBuffer))
            {
                using (var binaryWriter = new BinaryWriter(memoryStream))
                    for (uint i = 0; i < dataBuffer.Length; i += BlockSize)
                    {
                        blockBuffer[0] = BitConverter.ToUInt32(dataBuffer, (int)i);
                        blockBuffer[1] = BitConverter.ToUInt32(dataBuffer, (int)i + 4);

                        Encypher(blockBuffer, key);

                        binaryWriter.Write(blockBuffer[0]);
                        binaryWriter.Write(blockBuffer[1]);
                    }
            }
            return dataBuffer;
        }

        /// <summary>
        /// Decrypts the given data with the provided key.
        /// Throws an exception if the length of the data array is not a multiple of 8.
        /// Throws an exception if the decrypted length is longer than the actual array.
        /// </summary>
        /// <param name="data">The encrypted data.</param>
        /// <param name="key">The key used for decryption.</param>
        /// <returns></returns>
        public static string Decrypt(byte[] data)
        {
            if (data.Length % 8 != 0) throw new ArgumentException("Encrypted data length must be a multiple of 8 bytes.");
            var blockBuffer = new uint[2];
            var buffer = new byte[data.Length];
            Array.Copy(data, buffer, data.Length);
            using (var stream = new MemoryStream(buffer))
            {
                using (var writer = new BinaryWriter(stream))
                {
                    for (int i = 0; i < buffer.Length; i += 8)
                    {
                        blockBuffer[0] = BitConverter.ToUInt32(buffer, i);
                        blockBuffer[1] = BitConverter.ToUInt32(buffer, i + 4);
                        Decypher(blockBuffer, key);
                        writer.Write(blockBuffer[0]);
                        writer.Write(blockBuffer[1]);
                    }
                }
            }
            var str = System.Text.ASCIIEncoding.Default.GetString(buffer);
            return str;
        }


        // encode / decode binary blocks into
        // ascii strings for use in text files
        public static string Encode(byte[] block)
        {
            int len = block.Length * 2;
            byte[] work = new byte[len];

            for (int i = 0; i < block.Length; i++)
            {
                byte b = block[i];
                work[2 * i] = codesinbytes[b >> 4 & 0xf];
                work[2 * i + 1] = codesinbytes[b & 0xf];
            }

            // Encode string.
            // The original encoding was ASCII.
            return Encoding.ASCII.GetString(work);
        }

        /// <summary>
        /// Decrypts the given data with the provided key.
        /// Throws an exception if the length of the data array is not a multiple of 8.
        /// Throws an exception if the decrypted length is longer than the actual array.
        /// </summary>
        /// <param name="data">The encrypted and Base64 encoded data.</param>
        /// <returns></returns>
        public static byte[] Decode(string data)
        {
            // The original encoding was ASCII.
            byte[] encodedBytes = Encoding.ASCII.GetBytes(data);

            int len = data.Length / 2;
            byte[] work = new byte[len];

            for (int i = 0; i < len; i++)
            {
                byte u = encodedBytes[2 * i];
                byte l = encodedBytes[2 * i + 1];

                work[i] = (byte)((u - (byte)codes[0]) << 4 | (l - (byte)codes[0]));
            }
            return work;
        }

        /// <summary>
        /// TEA inplace decoding routine of the provided data array.
        /// </summary>
        /// <param name="v">The data array containing two values.</param>
        /// <param name="k">The key array containing 4 values.</param>
        private static void Decypher(uint[] v, uint[] k)
        {
            uint y = v[0];
            uint z = v[1];
            uint sum = 0;
            unchecked
            {
                sum = Delta * Rounds;
                for (uint i = 0; i < Rounds; i++)
                {
                    z -= (uint)((y << 4) + k[2] ^ y + sum ^ (y >> 5) + k[3]);
                    y -= (uint)((z << 4) + k[0] ^ z + sum ^ (z >> 5) + k[1]);
                    sum -= Delta;
                }
            }
            v[0] = y;
            v[1] = z;
        }

        /// <summary>
        /// TEA inplace encoding routine of the provided data array.
        /// </summary>
        /// <param name="v">The data array containing two values.</param>
        /// <param name="k">The key array containing 4 values.</param>
        private static void Encypher(uint[] v, uint[] k)
        {
            uint y = v[0];
            uint z = v[1];
            uint sum = 0;
            unchecked
            {
                for (int i = 0; i < Rounds; i++) // basic cycle start
                {
                    sum += Delta;
                    y += (z << 4) + k[0] ^ z + sum ^ (z >> 5) + k[1];
                    z += (y << 4) + k[2] ^ y + sum ^ (y >> 5) + k[3];
                }
            }
            v[0] = y;
            v[1] = z;
        }

        /// <summary>
        /// The 128 bit key used for encryption and decryption
        /// </summary>
        private static uint[] key = new uint[4]{
            0x3B398E26,
            0x40C29501,
            0x614D7630,
            0x7F59409A
        };

        /// <summary>
        /// The delta is derived from the golden ratio where delta = (sqrt(2) - 1) * 2^31
        /// A different multiple of delta is used in each round so that no bit of
        /// the multiple will not change frequently
        /// </summary>
        private const uint Delta = 0x9E3779B9;

        /// <summary>
        /// The recommended number of rounds is 32 and not 64, because each iteration performs two Feistel-cipher rounds.
        /// </summary>
        private const uint Rounds = 32;

        /// <summary>
        /// TEA operates with a block size of 8 bytes
        /// </summary>
        private const uint BlockSize = 8;

        private const string codes = "abcdefghijklmnop";
        private static readonly byte[] codesinbytes = Encoding.ASCII.GetBytes(codes);
    }
}
