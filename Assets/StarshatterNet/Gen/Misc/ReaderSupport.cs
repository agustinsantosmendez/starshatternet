﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Stars.StarSystems;
using UnityEditor;
using UnityEngine;

namespace StarshatterNet.Gen.Misc
{
    public static class ReaderSupport
    {
        public static T Deserialize<T>(string absolutefilename)
        {
            if (!File.Exists(absolutefilename))
            {
                ErrLogger.PrintLine("WARNING: File {0} does not exist. Using defaults", absolutefilename);
                return default(T);
            }
            try
            {
                using (StreamReader streamReader = File.OpenText(absolutefilename))
                {
                    string jsonString = streamReader.ReadToEnd();
                    return JsonUtility.FromJson<T>(jsonString);
                }
            }
            catch (Exception e)
            {
                ErrLogger.PrintLine("ERROR: JSON error while parsing file {0}: {1}. Using defaults", absolutefilename, e);

                return default(T);
            }
        }

        public static T DeserializeOverwrite<T>(string absolutefilename, T obj)
        {
            if (obj == null)
            {
                ErrLogger.PrintLine("ERROR: Obj to deserialize is null. File {0}. Using defaults", absolutefilename);
                return default(T);
            }

            if (!File.Exists(absolutefilename))
            {
                ErrLogger.PrintLine("WARNING: File {0} does not exist. Using defaults", absolutefilename);
                return default(T);
            }

            using (StreamReader streamReader = File.OpenText(absolutefilename))
            {
                string jsonString = streamReader.ReadToEnd();
                JsonUtility.FromJsonOverwrite(jsonString, obj);
                return obj;
            }
        }

        #region Assets
        public static T DeserializeAsset<T>(string filename)
        {
            filename = System.IO.Path.ChangeExtension(filename, null);
            var dataAsJson = Resources.Load<TextAsset>(filename);
            if (dataAsJson == null) return default(T);

            T gameData = JsonUtility.FromJson<T>(dataAsJson.text);
            return gameData;
        }
        public static T DeserializeAssetOverwrite<T>(string filename, T obj)
        {
            if (obj == null)
            {
                ErrLogger.PrintLine("ERROR: Obj to deserialize is null. File {0}. Using defaults", filename);
                return default(T);
            }

            filename = System.IO.Path.ChangeExtension(filename, null);
            var dataAsJson = Resources.Load<TextAsset>(filename);
            if (dataAsJson == null) return default(T);

            JsonUtility.FromJsonOverwrite(dataAsJson.text, obj);
            return obj;
        }
        public static bool DoesAssetFileExist(string name, string assetType = "t:object")
        {
            string[] guids2 = AssetDatabase.FindAssets(assetType, new[] { "Assets/Resources/" + name });
            return (guids2 != null && guids2.Length > 0);
        }
        public static string[] FindAssetFiles(string path, string assetType = "t:object", string filter = "")
        {
            // Types can either be builtin types such as Texture2D or user created script classes
            // "t:AudioClip"
            // "t:Texture2D "
            // "t:TextAsset"
            string[] guids2 = AssetDatabase.FindAssets("", new[] { "Assets/Resources/" + path });
            string[] fileEntries = new string[guids2.Length];
            for (int i = 0; i < guids2.Length; i++)
            {
                string guid2 = guids2[i];
                fileEntries[i] = AssetDatabase.GUIDToAssetPath(guid2);
            }
            return fileEntries;
        }
        public static IEnumerable<String> GetAssetFiles(string path, string filter = "")
        {
            string[] allfiles = AssetDatabase.GetAllAssetPaths();
            var partialfiles = allfiles.Where(s => s.Contains(path) && s.Contains(filter));
            return partialfiles;
        }
        #endregion

        #region Content
        public static T DeserializeContent<T>(string filename)
        {
            return Deserialize<T>(GetContentPath(filename));
        }
        public static T DeserializeContentOverwrite<T>(string filename, T obj)
        {
            return DeserializeOverwrite<T>(GetContentPath(filename), obj);
        }
        public static bool DoesContentFileExist(string name)
        {
            return File.Exists(GetContentPath(name));
        }
        public static bool DoesContentDirectoryExist(string name)
        {
            return Directory.Exists(GetContentPath(name));
        }
        public static string[] GetContentFiles(string path, string searchPattern)
        {
            return Directory.GetFiles(ReaderSupport.GetContentPath(path), searchPattern);
        }
        public static string GetContentPath(string name)
        {
            if (ContentPath == null)
            {
                ContentPath = Application.streamingAssetsPath;
                ErrLogger.PrintLine("INFO: Using {0} as Streaming Assets (content) path.", ContentPath);
            }
            return Path.Combine(ContentPath, name);
        }

        #endregion

        #region Data
        public static T DeserializeData<T>(string filename)
        {
            return Deserialize<T>(GetDataPath(filename));
        }
        public static T DeserializeDataOverwrite<T>(string filename, T obj)
        {
            return DeserializeOverwrite<T>(GetDataPath(filename), obj);
        }
        public static bool SerializeData<T>(T obj, string filename)
        {
            string dataAsJson = JsonUtility.ToJson(obj);

            string filePath = GetDataPath(filename);
            File.WriteAllText(filePath, dataAsJson);

            return true;
        }

        public static string GetDataPath(string name)
        {
            if (DataPath == null)
            {
                DataPath = Application.persistentDataPath;
                ErrLogger.PrintLine("INFO: Using {0} as persistent data path.", DataPath);
            }
            return Path.Combine(DataPath, name);
        }
        public static bool DeleteDataFile(string name)
        {
            try
            {
                File.Delete(GetDataPath(name));
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
        public static bool DoesDataFileExist(string name)
        {
            return File.Exists(GetDataPath(name));
        }

        public static bool DoesDataDirectoryExists(string dirname)
        {
            return Directory.Exists(GetDataPath(dirname));
        }

        public static bool CreateDataDirectory(string dirname)
        {
            try
            {
                Directory.CreateDirectory(GetDataPath(dirname));
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
        public static string[] GetDataFileList(string dirname, string searchPattern)
        {
            try
            {
                if (Directory.Exists(GetDataPath(dirname)))
                {
                    string[] dirs = Directory.GetFiles(GetDataPath(dirname), searchPattern);
                    return dirs;
                }
                else
                {
                    return new string[0];
                }
            }
            catch (Exception e)
            {
                ErrLogger.PrintLine("ERROR. GetDataFileList failed: {0}, Exception: {1}", dirname, e.Message);
                return null;
            }
        }
        public static string ReadAllDataText(string filename)
        {
            try
            {
                return File.ReadAllText(GetDataPath(filename));
            }
            catch (Exception e)
            {
                ErrLogger.PrintLine("ERROR. ReadAllDataText failed: {0}, Exception: {1}", filename, e.Message);
                return null;
            }
        }
        public static void WriteAllDataText(string filename, string text)
        {
            try
            {
                File.WriteAllText(GetDataPath(filename), text);
            }
            catch (Exception e)
            {
                ErrLogger.PrintLine("ERROR. ReadAllDataText failed: {0}, Exception: {1}", filename, e.Message);
            }
        }
        #endregion

        #region Parsers
        public static short ShortParse(string val)
        {
            short rst;
            if (short.TryParse(val, out rst))
                return rst;
            else
                rst = Convert.ToInt16(val, 16);
            return rst;
        }
        public static int IntParse(string val)
        {
            int rst;
            if (int.TryParse(val, out rst))
                return rst;
            else
                rst = Convert.ToInt32(val, 16);
            return rst;
        }
        public static int GetDefTime(string val)
        {
            int dst = 0;
            int d = 0, h, m, s;
            if (!string.IsNullOrWhiteSpace(val))
            {
                int pos = val.IndexOf('/');
                string time;
                if (pos >= 0)
                {
                    int.TryParse(val.Substring(0, pos), out d);
                    time = val.Substring(pos + 1);
                }
                else
                    time = val;
                string[] timeParts = time.Split(':');
                h = int.Parse(timeParts[0]);
                m = int.Parse(timeParts[1]);
                s = int.Parse(timeParts[2]);
                dst = d * 24 * 60 * 60 +
                      h * 60 * 60 +
                      m * 60 +
                      s;
            }
            return dst;
        }
        public static Vector3D GetDefVec(string[] val, string file)
        {
            Vector3D v = new Vector3D();
            if (val == null || val.Length != 3)
                ErrLogger.PrintLine("WARNING: vector expected in '{0}'", file);

            if (!double.TryParse(val[0], out v.X) ||
                !double.TryParse(val[1], out v.Y) ||
                !double.TryParse(val[2], out v.Z))
                ErrLogger.PrintLine("WARNING: malformed vector in '{0}'", file);

            return v;
        }
        public static Stars.Rect GetDefRect(string[] val, string file)
        {
            Stars.Rect dst = new Stars.Rect();
            if (val == null || val.Length != 3)
                ErrLogger.PrintLine("WARNING: vector expected in '{0}'", file);

            if (!int.TryParse(val[0], out dst.x) ||
                !int.TryParse(val[1], out dst.y) ||
                !int.TryParse(val[2], out dst.w) ||
                !int.TryParse(val[3], out dst.h))
                ErrLogger.PrintLine("WARNING: malformed vector in '{0}'", file);

            return dst;
        }

        #endregion

        private static string ContentPath;
        private static string DataPath;
    }
}
