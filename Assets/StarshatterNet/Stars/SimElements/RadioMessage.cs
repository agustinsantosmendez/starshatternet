﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      RadioMessage.h/RadioMessage.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    RadioMessage (radio comms) class declaration
*/
using System;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.SimElements;
using System.Collections.Generic;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Stars
{

#warning RadioMessage class is still in development and is not recommended for production.
    public class RadioMessage
    {
        public enum ACTION
        {
            NONE = 0,

            DOCK_WITH = Instruction.ACTION.DOCK,
            RTB = Instruction.ACTION.RTB,
            QUANTUM_TO = Instruction.ACTION.NUM_ACTIONS,
            FARCAST_TO,

            // protocol:
            ACK,
            NACK,

            // target mgt:
            ATTACK,
            ESCORT,
            BRACKET,
            IDENTIFY,

            // combat mgt:
            COVER_ME,
            WEP_FREE,
            WEP_HOLD,
            FORM_UP,       // alias for wep_hold
            SAY_POSITION,

            // sensor mgt:
            LAUNCH_PROBE,
            GO_EMCON1,
            GO_EMCON2,
            GO_EMCON3,

            // formation mgt:
            GO_DIAMOND,
            GO_SPREAD,
            GO_BOX,
            GO_TRAIL,

            // mission mgt:
            MOVE_PATROL,
            SKIP_NAVPOINT,
            RESUME_MISSION,

            // misc announcements:
            CALL_ENGAGING,
            FOX_1,
            FOX_2,
            FOX_3,
            SPLASH_1,
            SPLASH_2,
            SPLASH_3,
            SPLASH_4,
            SPLASH_5,   // target destroyed
            SPLASH_6,   // enemy destroyed
            SPLASH_7,   // confirmed kill
            DISTRESS,
            BREAK_ORBIT,
            MAKE_ORBIT,
            QUANTUM_JUMP,

            // friendly fire:
            WARN_ACCIDENT,
            WARN_TARGETED,
            DECLARE_ROGUE,

            // support:
            PICTURE,
            REQUEST_PICTURE,
            REQUEST_SUPPORT,

            // traffic control:
            CALL_INBOUND,
            CALL_APPROACH,
            CALL_CLEARANCE,
            CALL_FINALS,
            CALL_WAVE_OFF,

            NUM_ACTIONS
        }

        public RadioMessage(Ship dst, Ship s, ACTION a)
        {
            dst_ship = dst; dst_elem = null; sender = s; action = a; channel = 0;
            if (s!=null)
                channel = s.GetIFF();
        }

        public RadioMessage(Element dst, Ship s, ACTION a)
        {
            dst_ship = null; dst_elem = dst; sender = s; action = a; channel = 0;
            if (s!=null)
                channel = s.GetIFF();
        }

        public RadioMessage(RadioMessage rm)
        {
            dst_ship = rm.dst_ship; dst_elem = rm.dst_elem;
            sender = rm.sender; action = rm.action; channel = rm.channel;
            info = rm.info; location = rm.location;
            if (rm.target_list.Count > 0)
            {
                for (int i = 0; i < rm.target_list.Count; i++)
                {
                    SimObject obj = rm.target_list[i];
                    target_list.Add(obj);
                }
            }
        }
        // ~RadioMessage();

        // accessors:
        public static string ActionName(ACTION a)
        {
            if (a == ACTION.ACK)
            {
                int coin = RandomHelper.Rand();
                if (coin < 10000) return "Acknowledged";
                if (coin < 17000) return "Roger that";
                if (coin < 20000) return "Understood";
                if (coin < 22000) return "Copy that";
                return "Affirmative";
            }

            if (a == ACTION.DISTRESS)
            {
                int coin = RandomHelper.Rand();
                if (coin < 15000) return "Mayday! Mayday!";
                if (coin < 18000) return "She's breaking up!";
                if (coin < 21000) return "Checking out!";
                return "We're going down!";
            }

            if (a == ACTION.WARN_ACCIDENT)
            {
                int coin = RandomHelper.Rand();
                if (coin < 15000) return "Check your fire!";
                if (coin < 18000) return "Watch it!";
                if (coin < 21000) return "Hey! We're on your side!";
                return "Confirm your targets!";
            }

            if (a == ACTION.WARN_TARGETED)
            {
                int coin = RandomHelper.Rand();
                if (coin < 15000) return "Break off immediately!";
                if (coin < 20000) return "Buddy spike!";
                return "Abort! Abort!";
            }

            switch (a)
            {
                case ACTION.NONE: return "";

                case ACTION.NACK: return "Negative, Unable";

                case ACTION.ATTACK: return "Engage";
                case ACTION.ESCORT: return "Escort";
                case ACTION.BRACKET: return "Bracket";
                case ACTION.IDENTIFY: return "Identify";

                case ACTION.COVER_ME: return "Cover Me";
                case ACTION.MOVE_PATROL: return "Vector";
                case ACTION.SKIP_NAVPOINT: return "Skip Navpoint";
                case ACTION.RESUME_MISSION: return "Resume Mission";
                case ACTION.RTB: return "Return to Base";
                case ACTION.DOCK_WITH: return "Dock With";
                case ACTION.QUANTUM_TO: return "Jump to";
                case ACTION.FARCAST_TO: return "Farcast to";

                case ACTION.GO_DIAMOND: return "Goto Diamond Formation";
                case ACTION.GO_SPREAD: return "Goto Spread Formation";
                case ACTION.GO_BOX: return "Goto Box Formation";
                case ACTION.GO_TRAIL: return "Goto Trail Formation";

                case ACTION.WEP_FREE: return "Break and Attack";
                case ACTION.WEP_HOLD: return "Hold All Weapons";
                case ACTION.FORM_UP: return "Return to Formation";
                case ACTION.SAY_POSITION: return "Say Your Position";

                case ACTION.LAUNCH_PROBE: return "Launch Probe";
                case ACTION.GO_EMCON1: return "Goto EMCON 1";
                case ACTION.GO_EMCON2: return "Goto EMCON 2";
                case ACTION.GO_EMCON3: return "Goto EMCON 3";

                case ACTION.REQUEST_PICTURE: return "Request Picture";
                case ACTION.REQUEST_SUPPORT: return "Request Support";
                case ACTION.PICTURE: return "Picture is clear";

                case ACTION.CALL_INBOUND: return "Calling Inbound";
                case ACTION.CALL_APPROACH: return "Roger your approach";
                case ACTION.CALL_CLEARANCE: return "You have clearance";
                case ACTION.CALL_FINALS: return "On final approach";
                case ACTION.CALL_WAVE_OFF: return "Wave off - Runway is closed";

                case ACTION.DECLARE_ROGUE: return "Prepare to be destroyed!";

                case ACTION.CALL_ENGAGING: return "Engaging";
                case ACTION.FOX_1: return "Fox One!";
                case ACTION.FOX_2: return "Fox Two!";
                case ACTION.FOX_3: return "Fox Three!";
                case ACTION.SPLASH_1: return "Splash One!";
                case ACTION.SPLASH_2: return "Splash Two!";
                case ACTION.SPLASH_3: return "Splash Three!";
                case ACTION.SPLASH_4: return "Splash Four!";
                case ACTION.SPLASH_5: return "Target Destroyed!";
                case ACTION.SPLASH_6: return "Enemy Destroyed!";
                case ACTION.SPLASH_7: return "Confirmed Kill!";
                case ACTION.BREAK_ORBIT: return "Breaking Orbit";
                case ACTION.MAKE_ORBIT: return "Heading for Orbit";
                case ACTION.QUANTUM_JUMP: return "Going Quantum";

                default: return "Unknown";
            }
        }

        public Ship Sender() { return sender; }
        public Ship DestinationShip() { return dst_ship; }
        public Element DestinationElem() { return dst_elem; }
        public ACTION Action() { return action; }
        public List<SimObject> TargetList() { return target_list; }
        public Point Location() { return location; }
        public string Info() { return info; }
        public int Channel() { return channel; }

        // mutators:
        public void SetDestinationShip(Ship s) { dst_ship = s; }
        public void SetDestinationElem(Element e) { dst_elem = e; }
        public void AddTarget(SimObject obj)
        {
            if (obj != null && !target_list.Contains(obj))
            {
                target_list.Add(obj);
            }
        }

        public void SetLocation(Point l) { location = l; }
        public void SetInfo(string msg) { info = msg; }
        public void SetChannel(int c) { channel = c; }


        protected Ship sender;
        protected Ship dst_ship;
        protected Element dst_elem;
        protected RadioMessage.ACTION action;
        protected List<SimObject> target_list;
        protected Point location;
        protected string info;
        protected int channel;

    }
}