﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      FirstTimeDlg.h/FirstTimeDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class FirstTimeDlg : FormWindow
    {
        public FirstTimeDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
              base(s, 0, 0, s.Width(), s.Height(), c, p, "FirstTimeDlg")
        {
            manager = mgr;

            Init(def);
        }
        //public virtual ~FirstTimeDlg();
        public override void RegisterControls()
        {
            edt_name = (EditBox)FindControl(200);
            cmb_playstyle = (ComboBox)FindControl(201);
            cmb_experience = (ComboBox)FindControl(202);

            btn_apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, btn_apply, OnApply);
        }
        public override void Show()
        {
            if (!IsShown())
                base.Show();

            if (edt_name != null)
                edt_name.SetText("Noobie");
        }

        public virtual void ExecFrame()
        {
        }

        // Operations:
        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            Starshatter  stars = Starshatter.GetInstance();
            Player  player = Player.GetCurrentPlayer();

            if (player != null)
            {
                if (edt_name != null)
                {
                    string password = string.Format("{0:X8}", (int)RandomHelper.Random(0, 2e9));

                    player.SetName(edt_name.GetText());
                    player.SetPassword(password);
                }

                if (cmb_playstyle != null)
                {
                    // ARCADE:
                    if (cmb_playstyle.GetSelectedIndex() == 0)
                    {
                        player.SetFlightModel(SimElements.FLIGHT_MODEL.FM_ARCADE);
                        player.SetLandingModel(SimElements.LANDING_MODEL.LM_EASIER);
                        player.SetHUDMode(0);
                        player.SetGunsight(1);

                        if (stars != null)
                        {
                            KeyMap   keymap = stars.GetKeyMap();

                            keymap.Bind(KeyMap.KEY_CONTROL_MODEL, 1, 0);
                            keymap.SaveKeyMap("key.cfg", 256);

                            stars.MapKeys();
                        }

                        Ship.SetControlModel(1);
                    }

                    // HARDCORE:
                    else
                    {
                        player.SetFlightModel(0);
                        player.SetLandingModel(0);
                        player.SetHUDMode(0);
                        player.SetGunsight(0);

                        if (stars != null)
                        {
                            KeyMap   keymap = stars.GetKeyMap();

                            keymap.Bind(KeyMap.KEY_CONTROL_MODEL, 0, 0);
                            keymap.SaveKeyMap("key.cfg", 256);

                            stars.MapKeys();
                        }

                        Ship.SetControlModel(0);
                    }
                }

                if (cmb_experience != null && cmb_experience.GetSelectedIndex() > 0)
                {
                    player.SetRank(2);        // Lieutenant
                    player.SetTrained(255);   // Fully Trained
                }

                Player.Save();
            }

            manager.ShowMenuDlg();
        }


        protected MenuScreen manager;

        protected EditBox edt_name;
        protected ComboBox cmb_playstyle;
        protected ComboBox cmb_experience;

        protected Button btn_apply;
    }
}