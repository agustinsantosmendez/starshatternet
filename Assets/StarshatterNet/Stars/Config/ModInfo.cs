﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      ModInfo.h/ModInfo.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Information block for describing and deploying third party mods
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using StarshatterNet.Gen.Misc;
using UnityEngine;

namespace StarshatterNet.Config
{
    public class ModInfo
    {
        public ModInfo() { }
        public ModInfo(string filename)
        {
            if (!string.IsNullOrWhiteSpace(filename))
                Load(filename);
        }
        public ModInfo(string name, string version, string url)
        {
            this.name = name;
            this.version = version;
            this.url = url;
        }

        //public ~ModInfo();

        //public int operator ==(ModInfo& m) { return name.length() && name == m.name; }

        public string Name() { return name; }
        public string Description() { return desc; }
        public string Author() { return author; }
        public string URL() { return url; }
        public string Filename() { return filename; }
        public string Copyright() { return copyright; }
        public Texture2D LogoImage() { return logo; }
        public string Version() { return version; }
        public bool Distribute() { return distribute; }
        public bool IsEnabled() { return enabled; }

        public List<ModCampaign> GetCampaigns() { return campaigns; }

        public static ModInfo Load(string filename)
        {
            ModInfo modinfo = null;
            if (!File.Exists(filename))
            {
                ErrLogger.PrintLine("WARNING: invalid {0} file.", filename);
            }
            else
            {
                using (XmlReader reader = XmlReader.Create(filename))
                    modinfo = ModInfo.Load(reader);
            }
            return modinfo;
        }

        /// <summary>
        /// Loads the mod configuration entries.
        /// </summary>
        /// <param name="reader">
        /// An XML reader for th XML document containing the mod configuration.
        /// </param>
        public static ModInfo Load(XmlReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException();

            if (!reader.ReadToFollowing("ModInfo"))
                throw new Exception("ModInfo entry is missing");

            ModInfo modinfo = new ModInfo();

            var attr = reader.GetAttribute("name");
            if (!string.IsNullOrWhiteSpace(attr))
                modinfo.name = attr;
            else {
                ErrLogger.PrintLine("Error in mod config. Parsing attribute name = {0}", attr);
                return null;
            }

            attr = reader.GetAttribute("desc");
            if (!string.IsNullOrWhiteSpace(attr))
                modinfo.desc = attr;
            else
            {
                ErrLogger.PrintLine("Error in mod config. Parsing attribute desc = {0}", attr);
                return null;
            }

            attr = reader.GetAttribute("author");
            if (!string.IsNullOrWhiteSpace(attr))
                modinfo.author = attr;
            else
                 ErrLogger.PrintLine("Warning in mod config. Parsing attribute author = {0}", attr);

            attr = reader.GetAttribute("url");
            if (!string.IsNullOrWhiteSpace(attr))
                modinfo.url = attr;

            attr = reader.GetAttribute("copyright");
            if (!string.IsNullOrWhiteSpace(attr))
                modinfo.copyright = attr;

            attr = reader.GetAttribute("logo");
            if (!string.IsNullOrWhiteSpace(attr))
                modinfo.logoname = attr;

            attr = reader.GetAttribute("version");
            if (!string.IsNullOrWhiteSpace(attr))
                modinfo.version = attr;

            attr = reader.GetAttribute("distribute");
            if (!string.IsNullOrWhiteSpace(attr))
            {
                bool success = Boolean.TryParse(attr, out modinfo.distribute);
                if (!success)
                    ErrLogger.PrintLine("Error in mod config. Parsing boolean attribute 'distribute' = {0}", attr);
            }

            if (reader.ReadToDescendant("catalog"))
            {
                ModCatalog c = new ModCatalog();
                if (!reader.HasAttributes)
                    ErrLogger.PrintLine("WARNING: catalog structure missing in mod_info.def for {0}", modinfo.name);

                attr = reader.GetAttribute("file");
                if (!string.IsNullOrWhiteSpace(attr))
                    c.file = attr;
                attr = reader.GetAttribute("path");
                if (!string.IsNullOrWhiteSpace(attr))
                    c.path = attr;

                modinfo.catalog = c;
            }
            return modinfo;
        }

        public bool Enable()
        {
#warning ModInfo.Enable is not yet implemented
#if TODO
            DataLoader* loader = DataLoader::GetLoader();

            if (loader && !enabled)
            {
                loader->EnableDatafile(filename);
                enabled = true;

                if (catalog)
                    ShipDesign::LoadCatalog(catalog->Path(), catalog->File(), true);

                ShipDesign::LoadSkins("/Mods/Skins/", filename);

                for (int i = 0; i < campaigns.size(); i++)
                {
                    ModCampaign* c = campaigns[i];
                    Campaign::CreateCustomCampaign(c->Name(), c->Path());
                }
            }
#endif
            ErrLogger.PrintLine("Modinfo Enable() is not yet Implemented");
            return enabled;
        }

        public bool Disable()
        {
#warning ModInfo.Disable is not yet implemented
#if TODO
            DataLoader* loader = DataLoader::GetLoader();

            if (loader)
            {
                loader->DisableDatafile(filename);
                enabled = false;
            }
#endif
            ErrLogger.PrintLine("Modinfo Disable() is not yet Implemented");
            return !enabled;
        }


        private string name;
        private string desc;
        private string author;
        private string url;
        private string filename;
        private string copyright;
        private Texture2D logo = null;
        private string logoname;
        private string version;
        private bool distribute = false;
        private bool enabled = false;

        private List<ModCampaign> campaigns = new List<ModCampaign>();
        private ModCatalog catalog = null;
    }

    public class ModCampaign
    {
        public ModCampaign()   { }
        //public ~ModCampaign() { }

        public string Name() { return name; }
        public string Path() { return path; }
        public bool IsDynamic() { return isdynamic; }


        public string name;
        public string path;
        public bool isdynamic = false;
    }

    // +-------------------------------------------------------------------+

    public class ModCatalog
    {

        public ModCatalog()
        { }
        ///public ~ModCatalog() { }

        public string File() { return file; }
        public string Path() { return path; }


        public string file;
        public string path;
    }
}
