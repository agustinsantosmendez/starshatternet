﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Mission.h/Mission.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Universe and Region classes
*/
using StarshatterNet.Stars.StarSystems;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using System;
using UnityEngine;
using StarshatterNet.Stars.SimElements;
using DigitalRune.Mathematics.Algebra;
using DigitalRune.Mathematics;
using System.IO;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning Mission class is still in development and is not recommended for production.
    public class Mission
    {
        public enum TYPE
        {
            PATROL,
            SWEEP,
            INTERCEPT,
            AIR_PATROL,
            AIR_SWEEP,
            AIR_INTERCEPT,
            STRIKE,     // ground attack
            ASSAULT,    // starship attack
            DEFEND,
            ESCORT,
            ESCORT_FREIGHT,
            ESCORT_SHUTTLE,
            ESCORT_STRIKE,
            INTEL,
            SCOUT,
            RECON,
            BLOCKADE,
            FLEET,
            BOMBARDMENT,
            FLIGHT_OPS,
            TRANSPORT,
            CARGO,
            TRAINING,
            OTHER
        };

        public Mission(int identity, string fname = null, string pname = null)
        {
            id = identity; type = 0; team = 1; ok = false; active = false; complete = false;
            star_system = null; start = 33 * 3600; stardate = 0; target = null; ward = null;
            current = null; degrees = false;

            objective = ContentBundle.Instance.GetText("Mission.unspecified");
            sitrep = ContentBundle.Instance.GetText("Mission.unknown");

            if (!string.IsNullOrEmpty(fname))
                filename = fname;
            else
                filename = "";

            if (!string.IsNullOrEmpty(pname))
                path = pname;
            else
                path = "Missions/";

        }
        // ~Mission();

        //int operator ==(  Mission& m)     { return id == m.id;   }

        public virtual void Validate()
        {
            string err;

            ok = true;

            if (elements.IsEmpty())
            {
                err = string.Format(Game.GetText("Mission.error.no-elem"), filename);
                AddError(err);
            }
            else
            {
                bool found_player = false;

                for (int i = 0; i < elements.Count; i++)
                {
                    MissionElement elem = elements[i];

                    if (String.IsNullOrEmpty(elem.Name()))
                    {
                        err = string.Format(Game.GetText("Mission.error.unnamed-elem"), filename);
                        AddError(err);
                    }

                    if (elem.Player() > 0)
                    {
                        if (!found_player)
                        {
                            found_player = true;

                            if (elem.Region() != GetRegion())
                            {
                                err = string.Format(Game.GetText("Mission.error.wrong-sector"), elem.Name(), GetRegion());
                                AddError(err);
                            }
                        }
                        else
                        {
                            err = string.Format(Game.GetText("Mission.error.extra-player"), elem.Name(), filename);
                            AddError(err);
                        }
                    }
                }

                if (!found_player)
                {
                    err = string.Format(Game.GetText("Mission.error.no-player"), filename);
                    AddError(err);
                }
            }
        }
        public virtual bool Load(string fname = null, string pname = null)
        {
            ok = false;

            if (!string.IsNullOrWhiteSpace(fname))
                filename = fname;

            if (!string.IsNullOrWhiteSpace(pname))
                path = pname;

            if (string.IsNullOrWhiteSpace(filename))
            {
                ErrLogger.PrintLine("\nCan't Load Mission, script unspecified.");
                return ok;
            }
            // wipe existing mission before attempting to load...
            elements.Clear();
            events.Clear();

            string fullname = Path.Combine(path, filename);
            ErrLogger.PrintLine("\nLoad Mission: '{0}'", fullname);
            ok = ParseMission(fullname);
            ErrLogger.PrintLine("Mission Loaded.");

            if (ok)
                Validate();

            return ok;
        }
        public virtual bool Save() { throw new System.NotImplementedException(); }
        public virtual void SetPlayer(MissionElement player_element)
        {
            foreach (MissionElement element in elements)
            {
                if (element == player_element)
                    element.player = 1;
                else
                    element.player = 0;
            }
        }
        public virtual MissionElement GetPlayer()
        {
            MissionElement p = null;

            foreach (MissionElement elem in elements)
            {
                if (elem.player > 0)
                    p = elem;
            }

            return p;
        }

        // accessors/mutators:
        public int Identity() { return id; }
        public string FileName() { return filename; }
        public string Name() { return name; }
        public string Description() { return desc; }
        public string Situation() { return sitrep; }
        public string Objective() { return objective; }
        public string Subtitles()
        {
            return subtitles;
        }

        public int Start() { return start; }
        public double Stardate() { return stardate; }
        public TYPE Type() { return type; }
        public string TypeName() { return RoleName(type); }
        public int Team() { return team; }
        public bool IsOK() { return ok; }
        public bool IsActive() { return active; }
        public bool IsComplete() { return complete; }

        public StarSystem GetStarSystem() { return star_system; }
        public List<StarSystem> GetSystemList() { return system_list; }
        public string GetRegion() { return region; }

        public List<MissionElement> GetElements() { return elements; }
        public virtual MissionElement FindElement(string name)
        {
            foreach (MissionElement elem in elements)
            {
                if (elem.Name() == name)
                    return elem;
            }

            return null;
        }

        public virtual void AddElement(MissionElement elem)
        {
            if (elem != null)
                elements.Add(elem);
        }

        public List<MissionEvent> GetEvents() { return events; }
        public MissionEvent FindEvent(MissionEvent.EVENT_TYPE event_type)
        {
            foreach (MissionEvent evnt in events)
            {

                if (evnt.Event() == event_type)
                    return evnt;
            }

            return null;
        }
        public virtual void AddEvent(MissionEvent evnt)
        {
            if (evnt != null)
                events.Add(evnt);
        }

        public MissionElement GetTarget() { return target; }
        public MissionElement GetWard() { return ward; }

        public void SetName(string n) { name = n; }
        public void SetDescription(string d) { desc = d; }
        public void SetSituation(string sit) { sitrep = sit; }
        public void SetObjective(string obj) { objective = obj; }
        public void SetStart(int s) { start = s; }
        public void SetType(TYPE t) { type = t; }
        public void SetTeam(int iff) { team = iff; }
        public void SetStarSystem(StarSystem s)
        {
            if (star_system != s)
            {
                star_system = s;

                if (!system_list.Contains(s))
                    system_list.Add(s);
            }
        }
        public void SetRegion(string rgn) { region = rgn; }
        public void SetOK(bool a) { ok = a; }
        public void SetActive(bool a) { active = a; }
        public void SetComplete(bool c) { complete = c; }
        public void SetTarget(MissionElement t) { target = t; }
        public void SetWard(MissionElement w) { ward = w; }

        public void ClearSystemList()
        {
            star_system = null;
            system_list.Clear();
        }

        public void IncreaseElemPriority(int elem_index)
        {
            if (elem_index > 0 && elem_index < elements.Count)
            {
                MissionElement elem1 = elements[elem_index - 1];
                MissionElement elem2 = elements[elem_index];

                elements[elem_index - 1] = elem2;
                elements[elem_index] = elem1;
            }
        }

        public void DecreaseElemPriority(int elem_index)
        {
            if (elem_index >= 0 && elem_index < elements.Count - 1)
            {
                MissionElement elem1 = elements[elem_index];
                MissionElement elem2 = elements[elem_index + 1];

                elements[elem_index] = elem2;
                elements[elem_index + 1] = elem1;
            }
        }
        public void IncreaseEventPriority(int event_index)
        {
            if (event_index > 0 && event_index < events.Count)
            {
                MissionEvent event1 = events[event_index - 1];
                MissionEvent event2 = events[event_index];

                events[event_index - 1] = event2;
                events[event_index] = event1;
            }
        }
        public void DecreaseEventPriority(int event_index)
        {
            if (event_index >= 0 && event_index < events.Count - 1)
            {
                MissionEvent event1 = events[event_index];
                MissionEvent event2 = events[event_index + 1];

                events[event_index] = event2;
                events[event_index + 1] = event1;
            }
        }

        public static string RoleName(TYPE role)
        {
            switch (role)
            {
                case TYPE.PATROL: return "Patrol";
                case TYPE.SWEEP: return "Sweep";
                case TYPE.INTERCEPT: return "Intercept";
                case TYPE.AIR_PATROL: return "Airborne Patrol";
                case TYPE.AIR_SWEEP: return "Airborne Sweep";
                case TYPE.AIR_INTERCEPT: return "Airborne Intercept";
                case TYPE.STRIKE: return "Strike";
                case TYPE.ASSAULT: return "Assault";
                case TYPE.DEFEND: return "Defend";
                case TYPE.ESCORT: return "Escort";
                case TYPE.ESCORT_FREIGHT: return "Freight Escort";
                case TYPE.ESCORT_SHUTTLE: return "Shuttle Escort";
                case TYPE.ESCORT_STRIKE: return "Strike Escort";
                case TYPE.INTEL: return "Intel";
                case TYPE.SCOUT: return "Scout";
                case TYPE.RECON: return "Recon";
                case TYPE.BLOCKADE: return "Blockade";
                case TYPE.FLEET: return "Fleet";
                case TYPE.BOMBARDMENT: return "Attack";
                case TYPE.FLIGHT_OPS: return "Flight Ops";
                case TYPE.TRANSPORT: return "Transport";
                case TYPE.CARGO: return "Cargo";
                case TYPE.TRAINING: return "Training";
                default:
                case TYPE.OTHER: return "Misc";
            }
        }

        public static TYPE TypeFromName(string n)
        {
            TYPE result;
            if (!Enum.TryParse<TYPE>(n, true, out result))
                ErrLogger.PrintLine("ERROR: Invalid mission type {0}", n);
            if (result < TYPE.PATROL)
            {
                for (TYPE i = TYPE.PATROL; i <= TYPE.OTHER && result < TYPE.PATROL; i++)
                {
                    if (n == RoleName(i))
                    {
                        result = i;
                    }
                }
            }

            return result;
        }

        public string ErrorMessage() { return errmsg; }
        public void AddError(string err)
        {
            ErrLogger.PrintLine(err);
            errmsg += err;

            ok = false;
        }

        public string Serialize(string player_elem = null, int player_index = 0) { throw new System.NotImplementedException(); }
        public virtual bool ParseMission(string fname)
        {
            string target_name = null;
            string ward_name = null;

            var mi = ReaderSupport.DeserializeAsset<Serialization.MissionMainInfo>(fname);
            if (mi == null) return false;

            Serialization.MissionInfo m = mi.MISSION;
            if (!string.IsNullOrEmpty(m.name))
                name = Game.GetText(m.name);
            if (!string.IsNullOrEmpty(m.desc))
                desc = Game.GetText(m.desc);
            if (!string.IsNullOrEmpty(m.type))
                type = TypeFromName(m.type);
            if (!string.IsNullOrEmpty(m.system))
            {
                Galaxy galaxy = Galaxy.GetInstance();

                if (galaxy != null)
                {
                    SetStarSystem(galaxy.GetSystem(m.system));
                }
            }
            if (!string.IsNullOrEmpty(m.degrees))
                degrees = bool.Parse(m.degrees);
            if (!string.IsNullOrEmpty(m.region))
                region = m.region;
            if (m.objective != null && m.objective.Length > 0)
                objective = string.Join(" ", m.objective);
            if (m.sitrep != null && m.sitrep.Length > 0)
                sitrep = string.Join(" ", m.sitrep);
            if (!string.IsNullOrEmpty(m.subtitles))
            {
                string subtitles_path = m.subtitles;
                // TODO review if subtitles are assets or data
                subtitles = "\n" + ReaderSupport.ReadAllDataText(subtitles_path);
            }
            if (!string.IsNullOrEmpty(m.start))
                start = ReaderSupport.GetDefTime(m.start);
            if (!string.IsNullOrEmpty(m.stardate))
                stardate = ReaderSupport.GetDefTime(m.stardate);
            if (!string.IsNullOrEmpty(m.team))
                team = int.Parse(m.team);
            if (!string.IsNullOrEmpty(m.target))
                target_name = m.target;
            if (!string.IsNullOrEmpty(m.ward))
                ward_name = m.ward;
            if (m.elements != null && m.elements.Length != 0)
                foreach (var e in m.elements)
                {
                    MissionElement elem = ParseElement(e, filename);
                    AddElement(elem);
                }
            if (m.events != null && m.events.Length != 0)
                foreach (var e in m.events)
                {
                    MissionEvent evnt = ParseEvent(e, filename);
                    AddEvent(evnt);
                }
            if (!string.IsNullOrEmpty(target_name))
                target = FindElement(target_name);

            if (!string.IsNullOrEmpty(ward_name))
                ward = FindElement(ward_name);
            ok = true;
            return ok;
        }
        static int elem_id = 351;
        protected MissionElement ParseElement(Serialization.MissionElement val, string file)
        {
            MissionElement element = new MissionElement();
            element.rgn_name = region;
            element.elem_id = elem_id++;
            if (!string.IsNullOrEmpty(val.name))
                element.name = val.name;
            if (!string.IsNullOrEmpty(val.carrier))
                element.carrier = val.carrier;
            if (!string.IsNullOrEmpty(val.commander))
                element.commander = val.commander;
            if (!string.IsNullOrEmpty(val.squadron))
                element.squadron = val.squadron;
            if (!string.IsNullOrEmpty(val.path))
                element.path = val.path;
            if (!string.IsNullOrEmpty(val.design))
                element.design = ShipDesign.Get(val.design, element.path);
            if (!string.IsNullOrEmpty(val.skin))
                element.skin = element.design.FindSkin(val.skin);
            if (!string.IsNullOrEmpty(val.mission))
                element.mission_role = TypeFromName(val.mission);
            if (!string.IsNullOrEmpty(val.intel))
                element.intel = Intel.IntelFromName(val.intel);
            if (val.loc != null && val.loc.Length != 0)
            {
                Vector3D loc = ReaderSupport.GetDefVec(val.loc, file);
                element.SetLocation(loc);
            }
            if (val.rloc != null)
            {
                RLoc rloc = ParseRLoc(val.rloc, file);
                element.SetRLoc(rloc);
            }
            if (val.head != null)
            {
                Vector3D head = ReaderSupport.GetDefVec(val.head, file);
                if (degrees) head.Z *= (float)ConstantsF.DEGREES;
                element.heading = head.Z;
            }
            if (!string.IsNullOrEmpty(val.region))
                element.rgn_name = val.region;
            if (!string.IsNullOrEmpty(val.iff))
                element.IFF_code = int.Parse(val.iff);
            if (!string.IsNullOrEmpty(val.count))
                element.count = int.Parse(val.count);
            if (!string.IsNullOrEmpty(val.maint_count))
                element.maint_count = int.Parse(val.maint_count);
            if (!string.IsNullOrEmpty(val.dead_count))
                element.dead_count = int.Parse(val.dead_count);
            if (!string.IsNullOrEmpty(val.player))
                element.player = int.Parse(val.player);
            if (!string.IsNullOrEmpty(val.alert))
                element.alert = bool.Parse(val.alert);
            if (!string.IsNullOrEmpty(val.playable))
                element.playable = bool.Parse(val.playable);
            if (!string.IsNullOrEmpty(val.rogue))
                element.rogue = bool.Parse(val.rogue);
            if (!string.IsNullOrEmpty(val.invulnerable))
                element.invulnerable = bool.Parse(val.invulnerable);
            if (!string.IsNullOrEmpty(val.command_ai))
                element.command_ai = int.Parse(val.command_ai);
            if (!string.IsNullOrEmpty(val.respawn))
                element.respawns = int.Parse(val.respawn);
            if (!string.IsNullOrEmpty(val.hold))
                element.hold_time = int.Parse(val.hold);
            if (!string.IsNullOrEmpty(val.zone))
            {
                bool locked;
                if (bool.TryParse(val.zone, out locked))
                    element.zone_lock = locked ? 1 : 0;
                else
                    element.zone_lock = int.Parse(val.zone);
            }
            if (val.objectives != null && val.objectives.Length != 0)
                foreach (var o in val.objectives)
                {
                    Instruction obj = ParseInstruction(o, element, file);
                    element.objectives.Add(obj);
                }
            if (val.instrs != null && val.instrs.Length != 0)
                foreach (var o in val.instrs)
                    element.instructions.Add(o);
            if (val.ships != null && val.ships.Length != 0)
                foreach (var o in val.ships)
                {
                    MissionShip s = ParseShip(o, element, file);
                    element.ships.Add(s);

                    if (s.Integrity() < 0 && element.design != null)
                        s.SetIntegrity(element.design.integrity);
                }
            if (val.navpts != null && val.navpts.Length != 0)
                foreach (var o in val.navpts)
                {
                    Instruction npt = ParseInstruction(o, element, file);
                    element.navlist.Add(npt);
                }
            if (val.loadout != null)
                ParseLoadout(val.loadout, element, file);

            if (string.IsNullOrEmpty(element.name))
                ErrLogger.PrintLine(Game.GetText("Mission.error.unnamed-elem"), filename);

            if (element.design == null)
                ErrLogger.PrintLine(Game.GetText("Mission.error.unknown-ship"), element.name, filename);

            current = null;

            return element;
        }

        protected MissionEvent ParseEvent(Serialization.MissionEvent val, string file)
        {
            int event_id = 1;
            double event_time = 0;

            MissionEvent evnt = new MissionEvent();
            if (!string.IsNullOrEmpty(val.@event))
                evnt.evnt = MissionEvent.EventForName(val.@event);
            if (!string.IsNullOrEmpty(val.trigger))
                evnt.trigger = MissionEvent.TriggerForName(val.trigger);
            if (!string.IsNullOrEmpty(val.id))
                event_id = int.Parse(val.id);
            if (!string.IsNullOrEmpty(val.time))
                event_time = int.Parse(val.time);
            if (!string.IsNullOrEmpty(val.delay))
                evnt.delay = int.Parse(val.delay);
            if (val.event_param != null && val.event_param.Length != 0)
            {
                evnt.event_param.Initialize();
                for (int i = 0; i < 10 && i < val.event_param.Length; i++)
                {
                    float f = float.Parse(val.event_param[i]);
                    evnt.event_param[i] = (int)f;
                    evnt.event_nparams = i + 1;
                }

            }
            if (val.trigger_param != null && val.trigger_param.Length != 0)
            {
                evnt.trigger_param.Initialize();
                for (int i = 0; i < 10 && i < val.trigger_param.Length; i++)
                {
                    float f = float.Parse(val.trigger_param[i]);
                    evnt.trigger_param[i] = (int)f;
                    evnt.trigger_nparams = i + 1;
                }
            }
            if (!string.IsNullOrEmpty(val.event_ship))
                evnt.event_ship = val.event_ship;
            if (!string.IsNullOrEmpty(val.event_source))
                evnt.event_source = val.event_source;
            if (!string.IsNullOrEmpty(val.event_target))
                evnt.event_target = val.event_target;
            if (!string.IsNullOrEmpty(val.event_message))
            {
                string raw_msg = Game.GetText(val.event_message);
                //evnt.event_message = ReaderSupport.FormatTextEscape(raw_msg);
                evnt.event_message = raw_msg;
            }
            if (!string.IsNullOrEmpty(val.event_chance))
                evnt.event_chance = int.Parse(val.event_chance);
            if (!string.IsNullOrEmpty(val.event_sound))
                evnt.event_sound = val.event_sound;
            if (val.loc != null && val.loc.Length == 3)
                evnt.event_point = ReaderSupport.GetDefVec(val.loc, file);
            if (val.rect != null && val.rect.Length == 4)
                evnt.event_rect = ReaderSupport.GetDefRect(val.rect, file);
            if (!string.IsNullOrEmpty(val.trigger_ship))
                evnt.trigger_ship = val.trigger_ship;
            if (!string.IsNullOrEmpty(val.trigger_target))
                evnt.trigger_target = val.trigger_target;

            evnt.id = event_id++;
            evnt.time = event_time;
            return evnt;
        }

        protected MissionShip ParseShip(Serialization.MissionShip val, MissionElement element, string file)
        {
            MissionShip msn_ship = new MissionShip();
            string name = "";
            string regnum = null;
            string region = null;
            Vector3D loc = new Vector3D(-1.0e9f, -1.0e9f, -1.0e9f);
            Vector3D vel = new Vector3D(-1.0e9f, -1.0e9f, -1.0e9f);
            int respawns = -1;
            double heading = -1e9;
            double integrity = -1;
            int[] ammo = new int[16];
            int[] fuel = new int[4];
            int i;

            for (i = 0; i < 16; i++)
                ammo[i] = -10;

            for (i = 0; i < 4; i++)
                fuel[i] = -10;

            if (!string.IsNullOrEmpty(val.name))
                name = val.name;
            if (!string.IsNullOrEmpty(val.skin))
                msn_ship.skin = element.design.FindSkin(val.skin);
            if (!string.IsNullOrEmpty(val.regnum))
                regnum = val.regnum;
            if (!string.IsNullOrEmpty(val.region))
                region = val.region;
            if (val.loc != null && val.loc.Length == 3)
                loc = ReaderSupport.GetDefVec(val.loc, file);
            if (val.velocity != null && val.velocity.Length == 3)
                vel = ReaderSupport.GetDefVec(val.velocity, file);
            if (!string.IsNullOrEmpty(val.respawns))
                respawns = int.Parse(val.respawns);
            if (!string.IsNullOrEmpty(val.heading))
            {
                double h = 0;
                h = double.Parse(val.heading);
                if (degrees) h *= ConstantsF.DEGREES;
                heading = h;
            }
            if (!string.IsNullOrEmpty(val.integrity))
                integrity = double.Parse(val.integrity);
            if (val.ammo != null && val.ammo.Length == 16)
                for (int k = 0; k < 16; k++)
                    ammo[k] = int.Parse(val.ammo[k]);
            if (val.fuel != null && val.fuel.Length == 4)
                for (int k = 0; k < 4; k++)
                    fuel[k] = int.Parse(val.fuel[k]);

            msn_ship.SetName(name);
            msn_ship.SetRegNum(regnum);
            msn_ship.SetRegion(region);
            msn_ship.SetIntegrity(integrity);

            if (loc.X > -1e9)
                msn_ship.SetLocation(loc);

            if (vel.X > -1e9)
                msn_ship.SetVelocity(vel);

            if (respawns > -1)
                msn_ship.SetRespawns(respawns);

            if (heading > -1e9)
                msn_ship.SetHeading(heading);

            if (ammo[0] > -10)
                msn_ship.SetAmmo(ammo);

            if (fuel[0] > -10)
                msn_ship.SetFuel(fuel);

            return msn_ship;
        }

        protected Instruction ParseInstruction(Serialization.Instruction val, MissionElement element, string file)
        {
            Instruction.ACTION order = Instruction.ACTION.VECTOR;
            Instruction.STATUS status = Instruction.STATUS.PENDING;
            int formation = 0;
            int speed = 0;
            int priority = 1;
            int farcast = 0;
            int hold = 0;
            int emcon = 0;
            Vector3D loc = new Vector3D(0, 0, 0);
            RLoc rloc = null;
            string order_name;
            string status_name;
            string order_rgn_name = null;
            string tgt_name = null;
            string tgt_desc = null;

            if (!string.IsNullOrEmpty(val.cmd))
            {
                order_name = val.cmd;
                for (Instruction.ACTION cmd = 0; cmd < Instruction.ACTION.NUM_ACTIONS; cmd++)
                    if (order_name.Equals(Instruction.ActionName(cmd), StringComparison.InvariantCultureIgnoreCase))
                        order = (Instruction.ACTION)cmd;
            }
            if (!string.IsNullOrEmpty(val.status))
            {
                status_name = val.status;
                for (Instruction.STATUS n = 0; n < Instruction.STATUS.NUM_STATUS; n++)
                    if (status_name.Equals(Instruction.StatusName(n), StringComparison.InvariantCultureIgnoreCase))
                        status = (Instruction.STATUS)n;
            }
            if (val.loc != null && val.loc.Length == 3)
                loc = ReaderSupport.GetDefVec(val.loc, filename);
            if (val.rloc != null)
                rloc = ParseRLoc(val.rloc, filename);
            if (!string.IsNullOrEmpty(val.rgn))
                order_rgn_name = val.rgn;
            if (!string.IsNullOrEmpty(val.speed))
                speed = int.Parse(val.speed);
            if (!string.IsNullOrEmpty(val.formation))
                formation = int.Parse(val.formation);
            if (!string.IsNullOrEmpty(val.emcon))
                emcon = int.Parse(val.emcon);
            if (!string.IsNullOrEmpty(val.priority))
                priority = int.Parse(val.priority);
            if (!string.IsNullOrEmpty(val.farcast))
                farcast = int.Parse(val.farcast);
            if (!string.IsNullOrEmpty(val.tgt))
                tgt_name = val.tgt;
            if (!string.IsNullOrEmpty(val.tgt_desc))
                tgt_desc = val.tgt_desc;
            if (!string.IsNullOrEmpty(val.hold))
                hold = int.Parse(val.hold);

            string rgn;

            if (!string.IsNullOrEmpty(order_rgn_name))
                rgn = order_rgn_name;

            else if (element.navlist.Count > 0)
                rgn = element.navlist[element.navlist.Count - 1].RegionName();

            else
                rgn = region;

            if (!string.IsNullOrEmpty(tgt_desc) && !string.IsNullOrEmpty(tgt_name))
                tgt_desc = tgt_desc + " " + tgt_name;

            Instruction instr = new Instruction(rgn, loc, order);

            instr.SetStatus(status);
            instr.SetEMCON(emcon);
            instr.SetFormation((Instruction.FORMATION)formation);
            instr.SetSpeed(speed);
            instr.SetTarget(tgt_name);
            instr.SetTargetDesc(tgt_desc);
            instr.SetPriority((Instruction.PRIORITY)(priority - 1));
            instr.SetFarcast(farcast);
            instr.SetHoldTime(hold);

            if (rloc != null)
            {
                instr.SetRLoc(rloc);
                //delete rloc;
            }

            return instr;
        }

        protected void ParseLoadout(Serialization.MissionLoadout val, MissionElement element, string file)
        {
            if (val.ship == null && val.name == null && val.stations == null) return;
            int ship = -1;
            int[] stations = new int[16];
            string name = null;

            if (!string.IsNullOrEmpty(val.ship))
                ship = int.Parse(val.ship);
            if (!string.IsNullOrEmpty(val.name))
                name = val.name;
            if (val.stations != null && val.stations.Length == 16)
                for (int i = 0; i < 16; i++)
                    stations[i] = int.Parse(val.stations[i]);

            MissionLoad load = new MissionLoad(ship);

            if (!string.IsNullOrEmpty(name))
                load.SetName(name);

            for (int i = 0; i < 16; i++)
                load.SetStation(i, stations[i]);

            element.loadouts.Add(load);
        }

        protected RLoc ParseRLoc(Serialization.RLoc val, string file)
        {
            Vector3D base_loc = new Vector3D();
            RLoc rloc = new RLoc();
            RLoc @ref = null;

            double dex = 0;
            double dex_var = 5e3;
            double az = 0;
            double az_var = Math.PI;
            double el = 0;
            double el_var = 0.1;

            if (!string.IsNullOrEmpty(val.dex))
            {
                dex = double.Parse(val.dex);
                rloc.SetDistance(dex);
            }
            if (!string.IsNullOrEmpty(val.dex_var))
            {
                dex_var = double.Parse(val.dex_var);
                rloc.SetDistanceVar(dex_var);
            }
            if (!string.IsNullOrEmpty(val.az))
            {
                az = double.Parse(val.az);
                if (degrees) az *= ConstantsF.DEGREES;
                rloc.SetAzimuth(az);
            }
            if (!string.IsNullOrEmpty(val.az_var))
            {
                az_var = double.Parse(val.az_var);
                if (degrees) az_var *= ConstantsF.DEGREES;
                rloc.SetAzimuthVar(az_var);
            }
            if (!string.IsNullOrEmpty(val.el))
            {
                el = double.Parse(val.el);
                if (degrees) el *= ConstantsF.DEGREES;
                rloc.SetElevation(el);
            }
            if (!string.IsNullOrEmpty(val.el_var))
            {
                el_var = double.Parse(val.el_var);
                if (degrees) el_var *= ConstantsF.DEGREES;
                rloc.SetElevationVar(el_var);
            }
            if (val.loc != null && val.loc.Length == 3)
            {
                base_loc = ReaderSupport.GetDefVec(val.loc, filename);
                rloc.SetBaseLocation(base_loc);
            }
            if (!string.IsNullOrEmpty(val.@ref))
            {
                string refstr = val.@ref;
                int sep = refstr.IndexOf(':');
                if (sep >= 0)
                {
                    string elem_name = refstr.Substring(0, sep);
                    string nav_name = refstr.Substring(sep + 1, refstr.Length);
                    MissionElement elem = null;

                    if (elem_name == "this")
                        elem = current;
                    else
                        elem = FindElement(elem_name);

                    if (elem != null && elem.NavList().Count > 0)
                    {
                        int index = int.Parse(nav_name) - 1;
                        if (index < 0)
                            index = 0;
                        else if (index >= elem.NavList().Count)
                            index = elem.NavList().Count - 1;

                        @ref = elem.NavList()[index].GetRLoc();
                        rloc.SetReferenceLoc(@ref);
                    }
                    else
                    {
                        ErrLogger.PrintLine("Warning: no ref found for rloc '{0}' in elem '{1}'", refstr, current.Name());
                        rloc.SetBaseLocation(RandomHelper.RandomPoint());
                    }
                }
                else
                {
                    MissionElement elem = null;

                    if (refstr == "this")
                        elem = current;
                    else
                        elem = FindElement(refstr);

                    if (elem != null)
                    {
                        @ref = elem.GetRLoc();
                        rloc.SetReferenceLoc(@ref);
                    }
                    else
                    {
                        ErrLogger.PrintLine("Warning: no ref found for rloc '{0}' in elem '{1}'", refstr, current.Name());
                        rloc.SetBaseLocation(RandomHelper.RandomPoint());
                    }
                }
            }
            return rloc;
        }


        protected int id;
        protected string filename;
        protected string path;
        protected string region;
        protected string name;
        protected string desc;
        protected TYPE type;
        protected int team;
        protected int start;
        protected double stardate;
        protected bool ok;
        protected bool active;
        protected bool complete;
        protected bool degrees;
        protected string objective;
        protected string sitrep;
        protected string errmsg;
        protected string subtitles;
        protected StarSystem star_system;
        protected List<StarSystem> system_list = new List<StarSystem>();

        protected List<MissionElement> elements = new List<MissionElement>();
        protected List<MissionEvent> events = new List<MissionEvent>();

        protected MissionElement target;
        protected MissionElement ward;
        protected MissionElement current;
    }
}
namespace StarshatterNet.Stars.MissionCampaign.Serialization
{
    [Serializable]
    public class MissionMainInfo
    {
        public MissionInfo MISSION;
    }
    [Serializable]
    public class MissionInfo
    {
        public string name;
        public string desc;
        public string type;
        public string system;
        public string degrees;
        public string region;
        public string[] objective;
        public string[] sitrep;
        public string subtitles;
        public string start;
        public string stardate;
        public string team;
        public string target;
        public string ward;
        public MissionElement[] elements; // "element", "ship", "station"
        public MissionEvent[] events;
    }

    [Serializable]
    public class MissionElement
    {
        public string name;
        public string carrier;
        public string commander;
        public string squadron;
        public string path;
        public string design;
        public string skin;
        public string mission;
        public string intel;
        public string[] loc;
        public RLoc rloc;
        public string[] head;
        public string region;
        public string iff;
        public string count;
        public string maint_count;
        public string dead_count;
        public string player;
        public string alert;
        public string playable;
        public string rogue;
        public string invulnerable;
        public string command_ai;
        public string respawn;
        public string hold;
        public string zone;
        public Instruction[] objectives;
        public string[] instrs;
        public MissionShip[] ships;
        public Instruction[] navpts;
        public MissionLoadout loadout;
    }

    [Serializable]
    public class MissionEvent
    {
        public string @event;
        public string trigger;
        public string id;
        public string time;
        public string delay;
        public string[] event_param;
        public string[] trigger_param;
        public string event_ship;
        public string event_source;
        public string event_target;
        public string event_message;
        public string event_chance;
        public string event_sound;
        public string[] loc;
        public string[] rect;
        public string trigger_ship;
        public string trigger_target;
    }

    [Serializable]
    public class MissionShip
    {
        public string name;
        public string skin;
        public string regnum;
        public string region;
        public string[] loc;
        public string[] velocity;
        public string respawns;
        public string heading;
        public string integrity;
        public string[] ammo;
        public string[] fuel;
    }

    [Serializable]
    public class MissionLoadout
    {
        public string ship;
        public string name;
        public string[] stations;
    }
    //public class MissionLoad
    //{
    //}
    [Serializable]
    public class Instruction
    {
        public string cmd;
        public string status;
        public string[] loc;
        public RLoc rloc;
        public string rgn;
        public string speed;
        public string formation;
        public string emcon;
        public string priority;
        public string farcast;
        public string tgt;
        public string tgt_desc;
        public string hold;
    }

    [Serializable]
    public class RLoc
    {
        public string dex;
        public string dex_var;
        public string az;
        public string az_var;
        public string el;
        public string el_var;
        public string[] loc;
        public string @ref;
    }
}