﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MsnEditNavDlg.h/MsnEditNavDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Briefing Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MsnEditNavDlg : NavDlg
    {
        public MsnEditNavDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
           base(s, c, def, mgr, p)
        {
            mission_info = null;
            btn_accept = null; btn_cancel = null; btn_sit = null; btn_pkg = null; btn_map = null;

            RegisterControls();
        }
        //public virtual ~MsnEditNavDlg();

        public override void RegisterControls()
        {
            btn_accept = (Button)FindControl(1);
            btn_cancel = (Button)FindControl(2);
            btn_sit = (Button)FindControl(301);
            btn_pkg = (Button)FindControl(302);
            btn_map = (Button)FindControl(303);

            txt_name = (EditBox)FindControl(201);
            cmb_type = (ComboBox)FindControl(202);
            cmb_system = (ComboBox)FindControl(203);
            cmb_region = (ComboBox)FindControl(204);

            if (btn_accept != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_accept, OnCommit);

            if (btn_cancel != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_cancel, OnCancel);

            if (btn_sit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_sit, OnTabButton);

            if (btn_pkg != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_pkg, OnTabButton);

            if (btn_map != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_map, OnTabButton);

            if (cmb_system != null)
                REGISTER_CLIENT(ETD.EID_SELECT, cmb_system, OnSystemSelect);
        }
        public override void Show()
        {
            bool need_tab_update = !shown;

            base.Show();

            if (txt_name != null && cmb_type != null)
            {
                txt_name.SetText("");

                if (cmb_system != null)
                {
                    cmb_system.ClearItems();

                    Galaxy galaxy = Galaxy.GetInstance();
                    foreach (StarSystem iter in galaxy.GetSystemList())
                    {
                        cmb_system.AddItem(iter.Name());
                    }
                }

                if (mission != null)
                {
                    int i;

                    txt_name.SetText(mission.Name());
                    cmb_type.SetSelection((int)mission.Type());

                    StarSystem sys = mission.GetStarSystem();
                    if (sys != null && cmb_system != null && cmb_region != null)
                    {
                        for (i = 0; i < cmb_system.NumItems(); i++)
                        {
                            if (cmb_system.GetItem(i) == sys.Name())
                            {
                                cmb_system.SetSelection(i);
                                break;
                            }
                        }

                        cmb_region.ClearItems();
                        int sel_rgn = 0;

                        List<OrbitalRegion> regions = new List<OrbitalRegion>();
                        regions.AddRange(sys.AllRegions());
                        regions.Sort();

                        i = 0;
                        foreach (OrbitalRegion region in regions)
                        {
                            cmb_region.AddItem(region.Name());

                            if (mission.GetRegion() == region.Name())
                            {
                                sel_rgn = i;
                            }

                            i++;
                        }

                        cmb_region.SetSelection(sel_rgn);
                    }
                }
            }

            if (need_tab_update)
            {
                ShowTab(2);
            }

            exit_latch = true;
        }
        public virtual void SetMissionInfo(MissionInfo m)
        {
            mission_info = m;
        }
        public virtual void ScrapeForm()
        {
            if (mission != null)
            {
                if (txt_name != null)
                {
                    mission.SetName(txt_name.GetText());
                }

                if (cmb_type != null)
                {
                    mission.SetType((Mission.TYPE)cmb_type.GetSelectedIndex());

                    if (mission_info != null)
                        mission_info.type = (Mission.TYPE)cmb_type.GetSelectedIndex();
                }

                Galaxy galaxy = Galaxy.GetInstance();
                StarSystem system = null;

                if (galaxy != null && cmb_system != null)
                    system = galaxy.GetSystem(cmb_system.GetSelectedItem());

                if (system != null)
                {
                    mission.ClearSystemList();
                    mission.SetStarSystem(system);

                    if (mission_info != null)
                        mission_info.system = system.Name();
                }

                if (cmb_region != null)
                {
                    mission.SetRegion(cmb_region.GetSelectedItem());

                    if (mission_info != null)
                        mission_info.region = cmb_region.GetSelectedItem();
                }

                SetSystem(system);
            }
        }

        // Operations:
        public override void OnCommit(ActiveWindow obj, AWEvent evnt)
        {
            if (mission != null)
            {
                ScrapeForm();

                if (mission_info != null)
                    mission_info.name = mission.Name();

                mission.Save();
            }

            menu_screen.ShowMsnSelectDlg();
        }

        public override void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            if (mission != null)
                mission.Load();

            menu_screen.ShowMsnSelectDlg();
        }

        public virtual void OnTabButton(ActiveWindow obj, AWEvent evnt)
        {
            if (evnt == null) return;

            if (evnt.window == btn_sit)
                ShowTab(0);

            else if (evnt.window == btn_pkg)
                ShowTab(1);

            else if (evnt.window == btn_map)
                ShowTab(2);
        }
        public virtual void OnSystemSelect(ActiveWindow obj, AWEvent evnt)
        {
            StarSystem sys = null;

            if (cmb_system != null)
            {
                string name = cmb_system.GetSelectedItem();

                Galaxy galaxy = Galaxy.GetInstance();
                foreach (StarSystem s in galaxy.GetSystemList())
                {
                    if (s.Name() == name)
                    {
                        sys = s;
                        break;
                    }
                }
            }

            if (sys != null && cmb_region != null)
            {
                cmb_region.ClearItems();

                List<OrbitalRegion> regions = new List<OrbitalRegion>();
                regions.AddRange(sys.AllRegions());
                regions.Sort();

                foreach (OrbitalRegion region in regions)
                {
                    cmb_region.AddItem(region.Name());
                }
            }

            ScrapeForm();
        }


        protected virtual void ShowTab(int tab)
        {
            if (tab < 0 || tab > 2)
                tab = 0;

            if (btn_sit != null) btn_sit.SetButtonState(tab == 0 ? (short)1 : (short)0);
            if (btn_pkg != null) btn_pkg.SetButtonState(tab == 1 ? (short)1 : (short)0);
            if (btn_map != null) btn_map.SetButtonState(tab == 2 ? (short)1 : (short)0);

            if (tab != 2)
            {
                MsnEditDlg msnEditDlg = menu_screen.GetMsnEditDlg();

                if (msnEditDlg != null)
                    msnEditDlg.ShowTab(tab);

                menu_screen.ShowMsnEditDlg();
            }
        }

        protected MenuScreen menu_screen;

        protected Button btn_accept;
        protected Button btn_cancel;

        protected Button btn_sit;
        protected Button btn_pkg;
        protected Button btn_map;

        protected EditBox txt_name;
        protected ComboBox cmb_type;
        protected ComboBox cmb_system;
        protected ComboBox cmb_region;
        protected MissionInfo mission_info;

        protected bool exit_latch;
    }
}