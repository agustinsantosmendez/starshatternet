﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      EditBox.h/EditBox.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    EditBox ActiveWindow class
*/
using System;
using StarshatterNet.Stars.Views;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.Int32;

namespace StarshatterNet.Gen.Forms
{
    public class EditBox : ScrollWindow
    {
        public enum ALIGN
        {
            EDIT_ALIGN_LEFT = TextFormat.DT_LEFT,
            EDIT_ALIGN_CENTER = TextFormat.DT_CENTER,
            EDIT_ALIGN_RIGHT = TextFormat.DT_RIGHT
        };

        public EditBox(ActiveWindow p, int ax, int ay, int aw, int ah, DWORD aid) :
            base(ax, ay, aw, ah, aid, p)
        {
            caret_x = 0; caret_y = 0;
            sel_start = 0;
            sel_length = 0;
            h_offset = 0;
            pass_char = '0';

            selected_color = Color.yellow;

            desc = "EditBox " + id;
        }
        // Operations:
        public override void DrawContent(Rect ctrl_rect) { throw new NotImplementedException(); }
        public virtual void DrawTabbedText() { throw new NotImplementedException(); }

        // Event Target Interface:
        public virtual void SetFocus() { throw new NotImplementedException(); }
        public virtual void KillFocus() { throw new NotImplementedException(); }

        public override int OnMouseMove(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonDown(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonUp(int x, int y) { throw new NotImplementedException(); }
        public override int OnClick() { throw new NotImplementedException(); }

        public override int OnKeyDown(int vk, int flags) { throw new NotImplementedException(); }

        // ScrollWindow Interface:
        public override bool CanScroll(int direction, int nlines = 1)
        {
            return false;
        }
        public virtual void Scroll(int direction, int nlines = 1)
        {
            scrolling = SCROLL.SCROLL_NONE;
        }
        public override void ScrollTo(int index)
        {
        }
        public override int GetPageCount()
        {
            return 1;
        }
        public override int GetPageSize()
        {
            return page_size;
        }
        public override string GetText() {
            if (inputFieldCmp != null)
                text = inputFieldCmp.text;

            return text;
        }

        public override void SetText(string t)
        {
            base.SetText(t);
            SetText();
        }

        public void SetText()
        {
            if (inputFieldCmp != null)
                inputFieldCmp.text = text;
        }

        public Color GetSelectedColor()
        {
            return selected_color;
        }

        public void SetSelectedColor(Color c)
        {
            if (selected_color != c)
            {
                selected_color = c;
            }
        }

        public int GetSelStart() { return sel_start; }
        public int GetSelLength() { return sel_length; }
        public string GetSelText()
        {
            if (sel_start == 0 && sel_length == text.Length)
                return text;

            string selection = text.Substring(sel_start, sel_length);

            return selection;
        }

        public void SetSelStart(int n)
        {
            if (n >= 0 && n <= text.Length)
                sel_start = n;
        }
        public void SetSelLength(int n)
        {
            if (n <= text.Length - sel_start)
                sel_length = n;
        }

        public char GetPasswordChar() { return pass_char; }
        public void SetPasswordChar(char c) { pass_char = c; }


        protected void Insert(char c)
        {
            if (single_line && c == '\n')
                return;

            if (sel_start < text.Length)
            {
                if (sel_start == 0)
                {
                    text = c + text;
                    sel_start = 1;
                }
                else
                {
                    string t1 = text.Substring(0, sel_start);
                    string t2 = text.Substring(sel_start, text.Length - sel_start);
                    text = t1 + c + t2;
                    sel_start++;
                }
            }
            else
            {
                text += c;
                sel_start = text.Length;
            }

            EnsureCaretVisible();
        }
        protected void Insert(string s)
        {
        }
        protected void Delete()
        {
            if (sel_start < text.Length)
            {
                if (sel_start == 0)
                {
                    text = text.Substring(1, text.Length - 1);
                }
                else
                {
                    string t1 = text.Substring(0, sel_start);
                    string t2 = text.Substring(sel_start + 1, text.Length - sel_start - 1);
                    text = t1 + t2;
                }
            }

            EnsureCaretVisible();
        }
        protected void Backspace()
        {
            if (sel_start > 0)
            {
                if (sel_start == text.Length)
                {
                    text = text.Substring(0, sel_start - 1);
                }
                else
                {
                    string t1 = text.Substring(0, sel_start - 1);
                    string t2 = text.Substring(sel_start, text.Length - sel_start);
                    text = t1 + t2;
                }

                sel_start--;
                EnsureCaretVisible();
            }
        }
        protected int CaretFromPoint(int x, int y)
        {
            return 0;
        }
        protected void EnsureCaretVisible() { throw new NotImplementedException(); }
        public override void MoveTo(Stars.Rect r)
        {
            if (layoutElement != null)
            {
                layoutElement.preferredWidth = r.w - 2 * LeftPadding;
                layoutElement.preferredHeight = r.h;
            }

            base.MoveTo(r);
        }

        //https://github.com/jamesjlinden/unity-decompiled/blob/master/UnityEngine.UI/UI/DefaultControls.cs
        protected internal override void BuildGamebjects()
        {
            base.BuildGamebjects();

            DefaultControls.Resources uiResources = new DefaultControls.Resources();
            inputFieldObj = DefaultControls.CreateInputField(uiResources);
            layoutElement = inputFieldObj.AddComponent<LayoutElement>();
            inputFieldCmp = inputFieldObj.GetComponent<InputField>();

            RectTransform rectTransform = inputFieldObj.GetComponent<RectTransform>();
            rectTransform.SetParent(content.transform);

            var vp = obj.transform.Find("Viewport").gameObject;
            var vpimg = inputFieldObj.GetComponent<Image>();
            Color c = vpimg.color;
            c.a = 0;
            vpimg.color = c;

            GameObject uiObject1 = inputFieldObj.transform.Find("Placeholder").gameObject;
            GameObject uiObject2 = inputFieldObj.transform.Find("Text").gameObject;
            RectTransform component1 = uiObject2.GetComponent<RectTransform>();
            component1.anchorMin = Vector2.zero;
            component1.anchorMax = Vector2.one;
            component1.sizeDelta = Vector2.zero;
            component1.offsetMin = new Vector2(0f, 0f);
            component1.offsetMax = new Vector2(0f, 0f);
            RectTransform component2 = uiObject1.GetComponent<RectTransform>();
            component2.anchorMin = Vector2.zero;
            component2.anchorMax = Vector2.one;
            component2.sizeDelta = Vector2.zero;
            component2.offsetMin = new Vector2(0f, 0f);
            component2.offsetMax = new Vector2(0f, 0f);

            InputField inputField = inputFieldObj.GetComponent<InputField>();
            inputField.textComponent.fontSize = 12;
            inputField.textComponent.color = new Color(1, 1, 1, 1);
            inputField.textComponent.alignment = TextAnchor.MiddleLeft;

            inputFieldObj.name = desc;
            inputFieldObj.SetActive(true); //Activate the GameObject

            SetText();
        }

        protected int sel_start;
        protected int sel_length;
        protected int h_offset;
        protected int caret_x;
        protected int caret_y;

        protected char pass_char;

        protected Color selected_color;
        public GameObject inputFieldObj;
        protected LayoutElement layoutElement;
        protected InputField inputFieldCmp;
    }
}
