﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CombatEvent.h/CombatEvent.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    A significant (newsworthy) event in the dynamic campaign.
*/
using System;
using System.IO;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
namespace StarshatterNet.Stars.MissionCampaign
{
#warning CombatEvent class is still in development and is not recommended for production.
    public class CombatEvent : IComparable
    {
        public enum EVENT_TYPE
        {
            ATTACK,
            DEFEND,
            MOVE_TO,
            CAPTURE,
            STRATEGY,

            CAMPAIGN_START,
            STORY,
            CAMPAIGN_END,
            CAMPAIGN_FAIL
        };

        public enum EVENT_SOURCE
        {
            FORCOM,
            TACNET,
            INTEL,
            MAIL,
            NEWS
        };

        public CombatEvent(Campaign c, EVENT_TYPE typ, int tim, int tem, EVENT_SOURCE src, string rgn)
        {
            campaign = c; type = typ; time = tim; team = tem; source = src;
            region = rgn; points = 0; visited = false;
        }

        //int operator ==(const CombatEvent& u)  const { return this == &u; }

        // accessors/mutators:
        public EVENT_TYPE Type() { return type; }
        public int Time() { return time; }
        public int GetIFF() { return team; }
        public int Points() { return points; }
        public EVENT_SOURCE Source() { return source; }
        public Point Location() { return loc; }
        public string Region() { return region; }
        public string Title() { return title; }
        public string Information() { return info; }
        public string Filename() { return file; }
        public string ImageFile() { return image_file; }
        public string SceneFile() { return scene_file; }
        public Bitmap Image() { return image; }
        public string SourceName()
        {
            return SourceName(source);
        }
        public string TypeName()
        {
            return TypeName(type);
        }
        public bool Visited() { return visited; }

        public void SetType(EVENT_TYPE t) { type = t; }
        public void SetTime(int t) { time = t; }
        public void SetIFF(int t) { team = t; }
        public void SetPoints(int p) { points = p; }
        public void SetSource(EVENT_SOURCE s) { source = s; }
        public void SetLocation(Point p) { loc = p; }
        public void SetRegion(string rgn) { region = rgn; }
        public void SetTitle(string t) { title = t; }
        public void SetInformation(string t) { info = t; }
        public void SetFilename(string f) { file = f; }
        public void SetImageFile(string f) { image_file = f; }
        public void SetSceneFile(string f) { scene_file = f; }
        public void SetVisited(bool v) { visited = v; }

        // operations:
        public void Load()
        {
            if (campaign == null)
                return;

            string path = campaign.Path();
            if (!string.IsNullOrEmpty(file))
            {
                string filename = Path.Combine(path, file);
                ErrLogger.PrintLine("WARNING: CombatEvent.Load is not fully implemented.");
                // TODO. The file could be a resource or a regular file at data dir
                var infoTxt = Resources.Load(filename) as TextAsset;
                info = infoTxt.text;

                if (info.Contains("$"))
                {
                    Player player = Player.GetCurrentPlayer();
                    CombatGroup group = campaign.GetPlayerGroup();

                    if (player != null)
                    {
                        info = FormatUtil.FormatTextReplace(info, "$NAME", player.Name());
                        info = FormatUtil.FormatTextReplace(info, "$RANK", Player.RankName(player.Rank()));
                    }

                    if (group != null)
                    {
                        info = FormatUtil.FormatTextReplace(info, "$GROUP", group.GetDescription());
                    }

                    string timestr;
                    FormatUtil.FormatDayTime(out timestr, campaign.GetTime(), true);
                    info = FormatUtil.FormatTextReplace(info, "$TIME", timestr);
                }
            }

            if (type < EVENT_TYPE.CAMPAIGN_END && !string.IsNullOrEmpty(image_file))
            {
                //TODO loader->LoadBitmap(image_file, image);
            }
        }

        // utilities:
        public static EVENT_TYPE TypeFromName(string n)
        {
            for (EVENT_TYPE i = EVENT_TYPE.ATTACK; i <= EVENT_TYPE.CAMPAIGN_FAIL; i++)
                if (n == TypeName(i))
                    return i;

            return (EVENT_TYPE)(-1);
        }
        public static EVENT_SOURCE SourceFromName(string n)
        {
            for (EVENT_SOURCE i = EVENT_SOURCE.FORCOM; i <= EVENT_SOURCE.NEWS; i++)
                if (n == SourceName(i))
                    return i;

            return (EVENT_SOURCE)(-1);
        }
        public static string TypeName(EVENT_TYPE n)
        {
            switch (n)
            {
                case EVENT_TYPE.ATTACK: return "ATTACK";
                case EVENT_TYPE.DEFEND: return "DEFEND";
                case EVENT_TYPE.MOVE_TO: return "MOVE_TO";
                case EVENT_TYPE.CAPTURE: return "CAPTURE";
                case EVENT_TYPE.STRATEGY: return "STRATEGY";
                case EVENT_TYPE.STORY: return "STORY";
                case EVENT_TYPE.CAMPAIGN_START: return "CAMPAIGN_START";
                case EVENT_TYPE.CAMPAIGN_END: return "CAMPAIGN_END";
                case EVENT_TYPE.CAMPAIGN_FAIL: return "CAMPAIGN_FAIL";
            }

            return "Unknown";
        }
        public static string SourceName(EVENT_SOURCE n)
        {
            switch (n)
            {
                case EVENT_SOURCE.FORCOM: return "FORCOM";
                case EVENT_SOURCE.TACNET: return "TACNET";
                case EVENT_SOURCE.INTEL: return "SECURE";
                case EVENT_SOURCE.MAIL: return "Mail";
                case EVENT_SOURCE.NEWS: return "News";
            }

            return "Unknown";
        }

        public int CompareTo(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                throw new System.ArgumentException("Not a CombatEvent object");
            }

            CombatEvent other = (CombatEvent)obj;
            return this.time.CompareTo(other.time);
        }

        private Campaign campaign;
        private EVENT_TYPE type;
        private int time;
        private int team;
        private int points;
        private EVENT_SOURCE source;
        private bool visited;
        private Point loc;
        private string region;
        private string title;
        private string info;
        private string file;
        private string image_file;
        private string scene_file;
        private Bitmap image;
    }
}