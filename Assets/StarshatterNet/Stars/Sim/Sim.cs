﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Sim.h/Sim.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Universe and Region classes
 */
using System;
using System.Collections.Generic;
using StarshatterNet.Audio;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Simulator;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.StarSystems;
using StarshatterNet.Stars.Views;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.Simulator
{
#warning Sim class is still in development and is not recommended for production.
    public class Sim : Universe
    {

        public enum TYPES { REAL_SPACE, AIR_SPACE };

        public Sim(MotionController ctrl)
        {
            this.ctrl = ctrl; test_mode = false; grid_shown = false; dust = null;
            star_system = null; active_region = null; mission = null; netgame = null;
            start_time = 0;
            Drive.Initialize();
            Explosion.Initialize();
            FlightDeck.Initialize();
            NavLight.Initialize();
            Shot.Initialize();
            MFD.Initialize();
            Asteroid.Initialize();

            if (sim == null)
                sim = this;

            scene = new Scene();
            cam_dir = CameraDirector.GetInstance();
            cam_dir.SetCamera(scene.CameraNode);
        }

        // ~Sim();

        public static Sim GetSim() { return sim; }

        public virtual void ExecFrame(double seconds)
        {
            if (first_frame)
            {
                first_frame = false;
                netgame = NetGame.Create();
            }

            if (netgame != null)
                netgame.ExecFrame();

            if (regions.Count == 0)
            {
                active_region = null;
                rgn_queue.Clear();
                jumplist.Destroy();
                scene.Collect();
                return;
            }

            foreach (var elem in elements)
                if (!elem.IsSquadron())
                    elem.ExecFrame(seconds);

            foreach (SimRegion rgn in regions)
                if (rgn != active_region && rgn.NumShips() != 0 && !rgn_queue.Contains(rgn))
                    rgn_queue.Add(rgn);

            // execframe for one inactive sim region:
            if (rgn_queue.Count != 0)
            {
                SimRegion exec_rgn = rgn_queue[0]; rgn_queue.RemoveAt(0);
                while (exec_rgn != null && (exec_rgn.NumShips() == 0 || exec_rgn == active_region))
                    if (rgn_queue.Count != 0)
                    { exec_rgn = rgn_queue[0]; rgn_queue.RemoveAt(0); }
                    else
                        exec_rgn = null;

                if (exec_rgn != null)
                    exec_rgn.ExecFrame(seconds);
            }

            if (active_region != null)
                active_region.ExecFrame(seconds);

            ExecEvents(seconds);
            ResolveHyperList();
            ResolveSplashList();

            // GC all the dead objects:
            scene.Collect();

            if (!IsTestMode())
            {
                for (int i = elements.Count - 1; i >= 0; i--)
                {
                    Element elem = elements[i];
                    if (!elem.IsSquadron() && elem.IsFinished())
                    {
                        finished.Add(elem);
                        elements.Remove(elem);
                    }
                }
            }

            // setup music
            if (!MusicDirector.IsNoMusic())
            {
                Starshatter stars = Starshatter.GetInstance();
                if (stars != null && stars.GetGameMode() == Starshatter.MODE.PLAY_MODE)
                {
                    Ship player_ship = GetPlayerShip();
                    if (player_ship != null)
                    {
                        OP_MODE phase = player_ship.GetFlightPhase();

                        if (phase < OP_MODE.ACTIVE)
                        {
                            MusicDirector.SetMode(MusicDirector.MODES.LAUNCH);
                        }

                        else if (phase > OP_MODE.ACTIVE)
                        {
                            MusicDirector.SetMode(MusicDirector.MODES.RECOVERY);
                        }

                        else
                        {
                            if (player_ship.IsInCombat())
                            {
                                MusicDirector.SetMode(MusicDirector.MODES.COMBAT);
                            }

                            else
                            {
                                MusicDirector.SetMode(MusicDirector.MODES.FLIGHT);
                            }
                        }
                    }
                }
            }
        }

        public void LoadMission(Mission msn, bool preload_textures = false)
        {
            cam_dir = CameraDirector.GetInstance();

            if (mission == null)
            {
                mission = msn;
                mission.SetActive(true);

                if (preload_textures)
                {
                    Video video = Game.GetVideo();
                    List<Model> all_models = new List<Model>();
                    //List<Bitmap>   all_textures;

                    foreach (MissionElement elem in mission.GetElements())
                    {
                        ShipDesign design = elem.GetDesign();

                        if (design != null)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                List<Model> models = design.models[i];

                                foreach (Model model in models)
                                {
                                    if (!all_models.Contains(model))
                                    {
                                        all_models.Add(model);
                                        //model->GetAllTextures(all_textures);
#if TODO
                                        foreach (Surface surface in model.GetSurfaces())
                                        {
                                            video.PreloadSurface(surface);
                                        }
#endif
                                    }
                                }
                            }
                        }
                    }

                    /*
                if (video && all_textures.size() > 0) {
                    ::Print("Preloading %d textures into video texture cache\n", all_textures.size());
                    ListIter<Bitmap> bmp_iter = all_textures;
                    while (++bmp_iter) {
                    Bitmap* bmp = bmp_iter.value();
                    video->PreloadTexture(bmp);
                    }
                }
                */
                }
            }
        }
        public void ExecMission()
        {
            cam_dir = CameraDirector.GetInstance();

            if (mission == null)
            {
                ErrLogger.PrintLine("Sim.ExecMission() - No mission to execute.");
                return;
            }

            if (elements.Count != 0 || finished.Count != 0)
            {
                ErrLogger.PrintLine("Sim.ExecMission({0}) mission is already executing.", mission.Name());
                return;
            }
            ErrLogger.PrintLine();
            ErrLogger.PrintLine("Exec Mission: '{0}'", (string)mission.Name());

            if (cam_dir != null)
                cam_dir.Reset();

            if (mission.Stardate() > 0)
                StarSystem.SetBaseTime(mission.Stardate(), true);

            star_system = mission.GetStarSystem();
            star_system.Activate(scene);

            int dust_factor = 0;

            if (Starshatter.GetInstance() != null)
                dust_factor = Starshatter.GetInstance().Dust();

            if (star_system.NumDust() * dust_factor != 0)
            {
                dust = new Dust(star_system.NumDust() * 2 * (dust_factor + 1), dust_factor > 1);
                scene.AddGraphic(dust);
            }

            CreateRegions();
            BuildLinks();
            CreateElements();
            CopyEvents();

            if (netgame != null)
            {
                // delete netgame;
                netgame = null;
            }

            first_frame = true;
            start_time = (uint)Game.GameTime();

            AudioConfig.SetTraining(mission.Type() == Mission.TYPE.TRAINING);
        }
        public void CommitMission()
        {
            for (int i = 0; i < regions.Count; i++)
                regions[i].CommitMission();

            if (ShipStats.NumStats() > 0)
            {
                ErrLogger.PrintLine(); ErrLogger.PrintLine();
                ErrLogger.PrintLine("FINAL SCORE '{0}'", (string)mission.Name());
                ErrLogger.PrintLine("Name              Kill1  Kill2  Died   Colls  Points  Cmd Pts");
                ErrLogger.PrintLine("----------------  -----  -----  -----  -----  ------  ------");

                int tk1 = 0;
                int tk2 = 0;
                int td = 0;
                int tc = 0;

                for (int i = 0; i < ShipStats.NumStats(); i++)
                {
                    ShipStats s = ShipStats.GetStats(i);
                    s.Summarize();

                    ErrLogger.PrintLine("{0:-16}  {1:5}  {1:5}  {1:5}  %5d  {1:6}  {1:6}",
                    s.GetName(),
                    s.GetGunKills(),
                    s.GetMissileKills(),
                    s.GetDeaths(),
                    s.GetColls(),
                    s.GetPoints(),
                    s.GetCommandPoints());

                    tk1 += s.GetGunKills();
                    tk2 += s.GetMissileKills();
                    td += s.GetDeaths();
                    tc += s.GetColls();

                    CombatGroup group = s.GetCombatGroup();

                    if (group != null)
                    {
                        Combatant c = group.GetCombatant();

                        if (c != null)
                            c.AddScore(s.GetPoints());

                        if (s.GetElementIndex() == 1)
                            group.SetSorties(group.Sorties() + 1);

                        group.SetKills(group.Kills() + s.GetGunKills() + s.GetMissileKills());
                        group.SetPoints(group.Points() + s.GetPoints());
                    }

                    if (s.IsPlayer())
                    {
                        Player p = Player.GetCurrentPlayer();
                        p.ProcessStats(s, start_time);

                        if (mission != null && mission.Type() == Mission.TYPE.TRAINING &&
                                s.GetDeaths() == 0 && s.GetColls() == 0)
                            p.SetTrained(mission.Identity());

                        Player.Save(); // save training state right now before we forget!
                    }
                }

                ErrLogger.PrintLine("--------------------------------------------");
                ErrLogger.PrintLine("TOTAL             {0:5}  {1:5}  {2:5}  {3:5}", tk1, tk2, td, tc);
                ErrLogger.PrintLine();

                ShipStats.Initialize();
            }
        }

        public void UnloadMission()
        {
            if (netgame != null)
            {
                // delete netgame;
                netgame = null;
            }

            HUDView hud = HUDView.GetInstance();
            if (hud != null)
                hud.HideAll();

            ShipStats.Initialize();

            events.Destroy();
            mission_elements.Destroy();
            elements.Destroy();
            finished.Destroy();

            if (active_region != null)
                active_region.Deactivate();

            if (star_system != null)
                star_system.Deactivate();

            if (mission != null)
            {
                mission.SetActive(false);
                mission.SetComplete(true);
            }

            regions.Destroy();
            scene.Collect();

            //GRAPHIC_DESTROY(dust);
            if (dust != null) { dust.Dispose(false); dust = null; }

            star_system = null;
            active_region = null;
            mission = null;

            // reclaim memory used by radio traffic:
            RadioTraffic.DiscardMessages();

            // release texture memory for 2D screens:
            Starshatter stars = Starshatter.GetInstance();
            if (stars != null)
                stars.InvalidateTextureCache();

            cam_dir = CameraDirector.GetInstance();
            if (cam_dir != null)
                cam_dir.SetShip(null);

            AudioConfig.SetTraining(false);
        }

        public void NextView()
        {
            if (active_region != null)
                active_region.NextView();
        }

        public void ShowGrid(bool show = true)
        {
            Player player = Player.GetCurrentPlayer();

            if (player != null && player.GridMode() == 0)
            {
                show = false;
                grid_shown = false;
            }

            foreach (SimRegion rgn in regions)
            {
                rgn.ShowGrid(show);
            }

            grid_shown = show ? true : false;
        }

        public bool GridShown()
        {
            return grid_shown;
        }


        public string FindAvailCallsign(int IFF)
        {
            string call = "Unidentified";

            for (int i = 0; i < 32; i++)
            {
                call = Callsign.GetCallsign(IFF);

                if (FindElement(call) == null)
                    break;
            }

            return call;
        }

        public Element CreateElement(string callsign, int IFF, Mission.TYPE type = 0/*PATROL*/)
        {
            Element elem = new Element(callsign, IFF, type);
            elements.Add(elem);
            return elem;
        }


        public void DestroyElement(Element elem)
        {
            if (elements.Contains(elem))
                elements.Remove(elem);

            // delete elem;
        }

        public Ship CreateShip(string name, string reg_num, ShipDesign design, string rgn_name, Point loc, int IFF = 0, int cmd_ai = 0, int[] loadout = null)
        {
            if (design == null)
            {
                ErrLogger.PrintLine("WARNING: CreateShip({0}): invalid design", name);
                return null;
            }

            SimRegion rgn = FindRegion(rgn_name);

            if (rgn == null)
            {
                return null;
            }

            Ship ship = new Ship(name, reg_num, design, IFF, cmd_ai, loadout);
            ship.MoveTo(loc.OtherHand());

            if (rgn != null)
            {
                ErrLogger.PrintLine("Inserting Ship({0}) into Region({1}) ({2})", ship.Name(), rgn.Name(), FormatGameTime());
                rgn.InsertObject(ship);

                if (ship.IsAirborne() && ship.AltitudeAGL() > 25)
                    ship.SetVelocity(ship.Heading() * 250);
            }

            return ship;
        }

        public Ship FindShip(string name, string rgn_name = null)
        {
            Ship ship = null;

            if (rgn_name != null)
            {
                SimRegion rgn = FindRegion(rgn_name);
                if (rgn != null)
                    ship = rgn.FindShip(name);
            }

            if (ship == null)
            {
                foreach (SimRegion rgn in regions)
                {
                    ship = rgn.FindShip(name);
                    if (ship != null) break;
                }
            }

            return ship;
        }
        public Shot CreateShot(Point pos, CameraNode shot_cam, WeaponDesign design, Ship ship = null, SimRegion rgn = null)
        {
            Shot shot = null;

            if (design.drone)
                shot = new Drone(pos, shot_cam, design, ship);
            else
                shot = new Shot(pos, shot_cam, design, ship);

            if (rgn != null)
                rgn.InsertObject(shot);

            else if (active_region != null)
                active_region.InsertObject(shot);

            return shot;
        }

        public Explosion CreateExplosion(Point pos, Point vel, Explosion.ExplosionType type, float exp_scale, float part_scale, SimRegion rgn = null, SimObject source = null, ShipSystem sys = null)
        {
            // don't bother creating explosions that can't be seen:
            if (rgn == null || active_region == null || rgn != active_region)
                return null;

            Explosion exp = new Explosion(type, pos, vel, exp_scale, part_scale, rgn, source);

            if (rgn != null)
                rgn.InsertObject(exp);

            else if (active_region != null)
                active_region.InsertObject(exp);

            return exp;
        }


        public Debris CreateDebris(Point pos, Point vel, Model model, double mass, SimRegion rgn = null)
        {
            Debris debris = new Debris(model, pos, vel, mass);

            if (rgn != null)
                rgn.InsertObject(debris);

            else if (active_region != null)
                active_region.InsertObject(debris);

            return debris;
        }


        public Asteroid CreateAsteroid(Point pos, int t, double mass, SimRegion rgn = null)
        {
            Asteroid asteroid = new Asteroid(t, pos, mass);

            if (rgn != null)
                rgn.InsertObject(asteroid);

            else if (active_region != null)
                active_region.InsertObject(asteroid);

            return asteroid;
        }

        public void CreateSplashDamage(Ship ship)
        {
            if (ship != null && ship.GetRegion() != null && ship.Design().splash_radius > 1)
            {
                SimSplash splash = new SimSplash(ship.GetRegion(),
                                                ship.Location(),
                                                ship.Design().integrity / 4,
                                                ship.Design().splash_radius);

                splash.owner_name = ship.Name();
                splashlist.Add(splash);
            }
        }

        public void CreateSplashDamage(Shot shot)
        {
            if (shot != null && shot.GetRegion() != null)
            {
                double damage = shot.Damage();
                if (damage < shot.Design().damage)
                    damage = shot.Design().damage;

                SimSplash splash = new SimSplash(shot.GetRegion(),
                                                shot.Location(),
                                                damage,
                                                shot.Design().lethal_radius);

                if (shot.Owner() != null)
                    splash.owner_name = shot.Owner().Name();

                splash.missile = shot.IsMissile();

                splashlist.Add(splash);
                CreateExplosion(shot.Location(), new Point(), Explosion.ExplosionType.SHOT_BLAST, 20.0f, 1.0f, shot.GetRegion());
            }
        }

        public void DestroyShip(Ship ship)
        {
            SimRegion rgn = ship.GetRegion();
            if (rgn != null)
                rgn.DestroyShip(ship);
        }

        public void NetDockShip(Ship ship, Ship carrier, FlightDeck deck)
        {
            SimRegion rgn = ship.GetRegion();
            if (rgn != null)
                rgn.NetDockShip(ship, carrier, deck);
        }


        public virtual Ship FindShipByObjID(uint objid)
        {
            Ship ship = null;

            foreach (SimRegion rgn in regions)
            {
                ship = rgn.FindShipByObjID(objid);
                if (ship != null) break;
            }
            return ship;
        }

        public virtual Shot FindShotByObjID(uint objid)
        {
            Shot shot = null;

            foreach (SimRegion rgn in regions)
            {
                shot = rgn.FindShotByObjID(objid);
                if (shot != null) break;
            }
            return shot;
        }


        public Mission GetMission() { return mission; }
        public List<MissionEvent> GetEvents() { return events; }
        public List<SimRegion> GetRegions() { return regions; }
        public SimRegion FindRegion(string name)
        {
            foreach (SimRegion rgn in regions)
                if (rgn.name == name)
                    return rgn;

            return null;
        }


        public SimRegion FindRegion(OrbitalRegion orgn)
        {
            foreach (SimRegion rgn in regions)
                if (rgn.orbital_region == orgn)
                    return rgn;

            return null;
        }

        public SimRegion FindNearestSpaceRegion(SimObject obj)
        {
            return FindNearestRegion(obj, TYPES.REAL_SPACE);
        }

        public SimRegion FindNearestSpaceRegion(Orbital body)
        {
            SimRegion result = null;

            if (body == null)
                return result;

            foreach (SimRegion rgn in regions)
            {
                if (result != null) return result;
                if (rgn.IsOrbital())
                {
                    OrbitalRegion orgn = rgn.GetOrbitalRegion();
                    if (orgn != null)
                    {
                        foreach (OrbitalRegion iter in body.Regions())
                        {
                            if (iter == orgn)
                                result = rgn;
                        }
                    }
                }
            }

            return result;
        }
        public SimRegion FindNearestTerrainRegion(SimObject obj)
        {
            return FindNearestRegion(obj, TYPES.AIR_SPACE);
        }

        public SimRegion FindNearestRegion(SimObject obj, TYPES type)
        {
            if (obj == null) return null;

            SimRegion result = null;
            double distance = 1.0e40;
            Point objloc = obj.Location();

            objloc = objloc.OtherHand();

            if (obj.GetRegion() != null)
                objloc += obj.GetRegion().Location();

            foreach (SimRegion rgn in regions)
            {
                if ((int)rgn.Type() == (int)type)
                {
                    OrbitalRegion orgn = rgn.GetOrbitalRegion();
                    if (orgn != null)
                    {
                        double test = Math.Abs((orgn.Location() - objloc).Length);
                        if (test < distance)
                        {
                            result = rgn;
                            distance = test;
                        }
                    }
                }
            }

            return result;
        }

        public bool ActivateRegion(SimRegion rgn)
        {
            if (rgn != null && active_region != rgn && regions.Contains(rgn))
            {
                if (active_region != null)
                    active_region.Deactivate();

                if (active_region == null || active_region.System() != rgn.System())
                {
                    if (active_region != null)
                        active_region.System().Deactivate();
                    rgn.System().Activate(scene);
                }

                active_region = rgn;
                star_system = active_region.System();

                if (star_system != null)
                {
                    star_system.SetActiveRegion(active_region.orbital_region);
                }
                else
                {
                    ErrLogger.PrintLine("WARNING: Sim.ActivateRegion() No star system found for rgn '{0}'", rgn.Name());
                }

                active_region.Activate();
                return true;
            }

            return false;
        }

        public void RequestHyperJump(Ship obj, SimRegion rgn, Point loc, TRAN_TYPE type = 0, Ship fc_src = null, Ship fc_dst = null)
        {
            bool hyperdrive = false;

            if (obj.GetQuantumDrive() != null && (QuantumDrive.SUBTYPE)obj.GetQuantumDrive().Subtype() == QuantumDrive.SUBTYPE.HYPER)
                hyperdrive = true;

            jumplist.Add(new SimHyper(obj, rgn, loc, type, hyperdrive, fc_src, fc_dst));
        }



        public SimRegion GetActiveRegion() { return active_region; }
        public StarSystem GetStarSystem() { return star_system; }
        private static List<StarSystem> dummy_system_list = new List<StarSystem>();
        public List<StarSystem> GetSystemList()
        {
            if (mission != null)
                return mission.GetSystemList();

            return dummy_system_list;
        }

        public Scene GetScene() { return scene; }
        public Ship GetPlayerShip()
        {
            if (active_region != null)
                return active_region.GetPlayerShip();

            Starshatter stars = Starshatter.GetInstance();
            if (stars != null && stars.InCutscene())
            {
                Ship player = null;

                foreach (SimRegion rgn in regions)
                {
                    if (player != null) return player;
                    player = rgn.GetPlayerShip();
                }

                return player;
            }

            return null;
        }


        public Element GetPlayerElement()
        {
            Element elem = null;

            for (int i = 0; i < elements.Count; i++)
            {
                Element e = elements[i];

                if (e.Player() > 0)
                    elem = e;
            }

            return elem;
        }

        public Orbital FindOrbitalBody(string name)
        {
            Orbital body = null;

            if (mission != null)
            {
                foreach (StarSystem iter in mission.GetSystemList())
                {
                    if (body != null) return body;
                    StarSystem sys = iter;
                    body = sys.FindOrbital(name);
                }
            }

            return body;
        }


        public void SetSelection(Ship newsel)
        {
            if (active_region != null)
                active_region.SetSelection(newsel);
        }

        public bool IsSelected(Ship s)
        {
            if (active_region != null)
                return active_region.IsSelected(s);

            return false;
        }

        private static List<Ship> empty = new List<Ship>();
        public List<Ship> GetSelection()
        {
            if (active_region != null)
                return active_region.GetSelection();

            return empty;
        }

        public void ClearSelection()
        {
            if (active_region != null)
                active_region.ClearSelection();
        }

        public void AddSelection(Ship s)
        {
            if (active_region != null)
                active_region.AddSelection(s);
        }


        public void SetTestMode(bool t = true)
        {
            test_mode = t;
            Ship pship = GetPlayerShip();

            if (pship != null)
                if (IsTestMode())
                    pship.SetControls(null);
                else
                    pship.SetControls(ctrl);
        }


        public bool IsTestMode() { return test_mode; }
        public bool IsNetGame() { return netgame != null; }
        public bool IsActive()
        {
            return mission != null && mission.IsActive();
        }

        public bool IsComplete()
        {
            return mission != null && mission.IsComplete();
        }


        public MotionController GetControls() { return ctrl; }

        public Element FindElement(string name)
        {
            foreach (Element elem in elements)
            {
                string ename = elem.Name();

                if (ename == name)
                    return elem;
            }

            return null;
        }

        public List<Element> GetElements() { return elements; }

        public int GetAssignedElements(Element elem, List<Element> assigned)
        {
            assigned.Clear();

            if (elem != null)
            {
                for (int i = 0; i < elements.Count; i++)
                {
                    Element e = elements[i];
                    if (!e.IsSquadron() && e.GetAssignment() == elem)
                        assigned.Add(e);
                }
            }

            return assigned.Count;
        }

        public void SkipCutscene()
        {
            Starshatter stars = Starshatter.GetInstance();
            if (stars != null && stars.InCutscene())
            {
                bool end = false;
                double end_time = 0;

                foreach (MissionEvent evnt in events)
                {
                    if (end) break;

                    if (evnt.IsPending() || evnt.IsActive())
                    {
                        if (evnt.Event() == MissionEvent.EVENT_TYPE.END_SCENE ||
                                evnt.Event() == MissionEvent.EVENT_TYPE.END_MISSION)
                        {
                            end = true;
                            end_time = evnt.Time();
                        }

                        if (evnt.Event() == MissionEvent.EVENT_TYPE.FIRE_WEAPON)
                        {
                            evnt.Skip();
                        }

                        else
                        {
                            evnt.Activate();
                            evnt.Execute(true);
                        }
                    }
                }

                double skip_time = end_time - MissionClock();
                if (skip_time > 0)
                {
                    SimulationTime.SkipGameTime(skip_time);
                }
            }
        }
        public void ResolveTimeSkip(double seconds)
        {
            double skipped = 0;

            // allow elements to process hold time, and release as needed:
            foreach (Element elem in elements)
                elem.ExecFrame(seconds);

            // step through the skip, ten seconds at a time:
            if (active_region != null)
            {
                double total_skip = seconds;
                double frame_skip = 10;
                Ship player = GetPlayerShip();

                while (total_skip > frame_skip)
                {
                    if (active_region.CanTimeSkip())
                    {
                        active_region.ResolveTimeSkip(frame_skip);
                        total_skip -= frame_skip;
                        skipped += frame_skip;
                    }
                    // break out early if player runs into bad guys...
                    else
                    {
                        total_skip = 0;
                    }
                }

                if (total_skip > 0)
                    active_region.ResolveTimeSkip(total_skip);
                skipped += total_skip;
            }

            // give player control after time skip:
            Ship player_ship = GetPlayerShip();
            if (player_ship != null)
            {
                player_ship.SetAutoNav(false);
                player_ship.SetThrottle(75);

                HUDView hud = HUDView.GetInstance();
                if (hud != null)
                    hud.SetHUDMode(HUDView.HUDModes.HUD_MODE_TAC);

                if (IsTestMode())
                    player_ship.SetControls(null);
            }

            SimulationTime.SkipGameTime(skipped);
            CameraDirector.SetCameraMode(CameraDirector.CAM_MODE.MODE_COCKPIT);
        }

        public void ResolveHyperList()
        {
            // resolve the hyper space transitions:
            if (jumplist.Count != 0)
            {
                Ship pship = GetPlayerShip();

                foreach (SimHyper jump in jumplist)
                {
                    Ship jumpship = jump.ship;

                    if (jumpship != null)
                    {
                        SimRegion dest = jump.rgn;

                        if (dest == null)
                            dest = FindNearestSpaceRegion(jumpship);

                        if (dest != null)
                        {
                            // bring along fighters on deck:
                            foreach (FlightDeck deck in jumpship.FlightDecks())
                            {
                                for (int i = 0; i < deck.NumSlots(); i++)
                                {
                                    Ship s = deck.GetShip(i);

                                    if (s != null)
                                    {
                                        dest.InsertObject(s);
                                        s.ClearTrack();
                                    }
                                }
                            }

                            if (jump.type == 0 && !jump.hyperdrive)
                            {
                                // bring along nearby ships:
                                // have to do it in two parts, because inserting the ships
                                // into the destination corrupts the iter over the current
                                // region's list of ships...

                                // part one: gather the ships that will be jumping:
                                List<Ship> riders = new List<Ship>();
                                foreach (Ship neighbor in jumpship.GetRegion().Ships())
                                {
                                    if (neighbor.IsDropship())
                                    {
                                        Ship s = neighbor;
                                        if (s == jumpship) continue;

                                        Point delta = s.Location() - jumpship.Location();

                                        if (delta.Length < 5e3)
                                        {
                                            riders.Add(s);
                                        }
                                    }
                                }

                                // part two: now transfer the list to the destination:
                                for (int i = 0; i < riders.Count; i++)
                                {
                                    Ship s = riders[i];
                                    Point delta = s.Location() - jumpship.Location();
                                    dest.InsertObject(s);
                                    s.MoveTo(jump.loc.OtherHand() + delta);
                                    s.ClearTrack();

                                    if (jump.fc_dst != null)
                                    {
                                        double r = jump.fc_dst.Roll();
                                        double p = jump.fc_dst.Pitch();
                                        double w = jump.fc_dst.Yaw();

                                        s.SetAbsoluteOrientation(r, p, w);
                                        s.SetVelocity(jump.fc_dst.Heading() * 500);
                                    }

                                    ProcessEventTrigger(MissionEvent.EVENT_TRIGGER.TRIGGER_JUMP, 0, s.Name());
                                }
                            }

                            // now it is safe to move the main jump ship:
                            dest.InsertObject(jumpship);
                            jumpship.MoveTo(jump.loc.OtherHand());
                            jumpship.ClearTrack();

                            ProcessEventTrigger(MissionEvent.EVENT_TRIGGER.TRIGGER_JUMP, 0, jumpship.Name());
                            NetUtil.SendObjHyper(jumpship, dest.Name(), jump.loc, jump.fc_src, jump.fc_dst, jump.type);

                            // if using farcaster:
                            if (jump.fc_src != null)
                            {
                                ErrLogger.PrintLine("Ship '{0}' farcast to '{1}'", jumpship.Name(), dest.Name());
                                CreateExplosion(jumpship.Location(), new Point(0, 0, 0), Explosion.ExplosionType.QUANTUM_FLASH, 1.0f, 0, dest);

                                if (jump.fc_dst != null)
                                {
                                    double r = jump.fc_dst.Roll();
                                    double p = jump.fc_dst.Pitch();
                                    double w = jump.fc_dst.Yaw();

                                    jumpship.SetAbsoluteOrientation(r, p, w);
                                    jumpship.SetVelocity(jump.fc_dst.Heading() * 500);
                                }

                                jumpship.SetHelmHeading(jumpship.CompassHeading());
                                jumpship.SetHelmPitch(0);
                            }

                            // break orbit:
                            else if (jump.type == TRAN_TYPE.TRANSITION_DROP_ORBIT)
                            {
                                ErrLogger.PrintLine("Ship '{0}' broke orbit to '{1}'", jumpship.Name(), dest.Name());
                                jumpship.SetAbsoluteOrientation(0, Math.PI / 4, 0);
                                jumpship.SetVelocity(jumpship.Heading() * 1.0e3f);
                            }

                            // make orbit:
                            else if (jump.type == TRAN_TYPE.TRANSITION_MAKE_ORBIT)
                            {
                                ErrLogger.PrintLine("Ship '{0}' achieved orbit '{1}'", jumpship.Name(), dest.Name());
                                jumpship.LookAt(new Point(0, 0, 0));
                                jumpship.SetVelocity(jumpship.Heading() * 500.0f);
                            }

                            // hyper jump:
                            else
                            {
                                ErrLogger.PrintLine("Ship '{0}' quantum to '{1}'", jumpship.Name(), dest.Name());

                                if (jump.hyperdrive)
                                    CreateExplosion(jumpship.Location(), new Point(0, 0, 0), Explosion.ExplosionType.HYPER_FLASH, 1, 1, dest);
                                else
                                    CreateExplosion(jumpship.Location(), new Point(0, 0, 0), Explosion.ExplosionType.QUANTUM_FLASH, 1, 0, dest);

                                jumpship.LookAt(new Point(0, 0, 0));
                                jumpship.SetVelocity(jumpship.Heading() * 500.0f);
                                jumpship.SetHelmHeading(jumpship.CompassHeading());
                                jumpship.SetHelmPitch(0);
                            }
                        }

                        else if (regions.Count > 1)
                        {
                            ErrLogger.PrintLine("Warning: Unusual jump request for ship '{0}'", jumpship.Name());
                            regions[1].InsertObject(jumpship);
                        }

                        Sensor sensor = jumpship.GetSensor();
                        if (sensor != null)
                            sensor.ClearAllContacts();
                    }
                }

                jumplist.Destroy();

                if (pship != null && pship.GetRegion() != null)
                {
                    if (active_region != pship.GetRegion())
                    {
                        pship.GetRegion().SetPlayerShip(pship);
                    }
                }
            }
        }
        public void ResolveSplashList()
        {
            if (splashlist.Count != 0)
            {
                foreach (SimSplash splash in splashlist)
                {

                    if (splash.rgn == null)
                        continue;

                    // damage ships:
                    foreach (Ship ship in splash.rgn.Ships())
                    {
                        double distance = (ship.Location() - splash.loc).Length;

                        if (distance > 1 && distance < splash.range)
                        {
                            double damage = splash.damage * (1 - distance / splash.range);
                            if (!NetGame.IsNetGameClient())
                            {
                                ship.InflictDamage(damage);
                            }

                            bool ship_destroyed = (!ship.InTransition() && ship.Integrity() < 1.0f);

                            // then // delete the ship:
                            if (ship_destroyed)
                            {
                                NetUtil.SendObjKill(ship, null, NetObjKill.KillType.KILL_MISC);
                                ErrLogger.PrintLine("    {0} Killed {1} ({2})", (string)splash.owner_name, ship.Name(), FormatGameTime());

                                // record the kill
                                ShipStats killer = ShipStats.Find(splash.owner_name);
                                if (killer != null)
                                {
                                    if (splash.missile)
                                        killer.AddEvent(SimEvent.EVENT.MISSILE_KILL, ship.Name());
                                    else
                                        killer.AddEvent(SimEvent.EVENT.GUNS_KILL, ship.Name());
                                }

                                Ship owner = FindShip(splash.owner_name, splash.rgn.Name());
                                if (owner != null && owner.GetIFF() != ship.GetIFF())
                                {
                                    if (ship.GetIFF() > 0 || owner.GetIFF() > 1)
                                    {
                                        killer.AddPoints(ship.Value());

                                        Element elem = owner.GetElement();
                                        if (elem != null)
                                        {
                                            if (owner.GetElementIndex() > 1)
                                            {
                                                Ship s = elem.GetShip(1);

                                                if (s != null)
                                                {
                                                    ShipStats cmdr_stats = ShipStats.Find(s.Name());
                                                    if (cmdr_stats != null)
                                                    {
                                                        cmdr_stats.AddCommandPoints(ship.Value() / 2);
                                                    }
                                                }
                                            }

                                            Element cmdr = elem.GetCommander();
                                            if (cmdr != null)
                                            {
                                                Ship s = cmdr.GetShip(1);

                                                if (s != null)
                                                {
                                                    ShipStats cmdr_stats = ShipStats.Find(s.Name());
                                                    if (cmdr_stats != null)
                                                    {
                                                        cmdr_stats.AddCommandPoints(ship.Value() / 2);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                ShipStats killee = ShipStats.Find(ship.Name());
                                if (killee != null)
                                    killee.AddEvent(SimEvent.EVENT.DESTROYED, splash.owner_name);

                                ship.DeathSpiral();
                            }
                        }
                    }

                    // damage drones:
                    foreach (Drone drone in splash.rgn.Drones())
                    {
                        double distance = (drone.Location() - splash.loc).Length;

                        if (distance > 1 && distance < splash.range)
                        {
                            double damage = splash.damage * (1 - distance / splash.range);
                            drone.InflictDamage(damage);

                            bool destroyed = (drone.Integrity() < 1.0f);

                            // then mark the drone for deletion:
                            if (destroyed)
                            {
                                NetUtil.SendWepDestroy(drone);
                                sim.CreateExplosion(drone.Location(), drone.Velocity(), (Explosion.ExplosionType)21 /* was LARGE_EXP */, 1.0f, 1.0f, splash.rgn);
                                drone.SetLife(0);
                            }
                        }
                    }
                }

                splashlist.Destroy();
            }
        }

        public void ExecEvents(double seconds)
        {
            foreach (MissionEvent evnt in events)
            {
                evnt.ExecFrame(seconds);
            }
        }

        public void ProcessEventTrigger(MissionEvent.EVENT_TRIGGER type, int event_id = 0, string ship = null, int param = 0)
        {
            string ship_name = ship;

            foreach (MissionEvent evnt in events)
            {
                if (evnt.IsPending() && evnt.Trigger() == type)
                {
                    switch (type)
                    {
                        case MissionEvent.EVENT_TRIGGER.TRIGGER_DAMAGE:
                        case MissionEvent.EVENT_TRIGGER.TRIGGER_DESTROYED:
                        case MissionEvent.EVENT_TRIGGER.TRIGGER_JUMP:
                        case MissionEvent.EVENT_TRIGGER.TRIGGER_LAUNCH:
                        case MissionEvent.EVENT_TRIGGER.TRIGGER_DOCK:
                        case MissionEvent.EVENT_TRIGGER.TRIGGER_TARGET:
                            if (evnt.TriggerParam() <= param)
                            {
                                if (ship_name.StartsWith(evnt.TriggerShip()))
                                    evnt.Activate();
                            }
                            break;

                        case MissionEvent.EVENT_TRIGGER.TRIGGER_NAVPT:
                            if (evnt.TriggerParam() == param)
                            {
                                if (ship_name.StartsWith(evnt.TriggerShip()))
                                    evnt.Activate();
                            }
                            break;

                        case MissionEvent.EVENT_TRIGGER.TRIGGER_EVENT:
                        case MissionEvent.EVENT_TRIGGER.TRIGGER_SKIPPED:
                            if (evnt.TriggerParam() == event_id)
                                evnt.Activate();
                            break;
                    }
                }
            }
        }
        public double MissionClock()
        {
            return (Game.GameTime() - start_time) / 1000.0;
        }

        public uint StartTime() { return start_time; }

        // Create a list of mission elements based on the current
        // state of the simulation.  Used for multiplayer join in progress.
        public IEnumerable<MissionElement> GetMissionElements()
        {
            mission_elements.Clear();

            foreach (Element elem in elements)
            {
                int num_live_ships = 0;

                for (int i = 0; i < elem.NumShips(); i++)
                {
                    Ship s = elem.GetShip(i + 1);

                    if (s != null && !s.IsDying() && !s.IsDead())
                        num_live_ships++;
                }

                if (elem.IsSquadron() || num_live_ships > 0)
                {
                    MissionElement msn_elem = CreateMissionElement(elem);

                    if (msn_elem != null)
                        mission_elements.Add(msn_elem);
                }
            }

            return mission_elements;
        }


        protected void CreateRegions()
        {
            string active_region_name = null;

            if (mission != null)
                active_region_name = mission.GetRegion();
            else return;

            foreach (StarSystem sys in mission.GetSystemList())
            {
                // insert objects from star system:
                foreach (OrbitalBody star in sys.Bodies())
                {
                    foreach (OrbitalBody planet in star.Satellites())
                    {
                        foreach (OrbitalBody moon in planet.Satellites())
                        {
                            foreach (OrbitalRegion rgn in moon.Regions())
                            {
                                SimRegion sim_region = new SimRegion(this, rgn);
                                regions.Add(sim_region);
                                if (active_region_name == sim_region.Name())
                                {
                                    ActivateRegion(sim_region);
                                }
                            }
                        }

                        foreach (OrbitalRegion rgn in planet.Regions())
                        {
                            SimRegion sim_region = new SimRegion(this, rgn);
                            regions.Add(sim_region);
                            if (active_region_name == sim_region.Name())
                            {
                                ActivateRegion(sim_region);
                            }
                        }
                    }

                    foreach (OrbitalRegion rgn in star.Regions())
                    {
                        SimRegion sim_region = new SimRegion(this, rgn);
                        regions.Add(sim_region);
                        if (active_region_name == sim_region.Name())
                        {
                            ActivateRegion(sim_region);
                        }
                    }
                }
            }
        }
        protected void CreateElements()
        {
            foreach (MissionElement msn_elem in mission.GetElements())
            {
                // add element to a carrier?
                if (msn_elem.IsSquadron())
                {
                    Ship carrier = FindShip(msn_elem.Carrier());
                    if (carrier != null)
                    {
                        Hangar hangar = carrier.GetHangar();

                        if (hangar != null)
                        {
                            int[] def_load = null;

                            if (msn_elem.Loadouts().Count != 0)
                            {
                                MissionLoad m = msn_elem.Loadouts()[0];

                                if (m.GetName().Length != 0)
                                {
                                    ShipDesign dsn = (ShipDesign)msn_elem.GetDesign();
                                    foreach (ShipLoad sl in dsn.loadouts)
                                    {
                                        if (m.GetName() == sl.name)
                                            def_load = sl.load;
                                    }
                                }

                                if (def_load == null)
                                {
                                    def_load = m.GetStations();
                                }
                            }

                            hangar.CreateSquadron(msn_elem.Name(), msn_elem.GetCombatGroup(),
                            msn_elem.GetDesign(), msn_elem.Count(),
                            msn_elem.GetIFF(),
                            def_load, msn_elem.MaintCount(), msn_elem.DeadCount());

                            Element element = CreateElement(msn_elem.Name(),
                            msn_elem.GetIFF(),
                            msn_elem.MissionRole());

                            element.SetCarrier(carrier);
                            element.SetCombatGroup(msn_elem.GetCombatGroup());
                            element.SetCombatUnit(msn_elem.GetCombatUnit());
                            element.SetCount(msn_elem.Count());
                            element.SetRogue(false);
                            element.SetPlayable(false);
                            element.SetLoadout(def_load);
                        }
                    }
                }

                // create the element in space:
                else
                {
                    Ship carrier = null;
                    Hangar hangar = null;
                    int squadron = -1;
                    int slot = 0;

                    // first create the package element:
                    Element element = CreateElement(msn_elem.Name(),
                    msn_elem.GetIFF(),
                    msn_elem.MissionRole());

                    element.SetPlayer(msn_elem.Player());
                    element.SetCombatGroup(msn_elem.GetCombatGroup());
                    element.SetCombatUnit(msn_elem.GetCombatUnit());
                    element.SetCommandAILevel(msn_elem.CommandAI());
                    element.SetHoldTime(msn_elem.HoldTime());
                    element.SetZoneLock(msn_elem.ZoneLock());
                    element.SetRogue(msn_elem.IsRogue());
                    element.SetPlayable(msn_elem.IsPlayable());
                    element.SetIntelLevel(msn_elem.IntelLevel());

                    // if this is the player's element, make sure to activate the region:
                    if (msn_elem.Player() != 0)
                    {
                        SimRegion rgn = FindRegion(msn_elem.Region());

                        if (rgn != null && rgn != active_region)
                            ActivateRegion(rgn);
                    }

                    // if element belongs to a squadron, 
                    // find the carrier, squadron, flight deck, etc.:
                    if (msn_elem.Squadron().Length > 0)
                    {
                        MissionElement squadron_elem = mission.FindElement(msn_elem.Squadron());

                        if (squadron_elem != null)
                        {
                            element.SetSquadron(msn_elem.Squadron());

                            Element cmdr = FindElement(squadron_elem.Carrier());

                            if (cmdr != null)
                            {
                                element.SetCommander(cmdr);
                                carrier = cmdr.GetShip(1);

                                if (carrier != null)
                                {
                                    element.SetCarrier(carrier);
                                    hangar = carrier.GetHangar();

                                    for (int s = 0; s < hangar.NumSquadrons(); s++)
                                    {
                                        if (hangar.SquadronName(s) == msn_elem.Squadron())
                                        {
                                            squadron = s;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    else if (msn_elem.Commander().Length > 0)
                    {
                        Element cmdr = FindElement(msn_elem.Commander());

                        if (cmdr != null)
                        {
                            element.SetCommander(cmdr);
                        }
                    }

                    foreach (Instruction o in msn_elem.Objectives())
                    {
                        Instruction instr = null;

                        instr = new Instruction(o);

                        element.AddObjective(instr);
                    }

                    if (msn_elem.Instructions().Count > 0)
                    {
                        foreach (var instr in msn_elem.Instructions())
                        {
                            element.AddInstruction(instr);
                        }
                    }

                    foreach (Instruction nav in msn_elem.NavList())
                    {
                        SimRegion rgn = FindRegion(nav.RegionName());

                        if (rgn == null)
                            rgn = FindRegion(msn_elem.Region());

                        if (rgn != null)
                        {
                            Instruction npt = new Instruction(rgn, nav.Location(), (Instruction.ACTION)(nav.Action()));

                            npt.SetStatus(nav.Status());
                            npt.SetEMCON(nav.EMCON());
                            npt.SetFormation(nav.Formation());
                            npt.SetSpeed(nav.Speed());
                            npt.SetTarget(nav.TargetName());
                            npt.SetHoldTime(nav.HoldTime());
                            npt.SetFarcast(nav.Farcast());

                            element.AddNavPoint(npt);
                        }
                    }

                    bool alertPrep = false;
                    int[] loadout = null;
                    int respawns = msn_elem.RespawnCount();

                    // if ships are to start on alert,
                    // spot them onto the appropriate launch deck:
                    if (hangar != null && element != null && msn_elem.Count() > 0 && msn_elem.IsAlert())
                    {
                        FlightDeck deck = null;
                        int queue = 1000;
                        ShipDesign dsn = msn_elem.GetDesign();

                        if (dsn != null)
                        {
                            for (int i = 0; i < carrier.NumFlightDecks(); i++)
                            {
                                FlightDeck d = carrier.GetFlightDeck(i);
                                int dq = hangar.PreflightQueue(d);

                                if (d != null && d.IsLaunchDeck() && d.SpaceLeft(dsn.type) != 0 && dq < queue)
                                {
                                    queue = dq;
                                    deck = d;
                                }
                            }
                        }

                        if (deck != null)
                        {
                            alertPrep = true;

                            // choose best loadout:
                            if (msn_elem.Loadouts().Count != 0)
                            {
                                MissionLoad l = msn_elem.Loadouts()[0];
                                if (l.GetName().Length != 0)
                                {
                                    foreach (ShipLoad sl in ((ShipDesign)dsn).loadouts)
                                    {
                                        if (sl.name == l.GetName())
                                            loadout = sl.load;
                                    }
                                }

                                else
                                {
                                    loadout = l.GetStations();
                                }
                            }

                            element.SetLoadout(loadout);

                            for (int i = 0; i < msn_elem.Count(); i++)
                            {
                                int squadron2 = -1;
                                int slot2 = -1;

                                if (hangar.FindAvailSlot(msn_elem.GetDesign(), ref squadron2, ref slot2))
                                {
                                    alertPrep = alertPrep &&
                                    hangar.GotoAlert(squadron2,
                                                    slot2,
                                                    deck,
                                                    element,
                                                    loadout,
                                                    true,    // package for launch
                                                    true);   // expedite

                                    HangarSlot s = (HangarSlot)hangar.GetSlot(squadron2, slot2);
                                    Ship alertShip = hangar.GetShip(s);

                                    if (alertShip != null)
                                    {
                                        alertShip.SetRespawnCount(respawns);

                                        if (msn_elem.Player() == i + 1)
                                        {
                                            if (alertShip.GetRegion() != null)
                                            {
                                                alertShip.GetRegion().SetPlayerShip(alertShip);
                                            }
                                            else
                                            {
                                                ErrLogger.PrintLine("WARNING: alert ship '{0}' region is null", alertShip.Name());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!alertPrep)
                    {
                        // then, create the ships:
                        for (int i = 0; i < msn_elem.Count(); i++)
                        {
                            MissionShip msn_ship = null;
                            string sname = msn_elem.GetShipName(i);
                            string rnum = msn_elem.GetRegistry(i);
                            string rgn_name = msn_elem.Region();

                            if (msn_elem.Ships().Count > i)
                            {
                                msn_ship = msn_elem.Ships()[i];
                                sname = msn_ship.Name();
                                rnum = msn_ship.RegNum();
                                rgn_name = msn_ship.Region();
                            }

                            Point l2 = msn_elem.Location();

                            if (msn_ship != null && Math.Abs(msn_ship.Location().X) < 1e9)
                            {
                                l2 = msn_ship.Location();
                            }
                            else if (i != 0)
                            {
                                Point offset = RandomHelper.RandomPoint();
                                offset.Z = (float)RandomHelper.Random(-1e3, 1e3);

                                if (msn_elem.Count() < 5)
                                    offset *= 0.3f;

                                l2 += offset;
                            }

                            // choose best loadout:
                            foreach (MissionLoad l in msn_elem.Loadouts())
                            {
                                if ((l.GetShip() == i) || (l.GetShip() < 0 && loadout == null))
                                {
                                    if (l.GetName().Length != 0)
                                    {
                                        foreach (ShipLoad sl in ((ShipDesign)msn_elem.GetDesign()).loadouts)
                                        {
                                            if (sl.name == l.GetName())
                                                loadout = sl.load;
                                        }
                                    }

                                    else
                                    {
                                        loadout = l.GetStations();
                                    }
                                }
                            }

                            element.SetLoadout(loadout);

                            Ship ship = CreateShip(sname, rnum,
                                                    (ShipDesign)msn_elem.GetDesign(),
                                                    rgn_name, l2,
                                                    msn_elem.GetIFF(),
                                                    msn_elem.CommandAI(),
                                                    loadout);

                            if (ship != null)
                            {
                                double heading = msn_elem.Heading();
                                Skin skin = msn_elem.GetSkin();

                                if (msn_ship != null)
                                {
                                    heading = msn_ship.Heading();

                                    if (msn_ship.GetSkin() != null)
                                        skin = msn_ship.GetSkin();
                                }

                                ship.SetRogue(msn_elem.IsRogue());
                                ship.SetInvulnerable(msn_elem.IsInvulnerable());
                                ship.SetHeading(0, 0, heading + Math.PI);
                                ship.SetRespawnCount(respawns);
                                ship.UseSkin(skin);

                                if (netgame == null)
                                    ship.SetRespawnLoc(RandomHelper.RandomPoint() * 2);

                                if (ship.IsStarship())
                                    ship.SetHelmHeading(heading);

                                else if (ship.IsAirborne() && ship.AltitudeAGL() > 25)
                                    ship.SetVelocity(ship.Heading() * 250);

                                if (element != null)
                                    element.AddShip(ship);

                                if (hangar != null)
                                    hangar.FindSlot(ship, ref squadron, ref slot, Hangar.HANGAR_STATE.ACTIVE);

                                if (ship.GetRegion() != null && msn_elem.Player() == i + 1)
                                    ship.GetRegion().SetPlayerShip(ship);

                                if (ship.NumFlightDecks() != 0)
                                {
                                    for (int j = 0; j < ship.NumFlightDecks(); j++)
                                    {
                                        FlightDeck deck = ship.GetFlightDeck(j);
                                        if (deck != null)
                                            deck.Orient(ship);
                                    }
                                }

                                if (msn_ship != null)
                                {
                                    ship.SetVelocity(msn_ship.Velocity().OtherHand());
                                    ship.SetIntegrity((float)msn_ship.Integrity());
                                    ship.SetRespawnCount(msn_ship.Respawns());

                                    if (msn_ship.Ammo()[0] > -10)
                                    {
                                        for (int j = 0; j < 64; j++)
                                        {
                                            Weapon w = ship.GetWeaponByIndex(j + 1);
                                            if (w != null)
                                                w.SetAmmo(msn_ship.Ammo()[j]);
                                            else
                                                break;
                                        }
                                    }

                                    if (msn_ship.Fuel()[0] > -10)
                                    {
                                        for (int j = 0; j < 4; j++)
                                        {
                                            if (ship.Reactors().Count > j)
                                            {
                                                PowerSource p = ship.Reactors()[j];
                                                p.SetCapacity(msn_ship.Fuel()[j]);
                                            }
                                        }
                                    }

                                    if (msn_ship.Decoys() > -10)
                                    {
                                        Weapon w = ship.GetDecoy();
                                        if (w != null)
                                            w.SetAmmo(msn_ship.Decoys());
                                    }

                                    if (msn_ship.Probes() > -10)
                                    {
                                        Weapon w = ship.GetProbeLauncher();
                                        if (w != null)
                                            w.SetAmmo(msn_ship.Probes());
                                    }
                                }

                                Shield shield = ship.GetShield();

                                if (shield != null)
                                {
                                    shield.SetPowerLevel(50);
                                }

                                if (ship.Class() > CLASSIFICATION.FRIGATE)
                                {
                                    foreach (WeaponGroup weapon in ship.Weapons())
                                    {
                                        // anti-air weapon?
                                        if ((weapon.GetDesign().target_type & CLASSIFICATION.DRONE) != 0)
                                        {
                                            weapon.SetFiringOrders(Weapon.Orders.POINT_DEFENSE);
                                        }
                                        else
                                        {
                                            weapon.SetFiringOrders(Weapon.Orders.MANUAL);
                                        }
                                    }
                                }

                                if (ship.Class() > CLASSIFICATION.DRONE && ship.Class() < CLASSIFICATION.STATION)
                                {
                                    ShipStats stats = ShipStats.Find(sname);
                                    if (stats != null)
                                    {
                                        string design = string.Format("{0} {1}", ship.Abbreviation(), ship.Design().display_name);
                                        stats.SetType(design);
                                        stats.SetShipClass(ship.Class());
                                        stats.SetRole(Mission.RoleName(msn_elem.MissionRole()));
                                        stats.SetIFF(ship.GetIFF());
                                        stats.SetRegion(msn_elem.Region());
                                        stats.SetCombatGroup(msn_elem.GetCombatGroup());
                                        stats.SetCombatUnit(msn_elem.GetCombatUnit());
                                        stats.SetPlayer(msn_elem.Player() == i + 1);
                                        stats.SetElementIndex(ship.GetElementIndex());
                                    }
                                }
                            }  // ship
                        }     // count
                    }
                }
            }
        }

        protected void CopyEvents()
        {
            events.Destroy();

            if (mission != null)
            {
                foreach (MissionEvent orig in mission.GetEvents())
                {
                    MissionEvent evnt = (MissionEvent)orig.Clone();
                    events.Add(evnt);
                }
            }
        }

        protected void BuildLinks()
        {
            foreach (SimRegion rgn in regions)
            {
                OrbitalRegion orb = rgn.GetOrbitalRegion();

                if (orb != null)
                {
                    foreach (string t in orb.Links())
                    {
                        SimRegion tgt = FindRegion(t);

                        if (tgt != null && !rgn.Links().Contains(tgt))
                            rgn.Links().Add(tgt);
                    }
                }
            }
        }

        // Convert a single live element into a mission element
        // that can be serialized over the net.
        protected MissionElement CreateMissionElement(Element elem)
        {
            MissionElement msn_elem = null;

            if (elem.IsSquadron())
            {
                if (elem.GetCarrier() == null || elem.GetCarrier().Integrity() < 1)
                    return msn_elem;
            }

            if (elem != null && !elem.IsNetObserver())
            {
                msn_elem = new MissionElement();

                msn_elem.SetName(elem.Name());
                msn_elem.SetIFF(elem.GetIFF());
                msn_elem.SetMissionRole(elem.Type());

                if (elem.IsSquadron() && elem.GetCarrier() != null)
                {
                    Ship carrier = elem.GetCarrier();

                    msn_elem.SetCarrier(carrier.Name());
                    msn_elem.SetCount(elem.GetCount());
                    msn_elem.SetLocation(carrier.Location().OtherHand());

                    if (carrier.GetRegion() != null)
                        msn_elem.SetRegion(carrier.GetRegion().Name());

                    int squadron_index = 0;
                    Hangar hangar = FindSquadron(elem.Name(), out squadron_index);

                    if (hangar != null)
                    {
                        msn_elem.SetDeadCount(hangar.NumShipsDead(squadron_index));
                        msn_elem.SetMaintCount(hangar.NumShipsMaint(squadron_index));

                        ShipDesign design = hangar.SquadronDesign(squadron_index);
                        msn_elem.SetDesign(design);

                        string design_path = design.path_name;
                        //design_path.setSensitive(false);

                        if (design_path.StartsWith("/Mods/Ships"))
                        {
                            design_path = design_path.Substring(11, 1000);
                            msn_elem.SetPath(design_path);
                        }
                    }
                }

                else
                {
                    msn_elem.SetSquadron(elem.GetSquadron());
                    msn_elem.SetCount(elem.NumShips());
                }

                if (elem.GetCommander() != null)
                    msn_elem.SetCommander(elem.GetCommander().Name());

                msn_elem.SetCombatGroup(elem.GetCombatGroup());
                msn_elem.SetCombatUnit(elem.GetCombatUnit());

                Ship ship = elem.GetShip(1);
                if (ship != null)
                {
                    if (ship.GetRegion() != null)
                        msn_elem.SetRegion(ship.GetRegion().Name());

                    msn_elem.SetLocation(ship.Location().OtherHand());
                    msn_elem.SetDesign(ship.Design());

                    msn_elem.SetPlayer(elem.Player());
                    msn_elem.SetCommandAI(elem.GetCommandAILevel());
                    msn_elem.SetHoldTime((int)elem.GetHoldTime());
                    msn_elem.SetZoneLock(elem.GetZoneLock());
                    msn_elem.SetHeading(ship.CompassHeading());

                    msn_elem.SetPlayable(elem.IsPlayable());
                    msn_elem.SetRogue(elem.IsRogue());
                    msn_elem.SetIntelLevel(elem.IntelLevel());

                    string design_path = ship.Design().path_name;
                    //design_path.setSensitive(false);

                    if (design_path.StartsWith("/Mods/Ships"))
                    {
                        design_path = design_path.Substring(11, 1000);
                        msn_elem.SetPath(design_path);
                    }

                    msn_elem.SetRespawnCount(ship.RespawnCount());
                }

                MissionLoad loadout = new MissionLoad();
                //CopyMemory(loadout.GetStations(), elem.Loadout(), 16 * sizeof(int));
                Array.Copy(elem.Loadout(), loadout.GetStations(), 16);
                msn_elem.Loadouts().Add(loadout);

                int num_obj = elem.NumObjectives();
                for (int i = 0; i < num_obj; i++)
                {
                    Instruction o = elem.GetObjective(i);
                    Instruction instr = null;

                    instr = new Instruction(o);

                    msn_elem.AddObjective(instr);
                }

                int num_inst = elem.NumInstructions();
                for (int i = 0; i < num_inst; i++)
                {
                    string instr = elem.GetInstruction(i);
                    msn_elem.AddInstruction(instr);
                }

                foreach (Instruction nav in elem.GetFlightPlan())
                {
                    Instruction npt = new Instruction(nav.RegionName(), nav.Location(), (Instruction.ACTION)nav.Action());

                    npt.SetFormation(nav.Formation());
                    npt.SetSpeed(nav.Speed());
                    npt.SetTarget(nav.TargetName());
                    npt.SetHoldTime(nav.HoldTime());
                    npt.SetFarcast(nav.Farcast());
                    npt.SetStatus(nav.Status());

                    msn_elem.AddNavPoint(npt);
                }

                for (int i = 0; i < elem.NumShips(); i++)
                {
                    ship = elem.GetShip(i + 1);

                    if (ship != null)
                    {
                        MissionShip s = new MissionShip();

                        s.SetName(ship.Name());
                        s.SetRegNum(ship.Registry());
                        s.SetRegion(ship.GetRegion().Name());
                        s.SetLocation(ship.Location().OtherHand());
                        s.SetVelocity(ship.Velocity().OtherHand());

                        s.SetRespawns(ship.RespawnCount());
                        s.SetHeading(ship.CompassHeading());
                        s.SetIntegrity(ship.Integrity());

                        if (ship.GetDecoy() != null)
                            s.SetDecoys(ship.GetDecoy().Ammo());

                        if (ship.GetProbeLauncher() != null)
                            s.SetProbes(ship.GetProbeLauncher().Ammo());

                        int n;
                        int[] ammo = new int[16];
                        int[] fuel = new int[4];

                        for (n = 0; n < 16; n++)
                        {
                            Weapon w = ship.GetWeaponByIndex(n + 1);

                            if (w != null)
                                ammo[n] = w.Ammo();
                            else
                                ammo[n] = -10;
                        }

                        for (n = 0; n < 4; n++)
                        {
                            if (ship.Reactors().Count > n)
                                fuel[n] = ship.Reactors()[n].Charge();
                            else
                                fuel[n] = -10;
                        }

                        s.SetAmmo(ammo);
                        s.SetFuel(fuel);

                        msn_elem.Ships().Add(s);
                    }
                }
            }

            return msn_elem;
        }
        protected Hangar FindSquadron(string name, out int index)
        {
            index = -1;
            Hangar hangar = null;

            foreach (SimRegion rgn in regions)
            {
                foreach (Ship carrier in rgn.Carriers())
                {
                    Hangar h = carrier.GetHangar();

                    for (int i = 0; i < h.NumSquadrons() && hangar == null; i++)
                    {
                        if (h.SquadronName(i) == name)
                        {
                            hangar = h;
                            if (hangar != null) return hangar;
                            index = i;
                        }
                    }
                }
            }

            return hangar;
        }

        public static string FormatGameTime()
        {
            string txt;

            uint t = (uint)Game.GameTime();

            uint h = (t / 3600000);
            uint m = ((t - h * 3600000) / 60000);
            uint s = ((t - h * 3600000 - m * 60000) / 1000);
            uint e = (t - h * 3600000 - m * 60000 - s * 1000);

            if (h > 0)
                txt = string.Format("%02d:%02d:%02d.%03d", h, m, s, e);
            else
                txt = string.Format("%02d:%02d.%03d", m, s, e);

            return txt;
        }

        protected static Sim sim;
        protected SimRegion active_region;
        protected StarSystem star_system;
        public Scene scene;
        public Dust dust;
        protected CameraDirector cam_dir;

        protected List<SimRegion> regions = new List<SimRegion>();
        protected List<SimRegion> rgn_queue = new List<SimRegion>();
        protected List<SimHyper> jumplist = new List<SimHyper>();
        protected List<SimSplash> splashlist = new List<SimSplash>();
        protected List<Element> elements = new List<Element>();
        protected List<Element> finished = new List<Element>();
        protected List<MissionEvent> events = new List<MissionEvent>();
        protected List<MissionElement> mission_elements = new List<MissionElement>();

        public MotionController ctrl;

        protected bool test_mode;
        protected bool grid_shown;
        protected Mission mission;

        public NetGame netgame;
        protected uint start_time;
        protected static bool first_frame = true;
    }
    public class SimHyper
    {

        public SimHyper(Ship o, SimRegion r, Point l, TRAN_TYPE t, bool h, Ship fc1, Ship fc2)
        {
            ship = o; rgn = r; loc = l; type = t; hyperdrive = h; fc_src = fc1; fc_dst = fc2;
        }

        public Ship ship;
        public SimRegion rgn;
        public Point loc;
        public TRAN_TYPE type;
        public bool hyperdrive;
        public Ship fc_src;
        public Ship fc_dst;
    }

    // +--------------------------------------------------------------------+

    public class SimSplash
    {
        public SimSplash(SimRegion r, Point l, double d, double n)
        {
            rgn = r; loc = l; damage = d; range = n;
            owner_name = "Collateral Damage"; missile = false;
        }

        public string owner_name;
        public Point loc;
        public double damage;
        public double range;
        public SimRegion rgn;
        public bool missile;
    }
}
