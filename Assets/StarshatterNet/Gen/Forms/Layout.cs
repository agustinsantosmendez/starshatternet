﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      Layout.h/Layout.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Layout Manager class for ActiveWindow panels
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Stars;
using DWORD = System.Int32;

namespace StarshatterNet.Gen.Forms
{
    public class Layout
    {
        public Layout() { }
        //public virtual ~Layout();

        public virtual bool DoLayout(ActiveWindow panel)
        {
            if (panel == null || panel.GetChildren().Count < 1)
                return false;

            if (cols.Count < 1 || rows.Count < 1)
                return false;

            List<DWORD> cell_x = new List<DWORD>();
            List<DWORD> cell_y = new List<DWORD>();

            ScaleWeights();
            CalcCells((DWORD)panel.Width(), (DWORD)panel.Height(), cell_x, cell_y);

            foreach (ActiveWindow w in panel.GetChildren())
            {
                Rect c = w.GetCells();
                Rect r = new Rect();
                Rect rp = panel.GetRect();

                if (c.x < 0) c.x = 0;
                else if (c.x >= (int)cell_x.Count) c.x = cell_x.Count - 1;
                if (c.y < 0) c.y = 0;
                else if (c.y >= (int)cell_y.Count) c.y = cell_y.Count - 1;
                if (c.x + c.w >= (int)cell_x.Count) c.w = cell_x.Count - c.x - 1;
                if (c.y + c.h >= (int)cell_y.Count) c.h = cell_y.Count - c.y - 1;

                r.x = (int)(cell_x[c.x] + w.GetCellInsets().left);
                r.y = (int)(cell_y[c.y] + w.GetCellInsets().top);
                r.w = (int)(cell_x[c.x + c.w] - w.GetCellInsets().right - r.x);
                r.h = (int)(cell_y[c.y + c.h] - w.GetCellInsets().bottom - r.y);

                r.x += panel.X();
                r.y += panel.Y();

                if (w.GetFixedWidth() != 0 && w.GetFixedWidth() < r.w)
                    r.w = w.GetFixedWidth();

                if (w.GetFixedHeight() != 0 && w.GetFixedHeight() < r.h)
                    r.h = w.GetFixedHeight();

                if (w.GetID() == 330 || w.GetID() == 125)
                {
                    int y1 = r.y + r.h;
                    int y2 = rp.y + rp.h;
                }

                if (w.GetHidePartial() && (r.x + r.w > rp.x + rp.w))
                {
                    w.MoveTo(new Rect(0, 0, 0, 0));
                }

                else if (w.GetHidePartial() && (r.y + r.h > rp.y + rp.h))
                {
                    w.MoveTo(new Rect(0, 0, 0, 0));
                }

                else
                {
                    w.MoveTo(r);
                }
            }

            return true;
        }

        public virtual void Clear()
        {
            cols.Clear();
            rows.Clear();

            col_weights.Clear();
            row_weights.Clear();
        }
        public virtual void AddCol(DWORD min_width, float col_factor)
        {
            cols.Add(min_width);
            col_weights.Add(col_factor);
        }

        public virtual void AddRow(DWORD min_height, float row_factor)
        {
            rows.Add(min_height);
            row_weights.Add(row_factor);
        }

        public virtual void SetConstraints(List<DWORD> min_x, List<DWORD> min_y, List<float> weight_x, List<float> weight_y)
        {
            Clear();

            if (min_x.Count == weight_x.Count && min_y.Count == weight_y.Count)
            {
                foreach (var iter in min_x)
                    cols.Add(iter);

                foreach (var iter in min_y)
                    rows.Add(iter);

                foreach (var iter in weight_x)
                    col_weights.Add(iter);

                foreach (var iter in weight_y)
                    row_weights.Add(iter);
            }
        }

        public virtual void SetConstraints(List<float> min_x, List<float> min_y, List<float> weight_x, List<float> weight_y)
        {
            Clear();

            if (min_x.Count == weight_x.Count &&
                    min_y.Count == weight_y.Count)
            {
                foreach (var iter in min_x)
                    cols.Add((DWORD)iter);

                foreach (var iter in min_y)
                    rows.Add((DWORD)iter);

                foreach (var iter in weight_x)
                    col_weights.Add(iter);

                foreach (var iter in weight_y)
                    row_weights.Add(iter);
            }
        }

        public virtual void SetConstraints(int ncols, int nrows, List<DWORD> min_x, List<DWORD> min_y, List<float> weight_x, List<float> weight_y)
        {
            Clear();

            if (nrows > 0 && ncols > 0)
            {
                int i = 0;

                for (i = 0; i < ncols; i++)
                {
                    cols.Add(min_x[i]);
                    col_weights.Add(weight_x[i]);
                }

                for (i = 0; i < nrows; i++)
                {
                    rows.Add(min_y[i]);
                    row_weights.Add(weight_y[i]);
                }
            }
        }




        protected virtual void ScaleWeights()
        {
            float total = 0;

            foreach (var cwi in col_weights)
                total += cwi;

            if (total > 0)
            {
                for (int i = 0; i < col_weights.Count; i++)
                    col_weights[i] = col_weights[i] / total;
            }

            total = 0;
            foreach (var rwi in row_weights)
                total += rwi;

            if (total > 0)
            {
                for (int i = 0; i < row_weights.Count; i++)
                    row_weights[i] = row_weights[i] / total;
            }
        }
        protected virtual void CalcCells(DWORD w, DWORD h, List<DWORD> cell_x, List<DWORD> cell_y)
        {
            DWORD x = 0;
            DWORD y = 0;
            DWORD min_x = 0;
            DWORD min_y = 0;
            DWORD ext_x = 0;
            DWORD ext_y = 0;

            foreach (var cit in cols)
                min_x += cit;

            foreach (var rit in rows)
                min_y += rit;

            if (min_x < w)
                ext_x = w - min_x;

            if (min_y < h)
                ext_y = h - min_y;

            cell_x.Add(x);
            for (int i = 0; i < cols.Count; i++)
            {
                var cit = cols[i];
                x += cit + (DWORD)(ext_x * col_weights[i]);
                cell_x.Add(x);
            }

            cell_y.Add(y);
            for (int i = 0; i < rows.Count; i++)
            {
                var rit = rows[i];
                y += rit + (DWORD)(ext_y * row_weights[i]);
                cell_y.Add(y);
            }
        }
        public bool IsEmpty()
        {
            return cols.Count == 0 && rows.Count == 0 && col_weights.Count == 0 && row_weights.Count == 0;
        }
        protected List<DWORD> cols = new List<DWORD>();
        protected List<DWORD> rows = new List<DWORD>();
        protected List<float> col_weights = new List<float>();
        protected List<float> row_weights = new List<float>();
    }
}
