﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmpFileDlg.h/CmpFileDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Select Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmpFileDlg : CmdDlg
    {
        public CmpFileDlg(Screen s, Camera c, FormDef def, CmpnScreen mgr, GameObject p) :
             base(s, mgr, 0, 0, s.Width(), s.Height(), c, p, "CmpFileDlg")
        {
            exit_latch = false; btn_save = null; btn_cancel = null; edt_name = null; lst_campaigns = null;

            Init(def);
        }
        //public virtual ~CmpFileDlg();
        public override void RegisterControls()
        {
            btn_save = (Button)FindControl(1);
            btn_cancel = (Button)FindControl(2);

            if (btn_save != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_save, OnSave);

            if (btn_cancel != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_cancel, OnCancel);

            edt_name = (EditBox)FindControl(200);

            if (edt_name != null)
                edt_name.SetText("");

            lst_campaigns = (ListBox)FindControl(201);

            if (lst_campaigns != null)
                REGISTER_CLIENT(ETD.EID_SELECT, lst_campaigns, OnCampaign);
        }

        public override void Show()
        {
            base.Show();

            if (lst_campaigns != null)
            {
                lst_campaigns.ClearItems();
                lst_campaigns.SetLineHeight(12);

                List<string> save_list;

                CampaignSaveGame.GetSaveGameList(out save_list);
                save_list.Sort();

                for (int i = 0; i < save_list.Count; i++)
                    lst_campaigns.AddItem(save_list[i]);

                save_list.Destroy();
            }

            if (edt_name != null)
            {
                string save_name = "";

                campaign = Campaign.GetCampaign();
                if (campaign != null && campaign.GetPlayerGroup() != null)
                {
                    string op_name = campaign.Name();
                    string day;
                    CombatGroup group = campaign.GetPlayerGroup();

                    if (op_name.Contains("Operation "))
                        op_name += 10;

                    FormatUtil.FormatDay(out day, campaign.GetTime());

                    save_name = string.Format("{0} {1} ({2})",
                       op_name,
                       day,
                       group.GetRegion());
                }

                edt_name.SetText(save_name);
                edt_name.SetFocus();
            }
        }

        // Operations:
        public override void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnSave(null, null);
            }

            if (Keyboard.KeyDown(KeyMap.VK_ESCAPE))
            {
                if (!exit_latch)
                    OnCancel(null, null);

                exit_latch = true;
            }
            else
            {
                exit_latch = false;
            }
        }

        public override void OnSave(ActiveWindow obj, AWEvent evnt)
        {
            if (edt_name != null && !string.IsNullOrEmpty(edt_name.GetText()))
            {
                campaign = Campaign.GetCampaign();
                CampaignSaveGame save = new CampaignSaveGame(campaign);

                string filename;
                filename = edt_name.GetText();
                //TODO string newline = strchr(filename, '\n');
                //TODO if (newline != null)
                //TODO     *newline = 0;

                save.Save(filename);
                save.SaveAuto();

                if (cmpn_screen != null)
                    cmpn_screen.HideCmpFileDlg();
            }
        }

        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            if (cmpn_screen != null)
                cmpn_screen.HideCmpFileDlg();
        }

        public virtual void OnCampaign(ActiveWindow obj, AWEvent evnt)
        {
            int n = lst_campaigns.GetSelection();

            if (n >= 0)
            {
                string cmpn = lst_campaigns.GetItemText(n);

                if (edt_name != null)
                    edt_name.SetText(cmpn);
            }
        }

        protected Button btn_cancel;
        protected EditBox edt_name;
        protected ListBox lst_campaigns;

        protected bool exit_latch;

    }
}