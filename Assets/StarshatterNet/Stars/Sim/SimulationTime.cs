﻿using System;
using UnityEngine;

namespace StarshatterNet.Stars.Simulator
{
    public class SimulationTime : IComparable, IComparable<SimulationTime>, IEquatable<SimulationTime>
    {
        public static bool IsPaused { get { return Instance.Paused; } }

#warning Game.GameTime() returns uint instead of DateTime
        internal static uint GameTime()
        {
            return (uint)Instance.SimTime.Ticks;
        }

#warning SimulationTime.FrameTime() returns int instead of TimeSpan
        internal static int FrameTime()
        {
            return (int)Instance.ElapseTime.Ticks;
        }

        /// <summary>
        /// The completion time in seconds since the last frame. (Read Only).
        /// </summary>
        public float DeltaTime
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// The timeScale-independent interval in seconds from the last frame 
        /// to the current one (Read Only).
        /// </summary>
        public float UnscaledDeltaTime
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// The time at the beginning of this frame (Read Only). 
        /// This is the time in seconds since the start of the game.
        /// </summary>
        public float Time 
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// The timeScale-independant time for this frame (Read Only).
        /// This is the time in seconds since the start of the game.
        /// </summary>
        public float UnscaledTime
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// The real time in seconds since the game started (Read Only).
        /// </summary>
        public float realtimeSinceStartup
        {
            get { return UnityEngine.Time.realtimeSinceStartup;  }
        }

        public static SimulationTime Instance = new SimulationTime();

        public DateTime SimTime;
        public TimeSpan ElapseTime;

        private DateTime lastFrame;
        private float acceleration;
        private float oldAcc = 1;
        private bool isPaused = false;
        private bool isRunning = false;
        private const double defaultEpoch = 0.5e9;


        public void Start()
        {
            Start(DateTime.Now);
        }
        public void Start(float seconds)
        {
            DateTime initdate = new DateTime().AddSeconds(seconds);
            Start(initdate);
        }

        public void Start(DateTime startime)
        {
            this.SimTime = startime;
            this.lastFrame = DateTime.Now;
            this.TimeScale = 1;
            this.isRunning = true;
        }

        public void Stop()
        {
            this.isRunning = false;
        }

        public bool Paused { get { return isPaused; } }

        public void Pause()
        {
            if (!isPaused)
            {
                oldAcc = this.TimeScale;
                this.acceleration = 0;
                isPaused = true;
            }
        }

        public void Resume()
        {
            if (isPaused)
            {
                this.acceleration = oldAcc;
                isPaused = false;
                this.lastFrame = DateTime.Now;
            }
        }

        public float TimeScale
        {
            get { return acceleration; }

            set
            {
                if (isPaused && value != 0)
                    Resume();
                if (!isPaused && value == 0)
                    Pause();
                this.acceleration = value;
            }

        }

        public void NextFrame()
        {
            if (isRunning && !isPaused)
            {
                DateTime newTime = DateTime.Now;
                this.ElapseTime = TimeSpan.FromTicks((long)(TimeScale * (newTime - this.lastFrame).Ticks));
                this.SimTime = this.SimTime + this.ElapseTime;
                this.lastFrame = newTime;
            }
        }

        public void NextFrame(double totalSeconds, double deltaSenconds)
        {
            if (isRunning && !isPaused)
            {
                this.lastFrame = new DateTime();
                this.lastFrame.AddSeconds(totalSeconds);

                this.ElapseTime = TimeSpan.FromSeconds(TimeScale * deltaSenconds);
                this.SimTime = this.SimTime + this.ElapseTime;
            }
        }

        public void NextFrame(TimeSpan delta)
        {
            if (isRunning && !isPaused)
            {
                this.lastFrame = DateTime.Now;
                this.ElapseTime = delta;
                this.SimTime = this.SimTime + this.ElapseTime;
            }
        }

        public void NextFrame(DateTime frameTime)
        {
            if (isRunning && !isPaused)
            {
                this.lastFrame = DateTime.Now;
                this.ElapseTime = frameTime - this.SimTime;
                this.SimTime = frameTime;
            }
        }

        internal void Reset()
        {
            throw new NotImplementedException();
        }

        //public void SkipGameTime(double seconds)
        //{
        //    if (seconds > 0)
        //        this.SimTime = this.SimTime + new TimeSpan((long)(seconds * TimeSpan.TicksPerSecond));
        //}
        public static void SkipGameTime(double skip_time)
        {
            throw new NotImplementedException();
        }

        public int CompareTo(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                throw new System.ArgumentException("Not a SimulationTime object");
            }

            SimulationTime other = (SimulationTime)obj;
            return this.CompareTo(other);
        }

        public int CompareTo(SimulationTime other)
        {
            return this.SimTime.CompareTo(other.SimTime);
        }

        public bool Equals(SimulationTime other)
        {
            return this.SimTime.Equals(other.SimTime);
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            SimulationTime other = (SimulationTime)obj;
            return this.SimTime.Equals(other.SimTime);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return this.SimTime.GetHashCode();
        }

        public static bool operator ==(SimulationTime t1, SimulationTime t2)
        {
            if (object.ReferenceEquals(t1, t2))
            {
                return true;     // if both the same object, or both null
            }
            // If one is null, but not both, return false.
            if (((object)t1 == null) || ((object)t2 == null))
            {
                return false;
            }

            return t1.SimTime == t2.SimTime;
        }

        public static bool operator !=(SimulationTime t1, SimulationTime t2)
        {
            return !(t1 == t2);
        }
        public static bool operator <(SimulationTime t1, SimulationTime t2) { return t1.SimTime < t2.SimTime; }

        public static bool operator >(SimulationTime t1, SimulationTime t2) { return t1.SimTime > t2.SimTime; }

        public static bool operator <=(SimulationTime t1, SimulationTime t2) { return t1.SimTime <= t2.SimTime; }

        public static bool operator >=(SimulationTime t1, SimulationTime t2) { return t1.SimTime >= t2.SimTime; }

        public static SimulationTime operator +(SimulationTime d, TimeSpan t)
        {
            SimulationTime rst = new SimulationTime();
            rst.SimTime = d.SimTime + t;
            return rst;
        }

        public static TimeSpan operator -(SimulationTime d1, SimulationTime d2) { return d1.SimTime - d2.SimTime; }

        public static SimulationTime operator -(SimulationTime d, TimeSpan t)
        {
            SimulationTime rst = new SimulationTime();
            rst.SimTime = d.SimTime - t;
            return rst;
        }

        public override string ToString()
        {
            return this.SimTime.ToString("yyyy'-'MM'-'dd' 'HH':'mm':'ss'.'ffffff");
        }

        internal static int GetMaxFrameLength()
        {
            throw new NotImplementedException();
        }

        public string ToString(string format)
        {
            return this.SimTime.ToString(format);
        }

    }
}
