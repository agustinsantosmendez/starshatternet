﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars.Graphics.General
{
    public class Graphic2D : Graphic
    {
        public Graphic2D()
        {
            this.name = string.Format("Graphic2DGroupObjet{0:X8}", this.Identity());
            gameObj = new GameObject(this.name);
            var rectTransform = gameObj.AddComponent<RectTransform>();

            linePrefab = (GameObject)Resources.Load("Prefabs/LineGamePrefab");
            textPrefab = (GameObject)Resources.Load("Prefabs/TextGamePrefab");
        }

        public float Width { get => width; set => width = value; }

        // Ideas and code from https://gist.github.com/nothke/82dccc121457541fcce32aea7744618a
        public void DrawScreenLines(int nlines, float[] v, Color c, Video.BLEND_TYPE blend = 0)
        {
            for (int i = 0; i < nlines; i++)
            {
                DrawLine(v[4 * i + 0], v[4 * i + 1], v[4 * i + 2], v[4 * i + 3], c);
            }
        }
        /// <summary>
        /// Draws line between 2 points.
        /// </summary>
        public void DrawLine(Vector3 p1, Vector3 p2, Color color)
        {
            Begin();
            Vertex(p1);
            Vertex(p2);
            End(color);
        }

        public void DrawLine(float x1, float y1, float x2, float y2, Color color)
        {
            Begin();
            Vertex(new Vector3(x1, y1, 0));
            Vertex(new Vector3(x2, y2, 0));
            End(color);
        }

        public void DrawLines(Point[] pts, Color color)
        {
            Begin();
            foreach (var p in pts)
                Vertex(new Vector3((float)p.X, (float)p.Y, (float)p.Z));
            End(color);
        }
        public void DrawPoly(Point[] pts, Color color)
        { throw new NotImplementedException(); }
        public void FillPoly(Point[] pts, Color color)
        { throw new NotImplementedException(); }

        public void DrawRect(float x1, float y1, float x2, float y2, Color color)
        {
            Sort(ref x1, ref x2);
            Sort(ref y1, ref y2);
            Begin();
            Vertex(new Vector3(x1, y1, 0));
            Vertex(new Vector3(x2, y1, 0));
            Vertex(new Vector3(x2, y2, 0));
            Vertex(new Vector3(x1, y2, 0));
            Vertex(new Vector3(x1, y1, 0));
            End(color);
        }

        public void DrawRect(Rect r, Color color)
        {
            DrawRect(r.x, r.y, r.x + r.w, r.y + r.h, color);
        }

        public void FillRect(float x1, float y1, float x2, float y2, Color color)
        { throw new NotImplementedException(); }

        public void FillRect(Rect r, Color color)
        { throw new NotImplementedException(); }
        public void DrawBitmap(float x1, float y1, float x2, float y2, Bitmap img)
        { throw new NotImplementedException(); }
        public void DrawEllipse(float x1, float y1, float x2, float y2, Color color)
        {
            Sort(ref x1, ref x2);
            Sort(ref y1, ref y2);

            float w2 = (x2 - x1) / 2.0f;
            float h2 = (y2 - y1) / 2.0f;
            float cx = x1 + w2;
            float cy = y1 + h2;

            DrawEllipse(new Vector2(cx, cy), w2, h2, color);
        }
        public void DrawEllipse(Vector2 center, float radX, float radY, Color color)
        {
            Begin();
            // Vertices
            for (float theta = 0.0f; theta < (2 * Mathf.PI); theta += 1f / resolution)
            {
                Vector3 ci = new Vector3(center.x + (Mathf.Cos(theta) * radX), center.y + (Mathf.Sin(theta) * radY), 0);
                Vertex(ci);

                if (theta != 0)
                    Vertex(ci);
            }
            End(color);
        }
        public void FillEllipse(float x1, float y1, float x2, float y2, Color color)
        { throw new NotImplementedException(); }
        public void DrawCircle(Vector2 center, float rad, Color color)
        {
            DrawEllipse(center, rad, rad, color);
        }
        public void DrawText(string txt, StarshatterNet.Stars.Rect r, Color color)
        {
            numTexts++;
            GameObject textObj = (GameObject)GameObject.Instantiate(textPrefab);
            textObj.transform.SetParent(gameObj.transform);
            textObj.transform.localPosition = Vector3.zero;
            textObj.name = string.Format("Text{0:X2}", numTexts);
            var textMesh = textObj.GetComponent<TextMesh>();
            textMesh.color = color;
            textMesh.text = txt;
            texts.Add(textMesh);
        }
        public void DrawString(string txt, Color color, Rect r)
        {
            numTexts++;
            GameObject textObj = (GameObject)GameObject.Instantiate(textPrefab);
            textObj.transform.SetParent(gameObj.transform);
            textObj.transform.localPosition = new Vector3(r.x, r.y, 0);
            textObj.name = string.Format("Text{0:X2}", numTexts);
            var textMesh = textObj.GetComponent<TextMesh>();
            textMesh.color = color;
            textMesh.text = txt;
            texts.Add(textMesh);
        }
        public void DrawString(string txt, int x1, int y1, Rect r)
        {
            numTexts++;
            GameObject textObj = (GameObject)GameObject.Instantiate(textPrefab);
            textObj.transform.SetParent(gameObj.transform);
            textObj.transform.localPosition = new Vector3(x1, y1, 0);
            textObj.name = string.Format("Text{0:X2}", numTexts);
            var textMesh = textObj.GetComponent<TextMesh>();
            textMesh.color = font.color;
            textMesh.text = txt;
            texts.Add(textMesh);
        }

        public void SetFont(FontItem f) { font = f; }
        public FontItem GetFont() { return font; }

        protected void Vertex(Vector3 v)
        {
            points.Add(v);
        }
        protected void Vertex(float x, float y, float z = 0)
        {
            points.Add(new Vector3(x, y, z));
        }
        protected void Begin()
        {
            points.Clear();
        }

        protected void End(Color grid_color)
        {
            numlines++;
            GameObject lineObj = (GameObject)GameObject.Instantiate(linePrefab);
            lineObj.transform.SetParent(gameObj.transform);
            lineObj.transform.localPosition = Vector3.zero;
            lineObj.name = string.Format("Line{0:X2}", numlines);
            var line = lineObj.GetComponent<LineRenderer>();
            line.positionCount = points.Count;

            for (int index = 0; index < points.Count; index++)
            {
                var p = points[index];
                line.SetPosition(index, p);
            }
            line.startWidth = width; line.endWidth = width;
            line.startColor = grid_color; line.endColor = grid_color;
            lines.Add(line);
        }
        protected static void Swap(ref float a, ref float b) { float tmp = a; a = b; b = tmp; }
        protected static void Sort(ref float a, ref float b) { if (a > b) Swap(ref a, ref b); }

        protected uint numlines = 0;
        protected uint numTexts = 0;
        private GameObject linePrefab;
        private GameObject textPrefab;

        public List<LineRenderer> lines = new List<LineRenderer>();
        public List<TextMesh> texts = new List<TextMesh>();

        private List<Vector2> points = new List<Vector2>();
        private float width = 1f;
        public int resolution = 10;
        protected FontItem font;

    }
}
