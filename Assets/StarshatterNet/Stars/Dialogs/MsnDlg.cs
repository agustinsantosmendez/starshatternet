﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MsnDlg.h/MsnDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Briefing Dialog Active Window class
*/
using System;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.Dialogs
{
    public class MsnDlg
    {

        public MsnDlg(PlanScreen mgr)
        {
            plan_screen = mgr; commit = null; cancel = null; campaign = null; mission = null;
            pkg_index = -1; info = null;

            campaign = Campaign.GetCampaign();

            if (campaign != null)
                mission = campaign.GetMission();

            mission_name = null;
            mission_system = null;
            mission_sector = null;
            mission_time_start = null;
            mission_time_target = null;
            mission_time_target_label = null;

            sit_button = null;
            pkg_button = null;
            nav_button = null;
            wep_button = null;
            commit = null;
            cancel = null;
        }
        //public virtual ~MsnDlg();

        public void RegisterMsnControls(FormWindow win)
        {
            mission_name = win.FindControl(200);
            mission_system = win.FindControl(202);
            mission_sector = win.FindControl(204);
            mission_time_start = win.FindControl(206);
            mission_time_target = win.FindControl(208);
            mission_time_target_label = win.FindControl(207);

            sit_button = (Button)win.FindControl(900);
            pkg_button = (Button)win.FindControl(901);
            nav_button = (Button)win.FindControl(902);
            wep_button = (Button)win.FindControl(903);
            commit = (Button)win.FindControl(1);
            cancel = (Button)win.FindControl(2);
        }
        public void ShowMsnDlg()
        {
            campaign = Campaign.GetCampaign();

            mission = null;
            pkg_index = -1;

            if (campaign != null)
            {
                mission = campaign.GetMission();

                if (mission == null)
                    ErrLogger.PrintLine("ERROR - MsnDlg.Show() no mission.");
                else
                    ErrLogger.PrintLine("MsnDlg.Show() mission id = {0} name = '{1}'",
                                        mission.Identity(),
                                        mission.Name());
            }
            else
            {
                ErrLogger.PrintLine("ERROR - MsnDlg.Show() no campaign.");
            }

            if (mission_name != null)
            {
                if (mission != null)
                    mission_name.SetText(mission.Name());
                else
                    mission_name.SetText(Game.GetText("MsnDlg.no-mission"));
            }

            if (mission_system != null)
            {
                mission_system.SetText("");

                if (mission != null)
                {
                    StarSystem sys = mission.GetStarSystem();

                    if (sys != null)
                        mission_system.SetText(sys.Name());
                }
            }

            if (mission_sector != null)
            {
                mission_sector.SetText("");

                if (mission != null)
                {
                    mission_sector.SetText(mission.GetRegion());
                }
            }

            if (mission_time_start != null)
            {
                if (mission != null)
                {
                    string txt;
                    FormatUtil.FormatDayTime(out txt, mission.Start());
                    mission_time_start.SetText(txt);
                }
            }

            if (mission_time_target != null)
            {
                int time_on_target = CalcTimeOnTarget();

                if (time_on_target != 0)
                {
                    string txt;
                    FormatUtil.FormatDayTime(out txt, time_on_target);
                    mission_time_target.SetText(txt);
                    mission_time_target_label.SetText(Game.GetText("MsnDlg.target"));
                }
                else
                {
                    mission_time_target.SetText("");
                    mission_time_target_label.SetText("");
                }
            }


            if (sit_button != null)
            {
                sit_button.SetButtonState(plan_screen.IsMsnObjShown() ? (short)1 : (short)0);
                sit_button.SetEnabled(true);
            }

            if (pkg_button != null)
            {
                pkg_button.SetButtonState(plan_screen.IsMsnPkgShown() ? (short)1 : (short)0);
                pkg_button.SetEnabled(true);
            }

            if (nav_button != null)
            {
                nav_button.SetButtonState(plan_screen.IsNavShown() ? (short)1 : (short)0);
                nav_button.SetEnabled(true);
            }

            if (wep_button != null)
            {
                wep_button.SetButtonState(plan_screen.IsMsnWepShown() ? (short)1 : (short)0);
                wep_button.SetEnabled(true);
            }

            bool mission_ok = true;

            if (mission == null || !mission.IsOK())
            {
                mission_ok = false;

                if (sit_button != null) sit_button.SetEnabled(false);
                if (pkg_button != null) pkg_button.SetEnabled(false);
                if (nav_button != null) nav_button.SetEnabled(false);
                if (wep_button != null) wep_button.SetEnabled(false);
            }
            else
            {
                MissionElement player_elem = mission.GetPlayer();

                if (wep_button != null && player_elem != null)
                    wep_button.SetEnabled(player_elem.Loadouts().Count > 0);
            }

            if (wep_button != null && NetLobby.GetInstance() != null)
                wep_button.SetEnabled(false);

            commit.SetEnabled(mission_ok);
            cancel.SetEnabled(true);
        }

        // Operations:
        public virtual void OnCommit(ActiveWindow obj, AWEvent evnt)
        {
            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                Mouse.Show(false);
                stars.SetGameMode(Starshatter.MODE.LOAD_MODE);
            }

            else
                ErrLogger.PrintLine("PANIC: MsnDlg.OnCommit() - Game instance not found");
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                Mouse.Show(false);

                if (campaign != null && (campaign.IsDynamic() || campaign.IsTraining()))
                    stars.SetGameMode(Starshatter.MODE.CMPN_MODE);
                else
                    stars.SetGameMode(Starshatter.MODE.MENU_MODE);
            }

            else
                ErrLogger.PrintLine("PANIC: MsnDlg.OnCancel() - Game instance not found");
        }

        public virtual void OnTabButton(ActiveWindow obj, AWEvent evnt)
        {
            if (evnt.window == sit_button)
            {
                plan_screen.ShowMsnObjDlg();
            }

            if (evnt.window == pkg_button)
            {
                plan_screen.ShowMsnPkgDlg();
            }

            if (evnt.window == nav_button)
            {
                plan_screen.ShowNavDlg();
            }

            if (evnt.window == wep_button)
            {
                plan_screen.ShowMsnWepDlg();
            }
        }



        protected virtual int CalcTimeOnTarget()
        {
            if (mission != null)
            {
                MissionElement element = mission.GetElements()[0];
                if (element != null)
                {
                    Point loc = element.Location();
                    loc.SwapYZ();  // navpts use Z for altitude, element loc uses Y for altitude.

                    int mission_time = mission.Start();

                    int i = 0;
                    foreach (Instruction navpt in element.NavList())
                    {
                        Instruction.ACTION action = navpt.Action();

                        double dist = new Point(loc - navpt.Location()).Length;

                        int etr = 0;

                        if (navpt.Speed() > 0)
                            etr = (int)(dist / navpt.Speed());
                        else
                            etr = (int)(dist / 500);

                        mission_time += etr;

                        loc = navpt.Location();
                        i++;

                        if (action >= Instruction.ACTION.ESCORT)
                        {   // this is the target!
                            return mission_time;
                        }
                    }
                }
            }

            return 0;
        }

        protected internal PlanScreen plan_screen;
        protected internal Button commit;
        protected internal Button cancel;
        protected internal Button sit_button;
        protected internal Button pkg_button;
        protected internal Button nav_button;
        protected internal Button wep_button;

        protected ActiveWindow mission_name;
        protected ActiveWindow mission_system;
        protected ActiveWindow mission_sector;
        protected ActiveWindow mission_time_start;
        protected ActiveWindow mission_time_target;
        protected ActiveWindow mission_time_target_label;

        protected internal Campaign campaign;
        protected internal Mission mission;
        protected MissionInfo info;
        protected internal int pkg_index;

    }
}