﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmdDlg.h/CmdDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Operational Command Dialog Active Window class
*/
using System;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmdDlg : FormWindow
    {
        public enum MODE
        {
            MODE_ORDERS,
            MODE_THEATER,
            MODE_FORCES,
            MODE_INTEL,
            MODE_MISSIONS,
            NUM_MODES
        };

        public CmdDlg(Screen s, CmpnScreen mgr, int ax, int ay, int aw, int ah, Camera c, GameObject p, string name) : 
            base(s, 0, 0, s.Width(), s.Height(), c, p, name)
        {
            cmpn_screen = mgr;
            txt_group = null; txt_score = null; txt_name = null; txt_time = null;
            btn_save = null; btn_exit = null; stars = null; campaign = null; mode = 0;

            stars = Starshatter.GetInstance();
            campaign = Campaign.GetCampaign();

            for (int i = 0; i < 5; i++)
                btn_mode[i] = null;
        }
        //public virtual ~CmdDlg();

        public virtual void RegisterCmdControls(FormWindow win)
        {
            btn_save = (Button)win.FindControl(1);
            btn_exit = (Button)win.FindControl(2);

            for (int i = 0; i < 5; i++)
            {
                btn_mode[i] = (Button)win.FindControl(100 + i);
            }

            txt_group = win.FindControl(200);
            txt_score = win.FindControl(201);
            txt_name = win.FindControl(300);
            txt_time = win.FindControl(301);
        }
        public virtual void ShowCmdDlg()
        {
            campaign = Campaign.GetCampaign();

            if (txt_name != null)
            {
                if (campaign != null)
                    txt_name.SetText(campaign.Name());
                else
                    txt_name.SetText("No Campaign Selected");
            }

            ShowMode();

            if (btn_save != null)
                btn_save.SetEnabled(!campaign.IsTraining());

            if (btn_mode[(int)MODE.MODE_FORCES] != null) btn_mode[(int)MODE.MODE_FORCES].SetEnabled(!campaign.IsTraining());
            if (btn_mode[(int)MODE.MODE_INTEL] != null) btn_mode[(int)MODE.MODE_INTEL].SetEnabled(!campaign.IsTraining());
        }


        public virtual void ExecFrame()
        {
            CombatGroup g = campaign.GetPlayerGroup();
            if (g != null && txt_group != null)
                txt_group.SetText(g.GetDescription());

            if (txt_score != null)
            {
                string score = string.Format("Team Score: {0}", campaign.GetPlayerTeamScore());
                txt_score.SetText(score);
                txt_score.SetTextAlign(TextFormat.DT_RIGHT);
            }

            if (txt_time != null)
            {
                double t = campaign.GetTime();
                string daytime ;
                FormatUtil.FormatDayTime(out daytime, t);
                txt_time.SetText(daytime);
            }

            int unread = campaign.CountNewEvents();

            if (btn_mode[(int)MODE.MODE_INTEL] != null)
            {
                if (unread > 0)
                {
                    string text = string.Format("INTEL ({0})", unread);
                    btn_mode[(int)MODE.MODE_INTEL].SetText(text);
                }
                else
                {
                    btn_mode[(int)MODE.MODE_INTEL].SetText("INTEL");
                }
            }
        }

        // Operations:
        public virtual void OnMode(ActiveWindow obj, AWEvent evnt)
        {
            for (MODE i = MODE.MODE_ORDERS; i < MODE.NUM_MODES; i++)
            {
                Button btn = btn_mode[(int)i];

                if (evnt.window == btn)
                {
                    mode = i;
                }
            }

            switch (mode)
            {
                case MODE.MODE_ORDERS: cmpn_screen.ShowCmdOrdersDlg(); break;
                case MODE.MODE_THEATER: cmpn_screen.ShowCmdTheaterDlg(); break;
                case MODE.MODE_FORCES: cmpn_screen.ShowCmdForceDlg(); break;
                case MODE.MODE_INTEL: cmpn_screen.ShowCmdIntelDlg(); break;
                case MODE.MODE_MISSIONS: cmpn_screen.ShowCmdMissionsDlg(); break;
                default: cmpn_screen.ShowCmdOrdersDlg(); break;
            }
        }
        public virtual void OnSave(ActiveWindow obj, AWEvent evnt)
        {
            if (campaign != null && cmpn_screen != null)
            {
                CmpFileDlg fdlg = cmpn_screen.GetCmpFileDlg();

                cmpn_screen.ShowCmpFileDlg();
            }
        }

        public virtual void OnExit(ActiveWindow obj, AWEvent evnt)
        {
            if (stars != null)
            {
                Mouse.Show(false);
                stars.SetGameMode(Starshatter.MODE.MENU_MODE);
            }
        }


        protected virtual void ShowMode()
        {
            for (int i = 0; i < 5; i++)
            {
                if (btn_mode[i] != null) btn_mode[i].SetButtonState(0);
            }

            if (mode < MODE.MODE_ORDERS || mode > MODE.MODE_MISSIONS)
                mode = MODE.MODE_ORDERS;

            if (btn_mode[(int)mode] != null) btn_mode[(int)mode].SetButtonState(1);
        }


        protected CmpnScreen cmpn_screen;

        protected ActiveWindow txt_group;
        protected ActiveWindow txt_score;
        protected ActiveWindow txt_name;
        protected ActiveWindow txt_time;
        protected Button[] btn_mode = new Button[(int)MODE.NUM_MODES];
        protected Button btn_save;
        protected Button btn_exit;

        protected Starshatter stars;
        protected Campaign campaign;

        protected MODE mode;

    }
}