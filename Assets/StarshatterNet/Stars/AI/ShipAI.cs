﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      ShipAI.h/ShipAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Common base class and interface for low-level ship AI
*/
using System;
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.AI
{
    public class ShipAI : SteerAI
    {
        public ShipAI(SimObject s) : base(s)

        {
            support = null; rumor = null; threat = null; threat_missile = null; drop_time = 0;
            too_close = 0; navpt = null; patrol = 0; engaged_ship_id = 0;
            bracket = false; identify = false; hold = false; takeoff = false;
            throttle = 0; old_throttle = 0; element_index = 1; splash_count = 0;
            tactical = null; farcaster = null; ai_level = 2; last_avoid_time = 0;
            last_call_time = 0;
            ship = (Ship)self;

            Sim sim = Sim.GetSim();
            Ship pship = sim.GetPlayerShip();
            int player_team = 1;

            if (pship != null)
                player_team = pship.GetIFF();

            Player player = Player.GetCurrentPlayer();
            if (player != null)
            {
                if (ship != null && ship.GetIFF() != 0 && ship.GetIFF() != player_team)
                {
                    ai_level = player.AILevel();
                }
                else if (player.AILevel() == 0)
                {
                    ai_level = 1;
                }
            }

            // evil alien ships are *always* smart:
            if (ship != null && ship.GetIFF() > 1 && ship.Design().auto_roll > 1)
            {
                ai_level = 2;
            }
        }

        // ~ShipAI();

        public override void ExecFrame(double secs)
        {
            seconds = secs;

            if (drop_time > 0) drop_time -= seconds;
            if (ship == null) return;

            ship.SetDirectorInfo(" ");

            // check to make sure current navpt is still valid:
            if (navpt != null)
                navpt = ship.GetNextNavPoint();

            if (ship.GetFlightPhase() == OP_MODE.TAKEOFF || ship.GetFlightPhase() == OP_MODE.LAUNCH)
                takeoff = true;

            if (takeoff)
            {
                FindObjective();
                Navigator();

                if (ship.MissionClock() > 10000)
                    takeoff = false;

                return;
            }

            // initial assessment:
            if (ship.MissionClock() < 5000)
                return;

            element_index = ship.GetElementIndex();

            NavlightControl();
            CheckTarget();

            if (tactical != null)
                tactical.ExecFrame(seconds);

            if (target != null && target != ship.GetTarget())
            {
                ship.LockTarget(target);

                // if able to lock target, and target is a ship (not a shot)...
                if (target == ship.GetTarget() && (SimObject.TYPES)target.Type() == SimObject.TYPES.SIM_SHIP)
                {

                    // if this isn't the same ship we last called out:
                    if (target.Identity() != engaged_ship_id && Game.GameTime() - last_call_time > 10000)
                    {
                        // call engaging:
                        RadioMessage msg = new RadioMessage(ship.GetElement(), ship, RadioMessage.ACTION.CALL_ENGAGING);
                        msg.AddTarget(target);
                        RadioTraffic.Transmit(msg);
                        last_call_time = (uint)Game.GameTime();

                        engaged_ship_id = target.Identity();
                    }
                }
            }

            else if (target == null)
            {
                target = ship.GetTarget();

                if (engaged_ship_id != 0 && target == null)
                {
                    engaged_ship_id = 0;

                    /***
                *** XXX 
                *** Not the right place to make this decision.
                ***
                *** There is a brief wait between killing a target and
                *** selecting a new one, so this message is sent after
                *** every kill.
                ***
                *** Need to track when the entire element has been
                *** put down.

                if (element_index == 1) {
                    RadioMessage* msg = new(__FILE__,__LINE__) RadioMessage(ship.GetElement(), ship, RadioMessage.RESUME_MISSION);
                    RadioTraffic.Transmit(msg);
                }

                ***
                ***/
                }
            }

            FindObjective();
            Navigator();
        }
        public override bool Subframe()
        {
            return true;
        }

        public virtual Ship GetShip() { return ship; }

        public virtual Ship GetWard()
        {
            return ship.GetWard();
        }


        public virtual void SetWard(Ship s)
        {
            if (ship == null)
                return;
            if (s == ship.GetWard())
                return;

            if (ship != null)
                ship.SetWard(s);

            Point form = RandomHelper.RandomDirection();
            form.SwapYZ();

            if (Math.Abs(form.X) < 0.5)
            {
                if (form.X < 0)
                    form.X = -0.5f;
                else
                    form.X = 0.5f;
            }

            if (ship != null && ship.IsStarship())
            {
                form *= 30e3f;
            }
            else
            {
                form *= 15e3f;
                form.Y = 500;
            }

            SetFormationDelta(form);
        }
        public virtual Ship GetThreat() { return threat; }
        public virtual void SetThreat(Ship s)
        {
            if (threat == s)
                return;

            threat = s;

            if (threat != null)
                Observe(threat);
        }

        public virtual Ship GetSupport() { return support; }
        public virtual void SetSupport(Ship s)
        {
            if (support == s)
                return;

            support = s;

            if (support != null)
                Observe(support);
        }

        public virtual Ship GetRumor() { return rumor; }
        public virtual void SetRumor(Ship s)
        {
            if (s == null || rumor == s)
                return;

            rumor = s;

            if (rumor != null)
                Observe(rumor);
        }

        public virtual Shot GetThreatMissile() { return threat_missile; }
        public virtual void SetThreatMissile(Shot s)
        {
            if (threat_missile == s)
                return;

            threat_missile = s;

            if (threat_missile != null)
                Observe(threat_missile);
        }

        public virtual Instruction GetNavPoint() { return navpt; }
        public virtual void SetNavPoint(Instruction n) { navpt = n; }
        public virtual Point GetPatrol()
        {
            return patrol_loc;
        }

        public virtual void SetPatrol(Point p)
        {
            patrol = 1;
            patrol_loc = p;
        }

        public virtual void ClearPatrol()
        {
            patrol = 0;
        }

        public virtual void ClearRumor()
        {
            rumor = null;
        }

        public virtual void ClearTactical()
        {
            //delete tactical;
            tactical = null;
        }


        public virtual Farcaster GetFarcaster() { return farcaster; }

        // convert the goal point from world to local coords:
        public override void FindObjective()
        {
            distance = 0;

            RadioMessage.ACTION order = (RadioMessage.ACTION)ship.GetRadioOrders().Action();

            if (order == RadioMessage.ACTION.QUANTUM_TO ||
                    order == RadioMessage.ACTION.FARCAST_TO)
            {

                FindObjectiveQuantum();
                objective = Transform(obj_w);
                return;
            }

            bool form = (order == RadioMessage.ACTION.WEP_HOLD) ||
            (order == RadioMessage.ACTION.FORM_UP) ||
            (order == RadioMessage.ACTION.MOVE_PATROL) ||
            (order == RadioMessage.ACTION.RTB) ||
            (order == RadioMessage.ACTION.DOCK_WITH) ||
            (order == 0 && target == null) ||
            (farcaster != null);

            Ship ward = ship.GetWard();

            // if not the element leader, stay in formation:
            if (form && element_index > 1)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.formation"));

                if (navpt != null && (int)navpt.Action() == (int)Instruction.ACTION.LAUNCH)
                {
                    FindObjectiveNavPoint();
                }
                else
                {
                    navpt = null;
                    FindObjectiveFormation();
                }

                // transform into camera coords:
                objective = Transform(obj_w);
                return;
            }

            // under orders?
            bool directed = false;
            if (tactical != null)
                directed = (tactical.RulesOfEngagement() == TacticalAI.ROE.DIRECTED);

            // threat processing:
            if (threat != null && !directed)
            {
                double d_threat = (threat.Location() - ship.Location()).Length;

                // seek support:
                if (support != null)
                {
                    double d_support = (support.Location() - ship.Location()).Length;
                    if (d_support > 35e3)
                    {
                        ship.SetDirectorInfo(localeManager.GetText("ai.regroup"));
                        FindObjectiveTarget(support);
                        objective = Transform(obj_w);
                        return;
                    }
                }

                // run away:
                else if (threat != target)
                {
                    ship.SetDirectorInfo(localeManager.GetText("ai.retreat"));
                    obj_w = ship.Location() + (ship.Location() - threat.Location()) * 100;
                    objective = Transform(obj_w);
                    return;
                }
            }

            // normal processing:
            if (target != null)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.seek-target"));
                FindObjectiveTarget(target);
                objective = AimTransform(obj_w);
            }

            else if (patrol != 0)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.patrol"));
                FindObjectivePatrol();
                objective = Transform(obj_w);
            }

            else if (ward != null)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.seek-ward"));
                FindObjectiveFormation();
                objective = Transform(obj_w);
            }

            else if (navpt != null && form)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.seek-navpt"));
                FindObjectiveNavPoint();
                objective = Transform(obj_w);
            }

            else if (rumor != null)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.search"));
                FindObjectiveTarget(rumor);
                objective = Transform(obj_w);
            }

            else
            {
                obj_w = new Point();
                objective = new Point();
            }
        }

        public virtual void Splash(Ship targ)
        {
            if (splash_count > 6)
                splash_count = 4;

            // call splash:
            RadioTraffic.SendQuickMessage(ship, RadioMessage.ACTION.SPLASH_1 + splash_count);
            splash_count++;
        }


        public override void SetTarget(SimObject targ, ShipSystem sub = null)
        {
            if (targ != target)
            {
                bracket = false;
            }

            base.SetTarget(targ, sub);
        }

        public override void DropTarget(double dtime = 1.5)
        {
            SetTarget(null);
            drop_time = dtime;    // seconds until we can re-acquire

            ship.DropTarget();
        }

        public virtual double DropTime() { return drop_time; }
        public virtual void SetBracket(bool b)
        {
            bracket = b;
            identify = false;
        }

        public virtual void SetIdentify(bool i)
        {
            identify = i;
            bracket = false;
        }

        public virtual void SetFormationDelta(Point point)
        {
            formation_delta = point;
        }


        public override bool Update(SimObject obj)
        {
            if (obj == support)
                support = null;

            if (obj == threat)
                threat = null;

            if (obj == threat_missile)
                threat_missile = null;

            if (obj == rumor)
                rumor = null;

            return base.Update(obj);
        }


        public override string GetObserverName()
        {
            string name = string.Format("ShipAI({0})", self.Name());
            return name;
        }


        public virtual int GetAILevel() { return ai_level; }


        // accumulate behaviors:
        protected override void Navigator()
        {
            accumulator.Clear();
            magnitude = 0;

            hold = false;
            if ((ship.GetElement() != null && ship.GetElement().GetHoldTime() > 0) ||
                    (navpt != null && navpt.Status() == Instruction.STATUS.COMPLETE && navpt.HoldTime() > 0))
                hold = true;

            ship.SetFLCSMode(FLCS_MODE.FLCS_HELM);

            if (target != null)
                ship.SetDirectorInfo(localeManager.GetText("ai.seek-target"));
            else if (rumor != null)
                ship.SetDirectorInfo(localeManager.GetText("ai.seek-rumor"));
            else
                ship.SetDirectorInfo(localeManager.GetText("ai.none"));

            Accumulate(AvoidCollision());
            Accumulate(AvoidTerrain());

            if (!hold)
                Accumulate(SeekTarget());

            HelmControl();
            ThrottleControl();
            FireControl();
            AdjustDefenses();
        }


        // behaviors:
        protected virtual bool AvoidTestSingleObject(SimObject obj,
                                                          Point bearing,
                                                        double avoid_dist,
                                                        ref double avoid_time,
                                                        ref Steer avoid)
        {
            if (too_close == obj.Identity())
            {
                double d = (ship.Location() - obj.Location()).Length;
                double closure = Point.Dot((ship.Velocity() - obj.Velocity()), bearing);

                if (closure > 1 && d < avoid_dist)
                {
                    avoid = AvoidCloseObject(obj);
                    return true;
                }
                else
                {
                    too_close = 0;
                }
            }

            // will we get close?
            double time = ClosestApproachTime(ship.Location(), ship.Velocity(),
            obj.Location(), obj.Velocity());

            // already past the obstacle:
            if (time <= 0)
            {
                if (other == obj) other = null;
                return false;
            }

            // how quickly could we collide?
            Point current_relation = ship.Location() - obj.Location();
            double current_distance = current_relation.Length - ship.Radius() - obj.Radius();

            // are we really far away?
            if (current_distance > 25e3)
            {
                if (other == obj) other = null;
            }

            // is the obstacle a farcaster?
            if ((SimObject.TYPES)obj.Type() == SimObject.TYPES.SIM_SHIP)
            {
                Ship c_ship = (Ship)obj;

                if (c_ship.GetFarcaster() != null)
                {
                    // are we on a safe vector?
                    Point dir = ship.Velocity();
                    dir.Normalize();

                    double angle_off = Math.Abs(Math.Acos(Point.Dot(dir, obj.Cam().vpn())));

                    if (angle_off > MathHelper.ToRadians(90))
                        angle_off = MathHelper.ToRadians(180) - angle_off;

                    if (angle_off < MathHelper.ToRadians(35))
                    {
                        // will we pass through the center?
                        Point d = ship.Location() + dir * (float)(current_distance + ship.Radius() + obj.Radius());
                        double err = (obj.Location() - d).Length;

                        if (err < 0.667 * obj.Radius())
                        {
                            return false;
                        }
                    }
                }
            }

            // rate of closure:
            double closing_velocity = Point.Dot((ship.Velocity() - obj.Velocity()), bearing);

            // are we too close already?
            if (current_distance < (avoid_dist * 0.35))
            {
                if (closing_velocity > 1 || current_distance < ship.Radius())
                {
                    avoid = AvoidCloseObject(obj);
                    return true;
                }
            }

            // too far away to worry about:
            double separation = (avoid_dist + obj.Radius());
            if ((current_distance - separation) / closing_velocity > avoid_time)
            {
                if (other == obj) other = null;
                return false;
            }

            // where will we be?
            Point selfpt = ship.Location() + ship.Velocity() * (float)time;
            Point testpt = obj.Location() + obj.Velocity() * (float)time;

            // how close will we get?
            double dist = (selfpt - testpt).Length - ship.Radius() - obj.Radius();

            // that's too close:
            if (dist < avoid_dist)
            {
                if (dist < avoid_dist * 0.25 && time < avoid_time * 0.5)
                {
                    avoid = AvoidCloseObject(obj);
                    return true;
                }

                obstacle = Transform(testpt);

                if (obstacle.Z > 0)
                {
                    other = obj;
                    avoid_time = time;
                    brake = 0.5;

                    Observe(other);
                }
            }

            // hysteresis:
            else if (other == obj && dist > avoid_dist * 1.25)
            {
                other = null;
            }

            return false;
        }


        protected virtual Steer AvoidCloseObject(SimObject obj)
        {
            too_close = obj.Identity();
            obstacle = Transform(obj.Location());
            other = obj;

            Observe(other);

            Steer avoid = Flee(obstacle);
            avoid.brake = 0.3;

            ship.SetDirectorInfo(localeManager.GetText("ai.avoid-collision"));
            return avoid;
        }

        protected virtual Steer AvoidCollision()
        {
            Steer avoid = new Steer();

            if (ship == null || ship.GetRegion() == null | !ship.GetRegion().IsActive())
                return avoid;

            if (other != null && (other.Life() == 0 || other.Integrity() < 1))
            {
                other = null;
                last_avoid_time = 0; // check for a new obstacle immediately
            }

            if (other == null && Game.GameTime() - last_avoid_time < 500)
                return avoid;

            brake = 0;

            // don't get closer than this:
            double avoid_dist = 5 * self.Radius();

            if (avoid_dist < 1e3) avoid_dist = 1e3;
            else if (avoid_dist > 12e3) avoid_dist = 12e3;

            // find the soonest potential collision,
            // ignore any that occur after this:
            double avoid_time = 15;

            if (ship.Design().avoid_time > 0)
                avoid_time = ship.Design().avoid_time;
            else if (ship.IsStarship())
                avoid_time *= 1.5;

            Point bearing = self.Velocity();
            bearing.Normalize();

            bool found = false;
            int num_contacts = ship.NumContacts();

            // check current obstacle first:
            if (other != null)
            {
                found = AvoidTestSingleObject(other, bearing, avoid_dist, ref avoid_time, ref avoid);
            }

            if (!found)
            {
                // avoid ships:
                foreach (var contact in ship.ContactList())
                {
                    Ship c_ship = contact.GetShip();

                    if (c_ship != null && c_ship != ship && c_ship.IsStarship())
                    {
                        found = AvoidTestSingleObject(c_ship, bearing, avoid_dist, ref avoid_time, ref avoid);
                    }
                    if (found) break;
                }

                // also avoid large pieces of debris:
                if (!found)
                {
                    foreach (Debris debris in ship.GetRegion().Rocks())
                    {
                        if (debris.Mass() > ship.Mass())
                            found = AvoidTestSingleObject(debris, bearing, avoid_dist, ref avoid_time, ref avoid);
                        if (found) break;
                    }
                }

                // and asteroids:
                if (!found)
                {
                    // give asteroids a wider berth -
                    avoid_dist *= 8;

                    foreach (Asteroid roid in ship.GetRegion().Roids())
                    {
                        found = AvoidTestSingleObject(roid, bearing, avoid_dist, ref avoid_time, ref avoid);
                        if (found) break;
                    }

                    if (!found)
                        avoid_dist /= 8;
                }

                // if found, steer to avoid:
                if (other != null)
                {
                    avoid = Avoid(obstacle, (float)(ship.Radius() + other.Radius() + avoid_dist * 0.9));
                    avoid.brake = brake;

                    ship.SetDirectorInfo(localeManager.GetText("ai.avoid-collision"));
                }
            }

            last_avoid_time = (uint)Game.GameTime();
            return avoid;
        }

        protected virtual Steer AvoidTerrain()
        {
            Steer avoid = new Steer();
            return avoid;
        }

        protected virtual Steer SeekTarget()
        {
            Ship ward = ship.GetWard();

            if (target == null && ward == null && navpt == null && patrol == 0)
            {
                if (element_index > 1)
                {
                    // wingmen keep in formation:
                    return Seek(objective);
                }

                if (farcaster != null)
                {
                    return Seek(objective);
                }

                if (rumor != null)
                {
                    return Seek(objective);
                }

                return new Steer();
            }

            if (patrol != 0)
            {
                Steer result = Seek(objective);

                if (distance < 2000)
                {
                    result.brake = 1;
                }

                return result;
            }

            if (target != null && too_close == target.Identity())
            {
                drop_time = 4;
                return Avoid(objective, 0.0f);
            }
            else if (drop_time > 0)
            {
                return new Steer();
            }

            return Seek(objective);
        }

        protected virtual Steer EvadeThreat()
        {
            return new Steer();
        }

        protected override Point ClosingVelocity()
        {
            if (ship != null && target != null)
            {
                if (ship.GetPrimaryDesign() != null)
                {
                    WeaponDesign guns = ship.GetPrimaryDesign();
                    Point delta = target.Location() - ship.Location();

                    // fighters need to aim the ship so that the guns will hit the target
                    if (guns.firing_cone < MathHelper.ToRadians(10) && guns.max_range <= delta.Length)
                    {
                        Point aim_vec = ship.Heading();
                        aim_vec.Normalize();

                        Point shot_vel = ship.Velocity() + aim_vec * guns.speed;
                        return shot_vel - target.Velocity();
                    }

                    // ships with turreted weapons just need to worry about actual closing speed
                    else
                    {
                        return ship.Velocity() - target.Velocity();
                    }
                }

                else
                {
                    return ship.Velocity();
                }
            }

            return new Point(1, 0, 0);
        }

        // compute the goal point in world coords based on current ai state:
        protected virtual void FindObjectiveTarget(SimObject tgt)
        {
            if (tgt == null)
            {
                obj_w = new Point();
                return;
            }

            navpt = null; // this tells fire control that we are chasing a target,
                          // instead of a navpoint!

            Point cv = ClosingVelocity();
            double cvl = cv.Length;
            float time = 0;

            if (cvl > 50)
            {
                // distance from self to target:
                distance = (tgt.Location() - self.Location()).Length;

                // time to reach target:
                time = (float)(distance / cvl);

                // where the target will be when we reach it:
                if (time < 15)
                {
                    Point run_vec = tgt.Velocity();
                    obj_w = tgt.Location() + (run_vec * time);


                    if (time < 10)
                        obj_w += (tgt.Acceleration() * 0.33f * time * time);
                }
                else
                {
                    obj_w = tgt.Location();
                }
            }

            else
            {
                obj_w = tgt.Location();
            }

            distance = (obj_w - self.Location()).Length;

            if (cvl > 50)
            {
                time = (float)(distance / cvl);

                // where we will be when the target gets there:
                if (time < 15)
                {
                    Point self_dest = self.Location() + cv * (float)time;
                    Point err = obj_w - self_dest;

                    obj_w += err;
                }
            }

            Point approach = obj_w - self.Location();
            distance = approach.Length;

            if (bracket && distance > 25e3)
            {
                Point offset = approach * new Point(0, 1, 0);
                offset.Normalize();
                offset *= 15e3f;

                Ship s = (Ship)self;
                if ((s.GetElementIndex() & 1) != 0)
                    obj_w -= offset;
                else
                    obj_w += offset;
            }
        }
        public virtual void FindObjectiveNavPoint()
        {
            SimRegion self_rgn = ship.GetRegion();
            SimRegion nav_rgn = navpt.Region();
            QuantumDrive qdrive = ship.GetQuantumDrive();

            if (self_rgn == null)
                return;

            if (nav_rgn == null)
            {
                nav_rgn = self_rgn;
                navpt.SetRegion(nav_rgn);
            }

            bool use_farcaster = self_rgn != nav_rgn &&
                                (navpt.Farcast() != null ||
                                qdrive == null ||
                                !qdrive.IsPowerOn() ||
                                qdrive.Status() < ShipSystem.STATUS.DEGRADED
                                );

            if (use_farcaster)
            {
                FindObjectiveFarcaster(self_rgn, nav_rgn);
            }

            else
            {
                if (farcaster != null)
                {
                    if (farcaster.GetShip().GetRegion() != self_rgn)
                        farcaster = farcaster.GetDest().GetFarcaster();

                    obj_w = farcaster.EndPoint();
                }

                else
                {
                    // transform from starsystem to world coordinates:
                    Point npt = navpt.Region().Location() + navpt.Location();

                    SimRegion active_region = ship.GetRegion();

                    if (active_region != null)
                        npt -= active_region.Location();

                    npt = npt.OtherHand();

                    obj_w = npt;
                }

                // distance from self to navpt:
                distance = (obj_w - ship.Location()).Length;

                if (farcaster != null && distance < 1000)
                    farcaster = null;

                if (distance < 1000 || ((int)navpt.Action() == (int)Instruction.ACTION.LAUNCH && distance > 25000))
                    ship.SetNavptStatus(navpt, Instruction.STATUS.COMPLETE);
            }
        }

        protected virtual void FindObjectiveFormation()
        {
            const double prediction = 5;

            // find the base position:
            Element elem = ship.GetElement();
            Ship lead = elem.GetShip(1);
            Ship ward = ship.GetWard();

            if (lead == null || lead == ship)
            {
                lead = ward;

                distance = (lead.Location() - self.Location()).Length;
                if (distance < 30e3 && lead.Velocity().Length < 50)
                {
                    obj_w = self.Location() + lead.Heading() * 1e6f;
                    distance = -1;
                    return;
                }
            }

            obj_w = lead.Location() + lead.Velocity() * (float)prediction;
            Matrix33D m = Matrix33D.CreateRotation(QuaternionD.CreateRotation(0, Point.UnitX, 0, Point.UnitY, (float)(lead.CompassHeading() - Math.PI), Point.UnitZ, true));
            // TODO Cheack that the above code is similar to m.Rotate(0, 0, lead.CompassHeading() - Math.PI);
            Point fd = m * formation_delta;  // TODO matrix * vector multiplication order
            obj_w += fd;

            // try to avoid smacking into the ground...
            if (ship.IsAirborne())
            {
                if (ship.AltitudeAGL() < 3000 || lead.AltitudeAGL() < 3000)
                {
                    obj_w.Y += 500;
                }
            }

            Point dst_w = self.Location() + self.Velocity() * (float)prediction;
            Point dlt_w = obj_w - dst_w;

            distance = dlt_w.Length;

            // get slot z distance:
            dlt_w += ship.Location();
            slot_dist = Transform(dlt_w).Z;

            IDirector lead_dir = lead.GetDirector();
            if (lead_dir != null && ((SteerType)lead_dir.Type() == SteerType.FIGHTER || (SteerType)lead_dir.Type() == SteerType.STARSHIP))
            {
                ShipAI lead_ai = (ShipAI)lead_dir;
                farcaster = lead_ai.GetFarcaster();
            }
            else
            {
                Instruction navpt = elem.GetNextNavPoint();
                if (navpt == null)
                {
                    farcaster = null;
                    return;
                }

                SimRegion self_rgn = ship.GetRegion();
                SimRegion nav_rgn = navpt.Region();
                QuantumDrive qdrive = ship.GetQuantumDrive();

                if (self_rgn != null && nav_rgn == null)
                {
                    nav_rgn = self_rgn;
                    navpt.SetRegion(nav_rgn);
                }

                bool use_farcaster = self_rgn != nav_rgn &&
                (navpt.Farcast() != null ||
                qdrive == null ||
                !qdrive.IsPowerOn() ||
                qdrive.Status() < ShipSystem.STATUS.DEGRADED
                );

                if (use_farcaster)
                {
                    foreach (var s in self_rgn.Ships())
                    {
                        if (farcaster != null) break;
                        if (s.GetFarcaster() != null)
                        {
                            Ship dest = s.GetFarcaster().GetDest();
                            if (dest != null && dest.GetRegion() == nav_rgn)
                            {
                                farcaster = s.GetFarcaster();
                            }
                        }
                    }
                }
                else if (farcaster != null)
                {
                    if (farcaster.GetShip().GetRegion() != self_rgn)
                        farcaster = farcaster.GetDest().GetFarcaster();

                    obj_w = farcaster.EndPoint();
                    distance = (obj_w - ship.Location()).Length;

                    if (distance < 1000)
                        farcaster = null;
                }
            }
        }
        protected virtual void FindObjectivePatrol()
        {
            navpt = null;

            Point npt = patrol_loc;
            obj_w = npt;

            // distance from self to navpt:
            distance = (obj_w - self.Location()).Length;

            if (distance < 1000)
            {
                ship.ClearRadioOrders();
                ClearPatrol();
            }
        }

        protected virtual void FindObjectiveQuantum()
        {
            Instruction orders = ship.GetRadioOrders();
            SimRegion self_rgn = ship.GetRegion();
            SimRegion nav_rgn = orders.Region();
            QuantumDrive qdrive = ship.GetQuantumDrive();

            if (self_rgn == null || nav_rgn == null)
                return;

            bool use_farcaster = self_rgn != nav_rgn &&
                                    (orders.Farcast() != null ||
                                    qdrive == null ||
                                    !qdrive.IsPowerOn() ||
                                    qdrive.Status() < ShipSystem.STATUS.DEGRADED
                                    );

            if (use_farcaster)
            {
                FindObjectiveFarcaster(self_rgn, nav_rgn);
            }

            else
            {
                if (farcaster != null)
                {
                    if (farcaster.GetShip().GetRegion() != self_rgn)
                        farcaster = farcaster.GetDest().GetFarcaster();

                    obj_w = farcaster.EndPoint();
                }

                else
                {
                    // transform from starsystem to world coordinates:
                    Point npt = orders.Region().Location() + orders.Location();

                    SimRegion active_region = ship.GetRegion();

                    if (active_region != null)
                        npt -= active_region.Location();

                    npt = npt.OtherHand();

                    obj_w = npt;

                    if (qdrive != null && qdrive.ActiveState() == QuantumDrive.ACTIVE_STATES.ACTIVE_READY)
                    {
                        qdrive.SetDestination(nav_rgn, orders.Location());
                        qdrive.Engage();
                        return;
                    }
                }

                // distance from self to navpt:
                distance = (obj_w - ship.Location()).Length;

                if (farcaster != null)
                {
                    if (distance < 1000)
                    {
                        farcaster = null;
                        ship.ClearRadioOrders();
                    }
                }
                else if (self_rgn == nav_rgn)
                {
                    ship.ClearRadioOrders();
                }
            }
        }
        protected virtual void FindObjectiveFarcaster(SimRegion src_rgn, SimRegion dst_rgn)
        {
            if (farcaster == null)
            {
                foreach (Ship s in src_rgn.Ships())
                {
                    if (s.GetFarcaster() != null)
                    {
                        Ship dest = s.GetFarcaster().GetDest();
                        if (dest != null && dest.GetRegion() == dst_rgn)
                        {
                            farcaster = s.GetFarcaster();
                            if (farcaster != null) break;
                        }
                    }
                }
            }

            if (farcaster != null)
            {
                Point apt = farcaster.ApproachPoint(0);
                Point npt = farcaster.StartPoint();
                double r1 = (ship.Location() - npt).Length;

                if (r1 > 50e3)
                {
                    obj_w = apt;
                    distance = r1;
                }

                else
                {
                    double r2 = (ship.Location() - apt).Length;
                    double r3 = (npt - apt).Length;

                    if (r1 + r2 < 1.2 * r3)
                    {
                        obj_w = npt;
                        distance = r1;
                    }
                    else
                    {
                        obj_w = apt;
                        distance = r2;
                    }
                }

                objective = Transform(obj_w);
            }
        }

        // fire on target if appropriate:
        protected virtual void AdjustDefenses()
        {
            Shield shield = ship.GetShield();

            if (shield != null)
            {
                double desire = 50;

                if (threat_missile != null || threat != null)
                    desire = 100;

                shield.SetPowerLevel(desire);
            }
        }

        protected virtual void FireControl()
        {
        }

        protected virtual void HelmControl()
        {
            double trans_x = 0;
            double trans_y = 0;
            double trans_z = 0;

            ship.SetHelmHeading(accumulator.yaw);

            if (Math.Abs(accumulator.pitch) < MathHelper.ToRadians(5) || Math.Abs(accumulator.pitch) > MathHelper.ToRadians(45))
            {
                trans_z = objective.Y;
                ship.SetHelmPitch(0);
            }

            else
            {
                ship.SetHelmPitch(accumulator.pitch);
            }

            ship.SetTransX(trans_x);
            ship.SetTransY(trans_y);
            ship.SetTransZ(trans_z);

            ship.ExecFLCSFrame();
        }

        protected virtual void ThrottleControl()
        {
            if (navpt != null && threat == null && target == null)
            {     // lead only, get speed from navpt
                double speed = navpt.Speed();

                if (speed > 0)
                    throttle = speed / ship.VelocityLimit() * 100;
                else
                    throttle = 50;
            }

            else if (patrol != 0 && threat == null && target == null)
            { // lead only, get speed from navpt
                double speed = 200;

                if (distance > 5000)
                    speed = 500;

                if (ship.Velocity().Length > speed)
                    throttle = 0;
                else
                    throttle = 50;
            }

            else
            {
                if (threat != null || target != null || element_index < 2)
                { // element lead
                    throttle = 100;

                    if (threat == null && target == null)
                        throttle = 50;

                    if (accumulator.brake > 0)
                    {
                        throttle *= (1 - accumulator.brake);
                    }
                }

                else
                {                                       // wingman
                    Ship lead = ship.GetElement().GetShip(1);
                    double lv = lead.Velocity().Length;
                    double sv = ship.Velocity().Length;
                    double dv = lv - sv;
                    double dt = 0;

                    if (dv > 0) dt = dv * 1e-2 * seconds;
                    else if (dv < 0) dt = dv * 1e-2 * seconds;

                    throttle = old_throttle + dt;
                }
            }

            old_throttle = throttle;
            ship.SetThrottle((int)throttle);
        }

        protected virtual void NavlightControl()
        {
            Ship leader = ship.GetLeader();

            if (leader != null && leader != ship)
            {
                bool navlight_enabled = false;

                if (leader.NavLights().Count > 0)
                    navlight_enabled = leader.NavLights()[0].IsEnabled();

                for (int i = 0; i < ship.NavLights().Count; i++)
                {
                    if (navlight_enabled)
                        ship.NavLights()[i].Enable();
                    else
                        ship.NavLights()[i].Disable();
                }
            }
        }


        protected virtual void CheckTarget()
        {
            if (target != null)
            {
                if (target.Life() == 0)
                    target = null;

                else if ((SimObject.TYPES)target.Type() == SimObject.TYPES.SIM_SHIP)
                {
                    Ship tgt_ship = (Ship)target;

                    if (tgt_ship.GetIFF() == ship.GetIFF() && !tgt_ship.IsRogue())
                        target = null;
                }
            }
        }
        private double ClosestApproachTime(Point loc1, Point vel1, Point loc2, Point vel2)
        {
            double t = 0;

            Point D = loc1 - loc2;
            Point Dv = vel1 - vel2;

            if (Dv.X != 0 || Dv.Y != 0 || Dv.Z != 0)
                t = -1 * Point.Dot(Dv, D) / Point.Dot(Dv, Dv);

            return t;
        }


        protected Ship ship;
        protected Ship support;
        protected Ship rumor;
        protected Ship threat;
        protected Shot threat_missile;
        protected Instruction navpt;
        protected Point obstacle;
        protected TacticalAI tactical;
        protected Farcaster farcaster;
        protected int engaged_ship_id;
        protected int splash_count;

        protected Point formation_delta;
        protected double slot_dist;

        protected double throttle;
        protected double old_throttle;
        protected double seconds;
        protected double drop_time;
        protected double brake;
        protected uint last_avoid_time;
        protected uint last_call_time;

        protected int element_index;
        protected int too_close;
        protected bool bracket;
        protected bool identify;
        protected bool hold;
        protected bool takeoff;

        protected int patrol;
        protected Point patrol_loc;
        protected int ai_level;

        protected ContentBundle localeManager;
    }
}
