﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx.lib
    ORIGINAL FILE:      VideoSettings.h/VideoSettings.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
        Video Settings class
*/
using System;
using System.Xml;
using StarshatterNet.Gen.Misc;
using DWORD = System.UInt32;

namespace StarshatterNet.Config
{
    public struct VideoMode
    {
        public enum Format
        {
            FMT_NONE = 0,
            FMT_R5G5B5 = 24,
            FMT_R5G6B5 = 23,
            FMT_R8G8B8 = 20,
            FMT_X8R8G8B8 = 22
        };

        public VideoMode(int w, int h, Format f, int r = 0)
        {
            width = w;
            height = h;
            refresh = r;
            format = f;
        }

        public static bool operator ==(VideoMode left, VideoMode right)
        {
            return (left.width == right.width &&
                    left.height == right.height &&
                    left.refresh == right.refresh &&
                left.format == right.format);
        }
        public static bool operator !=(VideoMode left, VideoMode right)
        {
            return (left.width != right.width ||
                    left.height != right.height ||
                    left.refresh != right.refresh ||
                    left.format != right.format);
        }
        public override bool Equals(object obj)
        {
            return (obj is VideoMode) && (this == (VideoMode)obj);
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + width.GetHashCode();
            hash = hash * 23 + height.GetHashCode();
            hash = hash * 23 + refresh.GetHashCode();
            hash = hash * 23 + refresh.GetHashCode();
            return hash;
        }


        public int Depth
        {
            get
            {
                int bpp = 0;
                switch (format)
                {
                    default:
                    case VideoMode.Format.FMT_NONE: bpp = 0; break;
                    case VideoMode.Format.FMT_R5G5B5: bpp = 15; break;
                    case VideoMode.Format.FMT_R5G6B5: bpp = 16; break;
                    case VideoMode.Format.FMT_R8G8B8: bpp = 24; break;
                    case VideoMode.Format.FMT_X8R8G8B8: bpp = 32; break;
                }
                return bpp;
            }
        }

        public string GetDescription()
        {
            string desc;
            desc = string.Format("{0} x {1} x {2}", width, height, Depth);
            return desc;
        }



        public int width;
        public int height;
        public int refresh;
        public Format format;
    }

    public class VideoDeviceInfo
    {
        public VideoDeviceInfo()
        {
            vertex_processing = VideoSettings.VertexProcessing.VTX_HARDWARE;
            depth_buffer_bits = 32;
        }

        public VideoSettings.VertexProcessing vertex_processing;
        public int depth_buffer_bits;
        public int adapter_index;
        public int device_index;
        public DWORD device_type;
        public DWORD depth_stencil_format;
        public DWORD back_buffer_format;
        public DWORD multisample_type;
        public DWORD multisample_qual;
        public string adapter_desc;
        public string device_desc;
    }

    public class VideoSettings
    {
        public enum VertexProcessing
        {
            VTX_SOFTWARE,
            VTX_MIXED,
            VTX_HARDWARE,
            VTX_PURE
        };

        public VideoSettings()
        {
            // set up defaults:
            int screen_width = 1280;
            int screen_height = 720;
            int screen_depth = 32;
            int terrain_detail_level = 3;
            bool shadows_enabled = true;
            bool spec_maps_enabled = true;
            bool bump_maps_enabled = true;
            bool vertex_shader = true;
            bool pixel_shader = true;



            fullscreen_mode.width = 1024;
            fullscreen_mode.height = 768;
            fullscreen_mode.refresh = 75;
            fullscreen_mode.format = VideoMode.Format.FMT_X8R8G8B8;

            windowed_mode.width = 800;
            windowed_mode.height = 600;
            windowed_mode.refresh = 0;
            windowed_mode.format = VideoMode.Format.FMT_NONE;

            window_width = 800;
            window_height = 600;

#if DEBUG
            is_windowed = true;
#else
            is_windowed = false;
#endif

            use_effects = true;
            shadows = true;
            bumpmaps = true;
            specmaps = true;
            max_detail = 4;
            enable_vs = true;
            enable_ps = true;
            depth_bias = 0;
        }


        // accessor methods

        public bool IsWindowed
        {
            get { return is_windowed; }
        }

        public bool UseEffects()
        {
            return use_effects;
        }

        public int Width
        {
            get
            {
                if (is_windowed)
                    return window_width;

                return fullscreen_mode.width;
            }
        }

        public int Height
        {
            get
            {
                if (is_windowed)
                    return window_height;

                return fullscreen_mode.height;
            }
        }

        public int Depth
        {
            get
            {
                if (is_windowed)
                    return windowed_mode.Depth;
                else
                    return fullscreen_mode.Depth;
            }
        }

        public int GetPixSize()
        {
            VideoMode.Format fmt = VideoMode.Format.FMT_NONE;
            int pix = 0;

            if (is_windowed)
                fmt = windowed_mode.format;
            else
                fmt = fullscreen_mode.format;

            switch (fmt)
            {
                default:
                case VideoMode.Format.FMT_NONE: pix = 0; break;
                case VideoMode.Format.FMT_R5G5B5: pix = 2; break;
                case VideoMode.Format.FMT_R5G6B5: pix = 2; break;
                case VideoMode.Format.FMT_R8G8B8: pix = 3; break;
                case VideoMode.Format.FMT_X8R8G8B8: pix = 4; break;
            }

            return pix;
        }

        public int GetRefreshRate()
        {
            if (is_windowed)
                return windowed_mode.refresh;

            return fullscreen_mode.refresh;
        }

        public string GetModeDescription()
        {
            if (is_windowed)
                return windowed_mode.GetDescription();

            return fullscreen_mode.GetDescription();
        }

        public VideoSettings.VertexProcessing GetVertexProcessing()
        {
            if (is_windowed)
                return windowed_device.vertex_processing;

            return fullscreen_device.vertex_processing;
        }

        public int GetDepthBufferBits()
        {
            if (is_windowed)
                return windowed_device.depth_buffer_bits;

            return fullscreen_device.depth_buffer_bits;
        }
        public int GetAdapterIndex()
        {
            if (is_windowed)
                return windowed_device.adapter_index;

            return fullscreen_device.adapter_index;
        }

        public int GetDeviceIndex()
        {
            if (is_windowed)
                return windowed_device.device_index;

            return fullscreen_device.device_index;
        }

        public DWORD GetDeviceType()
        {
            if (is_windowed)
                return windowed_device.device_type;

            return fullscreen_device.device_type;
        }

        public DWORD GetDepthStencilFormat()
        {
            if (is_windowed)
                return windowed_device.depth_stencil_format;

            return fullscreen_device.depth_stencil_format;
        }

        public DWORD GetBackBufferFormat()
        {
            if (is_windowed)
                return windowed_device.back_buffer_format;

            return fullscreen_device.back_buffer_format;
        }

        public string GetAdapterDesc()
        {
            if (is_windowed)
                return windowed_device.adapter_desc;

            return fullscreen_device.adapter_desc;
        }

        public string GetDeviceDesc()
        {
            if (is_windowed)
                return windowed_device.device_desc;

            return fullscreen_device.device_desc;
        }

        /// <summary>
        /// Saves the video config entries.
        /// </summary>
        /// <param name="writer"> The XML writer to which the video config is written.</param>
        public void Save(XmlWriter writer)
        {
            if (writer == null)
                return;

            writer.WriteStartElement("VideoConfig");
            {
                writer.WriteStartElement("ScreenMode");
                writer.WriteAttributeString("width", this.Width.ToString());
                writer.WriteAttributeString("height", this.Height.ToString());
                writer.WriteAttributeString("depth", this.fullscreen_mode.Depth.ToString());
                writer.WriteEndElement();

                //writer.WriteStartElement("WindowedMode");
                //writer.WriteAttributeString("width", 1280.ToString());
                //writer.WriteAttributeString("height", 720.ToString());
                //writer.WriteAttributeString("depth", 32.ToString());
                //writer.WriteEndElement();

                writer.WriteStartElement("GraphicsSettings");
                writer.WriteAttributeString("is_windowed", false.ToString());
                writer.WriteAttributeString("shadows", true.ToString());
                writer.WriteAttributeString("max_detail", 4.ToString());
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        /// <summary>
        /// Loads the key map entries.
        /// </summary>
        /// <param name="reader">
        /// An XML reader for th XML document containing the video configuration.
        /// </param>
        public static VideoSettings Load(XmlReader reader)
        {
            VideoSettings videoSetting = new VideoSettings();

            if (reader == null)
                return videoSetting;

            if (!reader.ReadToFollowing("VideoConfig"))
                return videoSetting;

            if (!reader.ReadToDescendant("ScreenMode"))
                return videoSetting;

            var attr = reader.GetAttribute("width");
            if (!string.IsNullOrWhiteSpace(attr))
            {

                bool success = Int32.TryParse(attr, out videoSetting.window_width);
                if (!success)
                    ErrLogger.PrintLine("Error in video config. Parsing FullscreenMode, attribute width = {0}", attr);
            }
            attr = reader.GetAttribute("height");
            if (!string.IsNullOrWhiteSpace(attr))
            {

                bool success = Int32.TryParse(attr, out videoSetting.window_height);
                if (!success)
                    ErrLogger.PrintLine("Error in video config. Parsing FullscreenMode, attribute height = {0}", attr);
            }
            attr = reader.GetAttribute("depth");
            //if (!StringExtensions.IsNullOrWhiteSpace(attr))
            //{

            //    bool success = EnumHelper.TryParse(attr, true, out videoSetting.fullscreen_mode.format);
            //    if (!success)
            //        ErrLogger.PrintLine("Error in video config. Parsing FullscreenMode, attribute depth = {0}", attr);
            //}

            return videoSetting;
        }

        // properties

        public bool is_windowed;
        public bool use_effects;
        public VideoMode fullscreen_mode;
        public VideoMode windowed_mode;
        public int window_width;
        public int window_height;
        public VideoDeviceInfo fullscreen_device;
        public VideoDeviceInfo windowed_device;

        // feature set

        public bool shadows;
        public bool bumpmaps;
        public bool specmaps;
        public int max_detail;
        public bool enable_vs;
        public bool enable_ps;
        public float depth_bias;

    }
}
