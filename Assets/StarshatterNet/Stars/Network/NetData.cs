﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetData.h/NetData.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Payload structures for multiplayer network packets
*/
using System;
using UnityEngine;
using BYTE = System.Byte;
using DWORD = System.Int16;

namespace StarshatterNet.Stars.Network
{
    public class NetData
    {
        // UNRELIABLE: 0x01 - 0x0F

        public const BYTE NET_PING = 0x01;
        public const BYTE NET_PONG = 0x02;
        public const BYTE NET_OBJ_LOC = 0x03;

        // RELIABLE:   0x10 - 0x7F

        public const BYTE NET_JOIN_REQUEST = 0x10;
        public const BYTE NET_JOIN_ANNOUNCE = 0x11;
        public const BYTE NET_QUIT_REQUEST = 0x12;
        public const BYTE NET_QUIT_ANNOUNCE = 0x13;
        public const BYTE NET_KICK_REQUEST = 0x14;
        public const BYTE NET_KICK_ANNOUNCE = 0x15;
        public const BYTE NET_GAME_OVER = 0x16;
        public const BYTE NET_COMM_MESSAGE = 0x17;
        public const BYTE NET_CHAT_MESSAGE = 0x18;
        public const BYTE NET_DISCONNECT = 0x19;

        public const BYTE NET_OBJ_DAMAGE = 0x20;
        public const BYTE NET_OBJ_KILL = 0x21;
        public const BYTE NET_OBJ_SPAWN = 0x22;
        public const BYTE NET_OBJ_HYPER = 0x23;
        public const BYTE NET_OBJ_TARGET = 0x24;
        public const BYTE NET_OBJ_EMCON = 0x25;
        public const BYTE NET_SYS_DAMAGE = 0x26;
        public const BYTE NET_SYS_STATUS = 0x27;

        public const BYTE NET_ELEM_CREATE = 0x28;
        public const BYTE NET_SHIP_LAUNCH = 0x29;
        public const BYTE NET_NAV_DATA = 0x2A;
        public const BYTE NET_NAV_DELETE = 0x2B;
        public const BYTE NET_ELEM_REQUEST = 0x2C;

        public const BYTE NET_WEP_TRIGGER = 0x30;
        public const BYTE NET_WEP_RELEASE = 0x31;
        public const BYTE NET_WEP_DESTROY = 0x32;

        public const BYTE NET_SELF_DESTRUCT = 0x3F;

        public virtual int Type()
        {
            return 0;
        }
        public virtual int Length() { return 0; }
        //public virtual NetOutgoingMessage Pack(NetPeer peer) { return null; }
        //public virtual bool Unpack(NetIncomingMessage msg) { return false; }

        public virtual DWORD GetObjID() { return 0; }
        public virtual void SetObjID(DWORD o) { }
    }

    /// <summary>
    /// +--------------------------------------------------------------------+
    /// DATA     SIZE              OFFSET
    /// -------- ----------------- ---------
    /// type:    1                  0
    /// size:    1                  1
    /// objid:   2                  2
    /// loc:     12 (3 x 32 bits)   4
    /// vel:     12 (3 x 32 bits)   16
    /// euler:   12 (3 x 32 bits)   28
    /// shield:  4                  40
    /// status:  1                  44
    /// </summary>
    public class NetObjLoc : NetData
    {
        private const byte TYPE = NET_OBJ_LOC;
        private const byte SIZE = 45;

        public NetObjLoc() { }
        public NetObjLoc(DWORD oid, Vector3 pos, Vector3 orient, Vector3 vel)
        {
            objid = oid;
            location = pos;
            euler = orient;
            velocity = vel;
        }

        //public override NetOutgoingMessage Pack(NetPeer peer)
        //{
        //    NetOutgoingMessage msg = peer.CreateMessage(SIZE);
        //    msg.Write(TYPE);
        //    msg.Write(SIZE);
        //    msg.Write(objid);
        //    msg.Write(location);
        //    msg.Write(velocity);
        //    msg.Write(euler);
        //    msg.Write(shield);
        //    // status bits
        //    msg.Write(throttle);
        //    msg.Write(augmenter);
        //    msg.Write(gear);
        //    return msg;
        //}
        //public override bool Unpack(NetIncomingMessage msg)
        //{
        //    if (msg.ReadByte() != TYPE || msg.ReadByte() != SIZE) return false;
        //    objid = msg.ReadInt16();
        //    location = msg.ReadVector3();
        //    velocity = msg.ReadVector3();
        //    euler = msg.ReadVector3();
        //    shield = msg.ReadInt32();
        //    // status bits
        //    throttle = msg.ReadBoolean();
        //    augmenter = msg.ReadBoolean();
        //    gear = msg.ReadBoolean();
        //    return true;
        //}
        public override int Type() { return TYPE; }
        public override int Length() { return SIZE; }

        public override DWORD GetObjID() { return objid; }
        public override void SetObjID(DWORD id) { objid = id; }

        public Vector3 GetLocation() { return location; }
        public Vector3 GetVelocity() { return velocity; }
        public Vector3 GetOrientation() { return euler; }
        public bool GetThrottle() { return throttle; }
        public bool GetAugmenter() { return augmenter; }
        public bool GetGearDown() { return gear; }
        public int GetShield() { return shield; }

        public void SetLocation(Vector3 loc) { location = loc; }
        public void SetVelocity(Vector3 v) { velocity = v; }
        public void SetOrientation(Vector3 o) { euler = o; }
        public void SetThrottle(bool t) { throttle = t; }
        public void SetAugmenter(bool a) { augmenter = a; }
        public void SetGearDown(bool g) { gear = g; }
        public void SetShield(int s) { shield = s; }


        private DWORD objid = 0;
        private Vector3 location;
        private Vector3 velocity;
        private Vector3 euler;
        private bool throttle = false;
        private bool augmenter = false;
        private bool gear = false;
        private int shield = 0;
    }
}
