﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetMsg.h/NetMsg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

     OVERVIEW
     ========
     User level network message
 */
using System;
using System.Collections.Generic;
using System.Text;

namespace StarshatterNet.Stars.Network
{
    public class NetGram
    {
        public const uint NET_GRAM_ACK = 0x80000000;
        public const int NET_GRAM_RELIABLE = 0x40000000;
        public const int NET_GRAM_SEQ_MASK = 0x3fffffff;

        public const int NET_GRAM_HEADER_SIZE = 4;
        public const int NET_GRAM_MAX_SIZE = 1024;

        /// <summary>
        /// NetGram constructor for receiving packets from remote hosts
        /// </summary>
        /// <param name="src"></param>
        /// <param name="msg"></param>
        public NetGram(NetAddr src, string msg)
        {
            addr = src; retries = 0; send_time = 0;
            body = msg;

            throw new NotImplementedException();
#if TODO
            if (body.Length  >= NET_GRAM_HEADER_SIZE)
            {
               byte[] data =  body.data();

                packet_id = (((DWORD)data[0]) << 24) +
                                (((DWORD)data[1]) << 16) +
                                (((DWORD)data[2]) << 8) +
                                 ((DWORD)data[3]);
            }
#endif
        }

        /// <summary>
        /// NetGram constructor for composing packets to send to remote hosts
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="user_data"></param>
        /// <param name="retries"></param>
        public NetGram(NetAddr dst, string user_data, int r)
        {
            addr = dst; retries = r;
            send_time = NetLayer.GetTime();
            packet_id = net_gram_sequence++;

            if (retries != 0)
                packet_id |= NET_GRAM_RELIABLE;

            byte[] buf = new byte[NET_GRAM_MAX_SIZE];
            buf[0] = (byte)((packet_id >> 24) & 0xff);
            buf[1] = (byte)((packet_id >> 16) & 0xff);
            buf[2] = (byte)((packet_id >> 8) & 0xff);
            buf[3] = (byte)((packet_id) & 0xff);

            int len = user_data.Length;
            if (len >= NET_GRAM_MAX_SIZE - NET_GRAM_HEADER_SIZE)
                len = NET_GRAM_MAX_SIZE - NET_GRAM_HEADER_SIZE - 1;

            throw new NotImplementedException();
#if TODO
            CopyMemory(buf + NET_GRAM_HEADER_SIZE, user_data.data(), len);
            Array.Copy();
            body = Text((char*)buf, len + NET_GRAM_HEADER_SIZE);
#endif
        }

        /*
        public int operator ==(NetGram g)
        {
            return packet_id == g.packet_id &&
                        addr == g.addr;
        }
        public int operator <(NetGram g) { return Sequence() < g.Sequence(); }
        */
        public uint PacketID() { return packet_id; }
        public uint Sequence() { return packet_id & NET_GRAM_SEQ_MASK; }
        public long SendTime() { return send_time; }
        public byte[] Data()
        {
            return Encoding.UTF8.GetBytes(body);
        }
        public byte[] UserData()
        {
            throw new NotImplementedException();
#if TODO

            return this.Data() + NET_GRAM_HEADER_SIZE;
#endif
        }
        public int Size() { return this.Data().Length; }
        public string Body() { return body; }
        public NetAddr Address() { return addr; }

        public bool IsAck()
        {
            throw new NotImplementedException();
#if TODO
            return packet_id & NET_GRAM_ACK ? true : false; 
#endif
        }
        public bool IsReliable()
        {
            throw new NotImplementedException();
#if TODO
            return packet_id & NET_GRAM_RELIABLE ? true : false; 
#endif
        }
        public int Retries() { return retries; }

        public void Retry()
        {
            if (retries > 0)
            {
                retries--;
                send_time = NetLayer.GetTime();
            }
        }

        public NetGram Ack()
        {
            NetGram ack = new NetGram();

            ack.packet_id = packet_id | NET_GRAM_ACK;
            ack.send_time = NetLayer.GetTime();

            throw new NotImplementedException();
#if TODO

            static BYTE buf[NET_GRAM_HEADER_SIZE];
            buf[0] = (BYTE)(ack.packet_id >> 24) & 0xff;
            buf[1] = (BYTE)(ack.packet_id >> 16) & 0xff;
            buf[2] = (BYTE)(ack.packet_id >> 8) & 0xff;
            buf[3] = (BYTE)(ack.packet_id) & 0xff;

            ack.body = Text((char*)buf, NET_GRAM_HEADER_SIZE);

            return ack;
#endif
        }

        public void ClearAck() { packet_id &= ~NET_GRAM_ACK; }


        /// <summary>
        /// NetGram constructor for ACK packets
        /// </summary>
        protected NetGram()
        {
            retries = 0; packet_id = 0; send_time = 0;
        }

        protected NetAddr addr;       // network address of remote host
        protected int retries;    // number of retries remaining (reliable packets only)
        protected long send_time;  // time in msec of most recent send attempt

        protected uint packet_id;  // copy of packet id from header in body
        protected string body;       // header plus user data

        private static uint net_gram_sequence = 1;

    }
}