﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      FontMgr.h/FontMgr.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Font Resource Manager class
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StarshatterNet.Stars.Views;
using UnityEngine;

namespace StarshatterNet.Stars.Graphics
{
    public class FontMgr
    {
        public static void Close()
        {
            fonts.Clear();
        }
        public static FontItem Register(string name, Font font, int fontsize = 12)
        {
            FontItem item = new FontItem();

            if (item != null && font != null)
            {
                item.name = name;
                item.size = fontsize;
                item.font = font;

                fonts.Add(item);
            }
            return item;
        }
        public static FontItem Find(string name)
        {
            foreach (FontItem item in fonts)
            {
                if (item.name == name)
                    return item;
            }

            return GetDefault();
        }
        public static FontItem GetDefault()
        {
            if (defaultFont == null)
            {
                var font = Resources.GetBuiltinResource<Font>("Arial.ttf");
                defaultFont = Register("DefaultFont", font, 12);
            }
            return defaultFont;
        }

        private static List<FontItem> fonts = new List<FontItem>();
        private static FontItem defaultFont;
    }
    public class FontItem
    {
        public string name;
        public int size;
        public Font font;
        public Color color;

        public Color GetColor() { return color; }
        public void SetColor(Color c) { color = c; }

        internal void SetAlpha(int v)
        {
            throw new NotImplementedException();
        }

        internal void DrawText(string v1, int v2, Rect menu_rect, TextFormat dT_CENTER, Gen.Graphics.Bitmap cockpit_hud_texture)
        {
            throw new NotImplementedException();
        }

        internal void DrawText(string txt, int count, Rect clip_rect, TextFormat flags)
        {
            throw new NotImplementedException();
        }
    }
}
