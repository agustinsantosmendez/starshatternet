﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      Video.h/Video.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Abstract Video Interface
    The equivalent in Unity could be Screen or Display
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StarshatterNet.Config;
using UnityEngine;

namespace StarshatterNet.Gen.Graphics
{
    /// <summary>
    /// Provides access to a display / screen for rendering operations.
    /// Multi-display rendering is available on PC(Windows/Mac/Linux), iOS and Android.
    /// </summary>
    public class Video
    {
        public enum BLEND_TYPE
        {
            BLEND_SOLID = 1,
            BLEND_ALPHA = 2,
            BLEND_ADDITIVE = 4,
            BLEND_FORCE_DWORD = 0x7fffffff,
        };
        protected Video() { }

        public static Video GetVideo(int index = 0)
        {
            return video_instance;

            // TODO
            // To be implemented at some moment
            // Unity has support for serveral monitors or displays
            // the index will be used as the display number
            // You need to use the Display class to activate and change resolution of additional displays.
            // You can access the additional displays using Display.display[i], where i
            // is the display index(main = 0, 2nd = 1, etc.).
            //
            // for instance:
            // Display.displays[1].Activate();
            // Display.displays[1].SetRenderingResolution(Display.displays[1].systemWidth,
            //                                            Display.displays[1].systemHeight);
        }
        public static Video  GetInstance() { return video_instance; }

        public virtual VideoSettings GetVideoSettings() { throw new NotImplementedException(); }
        public virtual bool SetVideoSettings(VideoSettings vs) { throw new NotImplementedException(); }
        public virtual bool Reset(VideoSettings vs) { throw new NotImplementedException(); }

        public virtual bool SetBackgroundColor(Color c)
        {
            // In Unity, this information is managed by Camera
            return true;
        }
        public virtual int Width()
        {
            return UnityEngine.Screen.width;
        }
        public virtual int Height()
        {
            return UnityEngine.Screen.height;
        }
        public virtual int Depth()
        {
            // In Unity, this information is managed by Camera
            throw new NotImplementedException();
        }
        public virtual int RefreshRate()
        {
            return UnityEngine.Screen.currentResolution.refreshRate;
        }

        public virtual bool IsWindowed() { return !IsFullScreen(); }
        public virtual bool IsFullScreen() { return UnityEngine.Screen.fullScreen; }
        public virtual bool IsModeSupported(int width, int height, int bpp)
        {
            foreach (Resolution resolution in UnityEngine.Screen.resolutions)
            {
                if (resolution.width == width && resolution.height == height)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Switches the screen resolution.
        /// A width by height resolution will be used.If no matching resolution is supported, the closest one will be used.
        /// If preferredRefreshRate is 0 (default) Unity will switch to the highest refresh rate supported by the monitor.
        /// If preferredRefreshRate is not 0 Unity will use it if the monitor supports it, otherwise will choose the highest 
        /// supported one.
        /// </summary>
        /// <param name="width">Resolution width in pixels.</param>
        /// <param name="height">Resolution height in pixels.</param>
        /// <param name="fullscreen">Fullscreen window.</param>
        /// <param name="preferredRefreshRate">Resolution's vertical refresh rate in Hz.</param>
        public virtual void SetResolution(int width, int height, bool fullscreen, int preferredRefreshRate = 0)
        {
            UnityEngine.Screen.SetResolution(width, height, fullscreen, preferredRefreshRate);
        }
        public virtual void SetResolution(int width, int height, FullScreenMode fullscreenMode, int preferredRefreshRate = 0)
        {
            UnityEngine.Screen.SetResolution(width, height, fullscreenMode, preferredRefreshRate);
        }

        public virtual bool ClearAll() { return false; }
        public virtual bool StartFrame() { return false; }
        public virtual bool EndFrame() { return false; }


        protected static Video video_instance = new Video();

        protected int displayNum = 0;

        public int MaxTexSize() { return 256; }

        public void InvalidateCache()
        {
        }
    }
}
