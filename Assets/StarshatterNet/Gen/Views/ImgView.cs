﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      ImgView.h/ImgView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Bitmap "Billboard" View class
*/
using System;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars;
using UnityEngine;
using UnityEngine.UI;

namespace StarshatterNet.Gen.Views
{
    public class ImgView : View
    {
        public ImgView(Window g, Bitmap bmp, string name) : base(g, name)
        {
            img = bmp; width = 0; height = 0; x_offset = 0; y_offset = 0;
            //blend=Video.BLEND_SOLID;

            if (windowObj != null)
                canvas = windowObj.GetComponent<Canvas>();

            SetPicture(bmp);

        }

        //public virtual ~ImgView();

        // Operations:
        public override void Refresh() { }

        public virtual Bitmap GetPicture() { return img; }
        public virtual void SetPicture(Bitmap bmp)
        {
            img = bmp;
            width = 0;
            height = 0;
            x_offset = 0;
            y_offset = 0;

            if (img != null && img.Texture != null)
            {
                width = img.Texture.width;
                height = img.Texture.height;
            }

            if (rectTransform == null) return;

            if (width < rectTransform.rect.width)
                x_offset = (int)((rectTransform.rect.width - width) / 2);

            if (height < rectTransform.rect.height)
                y_offset = (int)((rectTransform.rect.height - height) / 2);
        }
        public virtual int GetBlend() { return blend; }
        public virtual void SetBlend(int b) { blend = b; }

        protected override void BuildGamebjects()
        {
            base.BuildGamebjects();

            imgGameObj = new GameObject(); //Create the GameObject
            image = imgGameObj.AddComponent<Image>(); //Add the Image Component script
            RectTransform rectTransform = imgGameObj.GetComponent<RectTransform>();
            rectTransform.SetParent(this.windowObj.transform); //Assign the newly created Image GameObject as a Child of the Parent Panel.
            imgGameObj.name = this.name;
            imgGameObj.SetActive(true); //Activate the GameObject
        }

        protected Bitmap img;
        protected int x_offset, y_offset;
        protected int width, height;
        protected int blend;

        protected Canvas canvas;
        protected GameObject imgGameObj;
        protected Image image;
    }
}