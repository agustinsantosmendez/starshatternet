﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Sky.h/Sky.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Celestial sphere, stars, planets, space dust...
*/
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.StarSystems;
using System;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.Graphics.General
{
    public class PlanetRep : Solid
    {
        public static readonly Color black = Color.black;
        public PlanetRep(string surface_name, string glow_name,
                      float rad, Point pos, double tscale = 1,
                      string rngname = null, double minrad = 0, double maxrad = 0,
                      Color atmos = new Color() /*= Color.black*/, string gloss_name = null) : base(surface_name, false)
        {
            mtl_surf = null; mtl_limb = null; mtl_ring = null; star_system = null;
            loc = pos;

            radius = rad;
            has_ring = 0;
            ring_verts = -1;
            ring_polys = -1;
            ring_rad = 0;
            body_rad = rad;
            daytime = false;
            atmosphere = atmos;
            star_system = null;

            if (string.IsNullOrEmpty(surface_name))
            {
                ErrLogger.PrintLine("   invalid Planet patch - no surface texture specified");
                return;
            }

            ErrLogger.PrintLine("   constructing Planet patch {0}", surface_name);
            name = surface_name;

            Bitmap bmp_surf = null;
            Bitmap bmp_spec = null;
            Bitmap bmp_glow = null;
            Bitmap bmp_ring = null;
            Bitmap bmp_limb = null;

            Vector3 position = new Vector3((float)pos.X, (float)pos.Y, (float)pos.Z);

            gameObj = new GameObject("Planet");
            gameObj.transform.localScale = Vector3.one * rad;
            gameObj.transform.position = position;
            MeshFilter meshFilter = gameObj.AddComponent<MeshFilter>();
            meshFilter.mesh = Resources.Load("Galaxy/Sphere128", typeof(UnityEngine.Mesh)) as UnityEngine.Mesh;
            MeshRenderer meshRenderer = gameObj.AddComponent<MeshRenderer>();
            meshRenderer.material = Resources.Load("Galaxy/Earth", typeof(UnityEngine.Material)) as UnityEngine.Material;

            GameObject gameObj2 = new GameObject("Atmosphere");
            gameObj2.transform.position = position;
            gameObj2.transform.localScale = new Vector3(1.018f, 1.018f, 1.018f);
            gameObj2.transform.SetParent(gameObj.transform);
            MeshFilter meshFilter2 = gameObj2.AddComponent<MeshFilter>();
            meshFilter2.mesh = Resources.Load("Galaxy/Sphere128", typeof(UnityEngine.Mesh)) as UnityEngine.Mesh;
            MeshRenderer meshRenderer2 = gameObj2.AddComponent<MeshRenderer>();
            meshRenderer2.material = Resources.Load("Galaxy/EarthAtmosphere", typeof(UnityEngine.Material)) as UnityEngine.Material;

#if TODO
            DataLoader loader = DataLoader.GetLoader();
            loader.LoadTexture(surface_name, bmp_surf, Bitmap.BMP_SOLID, true);

            if (string.IsNullOrEmpty(glow_name))
            {
                ErrLogger.PrintLine("   loading glow texture {0}", glow_name);
                loader.LoadTexture(glow_name, bmp_glow, Bitmap.BMP_SOLID, true);
            }

            if (string.IsNullOrEmpty(gloss_name))
            {
                ErrLogger.PrintLine("   loading gloss texture {0}", gloss_name);
                loader.LoadTexture(gloss_name, bmp_spec, Bitmap.BMP_SOLID, true);
            }

            mtl_surf = new Material();

            mtl_surf.Ka = Color.LightGray;
            mtl_surf.Kd = Color.White;
            mtl_surf.Ke = bmp_glow ? Color.White : Color.Black;
            mtl_surf.Ks = bmp_spec ? Color.LightGray : Color.Black;
            mtl_surf.power = 25.0f;
            mtl_surf.tex_diffuse = bmp_surf;
            mtl_surf.tex_specular = bmp_spec;
            mtl_surf.tex_emissive = bmp_glow;
            mtl_surf.blend = Material.BLEND_TYPE.MTL_SOLID;

            if (bmp_spec && Video.GetInstance().IsSpecMapEnabled())
            {
                if (glow_name != null && glow_name == "light")
                    mtl_surf.shader = "SimplePix/PlanetSurfNightLight";

                else if (glow_name != null)
                    mtl_surf.shader = "SimplePix/PlanetSurf";
            }

            if (atmosphere != Color.Black)
            {
                mtl_limb = new Material();

                mtl_limb.Ka = atmosphere;

                mtl_limb.shader = "PlanetLimb";

                ErrLogger.PrintLine("   loading atmospheric limb texture PlanetLimb.pcx");
                loader.LoadTexture("PlanetLimb.pcx", bmp_limb, Bitmap.BMP_TRANSLUCENT, true);
                mtl_limb.tex_diffuse = bmp_limb;
                mtl_limb.blend = Material.BLEND_TYPE.MTL_TRANSLUCENT;
            }

            if (maxrad > 0 && minrad > 0)
            {
                has_ring = 1;
                radius = (float)maxrad;
                ring_rad = (maxrad + minrad) / 2;
                loader.LoadTexture(rngname, bmp_ring, Bitmap.BMP_SOLID, true);

                mtl_ring = new Material();

                mtl_ring.Ka = Color.LightGray;
                mtl_ring.Kd = Color.White;
                mtl_ring.Ks = Color.Gray;
                mtl_ring.Ke = Color.Black;
                mtl_ring.power = 30.0f;
                mtl_ring.tex_diffuse = bmp_ring;
                mtl_ring.blend = Material.BLEND_TYPE.MTL_TRANSLUCENT;
            }

            if (rad > 2e6 && rad < 1e8)
                CreateSphere(rad, 24, 32, minrad, maxrad, 48, tscale);
            else
                CreateSphere(rad, 16, 24, minrad, maxrad, 48, tscale);
#endif
        }

        //public virtual ~PlanetRep();


        //public virtual void Render(Video* video, DWORD flags);

        public virtual int CheckRayIntersection(Point pt, Point vpn, double len, out Point ipt,
                                                bool treat_translucent_polys_as_solid = true)
        {
#if TODO
            // compute leading edge of ray:
            Point dst = Q + w * len;

            // check right angle spherical distance:
            Point d0 = loc - Q;
            Point d1 = d0.cross(w);
            double dlen = d1.Length;          // distance of point from line

            if (dlen > body_rad)                // clean miss
                return 0;                        // (no impact)

            // possible collision course...
            Point d2 = Q + w * (d0 * w);

            // so check the leading edge:
            Point delta0 = dst - loc;

            if (delta0.Length > radius)
            {
                // and the endpoints:
                Point delta1 = d2 - Q;
                Point delta2 = dst - Q;

                // if d2 is not between Q and dst, we missed:
                if (delta1 * delta2 < 0 ||
                        delta1.Length > delta2.Length)
                {
                    return 0;
                }
            }

            return 1;
#endif
            throw new NotImplementedException();
        }

        protected void CreateSphere(double radius, int nrings, int nsections,
                                    double minrad, double maxrad, int rsections,
                                    double tscale)
        { throw new NotImplementedException(); }

        public virtual void SetDaytime(bool d)
        {
            daytime = d;

            if (daytime)
            {
                if (mtl_surf != null) mtl_surf.blend = Material.BLEND_TYPE.MTL_ADDITIVE;
                if (mtl_ring != null) mtl_ring.blend = Material.BLEND_TYPE.MTL_ADDITIVE;
            }

            else
            {
                if (mtl_surf != null) mtl_surf.blend = Material.BLEND_TYPE.MTL_SOLID;
                if (mtl_ring != null) mtl_ring.blend = Material.BLEND_TYPE.MTL_TRANSLUCENT;
            }
        }
        public virtual void SetStarSystem(StarSystem system)
        {
            star_system = system;
        }


        protected Material mtl_surf;
        protected Material mtl_limb;
        protected Material mtl_ring;
        public virtual Color Atmosphere() { return atmosphere; }
        public virtual void SetAtmosphere(Color a) { atmosphere = a; }

        protected int has_ring;
        protected int ring_verts;
        protected int ring_polys;
        protected double ring_rad;
        protected double body_rad;
        protected Color atmosphere;
        protected bool daytime;
        protected StarSystem star_system;
    }
}
