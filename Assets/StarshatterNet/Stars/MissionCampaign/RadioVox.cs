﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      RadioVox.h/RadioVox.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    View class for Radio Communications HUD Overlay
*/
using StarshatterNet.Config;
using StarshatterNet.Audio;
using System;
using System.Collections.Generic;
using System.Threading;
using StarshatterNet.Stars.Views;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning RadioVox class is still in development and is not recommended for production.
    public class RadioVox
    {
        public RadioVox(int channel, string path, string message = null)
        {
            this.path = path;
            this.message = message;
            this.index = 0;
            this.channel = channel;
        }
        // ~RadioVox()
        //{
        //    sounds.destroy();
        //}


        // Operations:
        public virtual bool AddPhrase(string key)
        {
            if (AudioConfig.VoxVolume() <= AudioConfig.Silence())
                return false;

            if (!string.IsNullOrEmpty(key))
            {
                string datapath;
                string filename;

                datapath = string.Format("Vox/{0}/", path);
                filename = string.Format("{0}.wav", key);

                //bool use_fs = loader.IsFileSystemEnabled();
                Sound sound = null;

                //loader.UseFileSystem(true);
                //loader.SetDataPath(datapath);
                //loader.LoadSound(filename, sound, Sound.FlagEnum.LOCALIZED, true); // optional sound
                //loader.SetDataPath(0);
                //loader.UseFileSystem(use_fs);
                Sound.CreateStream(filename, Sound.FlagEnum.LOCALIZED);// optional sound

                if (sound != null)
                {
                    sound.SetVolume(AudioConfig.VoxVolume());
                    sound.SetFlags(Sound.FlagEnum.LOCALIZED | Sound.FlagEnum.LOCKED);
                    sound.SetFilename(filename);
                    sounds.Add(sound);

                    return true;
                }
            }

            return false;
        }

        public virtual bool Start()
        {
            if (RadioVoxController.controller != null)
                return RadioVoxController.controller.Add(this);

            return false;
        }

        public static void Initialize()
        {
            if (RadioVoxController.controller == null)
            {
                RadioVoxController.controller = new RadioVoxController();
            }
        }

        public static void Close()
        {
            //delete controller;
            RadioVoxController.controller = null;
        }



        protected internal virtual bool Update()
        {
            if (message.Length != 0)
            {
                RadioView.Message(message);
                message = "";
            }

            bool active = false;

            while (!active && index < sounds.Count)
            {
                Sound s = sounds[index];

                if (s.IsReady())
                {
                    if ((channel & 1) != 0)
                        s.SetPan(channel * -3000);
                    else
                        s.SetPan(channel * 3000);

                    s.Play();
                    active = true;
                }

                else if (s.IsPlaying())
                {
                    s.Update();
                    active = true;
                }

                else
                {
                    index++;
                }
            }

            return active;
        }

        protected string path;
        protected string message;
        protected internal List<Sound> sounds = new List<Sound>();
        protected int index;
        protected int channel;
    }

    internal class RadioVoxController
    {

        public const int MAX_QUEUE = 5;

        public RadioVoxController()
        {
            shutdown = false;
            uint thread_id = 0;
#if TODO
            hthread = CreateThread(0, 4096, VoxUpdateProc, (LPVOID)this, 0, &thread_id);
#endif
          // TODO  throw new NotImplementedException();
        }

#if TODO
        ~RadioVoxController()
        {
            shutdown = true;

            WaitForSingleObject(hthread, 500);
            CloseHandle(hthread);
            hthread = null;

            queue.destroy();
        }
#endif


        public bool Add(RadioVox vox)
        {
            if (vox == null || vox.sounds.IsEmpty())
                return false;
#if TODO

            AutoThreadSync a(sync);
#endif
            if (queue.Count < MAX_QUEUE)
            {
                queue.Enqueue(vox);
                return true;
            }

            return false;
        }

        public void Update()
        {
#if TODO
            AutoThreadSync a(sync);
#endif

            if (queue.Count > 0)
            {
                RadioVox vox = queue.Peek();

                if (!vox.Update())
                {
                    vox = queue.Dequeue();
                    //delete vox;
                }
            }
        }

        public uint UpdateThread()
        {
#if TODO
            while (!shutdown)
            {
                Update();
                Sleep(50);
            }

            return 0;
#endif
            throw new NotImplementedException();
        }

        public void VoxUpdateProc(object link)
        {
#if TODO
            RadioVoxController controller = (RadioVoxController)link;

            if (controller !=null)
                return controller.UpdateThread();

            return (DWORD)E_POINTER;
#endif
            throw new NotImplementedException();
        }


        public bool shutdown;
        public Thread hthread;
        public Queue<RadioVox> queue = new Queue<RadioVox>();
        public object sync;
        public static RadioVoxController controller = null;
    }

}