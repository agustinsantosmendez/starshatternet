﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Drone.h/Drone.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Decoy / Weapons Drone class
*/
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using System;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
namespace StarshatterNet.Stars
{
#warning Drone class is still in development and is not recommended for production.
    public class Drone: Shot
    {
        public Drone(Point pos, CameraNode shot_cam, WeaponDesign dsn, Ship ship = null)
            : base(pos, shot_cam, dsn, ship)
        {
            obj_type = (int)SimObject.TYPES.SIM_DRONE;

            if (dsn != null)
            {
                decoy_type = dsn.decoy_type;
                probe = dsn.probe;
                integrity = dsn.integrity;
                 name  = string.Format("Drone {0:4}", Identity());
            }
         }
        // ~Drone();

        public override void SeekTarget(SimObject target, ShipSystem sub =null)
        {
            if (!probe)
                base.SeekTarget(target, sub);
        }

        public override void ExecFrame(double factor)
        {
            base.ExecFrame(factor);
        }

        public override bool IsDrone() { return true; }
        public override bool IsDecoy() { return decoy_type != 0; }
        public override bool IsProbe() { return probe  ; }

        public override void Disarm()
        {
            base.Disarm();
        }
        public override void Destroy()
        {
            base.Destroy();
        }

        // SENSORS AND VISIBILITY:
        public override double PCS()
        {
            if (decoy_type == 0 && !probe)
                return 10e3;

            return 0;
        }
        public override double ACS()
        {
            if (decoy_type == 0 && !probe)
                return 1e3;

            return 0;
        }
        public virtual string ClassName()
        {
            return Ship.ClassName((CLASSIFICATION)decoy_type);
        }
        public virtual int Class()
        {
            return decoy_type;
        }

        // DAMAGE RESOLUTION:
        public void SetLife(int seconds) { life = seconds; }
        public virtual int HitBy(Shot shot, ref Point impact)
        {
            if (life == 0 || !shot.IsArmed()) return 0;

            const int HIT_NOTHING = 0;
            const int HIT_HULL = 1;

            Point hull_impact;
            int hit_type = HIT_NOTHING;
            Point shot_loc = shot.Location();
            Point shot_org = shot.Origin();
            Point delta = shot_loc - Location();
            double dlen = delta.Length;
            double dscale = 1;
            float scale = design.explosion_scale;
            Sim sim = Sim.GetSim();

            if (scale <= 0)
                scale = design.scale;

            // MISSILE PROCESSING ------------------------------------------------

            if (shot.IsMissile())
            {
                if (dlen < 10 * Radius())
                {
                    hull_impact = impact = shot_loc;
                    sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.HULL_FLASH, 0.3f * scale, scale, region);
                    sim.CreateExplosion(impact, new Point(), Explosion.ExplosionType.SHOT_BLAST, 2.0f, scale, region);
                    hit_type = HIT_HULL;
                }
            }

            // ENERGY WEP PROCESSING ---------------------------------------------

            else
            {
                if (shot.IsBeam())
                {
                    // check right angle spherical distance:
                    Point d0 = Location() - shot_org;
                    Point w = shot_loc - shot_org; w.Normalize();
                    Point test = shot_org + w * (d0 * w);
                    Point d1 = test - Location();
                      dlen = d1.Length;          // distance of point from line

                    if (dlen < 2 * Radius())
                    {
                        hull_impact = impact = test;
                        shot.SetBeamPoints(shot_org, impact);
                        sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.BEAM_FLASH, 0.30f * scale, scale, region);
                        hit_type = HIT_HULL;
                    }
                }
                else if (dlen < 2 * Radius())
                {
                    hull_impact = impact = shot_loc;
                    sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.HULL_FLASH, 0.30f * scale, scale, region);
                    hit_type = HIT_HULL;
                }
            }

            // DAMAGE RESOLUTION -------------------------------------------------

            if (hit_type != HIT_NOTHING)
            {
                double effective_damage = shot.Damage() * dscale;

                if (shot.IsBeam())
                {
                    effective_damage *= SimulationTime.FrameTime();
                }
                else
                {
                    ApplyTorque(shot.Velocity() * (float)effective_damage * 1e-6f);
                }

                if (effective_damage > 0)
                    base.InflictDamage(effective_damage);
            }

            return hit_type;
        }



        //TODO warning protected int iff_code;
        protected int decoy_type;
        protected bool probe;
    }
}