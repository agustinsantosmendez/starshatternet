﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      LandingGear.h/LandingGear.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Fighter undercarriage (landing gear) system class
*/
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Config;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Audio;
using System;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Gen.Graphics;
using UnityEngine;

namespace StarshatterNet.Stars.SimElements
{
    public class LandingGear : ShipSystem
    {
        public const int MAX_GEAR = 4;
        public enum GEAR_STATE { GEAR_UP, GEAR_LOWER, GEAR_DOWN, GEAR_RAISE };

        public LandingGear() : base(CATEGORY.MISC_SYSTEM, 0, "Landing Gear", 1, 1, 1, 1)
        {
            state = GEAR_STATE.GEAR_UP; transit = 0; ngear = 0; clearance = 0;
            name = Game.GetText("sys.landing-gear");
            abrv = Game.GetText("sys.landing-gear.abrv");

            for (int i = 0; i < MAX_GEAR; i++)
            {
                models[i] = null;
                gear[i] = null;
            }
        }

        public LandingGear(LandingGear g) : base(g)
        {
            state = GEAR_STATE.GEAR_UP; transit = 0; ngear = g.ngear; clearance = 0;
            Mount(g);
            SetAbbreviation(g.Abbreviation());
            int i;

            for (i = 0; i < ngear; i++)
            {
                models[i] = null;
                gear[i] = new Solid();
                start[i] = g.start[i];
                end[i] = g.end[i];

                gear[i].UseModel(g.models[i]);

                if (clearance > end[i].Y)
                    clearance = end[i].Y;
            }

            while (i < MAX_GEAR)
            {
                models[i] = null;
                gear[i] = null;
                i++;
            }

            clearance += mount_rel.Y;
        }

        // ~LandingGear();

        public virtual int AddGear(Model m, Point s, Point e)
        {
            if (ngear < MAX_GEAR)
            {
                models[ngear] = m;
                start[ngear] = s;
                end[ngear] = e;

                ngear++;
            }

            return ngear;
        }

        public override void ExecFrame(double seconds)
        {
            base.ExecFrame(seconds);
             switch (state)
            {
                case GEAR_STATE.GEAR_UP:
                    transit = 0;
                    break;

                case GEAR_STATE.GEAR_DOWN:
                    transit = 1;
                    break;

                case GEAR_STATE.GEAR_LOWER:
                    if (transit < 1)
                    {
                        transit += seconds;

                        Scene s = null;
                        if (ship != null && ship.Rep() != null)
                            s = ship.Rep().GetScene();

                        if (s != null)
                        {
                            for (int i = 0; i < ngear; i++)
                            {
                                if (gear[i] != null && gear[i].GetScene() == null)
                                {
                                    s.AddGraphic(gear[i]);
                                }
                            }
                        }
                    }
                    else
                    {
                        transit = 1;
                        state = GEAR_STATE.GEAR_DOWN;
                    }
                    break;

                case GEAR_STATE.GEAR_RAISE:
                    if (transit > 0)
                    {
                        transit -= seconds;
                    }
                    else
                    {
                        transit = 0;
                        state = GEAR_STATE.GEAR_UP;

                        for (int i = 0; i < ngear; i++)
                        {
                            if (gear[i] != null)
                            {
                                Scene s = gear[i].GetScene();
#if TODO
                                if (s != null) s.DelGraphic(gear[i]);
#endif 
                                throw new NotImplementedException();
                            }
                        }
                    }
                    break;
            }
         }

        public override void Orient(Physical rep)
        {
            base.Orient(rep);

            Matrix33D orientation = rep.Cam().Orientation();
            Point ship_loc = rep.Location();

            for (int i = 0; i < ngear; i++)
            {
                Point gloc;
                if (transit < 1) gloc = start[i] + (end[i] - start[i]) * (float)transit;
                else gloc = end[i];

                Point projector = (orientation * gloc) + ship_loc; // TODO matrix * vector multiplication order
                if (gear[i] != null)
                {
                    gear[i].MoveTo(projector);
                    gear[i].SetOrientation(orientation);
                }
            }
        }


        public GEAR_STATE GetState() { return state; }
        public void SetState(GEAR_STATE s)
        {
            if (state != s)
            {
                state = s;

                if (ship != null && ship == Sim.GetSim().GetPlayerShip())
                {
                    if (state == GEAR_STATE.GEAR_LOWER || state == GEAR_STATE.GEAR_RAISE)
                    {
                        if (gear_transit_sound != null)
                        {
                            Sound sound = gear_transit_sound.Duplicate();
                            sound.SetVolume(AudioConfig.EfxVolume());
                            sound.Play();
                        }
                    }
                }
            }
        }

        public int NumGear() { return ngear; }
        public Solid GetGear(int index)
        {
            if (index >= 0 && index < ngear)
            {
                Solid g = gear[index];
                return g;
            }

            return null;
        }

        public Point GetGearStop(int index)
        {
            if (index >= 0 && index < ngear)
            {
                Solid g = gear[index];

                if (g != null)
                    return g.Location();
            }

            return new Point();
        }
        public double GetTouchDown()
        {
            double down = 0;

            if (ship != null)
            {
                down = ship.Location().Y;

                if (state != GEAR_STATE.GEAR_UP)
                {
                    for (int i = 0; i < ngear; i++)
                    {
                        if (gear[i] != null)
                        {
                            Point stop = gear[i].Location();

                            if (stop.Y < down)
                                down = stop.Y;
                        }
                    }
                }
            }

            return down;
        }
        public double GetClearance() { return clearance; }

        public static void Initialize()
        {
            if (gear_transit_sound == null)
            {
                gear_transit_sound =   Sound.CreateStream("Sounds/GearTransit");
            }
        }

        public static void Close()
        {
            //delete gear_transit_sound;
            gear_transit_sound = null;
        }



        protected GEAR_STATE state;
        protected double transit;
        protected double clearance;

        protected int ngear;
        protected Model[] models = new Model[MAX_GEAR];
        protected Solid[] gear = new Solid[MAX_GEAR];
        protected Point[] start = new Point[MAX_GEAR];
        protected Point[] end = new Point[MAX_GEAR];

        protected static Sound gear_transit_sound = null;
    }
}