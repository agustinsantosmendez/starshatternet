﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Drive.h/Drive.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Conventional Drive (system) class
 */
using System;
using System.Collections.Generic;
using Assets.StarshatterNet.Stars.Graphics.General;
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Audio;
using StarshatterNet.Config;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.SimElements
{
    public class DrivePort
    {
        public DrivePort(Point l, float s) { loc = l; flare = null; trail = null; scale = s; }
        //public ~DrivePort();

        public Point loc;
        public float scale;

        public DriveSprite flare;
        public Bolt trail;
    }

    public class Drive : ShipSystem
    {
        public enum SUBTYPE { PLASMA, FUSION, GREEN, RED, BLUE, YELLOW, STEALTH };
        public const int MAX_ENGINES = 16;

        public Drive(SUBTYPE drive_type, float max_thrust, float max_aug, bool show_trail = true)
            : base(CATEGORY.DRIVE, (int)drive_type, "Drive", drive_value[(int)drive_type], max_thrust * 2, max_thrust * 2, max_thrust * 2)
        {
            thrust = max_thrust;
            augmenter = max_aug;
            scale = 0.0f;
            throttle = 0.0f;
            augmenter_throttle = 0.0f;
            intensity = 0.0f;
            sound = null;
            burner_sound = null;
            this.show_trail = show_trail;
            power_flags = POWER_FLAGS.POWER_WATTS;

            switch ((SUBTYPE)drive_type)
            {
                default:
                case SUBTYPE.PLASMA: name = Game.GetText("sys.drive.plasma"); break;
                case SUBTYPE.FUSION: name = Game.GetText("sys.drive.fusion"); break;
                case SUBTYPE.GREEN: name = Game.GetText("sys.drive.green"); break;
                case SUBTYPE.RED: name = Game.GetText("sys.drive.red"); break;
                case SUBTYPE.BLUE: name = Game.GetText("sys.drive.blue"); break;
                case SUBTYPE.YELLOW: name = Game.GetText("sys.drive.yellow"); break;
                case SUBTYPE.STEALTH: name = Game.GetText("sys.drive.stealth"); break;
            }

            abrv = Game.GetText("sys.drive.abrv");

            emcon_power[0] = 0;
            emcon_power[1] = 50;
            emcon_power[2] = 100;
        }

        public Drive(Drive d) : base(d)
        {
            thrust = d.thrust;
            augmenter = d.augmenter;
            scale = d.scale;
            throttle = 0.0f;
            augmenter_throttle = 0.0f;
            intensity = 0.0f;
            sound = null;
            burner_sound = null;
            show_trail = d.show_trail;
            power_flags = POWER_FLAGS.POWER_WATTS;

            Mount(d);

            if ((SUBTYPE)subtype != SUBTYPE.STEALTH)
            {
                for (int i = 0; i < d.ports.Count; i++)
                {
                    DrivePort p = d.ports[i];
                    CreatePort(p.loc, p.scale);
                }
            }
        }

        ~Drive()
        {
            if (sound != null)
            {
                sound.Stop();
                sound.Release();
                sound = null;
            }

            if (burner_sound != null)
            {
                burner_sound.Stop();
                burner_sound.Release();
                burner_sound = null;
            }

            ports.Clear();
        }


        private static bool initialized = false;
        public static void Initialize()
        {
            if (initialized) return;

            string path = "Drive/";
            drive_flare_bitmap[0] = new Bitmap(path + "Drive0");
            drive_flare_bitmap[1] = new Bitmap(path + "Drive1");
            drive_flare_bitmap[2] = new Bitmap(path + "Drive2");
            drive_flare_bitmap[3] = new Bitmap(path + "Drive3");
            drive_flare_bitmap[4] = new Bitmap(path + "Drive4");
            drive_flare_bitmap[5] = new Bitmap(path + "Drive5");

            drive_trail_bitmap[0] = new Bitmap(path + "Trail0");
            drive_trail_bitmap[1] = new Bitmap(path + "Trail1");
            drive_trail_bitmap[2] = new Bitmap(path + "Trail2");
            drive_trail_bitmap[3] = new Bitmap(path + "Trail3");
            drive_trail_bitmap[4] = new Bitmap(path + "Trail4");
            drive_trail_bitmap[5] = new Bitmap(path + "Trail5");

            drive_glow_bitmap[0] = new Bitmap(path + "Glow0");
            drive_glow_bitmap[1] = new Bitmap(path + "Glow1");
            drive_glow_bitmap[2] = new Bitmap(path + "Glow2");
            drive_glow_bitmap[3] = new Bitmap(path + "Glow3");
            drive_glow_bitmap[4] = new Bitmap(path + "Glow4");
            drive_glow_bitmap[5] = new Bitmap(path + "Glow5");

            const Sound.FlagEnum SOUND_FLAGS = Sound.FlagEnum.LOCALIZED |
                                                Sound.FlagEnum.LOC_3D |
                                                Sound.FlagEnum.LOOP |
                                                Sound.FlagEnum.LOCKED;

            path = "Sounds/";
            sound_resource[0] = Sound.CreateStream(path + "engine", SOUND_FLAGS);
            sound_resource[1] = Sound.CreateStream(path + "burner2", SOUND_FLAGS);
            sound_resource[2] = Sound.CreateStream(path + "rumble", SOUND_FLAGS);

            if (sound_resource[0] != null)
                sound_resource[0].SetMaxDistance(30.0e3f);

            if (sound_resource[1] != null)
                sound_resource[1].SetMaxDistance(30.0e3f);

            if (sound_resource[2] != null)
                sound_resource[2].SetMaxDistance(50.0e3f);

            initialized = true;
        }

        public static void Close()
        {
            for (int i = 0; i < 3; i++)
            {
                //delete sound_resource[i];
                sound_resource[i] = null;
            }
        }

        public static void StartFrame()
        {
        }

        public float Thrust(double seconds)
        {
            drive_seconds = seconds;

            float eff = (energy / capacity) * availability * 100.0f;
            float output = throttle * thrust * eff;
            bool aug_on = IsAugmenterOn();

            if (aug_on)
            {
                output += augmenter * augmenter_throttle * eff;

                // augmenter burns extra fuel:
                PowerSource reac = ship.Reactors()[source_index];
                reac.SetCapacity(reac.GetCapacity() - (0.1 * drive_seconds));
            }

            energy = 0.0f;

            if (output < 0 || GetPowerLevel() < 0.01)
                output = 0.0f;

            int vol = -10000;
            int vol_aug = -10000;
            double fraction = output / thrust;

            for (int i = 0; i < ports.Count; i++)
            {
                DrivePort p = ports[i];

                if (p.flare != null)
                {
                    if (i == 0)
                    {
                        if (fraction > 0)
                            intensity += (float)seconds;
                        else
                            intensity -= (float)seconds;

                        // capture volume based on actual output:
                        MathHelper.Clamp(intensity, 0.0f, 1.0f);

                        if (intensity > 0.25)
                        {
                            vol = (int)((intensity - 1.0) * 10000.0);
                            MathHelper.Clamp(vol, -10000, -1500);

                            if (aug_on && intensity > 0.5)
                            {
                                vol_aug = (int)((5 * augmenter_throttle - 1.0) * 10000.0);
                                MathHelper.Clamp(vol_aug, -10000, -1000);
                            }
                        }
                    }

                    p.flare.SetShade(intensity);
                }

                if (p.trail != null)
                {
                    p.trail.SetShade(intensity);
                }
            }

            CameraDirector cam_dir = CameraDirector.GetInstance();

            // no sound when paused!
            if (!SimulationTime.IsPaused && (SUBTYPE)subtype != SUBTYPE.STEALTH && cam_dir != null && cam_dir.GetCamera() != null)
            {
                if (ship != null && ship.GetRegion() == Sim.GetSim().GetActiveRegion())
                {
                    if (sound == null)
                    {
                        int sound_index = 0;
                        if (thrust > 100)
                            sound_index = 2;

                        if (sound_resource[sound_index] != null)
                            sound = sound_resource[sound_index].Duplicate();
                    }

                    if (aug_on && burner_sound == null)
                    {
                        if (sound_resource[1] != null)
                            burner_sound = sound_resource[1].Duplicate();
                    }

                    Point cam_loc = cam_dir.GetCamera().Pos();
                    double dist = (ship.Location() - cam_loc).Length;

                    if (sound != null && dist < sound.GetMaxDistance())
                    {
                        int max_vol = AudioConfig.EfxVolume();

                        if (vol > max_vol)
                            vol = max_vol;

                        if (sound != null)
                        {
                            sound.SetLocation(ship.Location());
                            sound.SetVolume(vol);
                            sound.Play();
                        }

                        if (burner_sound != null)
                        {
                            if (vol_aug > max_vol)
                                vol_aug = max_vol;

                            burner_sound.SetLocation(ship.Location());
                            burner_sound.SetVolume(vol_aug);
                            burner_sound.Play();
                        }
                    }
                    else
                    {
                        if (sound != null && sound.IsPlaying())
                            sound.Stop();

                        if (burner_sound != null && burner_sound.IsPlaying())
                            burner_sound.Stop();
                    }
                }
                else
                {
                    if (sound != null && sound.IsPlaying())
                        sound.Stop();

                    if (burner_sound != null && burner_sound.IsPlaying())
                        burner_sound.Stop();
                }
            }

            return output;
        }

        public float MaxThrust() { return thrust; }
        public float MaxAugmenter() { return augmenter; }
        public int NumEngines()
        {
            return ports.Count;
        }

        public DriveSprite GetFlare(int port)
        {
            if (port >= 0 && port < ports.Count)
            {
                return ports[port].flare;
            }

            return null;
        }

        public Bolt GetTrail(int port)
        {
            if (port >= 0 && port < ports.Count)
            {
                return ports[port].trail;
            }

            return null;
        }

        public bool IsAugmenterOn()
        {
            return augmenter > 0 &&
            augmenter_throttle > 0.05 &&
            IsPowerOn() &&
            status > STATUS.CRITICAL;
        }



        public virtual void AddPort(Point loc, float flare_scale = 0)
        {
            if (flare_scale == 0) flare_scale = scale;
            DrivePort port = new DrivePort(loc, flare_scale);
            ports.Add(port);
        }

        public virtual void CreatePort(Point loc, float flare_scale)
        {
            Bitmap flare_bmp = drive_flare_bitmap[subtype];
            Bitmap trail_bmp = drive_trail_bitmap[subtype];
            Bitmap glow_bmp = null;

            if (flare_scale <= 0)
                flare_scale = scale;

            if (augmenter <= 0)
                glow_bmp = drive_glow_bitmap[subtype];

            if ((SUBTYPE)subtype != SUBTYPE.STEALTH && flare_scale > 0)
            {
                DrivePort port = new DrivePort(loc, flare_scale);

                if (flare_bmp != null)
                {
                    DriveSprite flare_rep = new DriveSprite(flare_bmp, glow_bmp);
                    flare_rep.Scale(flare_scale * 1.5);
                    flare_rep.SetShade(0);
                    port.flare = flare_rep;
                }

                if (trail_bmp != null && show_trail)
                {
                    Bolt trail_rep = new Bolt(flare_scale * 30, flare_scale * 8, trail_bmp, true);
                    port.trail = trail_rep;
                }

                ports.Add(port);
            }
        }

        public override void Orient(Physical rep)
        {
            base.Orient(rep);

            Matrix33D orientation = rep.Cam().Orientation();
            Point ship_loc = rep.Location();

            for (int i = 0; i < ports.Count; i++)
            {
                DrivePort p = ports[i];

                Point projector = (orientation * p.loc) + ship_loc; // TODO matrix * vector multiplication order

                if (p.flare != null)
                {
                    p.flare.MoveTo(projector);
                    p.flare.SetFront(rep.Cam().vpn() * -10 * p.scale);
                }

                if (p.trail != null)
                {
                    if (intensity > 0.5)
                    {
                        double len = -60 * p.scale * intensity;

                        if (augmenter > 0 && augmenter_throttle > 0)
                            len += len * augmenter_throttle;

                        p.trail.Show();
                        p.trail.SetEndPoints(projector, projector + rep.Cam().vpn() * (float)len);
                    }
                    else
                    {
                        p.trail.Hide();
                    }
                }
            }
        }
        private static double drive_seconds = 0;

        public void SetThrottle(double t, bool aug = false)
        {
            double spool = 1.2 * drive_seconds;
            double throttle_request = t / 100;

            if (throttle < throttle_request)
            {
                if (throttle_request - throttle < spool)
                {
                    throttle = (float)throttle_request;
                }
                else
                {
                    throttle += (float)spool;
                }
            }

            else if (throttle > throttle_request)
            {
                if (throttle - throttle_request < spool)
                {
                    throttle = (float)throttle_request;
                }
                else
                {
                    throttle -= (float)spool;
                }
            }

            if (throttle < 0.5)
                aug = false;

            if (aug && augmenter_throttle < 1)
            {
                augmenter_throttle += (float)spool;

                if (augmenter_throttle > 1)
                    augmenter_throttle = 1.0f;
            }
            else if (!aug && augmenter_throttle > 0)
            {
                augmenter_throttle -= (float)spool;

                if (augmenter_throttle < 0)
                    augmenter_throttle = 0.0f;
            }
        }

        public override double GetRequest(double seconds)
        {
            if (!power_on) return 0;

            double t_factor = Math.Max(throttle + 0.5 * augmenter_throttle, 0.3);

            return t_factor * power_level * sink_rate * seconds;
        }



        protected float thrust;
        protected float augmenter;
        protected float scale;
        protected float throttle;
        protected float augmenter_throttle;
        protected float intensity;

        protected List<DrivePort> ports = new List<DrivePort>();

        protected Sound sound;
        protected Sound burner_sound;
        protected bool show_trail;

        public static Bitmap[] drive_flare_bitmap = new Bitmap[8];
        public static Bitmap[] drive_trail_bitmap = new Bitmap[8];
        public static Bitmap[] drive_glow_bitmap = new Bitmap[8];


        private static int[] drive_value = new int[]{
            1, 1, 1, 1, 1, 1, 1, 1
        };
        private static Sound[] sound_resource = new Sound[] { null, null, null };
    }
}