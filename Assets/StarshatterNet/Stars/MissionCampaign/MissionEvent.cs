﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MissionEvent.h/MissionEvent.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Events for mission scripting
*/
using System;
using StarshatterNet.Audio;
using StarshatterNet.Config;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.StarSystems;
using StarshatterNet.Stars.Views;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning MissionEvent class is still in development and is not recommended for production.
    public class MissionEvent : ICloneable
    {
        public enum EVENT_TYPE
        {
            MESSAGE,
            OBJECTIVE,
            INSTRUCTION,
            IFF,
            DAMAGE,
            JUMP,
            HOLD,
            SKIP,
            END_MISSION,

            BEGIN_SCENE,
            CAMERA,
            VOLUME,
            DISPLAY,
            FIRE_WEAPON,
            END_SCENE,

            NUM_EVENTS
        };

        public enum EVENT_STATUS
        {
            PENDING, ACTIVE, COMPLETE, SKIPPED
        };

        public enum EVENT_TRIGGER
        {
            TRIGGER_TIME, TRIGGER_DAMAGE, TRIGGER_DESTROYED,
            TRIGGER_JUMP, TRIGGER_LAUNCH, TRIGGER_DOCK,
            TRIGGER_NAVPT, TRIGGER_EVENT, TRIGGER_SKIPPED,
            TRIGGER_TARGET, TRIGGER_SHIPS_LEFT, TRIGGER_DETECT,
            TRIGGER_RANGE, TRIGGER_EVENT_ALL, TRIGGER_EVENT_ANY,
            NUM_TRIGGERS
        };

        public MissionEvent()
        {
            id = 0; status = EVENT_STATUS.PENDING; time = 0; delay = 0; evnt = 0; event_nparams = 0;
            event_chance = 100; trigger = 0; trigger_nparams = 0; sound = null;
        }
        // ~MissionEvent();

        // operations:
        public void ExecFrame(double seconds)
        {
            Sim sim = Sim.GetSim();

            if (sim == null)
            {
                status = EVENT_STATUS.PENDING;
                return;
            }

            if (status == EVENT_STATUS.PENDING)
                CheckTrigger();

            if (status == EVENT_STATUS.ACTIVE)
            {
                if (delay > 0)
                    delay -= seconds;

                else
                    Execute();
            }
        }
        public void Activate()
        {
            if (status == EVENT_STATUS.PENDING)
            {
                if (event_chance > 0 && event_chance < 100)
                {
                    if (RandomHelper.Random(0, 100) < event_chance)
                        status = EVENT_STATUS.ACTIVE;
                    else
                        status = EVENT_STATUS.SKIPPED;
                }

                else
                {
                    status = EVENT_STATUS.ACTIVE;
                }

                if (status == EVENT_STATUS.SKIPPED)
                {
                    Sim.GetSim().ProcessEventTrigger(EVENT_TRIGGER.TRIGGER_SKIPPED, id);
                }
            }
        }

        public virtual bool CheckTrigger()
        {
            Sim sim = Sim.GetSim();

            if (time > 0 && time > sim.MissionClock())
                return false;

            switch (trigger)
            {
                case EVENT_TRIGGER.TRIGGER_TIME:
                    {
                        if (time <= sim.MissionClock())
                            Activate();
                    }
                    break;

                case EVENT_TRIGGER.TRIGGER_DAMAGE:
                    {
                        Ship ship = sim.FindShip(trigger_ship);
                        if (ship != null)
                        {
                            double damage = 100.0 * (ship.Design().integrity - ship.Integrity()) /
                            (ship.Design().integrity);

                            if (damage >= trigger_param[0])
                                Activate();
                        }
                    }
                    break;

                case EVENT_TRIGGER.TRIGGER_DETECT:
                    {
                        Ship ship = sim.FindShip(trigger_ship);
                        Ship tgt = sim.FindShip(trigger_target);

                        if (ship != null && tgt != null)
                        {
                            if (ship.FindContact(tgt) != null)
                                Activate();
                        }
                        else
                        {
                            Skip();
                        }
                    }
                    break;

                case EVENT_TRIGGER.TRIGGER_RANGE:
                    {
                        Ship ship = sim.FindShip(trigger_ship);
                        Ship tgt = sim.FindShip(trigger_target);

                        if (ship != null && tgt != null)
                        {
                            double range = (ship.Location() - tgt.Location()).Length;
                            double min_range = 0;
                            double max_range = 1e12;

                            if (trigger_param[0] > 0)
                                min_range = trigger_param[0];
                            else
                                max_range = -trigger_param[0];

                            if (range < min_range || range > max_range)
                                Activate();
                        }
                        else
                        {
                            Skip();
                        }
                    }
                    break;

                case EVENT_TRIGGER.TRIGGER_SHIPS_LEFT:
                    {
                        int alive = 0;
                        int count = 0;
                        int iff = -1;
                        int nparams = NumTriggerParams();

                        if (nparams > 0) count = TriggerParam(0);
                        if (nparams > 1) iff = TriggerParam(1);

                        foreach (SimRegion rgn in sim.GetRegions())
                        {
                            foreach (Ship ship in rgn.Ships())
                            {
                                if (ship.Type() >= (int)CLASSIFICATION.STATION)
                                    continue;

                                if (ship.Life() == 0 && ship.RespawnCount() < 1)
                                    continue;

                                if (iff < 0 || ship.GetIFF() == iff)
                                    alive++;
                            }
                        }

                        if (alive <= count)
                            Activate();
                    }
                    break;

                case EVENT_TRIGGER.TRIGGER_EVENT_ALL:
                    {
                        bool all = true;
                        int nparams = NumTriggerParams();
                        for (int i = 0; all && i < nparams; i++)
                        {
                            int trigger_id = TriggerParam(i);

                            foreach (MissionEvent e in sim.GetEvents())
                            {
                                if (e.EventID() == trigger_id)
                                {
                                    if (e.Status() != EVENT_STATUS.COMPLETE)
                                        all = false;
                                    break;
                                }

                                else if (e.EventID() == -trigger_id)
                                {
                                    if (e.Status() == EVENT_STATUS.COMPLETE)
                                        all = false;
                                    break;
                                }
                            }
                        }

                        if (all)
                            Activate();
                    }
                    break;

                case EVENT_TRIGGER.TRIGGER_EVENT_ANY:
                    {
                        bool any = false;
                        int nparams = NumTriggerParams();
                        for (int i = 0; !any && i < nparams; i++)
                        {
                            int trigger_id = TriggerParam(i);

                            foreach (MissionEvent e in sim.GetEvents())
                            {
                                if (e.EventID() == trigger_id)
                                {
                                    if (e.Status() == EVENT_STATUS.COMPLETE)
                                        any = true;
                                    break;
                                }
                            }
                        }

                        if (any)
                            Activate();
                    }
                    break;
            }

            return status == EVENT_STATUS.ACTIVE;
        }
        public virtual void Execute(bool silent = false)
        {
            Starshatter stars = Starshatter.GetInstance();
            HUDView hud = HUDView.GetInstance();
            Sim sim = Sim.GetSim();
            Ship player = sim.GetPlayerShip();
            Ship ship = null;
            Ship src = null;
            Ship tgt = null;
            Element elem = null;
            int pan = 0;
            bool end_mission = false;
 
            if (event_ship.Length > 0)
                ship = sim.FindShip(event_ship);
            else
                ship = player;

            if (event_source.Length > 0)
                src = sim.FindShip(event_source);

            if (event_target.Length > 0)
                tgt = sim.FindShip(event_target);

            if (ship != null)
                elem = ship.GetElement();

            else if (event_ship.Length > 0)
            {
                elem = sim.FindElement(event_ship);

                if (elem != null)
                    ship = elem.GetShip(1);
            }

            // expire the delay, if any remains
            delay = 0;

            // fire the event action
            switch (evnt)
            {
                case EVENT_TYPE.MESSAGE:
                    if (event_message.Length > 0)
                    {
                        if (ship != null)
                        {
                            RadioMessage msg = new RadioMessage(ship, src, (RadioMessage.ACTION)event_param[0]);
                            msg.SetInfo(event_message);
                            msg.SetChannel(ship.GetIFF());
                            if (tgt != null)
                                msg.AddTarget(tgt);
                            RadioTraffic.Transmit(msg);
                        }

                        else if (elem != null)
                        {
                            RadioMessage msg = new RadioMessage(elem, src, (RadioMessage.ACTION)event_param[0]);
                            msg.SetInfo(event_message);
                            msg.SetChannel(elem.GetIFF());
                            if (tgt != null)
                                msg.AddTarget(tgt);
                            RadioTraffic.Transmit(msg);
                        }
                    }

                    if (event_sound.Length > 0)
                    {
                        pan = event_param[0];
                    }
                    break;

                case EVENT_TYPE.OBJECTIVE:
                    if (elem != null)
                    {
                        if (event_param[0] != 0)
                        {
                            elem.ClearInstructions();
                            elem.ClearObjectives();
                        }

                        Instruction obj = new Instruction((Instruction.ACTION)event_param[0], null);
                        obj.SetTarget(event_target);
                        elem.AddObjective(obj);

                        if (elem.Contains(player))
                        {
                            hud = HUDView.GetInstance();

                            if (hud != null)
                                hud.ShowHUDInst();
                        }
                    }
                    break;

                case EVENT_TYPE.INSTRUCTION:
                    if (elem != null)
                    {
                        if (event_param[0] != 0)
                            elem.ClearInstructions();

                        elem.AddInstruction(event_message);

                        if (elem.Contains(player) && event_message.Length > 0)
                        {
                            hud = HUDView.GetInstance();

                            if (hud != null)
                                hud.ShowHUDInst();
                        }
                    }
                    break;

                case EVENT_TYPE.IFF:
                    if (elem != null)
                    {
                        elem.SetIFF(event_param[0]);
                    }

                    else if (ship != null)
                    {
                        ship.SetIFF(event_param[0]);
                    }
                    break;

                case EVENT_TYPE.DAMAGE:
                    if (ship != null)
                    {
                        ship.InflictDamage(event_param[0]);

                        if (ship.Integrity() < 1)
                        {
                            NetUtil.SendObjKill(ship, null, NetObjKill.KillType.KILL_MISC);
                            ship.DeathSpiral();
                            ErrLogger.PrintLine("    {0} Killed By Scripted Event {1} ({2})", ship.Name(), id, Sim.FormatGameTime());
                        }
                    }
                    else
                    {
                        ErrLogger.PrintLine("   EVENT {0}: Could not apply damage to ship '{1}' (not found).", id, event_ship);
                    }
                    break;

                case EVENT_TYPE.JUMP:
                    if (ship != null)
                    {
                        SimRegion rgn = sim.FindRegion(event_target);

                        if (rgn != null && ship.GetRegion() != rgn)
                        {
                            if (rgn.IsOrbital())
                            {
                                SimElements.QuantumDrive quantum_drive = ship.GetQuantumDrive();
                                if (quantum_drive != null)
                                {
                                    quantum_drive.SetDestination(rgn, new Point(0, 0, 0));
                                    quantum_drive.Engage(true); // request immediate jump
                                }

                                else if (ship.IsAirborne())
                                {
                                    ship.MakeOrbit();
                                }
                            }

                            else
                            {
                                ship.DropOrbit();
                            }
                        }

                    }
                    break;

                case EVENT_TYPE.HOLD:
                    if (elem != null)
                        elem.SetHoldTime(event_param[0]);
                    break;

                case EVENT_TYPE.SKIP:
                    {
                        for (int i = 0; i < event_nparams; i++)
                        {
                            int skip_id = event_param[i];

                            foreach (MissionEvent e in sim.GetEvents())
                            {
                                if (e.EventID() == skip_id)
                                {
                                    if (e.status != EVENT_STATUS.COMPLETE)
                                        e.status = EVENT_STATUS.SKIPPED;
                                }
                            }
                        }
                    }
                    break;

                case EVENT_TYPE.END_MISSION:
                    ErrLogger.PrintLine("    END MISSION By Scripted Event {0} ({1})", id, Sim.FormatGameTime());
                    end_mission = true;
                    break;

                //
                // NOTE: CUTSCENE EVENTS DO NOT APPLY IN MULTIPLAYER
                //
                case EVENT_TYPE.BEGIN_SCENE:
                    ErrLogger.PrintLine("    ------------------------------------");
                    ErrLogger.PrintLine("    Begin Cutscene '{0}'", event_message);
                    stars.BeginCutscene();
                    break;

                case EVENT_TYPE.END_SCENE:
                    ErrLogger.PrintLine("    End Cutscene '{0}'", event_message);
                    ErrLogger.PrintLine("    ------------------------------------");
                    stars.EndCutscene();
                    break;

                case EVENT_TYPE.CAMERA:
                    if (stars.InCutscene())
                    {
                        CameraDirector cam_dir = CameraDirector.GetInstance();

                        if (cam_dir.GetShip() == null)
                            cam_dir.SetShip(player);

                        switch (event_param[0])
                        {
                            case 1:
                                if (cam_dir.GetMode() != CameraDirector.CAM_MODE.MODE_COCKPIT)
                                    cam_dir.SetMode(CameraDirector.CAM_MODE.MODE_COCKPIT, event_rect.x);
                                break;

                            case 2:
                                if (cam_dir.GetMode() != CameraDirector.CAM_MODE.MODE_CHASE)
                                    cam_dir.SetMode(CameraDirector.CAM_MODE.MODE_CHASE, event_rect.x);
                                break;

                            case 3:
                                if (cam_dir.GetMode() != CameraDirector.CAM_MODE.MODE_ORBIT)
                                    cam_dir.SetMode(CameraDirector.CAM_MODE.MODE_ORBIT, event_rect.x);
                                break;

                            case 4:
                                if (cam_dir.GetMode() != CameraDirector.CAM_MODE.MODE_TARGET)
                                    cam_dir.SetMode(CameraDirector.CAM_MODE.MODE_TARGET, event_rect.x);
                                break;
                        }

                        if (event_target.Length > 0)
                        {
                            ErrLogger.PrintLine("Mission Event {0}: setting camera target to {1}", id, event_target);
                            Ship s_tgt = null;

                            if (event_target.IndexOf("body:") < 0)
                                s_tgt = sim.FindShip(event_target);

                            if (s_tgt != null)
                            {
                                ErrLogger.PrintLine("   found ship {0}", s_tgt.Name());
                                cam_dir.SetViewOrbital(null);

                                if (cam_dir.GetViewObject() != s_tgt)
                                {

                                    if (event_param[0] == 6)
                                    {
                                        s_tgt.DropCam(event_param[1], event_param[2]);
                                        cam_dir.SetShip(s_tgt);
                                        cam_dir.SetMode(CameraDirector.CAM_MODE.MODE_DROP, 0);
                                    }
                                    else
                                    {
                                        Ship cam_ship = cam_dir.GetShip();

                                        if (cam_ship != null && cam_ship.IsDropCam())
                                        {
                                            cam_ship.CompleteTransition();
                                        }

                                        if (cam_dir.GetShip() != sim.GetPlayerShip())
                                            cam_dir.SetShip(sim.GetPlayerShip());
                                        cam_dir.SetViewObject(s_tgt, true); // immediate, no transition
                                    }
                                }
                            }

                            else
                            {
                                string body_name = event_target;

                                if (body_name.StartsWith("body:"))
                                    body_name += 5;

                                Orbital orb = sim.FindOrbitalBody(body_name);

                                if (orb != null)
                                {
                                    ErrLogger.PrintLine("   found body {0}", orb.Name());
                                    cam_dir.SetViewOrbital(orb);
                                }
                            }
                        }

                        if (event_param[0] == 3)
                        {
                            cam_dir.SetOrbitPoint(event_point.X, event_point.Y, event_point.Z);
                        }

                        else if (event_param[0] == 5)
                        {
                            cam_dir.SetOrbitRates(event_point.X, event_point.Y, event_point.Z);
                        }
                    }
                    break;

                case EVENT_TYPE.VOLUME:
                    if (stars.InCutscene())
                    {
                        AudioConfig audio_cfg = AudioConfig.GetInstance();

                        audio_cfg.SetEfxVolume(event_param[0]);
                        audio_cfg.SetWrnVolume(event_param[0]);
                    }
                    break;

                case EVENT_TYPE.DISPLAY:
                    if (stars.InCutscene())
                    {
#if TODO
                        DisplayView disp_view = DisplayView.GetInstance();

                        if (disp_view != null)
                        {
                            Color color;
                            color.Set(event_param[0]);

                            if (event_message.Length > 0 && event_source.Length > 0)
                            {

                                if (event_message.Contains("$"))
                                {
                                    Campaign campaign = Campaign.GetCampaign();
                                    Player user = Player.GetCurrentPlayer();
                                    CombatGroup group = campaign.GetPlayerGroup();

                                    if (user != null)
                                    {
                                        event_message = FormatTextReplace(event_message, "$NAME", user.Name());
                                        event_message = FormatTextReplace(event_message, "$RANK", Player.RankName(user.Rank()));
                                    }

                                    if (group != null)
                                    {
                                        event_message = FormatTextReplace(event_message, "$GROUP", group.GetDescription());
                                    }

                                    if (event_message.Contains("$TIME"))
                                    {
                                        string timestr;
                                        FormatDayTime(timestr, campaign.GetTime(), true);
                                        event_message = FormatTextReplace(event_message, "$TIME", timestr);
                                    }
                                }

                                disp_view.AddText(event_message,
                                FontMgr.Find(event_source),
                                color,
                                event_rect,
                                event_point.y,
                                event_point.x,
                                event_point.z);

                            }

                            else if (event_target.Length > 0)
                            {
                                DataLoader loader = DataLoader.GetLoader();

                                if (loader)
                                {
                                    loader.SetDataPath(0);
                                    loader.LoadBitmap(event_target, image, 0, true);
                                }

                                if (image.Width() && image.Height())
                                    disp_view.AddImage(&image,
                                    color,
                                    Video.BLEND_ALPHA,
                                    event_rect,
                                    event_point.y,
                                    event_point.x,
                                    event_point.z);
                            }
                        }
#endif
                        throw new System.NotImplementedException();

                    }
                    break;

                case EVENT_TYPE.FIRE_WEAPON:
                    if (ship != null)
                    {
                        // fire single weapon:
                        if (event_param[0] >= 0)
                        {
                            ship.FireWeapon(event_param[0]);
                        }

                        // fire all weapons:
                        else
                        {
                            foreach (WeaponGroup g_iter in ship.Weapons())
                            {
                                foreach (SimElements.Weapon w in g_iter.GetWeapons())
                                {
                                    w.Fire();
                                }
                            }
                        }
                    }
                    break;

                default:
                    break;
            }

            sim.ProcessEventTrigger(EVENT_TRIGGER.TRIGGER_EVENT, id);
#if TODO

            if (!silent &&  sound ==null&& event_sound.Length)
            {
                DataLoader loader = DataLoader.GetLoader();
                bool use_fs = loader.IsFileSystemEnabled();
                DWORD flags = pan ? Sound.LOCKED | Sound.LOCALIZED :
                Sound.LOCKED | Sound.AMBIENT;

                loader.UseFileSystem(true);
                loader.SetDataPath("Sounds/");
                loader.LoadSound(event_sound, sound, flags);
                loader.SetDataPath(0);

                if ( sound == null)
                {
                    loader.SetDataPath("Mods/Sounds/");
                    loader.LoadSound(event_sound, sound, flags);
                    loader.SetDataPath(0);
                }

                if (sound == null)
                {
                    loader.LoadSound(event_sound, sound, flags);
                }

                loader.UseFileSystem(use_fs);

                // fire and forget:
                if (sound != null)
                {
                    if (sound.GetFlags() & Sound.STREAMED)
                    {
                        sound.SetFlags(flags | sound.GetFlags());
                        sound.SetVolume(AudioConfig.VoxVolume());
                        sound.Play();
                    }
                    else
                    {
                        sound.SetFlags(flags);
                        sound.SetVolume(AudioConfig.VoxVolume());
                        sound.SetPan(pan);
                        sound.SetFilename(event_sound);
                        sound.AddToSoundCard();
                        sound.Play();
                    }
                }
            }
#endif
            throw new System.NotImplementedException();

            status = EVENT_STATUS.COMPLETE;

            if (end_mission)
            {
#if TODO
                StarServer server = StarServer.GetInstance();

                if (stars != null)
                {
                    stars.EndMission();
                }

                else if (server)
                {
                    // end mission event uses event_target member
                    // to forward server to next mission in the chain:
                    if (event_target.Length > 0)
                        server.SetNextMission(event_target);

                    server.SetGameMode(StarServer.MENU_MODE);
                }
#endif
                throw new System.NotImplementedException();
            }
        }

        public virtual void Skip()
        {
            if (status == EVENT_STATUS.PENDING)
            {
                status = EVENT_STATUS.SKIPPED;
            }
        }

        // accessors:
        public int EventID() { return id; }
        public EVENT_STATUS Status() { return status; }
        public bool IsPending() { return status == EVENT_STATUS.PENDING; }
        public bool IsActive() { return status == EVENT_STATUS.ACTIVE; }
        public bool IsComplete() { return status == EVENT_STATUS.COMPLETE; }
        public bool IsSkipped() { return status == EVENT_STATUS.SKIPPED; }

        public double Time() { return time; }
        public double Delay() { return delay; }

        public EVENT_TYPE Event() { return evnt; }
        public string EventName()
        {
            return event_names[(int)evnt];
        }
        public string EventShip() { return event_ship; }
        public string EventSource() { return event_source; }
        public string EventTarget() { return event_target; }
        public string EventMessage() { return event_message; }
        public string EventSound() { return event_sound; }

        public int EventParam(int index = 0)
        {
            if (index >= 0 && index < NumEventParams())
                return event_param[index];

            return 0;
        }
        public int NumEventParams()
        {
            return event_nparams;
        }

        public int EventChance() { return event_chance; }
        public Point EventPoint() { return event_point; }
        public Rect EventRect() { return event_rect; }

        public EVENT_TRIGGER Trigger() { return trigger; }
        public string TriggerName() { throw new System.NotImplementedException(); }
        public string TriggerShip() { return trigger_ship; }
        public string TriggerTarget() { return trigger_target; }

        public string TriggerParamStr()
        {

            string result = "";
            string buffer;

            if (trigger_param[0] == 0)
            {
                // nothing
            }

            else if (trigger_param[1] == 0)
            {
                buffer = string.Format("{0}", trigger_param[0]);
                result = buffer;
            }

            else
            {
                result = "(";

                for (int i = 0; i < 8; i++)
                {
                    if (trigger_param[i] == 0)
                        break;

                    if (i < 7 && trigger_param[i + 1] != 0)
                        buffer = string.Format("{0}", trigger_param[i]);
                    else
                        buffer = string.Format("{0}", trigger_param[i]);

                    result += buffer;
                }

                result += ")";
            }

            return result;
        }
        public int TriggerParam(int index = 0)
        {
            if (index >= 0 && index < NumTriggerParams())
                return trigger_param[index];

            return 0;
        }
        public int NumTriggerParams()
        {
            return trigger_nparams;
        }

        public static string EventName(int n)
        {
            return event_names[n];
        }
        public static EVENT_TYPE EventForName(string n)
        {
            for (int i = 0; i < (int)EVENT_TYPE.NUM_EVENTS; i++)
                if (n == event_names[i])
                    return (EVENT_TYPE)i;

            return (EVENT_TYPE)0;
        }
        public static string TriggerName(int n)
        {
            return trigger_names[n];
        }
        public static EVENT_TRIGGER TriggerForName(string n)
        {
            for (int i = 0; i < (int)EVENT_TRIGGER.NUM_TRIGGERS; i++)
                if (n == trigger_names[i])
                    return (EVENT_TRIGGER)i;

            return (EVENT_TRIGGER)0;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        protected internal int id;
        protected EVENT_STATUS status;
        internal protected double time;
        internal protected double delay;

        internal protected EVENT_TYPE evnt;
        internal protected string event_ship;
        internal protected string event_source;
        internal protected string event_target;
        internal protected string event_message;
        internal protected string event_sound;
        internal protected int[] event_param = new int[10];
        internal protected int event_nparams;
        internal protected int event_chance;
        internal protected Point event_point;
        internal protected Rect event_rect;

        internal protected EVENT_TRIGGER trigger;
        internal protected string trigger_ship;
        internal protected string trigger_target;
        internal protected int[] trigger_param = new int[10];
        internal protected int trigger_nparams;

        protected Bitmap image;
        protected Sound sound;

        private static string[] event_names = {
                    "Message",
                    "Objective",
                    "Instruction",
                    "IFF",
                    "Damage",
                    "Jump",
                    "Hold",
                    "Skip",
                    "Exit",

                    "BeginScene",
                    "Camera",
                    "Volume",
                    "Display",
                    "Fire",
                    "EndScene"
                };

        private static string[] trigger_names = {
                    "Time",
                    "Damage",
                    "Destroyed",
                    "Jump",
                    "Launch",
                    "Dock",
                    "Navpoint",
                    "Event",
                    "Skipped",
                    "Target",
                    "Ships Left",
                    "Detect",
                    "Range",
                    "Event (ALL)",
                    "Event (ANY)"
                };
    }
}