﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      PlanScreen.h/PlanScreen.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

*/
using System;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class PlanScreen : BaseScreen
    {
        public PlanScreen()
        {
            camera = null; navdlg = null; award_dlg = null; debrief_dlg = null;
            objdlg = null; pkgdlg = null; wepdlg = null; isShown = false;
        }
        //public virtual ~PlanScreen();

        public virtual void Setup(Screen screen, Camera cam, GameObject gameObj)
        {
            if (cam == null)
                return;

            camera = cam;

            // create windows
            //loader.UseFileSystem(true);

            FormDef msn_obj_def = new FormDef("MsnObjDlg", 0);
            msn_obj_def.Load("MsnObjDlg");
            objdlg = new MsnObjDlg(screen, camera, msn_obj_def, this, gameObj);

            FormDef msn_pkg_def = new FormDef("MsnPkgDlg", 0);
            msn_pkg_def.Load("MsnPkgDlg");
            pkgdlg = new MsnPkgDlg(screen, camera, msn_pkg_def, this, gameObj);

            FormDef msn_nav_def = new FormDef("MsnNavDlg", 0);
            msn_nav_def.Load("MsnNavDlg");
            navdlg = new MsnNavDlg(screen, camera, msn_nav_def, this, gameObj);

            FormDef msn_wep_def = new FormDef("MsnWepDlg", 0);
            msn_wep_def.Load("MsnWepDlg");
            wepdlg = new MsnWepDlg(screen, camera, msn_wep_def, this, gameObj);

            FormDef award_def = new FormDef("AwardDlg", 0);
            award_def.Load("AwardDlg");
            award_dlg = new AwardDlg(screen, camera, award_def, this, gameObj);

            FormDef debrief_def = new FormDef("DebriefDlg", 0);
            debrief_def.Load("DebriefDlg");
            debrief_dlg = new DebriefDlg(screen, camera, debrief_def, this, gameObj);

            //loader.UseFileSystem(Starshatter.UseFileSystem());
            ShowMsnDlg();
        }
        public virtual void TearDown()
        {
            if (camera != null)
            {
                //camera.DelWindow(objdlg);
                //camera.DelWindow(pkgdlg);
                //camera.DelWindow(wepdlg);
                //camera.DelWindow(navdlg);
                //camera.DelWindow(debrief_dlg);
                //camera.DelWindow(award_dlg);
            }

            //delete objdlg;
            //delete pkgdlg;
            //delete wepdlg;
            //delete navdlg;
            //delete debrief_dlg;
            //delete award_dlg;

            objdlg = null;
            pkgdlg = null;
            wepdlg = null;
            navdlg = null;
            debrief_dlg = null;
            award_dlg = null;
            camera = null;
        }
        public virtual bool CloseTopmost()
        {
            if (debrief_dlg.IsShown())
            {
                debrief_dlg.OnClose(null, null);
            }

            if (award_dlg.IsShown())
            {
                return true;
            }

            return false;
        }


        public virtual bool IsShown() { return isShown; }
        public virtual void Show()
        {
            if (!isShown)
            {
                ShowMsnDlg();
                isShown = true;
            }
        }

        public virtual void Hide()
        {
            HideAll();
            isShown = false;
        }

        public virtual void ShowMsnDlg()
        {
            HideAll();
            Mouse.Show(true);
            objdlg.Show();
        }
        public virtual void HideMsnDlg()
        {
            HideAll();
            Mouse.Show(true);
            objdlg.Show();
        }

        public virtual bool IsMsnShown()
        {
            return IsMsnObjShown() || IsMsnPkgShown() || IsMsnWepShown();
        }

        public virtual void ShowMsnObjDlg()
        {
            HideAll();
            Mouse.Show(true);
            objdlg.Show();
        }

        public virtual void HideMsnObjDlg()
        {
            HideAll();
            Mouse.Show(true);
        }

        public virtual bool IsMsnObjShown()
        {
            return objdlg != null && objdlg.IsShown();
        }

        public virtual MsnObjDlg GetMsnObjDlg() { return objdlg; }

        public virtual void ShowMsnPkgDlg()
        {
            HideAll();
            Mouse.Show(true);
            pkgdlg.Show();
        }

        public virtual void HideMsnPkgDlg()
        {
            HideAll();
            Mouse.Show(true);
        }
        public virtual bool IsMsnPkgShown()
        {
            return pkgdlg != null && pkgdlg.IsShown();
        }

        public virtual MsnPkgDlg GetMsnPkgDlg() { return pkgdlg; }

        public virtual void ShowMsnWepDlg()
        {
            HideAll();
            Mouse.Show(true);
            wepdlg.Show();
        }
        public virtual void HideMsnWepDlg()
        {
            HideAll();
            Mouse.Show(true);
        }
        public virtual bool IsMsnWepShown()
        {
            return wepdlg != null && wepdlg.IsShown();
        }

        public virtual MsnWepDlg GetMsnWepDlg() { return wepdlg; }

        public override void ShowNavDlg()
        {
            if (navdlg != null && !navdlg.IsShown())
            {
                HideAll();
                Mouse.Show(true);
                navdlg.Show();
            }
        }
        public override void HideNavDlg()
        {
            if (navdlg != null && navdlg.IsShown())
            {
                HideAll();
                Mouse.Show(true);
            }
        }

        public override bool IsNavShown()
        {
            return navdlg != null && navdlg.IsShown();
        }
        public override NavDlg GetNavDlg() { return (NavDlg)navdlg; }

        public virtual void ShowDebriefDlg()
        {
            HideAll();
            Mouse.Show(true);
            debrief_dlg.Show();
        }
        public virtual void HideDebriefDlg()
        {
            HideAll();
            Mouse.Show(true);
        }
        public virtual bool IsDebriefShown()
        {
            return debrief_dlg != null && debrief_dlg.IsShown();
        }

        public virtual DebriefDlg GetDebriefDlg() { return debrief_dlg; }

        public virtual void ShowAwardDlg()
        {
            HideAll();
            Mouse.Show(true);
            award_dlg.Show();
        }
        public virtual void HideAwardDlg()
        {
            HideAll();
            Mouse.Show(true);
        }
        public virtual bool IsAwardShown()
        {
            return award_dlg != null && award_dlg.IsShown();
        }

        public virtual AwardDlg GetAwardDlg() { return award_dlg; }

        public virtual void ExecFrame()
        {
            Game.SetScreenColor(Color.black);

            Mission mission = null;
            Campaign campaign = Campaign.GetCampaign();

            if (campaign != null)
                mission = campaign.GetMission();

            if (navdlg != null)
            {
                navdlg.SetMission(mission);

                if (navdlg.IsShown())
                    navdlg.ExecFrame();
            }

            if (objdlg != null && objdlg.IsShown())
            {
                objdlg.ExecFrame();
            }

            if (pkgdlg != null && pkgdlg.IsShown())
            {
                pkgdlg.ExecFrame();
            }

            if (wepdlg != null && wepdlg.IsShown())
            {
                wepdlg.ExecFrame();
            }

            if (award_dlg != null && award_dlg.IsShown())
            {
                award_dlg.ExecFrame();
            }

            if (debrief_dlg != null && debrief_dlg.IsShown())
            {
                debrief_dlg.ExecFrame();
            }
        }

        public virtual void HideAll()
        {
            if (objdlg != null) objdlg.Hide();
            if (pkgdlg != null) pkgdlg.Hide();
            if (wepdlg != null) wepdlg.Hide();
            if (navdlg != null) navdlg.Hide();
            if (award_dlg != null) award_dlg.Hide();
            if (debrief_dlg != null) debrief_dlg.Hide();
        }



        private Camera camera;

        private MsnObjDlg objdlg;
        private MsnPkgDlg pkgdlg;
        private MsnWepDlg wepdlg;
        private MsnNavDlg navdlg;
        private DebriefDlg debrief_dlg;
        private AwardDlg award_dlg;

        //TODO private DataLoader loader;

        private int wc, hc;
        private bool isShown;
    }
}