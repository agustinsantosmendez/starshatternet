﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars.exe
    ORIGINAL FILE:      Galaxy.h/Galaxy.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Galaxy (list of star systems) for a single campaign.
*/
using StarshatterNet.Gen.Misc;
using System.Collections.Generic;

namespace StarshatterNet.Stars.StarSystems
{
    public class Galaxy
    {
        public Galaxy(string name)
        {
            this.name = name;
            radius = 10;
        }
        //public virtual ~Galaxy(){
        //    Print("  Destroying Galaxy %s\n", (const char*) name);
        //    systems.destroy();
        //    stars.destroy();
        //}

        public bool Equals(Galaxy other)
        {
            return this.name.Equals(other.name);
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Galaxy other = (Galaxy)obj;
            return this.name.Equals(other.name);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }

        public static bool operator ==(Galaxy l, Galaxy r)
        {
            if (object.ReferenceEquals(l, r))
            {
                return true;     // if both the same object, or both null
            }
            // If one is null, but not both, return false.
            if (((object)l == null) || ((object)r == null))
            {
                return false;
            }

            return l.name.Equals(r.name);
        }

        public static bool operator !=(Galaxy l, Galaxy r)
        {
            return !(l == r);
        }

        // operations:
        public virtual void Load()
        {
            Load("Galaxy/" + name);
#if TODO
            // load mod galaxies:
            List<Text> mod_galaxies;
            loader->SetDataPath("Mods/Galaxy/");
            loader->ListFiles("*.def", mod_galaxies);

            ListIter<Text> iter = mod_galaxies;
            while (++iter)
            {
                Text* name = iter.value();

                if (!name->contains("/"))
                {
                    loader->SetDataPath("Mods/Galaxy/");
                    Load(name->data());
                }
            }
#endif
        }
        public virtual void Load(string filename)
        {
            ErrLogger.PrintLine();
            ErrLogger.PrintLine("Loading Galaxy: {0}", filename);

            //// ....
            var entryList = ReaderSupport.DeserializeAsset<StarSystemList>(filename);
            foreach (var entry in entryList.Galaxy)
            {
                if (string.IsNullOrWhiteSpace(entry.name))
                {
                    ErrLogger.PrintLine("WARNING. Galaxy Load. Star system name is empty");
                    continue;
                }
                var sysclass = (Star.SPECTRAL_CLASS)System.Enum.Parse(typeof(Star.SPECTRAL_CLASS), entry.sys_class);
                switch (entry.type.ToLower().Trim())
                {
                    case "system":
                        StarSystem star_system = new StarSystem(entry.name, entry.loc, entry.iff, sysclass);
                        star_system.Load();
                        systems.Add(star_system);

                        Star systemstar = new Star(entry.name, entry.loc, sysclass);
                        stars.Add(systemstar);
                        break;
                    case "star":
                        Star star = new Star(entry.name, entry.loc, sysclass);
                        stars.Add(star);
                        break;
                    default:
                        ErrLogger.PrintLine("WARNING. Galaxy Loading {0}. Unknown star system type {1}", entry.name, entry.type);
                        break;
                }
            }
            ErrLogger.PrintLine("Galaxy Load(string filename) is not yet implemented");
        }
        public virtual void ExecFrame()
        {
            foreach (var sys in systems)
            {
                sys.ExecFrame();
            }
        }

        // accessors:
        public string Name() { return name; }
        public string Description() { return description; }
        public List<StarSystem> GetSystemList() { return systems; }
        public List<Star> Stars() { return stars; }
        public double Radius() { return radius; }

        public StarSystem GetSystem(string name)
        {
            foreach (var sys in systems)
            {
                if (sys.Name().Equals(name))
                    return sys;
            }

            return null;
        }

        public StarSystem FindSystemByRegion(string rgn_name)
        {
            foreach (var sys in systems)
            {
                if (sys.FindRegion(rgn_name) != null)
                    return sys;
            }

            return null;
        }

        public static void Initialize()
        {
            if (galaxy != null) galaxy = null;
            galaxy = new Galaxy("Galaxy");
            galaxy.Load();
        }
        public static void Close()
        {
            //delete galaxy;
            galaxy = null;
        }
        public static Galaxy GetInstance()
        {
            return galaxy;
        }

        protected string filename;
        protected string name;
        protected string description;
        protected double radius;           // radius in parsecs

        protected List<StarSystem> systems = new List<StarSystem>();
        protected List<Star> stars = new List<Star>();
        private static Galaxy galaxy = null;
    }
}
