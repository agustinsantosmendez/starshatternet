﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetAuth.h/NetAuth.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    This class authenticates a user connecting to the multiplayer lobby
*/
using System;

namespace StarshatterNet.Stars.Network
{
    public class NetAuth
    {
        public enum AUTH_STATE
        {
            NET_AUTH_INITIAL = 0,
            NET_AUTH_FAILED = 1,
            NET_AUTH_OK = 2
        };

        public enum AUTH_LEVEL
        {
            NET_AUTH_MINIMAL = 0,
            NET_AUTH_STANDARD = 1,
            NET_AUTH_SECURE = 2
        };

        public static int AuthLevel() { throw new NotImplementedException(); }
        public static void SetAuthLevel(int n) { throw new NotImplementedException(); }

        public static string CreateAuthRequest(NetUser u) { throw new NotImplementedException(); }
        public static string CreateAuthResponse(int level, string salt) { throw new NotImplementedException(); }
        public static bool AuthUser(NetUser u, string response) { throw new NotImplementedException(); }
    }
}