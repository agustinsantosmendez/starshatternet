














<b>STARSHATTER: The Gathering Storm</b>
A Destroyer Studios Production


<b>DEVELOPMENT</b>

<color=#bbbbb8>Producer</color>						John DiCamillo
<color=#bbbbb8>Designer</color>						John DiCamillo

<color=#bbbbb8>Lead Programmer</color>			John DiCamillo
<color=#bbbbb8>nGen Programmer</color>			John DiCamillo
<color=#bbbbb8>Magic3D Programmer</color>		John DiCamillo

<color=#bbbbb8>Art Director</color>					Sean Moser
<color=#bbbbb8>3D Artwork</color>					Jason Blaz
									John DiCamillo
<color=#bbbbb8>2D Artwork</color>					John DiCamillo
									Sean Moser

<color=#bbbbb8>Player Manual</color>				John DiCamillo

<b>AUDIO TEAM</b>

<color=#bbbbb8>Audio Director</color>				Scott Cairns
<color=#bbbbb8>Voice Over Director</color>		Stuart Smith
<color=#bbbbb8>Musical Score</color>				Scott Cairns
<color=#bbbbb8>Sound Effects</color>				John DiCamillo

			<color=#bbbbb8>Jonas Devlin</color>				Stephen Hirst
			<color=#bbbbb8>Sarah Hunter</color>				Amanda Gerrard
			<color=#bbbbb8>President Valmar</color>		Andrew Godbold
			<color=#bbbbb8>Admiral Evars</color>			James Turner
			<color=#bbbbb8>Vice Admiral Caldott</color>	John DiCamillo 
			<color=#bbbbb8>Kash Anlon</color>				Sam Cavanaugh
			<color=#bbbbb8>Newscaster</color>				Stuart Smith
			<color=#bbbbb8>Newscaster</color>				Jodie Connor
			<color=#bbbbb8>Training Instructor</color>		Callum Hayes

			<color=#bbbbb8>Fighter Pilot 1</color>			Anthony Howes
			<color=#bbbbb8>Fighter Pilot 2</color>			Stuart Smith
			<color=#bbbbb8>Fighter Pilot 3</color>			Scott Cairns
			<color=#bbbbb8>Fighter Pilot 4</color>			Lina Cairns

<b>QUALITY ASSURANCE</b>

<color=#bbbbb8>Beta Team Leads</color>			Crazy Eddie
									Mehrunes
									Pheagey

<color=#bbbbb8>Beta Testers</color>
Dragon Lady * cybersleuth58 * ravenMKII * sky_walker *
se5a * DamoclesX * Henry * executioner_de * FLY135 *
Ancient Angel * Phoenix Starflare * Vice * Daedalus *
Deadmannumberone * Deslok * Gunfighter * sniperscope *
Swordsman * Steelviper33 * Phear * spacedad62 * loafer *
mldaalder *URG_thrash * Gmicek * Veloxi * DarkeLyte *
bliu * Slaor * Maddi * Qrias * xaotik * Blair * Slippy *
Lord QDaan * A2597 * Lord Darkstar * Mr_Torgo * Rhett *
Col. Blackwolf * starbird34 * fattytheking * Maurader *
FS_Lancer * GreenJedi * Mopar * Bloodborg * tyrkina *
amwhere * Bollinger * texmurph * trancetopia * XPav *
Fallen_angel * panick * Game Ender * Spector445 * Magus *
Darrylbar * Kallis * river * Stuntie * Kurimuzon * Ace1 *
John Rowan * Matti Kuokkanen * Parias * Livid Hybrid *
Mutos * Seawolf156 * Leonides * Elenkis * RedMenace *
Bob McDobb * bk_raven * Dekzar * Mark Garanchon * jwal *
Mark Hymer * Ploppy * Red Menace * Rick BC * dukrous *
nanoprobe * Vespero * Alex Belton * Brian Rubin * Jason
Ross * John Magarrell * Pumpkin Patch * Tony Molder


<b>CREATIVE CONSULTANTS</b>

<color=#bbbbb8>Design</color>							Greg Eakin
<color=#bbbbb8>Graphics</color>						Mark DiCamillo

<b>TECHNICAL CONSULTANTS</b>

<color=#bbbbb8>Aerodynamics</color>					Joseph R. DiCamillo
<color=#bbbbb8>Digital Control Systems</color>		David Jenkins
<color=#bbbbb8>Dynamic Campaign</color>			John Walker

<b>SPECIAL THANKS</b>

Ken Beckett
Richard Brewer
David Coy
Dan Foy
Jill Goldworn
Marc Hudgins
Randy Littlejohn
Jack Nichols
Jay Patel
David Ray
Jeff Reitman
John Walker

NaturalPoint
Prime Sounds
Propellerheads
Sound Ideas
Xiphophorous

<b>EXTRA SPECIAL THANKS</b>

Mary DiCamillo
Marisa DiCamillo
Michael DiCamillo
Connor DiCamillo

<b>TEAM MASCOT</b>

Jake

<b>PUBLIC RELATIONS</b>

http://www.starshatter.com
milo@starshatter.com

<b>PORTING FROM C++ TO C# AND UNITY3D</b>

Agustin Santos