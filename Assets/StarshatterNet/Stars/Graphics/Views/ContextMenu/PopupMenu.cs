﻿using System;
using System.Collections.Generic;
using StarshatterNet.Stars.Views;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace StarshatterNet.Stars.Graphics.Views
{

    /// <summary>
    ///   <para>UnityEvent callback for when a popup menu item is selected.</para>
    /// </summary>
    [Serializable]
    public class PopupEvent : UnityEvent<GameObject>
    {
    }


    /// <summary>
    ///   <para>A standard Popup menu that could be used as context menu. 
    ///   A context menu (also called contextual, shortcut, and pop up or pop-up menu) is a menu in a graphical user interface (GUI) that
    ///   appears upon user interaction, such as a right-click mouse operation. 
    ///   A context menu offers a limited set of choices that are available in the current state, or context, of the operating system or 
    ///   application to which the menu belongs.
    ///   Context menus are sometimes hierarchically organized, allowing navigation through different levels of the menu structure. 
    ///   This code borrows ideas and desgign from Dropdown Unity code: 
    ///   https://github.com/jamesjlinden/unity-decompiled/blob/master/UnityEngine.UI/UI/Dropdown.cs
    ///   </para>
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu("Custom/PopupMenu")]
    public class PopupMenu : Selectable, IEventSystemHandler, IPointerClickHandler, ISubmitHandler, ICancelHandler
    {
        private static StarshatterNet.Stars.Views.MenuItem s_NoOptionData = new StarshatterNet.Stars.Views.MenuItem(null);

        [Space]
        [SerializeField]
        private StarshatterNet.Stars.Views.Menu m_MenuOptions = new StarshatterNet.Stars.Views.Menu();

        [Space]
        [SerializeField]
        private PopupEvent m_OnValueSelected = new PopupEvent();
        protected List<PopupItem> m_Items = new List<PopupItem>();

        [SerializeField]
        private RectTransform m_MenuTemplate;
        [SerializeField]
        private RectTransform m_ItemTemplate;
        [SerializeField]
        private RectTransform m_SeparatorTemplate;

        [SerializeField]
        private Text m_CaptionText;

        [SerializeField]
        private Image m_CaptionImage;

        [SerializeField]
        [Space]
        private Text m_ItemText;

        [SerializeField]
        private Image m_ItemImage;

        private GameObject m_PopupMenu;
        private GameObject m_PopupSubMenu;
        private GameObject m_Blocker;
        private bool validTemplate;

        private static List<GameObject> m_MenuObjs = new List<GameObject>();

        /// <summary>
        ///   <para>The Rect Transform of the template for the Popup menu.</para>
        /// </summary>
        public RectTransform menutemplate
        {
            get
            {
                return this.m_MenuTemplate;
            }
            set
            {
                this.m_MenuTemplate = UnityEngine.Object.Instantiate(value);
                PopupMenu newmenu = this.m_MenuTemplate.GetComponent<PopupMenu>();
                // Just in case that the template had some items
                foreach (var item in newmenu.m_Items)
                {
                    DestroyItem(item);
                }
                newmenu.m_Items.Clear();
                this.m_MenuTemplate.gameObject.SetActive(false);
                this.m_MenuTemplate.parent = this.transform;
                this.m_MenuTemplate.name = "MenuTemplate";
                this.RefreshShownValue();
            }
        }

        /// <summary>
        ///   <para>The Rect Transform of the template for the Popup item.</para>
        /// </summary>
        public RectTransform itemtemplate
        {
            get
            {
                return this.m_ItemTemplate;
            }
            set
            {
                this.m_ItemTemplate = value;
                if (this.m_MenuTemplate != null)
                {
                    PopupMenu newmenu = this.m_MenuTemplate.GetComponent<PopupMenu>();
                    newmenu.itemtemplate = this.m_ItemTemplate;
                }
                this.RefreshShownValue();
            }
        }

        /// <summary>
        ///   <para>The Text component to hold the text of the currently selected option.</para>
        /// </summary>
        public Text captionText
        {
            get
            {
                return this.m_CaptionText;
            }
            set
            {
                this.m_CaptionText = value;
                this.RefreshShownValue();
            }
        }

        /// <summary>
        ///   <para>The Image component to hold the image of the currently selected option.</para>
        /// </summary>
        public Image captionImage
        {
            get
            {
                return this.m_CaptionImage;
            }
            set
            {
                this.m_CaptionImage = value;
                this.RefreshShownValue();
            }
        }

        /// <summary>
        ///   <para>The Text component to hold the text of the item.</para>
        /// </summary>
        public Text itemText
        {
            get
            {
                return this.m_ItemText;
            }
            set
            {
                this.m_ItemText = value;
                this.RefreshShownValue();
            }
        }

        /// <summary>
        ///   <para>The Image component to hold the image of the item.</para>
        /// </summary>
        public Image itemImage
        {
            get
            {
                return this.m_ItemImage;
            }
            set
            {
                this.m_ItemImage = value;
                this.RefreshShownValue();
            }
        }

        /// <summary>
        ///   <para>A menu with the list of possible hierarchical options. A text string and an image can be specified for each option.</para>
        /// </summary>
        public StarshatterNet.Stars.Views.Menu menu
        {
            get
            {
                return this.m_MenuOptions;
            }
            set
            {
                this.m_MenuOptions = value;
                AddMenuItems(menu.GetItems());
                if (menu.GetLevel() == 0)
                {
                    for (int i = m_MenuObjs.Count - 1; i >= 0; i--)
                    {
                        GameObject.Destroy(m_MenuObjs[i]);
                        m_MenuObjs.RemoveAt(i);
                    }
                    m_MenuObjs.Clear();
                }
                this.RefreshShownValue();
            }
        }

        /// <summary>
        ///   <para>A UnityEvent that is invoked when when a user has clicked one of the options in the item menu.</para>
        /// </summary>
        public PopupEvent onValueSelected
        {
            get
            {
                return this.m_OnValueSelected;
            }
            set
            {
                this.m_OnValueSelected = value;
            }
        }

        public List<GameObject> MenuObjs { get => m_MenuObjs; set => m_MenuObjs = value; }
        public GameObject RootPopupMenu { get => m_PopupMenu; set => m_PopupMenu = value; }

        protected PopupMenu()
        {
        }

        protected override void Awake()
        {
            if (!Application.isPlaying)
                return;
            if ((bool)((UnityEngine.Object)this.m_CaptionImage))
                this.m_CaptionImage.enabled = (UnityEngine.Object)this.m_CaptionImage.sprite != (UnityEngine.Object)null;
            if (!(bool)((UnityEngine.Object)this.m_ItemTemplate))
                return;
            this.m_ItemTemplate.gameObject.SetActive(false);
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            if (!this.IsActive())
                return;
            this.RefreshShownValue();
        }

        /// <summary>
        /// <para>Refreshes the text and image (if available) of the menu.
        /// 
        /// If you have modified the menu, you should call this method afterwards to ensure 
        /// that the visual state of the popup corresponds to the updated menu.</para>
        /// </summary>
        public void RefreshShownValue()
        {
        }
        protected void AddMenuItem(MenuItem menuitem)
        {
            GameObject item = GameObject.Instantiate<GameObject>(m_ItemTemplate.gameObject, this.transform);
            item.name = menuitem.GetText();

            Text text = item.GetComponentInChildren<Text>();
            if (text != null)
                text.text = menuitem.GetText();

            PopupItem itemCmpt = item.GetComponent<PopupItem>();
            itemCmpt.Value = menuitem;
            itemCmpt.level = menuitem.GetLevel();
            itemCmpt.onItemClicked.AddListener(x => OnSelectItem(item));
            itemCmpt.submenu = menuitem.GetSubmenu();

            if (menuitem.GetSubmenu() == null || menuitem.GetSubmenu().NumItems() == 0)
                item.transform.Find("Arrow").gameObject.SetActive(false);

            this.m_Items.Add(itemCmpt);
            item.SetActive(true);
        }
        public void AddMenuItems(List<MenuItem> options)
        {
            for (int index = 0; index < options.Count; ++index)
                this.AddMenuItem(options[index]);
            this.RefreshShownValue();
        }
        public void AddMenu(Menu menu)
        {
            foreach (MenuItem item in menu.GetItems())
                this.AddMenuItem(item);
            this.RefreshShownValue();
        }

        /// <summary>
        ///   <para>Clear the list of item in the Popup.</para>
        /// </summary>
        public void ClearMenuItems()
        {
            this.m_MenuOptions.ClearItems();
            this.RefreshShownValue();
        }

        private void SetupItemTemplate()
        {
            this.validTemplate = false;
            if (!(bool)((UnityEngine.Object)this.m_ItemTemplate))
            {
                UnityEngine.Debug.LogError((object)"The PopupMenu Item template is not assigned. The template needs to be assigned and must have a child GameObject with a Toggle component serving as the item.", (UnityEngine.Object)this);
            }
            else
            {
                GameObject gameObject = this.m_ItemTemplate.gameObject;
                gameObject.SetActive(true);
                Toggle componentInChildren = this.m_ItemTemplate.GetComponentInChildren<Toggle>();
                this.validTemplate = true;
                if (!(bool)((UnityEngine.Object)componentInChildren) || (UnityEngine.Object)componentInChildren.transform == (UnityEngine.Object)this.itemtemplate)
                {
                    this.validTemplate = false;
                    UnityEngine.Debug.LogError((object)"The PopupMenu Item template is not valid. The template must have a child GameObject with a Toggle component serving as the item.", (UnityEngine.Object)this.itemtemplate);
                }
                else if (!(componentInChildren.transform.parent is RectTransform))
                {
                    this.validTemplate = false;
                    UnityEngine.Debug.LogError((object)"The PopupMenu Item template is not valid. The child GameObject with a Toggle component (the item) must have a RectTransform on its parent.", (UnityEngine.Object)this.itemtemplate);
                }
                else if ((UnityEngine.Object)this.itemText != (UnityEngine.Object)null && !this.itemText.transform.IsChildOf(componentInChildren.transform))
                {
                    this.validTemplate = false;
                    UnityEngine.Debug.LogError((object)"The PopupMenu Item template is not valid. The Item Text must be on the item GameObject or children of it.", (UnityEngine.Object)this.itemtemplate);
                }
                else if ((UnityEngine.Object)this.itemImage != (UnityEngine.Object)null && !this.itemImage.transform.IsChildOf(componentInChildren.transform))
                {
                    this.validTemplate = false;
                    UnityEngine.Debug.LogError((object)"The PopupMenu Item template is not valid. The Item Image must be on the item GameObject or children of it.", (UnityEngine.Object)this.itemtemplate);
                }
                if (!this.validTemplate)
                {
                    gameObject.SetActive(false);
                }
                else
                {
                    PopupItem popupItem = componentInChildren.gameObject.AddComponent<PopupItem>();
                    popupItem.text = this.m_ItemText;
                    popupItem.image = this.m_ItemImage;
                    popupItem.toggle = componentInChildren;
                    popupItem.rectTransform = (RectTransform)componentInChildren.transform;
                    Canvas orAddComponent = PopupMenu.GetOrAddComponent<Canvas>(gameObject);
                    orAddComponent.overrideSorting = true;
                    orAddComponent.sortingOrder = 30000;
                    PopupMenu.GetOrAddComponent<GraphicRaycaster>(gameObject);
                    PopupMenu.GetOrAddComponent<CanvasGroup>(gameObject);
                    gameObject.SetActive(false);
                    this.validTemplate = true;
                }
            }
        }

        private static T GetOrAddComponent<T>(GameObject go) where T : Component
        {
            T obj = go.GetComponent<T>();
            if (!(bool)((UnityEngine.Object)obj))
                obj = go.AddComponent<T>();
            return obj;
        }

        /// <summary>
        ///   <para>Handling for when a menu item is 'clicked'.</para>
        /// </summary>
        /// <param name="eventData">Current event.</param>
        public virtual void OnPointerClick(PointerEventData eventData)
        {
            this.m_OnValueSelected.Invoke(this.gameObject);
        }

        /// <summary>
        ///   <para>What to do when the event system sends a submit Event.</para>
        /// </summary>
        /// <param name="eventData">Current event.</param>
        public virtual void OnSubmit(BaseEventData eventData)
        {
            this.Show();
        }

        /// <summary>
        ///   <para>Called by a BaseInputModule when a Cancel event occurs.</para>
        /// </summary>
        /// <param name="eventData"></param>
        public virtual void OnCancel(BaseEventData eventData)
        {
            this.Hide();
        }

        /// <summary>
        ///   <para>Show the dropdown list.</para>
        /// </summary>
        public void Show()
        {
            if (!this.IsActive() || !this.IsInteractable() || (UnityEngine.Object)this.m_PopupMenu != (UnityEngine.Object)null)
                return;
            if (!this.validTemplate)
            {
                this.SetupItemTemplate();
                if (!this.validTemplate)
                    return;
            }
            List<Canvas> canvasList = new List<Canvas>();
            this.gameObject.GetComponentsInParent<Canvas>(false, canvasList);
            if (canvasList.Count == 0)
                return;
            Canvas rootCanvas = canvasList[0];
            canvasList = null;
            this.m_ItemTemplate.gameObject.SetActive(true);
            PopupItem componentInChildren = this.m_PopupMenu.GetComponentInChildren<PopupItem>();
            RectTransform transform2 = componentInChildren.rectTransform.parent.gameObject.transform as RectTransform;
            componentInChildren.rectTransform.gameObject.SetActive(true);

            // TODO set Navigation

            for (int i = 0; i < menu.NumItems(); i++)
            {
                Text text = this.m_ItemTemplate.gameObject.GetComponent<Text>();
                text.text = menu.GetItem(i).GetText();
                GameObject item = GameObject.Instantiate<GameObject>(this.m_ItemTemplate.gameObject, this.transform);
                item.name = menu.GetItem(i).GetText();
                PopupItem detection = item.GetComponent<PopupItem>();
                //detection.ItemGameObject = item;
                //detection.Value = i;
                detection.onItemClicked.AddListener(x => OnSelectItem(item));
                if (menu.GetItem(i).GetSubmenu() == null || menu.GetItem(i).GetSubmenu().NumItems() == 0)
                    item.transform.Find("Arrow").gameObject.SetActive(false);
                item.SetActive(true);
            }

            this.m_ItemTemplate.gameObject.SetActive(false);
            componentInChildren.gameObject.SetActive(false);
            this.m_Blocker = this.CreateBlocker(rootCanvas);
        }

        /// <summary>
        ///   <para>Override this method to implement a different way to obtain a blocker GameObject.</para>
        /// </summary>
        /// <param name="rootCanvas">The root canvas the popup menu is under.</param>
        /// <returns>
        ///   <para>The obtained blocker.</para>
        /// </returns>
        protected virtual GameObject CreateBlocker(Canvas rootCanvas)
        {
            GameObject gameObject = new GameObject("Blocker");
            RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
            rectTransform.SetParent(rootCanvas.transform, false);
            rectTransform.anchorMin = (Vector2)Vector3.zero;
            rectTransform.anchorMax = (Vector2)Vector3.one;
            rectTransform.sizeDelta = Vector2.zero;
            Canvas canvas = gameObject.AddComponent<Canvas>();
            canvas.overrideSorting = true;
            Canvas component = this.m_PopupMenu.GetComponent<Canvas>();
            canvas.sortingLayerID = component.sortingLayerID;
            canvas.sortingOrder = component.sortingOrder - 1;
            gameObject.AddComponent<GraphicRaycaster>();
            gameObject.AddComponent<Image>().color = Color.clear;
            gameObject.AddComponent<Button>().onClick.AddListener(new UnityAction(this.Hide));
            return gameObject;
        }

        /// <summary>
        ///   <para>Override this method to implement a different way to dispose of a blocker GameObject 
        ///   that blocks clicks to other controls while the popup menu is open.</para>
        /// </summary>
        /// <param name="blocker">The blocker to dispose of.</param>
        protected virtual void DestroyBlocker(GameObject blocker)
        {
            UnityEngine.Object.Destroy((UnityEngine.Object)blocker);
        }

        /// <summary>
        ///   <para>Override this method to implement a different way to obtain a popup menu  GameObject.</para>
        /// </summary>
        /// <param name="menuTemplate">The template to create the popup menu from.</param>
        /// <returns>
        ///   <para>The obtained popup menu.</para>
        /// </returns>
        protected virtual PopupMenu CreatePopupMenu(GameObject menuTemplate)
        {
            GameObject newObj = UnityEngine.Object.Instantiate<GameObject>(menuTemplate);
            PopupMenu newmenu = newObj.GetComponent<PopupMenu>();
            return newmenu;
        }
        public virtual PopupMenu CreatePopupMenu(Vector3 position, Menu submenu)
        {
            PopupMenu rootMenu = this.RootPopupMenu.GetComponent<PopupMenu>();
            PopupMenu childPopupItem = CreatePopupMenu(rootMenu.menutemplate.gameObject);
            GameObject newPopupMenu = childPopupItem.gameObject;
            childPopupItem.menu = submenu;
            childPopupItem.RootPopupMenu = this.RootPopupMenu;
            this.MenuObjs.Add(newPopupMenu);

            newPopupMenu.name = this.name + "-" + submenu.GetLevel();
            newPopupMenu.SetActive(true);
            //childPopupItem.onValueSelected.AddListener(x => this.OnSelectItem(this.gameObject));
            childPopupItem.onValueSelected.AddListener(OnSelectItem);
            Canvas canvas = this.gameObject.GetComponentInParent<Canvas>();
            // Update the canvases to get the updated rect of the text.
            Canvas.ForceUpdateCanvases();
            RectTransform rectTransform = newPopupMenu.GetComponent<RectTransform>();
            rectTransform.localPosition = position - new Vector3(0, rectTransform.rect.height, 0);
            SetParentAndAlign(newPopupMenu, canvas.gameObject);

            return childPopupItem;
        }
        public static void SetParentAndAlign(GameObject child, GameObject parent)
        {
            if ((UnityEngine.Object)parent == (UnityEngine.Object)null)
                return;
            child.transform.SetParent(parent.transform, false);
            SetLayerRecursively(child, parent.layer);
        }
        private static void SetLayerRecursively(GameObject go, int layer)
        {
            go.layer = layer;
            Transform transform = go.transform;
            for (int index = 0; index < transform.childCount; ++index)
                SetLayerRecursively(transform.GetChild(index).gameObject, layer);
        }

        /// <summary>
        ///   <para>Override this method to implement a different way to obtain a popup item  GameObject.</para>
        /// </summary>
        /// <param name="menuTemplate">The template to create the popup item from.</param>
        /// <returns>
        ///   <para>The obtained popup item.</para>
        /// </returns>
        protected virtual PopupItem CreateItem(PopupItem itemTemplate)
        {
            return UnityEngine.Object.Instantiate<PopupItem>(itemTemplate);
        }

        /// <summary>
        ///   <para>Override this method to implement a different way to dispose of a popup menu GameObject.</para>
        /// </summary>
        /// <param name="dropdownList">The dropdown menu to dispose of.</param>
        protected virtual void DestroyPopupMenu(GameObject popupList)
        {
            //Debug.Log("DestroyPopupMenu:" + popupList.name);
            UnityEngine.Object.Destroy((UnityEngine.Object)popupList);
        }


        protected virtual void DestroyItem(PopupItem item)
        {
            UnityEngine.Object.Destroy(item.gameObject);
        }

        /// <summary>
        ///   <para>Hide the dropdown list.</para>
        /// </summary>
        public void Hide()
        {
            //if ((UnityEngine.Object)this.m_PopupMenu != (UnityEngine.Object)null)
            {
                //this.AlphaFadeList(0.15f, 0.0f);
                if (this.IsActive())
                    this.StartCoroutine(this.DelayedDestroyPopupMenu(0.15f));
            }
            if ((UnityEngine.Object)this.m_Blocker != (UnityEngine.Object)null)
                this.DestroyBlocker(this.m_Blocker);
            this.m_Blocker = (GameObject)null;
            this.Select();
        }

        private IEnumerator<WaitForSecondsRealtime> DelayedDestroyPopupMenu(float delay)
        {
            yield return new WaitForSecondsRealtime(delay);
            ImmediateDestroyPopupMenu();
        }

        private void ImmediateDestroyPopupMenu()
        {
            //Debug.Log("ImmediateDestroyPopupMenu() " + m_MenuObjs.Count);
            for (int i = m_MenuObjs.Count; i > 0; i--)
            {
                GameObject.Destroy(m_MenuObjs[i - 1]);
            }
            m_MenuObjs.Clear();

            for (int i = 0; i < m_Items.Count; i++)
            {
                if (m_Items[i] != null)
                    DestroyItem(m_Items[i]);
            }
            m_Items.Clear();

            if (m_PopupMenu != null)
                DestroyPopupMenu(m_PopupMenu);
            m_PopupMenu = null;
        }

        private void OnSelectItem(GameObject gameObj)
        {
            //Debug.Log("PopupMenu.OnSelectItem Clicked on index:" + gameObj.name);
            this.onValueSelected.Invoke(gameObj);
            if (this.RootPopupMenu == this.gameObject)
                this.Hide();
        }
    }
}
