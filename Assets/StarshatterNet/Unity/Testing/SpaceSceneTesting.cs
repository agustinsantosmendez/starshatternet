﻿using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

public class SpaceSceneTesting : MonoBehaviour
{
    Stars stars;
    SkyNebula nebula1, nebula2;
    PlanetRep earth;
    void Awake()
    {
        stars = new Stars(1000);
        nebula1 = new SkyNebula("NebulaAthenar");
        nebula2 = new SkyNebula("NebulaRadix");

        Vector3D planetloc = new Vector3D(350, 250, 450);
        earth = new PlanetRep("surface", "glow", 100, planetloc);
    }


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 100, 20), "NebulaAthenar"))
        {
            nebula2.Hide();
            nebula1.Show();
        }
        if (GUI.Button(new Rect(0, 50, 100, 20), "NebulaRadix"))
        {
            nebula1.Hide();
            nebula2.Show();
        }
    }
}
