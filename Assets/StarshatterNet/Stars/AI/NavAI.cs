﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NavAI.h/NavAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Automatic Navigator
*/
using System;
using DigitalRune.Mathematics;
using StarshatterNet.Audio;
using StarshatterNet.Config;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.Views;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.AI
{
    public class NavAI : ShipAI
    {

        public NavAI(Ship s) : base(s)
        {
            complete = false; brakes = 0;
            drop_state = 0; quantum_state = 0; farcaster = null; terrain_warning = false;
            seek_gain = 20;
            seek_damp = 0.55;

            //delete tactical;
            tactical = null;
        }
        // public virtual ~NavAI();

        public const int DIR_TYPE = 2000;
        public override SteerType Type() { return (SteerType)DIR_TYPE; }

        static double time_til_change = 0.0;
        public override void ExecFrame(double s)
        {
            if (ship == null) return;

            seconds = s;

            ship.SetDirectorInfo(" ");

            if (ship.GetFlightPhase() == OP_MODE.TAKEOFF)
                takeoff = true;

            else if (takeoff && ship.MissionClock() > 10000)
                takeoff = false;

            FindObjective();
            Navigator();

            // watch for disconnect:
            if (ShipCtrl.Toggled(KeyMap.KEY_AUTO_NAV))
            {
                NavSystem navsys = ship.GetNavSystem();
                if (navsys != null)
                {
                    HUDView.GetInstance().SetHUDMode(HUDView.HUDModes.HUD_MODE_TAC);
                    navsys.DisengageAutoNav();

                    Sim sim = Sim.GetSim();
                    if (sim != null)
                    {
                        ship.SetControls(sim.GetControls());
                        return;
                    }
                }
            }


            if (time_til_change < 0.001)
            {
                if (ship.GetShield() != null)
                {
                    Shield shield = ship.GetShield();
                    double level = shield.GetPowerLevel();

                    if (ShipCtrl.KeyDown(KeyMap.KEY_SHIELDS_FULL))
                    {
                        HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_SHIELD_LEVEL);
                        shield.SetPowerLevel(100);
                        time_til_change = 0.5f;
                    }

                    else if (ShipCtrl.KeyDown(KeyMap.KEY_SHIELDS_ZERO))
                    {
                        HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_SHIELD_LEVEL);
                        shield.SetPowerLevel(0);
                        time_til_change = 0.5f;
                    }

                    else if (ShipCtrl.KeyDown(KeyMap.KEY_SHIELDS_UP))
                    {
                        if (level < 25) level = 25;
                        else if (level < 50) level = 50;
                        else if (level < 75) level = 75;
                        else level = 100;

                        HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_SHIELD_LEVEL);
                        shield.SetPowerLevel(level);
                        time_til_change = 0.5f;
                    }

                    else if (ShipCtrl.KeyDown(KeyMap.KEY_SHIELDS_DOWN))
                    {
                        if (level > 75) level = 75;
                        else if (level > 50) level = 50;
                        else if (level > 25) level = 25;
                        else level = 0;

                        HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_SHIELD_LEVEL);
                        shield.SetPowerLevel(level);
                        time_til_change = 0.5f;
                    }

                }
            }
            else
            {
                time_til_change -= seconds;
            }

            if (ShipCtrl.Toggled(KeyMap.KEY_DECOY))
                ship.FireDecoy();

            if (ShipCtrl.Toggled(KeyMap.KEY_LAUNCH_PROBE))
                ship.LaunchProbe();

            if (ShipCtrl.Toggled(KeyMap.KEY_GEAR_TOGGLE))
                ship.ToggleGear();

            if (ShipCtrl.Toggled(KeyMap.KEY_NAVLIGHT_TOGGLE))
                ship.ToggleNavlights();

            if (drop_state < 0)
            {
                ship.DropOrbit();
                return;
            }

            if (drop_state > 0)
            {
                ship.MakeOrbit();
                return;
            }
        }
        public override bool Subframe() { return true; }
        public void Disengage()
        {
            throttle = 0;
        }
        public bool Complete() { return complete; }

        // behaviors:
        protected override Steer SeekTarget()
        {
            if (ship == null)
                return new Steer();

            if (takeoff)
                return Seek(objective);

            if (navpt != null)
            {
                if (quantum_state == 1)
                {
                    QuantumDrive q = ship.GetQuantumDrive();

                    if (q != null)
                    {
                        if (q.ActiveState() == QuantumDrive.ACTIVE_STATES.ACTIVE_READY)
                        {
                            q.SetDestination(navpt.Region(), navpt.Location());
                            q.Engage();
                        }

                        else if (q.ActiveState() == QuantumDrive.ACTIVE_STATES.ACTIVE_POSTWARP)
                        {
                            quantum_state = 0;
                        }
                    }
                }

                if (distance < 2 * self.Radius())
                {
                    ship.SetNavptStatus(navpt, Instruction.STATUS.COMPLETE);

                    return new Steer();
                }
                else
                {
                    return Seek(objective);
                }
            }

            return new Steer();
        }

        // steering functions:
        protected override Point Transform(Point point)
        {
            if (ship != null && ship.IsStarship())
                return point - self.Location();

            return base.Transform(point);
        }

        protected override Steer Seek(Point point)
        {
            // if ship is starship, the point is in relative world coordinates
            //   x: distance east(-)  / west(+)
            //   y: altitude down(-)  / up(+)
            //   z: distance north(-) / south(+)

            if (ship != null && ship.IsStarship())
            {
                Steer result = new Steer();

                result.yaw = Math.Atan2(point.X, point.Z) + Math.PI;

                double adjacent = Math.Sqrt(point.X * point.X + point.Z * point.Z);
                if (Math.Abs(point.Y) > ship.Radius() && adjacent > ship.Radius())
                    result.pitch = Math.Atan(point.Y / adjacent);

                if (double.IsNaN(result.yaw))
                    result.yaw = 0;

                if (double.IsNaN(result.pitch))
                    result.pitch = 0;

                return result;
            }

            return base.Seek(point);
        }
        protected override Steer Flee(Point point)
        {
            if (ship != null && ship.IsStarship())
            {
                Steer result = Seek(point);
                result.yaw += Math.PI;
                result.pitch *= -1;
                return result;
            }

            return base.Flee(point);
        }
        protected override Steer Avoid(Point point, float radius)
        {
            if (ship != null && ship.IsStarship())
            {
                Steer result = Seek(point);

                if (Point.Dot(point, ship.BeamLine()) > 0)
                    result.yaw -= Math.PI / 2;
                else
                    result.yaw += Math.PI / 2;

                return result;
            }

            return base.Avoid(point, radius);
        }
        protected override Steer AvoidTerrain()
        {
            Steer avoid = new Steer();

            terrain_warning = false;

            if (ship == null || ship.GetRegion() == null || !ship.GetRegion().IsActive() ||
                    takeoff || (navpt != null && navpt.Action() == Instruction.ACTION.LAUNCH))
                return avoid;

            if (ship.IsAirborne() && ship.GetFlightPhase() == OP_MODE.ACTIVE)
            {
                // too low?
                if (ship.AltitudeAGL() < 1000)
                {
                    terrain_warning = true;
                    ship.SetDirectorInfo(Game.GetText("ai.too-low"));

                    // way too low?
                    if (ship.AltitudeAGL() < 750)
                    {
                        ship.SetDirectorInfo(Game.GetText("ai.way-too-low"));
                    }

                    // where will we be?
                    Point selfpt = ship.Location() + ship.Velocity() + new Point(0, 10e3, 0);

                    // transform into camera coords:
                    Point obj = Transform(selfpt);

                    // pull up!
                    avoid = Seek(obj);
                }
            }

            return avoid;
        }

        // accumulate behaviors:
        protected override void Navigator()
        {
            accumulator.Clear();
            magnitude = 0;
            brakes = 0;
            hold = false;

            if (navpt != null)
            {
                if (navpt.Status() == Instruction.STATUS.COMPLETE && navpt.HoldTime() > 0)
                {
                    ship.SetDirectorInfo(Game.GetText("ai.auto-hold"));
                    hold = true;
                }
                else
                {
                    ship.SetDirectorInfo(Game.GetText("ai.auto-nav"));
                }
            }
            else
            {
                ship.SetDirectorInfo(Game.GetText("ai.auto-stop"));
            }

            Accumulate(AvoidTerrain());
            Accumulate(AvoidCollision());

            if (!hold)
                accumulator = SeekTarget();

            HelmControl();
            ThrottleControl();
        }
        public override void FindObjective()
        {
            navpt = null;
            distance = 0;

            // runway takeoff:
            if (takeoff)
            {
                obj_w = ship.Location() + ship.Heading() * 10e3;
                obj_w.Y = ship.Location().Y + 2e3;

                // transform into camera coords:
                objective = Transform(obj_w);
                ship.SetDirectorInfo(Game.GetText("ai.takeoff"));
                return;
            }

            // PART I: Find next NavPoint:
            if (ship.GetNavSystem() != null)
                navpt = ship.GetNextNavPoint();

            complete = navpt == null;
            if (complete) return;

            // PART II: Compute Objective from NavPoint:
            Point npt = navpt.Location();
            Sim sim = Sim.GetSim();
            SimRegion self_rgn = ship.GetRegion();
            SimRegion nav_rgn = navpt.Region();

            if (self_rgn != null && nav_rgn == null)
            {
                nav_rgn = self_rgn;
                navpt.SetRegion(nav_rgn);
            }

            if (self_rgn == nav_rgn)
            {
                if (farcaster != null)
                {
                    if (farcaster.GetShip().GetRegion() != self_rgn)
                        farcaster = farcaster.GetDest().GetFarcaster();

                    obj_w = farcaster.EndPoint();
                }

                else
                {
                    obj_w = npt.OtherHand();
                }

                // distance from self to navpt:
                distance = new Point(obj_w - self.Location()).Length;

                // transform into camera coords:
                objective = Transform(obj_w);

                if (!ship.IsStarship())
                    objective.Normalize();

                if (farcaster != null && distance < 1000)
                    farcaster = null;
            }

            // PART III: Deal with orbital transitions:
            else if (ship.IsDropship())
            {
                if (nav_rgn.GetOrbitalRegion().Primary() ==
                        self_rgn.GetOrbitalRegion().Primary())
                {

                    npt = nav_rgn.Location() - self_rgn.Location();
                    obj_w = npt.OtherHand();

                    // distance from self to navpt:
                    distance = new Point(obj_w - ship.Location()).Length;

                    // transform into camera coords:
                    objective = Transform(obj_w);

                    if (nav_rgn.IsAirSpace())
                    {
                        drop_state = -1;
                    }
                    else if (nav_rgn.IsOrbital())
                    {
                        drop_state = 1;
                    }
                }

                // PART IIIa: Deal with farcaster jumps:
                else if (nav_rgn.IsOrbital() && self_rgn.IsOrbital())
                {
                    foreach (Ship s in self_rgn.Ships())
                    {
                        if (farcaster != null) break;
                        if (s.GetFarcaster() != null)
                        {
                            Ship dest = s.GetFarcaster().GetDest();
                            if (dest != null && dest.GetRegion() == nav_rgn)
                            {
                                farcaster = s.GetFarcaster();
                            }
                        }
                    }

                    if (farcaster != null)
                    {
                        Point apt = farcaster.ApproachPoint(0);
                        npt = farcaster.StartPoint();
                        double r1 = (ship.Location() - npt).Length;

                        if (r1 > 50e3)
                        {
                            obj_w = apt;
                            distance = r1;
                            objective = Transform(obj_w);
                        }

                        else
                        {
                            double r2 = (ship.Location() - apt).Length;
                            double r3 = (npt - apt).Length;

                            if (r1 + r2 < 1.2 * r3)
                            {
                                obj_w = npt;
                                distance = r1;
                                objective = Transform(obj_w);
                            }
                            else
                            {
                                obj_w = apt;
                                distance = r2;
                                objective = Transform(obj_w);
                            }
                        }
                    }
                }
            }

            // PART IV: Deal with quantum jumps:
            else if (ship.IsStarship())
            {
                quantum_state = 1;

                npt = nav_rgn.Location() + navpt.Location();
                npt -= self_rgn.Location();
                obj_w = npt.OtherHand();

                // distance from self to navpt:
                distance = new Point(obj_w - ship.Location()).Length;

                // transform into camera coords:
                objective = Transform(obj_w);
            }
        }

        protected override void HelmControl()
        {
            // ----------------------------------------------------------
            // STARSHIP HELM MODE
            // ----------------------------------------------------------

            if (ship.IsStarship())
            {
                ship.SetFLCSMode(FLCS_MODE.FLCS_HELM);
                ship.SetHelmHeading(accumulator.yaw);

                if (accumulator.pitch > 45 * ConstantsF.DEGREES)
                    ship.SetHelmPitch(45 * ConstantsF.DEGREES);

                else if (accumulator.pitch < -45 * ConstantsF.DEGREES)
                    ship.SetHelmPitch(-45 * ConstantsF.DEGREES);

                else
                    ship.SetHelmPitch(accumulator.pitch);
            }

            // ----------------------------------------------------------
            // FIGHTER FLCS AUTO MODE
            // ----------------------------------------------------------

            else
            {
                ship.SetFLCSMode(FLCS_MODE.FLCS_AUTO);

                // are we being asked to flee?
                if (Math.Abs(accumulator.yaw) == 1.0 && accumulator.pitch == 0.0)
                {
                    accumulator.pitch = -0.7f;
                    accumulator.yaw *= 0.25f;
                }

                self.ApplyRoll((float)(accumulator.yaw * -0.4));
                self.ApplyYaw((float)(accumulator.yaw * 0.2));

                if (Math.Abs(accumulator.yaw) > 0.5 && Math.Abs(accumulator.pitch) < 0.1)
                    accumulator.pitch -= 0.1f;

                if (accumulator.pitch != 0)
                    self.ApplyPitch((float)accumulator.pitch);

                // if not turning, roll to orient with world coords:
                if (Math.Abs(accumulator.yaw) < 0.1)
                {
                    Point vrt = ((CameraNode)(self.Cam())).vrt();
                    double deflection = vrt.Y;
                    if (deflection != 0)
                    {
                        double theta = Math.Asin(deflection / vrt.Length);
                        self.ApplyRoll(-theta);
                    }
                }

                if (!ship.IsAirborne() || ship.AltitudeAGL() > 100)
                    ship.RaiseGear();
            }

            ship.SetTransX(0);
            ship.SetTransY(0);
            ship.SetTransZ(0);
            ship.ExecFLCSFrame();
        }
        protected override void ThrottleControl()
        {
            double ship_speed = Point.Dot(ship.Velocity(), ship.Heading());
            bool augmenter = false;

            if (hold)
            {
                throttle = 0;
                brakes = 1;
            }

            else if (navpt != null)
            {
                double speed = navpt.Speed();

                if (speed < 10)
                    speed = 250;

                throttle = 0;

                if (Ship.GetFlightModel() > 0)
                {
                    if (ship_speed > speed + 10)
                        throttle = old_throttle - 0.25;

                    else if (ship_speed < speed - 10)
                        throttle = old_throttle + 0.25;

                    else
                        throttle = old_throttle;
                }

                else
                {
                    if (ship_speed > speed + 5)
                        brakes = 0.25;

                    else if (ship_speed < speed - 5)
                        throttle = 50;
                }
            }
            else
            {
                throttle = 0;
                brakes = 0.25;
            }

            if (ship.IsAirborne() && ship.Class() < CLASSIFICATION.LCA)
            {
                if (ship_speed < 250)
                {
                    throttle = 100;
                    brakes = 0;

                    if (ship_speed < 200)
                        augmenter = true;
                }

                else if (throttle < 20)
                {
                    throttle = 20;
                }
            }

            old_throttle = throttle;
            ship.SetThrottle(throttle);
            ship.SetAugmenter(augmenter);

            if (ship_speed > 1 && brakes > 0)
                ship.SetTransY(-brakes * ship.Design().trans_y);
        }

        protected bool complete;
        protected int drop_state;
        protected int quantum_state;
        protected bool terrain_warning;
        protected double brakes;
        protected Farcaster farcaster;
    }
}
