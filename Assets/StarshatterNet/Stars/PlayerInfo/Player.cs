﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Player.h/Player.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Player / Logbook class
*/
using System;
using System.Collections.Generic;
using System.Text;
using StarshatterNet.Audio;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.Views;
using DWORD = System.UInt32;

namespace StarshatterNet.Stars
{
    public class Player
    {
        public Player(string name)
        {
            this.name = name;
        }

        public int Identity() { return uid; }
        public string Name() { return name; }
        public string Password() { return pass; }
        public string Squadron() { return squadron; }
        public string Signature() { return signature; }
        public string ChatMacro(int n)
        {
            if (n >= 0 && n < 10)
                return chat_macros[n];

            return chat_macros[0];
        }

        public DateTime CreateDate() { return create_date; }
        public int Rank()
        {
            for (int i = AwardInfo.RankTable.Count - 1; i >= 0; i--)
            {
                AwardInfo award = AwardInfo.RankTable[i];
                if (points >= award.total_points)
                    return award.id;
            }

            return 0;
        }

        public int Medal(int n)
        {
            if (n < 0)
                return 0;

            for (int i = 0; i < 16; i++)
            {
                int selector = 1 << (15 - i);

                // found a medal:
                if ((medals & selector) != 0)
                {

                    // and it's the nth medal!
                    if (n == 0)
                    {
                        return selector;
                    }

                    n--;
                }
            }

            return 0;
        }

        public int Points() { return points; }
        public int Medals() { return medals; }
        public TimeSpan FlightTime() { return flight_time; }
        public int Missions() { return missions; }
        public int Kills() { return kills; }
        public int Losses() { return losses; }
        public int Campaigns() { return campaigns; }
        public int Trained() { return trained; }

        public FLIGHT_MODEL FlightModel() { return flight_model; }
        public int FlyingStart() { return flying_start; }
        public LANDING_MODEL LandingModel() { return landing_model; }
        public int AILevel() { return ai_level; }
        public int HUDMode() { return hud_mode; }
        public int HUDColor() { return hud_color; }
        public int FriendlyFire() { return ff_level; }
        public int GridMode() { return grid; }
        public int Gunsight() { return gunsight; }

        public bool ShowAward() { return award != null; }

        public string AwardName()
        {
            if (award != null)
                return award.name;

            return "";
        }

        public string AwardDesc()
        {
            if (award != null)
                return award.grant;

            return "";
        }

        public Bitmap AwardImage()
        {
            if (award != null)
                return award.large_insignia;

            return null;
        }

        public Sound AwardSound()
        {
            if (award != null && !string.IsNullOrEmpty(award.grant_sound))
            {
                Sound result = Sound.CreateStream(award.grant_sound);
                return result;
            }

            return null;
        }


        public bool CanCommand(int ship_class)
        {
            if (ship_class <= (int)CLASSIFICATION.ATTACK)
                return true;

            for (int i = rank_table.Count - 1; i >= 0; i--)
            {
                AwardInfo award = rank_table[i];
                if (points > award.total_points)
                {
                    return (ship_class & award.granted_ship_classes) != 0;
                }
            }

            return false;
        }

        public void SetName(string n)
        {
            if (!string.IsNullOrWhiteSpace(n))
                this.name = n;
        }

        public void SetPassword(string p)
        {
            if (!string.IsNullOrWhiteSpace(p))
                this.pass = p;
        }

        public void SetSquadron(string s)
        {
            if (!string.IsNullOrWhiteSpace(s))
                this.squadron = s;
        }

        public void SetSignature(string s)
        {
            if (!string.IsNullOrWhiteSpace(s))
                this.signature = s;
        }

        public void SetChatMacro(int n, string m)
        {
            if (n >= 0 && n < 10 && !string.IsNullOrWhiteSpace(m))
                chat_macros[n] = m;
        }

        public void SetCreateDate(DateTime d)
        {
            create_date = d;
        }
        public void SetRank(int r)
        {
            foreach (AwardInfo award in AwardInfo.RankTable)
            {
                if (r == award.id)
                    points = award.total_points;
            }
        }

        public void SetPoints(int p)
        {
            if (p >= 0)
                points = p;
        }

        public void SetMedals(int m)
        {
            medals = m;
        }
        public void SetCampaigns(int n)
        {
            campaigns = n;
        }
        public void SetTrained(int n)
        {
            if (n == 0)
                trained = 0;

            else if (n > 0 && n <= 20)
                trained = trained | (1 << (n - 1));

            else if (n > 20)
                trained = n;
        }

        public void SetFlightTime(double t)
        {
            if (t >= 0)
                flight_time = TimeSpan.FromSeconds(t);
        }
        public void SetMissions(int m)
        {
            if (m >= 0)
                missions = m;
        }

        public void SetKills(int k)
        {
            if (k >= 0)
                kills = k;
        }

        public void SetLosses(int l)
        {
            if (l >= 0)
                losses = l;
        }

        public void AddFlightTime(double t)
        {
            if (t > 0)
                flight_time.Add(TimeSpan.FromSeconds(t));
        }

        public void AddPoints(int p)
        {
            if (p > 0)
                points += p;
        }

        // public void AddMedal(int m)


        public void AddMissions(int m)
        {
            if (m > 0)
                missions += m;
        }

        public void AddKills(int k)
        {
            if (k > 0)
                kills += k;
        }

        public void AddLosses(int l)
        {
            if (l > 0)
                losses += l;
        }

        public bool HasTrained(int n)
        {
            if (n > 0 && n <= 20)
                return ((trained & (1 << (n - 1))) != 0) ? true : false;

            return false;
        }

        public bool HasCompletedCampaign(int id)
        {
            if (id > 0 && id < 30)
                return ((campaigns & (1 << id)) != 0) ? true : false;

            return false;
        }

        public void SetCampaignComplete(int id)
        {
            if (id > 0 && id < 30)
            {
                campaigns = campaigns | (1 << id);
                Save();
            }
        }

        public void SetFlightModel(FLIGHT_MODEL n)
        {
            if (n >= FLIGHT_MODEL.FM_STANDARD && n <= FLIGHT_MODEL.FM_ARCADE)
            {
                flight_model = n;
                Ship.SetFlightModel(n);
            }
        }

        public void SetFlyingStart(int n)
        {
            flying_start = n;
        }

        public void SetLandingModel(LANDING_MODEL n)
        {
            if (n >= LANDING_MODEL.LM_STANDARD && n <= LANDING_MODEL.LM_EASIER)
            {
                landing_model = n;
                Ship.SetLandingModel(landing_model);
            }
        }

        public void SetAILevel(int n)
        {
            ai_level = n;
        }

        public void SetHUDMode(int n)
        {
            hud_mode = n;
            HUDView.SetArcade(n > 0);
        }

        public void SetHUDColor(int n)
        {
            hud_color = n;
            HUDView.SetDefaultColorSet(n);
        }

        public void SetFriendlyFire(int n)
        {
            if (n >= 0 && n <= 4)
            {
                ff_level = n;
                Ship.SetFriendlyFireLevel(n / 4.0);
            }
        }

        public void SetGridMode(int n)
        {
            if (n >= 0 && n <= 1)
            {
                grid = n;
            }
        }

        public void SetGunsight(int n)
        {
            if (n >= 0 && n <= 1)
            {
                gunsight = n;
            }
        }

        public void ClearShowAward()
        {
            award = null;
        }
        public string EncodeStats() { throw new System.NotImplementedException(); }
        public void DecodeStats(string stats) { throw new System.NotImplementedException(); }

        public int GetMissionPoints(ShipStats s, DWORD start_time)
        {
            int result = 0;

            if (s != null)
            {
                result = s.GetPoints();

                uint flight_time = ((uint)Game.GameTime() - start_time) / 1000;

                // if player survived mission, award one experience point
                // for each minute of action, in ten point blocks:
                if (s.GetDeaths() == 0 && s.GetColls() == 0)
                {
                    uint minutes = flight_time / 60;
                    minutes /= 10;
                    minutes *= 10;
                    result += (int)minutes;

                    if (s.HasEvent(SimEvent.EVENT.DOCK))
                        result += 100;
                }
                else
                {
                    result -= (int)(2.5 * Ship.Value(s.GetShipClass()));
                }

                if (result < 0)
                    result = 0;
            }

            return result;
        }

        public void ProcessStats(ShipStats s, DWORD start_time)
        {
            if (s == null) return;

            int old_rank = Rank();
            int pts = GetMissionPoints(s, start_time);

            AddPoints(pts);
            AddPoints(s.GetCommandPoints());
            AddKills(s.GetGunKills());
            AddKills(s.GetMissileKills());
            AddLosses(s.GetDeaths());
            AddLosses(s.GetColls());
            AddMissions(1);
            AddFlightTime((int)((Game.GameTime() - start_time) / 1000));

            int rank = Rank();

            // did the player earn a promotion?
            if (old_rank != rank)
            {
                foreach (AwardInfo a in rank_table)
                {
                    if (rank == a.id)
                    {
                        award = a;
                    }
                }
            }

            // if not, did the player earn a medal?
            else
            {
                foreach (AwardInfo a in medal_table)
                {

                    if (EarnedAward(a, s) && a.ceremony)
                    {
                        award = a;
                        break;
                    }
                }
            }

            // persist all stats, promotions, and medals:
            Save();
        }

        public bool EarnedAward(AwardInfo a, ShipStats s)
        {
            if (a == null || s == null)
                return false;

            // already earned this medal?
            if (a.id != 0 & medals != 0)
                return false;

            // eligible for this medal?
            int rank = Rank();
            if (a.min_rank > rank || a.max_rank < rank)
                return false;

            if ((a.required_awards & medals) < a.required_awards)
                return false;

            if (a.minval_ship_class > (int)s.GetShipClass() || a.maxval_ship_class < (int)s.GetShipClass())
                return false;

            if (a.total_points > points)
                return false;

            if (a.total_missions > missions)
                return false;

            if (a.campaign_id != 0 && a.campaign_complete)
            {
                if (!HasCompletedCampaign(a.campaign_id))
                    return false;
            }

            else
            {
                // campaign related requirements
                Campaign c = Campaign.GetCampaign();

                if (c != null)
                {
                    if (a.dynamic_campaign && !c.IsDynamic())
                        return false;
                }
            }

            // sufficient merit for this medal?
            if (a.mission_points > s.GetPoints())
                return false;

            if (a.kills > s.GetGunKills() + s.GetMissileKills())
                return false;

            if (a.mission_points > s.GetPoints())
                return false;

            // player must survive mission if lost = -1
            if (a.lost < 0 && (s.GetDeaths() != 0 || s.GetColls() != 0))
                return false;

            // do we need to be wounded in battle?
            if (a.lost > s.GetDeaths() || a.collision > s.GetColls())
                return false;

            // final lottery check:
            if (a.lottery < 2 || RandomHelper.RandomChance(1, a.lottery))
            {
                medals |= a.id;
                return true;
            }

            // what do we have for the losers, judge?
            return false;
        }

        public static string RankName(int rank)
        {
            foreach (AwardInfo award in AwardInfo.RankTable)
            {
                if (award.id == rank)
                    return award.name;
            }

            return "Conscript";
        }

        public static string RankAbrv(int rank)
        {
            foreach (AwardInfo award in AwardInfo.RankTable)
            {
                if (award.id == rank)
                    return award.abrv;
            }

            return "";
        }

        public static int RankFromName(string name)
        {
            foreach (AwardInfo award in AwardInfo.RankTable)
            {
                if (award.name == name)
                    return award.id;
            }

            return 0;
        }

        public static Bitmap RankInsignia(int rank, int size)
        {
            foreach (AwardInfo award in AwardInfo.RankTable)
            {
                if (award.id == rank)
                {
                    if (size == 0)
                        return award.small_insignia;

                    if (size == 1)
                        return award.large_insignia;
                }
            }

            return null;
        }

        public static string RankDescription(int rank)
        {
            foreach (AwardInfo award in AwardInfo.RankTable)
            {
                if (award.id == rank)
                    return award.desc;
            }

            return "";
        }

        public static string MedalName(int medal)
        {
            foreach (AwardInfo award in AwardInfo.MedalTable)
            {
                if (award.id == medal)
                    return award.name;
            }

            return "";
        }

        public static Bitmap MedalInsignia(int medal, int size)
        {
            foreach (AwardInfo award in AwardInfo.MedalTable)
            {
                if (award.id == medal)
                {
                    if (size == 0)
                        return award.small_insignia;

                    if (size == 1)
                        return award.large_insignia;
                }
            }

            return null;
        }

        public static string MedalDescription(int medal)
        {
            foreach (AwardInfo award in AwardInfo.MedalTable)
            {
                if (award.id == medal)
                    return award.desc;
            }

            return "";
        }

        public static int CommandRankRequired(int ship_class)
        {
            for (int i = 0; i < AwardInfo.RankTable.Count; i++)
            {
                AwardInfo award = AwardInfo.RankTable[i];
                if ((ship_class & award.granted_ship_classes) != 0)
                {
                    return i;
                }
            }

            return AwardInfo.RankTable.Count - 1;
        }

        public static List<Player> GetRoster()
        {
            return player_roster;
        }

        public static Player GetCurrentPlayer()
        {
            return current_player;
        }

        public static void SelectPlayer(Player p)
        {
            ErrLogger.PrintLine("Player SelectPlayer() is not yet Implemented");

            HUDView hud = HUDView.GetInstance();

            if (current_player != null && current_player != p)
            {
                if (hud != null)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        MFD mfd = hud.GetMFD(i);

                        if (mfd != null)
                            current_player.mfd[i] = mfd.GetMode();
                    }
                }
            }

            if (player_roster.Contains(p))
            {
                current_player = p;

                Ship.SetFlightModel(p.flight_model);
                Ship.SetLandingModel(p.landing_model);
                HUDView.SetArcade(p.hud_mode > 0);
                HUDView.SetDefaultColorSet(p.hud_color);

                if (hud != null)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        if (p.mfd[i] >= 0)
                        {
                            MFD mfd = hud.GetMFD(i);

                            if (mfd != null)
                                mfd.SetMode(p.mfd[i]);
                        }
                    }
                }
            }
        }

        public static Player Create(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                // check for existence:
                if (Find(name) != null)
                    return null;
                Player newbie = new Player(name);
                newbie.SetCreateDate(DateTime.Now);

                player_roster.Add(newbie);
                newbie.CreateUniqueID();
                return newbie;
            }

            return null;
        }

        public static void Destroy(Player p)
        {
            if (p != null)
            {
                player_roster.Remove(p);

                if (p == current_player)
                {
                    current_player = null;

                    if (player_roster.Count != 0)
                        current_player = player_roster[0];
                }

                //TODO CampaignSaveGame.RemovePlayer(p);
                //delete p;
            }
        }

        public static Player Find(string name)
        {
            for (int i = 0; i < player_roster.Count; i++)
            {
                Player p = player_roster[i];
                if (p.Name() == name)
                    return p;
            }

            return null;
        }

        public static void Initialize()
        {
            AwardInfo.LoadAwardTables();
            Load();

            if (current_player == null)
            {
                if (player_roster.Count == 0)
                {
                    Create("Pilot");
                }

                SelectPlayer(player_roster[0]);
            }
        }

        public static void Close()
        {
            player_roster.Clear();
            current_player = null;
            AwardInfo.Clear();
        }

        /// <summary>
        /// Loads the player configuration entries.
        /// </summary>
        public static void Load(string filename = PlayerFilename)
        {
            config_exists = false;

            if (!ReaderSupport.DoesDataFileExist(filename))
            {
                if (current_player == null && player_roster.Count == 0)
                {
                    current_player = Create("Pilot");
                    Save();
                }
                return;
            }
            var roster = ReaderSupport.DeserializeData<Roster>(filename);
            if (roster == null || roster.players == null)
            {
                ErrLogger.PrintLine("WARNING: player structure missing in '{0}'", filename);
                return;
            }
            player_roster.Clear();
            foreach (PlayerInfo pi in roster.players)
            {
                Player p = pi;

                if (pi.current)
                    current_player = p;
                player_roster.Add(p);
            }
            config_exists = true;
        }


        /// <summary>
        /// Saves the player roster entries.
        /// </summary>
        /// <param name="filename">the file to save. By default it uses Player.def</param>
        public static void Save(string filename = PlayerFilename)
        {
            Roster roster = new Roster()
            {
                players = new List<PlayerInfo>()
            };
            foreach (Player player in player_roster)
            {
                PlayerInfo pi = player;
                if (current_player == player)
                    pi.current = true;
                roster.players.Add(pi);
            }
            ReaderSupport.SerializeData<Roster>(roster, filename);
        }


        public static bool ConfigExists()
        {
            return config_exists;
        }

        protected Player() { }

        protected void CreateUniqueID()
        {
            foreach (Player p in player_roster)
            {
                if (!this.Equals(p) && p.uid >= uid)
                    uid = p.uid + 1;
            }

            if (uid < 1)
                uid = 1;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return this.name == ((Player)obj).name;
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }


        // Implicit conversion from PlayerInfo to Player
        public static implicit operator Player(PlayerInfo playerinfo)
        {
            Player p = new Player()
            {
                name = playerinfo.name,
                squadron = playerinfo.squadron,
                signature = playerinfo.signature,
                trained = playerinfo.trained,
                flight_model = (FLIGHT_MODEL)playerinfo.flight_model,
                flying_start = playerinfo.flying_start,
                ai_level = playerinfo.ai_level,
                hud_mode = playerinfo.hud_mode,
                hud_color = playerinfo.hud_color,
                ff_level = playerinfo.ff_level,
                grid = playerinfo.grid,
                gunsight = playerinfo.gunsight,
            };
            p.chat_macros[0] = playerinfo.chat_0;
            p.chat_macros[1] = playerinfo.chat_1;
            p.chat_macros[2] = playerinfo.chat_2;
            p.chat_macros[3] = playerinfo.chat_3;
            p.chat_macros[4] = playerinfo.chat_4;
            p.chat_macros[5] = playerinfo.chat_5;
            p.chat_macros[6] = playerinfo.chat_6;
            p.chat_macros[7] = playerinfo.chat_7;
            p.chat_macros[8] = playerinfo.chat_8;
            p.chat_macros[9] = playerinfo.chat_9;
            p.mfd[0] = playerinfo.mfd0;
            p.mfd[1] = playerinfo.mfd1;
            p.mfd[2] = playerinfo.mfd2;

            var stat = playerinfo.GetStats();
            p.pass = stat.password;
            p.create_date = stat.create_date;
            p.points = stat.points;
            p.flight_time = stat.flight_time;
            p.medals = stat.medals;
            p.missions = stat.missions;
            p.kills = stat.kills;
            p.losses = stat.losses;
            p.campaigns = stat.campaigns;

            return p;
        }
        //  Implicit conversion from Player to PlayerInfo
        public static implicit operator PlayerInfo(Player player)
        {
            PlayerInfo pi = new PlayerInfo()
            {
                name = player.name,
                squadron = player.squadron,
                signature = player.signature,
                trained = player.trained,
                flight_model = (int)player.flight_model,
                flying_start = player.flying_start,
                ai_level = player.ai_level,
                hud_mode = player.hud_mode,
                hud_color = player.hud_color,
                ff_level = player.ff_level,
                grid = player.grid,
                gunsight = player.gunsight,
            };
            pi.chat_0 = player.chat_macros[0];
            pi.chat_1 = player.chat_macros[1];
            pi.chat_2 = player.chat_macros[2];
            pi.chat_3 = player.chat_macros[3];
            pi.chat_4 = player.chat_macros[4];
            pi.chat_5 = player.chat_macros[5];
            pi.chat_6 = player.chat_macros[6];
            pi.chat_7 = player.chat_macros[7];
            pi.chat_8 = player.chat_macros[8];
            pi.chat_9 = player.chat_macros[9];
            pi.mfd0 = player.mfd[0];
            pi.mfd1 = player.mfd[1];
            pi.mfd2 = player.mfd[2];

            Stats stat = new Stats()
            {
                password = player.pass,
                create_date = player.create_date,
                points = player.points,
                flight_time = player.flight_time,
                medals = player.medals,
                missions = player.missions,
                kills = player.kills,
                losses = player.losses,
                campaigns = player.campaigns,
            };
            pi.SetStats(stat);

            return pi;
        }

        protected int uid = 0;
        protected string name;
        protected string pass;
        protected string squadron;
        protected string signature;
        protected string[] chat_macros = new string[10];
        protected MFD.Modes[] mfd = new MFD.Modes[4] { 0, 0, 0, 0 };

        // stats:
        protected DateTime create_date;
        protected int points = 0;
        protected int medals = 0;        // bitmap of earned medals
        protected TimeSpan flight_time = new TimeSpan();
        protected int missions = 0;
        protected int kills = 0;
        protected int losses = 0;
        protected int campaigns = 0;     // bitmap of completed campaigns
        protected int trained = 0;       // id of highest training mission completed

        // gameplay options:
        protected FLIGHT_MODEL flight_model = FLIGHT_MODEL.FM_STANDARD;
        protected int flying_start = 0;
        protected LANDING_MODEL landing_model = 0;
        protected int ai_level = 1;
        protected int hud_mode = 0;
        protected int hud_color = 1;
        protected int ff_level = 4;
        protected int grid = 1;
        protected int gunsight = 0;

        // transient:
        protected AwardInfo award;

        private static bool config_exists = false;
        private static List<Player> player_roster = new List<Player>();
        private static Player current_player = null;

        private const string PlayerFilename = "player.json";

        private static List<AwardInfo> rank_table = new List<AwardInfo>();
        private static List<AwardInfo> medal_table = new List<AwardInfo>();
    }

    [Serializable]
    public class Roster
    {
        public List<PlayerInfo> players;
    }

    [Serializable]
    public class PlayerInfo
    {
        public int uid;
        public string name;
        public string squadron;
        public string signature;
        public string[] stats;
        public bool current;
        public int trained;
        public int flight_model;
        public int flying_start;
        public int landing_model;
        public int ai_level;
        public int hud_mode;
        public int hud_color;
        public int ff_level;
        public int grid;
        public int gunsight;
        public string chat_0;
        public string chat_1;
        public string chat_2;
        public string chat_3;
        public string chat_4;
        public string chat_5;
        public string chat_6;
        public string chat_7;
        public string chat_8;
        public string chat_9;
        public MFD.Modes mfd0;
        public MFD.Modes mfd1;
        public MFD.Modes mfd2;

        public Stats GetStats()
        {
            string allStats = string.Join("", stats);
            var plain = Encryption.Decrypt(Encryption.Decode(allStats));

            char[] stat_buf = new char[plain.Length];
            if (plain.Length == 64)
            {
                for (int i = 0; i < 8; i++)
                    for (int j = 0; j < 8; j++)
                        stat_buf[j * 8 + i] = plain[i * 8 + j];
            }

            else if (plain.Length == 256)
            {
                for (int i = 0; i < 16; i++)
                    for (int j = 0; j < 16; j++)
                        stat_buf[j * 16 + i] = plain[i * 16 + j];
            }

            else
            {
                ErrLogger.PrintLine("Player::DecodeStats() invalid plain text length {0}", plain.Length);
                return new Stats();
            }

            var stat = new Stats();
            stat.password = SubArray(stat_buf, 0, 16).TrimEnd();
            stat.create_date = DateTimeHelper.UnixTimeStampToDateTime(Convert.ToInt64(SubArray(stat_buf, 16, 8), 16));
            stat.points = Convert.ToInt32(SubArray(stat_buf, 24, 8), 16);
            stat.flight_time = TimeSpan.FromSeconds(Convert.ToInt32(SubArray(stat_buf, 32, 8), 16));
            stat.missions = Convert.ToInt32(SubArray(stat_buf, 40, 8), 16);
            stat.kills = Convert.ToInt32(SubArray(stat_buf, 48, 8), 16);
            stat.losses = Convert.ToInt32(SubArray(stat_buf, 56, 8), 16);
            if (plain.Length > 64)
            {
                stat.medals = Convert.ToInt32(SubArray(stat_buf, 64, 8), 16);
                stat.campaigns = Convert.ToInt32(SubArray(stat_buf, 72, 8), 16);
            }
            return stat;
        }
        private static string SubArray(char[] data, int index, int length)
        {
            char[] result = new char[length];
            Array.Copy(data, index, result, 0, length);
            return new string(result);
        }

        public void SetStats(Stats statsInfo)
        {
            //"%-16s%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x%08x",
            var str = String.Format("{0,-16:s}{1:x8}{2:x8}{3:x8}{4:x8}{5:x8}{6:x8}{7:x8}{8:x8}{9:x8}{10:x8}" +
                                    "{11:x8}{12:x8}{13:x8}{14:x8}{15:x8}{16:x8}{17:x8}{18:x8}{19:x8}{20:x8}" +
                                    "{21:x8}{22:x8}{23:x8}{24:x8}{25:x8}{26:x8}{27:x8}{28:x8}{29:x8}{30:x8}",
                       statsInfo.password, //0
                       (long)DateTimeHelper.DateTimeToUnixTimestamp(statsInfo.create_date),
                       statsInfo.points,
                       (long)statsInfo.flight_time.TotalSeconds,
                       statsInfo.missions,
                       statsInfo.kills,
                       statsInfo.losses,
                       statsInfo.medals,
                       statsInfo.campaigns,
                       11, 12, 13, 14, 15, 16,
                       17, 18, 19, 20, 21, 22, 23, 24,
                       25, 26, 27, 28, 29, 30, 31, 32);
            byte[] stat_buf = Encoding.ASCII.GetBytes(str);
            byte[] code_buf = new byte[stat_buf.Length];
            for (int i = 0; i < 16; i++)
                for (int j = 0; j < 16; j++)
                    code_buf[i * 16 + j] = stat_buf[j * 16 + i];

            var stat = Encryption.Encode(Encryption.Encrypt(code_buf));
            if (stat.Length != 32 * 16)
            {
                ErrLogger.PrintLine("ERROR: PlayerInfo.SetStats() invalid stat length {0}", stat);
                throw new ArgumentException();
            }
            stats = Split(stat, 32);
        }
        private static string[] Split(string str, int chunkSize)
        {
            string[] chunks = new string[str.Length / chunkSize];
            for (int i = 0; i < chunks.Length; i++)
                chunks[i] = str.Substring(i * chunkSize, chunkSize);
            return chunks;
        }
    }
    public class Stats
    {
        public string password;
        public DateTime create_date;
        public int points;
        public TimeSpan flight_time;
        public int medals;
        public int missions;
        public int kills;
        public int losses;
        public int campaigns;
    }
}
