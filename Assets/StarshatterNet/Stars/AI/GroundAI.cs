﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      GroundAI.h/GroundAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Ground Unit (low-level) Artifical Intelligence class
*/
using System;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.SimElements;
 
namespace StarshatterNet.Stars.AI
{
    public class GroundAI : SimObserver, IDirector
    {

        public GroundAI(SimObject s)
        {
            ship = (Ship)s; target = null; subtarget = null; exec_time = 0; carrier_ai = null;
            Sim sim = Sim.GetSim();
            Ship pship = sim.GetPlayerShip();
            int player_team = 1;
            int ai_level = 1;

            if (pship != null)
                player_team = pship.GetIFF();

            Player player = Player.GetCurrentPlayer();
            if (player != null)
            {
                if (ship != null && ship.GetIFF() != 0 && ship.GetIFF() != player_team)
                {
                    ai_level = player.AILevel();
                }
                else if (player.AILevel() == 0)
                {
                    ai_level = 1;
                }
            }

            // evil alien ships are *always* smart:
            if (ship != null && ship.GetIFF() > 1 && ship.Design().auto_roll > 1)
            {
                ai_level = 2;
            }

            if (ship != null && ship.GetHangar() != null && ship.GetCommandAILevel() > 0)
                carrier_ai = new CarrierAI(ship, ai_level);
        }
        // ~GroundAI();

        public virtual void ExecFrame(double secs)
        {
            const int exec_period = 1000;

            if ((int)Game.GameTime() - exec_time > exec_period)
            {
                exec_time = (int)Game.GameTime();
                SelectTarget();
            }

            if (ship != null)
            {
                Shield shield = ship.GetShield();

                if (shield != null)
                    shield.SetPowerLevel(100);

                foreach (WeaponGroup group in ship.Weapons())
                {
                    if (group.NumWeapons() > 1 && group.CanTarget(CLASSIFICATION.DROPSHIPS))
                        group.SetFiringOrders(Weapon.Orders.POINT_DEFENSE);
                    else
                        group.SetFiringOrders(Weapon.Orders.AUTO);

                    group.SetTarget((Ship)target, null);
                }

                if (carrier_ai != null)
                    carrier_ai.ExecFrame(secs);
            }
        }
        public virtual void SetTarget(SimObject targ, ShipSystem sub = null)
        {
            if (target != targ)
            {
                target = targ;

                if (target != null)
                    Observe(target);
            }

            subtarget = sub;
        }

        public virtual SimObject GetTarget() { return target; }
        public virtual ShipSystem GetSubTarget() { return subtarget; }
        public virtual SteerAI.SteerType Type()
        {
            return SteerAI.SteerType.GROUND;
        }

        public virtual bool Subframe() { return false; }

        public override bool Update(SimObject obj)
        {
            if (obj == target)
            {
                target = null;
                subtarget = null;
            }

            return base.Update(obj);
        }

        public override string GetObserverName()
        {
            string name = string.Format("GroundAI({0})", ship.Name());
            return name;
        }

        protected virtual void SelectTarget()
        {
            SimObject potential_target = null;

            // pick the closest combatant ship with a different IFF code:
            double target_dist = 1.0e15;

            Ship current_ship_target = null;

            foreach (Contact contact in ship.ContactList())
            {
                int c_iff = contact.GetIFF(ship);
                Ship c_ship = contact.GetShip();
                Shot c_shot = contact.GetShot();
                bool rogue = false;

                if (c_ship != null)
                    rogue = c_ship.IsRogue();

                if (rogue || c_iff > 0 && c_iff != ship.GetIFF() && c_iff < 1000)
                {
                    if (c_ship != null && !c_ship.InTransition())
                    {
                        // found an enemy, check distance:
                        double dist = (ship.Location() - c_ship.Location()).Length;

                        if (current_ship_target == null || (c_ship.Class() <= current_ship_target.Class() &&
                                    dist < target_dist))
                        {
                            current_ship_target = c_ship;
                            target_dist = dist;
                        }
                    }
                }

                potential_target = current_ship_target;
            }

            SetTarget(potential_target);
        }

        protected Ship ship;
        protected SimObject target;
        protected ShipSystem subtarget;
        protected double exec_time;
        protected CarrierAI carrier_ai;
    }
}