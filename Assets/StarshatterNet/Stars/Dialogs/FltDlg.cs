﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      FltDlg.h/FltDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Flight Operations Active Window class
*/
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.AI;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class FltDlg : FormWindow
    {
        public FltDlg(Screen s, Camera c, FormDef def, GameScreen mgr, GameObject p) :
                  base(s, 0, 0, s.Width(), s.Height(), c, p, "FltDlg")
        {
            manager = mgr;
            ship = null; filter_list = null; hangar_list = null;
            package_btn = null; alert_btn = null; launch_btn = null; stand_btn = null; recall_btn = null;
            mission_type = -1; flight_planner = null; patrol_pattern = 0;

            Init(def);
        }
        //public virtual ~FltDlg();
        public override void RegisterControls()
        {
            filter_list = (ComboBox)FindControl(101);
            hangar_list = (ListBox)FindControl(102);
            package_btn = (Button)FindControl(110);
            alert_btn = (Button)FindControl(111);
            launch_btn = (Button)FindControl(112);
            stand_btn = (Button)FindControl(113);
            recall_btn = (Button)FindControl(114);
            close_btn = (Button)FindControl(1);

            if (filter_list != null)
                REGISTER_CLIENT(ETD.EID_SELECT, filter_list, OnFilter);

            if (package_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, package_btn, OnPackage);

            if (alert_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, alert_btn, OnAlert);

            if (launch_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, launch_btn, OnLaunch);

            if (stand_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, stand_btn, OnStandDown);

            if (recall_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, recall_btn, OnRecall);

            if (close_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, close_btn, OnClose);

            for (int i = 0; i < 6; i++)
            {
                mission_btn[i] = (Button)FindControl(210 + i);
                if (mission_btn[i] != null)
                    REGISTER_CLIENT(ETD.EID_CLICK, mission_btn[i], OnMissionType);
            }

            objective_list = (ListBox)FindControl(221);
            loadout_list = (ListBox)FindControl(222);
        }
        public override void Show()
        {
            if (shown) return;

            base.Show();
            UpdateSelection();
            UpdateObjective();
        }

        // Operations:
        public override void Hide()
        {
            base.Hide();
        }
        public virtual void OnFilter(ActiveWindow obj, AWEvent evnt)
        {
            if (filter_list == null || hangar_list == null) return;

            int seln = filter_list.GetSelectedIndex();

            hangar_list.ClearItems();

            if (ship == null) return;

            Hangar hangar = ship.GetHangar();

            // selected squadron:
            if (seln < hangar.NumSquadrons())
            {
                int nslots = hangar.SquadronSize(seln);

                for (int i = 0; i < nslots; i++)
                {
                    string txt;
                    txt = string.Format(" {0:D2}    ", i + 1);

                    HangarSlot s = hangar.GetSlot(seln, i);
                    hangar_list.AddItemWithData(txt, i);

                    hangar_list.SetItemText(i, 1, "--");
                    hangar_list.SetItemText(i, 2, hangar.StatusName(s));

                    FormatUtil.FormatTime(out txt, hangar.TimeRemaining(s));
                    hangar_list.SetItemText(i, 4, txt);
                }
            }

            // selected pending filter:
            else if (seln == hangar.NumSquadrons())
            {
                int item = 0;

                for (int f = 0; f < ship.NumFlightDecks(); f++)
                {
                    FlightDeck deck = ship.GetFlightDeck(f);

                    if (deck.IsLaunchDeck())
                    {
                        for (int i = 0; i < deck.NumSlots(); i++)
                        {
                            FlightDeck.FLIGHT_SLOT_STATE state = deck.State(i);
                            int seq = deck.Sequence(i);
                            double time = deck.TimeRemaining(i);
                            Ship deckship = deck.GetShip(i);

                            int squadron = -1;
                            int slot = -1;

                            string txt;
                            txt = string.Format("{0}-{1}    ", f + 1, i + 1);

                            hangar_list.AddItemWithData(txt, item); // use data for sort

                            if (deckship != null)
                            {
                                hangar_list.SetItemText(item, 1, deckship.Name());

                                HangarSlot s = null;

                                for (int a = 0; s == null && a < hangar.NumSquadrons(); a++)
                                {
                                    for (int b = 0; s == null && b < hangar.SquadronSize(a); b++)
                                    {
                                        HangarSlot test = hangar.GetSlot(a, b);
                                        if (hangar.GetShip(test) == deckship)
                                        {
                                            s = test;
                                            squadron = a;
                                            slot = b;
                                        }
                                    }
                                }

                                if (s != null)
                                {
                                    hangar_list.SetItemText(item, 2, hangar.StatusName(s));
                                    if (hangar.GetPackageElement(s) != null)
                                        hangar_list.SetItemText(item, 3, Mission.RoleName(hangar.GetPackageElement(s).Type()));
                                }
                            }
                            else
                            {
                                hangar_list.SetItemText(item, 1, "--");
                                hangar_list.SetItemText(item, 2, Game.GetText("FltDlg.Open"));
                            }


                            FormatUtil.FormatTime(out txt, time);
                            hangar_list.SetItemText(item, 4, txt);

                            hangar_list.SetItemData(item, 1, f);
                            hangar_list.SetItemData(item, 2, i);
                            hangar_list.SetItemData(item, 3, squadron);
                            hangar_list.SetItemData(item, 4, slot);

                            item++;
                        }
                    }
                }
            }

            // selected active filter:
            else if (seln == hangar.NumSquadrons() + 1)
            {
                int item = 0;

                for (int i = 0; i < hangar.NumSquadrons(); i++)
                {
                    int nslots = hangar.SquadronSize(i);

                    for (int j = 0; j < nslots; j++)
                    {
                        HangarSlot s = hangar.GetSlot(i, j);

                        if (hangar.GetState(s) >= Hangar.HANGAR_STATE.ACTIVE)
                        {
                            string txt;
                            txt = string.Format(" {0:D2}    ", item + 1);

                            hangar_list.AddItemWithData(txt, item); // use data for sort

                            if (hangar.GetShip(s) != null)
                                hangar_list.SetItemText(item, 1, hangar.GetShip(s).Name());

                            hangar_list.SetItemText(item, 2, hangar.StatusName(s));

                            if (hangar.GetPackageElement(s) != null)
                                hangar_list.SetItemText(item, 3, Mission.RoleName(hangar.GetPackageElement(s).Type()));

                            FormatUtil.FormatTime(out txt, hangar.TimeRemaining(s));
                            hangar_list.SetItemText(item, 4, txt);

                            hangar_list.SetItemData(item, 1, i);
                            hangar_list.SetItemData(item, 2, j);

                            item++;
                        }
                    }
                }
            }
        }
        public virtual void OnPackage(ActiveWindow obj, AWEvent evnt)
        {
            if (filter_list == null || hangar_list == null || ship == null) return;

            Mission.TYPE code = Mission.TYPE.PATROL;

            switch (mission_type)
            {
                case 0: code = Mission.TYPE.PATROL; break;
                case 1: code = Mission.TYPE.INTERCEPT; break;
                case 2: code = Mission.TYPE.ASSAULT; break;
                case 3: code = Mission.TYPE.STRIKE; break;
                case 4: code = Mission.TYPE.ESCORT; break;
                case 5: code = Mission.TYPE.INTEL; break;
            }

            int squad = filter_list.GetSelectedIndex();
            Hangar hangar = ship.GetHangar();
            Sim sim = Sim.GetSim();
            string call = sim.FindAvailCallsign(ship.GetIFF());
            Element elem = sim.CreateElement(call, ship.GetIFF(), code);
            Element tgt = null;
            FlightDeck deck = null;
            int queue = 1000;
            int[] load = null;

            elem.SetSquadron(hangar.SquadronName(squad));
            elem.SetCarrier(ship);

            if (objective_list != null)
            {
                int index = objective_list.GetListIndex();
                string target = objective_list.GetItemText(index);

                Instruction objective = new Instruction((Instruction.ACTION)code, target);
                elem.AddObjective(objective);

                tgt = sim.FindElement(target);
            }

            if (loadout_list != null && design != null)
            {
                int index = loadout_list.GetListIndex();
                string loadname = loadout_list.GetItemText(index);

                foreach (ShipLoad sl in design.loadouts)
                {
                    if (load != null) break;
                    if (sl.name == loadname)
                    {
                        load = sl.load;
                        elem.SetLoadout(load);
                    }
                }
            }

            for (int i = 0; i < ship.NumFlightDecks(); i++)
            {
                FlightDeck d = ship.GetFlightDeck(i);

                if (d != null && d.IsLaunchDeck())
                {
                    int dq = hangar.PreflightQueue(d);

                    if (dq < queue)
                    {
                        queue = dq;
                        deck = d;
                    }
                }
            }

            int npackage = 0;
            int[] slots = new int[4];

            for (int i = 0; i < 4; i++)
                slots[i] = -1;

            for (int i = 0; i < hangar_list.NumItems(); i++)
            {
                if (hangar_list.IsSelected(i))
                {
                    int nslot = (int)hangar_list.GetItemData(i);
                    hangar.GotoAlert(squad, nslot, deck, elem, load, true);
                    slots[npackage] = nslot;
                    hangar_list.SetSelected(i, false);
                    npackage++;

                    if (npackage >= 4)
                        break;
                }
            }

            NetUtil.SendElemCreate(elem, squad, slots, false);

            if (flight_planner != null)
            {
                switch (mission_type)
                {
                    case 0:
                    default:
                        flight_planner.CreatePatrolRoute(elem, patrol_pattern++);
                        break;

                    case 1:
                    case 2:
                    case 3:
                        if (tgt != null)
                            flight_planner.CreateStrikeRoute(elem, tgt);
                        else
                            flight_planner.CreatePatrolRoute(elem, patrol_pattern++);
                        break;

                    case 4:
                        if (tgt != null)
                            flight_planner.CreateEscortRoute(elem, tgt);
                        else
                            flight_planner.CreatePatrolRoute(elem, patrol_pattern++);
                        break;
                }

                if (patrol_pattern < 0 || patrol_pattern > 3)
                    patrol_pattern = 0;
            }
        }
        public virtual void OnAlert(ActiveWindow obj, AWEvent evnt)
        {
            if (filter_list == null || hangar_list == null || ship == null) return;

            int squad = filter_list.GetSelectedIndex();
            Hangar hangar = ship.GetHangar();
            Sim sim = Sim.GetSim();
            string call = sim.FindAvailCallsign(ship.GetIFF());
            Element elem = sim.CreateElement(call, ship.GetIFF());
            FlightDeck deck = null;
            int queue = 1000;
            int[] load = null;

            elem.SetSquadron(hangar.SquadronName(squad));
            elem.SetCarrier(ship);

            for (int i = 0; i < ship.NumFlightDecks(); i++)
            {
                FlightDeck d = ship.GetFlightDeck(i);

                if (d != null && d.IsLaunchDeck())
                {
                    int dq = hangar.PreflightQueue(d);

                    if (dq < queue)
                    {
                        queue = dq;
                        deck = d;
                    }
                }
            }

            int nalert = 0;
            int[] slots = new int[4];

            for (int i = 0; i < 4; i++)
                slots[i] = -1;

            for (int i = 0; i < hangar_list.NumItems(); i++)
            {
                if (hangar_list.IsSelected(i))
                {
                    int nslot = (int)hangar_list.GetItemData(i);
                    slots[nalert] = nslot;

                    if (load == null)
                    {
                        HangarSlot hangar_slot = hangar.GetSlot(squad, nslot);
                        if (hangar_slot != null)
                        {
                            load = hangar.GetLoadout(hangar_slot);
                            elem.SetLoadout((int[])load);
                        }
                    }

                    hangar.GotoAlert(squad, nslot, deck, elem);
                    hangar_list.SetSelected(i, false);
                    nalert++;

                    if (nalert >= 4)
                        break;
                }
            }

            NetUtil.SendElemCreate(elem, squad, slots, true);
        }
        public virtual void OnLaunch(ActiveWindow obj, AWEvent evnt)
        {
            if (filter_list == null || hangar_list == null || ship == null) return;

            int squad = filter_list.GetSelectedIndex();

            Hangar hangar = ship.GetHangar();

            // selected squadron:
            if (squad < hangar.NumSquadrons())
            {
                for (int i = 0; i < hangar_list.NumItems(); i++)
                {
                    if (hangar_list.IsSelected(i))
                    {
                        int nslot = (int)hangar_list.GetItemData(i);
                        hangar.Launch(squad, nslot);
                        NetUtil.SendShipLaunch(ship, squad, nslot);
                    }
                }
            }

            // selected pending filter:
            else if (squad == hangar.NumSquadrons())
            {
                for (int item = 0; item < hangar_list.NumItems(); item++)
                {
                    if (hangar_list.IsSelected(item))
                    {
                        int squadron = (int)hangar_list.GetItemData(item, 3);
                        int slot = (int)hangar_list.GetItemData(item, 4);

                        if (squadron >= 0 && slot >= 0)
                        {
                            hangar.Launch(squadron, slot);
                            NetUtil.SendShipLaunch(ship, squadron, slot);
                        }
                    }
                }
            }
        }

        public virtual void OnStandDown(ActiveWindow obj, AWEvent evnt)
        {
            if (filter_list == null || hangar_list == null || ship == null) return;

            int seln = filter_list.GetSelectedIndex();

            Hangar hangar = ship.GetHangar();

            // selected squadron:
            if (seln < hangar.NumSquadrons())
            {
                for (int i = 0; i < hangar_list.NumItems(); i++)
                {
                    if (hangar_list.IsSelected(i))
                    {
                        int nslot = (int)hangar_list.GetItemData(i);
                        hangar.StandDown(seln, nslot);
                    }
                }
            }

            // selected pending filter:
            else if (seln == hangar.NumSquadrons())
            {
                for (int item = 0; item < hangar_list.NumItems(); item++)
                {
                    if (hangar_list.IsSelected(item))
                    {
                        int squadron = (int)hangar_list.GetItemData(item, 3);
                        int slot = (int)hangar_list.GetItemData(item, 4);

                        if (squadron >= 0 && slot >= 0)
                            hangar.StandDown(squadron, slot);
                    }
                }
            }
        }
        public virtual void OnRecall(ActiveWindow obj, AWEvent evnt)
        {
            if (filter_list == null || hangar_list == null || ship == null) return;

            int seln = filter_list.GetSelectedIndex();

            Hangar hangar = ship.GetHangar();

            // selected squadron:             or selected active filter:
            if (seln < hangar.NumSquadrons() || seln == hangar.NumSquadrons() + 1)
            {
                for (int i = 0; i < hangar_list.NumItems(); i++)
                {
                    if (hangar_list.IsSelected(i))
                    {
                        int nsquad = seln;
                        int nslot = (int)hangar_list.GetItemData(i);

                        if (seln > hangar.NumSquadrons())
                        {
                            nsquad = (int)hangar_list.GetItemData(i, 1);
                            nslot = (int)hangar_list.GetItemData(i, 2);
                        }

                        HangarSlot slot = hangar.GetSlot(nsquad, nslot);
                        Ship recall = hangar.GetShip(slot);

                        if (recall != null)
                        {
                            RadioMessage msg = new RadioMessage(recall, ship, RadioMessage.ACTION.RTB);
                            RadioTraffic.Transmit(msg);
                        }
                    }
                }
            }
        }
        public virtual void OnClose(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.CloseTopmost();
        }
        public virtual void OnMissionType(ActiveWindow obj, AWEvent evnt)
        {
            mission_type = -1;

            for (int i = 0; i < 6; i++)
            {
                if (mission_btn[i] != null)
                {
                    if (mission_btn[i] == evnt.window)
                    {
                        mission_btn[i].SetButtonState(1);
                        mission_type = i;
                    }
                    else
                    {
                        mission_btn[i].SetButtonState(0);
                    }
                }
            }

            if (objective_list != null && mission_type > -1)
            {
                objective_list.ClearItems();

                string txt = "";
                Sim sim = Sim.GetSim();

                foreach (Element elem in sim.GetElements())
                {
                    if (!elem.IsActive() || elem.IsFinished() || elem.IsSquadron())
                        continue;

                    CombatGroup group = elem.GetCombatGroup();
                    int iff = elem.GetIFF();
                    Ship s = elem.GetShip(1);
                    double r = 0;
                    bool con = false;

                    if (iff != ship.GetIFF())
                    {
                        if (elem.IntelLevel() < Intel.INTEL_TYPE.LOCATED)
                            continue;

                        if (group != null && group.IntelLevel() < Intel.INTEL_TYPE.LOCATED)
                            continue;
                    }

                    if (s != null)
                    {
                        Point s_loc = s.Location() + s.GetRegion().Location();
                        Point h_loc = ship.Location() + ship.GetRegion().Location();

                        r = (s_loc - h_loc).Length;

                        con = ship.FindContact(s) != null;

                        if (con)
                        {
                            FormatUtil.FormatNumber(out txt, r);
                        }
                        else
                        {
                            txt = string.Format(Game.GetText("FltDlg.Unknown"));
                            r = 2e9;
                        }
                    }

                    switch (mission_type)
                    {
                        case 1:  // INTERCEPT
                            if (iff != 0 && iff != ship.GetIFF() && s != null && s.IsDropship())
                            {
                                int item = objective_list.AddItem(elem.Name()) - 1;
                                objective_list.SetItemText(item, 1, s.GetRegion().Name());

                                objective_list.SetItemText(item, 2, txt);
                                objective_list.SetItemData(item, 2, (int)r);
                            }
                            break;

                        case 2:  // ASSAULT
                            if (iff != 0 && iff != ship.GetIFF() && s != null && (s.IsStarship() || s.IsStatic()))
                            {
                                int item = objective_list.AddItem(elem.Name()) - 1;
                                objective_list.SetItemText(item, 1, s.GetRegion().Name());

                                objective_list.SetItemText(item, 2, txt);
                                objective_list.SetItemData(item, 2, (int)r);
                            }
                            break;

                        case 3:  // STRIKE
                            if (iff != 0 && iff != ship.GetIFF() && s != null && s.IsGroundUnit())
                            {
                                int item = objective_list.AddItem(elem.Name()) - 1;
                                objective_list.SetItemText(item, 1, s.GetRegion().Name());

                                objective_list.SetItemText(item, 2, txt);
                                objective_list.SetItemData(item, 2, (int)r);
                            }
                            break;

                        case 4:  // ESCORT
                            if ((iff == 0 || iff == ship.GetIFF()) && (s == null || !s.IsStatic()))
                            {
                                int item = objective_list.AddItem(elem.Name()) - 1;

                                if (s != null)
                                {
                                    objective_list.SetItemText(item, 1, s.GetRegion().Name());
                                    objective_list.SetItemText(item, 2, txt);
                                    objective_list.SetItemData(item, 2, (int)r);
                                }

                                else
                                {
                                    objective_list.SetItemText(item, 1, "0");
                                    objective_list.SetItemData(item, 1, 0);
                                }
                            }
                            break;

                        case 5:  // SCOUT?
                            break;

                        default: break;
                    }
                }
            }

            if (loadout_list != null && mission_type > -1)
            {
                loadout_list.ClearItems();

                if (design != null)
                {
                    foreach (ShipLoad sl in design.loadouts)
                    {
                        int item = loadout_list.AddItem(sl.name) - 1;

                        string weight;
                        weight = string.Format("{0} kg", (int)((design.mass + sl.mass) * 1000));
                        loadout_list.SetItemText(item, 1, weight);
                        loadout_list.SetItemData(item, 1, (int)(sl.mass * 1000));
                    }
                }
            }
        }

        public virtual void ExecFrame()
        {
            if (ship == null || ship.GetHangar() == null)
            {
                manager.HideFltDlg();
            }
            else
            {
                UpdateSelection();
                UpdateObjective();
            }
        }

        public void SetShip(Ship s)
        {
            if (ship != s)
            {
                ship = s;

                //delete flight_planner;
                flight_planner = null;

                if (filter_list != null)
                {
                    filter_list.ClearItems();

                    if (ship != null)
                    {
                        int nsquadrons = 0;
                        int nslots = 0;
                        Hangar hangar = ship.GetHangar();

                        if (hangar != null)
                        {
                            nsquadrons = hangar.NumSquadrons();

                            for (int i = 0; i < nsquadrons; i++)
                            {
                                string filter;
                                filter = string.Format("{0} {1}",
                              hangar.SquadronDesign(i).abrv,
                              hangar.SquadronName(i));

                                filter_list.AddItem(filter);
                            }

                            filter_list.AddItem(Game.GetText("FltDlg.PENDING"));
                            filter_list.AddItem(Game.GetText("FltDlg.ACTIVE"));
                        }

                        flight_planner = new FlightPlanner(ship);
                    }

                    OnFilter(null, null);
                }
            }
        }
        public void UpdateSelection()
        {
            if (filter_list == null || hangar_list == null || ship == null) return;

            design = null;

            bool package = false;
            bool alert = false;
            bool launch = false;
            bool stand = false;
            bool recall = false;

            Hangar hangar = ship.GetHangar();
            int seln = filter_list.GetSelectedIndex();
            string txt;
            int item;

            // selected squadron:
            if (seln < hangar.NumSquadrons())
            {
                int nslots = hangar.SquadronSize(seln);

                for (item = 0; item < hangar_list.NumItems(); item++)
                {
                    int i = (int)hangar_list.GetItemData(item);
                    HangarSlot s = hangar.GetSlot(seln, i);

                    if (hangar.GetState(s) == Hangar.HANGAR_STATE.UNAVAIL)
                        hangar_list.SetItemColor(item, ColorExtensions.DarkGray);
                    else if (hangar.GetState(s) == Hangar.HANGAR_STATE.MAINT)
                        hangar_list.SetItemColor(item, Color.gray);
                    else
                        hangar_list.SetItemColor(item, Color.white);

                    if (hangar.GetState(s) > Hangar.HANGAR_STATE.STORAGE)
                    {
                        if (hangar.GetShip(s) != null)
                            hangar_list.SetItemText(item, 1, hangar.GetShip(s).Name());
                        else if (hangar.GetPackageElement(s) != null)
                            hangar_list.SetItemText(item, 1, hangar.GetPackageElement(s).Name());
                        else
                            hangar_list.SetItemText(item, 1, hangar.SquadronName(seln));

                        if (hangar.GetPackageElement(s) != null)
                            hangar_list.SetItemText(item, 3, Mission.RoleName(hangar.GetPackageElement(s).Type()));
                        else
                            hangar_list.SetItemText(item, 3, "--");

                    }
                    else
                    {
                        hangar_list.SetItemText(item, 1, "--");
                        hangar_list.SetItemText(item, 3, "--");
                    }

                    hangar_list.SetItemText(item, 2, hangar.StatusName(s));

                    if (hangar.GetState(s) >= Hangar.HANGAR_STATE.ACTIVE)
                    {
                        FormatUtil.FormatTime(out txt, hangar.GetShip(s).MissionClock() / 1000);
                        hangar_list.SetItemText(item, 4, txt);
                    }

                    else if (hangar.GetState(s) == Hangar.HANGAR_STATE.MAINT ||
                            hangar.GetState(s) > Hangar.HANGAR_STATE.STORAGE)
                    {
                        FormatUtil.FormatTime(out txt, hangar.TimeRemaining(s));
                        hangar_list.SetItemText(item, 4, txt);
                    }

                    else
                    {
                        hangar_list.SetItemText(item, 4, "");
                    }

                    if (hangar_list.IsSelected(item))
                    {
                        if (design == null) design = hangar.GetDesign(s);

                        switch (hangar.GetState(s))
                        {
                            case Hangar.HANGAR_STATE.STORAGE: alert = true; break;
                            case Hangar.HANGAR_STATE.ALERT:
                                launch = true;
                                stand = true; break;
                            case Hangar.HANGAR_STATE.QUEUED: stand = true; break;
                            case Hangar.HANGAR_STATE.ACTIVE: recall = true; break;
                        }
                    }
                }
            }

            // selected pending filter:
            else if (seln == hangar.NumSquadrons())
            {
                for (item = 0; item < hangar_list.NumItems(); item++)
                {
                    int f = (int)hangar_list.GetItemData(item, 1);
                    int i = (int)hangar_list.GetItemData(item, 2);

                    int squadron = -1;
                    int slot = -1;
                    HangarSlot s = null;

                    FlightDeck deck = ship.GetFlightDeck(f);

                    if (deck.IsLaunchDeck())
                    {
                        FlightDeck.FLIGHT_SLOT_STATE state = deck.State(i);
                        int seq = deck.Sequence(i);
                        double time = deck.TimeRemaining(i);
                        Ship deckship = deck.GetShip(i);

                        if (deckship != null)
                        {
                            hangar_list.SetItemText(item, 1, deckship.Name());

                            for (int a = 0; s == null && a < hangar.NumSquadrons(); a++)
                            {
                                for (int b = 0; s == null && b < hangar.SquadronSize(a); b++)
                                {
                                    HangarSlot test = hangar.GetSlot(a, b);
                                    if (hangar.GetShip(test) == deckship)
                                    {
                                        s = test;
                                        squadron = a;
                                        slot = b;
                                    }
                                }
                            }

                            if (s != null)
                            {
                                hangar_list.SetItemText(item, 2, hangar.StatusName(s));
                                if (hangar.GetPackageElement(s) != null)
                                    hangar_list.SetItemText(item, 3, Mission.RoleName(hangar.GetPackageElement(s).Type()));
                            }
                        }
                        else
                        {
                            hangar_list.SetItemText(item, 1, "--");
                            hangar_list.SetItemText(item, 2, Game.GetText("FltDlg.Open"));
                        }

                        FormatUtil.FormatTime(out txt, time);
                        hangar_list.SetItemText(item, 4, txt);
                        hangar_list.SetItemData(item, 3, squadron);
                        hangar_list.SetItemData(item, 4, slot);

                        if (hangar_list.IsSelected(item) && s != null)
                        {
                            if (design == null) design = hangar.GetDesign(s);

                            switch (hangar.GetState(s))
                            {
                                case Hangar.HANGAR_STATE.ALERT:
                                    launch = true;
                                    stand = true; break;
                                case Hangar.HANGAR_STATE.QUEUED: stand = true; break;
                            }
                        }
                    }
                }
            }

            // selected active filter:
            else if (seln == hangar.NumSquadrons() + 1)
            {
                int last_index = -1;

                for (item = 0; item < hangar_list.NumItems(); item++)
                {
                    int squadron = (int)hangar_list.GetItemData(item, 1);
                    int slot = (int)hangar_list.GetItemData(item, 2);

                    int nslots = hangar.SquadronSize(squadron);

                    if (slot >= 0 && slot < nslots)
                    {
                        HangarSlot s = hangar.GetSlot(squadron, slot);

                        if (hangar.GetState(s) > Hangar.HANGAR_STATE.STORAGE)
                        {
                            if (hangar.GetShip(s) != null)
                                hangar_list.SetItemText(item, 1, hangar.GetShip(s).Name());
                            else if (hangar.GetPackageElement(s) != null)
                                hangar_list.SetItemText(item, 1, hangar.GetPackageElement(s).Name());
                            else
                                hangar_list.SetItemText(item, 1, hangar.SquadronName(squadron));

                            if (hangar.GetPackageElement(s) != null)
                                hangar_list.SetItemText(item, 3, Mission.RoleName(hangar.GetPackageElement(s).Type()));
                            else
                                hangar_list.SetItemText(item, 3, "--");

                            hangar_list.SetItemText(item, 2, hangar.StatusName(s));

                            FormatUtil.FormatTime(out txt, hangar.GetShip(s).MissionClock() / 1000);
                            hangar_list.SetItemText(item, 4, txt);

                            if (last_index < (int)hangar_list.GetItemData(item))
                                last_index = (int)hangar_list.GetItemData(item);
                        }
                        else
                        {
                            hangar_list.RemoveItem(item);
                            item--;
                        }
                    }
                    else
                    {
                        hangar_list.RemoveItem(item);
                        item--;
                    }
                }

                for (int i = 0; i < hangar.NumSquadrons(); i++)
                {
                    int nslots = hangar.SquadronSize(i);

                    for (int j = 0; j < nslots; j++)
                    {
                        HangarSlot s = hangar.GetSlot(i, j);

                        if (hangar.GetState(s) >= Hangar.HANGAR_STATE.ACTIVE)
                        {
                            bool found = false;

                            for (int n = 0; !found && n < hangar_list.NumItems(); n++)
                            {
                                if ((int)hangar_list.GetItemData(n, 1) == i &&
                                        (int)hangar_list.GetItemData(n, 2) == j)
                                    found = true;
                            }

                            if (!found)
                            {
                                last_index++;

                                string txt2;
                                txt2 = string.Format("{0:D2}    ", last_index + 1);
                                hangar_list.AddItemWithData(txt2, last_index); // use data for sort

                                if (hangar.GetShip(s) != null)
                                    hangar_list.SetItemText(item, 1, hangar.GetShip(s).Name());

                                hangar_list.SetItemText(item, 2, hangar.StatusName(s));

                                if (hangar.GetPackageElement(s) != null)
                                    hangar_list.SetItemText(item, 3, Mission.RoleName(hangar.GetPackageElement(s).Type()));

                                FormatUtil.FormatTime(out txt2, hangar.GetShip(s).MissionClock() / 1000);
                                hangar_list.SetItemText(item, 4, txt2);

                                hangar_list.SetItemData(item, 1, i);
                                hangar_list.SetItemData(item, 2, j);

                                item++;
                            }
                        }
                    }
                }

                if (hangar_list.GetSelCount() > 0)
                    recall = true;
            }

            if (package_btn != null)
            {
                bool pkg_ok = alert && mission_type > -1;

                if (pkg_ok && mission_type > 0)
                {
                    pkg_ok = objective_list != null &&
                    objective_list.GetSelCount() > 0;

                    if (pkg_ok)
                    {
                        int obj_index = objective_list.GetSelection();
                        int obj_data = (int)objective_list.GetItemData(obj_index, 2);

                        if (obj_data > 1e9)
                            pkg_ok = false;
                    }
                }

                package_btn.SetEnabled(pkg_ok);
            }

            if (alert_btn != null)
            {
                alert_btn.SetEnabled(alert);

                for (int i = 0; i < 6; i++)
                    if (mission_btn[i] != null)
                        mission_btn[i].SetEnabled(alert);
            }

            if (launch_btn != null)
                launch_btn.SetEnabled(launch);

            if (stand_btn != null)
                stand_btn.SetEnabled(stand);

            if (recall_btn != null)
                recall_btn.SetEnabled(recall);
        }
        public void UpdateObjective()
        {
            if (objective_list == null || mission_type < 0 || ship == null) return;

            Sim sim = Sim.GetSim();
            string txt = "";

            for (int item = 0; item < objective_list.NumItems(); item++)
            {
                string obj_name = objective_list.GetItemText(item);

                Element elem = sim.FindElement(obj_name);

                // if element has expired, remove it from the objective list
                if (elem == null || !elem.IsActive() || elem.IsFinished())
                {
                    objective_list.RemoveItem(item);
                    item--;
                }

                // otherwise, the element is still active, so update range/region
                else
                {
                    Ship s = elem.GetShip(1);
                    double r = 0;
                    bool con = false;

                    if (s != null)
                    {
                        Point s_loc = s.Location() + s.GetRegion().Location();
                        Point h_loc = ship.Location() + ship.GetRegion().Location();

                        r = (s_loc - h_loc).Length;

                        con = ship.FindContact(s) != null;

                        if (con)
                        {
                            FormatUtil.FormatNumber(out txt, r);
                        }
                        else
                        {
                            txt = Game.GetText("FltDlg.Unknown");
                            r = 2e9;
                        }
                    }

                    objective_list.SetItemText(item, 1, s.GetRegion().Name());

                    objective_list.SetItemText(item, 2, txt);
                    objective_list.SetItemData(item, 2, (int)r);
                }
            }
        }


        protected GameScreen manager;

        protected ComboBox filter_list;
        protected ListBox hangar_list;

        protected Button package_btn;
        protected Button alert_btn;
        protected Button launch_btn;
        protected Button stand_btn;
        protected Button recall_btn;
        protected Button close_btn;

        protected int mission_type;
        protected Button[] mission_btn = new Button[6];

        protected ListBox objective_list;
        protected ListBox loadout_list;

        protected Ship ship;
        protected FlightPlanner flight_planner;

        protected int patrol_pattern;
        protected static ShipDesign design = null;

    }
}