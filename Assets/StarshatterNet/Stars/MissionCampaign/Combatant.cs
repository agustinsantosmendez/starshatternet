﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Combatant.h/Combatant.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    One side in a military conflict
*/
using System.Collections.Generic;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning Combatant class is still in development and is not recommended for production.
    public class Combatant
    {
        public Combatant(string com_name, string fname, int team)
        {
            name = com_name; iff = team; score = 0; force = null;
            for (int i = 0; i < 6; i++)
                target_factor[i] = 1;

            target_factor[2] = 1000;

            if (!string.IsNullOrEmpty(fname))
                force = CombatGroup.LoadOrderOfBattle(fname, iff, this);
        }
        public Combatant(string com_name, CombatGroup f)
        {
            name = com_name; iff = 0; score = 0; force = f;
            for (int i = 0; i < 6; i++)
                target_factor[i] = 1;

            target_factor[2] = 1000;

            if (force != null)
            {
                SetCombatant(force, this);
                iff = force.GetIFF();
            }
        }
        // ~Combatant();

        // operations:

        // accessors:
        public string Name() { return name; }
        public int GetIFF() { return iff; }
        public int Score() { return score; }
        public string GetDescription() { return name; }
        public CombatGroup GetForce() { return force; }
        public CombatGroup FindGroup(CombatGroup.GROUP_TYPE type, int id = -1)
        {
            if (force != null)
                return force.FindGroup(type, id);

            return null;
        }
        public List<CombatGroup> GetTargetList() { return target_list; }
        public List<CombatGroup> GetDefendList() { return defend_list; }
        public List<Mission> GetMissionList() { return mission_list; }

        public void AddMission(Mission m)
        {
            mission_list.Add(m);
        }

        public void SetScore(int points) { score = points; }
        public void AddScore(int points) { score += points; }

        public double GetTargetStratFactor(CombatGroup.GROUP_TYPE type)
        {
            switch (type)
            {
                case CombatGroup.GROUP_TYPE.FLEET:
                case CombatGroup.GROUP_TYPE.CARRIER_GROUP:
                case CombatGroup.GROUP_TYPE.BATTLE_GROUP:
                case CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON: return target_factor[0];

                case CombatGroup.GROUP_TYPE.WING:
                case CombatGroup.GROUP_TYPE.ATTACK_SQUADRON:
                case CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON:
                case CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON: return target_factor[1];

                case CombatGroup.GROUP_TYPE.BATTERY:
                case CombatGroup.GROUP_TYPE.MISSILE: return target_factor[2];

                case CombatGroup.GROUP_TYPE.BATTALION:
                case CombatGroup.GROUP_TYPE.STARBASE:
                case CombatGroup.GROUP_TYPE.C3I:
                case CombatGroup.GROUP_TYPE.COMM_RELAY:
                case CombatGroup.GROUP_TYPE.EARLY_WARNING:
                case CombatGroup.GROUP_TYPE.FWD_CONTROL_CTR:
                case CombatGroup.GROUP_TYPE.ECM: return target_factor[3];

                case CombatGroup.GROUP_TYPE.SUPPORT:
                case CombatGroup.GROUP_TYPE.COURIER:
                case CombatGroup.GROUP_TYPE.MEDICAL:
                case CombatGroup.GROUP_TYPE.SUPPLY:
                case CombatGroup.GROUP_TYPE.REPAIR: return target_factor[4];
            }

            return target_factor[5];
        }
        public void SetTargetStratFactor(CombatGroup.GROUP_TYPE type, double factor)
        {
            switch (type)
            {
                case CombatGroup.GROUP_TYPE.FLEET:
                case CombatGroup.GROUP_TYPE.CARRIER_GROUP:
                case CombatGroup.GROUP_TYPE.BATTLE_GROUP:
                case CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON:
                    target_factor[0] = factor;
                    break;

                case CombatGroup.GROUP_TYPE.WING:
                case CombatGroup.GROUP_TYPE.ATTACK_SQUADRON:
                case CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON:
                case CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON:
                    target_factor[1] = factor;
                    break;

                case CombatGroup.GROUP_TYPE.BATTALION:
                case CombatGroup.GROUP_TYPE.STARBASE:
                case CombatGroup.GROUP_TYPE.BATTERY:
                case CombatGroup.GROUP_TYPE.MISSILE:
                    target_factor[2] = factor;
                    break;

                case CombatGroup.GROUP_TYPE.C3I:
                case CombatGroup.GROUP_TYPE.COMM_RELAY:
                case CombatGroup.GROUP_TYPE.EARLY_WARNING:
                case CombatGroup.GROUP_TYPE.FWD_CONTROL_CTR:
                case CombatGroup.GROUP_TYPE.ECM:
                    target_factor[3] = factor;
                    break;

                case CombatGroup.GROUP_TYPE.SUPPORT:
                case CombatGroup.GROUP_TYPE.COURIER:
                case CombatGroup.GROUP_TYPE.MEDICAL:
                case CombatGroup.GROUP_TYPE.SUPPLY:
                case CombatGroup.GROUP_TYPE.REPAIR:
                    target_factor[4] = factor;
                    break;

                default:
                    target_factor[5] = factor;
                    break;
            }
        }

        private static void SetCombatant(CombatGroup g, Combatant c)
        {
            if (g == null) return;

            g.SetCombatant(c);

            foreach (var iter in g.GetComponents())
                SetCombatant(iter, c);
        }


        private string name;
        private int iff;
        private int score;

        private CombatGroup force;
        private List<CombatGroup> target_list = new List<CombatGroup>();
        private List<CombatGroup> defend_list = new List<CombatGroup>();
        private List<Mission> mission_list = new List<Mission>();

        private double[] target_factor = new double[8];

    }
}