﻿
using StarshatterNet.Gen.Misc;
/// Credit Adam Kapos (Nezz) - http://www.songarc.net
/// Sourced from - https://github.com/YousicianGit/UnityMenuSystem
/// Updated by SimonDarksideJ - Refactored to be a more generic component
namespace UnityEngine.UI.Extensions
{
    /// <summary>
    /// A base menu class that implements parameterless Show and Hide methods
    /// </summary>
    public abstract class SimpleMenu<T> : Menu<T> where T : SimpleMenu<T>
    {
        public abstract void RegisterControls();

        public static void Show()
        {
            Open();
        }

        public static void Hide()
        {
            Close();
        }

        public GameObject SetTextLabel(string buttonName, string labelName, bool toUpper = false)
        { 
            string label = ContentBundle.Instance.GetText(labelName);
            if (toUpper) label = label.ToUpperInvariant();
            var button = GameObject.Find(buttonName);
            Text txt = button.GetComponentInChildren<Text>();
            txt.text = label;
            return button;
        }
    }
}