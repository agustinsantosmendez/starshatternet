﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Weapon.h/Weapon.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Weapon (gun or missile launcher) class
*/
using System;
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.Simulator;
using DWORD = System.UInt32;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.SimElements
{
    public class Weapon : ShipSystem, ISimObserver
    {
        public const int MAX_BARRELS = 8;
        public enum Orders { MANUAL, AUTO, POINT_DEFENSE };
        public enum Control { SINGLE_FIRE, RIPPLE_FIRE, SALVO_FIRE };
        public enum Sweep { SWEEP_NONE, SWEEP_TIGHT, SWEEP_WIDE };

        public Weapon(WeaponDesign d, int nmuz, Point[] muzzles, double az = 0, double el = 0)
            : base(CATEGORY.WEAPON, (int)d.type, d.name, d.value, d.capacity, d.capacity, d.recharge_rate)
        {
            weapondesign = d; group = d.group; ammo = -1; ripple_count = 0;
            aim_azimuth = (float)az; aim_elevation = (float)el;
            old_azimuth = 0.0f; old_elevation = 0.0f; aim_time = 0;
            enabled = true; refire = 0.0f;
            mass = d.carry_mass; resist = d.carry_resist;
            guided = d.guided; shot_speed = d.speed;
            active_barrel = 0; locked = false; centered = false; firing = false; blocked = false;
            index = 0; target = null; subtarget = null; beams = null; orders = Orders.MANUAL;
            control = Control.SINGLE_FIRE; sweep = Sweep.SWEEP_TIGHT; turret = null; turret_base = null;


            //ZeroMemory(visible_stores, sizeof(visible_stores));

            if (weapondesign.primary)
                abrv = Game.GetText("sys.weapon.primary.abrv");
            else
                abrv = Game.GetText("sys.weapon.secondary.abrv");

            nbarrels = nmuz;

            if (nbarrels > MAX_BARRELS)
                nbarrels = MAX_BARRELS;

            if (nbarrels == 0 && weapondesign.nbarrels > 0)
            {
                nbarrels = weapondesign.nbarrels;

                for (int i = 0; i < nbarrels; i++)
                    muzzle_pts[i] = rel_pts[i] = weapondesign.muzzle_pts[i] * weapondesign.scale;

                ammo = weapondesign.ammo * nbarrels;
            }
            else if (nbarrels == 1 && weapondesign.nstores > 0)
            {
                nbarrels = weapondesign.nstores;

                for (int i = 0; i < nbarrels; i++)
                    muzzle_pts[i] = rel_pts[i] = (muzzles[0] + weapondesign.attachments[i]);

                ammo = nbarrels;
            }
            else
            {
                for (int i = 0; i < nbarrels; i++)
                    muzzle_pts[i] = rel_pts[i] = muzzles[i];

                ammo = weapondesign.ammo * nbarrels;
            }

            if (weapondesign.syncro)
                active_barrel = -1;

            emcon_power[0] = 0;
            emcon_power[1] = 0;
            emcon_power[2] = 100;

            aim_az_max = weapondesign.aim_az_max;
            aim_az_min = weapondesign.aim_az_min;
            aim_az_rest = weapondesign.aim_az_rest;

            aim_el_max = weapondesign.aim_el_max;
            aim_el_min = weapondesign.aim_el_min;
            aim_el_rest = weapondesign.aim_el_rest;
        }
        public Weapon(Weapon w) : base(w)
        {
            weapondesign = w.weapondesign; ammo = -1; ripple_count = 0;
            enabled = true; refire = 0.0f;
            mass = w.mass; resist = w.resist;
            aim_azimuth = w.aim_azimuth; aim_elevation = w.aim_elevation;
            old_azimuth = 0.0f; old_elevation = 0.0f; aim_time = 0;
            guided = w.guided; shot_speed = w.shot_speed;
            active_barrel = 0; locked = false; centered = false; firing = false; blocked = false;
            target = null; subtarget = null; beams = null; orders = Orders.MANUAL;
            control = Control.SINGLE_FIRE; sweep = Sweep.SWEEP_TIGHT; group = w.group;
            aim_az_max = w.aim_az_max; aim_az_min = w.aim_az_min; aim_az_rest = w.aim_az_rest;
            aim_el_max = w.aim_el_max; aim_el_min = w.aim_el_min; aim_el_rest = w.aim_el_rest;
            turret = null; turret_base = null;

            Mount(w);
            //ZeroMemory(visible_stores, sizeof(visible_stores));

            nbarrels = w.nbarrels;

            for (int i = 0; i < nbarrels; i++)
            {
                muzzle_pts[i] = rel_pts[i] = w.muzzle_pts[i];
            }

            ammo = weapondesign.ammo * nbarrels;

            if (weapondesign.syncro)
                active_barrel = -1;

            if (weapondesign.beam)
            {
                beams = new Shot[nbarrels];
                //ZeroMemory(beams, sizeof(Shot) * nbarrels);
            }

            if (aim_az_rest >= 2 * Math.PI)
                aim_az_rest = weapondesign.aim_az_rest;

            if (aim_el_rest >= 2 * Math.PI)
                aim_el_rest = weapondesign.aim_el_rest;
        }
        //  ~Weapon();

        //int operator ==(const Weapon& w) const { return this == &w; }

        public bool Track(SimObject targ, ShipSystem sub)
        {
            if (ammo != 0 && enabled && (availability > crit_level))
            {
                firing = false;
                Aim();
            }
            else if (turret != null)
            {
                ZeroAim();
            }

            return locked;
        }

        public Shot Fire()
        {
            if (SimulationTime.IsPaused)
                return null;

            if (ship != null && ship.CheckFire())
                return null;

            if (ship.IsStarship() && target != null && !centered)
                return null;

            if (beams != null && active_barrel >= 0 && active_barrel < nbarrels && beams[active_barrel] != null)
                return null;

            Shot shot = null;

            if (ammo != 0 && enabled &&
                    (refire <= 0) && (energy > weapondesign.min_charge) &&
                    (availability > crit_level))
            {

                refire = weapondesign.refire_delay;

                NetGame net_game = NetGame.GetInstance();
                bool net_client = net_game != null ? net_game.IsClient() : false;

                // all barrels
                if (active_barrel < 0)
                {
                    if (net_client || IsPrimary())
                        NetUtil.SendWepTrigger(this, nbarrels);

                    if (net_game == null || IsPrimary())
                    {
                        for (int i = 0; i < nbarrels; i++)
                            shot = FireBarrel(i);
                    }

                    else if (net_game != null && net_game.IsServer() && IsMissile())
                    {
                        for (int i = 0; i < nbarrels; i++)
                        {
                            shot = FireBarrel(i);
                            NetUtil.SendWepRelease(this, shot);
                        }
                    }
                }

                // single barrel
                else
                {
                    if (net_client || IsPrimary())
                        NetUtil.SendWepTrigger(this, nbarrels);

                    if (net_game == null || IsPrimary())
                    {
                        shot = FireBarrel(active_barrel++);
                    }

                    else if (net_game != null && net_game.IsServer() && IsMissile())
                    {
                        shot = FireBarrel(active_barrel++);
                        NetUtil.SendWepRelease(this, shot);
                    }

                    if (active_barrel >= nbarrels)
                    {
                        active_barrel = 0;
                        refire += weapondesign.salvo_delay;
                    }
                }

                if (weapondesign.ripple_count > 0 && ripple_count <= 0)
                    ripple_count = weapondesign.ripple_count - 1;

                if (status != STATUS.NOMINAL)
                    refire *= 2;
            }

            return shot;
        }

        public Shot NetFirePrimary(SimObject tgt, ShipSystem sub, int count)
        {
            Shot shot = null;

            if (!IsPrimary() || SimulationTime.IsPaused)
                return shot;

            if (active_barrel < 0 || active_barrel >= nbarrels)
                active_barrel = 0;

            target = tgt;
            subtarget = sub;
            aim_time = 0;

            for (int i = 0; i < count; i++)
            {
                shot = FireBarrel(active_barrel++);

                if (active_barrel >= nbarrels)
                {
                    active_barrel = 0;
                    refire += weapondesign.salvo_delay;
                }
            }

            if (target != null)
                Observe(target);

            return shot;
        }

        public Shot NetFireSecondary(SimObject tgt, ShipSystem sub, DWORD objid)
        {
            Shot shot = null;

            if (IsPrimary() || SimulationTime.IsPaused)
                return shot;

            if (active_barrel < 0 || active_barrel >= nbarrels)
                active_barrel = 0;

            target = tgt;
            subtarget = sub;
            aim_time = 0;

            shot = FireBarrel(active_barrel++);

            if (active_barrel >= nbarrels)
            {
                active_barrel = 0;
                refire += weapondesign.salvo_delay;
            }

            if (shot != null)
                shot.SetObjID(objid);

            if (target != null)
                Observe(target);

            return shot;
        }

        public void SetTarget(SimObject targ, ShipSystem sub)
        {
            // check self targeting:
            if (targ == (SimObject)ship)
                return;

            // check target class filter:
            if (targ != null)
            {
                switch ((SimObject.TYPES)targ.Type())
                {
                    case SimObject.TYPES.SIM_SHIP:
                        {
                            Ship tgt_ship = (Ship)targ;

                            if ((tgt_ship.Class() & weapondesign.target_type) == 0)
                                return;
                        }
                        break;

                    case SimObject.TYPES.SIM_SHOT:
                        return;

                    case SimObject.TYPES.SIM_DRONE:
                        {
                            if ((weapondesign.target_type & CLASSIFICATION.DRONE) == 0)
                                return;
                        }
                        break;

                    default:
                        return;
                }
            }

            // if ok, target this object:
            if (target != targ)
            {
                target = targ;

                if (target != null)
                    Observe(target);
            }

            subtarget = sub;
        }

        public void SelectTarget()
        {
            bool select_locked = false;
            SimObject targ = null;
            double dist = 1e9;
            double az = 0;
            double el = 0;

            if (ammo != 0 && enabled && (availability > crit_level))
            {
                ZeroAim();


                // lock onto any threatening shots first (if we can):
                if ((weapondesign.target_type & CLASSIFICATION.DRONE) != 0)
                {
                    foreach (Contact contact in ship.ContactList())
                    {
                        Shot c_shot = contact.GetShot();

                        if (c_shot != null && contact.Threat(ship))
                        {

                            // distance from self to target:
                            double distance = (c_shot.Location() - muzzle_pts[0]).Length;

                            if (distance > weapondesign.min_range &&
                                    distance < weapondesign.max_range &&
                                    distance < dist)
                            {
                                Point dummy = new Point();
                                // check aim basket:
                                select_locked = CanLockPoint(c_shot.Location(), out az, out el, ref dummy);

                                if (select_locked)
                                {
                                    targ = c_shot;
                                    dist = distance;
                                }
                            }
                        }
                    }
                }

                // lock onto a threatening ship only if it is (much) closer:
                dist *= 0.2;
                foreach (Contact contact in ship.ContactList())
                {
                    Ship c_ship = contact.GetShip();

                    if (c_ship == null) continue;

                    // can we lock onto this target?
                    if ((c_ship.IsRogue() || c_ship.GetIFF() > 0 && c_ship.GetIFF() != ship.GetIFF()) &&
                            (c_ship.Class() & weapondesign.target_type) != 0 &&
                            c_ship.Weapons().Count > 0)
                    {
                        // distance from self to target:
                        double distance = (c_ship.Location() - muzzle_pts[0]).Length;

                        if (distance < weapondesign.max_range && distance < dist)
                        {
                            // check aim basket:
                            Point dummy = new Point();
                            select_locked = CanLockPoint(c_ship.Location(), out az, out el, ref dummy);

                            if (select_locked)
                            {
                                targ = c_ship;
                                dist = distance;
                            }
                        }
                    }
                }
            }

            if (ammo == 0 || !enabled)
            {
                SetTarget(null, null);
                locked = false;
            }

            else
            {
                SetTarget(targ, null);
            }
        }

        public bool CanTarget(CLASSIFICATION classification)
        {
            return (weapondesign.target_type & classification) != 0 ? true : false;
        }

        public SimObject GetTarget() { return target; }
        public ShipSystem GetSubTarget() { return subtarget; }
        public void SetFiringOrders(Orders o)
        {
            if (o >= Orders.MANUAL && o <= Orders.POINT_DEFENSE)
                orders = o;
        }

        public Orders GetFiringOrders() { return orders; }
        public void SetControlMode(Control m)
        {
            if (m >= Control.SINGLE_FIRE && m <= Control.SALVO_FIRE)
                control = m;
        }

        public Control GetControlMode() { return control; }
        public void SetSweep(Sweep s)
        {
            if (s >= Sweep.SWEEP_NONE && s <= Sweep.SWEEP_WIDE)
                sweep = s;
        }


        public Sweep GetSweep() { return sweep; }

        public void Enable() { enabled = true; }
        public void Disable() { enabled = false; }

        public WeaponDesign Design() { return weapondesign; }
        public string Group() { return group; }
        public bool Enabled() { return enabled; }
        public int Ammo() { return ammo; }
        public int Guided() { return guided; }
        public bool Locked() { return locked; }
        public float Velocity() { return shot_speed; }
        public float Mass() { return mass * ammo; }
        public float Resistance() { return resist * ammo; }
        public Shot GetBeam(int i)
        {
            if (beams != null && i >= 0 && i < nbarrels)
                return beams[i];

            return null;
        }

        // needed to set proper ammo level when joining multiplayer in progress:
        public void SetAmmo(int a)
        {
            if (a >= 0)
            {
                if (active_barrel >= 0 && weapondesign.visible_stores)
                {
                    while (a < ammo)
                    {
                        if (active_barrel >= nbarrels)
                            active_barrel = 0;

                        if (visible_stores[active_barrel] != null)
                        {
                            // GRAPHIC_DESTROY(visible_stores[active_barrel]);
                            if (visible_stores[active_barrel] != null) { visible_stores[active_barrel].Dispose(); visible_stores[active_barrel] = null; }
                            active_barrel++;
                            ammo--;
                        }
                    }
                }

                ammo = a;
            }
        }


        public bool IsPrimary()
        {
            return weapondesign.primary;
        }

        public bool IsDrone()
        {
            return weapondesign.drone;
        }


        public bool IsDecoy()
        {
            return weapondesign.decoy_type != 0;
        }

        public bool IsProbe()
        {
            return weapondesign.probe;
        }


        public bool IsMissile()
        {
            return !weapondesign.primary;
        }
        public bool IsBeam()
        {
            return weapondesign.beam;
        }

        public override void ExecFrame(double seconds)
        {
            base.ExecFrame(seconds);

            if (refire > 0)
                refire -= (float)seconds;

            locked = false;
            centered = false;

            if (ship == null)
                return;

            if (orders == Orders.POINT_DEFENSE && enabled)
                SelectTarget();

            if (beams != null && target == null)
            {
                for (int i = 0; i < nbarrels; i++)
                {
                    if (beams[i] != null)
                    {
                        // aim beam straight:
                        Aim();
                        SetBeamPoints(false);
                        return;
                    }
                }
            }

            if (weapondesign.self_aiming)
            {
                Track(target, subtarget);
            }
            else if (turret != null)
            {
                ZeroAim();
            }

            if (ship.CheckFire())
                return;

            // aim beam at target:
            bool aim_beams = false;

            if (beams != null)
            {
                for (int i = 0; i < nbarrels; i++)
                {
                    if (beams[i] != null)
                    {
                        aim_beams = true;
                        SetBeamPoints(true);
                        break;
                    }
                }
            }

            if (!aim_beams)
            {
                if (ripple_count > 0)
                {
                    if (Fire() != null)
                        ripple_count--;
                }

                else if (locked && !blocked)
                {
                    if (!ship.IsHostileTo(target))
                        return;

                    if (orders == Orders.AUTO && centered)
                    {
                        if (energy >= weapondesign.charge &&
                                (ammo < 0 || target != null && target.Integrity() >= 1) &&
                                objective.Length < weapondesign.max_range)
                            Fire();
                    }

                    else if (orders == Orders.POINT_DEFENSE)
                    {
                        if (energy >= weapondesign.min_charge &&
                                (ammo < 0 || target != null && target.Integrity() >= 1) &&
                                objective.Length < weapondesign.max_range)
                            Fire();
                    }
                }
            }
        }

        public override void Orient(Physical rep)
        {
            base.Orient(rep);

            Matrix33D orientation = rep.Cam().Orientation();
            Point loc = rep.Location();

            // align graphics with camera:
            if (turret != null)
            {
                if (!weapondesign.self_aiming)
                    ZeroAim();

                Matrix33D aim = aim_cam.Orientation();

                turret.MoveTo(mount_loc);
                turret.SetOrientation(aim);

                if (turret_base != null)
                {
                    Matrix33D base_ = orientation;

                    if (weapondesign.turret_axis == 1)
                    {
                        base_.Pitch(aim_elevation);
                        base_.Pitch(old_elevation);
                    }
                    else
                    {
                        base_.Yaw(aim_azimuth);
                        base_.Yaw(old_azimuth);
                    }

                    turret_base.MoveTo(mount_loc);
                    turret_base.SetOrientation(base_);
                }

                for (int i = 0; i < nbarrels; i++)
                {
                    muzzle_pts[i] = (aim * rel_pts[i]) + mount_loc; // TODO matrix * vector multiplication order

                    if (visible_stores[i] != null)
                    {
                        visible_stores[i].SetOrientation(aim);
                        visible_stores[i].MoveTo(muzzle_pts[i]);
                    }
                }
            }
            else
            {
                for (int i = 0; i < nbarrels; i++)
                {
                    muzzle_pts[i] = (orientation * rel_pts[i]) + loc;// TODO matrix * vector multiplication order

                    if (visible_stores[i] != null)
                    {
                        visible_stores[i].SetOrientation(orientation);
                        visible_stores[i].MoveTo(muzzle_pts[i]);
                    }
                }
            }
        }

        public override void Distribute(double delivered_energy, double seconds)
        {
            if (UsesWatts())
            {
                if (seconds < 0.01)
                    seconds = 0.01;

                // convert Joules to Watts:
                energy = (float)(delivered_energy / seconds);
            }

            else if (!SimulationTime.IsPaused)
            {
                energy += (float)(delivered_energy * 1.25);

                if (energy > capacity)
                    energy = capacity;

                else if (energy < 0)
                    energy = 0.0f;
            }
        }


        public Ship Owner() { return ship; }
        public void SetOwner(Ship s)
        {
            ship = s;

            if (weapondesign.turret_model != null)
            {
                Solid t = new Solid();
                t.UseModel(weapondesign.turret_model);
                turret = t;
            }

            if (weapondesign.turret_base_model != null)
            {
                Solid t = new Solid();
                t.UseModel(weapondesign.turret_base_model);
                turret_base = t;
            }

            if (!weapondesign.primary &&
                    weapondesign.visible_stores &&
                    ammo == nbarrels &&
                    weapondesign.shot_model != null)
            {
                for (int i = 0; i < nbarrels; i++)
                {
                    Solid sld = new Solid();
                    sld.UseModel(weapondesign.shot_model);

                    visible_stores[i] = sld;
                }
            }
        }

        public int GetIndex() { return index; }
        public void SetIndex(int n) { index = n; }

        public Point GetAimVector() { return aim_cam.vpn(); }
        public void SetAzimuth(double a) { aim_azimuth = (float)a; }
        public double GetAzimuth() { return aim_azimuth; }
        public void SetElevation(double e) { aim_elevation = (float)e; }
        public double GetElevation() { return aim_elevation; }

        public void SetRestAzimuth(double a) { aim_az_rest = (float)a; }
        public double GetRestAzimuth() { return aim_az_rest; }
        public void SetRestElevation(double e) { aim_el_rest = (float)e; }
        public double GetRestElevation() { return aim_el_rest; }

        public void SetAzimuthMax(double a) { aim_az_max = (float)a; }
        public double GetAzimuthMax() { return aim_az_max; }
        public void SetAzimuthMin(double a) { aim_az_min = (float)a; }
        public double GetAzimuthMin() { return aim_az_min; }

        public void SetElevationMax(double e) { aim_el_max = (float)e; }
        public double GetElevationMax() { return aim_el_max; }
        public void SetElevationMin(double e) { aim_el_min = (float)e; }
        public double GetElevationMin() { return aim_el_min; }

        public void SetGroup(string n) { group = n; }

        public bool IsBlockedFriendly() { return blocked; }
        public void SetBlockedFriendly(bool b) { blocked = b; }

        public Solid GetTurret()
        {
            return turret;
        }

        public Solid GetTurretBase()
        {
            return turret_base;
        }


        public Solid GetVisibleStore(int i)
        {
            if (i >= 0 && i < MAX_BARRELS)
                return visible_stores[i];

            return null;
        }


        public virtual bool Update(SimObject obj)
        {
            if (obj == target)
            {
                target = null;
            }

            else if (beams != null)
            {
                for (int i = 0; i < nbarrels; i++)
                    if (obj == beams[i])
                        beams[i] = null;
            }

            return simObserver.Update(obj);
        }

        public virtual string GetObserverName()
        {
            string name = string.Format("Weapon {0}", weapondesign.name);
            return name;
        }



        protected Shot FireBarrel(int n)
        {
            Point base_vel = ship.Velocity();
            Shot shot = null;
            SimRegion region = ship.GetRegion();

            if (region == null || n < 0 || n >= nbarrels || SimulationTime.IsPaused)
                return null;

            firing = true;
            Aim();

            CameraNode rail_cam = aim_cam.Clone();
            //rail_cam.Clone(aim_cam);

            Point shotpos = muzzle_pts[n];
            if (weapondesign.length > 0)
                shotpos = shotpos + aim_cam.vpn() * weapondesign.length;

            // guns may be slewed towards target:
            if (weapondesign.primary)
            {
                shot = CreateShot(shotpos, aim_cam, weapondesign, ship);

                if (shot != null)
                {
                    shot.SetVelocity(shot.Velocity() + base_vel);
                }
            }

            // missiles always launch in rail direction:
            else
            {
                // unless they are on a mobile launcher
                if (turret != null && weapondesign.self_aiming)
                {
                    shot = CreateShot(shotpos, aim_cam, weapondesign, ship);
                    shot.SetVelocity(base_vel);
                }
                else
                {
                    shot = CreateShot(shotpos, rail_cam, weapondesign, ship);
                    if (shot != null/* && !turret */)
                    {
                        Matrix33D orient = ship.Cam().Orientation();
                        if (aim_azimuth != 0) orient.Yaw(aim_azimuth);
                        if (aim_elevation != 0) orient.Pitch(aim_elevation);

                        Point eject = orient * weapondesign.eject; // TODO matrix * vector multiplication order
                        shot.SetVelocity(base_vel + eject);
                    }
                }

                if (shot != null && visible_stores[n] != null)
                {
                    //GRAPHIC_DESTROY(visible_stores[n]);
                    if (visible_stores[n] != null) { visible_stores[n].Dispose(); visible_stores[n] = null; }
                }
            }

            if (shot != null)
            {
                if (ammo > 0)
                    ammo--;

                if (guided != 0 && target != null)
                    shot.SeekTarget(target, subtarget);

                float shot_load;
                if (energy > weapondesign.charge)
                    shot_load = weapondesign.charge;
                else
                    shot_load = energy;

                energy -= shot_load;
                shot.SetCharge(shot_load * availability);

                if (target != null && weapondesign.flak && weapondesign.guided == 0)
                {
                    double speed = shot.Velocity().Length;
                    double range = (target.Location() - shot.Location()).Length;

                    if (range > weapondesign.min_range && range < weapondesign.max_range)
                    {
                        shot.SetFuse(range / speed);
                    }
                }

                region.InsertObject(shot);

                if (beams != null)
                {
                    beams[n] = shot;
                    Observe(beams[n]);

                    // aim beam at target:
                    SetBeamPoints(true);
                }

                if (ship != null)
                {
                    ShipStats stats = ShipStats.Find(ship.Name());

                    if (weapondesign.primary)
                        stats.AddGunShot();
                    else if (weapondesign.decoy_type == 0 && weapondesign.damage > 0)
                        stats.AddMissileShot();
                }
            }

            return shot;
        }

        protected Shot CreateShot(Point loc, CameraNode cam, WeaponDesign dsn, Ship own)
        {
            if (dsn.drone)
                return new Drone(loc, cam, dsn, own);

            else
                return new Shot(loc, cam, dsn, own);
        }


        protected void SetBeamPoints(bool aim = false)
        {
            for (int i = 0; i < nbarrels; i++)
            {
                if (beams != null && beams[i] != null)
                {
                    Point from = muzzle_pts[i];
                    Point to;
                    double len = weapondesign.length;

                    if (len < 1 || len > 1e7)
                        len = weapondesign.max_range;

                    if (len < 1)
                        len = 3e5;

                    else if (len > 1e7)
                        len = 1e7;

                    // always use the aim cam, to enforce slew_rate
                    to = from + aim_cam.vpn() * (float)len;

                    beams[i].SetBeamPoints(from, to);
                }
            }
        }

        protected void Aim()
        {
            locked = false;
            centered = false;

            FindObjective();

            if (target != null)
            {
                double az = 0;
                double el = 0;

                locked = CanLockPoint(obj_w, out az, out el, ref objective);

                double spread_az = weapondesign.spread_az;
                double spread_el = weapondesign.spread_el;

                // beam sweep target:
                if (weapondesign.beam)
                {
                    double factor = 0;
                    double az_phase = 0;
                    double el_phase = 0;

                    if ((SimObject.TYPES)target.Type() == SimObject.TYPES.SIM_SHIP)
                    {
                        Ship s = (Ship)target;

                        if (s.IsStarship())
                        {
                            switch (sweep)
                            {
                                default:
                                case Sweep.SWEEP_NONE: factor = 0; break;
                                case Sweep.SWEEP_TIGHT: factor = 1; break;
                                case Sweep.SWEEP_WIDE: factor = 2; break;
                            }
                        }
                    }

                    if (factor > 0)
                    {
                        factor *= Math.Atan2(target.Radius(), (double)objective.Z);

                        for (int i = 0; i < nbarrels; i++)
                        {
                            if (beams != null && beams[i] != null)
                            {
                                az_phase = Math.Sin(beams[i].Life() * 0.4 * Math.PI);
                                el_phase = Math.Sin(beams[i].Life() * 1.0 * Math.PI);
                                break;
                            }
                        }

                        az += factor * spread_az * az_phase;
                        el += factor * spread_el * el_phase * 0.25;
                    }
                }

                else if (!weapondesign.beam)
                {
                    if (spread_az > 0)
                        az += RandomHelper.Random(-spread_az, spread_az);

                    if (spread_el > 0)
                        el += RandomHelper.Random(-spread_el, spread_el);
                }

                AimTurret(az, -el);

                // check range for guided weapons:
                if (locked && guided != 0)
                {
                    double range = objective.Length;

                    if (range > weapondesign.max_track)
                        locked = false;

                    else if (range > weapondesign.max_range)
                    {
                        if (firing)
                        {
                            if (RandomHelper.RandomChance(1, 4))   // 1 in 4 chance of locking anyway
                                locked = false;
                        }
                        else
                        {
                            locked = false;
                        }
                    }
                }

                if (locked)
                {
                    Point tloc = target.Location();
                    tloc = Transform(tloc);

                    if (tloc.Z > 1)
                    {
                        az = Math.Atan2(Math.Abs(tloc.X), tloc.Z);
                        el = Math.Atan2(Math.Abs(tloc.Y), tloc.Z);

                        double firing_cone = MathHelper.ToRadians(10);

                        if (orders == Orders.MANUAL)
                            firing_cone = MathHelper.ToRadians(30);

                        if (az < firing_cone && el < firing_cone)
                            centered = true;
                    }
                }
            }
            else
            {
                AimTurret(aim_az_rest, -aim_el_rest);
            }
        }

        protected void AimTurret(double az, double el)
        {
            double seconds = (Game.GameTime() - aim_time) / 1000.0;

            // don't let the weapon turn faster than turret slew rate:
            double max_turn = weapondesign.slew_rate * seconds;

            if (Math.Abs(az - old_azimuth) > max_turn)
            {
                if (az > old_azimuth)
                    az = old_azimuth + max_turn;
                else
                    az = old_azimuth - max_turn;
            }

            if (Math.Abs(el - old_elevation) > Math.PI / 2 * seconds)
            {
                if (el > old_elevation)
                    el = old_elevation + max_turn;
                else
                    el = old_elevation - max_turn;
            }

            aim_cam.Yaw(az);
            aim_cam.Pitch(el);

            old_azimuth = (float)az;
            old_elevation = (float)el;

            aim_time = (uint)Game.GameTime();
        }

        protected void ZeroAim()
        {
            aim_cam = ship.Cam().Clone();
            if (aim_azimuth != 0) aim_cam.Yaw(aim_azimuth);
            if (aim_elevation != 0) aim_cam.Pitch(aim_elevation);

            aim_cam.MoveTo(muzzle_pts[0]);
        }

        protected void FindObjective()
        {
            ZeroAim();

            if (target == null || !weapondesign.self_aiming)
            {
                objective = new Point();
                return;
            }

            obj_w = target.Location();

            if (subtarget != null)
            {
                obj_w = subtarget.MountLocation();
            }
            else if ((SimObject.TYPES)target.Type() == SimObject.TYPES.SIM_SHIP)
            {
                Ship tgt_ship = (Ship)target;

                if (tgt_ship.IsGroundUnit())
                    obj_w += new Point(0, 150, 0);
            }

            if (!weapondesign.beam && shot_speed > 0)
            {
                // distance from self to target:
                double distance = (obj_w - muzzle_pts[0]).Length;

                // TRUE shot speed is relative to ship speed:
                Point eff_shot_vel = ship.Velocity() + aim_cam.vpn() * shot_speed - target.Velocity();
                double eff_shot_speed = eff_shot_vel.Length;

                // time to reach target:
                double time = distance / eff_shot_speed;

                // where the target will be when the shot reaches it:
                obj_w += (target.Velocity() - ship.Velocity()) * (float)time +
                target.Acceleration() * (float)(0.25 * time * time);
            }

            // transform into camera coords:
            objective = Transform(obj_w);
        }

        protected Point Transform(Point pt)
        {
            Point result;

            Point obj_t = pt - aim_cam.Pos();
            result.X = Point.Dot(obj_t, aim_cam.vrt());
            result.Y = Point.Dot(obj_t, aim_cam.vup());
            result.Z = Point.Dot(obj_t, aim_cam.vpn());

            return result;
        }

        protected bool CanLockPoint(Point test, out double az, out double el, ref Point obj)
        {
            Point pt = Transform(test);
            bool locked = true;

            // first compute az:
            if (Math.Abs(pt.Z) < 0.1) pt.Z = 0.1f;
            az = Math.Atan(pt.X / pt.Z);
            if (pt.Z < 0) az -= Math.PI;
            if (az < -Math.PI) az += 2 * Math.PI;

            // then, rotate target into az-coords to compute el:
            CameraNode tmp = aim_cam.Clone();
            //tmp.Clone(aim_cam);
            aim_cam.Yaw(az);
            pt = Transform(test);
            aim_cam = tmp.Clone();

            if (Math.Abs(pt.Z) < 0.1) pt.Z = 0.1f;
            el = Math.Atan(pt.Y / pt.Z);

            if (obj != null) obj = pt;

            // is the target in the basket?
            // clamp if necessary:

            if (az > aim_az_max)
            {
                az = aim_az_max;
                locked = false;
            }
            else if (az < aim_az_min)
            {
                az = aim_az_min;
                locked = false;
            }

            if (el > aim_el_max)
            {
                el = aim_el_max;
                locked = false;
            }
            else if (el < aim_el_min)
            {
                el = aim_el_min;
                locked = false;
            }

            if (IsDrone() && guided != 0)
            {
                double firing_cone = MathHelper.ToRadians(10);

                if (orders == Orders.MANUAL)
                    firing_cone = MathHelper.ToRadians(20);

                if (az < firing_cone && el < firing_cone)
                    locked = true;
            }

            return locked;
        }

        public void Observe(SimObject obj)
        {
            simObserver.Observe(obj);
        }

        public void Ignore(SimObject obj)
        {
            simObserver.Ignore(obj);
        }

        // data members:
        protected WeaponDesign weapondesign;
        protected string group;
        protected Point[] muzzle_pts = new Point[MAX_BARRELS];
        protected Point[] rel_pts = new Point[MAX_BARRELS];
        protected Solid turret;
        protected Solid turret_base;
        protected Solid[] visible_stores = new Solid[MAX_BARRELS];
        protected int nbarrels;
        protected int active_barrel;

        protected float refire;
        protected int ammo;
        protected int ripple_count;

        // carrying costs per shot:
        protected float mass;
        protected float resist;

        // for targeting computer:
        protected int guided;
        protected bool enabled;
        protected bool locked;
        protected bool centered;
        protected bool firing;
        protected bool blocked;
        protected float shot_speed;

        protected int index;

        protected Orders orders;
        protected Control control;
        protected Sweep sweep;

        protected SimObject target;
        protected ShipSystem subtarget;

        protected Point objective;
        protected Point obj_w;
        protected CameraNode aim_cam;
        protected float aim_azimuth;
        protected float aim_elevation;
        protected float old_azimuth;
        protected float old_elevation;

        // auto-aiming arc
        protected float aim_az_max;       // maximum deflection in azimuth
        protected float aim_az_min;       // minimum deflection in azimuth
        protected float aim_az_rest;      // azimuth of turret at rest
        protected float aim_el_max;       // maximum deflection in elevation
        protected float aim_el_min;       // minimum deflection in elevation
        protected float aim_el_rest;      // elevation of turret at rest

        protected DWORD aim_time;

        protected Shot[] beams;
        protected ISimObserver simObserver = new  SimObserver();
    }

}