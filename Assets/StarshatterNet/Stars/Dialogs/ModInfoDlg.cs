﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      ModInfoDlg.h/ModInfoDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mod Info Splash Dialog Active Window class
*/
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class ModInfoDlg : FormWindow
    {
        public ModInfoDlg(Screen s, Camera c, FormDef def, BaseScreen mgr, GameObject p) :
             base(s, 0, 0, s.Width(), s.Height(), c, p, "ModInfoDlg")
        {
            manager = mgr;
            btn_accept = null; mod_info = null;

            Init(def);
        }
        //public virtual ~ModInfoDlg();
        public override void RegisterControls()
        {
            btn_accept = (Button)FindControl(1);

            if (btn_accept != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_accept, OnAccept);

            lbl_name = FindControl(101);
            lbl_desc = FindControl(102);
            lbl_copy = FindControl(103);

            img_logo = (ImageBox)FindControl(200);

#if TODO
            if (img_logo != null)
            {
                img_logo.GetPicture(  bmp_default.Texture);
                 img_logo.SetBlendMode(Video.BLEND_SOLID);
            }
#endif
        }

        public override void Show()
        {
            base.Show();
        }

        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                if (btn_accept != null && btn_accept.IsEnabled())
                    OnAccept(null, null);
            }
        }

        // Operations:
        public virtual void SetModInfo(ActiveWindow obj, ModInfo info)
        {
            mod_info = info;

            if (mod_info != null)
            {
                if (lbl_name != null) lbl_name.SetText(mod_info.Name());
                if (lbl_desc != null) lbl_desc.SetText(mod_info.Description());
                if (lbl_copy != null) lbl_copy.SetText(mod_info.Copyright());

                if (img_logo != null && mod_info.LogoImage() != null && mod_info.LogoImage().width > 32)
                    img_logo.SetPicture(mod_info.LogoImage());
                else if (img_logo != null)
                    img_logo.SetPicture(bmp_default.Texture);
            }
        }

        public virtual void OnAccept(ActiveWindow obj, AWEvent evnt)
        {
            manager.ShowModDlg();
        }


        protected BaseScreen manager;

        protected ActiveWindow lbl_name;
        protected ActiveWindow lbl_desc;
        protected ActiveWindow lbl_copy;
        protected ImageBox img_logo;

        protected Button btn_accept;

        protected ModInfo mod_info;

        protected Bitmap bmp_default;
    }
}