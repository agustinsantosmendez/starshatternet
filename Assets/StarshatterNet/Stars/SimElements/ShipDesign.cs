﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      ShipDesign.h/ShipDesign.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Starship Design parameters class
*/
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Audio;
using StarshatterNet.Stars.Graphics.General;
using System;
using System.Collections.Generic;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars.SimElements
{
    public class ShipLoad
    {
        public string name;
        public int[] load = new int[16];
        public double mass;
    }

    public class ShipSquadron
    {
        public string name;
        public ShipDesign design;
        public int count = 4;
        public int avail = 4;
    }

    public class ShipExplosion
    {
        public Explosion.ExplosionType type;
        public float time;
        public Vector3D loc;
        public bool final;
    }

    public class ShipDebris
    {
        public Model model;
        public int count;
        public int life;
        public Vector3F loc;
        public float mass;
        public float speed;
        public float drag;
        public int fire_type;
        public Vector3F[] fire_loc = new Vector3F[5];
    }

    // Used to share common information about ships of a single type.
    // ShipDesign objects are loaded from a text file and stored in a
    // static list (catalog) member for use by the Ship.
    public class ShipDesign : IComparable
    {
        public ShipDesign()
        {
            for (int i = 0; i < models.Length; i++)
            {
                models[i] = new List<Model>();
            }
            for (int i = 0; i < offsets.Length; i++)
            {
                offsets[i] = new List<Point>();
            }

        }
        public ShipDesign(string name, string path, string fname, bool secret = false) : this()
        {
            this.name = name;
            fname = fname.Replace(".def", ".json");
            if (fname.EndsWith(".json"))
                this.filename = fname;
            else
                this.filename = fname + ".json";

            scale = 1.0f;

            agility = 2e2f;
            air_factor = 0.1f;
            vlimit = 8e3f;
            drag = 2.5e-5f;
            arcade_drag = 1.0f;
            roll_drag = 5.0f;
            pitch_drag = 5.0f;
            yaw_drag = 5.0f;

            roll_rate = 0.0f;
            pitch_rate = 0.0f;
            yaw_rate = 0.0f;

            trans_x = 0.0f;
            trans_y = 0.0f;
            trans_z = 0.0f;

            turn_bank = (float)(Math.PI / 8);

            CL = 0.0f;
            CD = 0.0f;
            stall = 0.0f;

            prep_time = 30.0f;
            avoid_time = 0.0f;
            avoid_fighter = 0.0f;
            avoid_strike = 0.0f;
            avoid_target = 0.0f;
            commit_range = 0.0f;

            splash_radius = -1.0f;
            scuttle = 5e3f;
            repair_speed = 1.0f;
            repair_teams = 2;
            repair_auto = true;
            repair_screen = true;
            wep_screen = true;

            chase_vec = new Point(0, -100, 20);
            bridge_vec = new Point(0, 0, 0);
            beauty_cam = new Point(0, 0, 0);
            cockpit_scale = 1.0f;

            radius = 1.0f;
            integrity = 500.0f;

            primary = 0;
            secondary = 1;
            main_drive = -1;

            pcs = 3.0f;
            acs = 1.0f;
            detet = 250.0e3f;
            e_factor[0] = 0.1f;
            e_factor[1] = 0.3f;
            e_factor[2] = 1.0f;

            explosion_scale = 0.0f;
            death_spiral_time = 3.0f;

            if (!secret)
                ErrLogger.PrintLine("Loading ShipDesign '{0}'", name);
            path_name = path;
            if (!path_name.EndsWith("/"))
                path_name += "/";

            Serialization.ShipDesignSerializer.JSONReadShipDesignOverwrite(path_name, fname, this);

            valid = true;
            degrees = false;
#if TODO
            for (int i = 0; i < 4; i++)
            {
                int n = 0;
                foreach (string model_name in detail[i])
                {
                    string mname = path_name + model_name.Replace(".mag","");
                    Gen.Graphics.Model model = new Gen.Graphics.Model();
                    if (!string.IsNullOrEmpty(model_name) && !model.Load(mname, scale))
                    {
                        ErrLogger.PrintLine("ERROR: Could not load detail {0}, model '{1}'", i, model_name);
                        //delete model;
                        model = null;
                        valid = false;
                    }

                    else
                    {
                        lod_levels = i + 1;

                        if (model.Radius() > radius)
                            radius = (float)model.Radius();

                        //models[i].Add(model);
                        //PrepareModel(model);

                        if (offset[i].Count != 0)
                        {
                            offset[i][n] *= scale;
                            offsets[i].Add(offset[i][n]); // transfer ownership
                        }
                        else
                            offsets[i].Add(new Point());

                        n++;
                    }
                }

                detail[i].Destroy();
            }

            if (!secret)
                ErrLogger.PrintLine("   Ship Design Radius = {0}", radius);

            if (!string.IsNullOrEmpty(cockpit_name))
            {
                string model_name = cockpit_name;

                cockpit_model = new Model();
                if (!cockpit_model.Load(model_name, cockpit_scale))
                {
                    ErrLogger.PrintLine("ERROR: Could not load cockpit model '{0}'", model_name);
                    //delete cockpit_model;
                    cockpit_model = null;
                }
                else
                {
                    if (!secret)
                        ErrLogger.PrintLine("   Loaded cockpit model '{0}', preparing tangents", model_name);
                    PrepareModel(cockpit_model);
                }
            }
 
            if (beauty.Width() < 1 && loader.FindFile("beauty.pcx"))
            loader.LoadBitmap("beauty.pcx", beauty);

            if (hud_icon.Width() < 1 && loader.FindFile("hud_icon.pcx"))
            loader.LoadBitmap("hud_icon.pcx", hud_icon);
#endif

            if (string.IsNullOrEmpty(abrv))
            {
                switch (type)
                {
                    case CLASSIFICATION.DRONE: abrv = "DR"; break;
                    case CLASSIFICATION.FIGHTER: abrv = "F"; break;
                    case CLASSIFICATION.ATTACK: abrv = "F/A"; break;
                    case CLASSIFICATION.LCA: abrv = "LCA"; break;
                    case CLASSIFICATION.CORVETTE: abrv = "FC"; break;
                    case CLASSIFICATION.COURIER:
                    case CLASSIFICATION.CARGO:
                    case CLASSIFICATION.FREIGHTER: abrv = "MV"; break;
                    case CLASSIFICATION.FRIGATE: abrv = "FF"; break;
                    case CLASSIFICATION.DESTROYER: abrv = "DD"; break;
                    case CLASSIFICATION.CRUISER: abrv = "CA"; break;
                    case CLASSIFICATION.BATTLESHIP: abrv = "BB"; break;
                    case CLASSIFICATION.CARRIER: abrv = "CV"; break;
                    case CLASSIFICATION.DREADNAUGHT: abrv = "DN"; break;
                    case CLASSIFICATION.MINE: abrv = "MINE"; break;
                    case CLASSIFICATION.COMSAT: abrv = "COMS"; break;
                    case CLASSIFICATION.DEFSAT: abrv = "DEFS"; break;
                    case CLASSIFICATION.SWACS: abrv = "SWAC"; break;
                    default: break;
                }
            }

            if (scuttle < 1)
                scuttle = 1;

            if (splash_radius < 0)
                splash_radius = radius * 12.0f;
            if (repair_speed <= 1e-6)
                repair_speed = 1.0e-6f;

            if (commit_range <= 0)
            {
                if (type <= CLASSIFICATION.LCA)
                    commit_range = 80.0e3f;
                else
                    commit_range = 200.0e3f;
            }

            // calc standard loadout weights:
            foreach (var sl in loadouts)
            {
                for (int i = 0; i < hard_points.Count; i++)
                {
                    HardPoint hp = hard_points[i];
                    sl.mass += hp.GetCarryMass(sl.load[i]);
                }
            }
        }

        //~ShipDesign() { }

        internal static void PrepareModel(Model model)
        { }

        // public interface:
        public static void Initialize()
        {
            if (catalog.Count != 0) return;

            LoadCatalog("Ships/", "catalog.json");
            LoadSkins("Mods/Skins/");
#if TODO
            List<string> mod_designs = new List<string>();
            DataLoader loader = DataLoader.GetLoader();
            loader.SetDataPath("Mods/Ships/");
            loader.ListFiles("*.def", mod_designs, true);

            for (int i = 0; i < mod_designs.Count; i++)
            {
                string full_name = mod_designs[i];
                full_name.setSensitive(false);

                if (full_name.contains('/') && !full_name.contains("catalog"))
                {
                    string path;
                    strcpy_s(path, full_name.data());

                    string name = path + full_name.length();
                    while (*name != '/')
                        name--;

                    *name++ = 0;

                    char* p = strrchr(name, '.');
                    if (p && strlen(p) > 3)
                    {
                        if ((p[1] == 'd' || p[1] == 'D') &&
                                (p[2] == 'e' || p[2] == 'E') &&
                                (p[3] == 'f' || p[3] == 'F'))
                        {

                            *p = 0;
                        }
                    }

                    // Just do a quick parse of the def file and add the
                    // info to the catalog.  DON'T preload all of the models,
                    // textures, and weapons at this time.  That takes way
                    // too long with some of the larger user mods.

                    AddModCatalogEntry(name, path);
                }
            }

            mod_designs.Clear();
            loader.SetDataPath(0);
#endif
        }
        public static void Close()
        {
            mod_catalog.Clear();
            catalog.Clear();
        }

        public static bool CheckName(string design_name)
        {
            foreach (var entry in catalog)
            {
                if (entry.name == design_name)
                    return true;
            }

            foreach (var entry in mod_catalog)
            {
                if (entry.name == design_name)
                    return true;
            }

            return false;
        }

        public static ShipDesign Get(string design_name, string design_path = null)
        {
            if (string.IsNullOrWhiteSpace(design_name))
                return null;

            ShipCatalogEntry entry = null;

            for (int i = 0; i < catalog.Count; i++)
            {
                ShipCatalogEntry e = catalog[i];
                if (e.name == design_name)
                {
                    if (!string.IsNullOrWhiteSpace(design_path) && e.path != design_path)
                        continue;
                    entry = e;
                    break;
                }
            }

            if (entry == null)
            {
                for (int i = 0; i < mod_catalog.Count; i++)
                {
                    ShipCatalogEntry e = mod_catalog[i];
                    if (e.name == design_name)
                    {
                        if (!string.IsNullOrWhiteSpace(design_path))
                        {
                            string full_path = "Mods/Ships/";
                            full_path += design_path;

                            if (e.path != full_path)
                                continue;
                        }

                        entry = e;
                        break;
                    }
                }
            }

            if (entry != null)
            {
                if (entry.design == null)
                {
                    entry.design = new ShipDesign(entry.name,
                     entry.path,
                     entry.file,
                     entry.hide);
                }
                return entry.design;
            }
            else
            {
                ErrLogger.PrintLine("ShipDesign: no catalog entry for design '{0}', checking mods...", design_name);
                return ShipDesign.FindModDesign(design_name, design_path);
            }
        }

        public static ShipDesign FindModDesign(string design_name, string design_path = null)
        {
            string file = design_name + ".def";
            string path = "Mods/Ships/";

            if (!string.IsNullOrWhiteSpace(design_path))
                path += design_path;
            else
                path += design_name;
#if TODO
            DataLoader loader = DataLoader.GetLoader();
            loader.SetDataPath(path);
            ShipDesign design = new ShipDesign(design_name, path, file);

            if (design.valid)
            {
                ErrLogger.PrintLine("ShipDesign: found mod design '{0}'", design.name);

                ShipCatalogEntry entry = new ShipCatalogEntry(design.name, ClassName(design.type), path, file);
                mod_catalog.Add(entry);
                entry.design = design;
                return entry.design;
            }
            else
            {
                //delete design;
            }

            return null;
#endif
            throw new NotImplementedException();
        }

        public static void ClearModCatalog()
        {
            mod_catalog.Clear();

            for (int i = 0; i < catalog.Count; i++)
            {
                ShipCatalogEntry e = catalog[i];

                if (e != null && e.design != null)
                {
                    foreach (Skin skin in e.design.skins)
                    {
                        if (skin.Path() != null)
                            e.design.skins.Remove(skin); //TODO Cuidado va a dar una exception
                    }
                }
            }
        }

        public static int GetDesignList(int type, List<string> designs) // never destroy the design list!
        {
            designs.Clear();

            for (int i = 0; i < catalog.Count; i++)
            {
                ShipCatalogEntry e = catalog[i];

                CLASSIFICATION etype = ClassForName(e.type);
                if (((int)etype & type) != 0)
                {
                    if (e.design == null)
                        e.design = new ShipDesign(e.name, e.path, e.file, e.hide);

                    if (e.hide || e.design == null || !e.design.valid || e.design.secret)
                        continue;

                    designs.Add(e.name);
                }
            }

            for (int i = 0; i < mod_catalog.Count; i++)
            {
                ShipCatalogEntry e = mod_catalog[i];

                CLASSIFICATION etype = ClassForName(e.type);
                if (((int)etype & type) != 0)
                {
                    designs.Add(e.name);
                }
            }

            return designs.Count;
        }

        public static CLASSIFICATION ClassForName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return 0;

            for (int i = 0; i < 32; i++)
            {
                if (name.Equals(ship_design_class_name[i], StringComparison.InvariantCultureIgnoreCase))
                {
                    return (CLASSIFICATION)(1 << i);
                }
            }

            return 0;
        }

        public static string ClassName(int type)
        {
            if (type != 0)
            {
                int index = 0;

                while ((type & 1) == 0)
                {
                    type >>= 1;
                    index++;
                }

                if (index >= 0 && index < 32)
                    return ship_design_class_name[index];
            }

            return "Unknown";
        }

        public static void LoadCatalog(string path, string fname, bool mod = false)
        {
            ErrLogger.PrintLine("Loading ship design catalog: {0}{1}", path, fname);

            var sc = Serialization.ShipCatalogSerializer.JSONReadCatalogEntries(path, fname);
            foreach (var sd in sc.SHIPCATALOG)
            {
                if (string.IsNullOrEmpty(sd.name))
                    ErrLogger.PrintLine("WARNING: invalid or missing ship name in '{0}'", fname);
                if (string.IsNullOrEmpty(sd.type))
                    ErrLogger.PrintLine("WARNING: invalid or missing ship type in '{0}'", fname);
                if (string.IsNullOrEmpty(sd.path))
                    ErrLogger.PrintLine("WARNING: invalid or missing ship path in '{0}'", fname);
                if (string.IsNullOrEmpty(sd.file))
                    ErrLogger.PrintLine("WARNING: invalid or missing ship file in '{0}'", fname);
                ShipCatalogEntry entry = new ShipCatalogEntry(sd.name, sd.type, sd.path, sd.file, sd.hide);
                if (mod) mod_catalog.Add(entry);
                else catalog.Add(entry);
            }
        }

        public static void LoadSkins(string path, string archive = null)
        {
#if TODO
            // Load MOD Skin Files:
            List<string> list;
            DataLoader  loader = DataLoader.GetLoader();
            bool oldfs = loader.IsFileSystemEnabled();

            loader.UseFileSystem(true);
            loader.SetDataPath(path);
            loader.ListArchiveFiles(archive, "*.def", list);

            ListIter<Text> iter = list;
            while (++iter)
            {
                Text filename = *iter.value();
                BYTE* block;
                int blocklen = loader.LoadBuffer(filename, block, true);

                // file not found:
                if (blocklen <= 4)
                {
                    continue;
                }

                Parser parser(new BlockReader((string)block, blocklen));
                Term* term = parser.ParseTerm();
                ShipDesign* design = 0;

                if (!term)
                {
                    Print("ERROR: could not parse '%s'\n", filename.data());
                    return;
                }
                else
                {
                    TermText* file_type = term.isText();
                    if (!file_type || file_type.value() != "SKIN")
                    {
                        Print("ERROR: invalid skin file '%s'\n", filename.data());
                        return;
                    }
                }

                do
                {
                    delete term;

                    term = parser.ParseTerm();

                    if (term)
                    {
                        TermDef* def = term.isDef();
                        if (def)
                        {
                            Text defname = def.name().value();
                            defname.setSensitive(false);

                            if (defname == "name")
                            {
                                Text name;
                                GetDefText(name, def, filename);
                                design = Get(name);
                            }

                            else if (defname == "skin" && design != 0)
                            {
                                if (!def.term() || !def.term().isStruct())
                                {
                                    Print("WARNING: skin struct missing in '%s'\n", filename.data());
                                }
                                else
                                {
                                    TermStruct* val = def.term().isStruct();
                                    Skin* skin = design.ParseSkin(val);

                                    if (skin)
                                        skin.SetPath(archive);
                                }
                            }
                        }
                    }
                }
                while (term);
            }

            loader.UseFileSystem(oldfs);
#endif
            ErrLogger.PrintLine("WARNING: LoadSkins is not yet implemented.");
        }

        public static void PreloadCatalog(int index = -1)
        {
            if (index >= 0 && index < catalog.Count)
            {
                ShipCatalogEntry entry = catalog[index];

                if (entry.hide)
                    return;

                CLASSIFICATION ship_class = ClassForName(entry.type);
                if (ship_class > CLASSIFICATION.STARSHIPS)
                    return;

                if (!entry.path.Contains("Alliance_"))
                    return;

                if (entry.design == null)
                {
                    entry.design = new ShipDesign(entry.name, entry.path, entry.file, entry.hide);
                }
            }
            else
            {
                foreach (var entry in catalog)
                {

                    if (entry.design == null)
                    {
                        entry.design = new ShipDesign(entry.name, entry.path, entry.file, entry.hide);
                    }
                }
            }
        }

        public static int StandardCatalogSize()
        {
            return catalog.Count;
        }


        // general information:
        public string DisplayName()
        {
            if (!string.IsNullOrWhiteSpace(display_name))
                return display_name;

            return name;
        }


        public string filename;
        public string path_name;
        public string name;
        public string display_name;
        public string abrv;
        public CLASSIFICATION type;
        public float scale;
        public int auto_roll = 1;
        public bool valid;
        public bool secret;        // don't display in editor
        public string description;   // background info for tactical reference
        public string cockpit_name;

        // LOD representation:
        public int lod_levels;
        public List<Model>[] models = new List<Model>[4];
        public List<Point>[] offsets = new List<Point>[4];
        public float[] feature_size = new float[4];
        public List<Point> spin_rates = new List<Point>();

        // player selectable skins:
        public List<Skin> skins = new List<Skin>();
        public Skin FindSkin(string skin_name)
        {
            int n = skins.Count;

            foreach (Skin s in skins)
            {

                if (s.Name() == skin_name)
                    return s;
            }

            return null;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            ShipDesign other = obj as ShipDesign;
            if (other != null)
                return this.name.CompareTo(other.name);
            else
                throw new ArgumentException("Object is not a ShipDesign");
        }


        // virtual cockpit:
        public Model cockpit_model;
        public float cockpit_scale;

        // performance:
        public float vlimit;
        public float agility;
        public float air_factor;
        public float roll_rate;
        public float pitch_rate;
        public float yaw_rate;
        public float trans_x;
        public float trans_y;
        public float trans_z;
        public float turn_bank;
        public Point chase_vec;
        public Point bridge_vec;
        public Point beauty_cam;

        public float prep_time;

        // physical data:
        public float drag, roll_drag, pitch_drag, yaw_drag;
        public float arcade_drag;
        public float mass, integrity, radius;

        // aero data:
        public float CL, CD, stall;

        // weapons:
        public int primary;
        public int secondary;

        // drives:
        public int main_drive;

        // visibility:
        public float pcs;           // passive sensor cross section
        public float acs;           // active sensor cross section
        public float detet;         // maximum detection range
        public float[] e_factor = new float[3];   // pcs scaling by emcon setting

        // ai settings:
        public float avoid_time;
        public float avoid_fighter;
        public float avoid_strike;
        public float avoid_target;
        public float commit_range;

        // death spriral sequence:
        public float death_spiral_time;
        public float explosion_scale;
        public ShipExplosion[] explosion = new ShipExplosion[MAX_EXPLOSIONS];
        public ShipDebris[] debris = new ShipDebris[MAX_DEBRIS];

        public List<PowerSource> reactors = new List<PowerSource>();
        public List<Weapon> weapons = new List<Weapon>();
        public List<HardPoint> hard_points = new List<HardPoint>();
        public List<Drive> drives = new List<Drive>();
        public List<Computer> computers = new List<Computer>();
        public List<FlightDeck> flight_decks = new List<FlightDeck>();
        public List<NavLight> navlights = new List<NavLight>();
        public QuantumDrive quantum_drive;
        public Farcaster farcaster;
        public Thruster thruster;
        public Sensor sensor = null;
        public NavSystem navsys = null;
        public Shield shield = null;
        public Model shield_model;
        public Weapon decoy = null;
        public Weapon probe;
        public LandingGear gear;

        public float splash_radius;
        public float scuttle;
        public float repair_speed;
        public int repair_teams;
        public bool repair_auto;
        public bool repair_screen;
        public bool wep_screen;

        public string bolt_hit_sound;
        public string beam_hit_sound;

        public Sound bolt_hit_sound_resource;
        public Sound beam_hit_sound_resource;

        public List<ShipLoad> loadouts = new List<ShipLoad>();
        public List<Bitmap> map_sprites = new List<Bitmap>();
        public List<ShipSquadron> squadrons = new List<ShipSquadron>();

        public Bitmap beauty;
        public Bitmap hud_icon;

        public const int MAX_DEBRIS = 10;
        public const int MAX_EXPLOSIONS = 10;

        public static List<string>[] detail = new List<string>[4] { new List<string>(), new List<string>(), new List<string>(), new List<string>() };
        public static List<Point>[] offset = new List<Point>[4];

        public static readonly string[] ship_design_class_name = new string[32]{
            "Drone",          "Fighter",
            "Attack",         "LCA",
            "Courier",        "Cargo",
            "Corvette",       "Freighter",

            "Frigate",        "Destroyer",
            "Cruiser",        "Battleship",
            "Carrier",        "Dreadnaught",

            "Station",        "Farcaster",

            "Mine",           "DEFSAT",
            "COMSAT",         "SWACS",

            "Building",       "Factory",
            "SAM",            "EWR",
            "C3I",            "Starbase",

            "0x04000000",     "0x08000000",
            "0x10000000",     "0x20000000",
            "0x40000000",     "0x80000000"
        };

        private const int NAMELEN = 64;
        internal static bool degrees = false;

        private class ShipCatalogEntry
        {

            public ShipCatalogEntry() { }

            public ShipCatalogEntry(string n, string t, string p, string f, bool h = false)
            {
                name = n; type = t; path = p; file = f; hide = h; design = null;
            }

            //~ShipCatalogEntry() { delete design; }

            public string name;
            public string type;
            public string path;
            public string file;
            public bool hide = false;

            public ShipDesign design = null;
        }
        private static List<ShipCatalogEntry> catalog = new List<ShipCatalogEntry>();
        private static List<ShipCatalogEntry> mod_catalog = new List<ShipCatalogEntry>();

    }
}
