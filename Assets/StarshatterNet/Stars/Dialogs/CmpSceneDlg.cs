﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmpSceneDlg.h/CmpSceneDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Campaign title card and load progress dialog
*/
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmpSceneDlg : CmdDlg
    {
        public CmpSceneDlg(Screen s, Camera c, FormDef def, CmpnScreen mgr, GameObject p) :
             base(s, mgr, 0, 0, s.Width(), s.Height(), c, p, "CmpSceneDlg")
        {
            mov_scene = null; subtitles_box = null; cam_view = null; disp_view = null; old_disp_win = null;
            flare1 = null; flare2 = null; flare3 = null; flare4 = null; subtitles_delay = 0; subtitles_time = 0;

            Init(def);
#if TODO
            DataLoader  loader = DataLoader.GetLoader();
            const char* oldpath = loader.GetDataPath();

            loader.SetDataPath(0);
            loader.LoadTexture("flare0+.pcx", flare1, Bitmap.BMP_TRANSLUCENT);
            loader.LoadTexture("flare2.pcx", flare2, Bitmap.BMP_TRANSLUCENT);
            loader.LoadTexture("flare3.pcx", flare3, Bitmap.BMP_TRANSLUCENT);
            loader.LoadTexture("flare4.pcx", flare4, Bitmap.BMP_TRANSLUCENT);
            loader.SetDataPath(oldpath);
#endif
        }
        //public virtual ~CmpSceneDlg();
        public override void RegisterControls()
        {
            mov_scene = FindControl(101);
            subtitles_box = (RichTextBox)FindControl(102);

            if (mov_scene != null)
            {
#if TODO
                CameraDirector cam_dir = CameraDirector.GetInstance();
                cam_view = new CameraView(mov_scene, cam_dir.GetCamera(), 0);
                if (cam_view)
                    mov_scene.AddView(cam_view);

                disp_view = DisplayView.GetInstance();
#endif
            }
        }
        public override void Show()
        {
            base.Show();

            Starshatter  stars = Starshatter.GetInstance();

            if (stars.InCutscene())
            {
                Sim  sim = Sim.GetSim();
#if TODO
                if (sim!=null)
                {
                    cam_view.UseCamera(CameraDirector.GetInstance().GetCamera());
                    cam_view.UseScene(sim.GetScene());
                }

                // initialize lens flare bitmaps:
                if (stars.LensFlare())
                {
                    cam_view.LensFlareElements(flare1, flare4, flare2, flare3);
                    cam_view.LensFlare(true);
                }

                // if lens flare disabled, just create the corona:
                else if (stars.Corona())
                {
                    cam_view.LensFlareElements(flare1, 0, 0, 0);
                    cam_view.LensFlare(true);
                }

                if (disp_view)
                {
                    old_disp_win = disp_view.GetWindow();

                    disp_view.SetWindow(mov_scene);
                    mov_scene.AddView(disp_view);
                }
#endif
                ErrLogger.PrintLine("WARNING: CmpSceneDlg.Show is not yet implemented.");

                if (subtitles_box != null)
                {
                    subtitles_box.SetText(stars.GetSubtitles());
                    subtitles_delay = 0;
                    subtitles_time = 0;
                }
            }
        }
        public override void Hide()
        {
            base.Hide();

            if (disp_view != null && mov_scene != null && old_disp_win != null)
            {
#if TODO
                mov_scene.DelView(disp_view);
                disp_view.SetWindow(old_disp_win);
#endif
                ErrLogger.PrintLine("WARNING: CmpSceneDlg.Hide is not yet implemented.");
            }
        }
        public override void ExecFrame()
        {
             Mission  cutscene_mission = stars.GetCutsceneMission();

            if (cutscene_mission != null && disp_view != null)
            {
#if TODO
                disp_view.ExecFrame();
#endif
                ErrLogger.PrintLine("WARNING: CmpSceneDlg.ExecFrame is not yet implemented.");

                if (subtitles_box != null && subtitles_box.GetLineCount() > 0)
                {
                    if (subtitles_delay == 0)
                    {
                        int nlines = subtitles_box.GetLineCount();

                        MissionEvent  begin_scene = cutscene_mission.FindEvent(MissionEvent.EVENT_TYPE.BEGIN_SCENE);
                        MissionEvent  end_scene = cutscene_mission.FindEvent(MissionEvent.EVENT_TYPE.END_SCENE);

                        if (begin_scene != null && end_scene != null)
                        {
                            double total_time = end_scene.Time() - begin_scene.Time();
                            subtitles_delay = total_time / nlines;
                            subtitles_time = Game.RealTime() / 1000.0 + subtitles_delay;
                        }
                        else
                        {
                            subtitles_delay = -1;
                        }
                    }

                    if (subtitles_delay > 0)
                    {
                        double seconds = Game.RealTime() / 1000.0;

                        if (subtitles_time <= seconds)
                        {
                            subtitles_time = seconds + subtitles_delay;
                            subtitles_box.Scroll(ScrollWindow.SCROLL.SCROLL_DOWN);
                        }
                    }
                }
            }
            else
            {
                manager.ShowCmdDlg();
            }
        }
        public CameraView GetCameraView()
        {
            return cam_view;
        }
        public DisplayView GetDisplayView()
        {
            return disp_view;
        }



        protected ActiveWindow mov_scene;
        protected RichTextBox subtitles_box;
        protected CameraView cam_view;
        protected DisplayView disp_view;
        protected Window old_disp_win;

        protected Bitmap flare1;
        protected Bitmap flare2;
        protected Bitmap flare3;
        protected Bitmap flare4;

        protected CmpnScreen manager;

        protected double subtitles_delay;
        protected double subtitles_time;

    }
}