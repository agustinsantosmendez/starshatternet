﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmdMsgDlg.h/CmdMsgDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Navigation Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmdMsgDlg : CmdDlg
    {
        public CmdMsgDlg(Screen s, Camera c, FormDef def, CmpnScreen mgr, GameObject p) :
               base(s, mgr, 0, 0, s.Width(), s.Height(), c, p, "CmdMsgDlg")
        {
            exit_latch = false;
            Init(def);
        }
        //public virtual ~CmdMsgDlg();
        public override void RegisterControls()
        {
            title = FindControl(100);
            message = FindControl(101);

            apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, apply, OnApply);
        }

        public override void Show()
        {
            base.Show();
            //TODO SetFocus();
        }

        // Operations:
        public override void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnApply(null, null);
            }

            if (Keyboard.KeyDown(KeyMap.VK_ESCAPE))
            {
                if (!exit_latch)
                    OnApply(null, null);

                exit_latch = true;
            }
            else
            {
                exit_latch = false;
            }
        }

        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            if (cmpn_screen != null)
                cmpn_screen.CloseTopmost();
        }

        public ActiveWindow Title() { return title; }
        public ActiveWindow Message() { return message; }


        protected CmpnScreen manager;

        protected ActiveWindow title;
        protected ActiveWindow message;

        protected Button apply;
        protected bool exit_latch;
    }
}