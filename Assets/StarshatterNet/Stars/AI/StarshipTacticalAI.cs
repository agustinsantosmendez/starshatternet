﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      StarshipTacticalAI.h/StarshipTacticalAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Starship-specific mid-level (tactical) AI
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.AI
{
    public class StarshipTacticalAI : TacticalAI
    {
        public StarshipTacticalAI(ShipAI ai) : base(ai)
        {
            drop_time = 1.0e9; bugout = false; ai_level = 0; initial_integrity = 0;
            if (ai != null && ai.GetShip() != null)
            {
                ai_level = ai.GetAILevel();
                initial_integrity = ai.GetShip().Integrity();
            }

            switch (ai_level)
            {
                default:
                case 2: THREAT_REACTION_TIME = 500; break;
                case 1: THREAT_REACTION_TIME = 1000; break;
                case 0:
                    THREAT_REACTION_TIME = 2500;
                    drop_time = STARSHIP_TACTICAL_DROP_TIME +
                    RandomHelper.Random(0, STARSHIP_TACTICAL_DROP_TIME);
                    break;
            }
        }
        // ~StarshipTacticalAI();

        public override void ExecFrame(double seconds)
        {
            base.ExecFrame(seconds);

            drop_time -= seconds;

            if (drop_time <= 0)
            {
                drop_time = STARSHIP_TACTICAL_DROP_TIME;

                ship_ai.DropTarget(STARSHIP_TACTICAL_DROP_TIME / 4);
            }
        }



        protected virtual void FindThreat()
        {
            // pick the closest contact on Threat Warning System:
            Ship threat_ship = null;
            Shot threat_missile = null;
            Ship rumor = null;
            double threat_dist = 1e9;
            double CELL_SIZE = 20e3;

            threat_level = 0;
            support_level = ship.AIValue() / CELL_SIZE;

            foreach (Contact contact in ship.ContactList())
            {
                Ship c_ship = contact.GetShip();
                Shot c_shot = contact.GetShot();

                if (c_ship == null && c_shot == null)
                    continue;

                if (c_ship != null && c_ship != ship)
                {
                    double basis = Math.Max(contact.Range(ship), CELL_SIZE);
                    double ai_value = c_ship.AIValue() / basis;

                    if (c_ship.GetIFF() == ship.GetIFF())
                    {
                        support_level += ai_value;
                    }
                    else if (ship.GetIFF() > 0 && c_ship.GetIFF() > 0)
                    {
                        threat_level += ai_value;
                    }
                    else if (c_ship.GetIFF() > 1)
                    { // neutrals should not be afraid of alliance
                        threat_level += ai_value;
                    }
                }

                if (contact.Threat(ship) &&
                        (Game.GameTime() - contact.AcquisitionTime()) > THREAT_REACTION_TIME)
                {

                    if (c_shot != null)
                    {
                        threat_missile = c_shot;
                        rumor = (Ship)threat_missile.Owner();
                    }
                    else
                    {
                        double rng = contact.Range(ship);

                        if (c_ship != null &&
                                c_ship.Class() != CLASSIFICATION.FREIGHTER &&
                                c_ship.Class() != CLASSIFICATION.FARCASTER)
                        {


                            if (c_ship.GetTarget() == ship)
                            {
                                if (threat_ship == null || c_ship.Class() > threat_ship.Class())
                                {
                                    threat_ship = c_ship;
                                    threat_dist = 0;
                                }
                            }
                            else if (rng < threat_dist)
                            {
                                threat_ship = c_ship;
                                threat_dist = rng;
                            }

                            CheckBugOut(c_ship, rng);
                        }
                    }
                }
            }

            if (rumor != null)
            {
                foreach (Contact contact in ship.ContactList())
                {
                    if (contact.GetShip() == rumor)
                    {
                        rumor = null;
                        ship_ai.ClearRumor();
                        break;
                    }
                }
            }

            ship_ai.SetRumor(rumor);
            ship_ai.SetThreat(threat_ship);
            ship_ai.SetThreatMissile(threat_missile);
        }

        protected override void FindSupport()
        {
            if (threat_level < 0.01)
            {
                ship_ai.SetSupport(null);
                return;
            }

            // pick the biggest friendly contact in the sector:
            Ship support = null;
            double support_dist = 1e9;

            foreach (Contact contact in ship.ContactList())
            {
                if (contact.GetShip() != null && contact.GetIFF(ship) == ship.GetIFF())
                {
                    Ship c_ship = contact.GetShip();

                    if (c_ship != ship && c_ship.Class() >= ship.Class())
                    {
                        if (support == null || c_ship.Class() > support.Class())
                            support = c_ship;
                    }
                }
            }

            ship_ai.SetSupport(support);
        }

        protected virtual void CheckBugOut(Ship c_ship, double rng)
        {
            // see if carrier should bug out...
            if (ship == null || c_ship == null || ship.Class() != CLASSIFICATION.CARRIER && ship.Class() != CLASSIFICATION.SWACS)
                return;

            if (bugout)
                return;

            if (ship.GetElement() != null && ship.GetElement().GetZoneLock() != 0)
                return;

            if (c_ship.Class() < CLASSIFICATION.DESTROYER || c_ship.Class() > CLASSIFICATION.STATION)
                return;

            Starshatter stars = Starshatter.GetInstance();
            if (stars != null && stars.InCutscene())
                return;

            double sustained_damage = initial_integrity - ship.Integrity();
            double allowable_damage = ship.Design().integrity * 0.25;

            if (rng > 50e3 && sustained_damage < allowable_damage)
                return;

            // still here?  we must need to bug out!

            Sim sim = Sim.GetSim();
            SimRegion dst = null;

            List<SimRegion> regions = sim.GetRegions();

            if (regions.Count > 1)
            {
                int tries = 10;
                while (dst == null && tries-- != 0)
                {
                    int n = RandomHelper.RandomIndex() % regions.Count;
                    dst = regions[n];

                    if (dst == ship.GetRegion() || dst.IsAirSpace())
                        dst = null;
                }
            }

            if (dst != null)
            {
                // bug out!
                QuantumDrive quantum = ship.GetQuantumDrive();
                if (quantum != null)
                {
                    quantum.SetDestination(dst, new Point(0, 0, 0));
                    quantum.Engage();
                }

                // ask highest ranking escort to go with you:
                Element escort = null;

                foreach (Element elem in sim.GetElements())
                {
                    if (escort == null || elem.GetShipClass() > escort.GetShipClass())
                    {
                        if (ship.GetElement().CanCommand(elem))
                            escort = elem;
                    }
                }

                if (escort != null)
                {
                    RadioMessage msg = new RadioMessage(escort, ship, RadioMessage.ACTION.QUANTUM_TO);
                    if (msg != null)
                    {
                        msg.SetInfo(dst.Name());
                        RadioTraffic.Transmit(msg);
                    }
                }

                bugout = true;
            }
        }


        protected uint THREAT_REACTION_TIME;
        protected int ai_level;
        protected double drop_time;
        protected double initial_integrity;
        protected bool bugout;
        private const double STARSHIP_TACTICAL_DROP_TIME = 15;
    }
}