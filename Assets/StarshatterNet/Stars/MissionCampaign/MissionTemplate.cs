﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MissionTemplate.h/MissionTemplate.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Universe and Region classes
 */
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.StarSystems;
using System;
using System.Collections.Generic;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning MissionTemplate class is still in development and is not recommended for production.
    public class MissionTemplate : Mission
    {
        public MissionTemplate(int identity, string filename = null, string path = null) : base(identity, filename, path) { }
        // ~MissionTemplate();

        public override bool Load(string fname = null, string pname = null)
        {
            ok = false;

            if (!string.IsNullOrWhiteSpace(fname))
                filename = fname;

            if (!string.IsNullOrWhiteSpace(pname))
                path = pname;

            if (string.IsNullOrWhiteSpace(filename))
            {
                ErrLogger.PrintLine("\nCan't Load Mission Template, script unspecified.");
                return ok;
            }
            // wipe existing mission before attempting to load...
            elements.Clear();
            events.Clear();

            ErrLogger.PrintLine("\nLoad Mission Template: '{0}'", filename);
            ok = ParseMissionTemplate(filename);

            if (ok)
            {
                CheckObjectives();

                ErrLogger.PrintLine("Mission Template Loaded.");
            }

            return ok;

        }

        // accessors/mutators:
        public override MissionElement FindElement(string n)
        {
            string name = n;

            foreach (MissionCallsign c in callsigns)
            {
                if (c.Name() == name)
                {
                    name = c.Callsign();
                    break;
                }
            }

            foreach (MissionAlias a in aliases)
            {
                if (a.Name() == name)
                    return a.Element();
            }

            foreach (MissionElement elem in elements)
            {
                if (elem.Name() == name)
                    return elem;
            }

            return null;
        }
        public override void AddElement(MissionElement elem)
        {
            if (elem != null)
            {
                elements.Add(elem);
                aliases.Add(new MissionAlias(elem.Name(), elem));
            }
        }

        public virtual bool MapElement(MissionElement elem)
        {
            bool result = false;

            if (elem != null && elem.GetCombatUnit() == null)
            {
                if (elem.IsDropship())
                {
                    string callsign = MapCallsign(elem.Name(), elem.GetIFF());

                    if (callsign.Length > 0)
                        elem.SetName(callsign);
                }

                foreach (Instruction i in elem.Objectives())
                {
                    if (!string.IsNullOrEmpty(i.TargetName()))
                    {
                        // find a callsign, only if one already exists:
                        string callsign = MapCallsign(i.TargetName(), -1);
                        if (callsign.Length > 0)
                            i.SetTarget(callsign);
                    }
                }

                foreach (Instruction i in elem.NavList())
                {
                    if (i.TargetName().Length > 0)
                    {
                        // find a callsign, only if one already exists:
                        string callsign = MapCallsign(i.TargetName(), -1);
                        if (callsign.Length > 0)
                            i.SetTarget(callsign);
                    }
                }

                CombatGroup g = FindCombatGroup(elem.GetIFF(), elem.GetDesign());

                if (g != null)
                {
                    CombatUnit u = g.GetNextUnit();

                    if (u != null)
                    {
                        elem.SetCombatGroup(g);
                        elem.SetCombatUnit(u);
                    }
                }

                if (elem.GetCombatUnit() != null)
                {
                    MissionElement cmdr = FindElement(elem.Commander());
                    if (cmdr != null)
                        elem.SetCommander(cmdr.Name());

                    MissionElement sqdr = FindElement(elem.Squadron());
                    if (sqdr != null)
                        elem.SetSquadron(sqdr.Name());

                    if (!elem.IsDropship())
                    {
                        aliases.Add(new MissionAlias(elem.Name(), elem));
                        elem.SetName(elem.GetCombatUnit().Name());
                    }

                    result = true;
                }
            }

            return result;
        }
        public virtual string MapShip(string name)
        {
            string result = name;
            int len = name.Length;

            if (len > 0)
            {
                MissionElement elem = null;

                // single ship from an element (e.g. "Alpha 1")?
                if (Char.IsDigit(name[len - 1]) && Char.IsWhiteSpace(name[len - 2]))
                {
                    string elem_name = name.Substring(0, len - 2);

                    elem = FindElement(elem_name);
                    if (elem != null)
                        result = elem.Name() + name.Substring(len - 2, 2);
                }

                // full element name
                // (also used to try again if single-ship search fails)
                if (elem == null)
                {
                    elem = FindElement(name);

                    if (elem != null)
                        result = elem.Name();
                }
            }

            return result;
        }
        public virtual CombatGroup GetPlayerSquadron() { return player_squadron; }
        public virtual void SetPlayerSquadron(CombatGroup ps) { player_squadron = ps; }
        public virtual string MapCallsign(string name, int iff)
        {
            for (int i = 0; i < callsigns.Count; i++)
            {
                if (callsigns[i].Name() == name)
                    return callsigns[i].Callsign();
            }

            if (iff >= 0)
            {
                string callsign = Callsign.GetCallsign(iff);
                MissionCallsign mc = new MissionCallsign(callsign, name);
                callsigns.Add(mc);

                return mc.Callsign();
            }

            return name;
        }
        public virtual bool MapEvent(MissionEvent evnt)
        {
            bool result = false;

            if (evnt != null)
            {
                evnt.event_ship = MapShip(evnt.event_ship);
                evnt.event_source = MapShip(evnt.event_source);
                evnt.event_target = MapShip(evnt.event_target);
                evnt.trigger_ship = MapShip(evnt.trigger_ship);
                evnt.trigger_target = MapShip(evnt.trigger_target);

                result = true;
            }

            return result;
        }



        static int combat_group_index = 0;
        protected CombatGroup FindCombatGroup(int iff, ShipDesign dsn)
        {
            CombatGroup result = null;
            Campaign campaign = Campaign.GetCampaign();
            List<CombatGroup> group_list = new List<CombatGroup>();

            if (campaign != null)
            {
                foreach (Combatant combatant in campaign.GetCombatants())
                {
                    if (combatant.GetIFF() == iff)
                    {
                        SelectCombatGroups(combatant.GetForce(), dsn, group_list);
                    }
                }

                if (group_list.Count > 0)
                    result = group_list[combat_group_index++ % group_list.Count];
            }

            return result;
        }

        protected void CheckObjectives()
        {
            foreach (MissionElement elem in elements)
            {
                var objs = elem.Objectives();
                for (int i = objs.Count - 1; i >= 0; i--)
                {
                    Instruction o = elem.Objectives()[i];
                    string tgt = o.TargetName();

                    MissionElement tgt_elem = null;

                    if (tgt.Length > 0)
                    {
                        tgt_elem = FindElement(tgt);

                        if (tgt_elem == null)
                            //obj.removeItem();
                            objs.RemoveAt(i);
                        else
                            o.SetTarget(tgt_elem.Name());
                    }
                }
            }
        }

        static void SelectCombatGroups(CombatGroup g, ShipDesign d, List<CombatGroup> list)
        {
            if (g.IntelLevel() <= Intel.INTEL_TYPE.RESERVE)
                return;

            if (g.GetUnits().Count > 0)
            {
                for (int i = 0; i < g.GetUnits().Count; i++)
                {
                    CombatUnit u = g.GetUnits()[i];
                    if (u.GetDesign() == d && u.Count() - u.DeadCount() > 0)
                    {
                        list.Add(g);
                    }
                }
            }

            foreach (CombatGroup subgroup in g.GetComponents())
                SelectCombatGroups(subgroup, d, list);
        }


        protected virtual bool ParseMissionTemplate(string fname)
        {
            bool ok = false;

            string target_name = null;
            string ward_name = null;

            var mi = ReaderSupport.DeserializeAsset<Serialization.MissionTemplateMainInfo>(fname);
            if (mi == null) return false;

            Serialization.MissionTemplateInfo m = mi.MISSION_TEMPLATE;
            if (!string.IsNullOrEmpty(m.name))
                name = Game.GetText(m.name);
            if (!string.IsNullOrEmpty(m.type))
                type = TypeFromName(m.type);
            if (!string.IsNullOrEmpty(m.system))
            {
                Galaxy galaxy = Galaxy.GetInstance();

                if (galaxy != null)
                {
                    SetStarSystem(galaxy.GetSystem(m.system));
                }
            }
            if (!string.IsNullOrEmpty(m.degrees))
                degrees = bool.Parse(m.degrees);
            if (!string.IsNullOrEmpty(m.region))
                region = m.region;
            if (m.objective != null && m.objective.Length > 0)
                objective = string.Join(" ", m.objective);
            if (m.sitrep != null && m.sitrep.Length > 0)
                sitrep = string.Join(" ", m.sitrep);
            if (!string.IsNullOrEmpty(m.start))
                start = ReaderSupport.GetDefTime(m.start);
            if (!string.IsNullOrEmpty(m.team))
                team = int.Parse(m.team);
            if (!string.IsNullOrEmpty(m.target))
                target_name = m.target;
            if (!string.IsNullOrEmpty(m.ward))
                ward_name = m.ward;

            // ParseAlias
            // ParseCallsign
            // ParseOptional

            if (m.elements != null && m.elements.Length != 0)
                foreach (var e in m.elements)
                {
                    MissionElement elem = ParseElement(e, filename);
                    AddElement(elem);
                }
            if (m.events != null && m.events.Length != 0)
                foreach (var e in m.events)
                {
                    MissionEvent evnt = ParseEvent(e, filename);
                    AddEvent(evnt);
                }
            if (!string.IsNullOrEmpty(target_name))
                target = FindElement(target_name);

            if (!string.IsNullOrEmpty(ward_name))
                ward = FindElement(ward_name);

            return ok;
        }
        protected Serialization.MissionAlias ParseAlias(Serialization.MissionAlias val, string file)
        {
            throw new NotImplementedException();
        }
        protected Serialization.MissionCallsign ParseCallsign(Serialization.MissionCallsign val, string file)
        {
            throw new NotImplementedException();
        }
        protected Serialization.MissionOptional ParseOptional(Serialization.MissionOptional val, string file)
        {
            throw new NotImplementedException();
        }

        protected List<MissionAlias> aliases = new List<MissionAlias>();
        protected List<MissionCallsign> callsigns = new List<MissionCallsign>();
        protected CombatGroup player_squadron;
    }

    // +--------------------------------------------------------------------+

    public class MissionAlias
    {

        public MissionAlias() { elem = null; }
        public MissionAlias(string n, MissionElement e) { name = n; elem = e; }
        // ~MissionAlias() { }


        public string Name() { return name; }
        public MissionElement Element() { return elem; }

        public void SetName(string n) { name = n; }
        public void SetElement(MissionElement e) { elem = e; }

        // int operator ==(const MissionAlias& a)   const { return name == a.name;  }
        public override bool Equals(object obj)
        {
            var alias = obj as MissionAlias;
            return alias != null &&
                   name == alias.name;
        }

        public override int GetHashCode()
        {
            return 363513814 + EqualityComparer<string>.Default.GetHashCode(name);
        }

        protected string name;
        protected MissionElement elem;
    }

    // +--------------------------------------------------------------------+

    public class MissionCallsign
    {
        public MissionCallsign()
        { }
        public MissionCallsign(string c, string n) { call = c; name = n; }
        // ~MissionCallsign() { }


        public string Callsign() { return call; }
        public string Name() { return name; }

        public void SetCallsign(string c) { call = c; }
        public void SetName(string n) { name = n; }

        //int operator ==(const MissionCallsign& a)const { return call == a.call;  }
        public override bool Equals(object obj)
        {
            var callsign = obj as MissionCallsign;
            return callsign != null &&
                   call == callsign.call;
        }

        public override int GetHashCode()
        {
            return -69772271 + EqualityComparer<string>.Default.GetHashCode(call);
        }

        protected string call;
        protected string name;
    }
}
namespace StarshatterNet.Stars.MissionCampaign.Serialization
{
    [Serializable]
    public class MissionTemplateMainInfo
    {
        public MissionTemplateInfo MISSION_TEMPLATE;
    }
    [Serializable]
    public class MissionTemplateInfo
    {
        public string name;
        public string type;
        public string system;
        public string degrees;
        public string region;
        public string[] objective;
        public string[] sitrep;
        public string start;
        public string team;
        public string target;
        public string ward;
        public MissionAlias alias;
        public MissionCallsign callsign;
        public MissionOptional optional;
        public MissionElement[] elements; // "element", "ship", "station"
        public MissionEvent[] events;
    }
    [Serializable]
    public class MissionAlias
    {
        public string name;
        public string elem;
        public string code;
        public string design;
        public string iff;
        public string[] loc;
        public string[] rloc;
        public string player;
    }
    [Serializable]
    public class MissionCallsign
    {
        public string name;
        public string iff;
    }
    [Serializable]
    public class MissionOptional
    {
        public string min;
        public string max;
        public MissionOptional optional;
        public MissionElement[] elements;
    }

}
