﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      QuantumView.h/QuantumView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    View class for Radio Communications HUD Overlay
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.Views
{
    public class QuantumView : View, ISimObserver
    {
        public QuantumView(Window c) :
            base(c, "QuantumView")
        {
            sim = null; ship = null; font = null;
            quantum_view = this;
            sim = Sim.GetSim();

            width = (int)windowObj.Width();
            height = (int)windowObj.Height();
            xcenter = (width / 2.0) - 0.5;
            ycenter = (height / 2.0) + 0.5;
            font = FontMgr.Find("HUD");

            HUDView hud = HUDView.GetInstance();
            if (hud != null)
                SetColor(hud.GetTextColor());
        }
        //public virtual ~QuantumView();

        // Operations:
        public override void Refresh()
        {
            sim = Sim.GetSim();

            if (sim != null && ship != sim.GetPlayerShip())
            {
                ship = sim.GetPlayerShip();

                if (ship != null)
                {
                    if (ship.Life() == 0 || ship.IsDying() || ship.IsDead())
                    {
                        ship = null;
                    }
                    else
                    {
                        Observe(ship);
                    }
                }
            }

            if (IsMenuShown())
            {
                Rect menu_rect = new Rect(width - 115, 10, 115, 12);

                font.SetColor(hud_color);
                font.SetAlpha(1);
                font.DrawText(quantum_menu.GetTitle(), 0, menu_rect, TextFormat.DT_CENTER);

                menu_rect.y += 15;

                foreach (MenuItem item in quantum_menu.GetItems())
                {
                    item.SetEnabled(true);

                    font.DrawText(item.GetText(), 0, menu_rect, TextFormat.DT_LEFT);
                    menu_rect.y += 10;
                }
            }
        }
        public override void OnWindowMove()
        {
            width = (int)windowObj.Width();
            height = (int)windowObj.Height();
            xcenter = (width / 2.0) - 0.5;
            ycenter = (height / 2.0) + 0.5;
        }
        static double time_til_change = 0;
        public virtual void ExecFrame()
        {
            HUDView hud = HUDView.GetInstance();
            if (hud != null)
            {
                if (hud_color != hud.GetTextColor())
                {
                    hud_color = hud.GetTextColor();
                    SetColor(hud_color);
                }
            }

            if (time_til_change > 0)
                time_til_change -= Game.GUITime();

            if (time_til_change <= 0)
            {
                time_til_change = 0;

                if (show_menu)
                {
                    QuantumDrive quantum_drive = null;

                    if (ship != null)
                        quantum_drive = ship.GetQuantumDrive();

                    if (quantum_drive != null && quantum_drive.ActiveState() != QuantumDrive.ACTIVE_STATES.ACTIVE_READY)
                    {
                        show_menu = false;
                        return;
                    }

                    int max_items = quantum_menu.NumItems();

                    for (int i = 0; i < max_items; i++)
                    {
                        if (Keyboard.KeyDown('1' + i))
                        {
                            MenuItem item = quantum_menu.GetItem(i);
                            if (item != null && item.GetEnabled())
                            {

                                SimRegion rgn = (SimRegion)item.GetData();

                                if (rgn != null)
                                {
                                    quantum_drive.SetDestination(rgn, new Point(0, 0, 0));
                                    quantum_drive.Engage();
                                }

                                show_menu = false;
                                time_til_change = 0.3;
                                break;
                            }
                        }
                    }
                }
            }
        }

        public virtual Menu GetQuantumMenu(Ship s)
        {
            if (s != null && sim != null)
            {
                if (s.IsStarship())
                {
                    quantum_menu.ClearItems();

                    SimRegion current_region = ship.GetRegion();

                    if (current_region == null) return null;

                    StarSystem current_system = current_region.System();

                    List<SimRegion> rgn_list = new List<SimRegion>();

                    foreach (SimRegion rgn in sim.GetRegions())
                    {
                        StarSystem rgn_system = rgn.System();

                        if (rgn != ship.GetRegion() && !rgn.IsAirSpace() &&
                                rgn_system == current_system)
                        {
                            rgn_list.Add(rgn);
                        }
                    }

                    // sort local regions by distance from star:
                    rgn_list.Sort();

                    // now add regions in other star systems:
                    foreach (SimRegion rgn in sim.GetRegions())
                    {
                        StarSystem rgn_system = rgn.System();

                        if (rgn != ship.GetRegion() && rgn.Type() != SimRegion.TYPES.AIR_SPACE &&
                                rgn_system != current_system && current_region.Links().Contains(rgn))
                        {
                            rgn_list.Add(rgn);
                        }
                    }

                    int n = 1;
                    // iter.attach(rgn_list);
                    foreach (SimRegion rgn in rgn_list)
                    {
                        StarSystem rgn_system = rgn.System();
                        string text;

                        if (rgn_system != current_system)
                            text = string.Format("{0}. {1}/{2}", n++, rgn_system.Name(), rgn.Name());
                        else
                            text = string.Format("{0}. {1}", n++, rgn.Name());

                        quantum_menu.AddItem(text, rgn);
                    }

                    return quantum_menu;
                }
            }

            return null;
        }
        public virtual bool IsMenuShown()
        {
            return show_menu;
        }
        public virtual void ShowMenu()
        {
            if (ship == null) return;

            if (!show_menu)
            {
                if (ship.IsStarship() && ship.GetQuantumDrive() != null)
                {
                    GetQuantumMenu(ship);
                    show_menu = true;
                }

                for (int i = 0; i < 10; i++)
                {
                    if (Keyboard.KeyDown('1' + i))
                    {
                        // just need to clear the key down flag
                        // so we don't process old keystrokes
                        // as valid menu picks...
                    }
                }
            }
        }
        public virtual void CloseMenu()
        {
            show_menu = false;
        }

        public virtual bool Update(SimObject obj)
        {
            if (obj == ship)
                ship = null;

            return simObserver.Update(obj);
        }
        public virtual string GetObserverName() { return "QuantumView"; }

        public static void SetColor(Color c)
        {
            hud_color = c;
        }

        public static void Initialize()
        {
            if (initialized) return;

            quantum_menu = new Menu(Game.GetText("QuantumView.menu"));

            initialized = true;
        }
        public static void Close()
        {
            //delete quantum_menu;
        }

        public static QuantumView GetInstance() { return quantum_view; }


        public void Observe(SimObject obj)
        {
            simObserver.Observe(obj);
        }

        public void Ignore(SimObject obj)
        {
            simObserver.Ignore(obj);
        }

        protected SimObserver simObserver = new SimObserver();
        protected int width, height;
        protected double xcenter, ycenter;

        protected FontItem font;
        protected Sim sim;
        protected Ship ship;

        protected static QuantumView quantum_view;

        protected static Menu quantum_menu = null;
        protected static bool show_menu = false;
        protected static Color hud_color = Color.black;
        protected static bool initialized = false;
    }
}