﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      ShipCtrl.h/ShipCtrl.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Starship (or space/ground station) class
*/
using System;
using StarshatterNet.Audio;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using static StarshatterNet.Stars.AI.SteerAI;

namespace StarshatterNet.Stars.AI
{
    public class ShipCtrl : Director
    {
        public const int DIR_TYPE = 1;

        public ShipCtrl(Ship s, MotionController ctrl)
        {
            ship = s; controller = ctrl; throttle_active = false; launch_latch = false;
            pickle_latch = false; target_latch = false;
            for (int i = 0; i < 10; i++)
                GetAsyncKeyState('0' + i);
        }

        private void GetAsyncKeyState(int v)
        {
            throw new NotImplementedException();
        }

        static double time_til_change = 0.0;
        public override void ExecFrame(double seconds)
        {
            Starshatter stars = Starshatter.GetInstance();
            if (stars != null && stars.GetChatMode() != 0) return;
            if (ship == null) return;

            const int DELTA_THROTTLE = 5;

            controller.Acquire();

            if (ship.IsStarship() && ship.GetFLCSMode() == (int)FLCS_MODE.FLCS_HELM)
            {
                ship.ApplyHelmPitch(controller.Pitch() * seconds);
                ship.ApplyHelmYaw(controller.Yaw() * seconds);
            }
            else
            {
                ship.ApplyRoll(controller.Roll());
                ship.ApplyPitch(controller.Pitch());
                ship.ApplyYaw(controller.Yaw());
            }

            ship.SetTransX(controller.X() * ship.Design().trans_x);
            ship.SetTransY(controller.Y() * ship.Design().trans_y);
            ship.SetTransZ(controller.Z() * ship.Design().trans_z);

            bool augmenter = false;

            if (controller.Throttle() > 0.05)
            {
                if (throttle_active)
                {
                    ship.SetThrottle(controller.Throttle() * 100);

                    if (controller.Throttle() >= 0.99)
                        augmenter = true;
                }
                else if (!launch_latch || controller.Throttle() > 0.5)
                {
                    throttle_active = true;
                    launch_latch = false;
                }
            }
            else if (throttle_active)
            {
                ship.SetThrottle(0);
                throttle_active = false;
            }

            ship.ExecFLCSFrame();

            time_til_change = 0.0;

            if (time_til_change < 0.001)
            {
                if (KeyDown(KeyMap.KEY_THROTTLE_UP))
                {
                    ship.SetThrottle(ship.Throttle() + DELTA_THROTTLE);
                    time_til_change = 0.05;
                }

                else if (KeyDown(KeyMap.KEY_THROTTLE_DOWN))
                {
                    ship.SetThrottle(ship.Throttle() - DELTA_THROTTLE);
                    time_til_change = 0.05;
                }

                else if (KeyDown(KeyMap.KEY_THROTTLE_ZERO))
                {
                    ship.SetThrottle(0);
                    if (ship.GetFLCS() != null)
                        ship.GetFLCS().FullStop();
                    time_til_change = 0.05;
                }

                else if (KeyDown(KeyMap.KEY_THROTTLE_FULL))
                {
                    ship.SetThrottle(100);
                    time_til_change = 0.05;
                }

                else if (KeyDown(KeyMap.KEY_FLCS_MODE_AUTO))
                {
                    ship.CycleFLCSMode();
                    time_til_change = 0.5f;
                }

                else if (KeyDown(KeyMap.KEY_CYCLE_PRIMARY))
                {
                    HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_WEP_MODE);
                    ship.CyclePrimary();
                    time_til_change = 0.5f;
                }

                else if (KeyDown(KeyMap.KEY_CYCLE_SECONDARY))
                {
                    HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_WEP_MODE);
                    ship.CycleSecondary();
                    time_til_change = 0.5f;
                }

                if (ship.GetShield() != null)
                {
                    Shield shield = ship.GetShield();
                    double level = shield.GetPowerLevel();

                    if (KeyDown(KeyMap.KEY_SHIELDS_FULL))
                    {
                        HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_SHIELD_LEVEL);
                        shield.SetPowerLevel(100);
                        time_til_change = 0.5f;
                    }

                    else if (KeyDown(KeyMap.KEY_SHIELDS_ZERO))
                    {
                        HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_SHIELD_LEVEL);
                        shield.SetPowerLevel(0);
                        time_til_change = 0.5f;
                    }

                    else if (KeyDown(KeyMap.KEY_SHIELDS_UP))
                    {
                        if (level < 25) level = 25;
                        else if (level < 50) level = 50;
                        else if (level < 75) level = 75;
                        else level = 100;

                        HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_SHIELD_LEVEL);
                        shield.SetPowerLevel(level);
                        time_til_change = 0.5f;
                    }

                    else if (KeyDown(KeyMap.KEY_SHIELDS_DOWN))
                    {
                        if (level > 75) level = 75;
                        else if (level > 50) level = 50;
                        else if (level > 25) level = 25;
                        else level = 0;

                        HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_SHIELD_LEVEL);
                        shield.SetPowerLevel(level);
                        time_til_change = 0.5f;
                    }

                }

                if (ship.GetSensor() != null)
                {
                    Sensor sensor = ship.GetSensor();

                    if (sensor.GetMode() < Sensor.Mode.PST)
                    {
                        if (KeyDown(KeyMap.KEY_SENSOR_MODE))
                        {
                            Sensor.Mode sensor_mode = sensor.GetMode() + 1;
                            if (sensor_mode > Sensor.Mode.GM)
                                sensor_mode = Sensor.Mode.PAS;

                            sensor.SetMode((Sensor.Mode)sensor_mode);
                            time_til_change = 0.5f;
                        }

                        else if (KeyDown(KeyMap.KEY_SENSOR_GROUND_MODE))
                        {
                            if (ship.IsAirborne())
                            {
                                sensor.SetMode(Sensor.Mode.GM);
                                time_til_change = 0.5f;
                            }
                        }
                    }
                    else
                    {
                        // manual "return to search" command for starships:
                        if (KeyDown(KeyMap.KEY_SENSOR_MODE))
                        {
                            ship.DropTarget();
                        }
                    }

                    if (KeyDown(KeyMap.KEY_SENSOR_RANGE_PLUS))
                    {
                        sensor.IncreaseRange();
                        time_til_change = 0.5f;
                    }

                    else if (KeyDown(KeyMap.KEY_SENSOR_RANGE_MINUS))
                    {
                        sensor.DecreaseRange();
                        time_til_change = 0.5f;
                    }
                }

                if (KeyDown(KeyMap.KEY_EMCON_PLUS))
                {
                    ship.SetEMCON(ship.GetEMCON() + 1);
                    time_til_change = 0.5f;
                }

                else if (KeyDown(KeyMap.KEY_EMCON_MINUS))
                {
                    ship.SetEMCON(ship.GetEMCON() - 1);
                    time_til_change = 0.5f;
                }
            }
            else
                time_til_change -= seconds;

            if (controller.ActionMap(KeyMap.KEY_ACTION_0))
                ship.FirePrimary();

            if (controller.ActionMap(KeyMap.KEY_ACTION_1))
            {
                if (!pickle_latch)
                    ship.FireSecondary();

                pickle_latch = true;
            }
            else
            {
                pickle_latch = false;
            }

            if (controller.ActionMap(KeyMap.KEY_ACTION_3))
            {
                if (!target_latch)
                    ship.LockTarget(SimObject.TYPES.SIM_SHIP);

                target_latch = true;
            }
            else
            {
                target_latch = false;
            }

            ship.SetAugmenter(augmenter || (KeyDown(KeyMap.KEY_AUGMENTER) ? true : false));

            if (Toggled(KeyMap.KEY_DECOY))
                ship.FireDecoy();

            if (Toggled(KeyMap.KEY_LAUNCH_PROBE))
                ship.LaunchProbe();

            if (Toggled(KeyMap.KEY_GEAR_TOGGLE))
                ship.ToggleGear();

            if (Toggled(KeyMap.KEY_NAVLIGHT_TOGGLE))
                ship.ToggleNavlights();

            if (Toggled(KeyMap.KEY_LOCK_TARGET))
                ship.LockTarget(SimObject.TYPES.SIM_SHIP, false, true);

            else if (Toggled(KeyMap.KEY_LOCK_THREAT))
                ship.LockTarget(SimObject.TYPES.SIM_DRONE);

            else if (Toggled(KeyMap.KEY_LOCK_CLOSEST_SHIP))
                ship.LockTarget(SimObject.TYPES.SIM_SHIP, true, false);

            else if (Toggled(KeyMap.KEY_LOCK_CLOSEST_THREAT))
                ship.LockTarget(SimObject.TYPES.SIM_DRONE, true, false);

            else if (Toggled(KeyMap.KEY_LOCK_HOSTILE_SHIP))
                ship.LockTarget(SimObject.TYPES.SIM_SHIP, true, true);

            else if (Toggled(KeyMap.KEY_LOCK_HOSTILE_THREAT))
                ship.LockTarget(SimObject.TYPES.SIM_DRONE, true, true);

            else if (Toggled(KeyMap.KEY_CYCLE_SUBTARGET))
                ship.CycleSubTarget(1);

            else if (Toggled(KeyMap.KEY_PREV_SUBTARGET))
                ship.CycleSubTarget(-1);

            if (Toggled(KeyMap.KEY_AUTO_NAV))
            {
                ship.SetAutoNav(true);
                // careful: this object has just been deleted!
                return;
            }

            if (Toggled(KeyMap.KEY_DROP_ORBIT))
            {
                ship.DropOrbit();
                // careful: this object has just been deleted!
                return;
            }
        }
        public override bool Subframe()
        {
            return true;
        }
        public virtual void Launch()
        {
            if (controller != null)
            {
                ship.SetThrottle(100);
                throttle_active = false;
                launch_latch = true;
            }
        }

        public static bool KeyDown(int action)
        {
            bool k = Joystick.KeyDownMap(action) || Keyboard.KeyDownMap(action);

            return k;
        }
        static double last_toggle_time = 0;
        public static bool Toggled(int action)
        {

            if (KeyDown(action))
            {
                if ((Game.RealTime() - last_toggle_time) > 250)
                {
                    last_toggle_time = Game.RealTime();
                    return true;
                }
            }

            return false;
        }

        public override SteerType Type() { return (SteerType)DIR_TYPE; }

        protected Ship ship;
        protected MotionController controller;

        protected bool throttle_active;
        protected bool launch_latch;
        protected bool pickle_latch;
        protected bool target_latch;
    }
}
