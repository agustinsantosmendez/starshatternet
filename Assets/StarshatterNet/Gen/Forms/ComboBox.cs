﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      ComboBox.h/ComboBox.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    ComboBox class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.Int32;

namespace StarshatterNet.Gen.Forms
{
    public class ComboBox : ActiveWindow
    {
        public ComboBox(ActiveWindow p, int ax, int ay, int aw, int ah, DWORD aid = 0) :
            base(p.GetScreen(), ax, ay, aw, ah, aid, 0, p)
        {
            button_state = 0;
            captured = 0;
            pre_state = 0;
            seln = 0;
            list = null;
            list_showing = false;
            border = true;
            animated = true;
            bevel_width = 5;

            desc = "ComboBox " + id;
        }
        // Operations:
        public virtual void Draw() { throw new NotImplementedException(); }
        public virtual void ShowList() { throw new NotImplementedException(); }
        public virtual void HideList() { throw new NotImplementedException(); }
        public override void MoveTo(Stars.Rect r)
        {
            base.MoveTo(r);

            if (list != null)
            {
                //delete list;
                list = null;
            }
        }


        // Event Target Interface:
        public virtual int OnMouseMove(int x, int y) { throw new NotImplementedException(); }
        public virtual int OnLButtonDown(int x, int y) { throw new NotImplementedException(); }
        public virtual int OnLButtonUp(int x, int y) { throw new NotImplementedException(); }
        public override int OnClick() { throw new NotImplementedException(); }
        public override int OnMouseEnter(int x, int y) { throw new NotImplementedException(); }
        public override int OnMouseExit(int x, int y) { throw new NotImplementedException(); }

        public virtual void OnListSelect(AWEvent e) { throw new NotImplementedException(); }
        public virtual void OnListExit(AWEvent e) { throw new NotImplementedException(); }

        // Property accessors:
        public virtual int NumItems()
        {
            return items.Count;
        }
        public virtual void ClearItems()
        {
            items.Destroy();
            seln = 0;
        }
        public virtual void AddItem(string item)
        {
            if (!string.IsNullOrEmpty(item)) items.Add(item);
        }
        public virtual string GetItem(int index)
        {
            if (index >= 0 && index < items.Count)
                return items[index];
            else
                return null;
        }
        public virtual void SetItem(int index, string item)
        {
            if (index >= 0 && index < items.Count)
            {
                items[index] = item;
            }

            else
            {
                if (!string.IsNullOrEmpty(item)) items.Add(item);
            }
        }
        public void SetAllItems()
        {
            if (dropdownCmp == null) return;
            dropdownCmp.ClearOptions();
            dropdownCmp.AddOptions(items);
        }

        public virtual void SetLabel(string label)
        {
            SetText(label);
        }

        public virtual int GetCount()
        {
            return items.Count;
        }
        public virtual string GetSelectedItem()
        {
            if (seln >= 0 && seln < items.Count)
                return items[seln];
            else
                return null;
        }

        public virtual int GetSelectedIndex()
        {
            if (seln >= 0 && seln < items.Count)
                return seln;
            else
                return -1;
        }
        public virtual void SetSelection(int index)
        {
            if (seln != index && index >= 0 && index < items.Count)
            {
                seln = index;
            }
        }

        public Color GetActiveColor() { return active_color; }
        public void SetActiveColor(Color c)
        {
            active_color = c;
            SetActiveColor();
        }
        public void SetActiveColor()
        {
        }

        public bool GetAnimated() { return animated; }
        public void SetAnimated(bool bNewValue)
        {
            animated = bNewValue;
        }
        public int GetBevelWidth() { return bevel_width; }
        public void SetBevelWidth(short nNewValue)
        {
            if (nNewValue < 0) nNewValue = 0;
            bevel_width = nNewValue;
        }

        public bool GetBorder() { return border; }
        public void SetBorder(bool bNewValue)
        {
            border = bNewValue;
        }
        public Color GetBorderColor() { return border_color; }
        public void SetBorderColor(Color c)
        {
            border_color = c;
        }
        public int GetButtonState() { return button_state; }
        public void SetButtonState(short nNewValue) { throw new NotImplementedException(); }

        public bool IsListShowing() { return list_showing; }

        public override void SetFont(FontItem f)
        {
            base.SetFont(f);
            SetFont();
        }
        public override void SetFontSize(int s)
        {
            base.SetFontSize(s);
            SetFont();
        }
        protected void SetFont()
        {
            if (dropdownText == null) return;
            dropdownText.font = this.GetFont().font;
            dropdownText.fontSize = this.GetFontSize();
        }
        public override void SetForeColor(Color c)
        {
            base.SetForeColor(c);
            SetForeColor();
        }
        protected void SetForeColor()
        {
        }
        public override void SetBaseColor(Color c)
        {
            base.SetBaseColor(c);
            SetBaseColor();
        }
        protected void SetBaseColor()
        {
            if (dropdownText != null)
                dropdownText.color = base_color;
        }
        public override void SetBackColor(Color c)
        {
            base.SetBackColor(c);
            SetBackColor();
        }
        protected void SetBackColor()
        {
        }
        protected Stars.Rect CalcLabelRect()
        {
            // fit the text in the bevel:
            Stars.Rect label_rect = new Stars.Rect();
            label_rect.x = 0;
            label_rect.y = 0;
            label_rect.w = rect.w;
            label_rect.h = rect.h;

            label_rect.Deflate(bevel_width, bevel_width);

            return label_rect;
        }
        protected void DrawRectSimple(Rect rect, int stat) { throw new NotImplementedException(); }
        private void OnValueChangedEvent(int selectedVal)
        {
            Debug.Log("OnValueChangedEvent:" + selectedVal);
            Button.PlaySound(Button.SOUNDS.SND_COMBO_SELECT);
        }

        protected internal override void BuildGamebjects()
        {
            base.BuildGamebjects();

            DefaultControls.Resources uiResources = new DefaultControls.Resources();
            obj = DefaultControls.CreateDropdown(uiResources);
            dropdownCmp = obj.GetComponent<UnityEngine.UI.Dropdown>();
            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            dropdownCmp.onValueChanged.AddListener(OnValueChangedEvent);

            rectTransform.SetParent(this.parentObj.transform);
            obj.name = desc;

            var vpimg = obj.GetComponent<Image>();
            Color c = vpimg.color;
            c.a = 0;
            vpimg.color = c;

            GameObject uiObject1 = obj.transform.Find("Arrow").gameObject;
            Image img1 = uiObject1.GetComponent<Image>();
            img1.sprite = AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/DropdownArrow.psd");

            GameObject uiObject2 = obj.transform.Find("Label").gameObject;
            RectTransform component1 = uiObject2.GetComponent<RectTransform>();
            component1.anchorMin = Vector2.zero;
            component1.anchorMax = Vector2.one;
            component1.sizeDelta = Vector2.zero;
            component1.offsetMin = new Vector2(0f, 0f);
            component1.offsetMax = new Vector2(0f, 0f);

            dropdownText = uiObject2.GetComponent<Text>();
            //dropdownText.fontSize = 12;
            dropdownText.color = Color.white;

            SetFont();
            SetForeColor();
            SetBackColor();
            //SetBaseColor();
            SetActiveColor();
            SetAllItems();
        }
        protected List<string> items = new List<string>();
        protected ComboList list;
        protected bool list_showing;
        protected bool animated;
        protected bool border;
        protected int seln;
        protected int captured;
        protected int bevel_width;
        protected int button_state;
        protected int pre_state;

        protected Color active_color;
        protected Color border_color;

        protected Dropdown dropdownCmp;
        protected Text dropdownText;
    }
}
