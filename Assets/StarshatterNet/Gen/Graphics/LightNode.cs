﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    FILE:         Light.h/Light.cpp
    ORIGINAL AUTHOR:       John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Dynamic Light Source
*/
using System;
using StarshatterNet.Gen.Graphics;
using UnityEngine;
using Point = UnityEngine.Vector3;

namespace StarshatterNet.Gen.Graphics
{
    public class LightNode
    {
        public enum TYPES : UInt32
        {
            LIGHT_POINT = 1,
            LIGHT_SPOT = 2,
            LIGHT_DIRECTIONAL = 3,
            LIGHT_FORCE_DWORD = 0x7fffffff
        };

        public LightNode(float l = 0.0f, float dl = 1.0f, int time = -1)
        {
            id = id_key++;
            gameObj = new GameObject(string.Format("Light{0:X8}", this.Identity()));
            lightComponent = gameObj.AddComponent<Light>();

            SetType(TYPES.LIGHT_POINT);
            life = time;
            SetIntensity(l);
            dldt = dl;
            SetColor(new Color(255, 255, 255));
            active = true;
            shadow = false;
            scene = null;
        }
        //public virtual ~Light();


        // operations
        public virtual void Update()
        {
            if (dldt < 1.0f)
                light *= dldt;

            if (life > 0) life--;
        }

        // accessors / mutators
        public uint Identity() { return id; }
        public Point Location() { return loc; }

        public TYPES Type() { return type; }
        public void SetType(TYPES t)
        {
            type = t;
            switch (t)
            {
                case TYPES.LIGHT_DIRECTIONAL: lightComponent.type = LightType.Directional; break;
                case TYPES.LIGHT_POINT: lightComponent.type = LightType.Point; break;
                case TYPES.LIGHT_SPOT: lightComponent.type = LightType.Spot; break;
            }
        }

        public float Intensity() { return light; }
        public void SetIntensity(float f)
        {
            light = f;
            if (lightComponent != null)
                lightComponent.intensity = light;
        }
        public Color GetColor() { return color; }
        public void SetColor(Color c)
        {
            color = c;
            if (lightComponent != null)
                lightComponent.color = color;
        }
        public bool IsActive() { return active; }
        public void SetActive(bool a)
        {
            active = a;
            if (gameObj != null)
                gameObj.SetActive(active);
        }
        public bool CastsShadow() { return shadow; }
        public void SetShadow(bool s)
        {
            shadow = s;
            if (lightComponent != null)
                if (shadow)
                    lightComponent.shadows = LightShadows.Soft;
                else
                    lightComponent.shadows = LightShadows.None;
        }

        public bool IsPoint() { return type == TYPES.LIGHT_POINT; }
        public bool IsSpot() { return type == TYPES.LIGHT_SPOT; }
        public bool IsDirectional() { return type == TYPES.LIGHT_DIRECTIONAL; }

        public virtual void MoveTo(Point dst)
        {
            //if (type != LIGHT_DIRECTIONAL)
            loc = dst;
            UpdateGameObject();
        }
        public virtual void MoveTo(DigitalRune.Mathematics.Algebra.Vector3D dst)
        {
            //if (type != LIGHT_DIRECTIONAL)
            loc.x = (float)dst.X;
            loc.y = (float)dst.Y;
            loc.z = (float)dst.Z;
            UpdateGameObject();
        }
        public virtual void MoveTo(DigitalRune.Mathematics.Algebra.Vector3F dst)
        {
            //if (type != LIGHT_DIRECTIONAL)
            loc.x = dst.X;
            loc.y = dst.Y;
            loc.z = dst.Z;
            UpdateGameObject();
        }

        public virtual void TranslateBy(Point ref_)
        {
            if (type != TYPES.LIGHT_DIRECTIONAL)
            {
                loc = loc - ref_;
                UpdateGameObject();
            }
        }

        public virtual int Life() { return life; }
        public virtual void Destroy()
        {
            //if (scene != null)
            //    scene.DelLight(this);

            //delete this;
        }


        public virtual Scene GetScene() { return scene; }
        public virtual void SetScene(Scene s)
        {
            if (scene != null)
                scene.DelLight(this);
            scene = s;
        }
        protected void UpdateGameObject(bool updateOrientation = false)
        {
            //update the position
            gameObj.transform.position = loc;
            if (updateOrientation)
            {
                // todo
            }
        }

        //  int operator == (const Light& l) const { return id == l.id; }
        public override bool Equals(object obj)
        {
            var node = obj as LightNode;
            return node != null &&
                   id == node.id;
        }

        public override int GetHashCode()
        {
            return 1877310944 + id.GetHashCode();
        }

        protected static uint id_key = 0;

        protected uint id;
        protected TYPES type;
        protected Point loc;
        protected int life;
        protected float light;
        protected float dldt;
        protected Color color;
        protected bool active;
        protected bool shadow;
        protected Scene scene;

        internal protected GameObject gameObj;
        protected Light lightComponent;
    }
}
