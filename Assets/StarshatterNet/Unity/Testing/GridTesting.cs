﻿using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Graphics.General;
using UnityEngine;

public class GridTesting : MonoBehaviour
{
    public Scene scene;

    private Camera cam;

    // Use this for initialization
    void Start()
    {
        // This code is similar to TacRefDlg.cpp, lines 133-160
        scene.SetAmbient(new Color(60f / 255f, 60f / 255f, 60f / 255f));

        CameraNode camera = new CameraNode();
        scene.AddCamera(camera.gameObj);
        cam = camera.gameObj.GetComponent<Camera>();

        GridNode grid = new GridNode();
        grid.MoveTo(0, 0, 15);
        grid.Width = 0.03f;
        scene.AddGraphic(grid);
    }

    // Update is called once per frame
    void Update()
    {
        //cubex += Time.deltaTime * 10;
        //cube.SetObjOrientation(Quaternion.Euler(cubex, 0, 0));

        //if (Input.GetMouseButtonDown(0))
        //{
        //    visible = !visible;

        //    if (visible)
        //    {
        //        Cursor.lockState = CursorLockMode.None;
        //        Cursor.visible = true;
        //    }
        //    else
        //    {
        //        Cursor.lockState = CursorLockMode.Locked;
        //        Cursor.visible = false;
        //    }
        //}
        //if (Input.GetMouseButtonDown(1))
        //{
        //    useModSpeed = !useModSpeed;
        //}
        //if (cam != null)
        //{
        //    float targetFov = startingFov;
        //    if (Input.GetMouseButton(2))
        //        targetFov = zoomFov;

        //    cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, targetFov, 10.0f * Time.deltaTime);
        //}
        //MouseLookMovement();
        //OnCamMove();
        //OnCamZoom();
        //UpdateCamera();

    }
}
