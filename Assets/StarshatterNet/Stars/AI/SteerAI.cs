﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      SteerAI.h/SteerAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Steering (low-level) Artificial Intelligence class
*/
using System;
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.AI
{
    public class SteerAI : SimObserver, IDirector
    {
        // IDirector
        public virtual void ExecFrame(double factor)
        {
        }

        public virtual bool Subframe()
        {
            return false;
        }


        public enum SteerType { SEEKER = 1000, FIGHTER, STARSHIP, GROUND };

        public SteerAI(SimObject ship)
        {
            self = ship;
            target = null; subtarget = null; other = null; distance = 0.0; evade_time = 0;
            objective = new Point(0.0f, 0.0f, 0.0f); seek_gain = 20;
            seek_damp = 0.5;

            for (int i = 0; i < 3; i++)
                az[i] = el[i] = 0;
        }

        //~SteerAI() { }

        public static IDirector Create(SimObject self, SteerType type)
        {
            switch (type)
            {
                case SteerType.SEEKER:
                    return new SeekerAI(self);

                case SteerType.STARSHIP:
                    return new StarshipAI(self);

                case SteerType.GROUND:
                    return new GroundAI(self);

                default:
                case SteerType.FIGHTER:
                    return new FighterAI(self);
            }
        }


        public virtual void SetTarget(SimObject targ, ShipSystem sub = null)
        {
            if (target != targ)
            {
                target = targ;

                if (target != null)
                    Observe(target);
            }

            subtarget = sub;
        }


        public virtual SimObject GetTarget() { return target; }
        public virtual ShipSystem GetSubTarget() { return subtarget; }
        public virtual void DropTarget(double drop_time = 1.5)
        {
            SetTarget(null);
        }

        public virtual SteerAI.SteerType Type() { return  ai_type; }

        public override bool Update(SimObject obj)
        {
            if (obj == target)
            {
                target = null;
                subtarget = null;
            }

            if (obj == other)
            {
                other = null;
            }

            return base.Update(obj);
        }

        public override string GetObserverName()
        {
            string name = string.Format("SteerAI({0})", self.Name());
            return name;
        }

        // debug:
        public virtual Point GetObjective() { return obj_w; }
        public virtual SimObject GetOther() { return other; }



        // accumulate behaviors:
        protected virtual void Navigator()
        {
            accumulator.Clear();
            magnitude = 0;
        }


        protected virtual int Accumulate(Steer steer)
        {
            int overflow = 0;

            double mag = steer.Magnitude();

            if (magnitude + mag > 1)
            {
                overflow = 1;
                double scale = (1 - magnitude) / mag;

                accumulator += steer * scale;
                magnitude = 1;

                if (seeking )
                {
                    az[0] *= scale;
                    el[0] *= scale;
                    seeking = false;
                }
            }
            else
            {
                accumulator += steer;
                magnitude += mag;
            }

            return overflow;
        }

        // steering functions:
        protected virtual Steer Seek(Point point)
        {
            Steer s = new Steer();

            // advance memory pipeline:
            az[2] = az[1]; az[1] = az[0];
            el[2] = el[1]; el[1] = el[0];

            // approach
            if (point.Z > 0.0f)
            {
                az[0] = Math.Atan2(Math.Abs(point.X), point.Z) * seek_gain;
                el[0] = Math.Atan2(Math.Abs(point.Y), point.Z) * seek_gain;

                if (point.X < 0) az[0] = -az[0];
                if (point.Y > 0) el[0] = -el[0];

                s.yaw = az[0] - seek_damp * (az[1] + az[2] * 0.5);
                s.pitch = el[0] - seek_damp * (el[1] + el[2] * 0.5);
            }

            // reverse
            else
            {
                if (point.X > 0) s.yaw = 1.0f;
                else s.yaw = -1.0f;

                s.pitch = -point.Y * 0.5f;
            }

            seeking = true;

            return s;
        }
        protected virtual Steer Flee(Point pt)
        {
            Steer s = new Steer();

            Point point = pt;
            point.Normalize();

            // approach
            if (point.Z > 0.0f)
            {
                if (point.X > 0) s.yaw = -1.0f;
                else s.yaw = 1.0f;
            }

            // flee
            else
            {
                s.yaw = -point.X;
                s.pitch = point.Y;
            }

            return s;
        }

        protected virtual Steer Avoid(Point point, float radius)
        {
            Steer s = new Steer();

            if (point.Z > 0)
            {
                double ax = radius - Math.Abs(point.X);
                double ay = radius - Math.Abs(point.Y);

                // go around?
                if (ax < ay)
                {
                    s.yaw = Math.Atan2(ax, point.Z) * seek_gain;
                    if (point.X > 0) s.yaw = -s.yaw;
                }

                // go over/under:
                else
                {
                    s.pitch = Math.Atan2(ay, point.Z) * seek_gain;
                    if (point.Y < 0) s.pitch = -s.pitch;
                }
            }

            return s;
        }
        protected virtual Steer Evade(Point point, Point vel)
        {
            Steer evade = new Steer();

            if (Game.GameTime() - evade_time > 1250)
            {
                evade_time = (uint)Game.GameTime();

                int direction = (RandomHelper.Rand() >> 9) & 0x07;

                switch (direction)
                {
                    default:
                    case 0: evade.yaw = 0; evade.pitch = -0.5; break;
                    case 1: evade.yaw = 0; evade.pitch = -1.0; break;
                    case 2: evade.yaw = 1; evade.pitch = -0.3; break;
                    case 3: evade.yaw = 1; evade.pitch = -0.6; break;
                    case 4: evade.yaw = 1; evade.pitch = -1.0; break;
                    case 5: evade.yaw = -1; evade.pitch = -0.3; break;
                    case 6: evade.yaw = -1; evade.pitch = -0.6; break;
                    case 7: evade.yaw = -1; evade.pitch = -1.0; break;
                }
            }

            return evade;
        }

        // compute the goal point based on target stats:
        public virtual void FindObjective()
        {
            if ( self ==null ||  target == null) return;

            Point cv = ClosingVelocity();
            double cvl = cv.Length ;
            double time = 0;

            if (cvl > 5)
            {
                // distance from self to target:
                distance =  (target.Location() - self.Location()).Length;

                // time to reach target:
                time = distance / cvl;

                // where the target will be when we reach it:
                Point run_vec = target.Velocity();
                obj_w = target.Location() + (run_vec * (float)time);
            }

            else
            {
                obj_w = target.Location();
            }

            // subsystem offset:
            if (subtarget != null)
            {
                Point offset = target.Location() - subtarget.MountLocation();
                obj_w -= offset;
            }

            distance = (obj_w - self.Location()).Length;

            if (cvl > 5)
                time = distance / cvl;

            // where we will be when the target gets there:
            Point self_dest = self.Location() + cv * (float)time;
            Point err = obj_w - self_dest;

            obj_w += err;

            // transform into camera coords:
            objective = Transform(obj_w);
            objective.Normalize();

            distance =  (obj_w - self.Location()).Length ;
        }
        protected virtual Point ClosingVelocity()
        {
            if (self != null)
            {
                if (target != null)
                    return self.Velocity() - target.Velocity();
                else
                    return self.Velocity();
            }

            return new Point(1, 0, 0);
        }


        readonly double MAX_ANGLE = MathHelper.ToRadians(15);
        readonly double MIN_ANGLE = MathHelper.ToRadians(3);
        protected virtual Point Transform(Point pt)
        {
            Point obj_t = pt - self.Location();
            Point result;

            if (self.FlightPathYawAngle() != 0 || self.FlightPathPitchAngle() != 0)
            {
                double az = self.FlightPathYawAngle();
                double el = self.FlightPathPitchAngle();


                if (az > MAX_ANGLE)
                    az = MAX_ANGLE;
                else if (az < -MAX_ANGLE)
                    az = -MAX_ANGLE;
                else if (az > MIN_ANGLE)
                    az = MIN_ANGLE + (az - MIN_ANGLE) / 2;
                else if (az < -MIN_ANGLE)
                    az = -MIN_ANGLE + (az + MIN_ANGLE) / 2;

                if (el > MAX_ANGLE)
                    el = MAX_ANGLE;
                else if (el < -MAX_ANGLE)
                    el = -MAX_ANGLE;
                else if (el > MIN_ANGLE)
                    el = MIN_ANGLE + (el - MIN_ANGLE) / 2;
                else if (el < -MIN_ANGLE)
                    el = -MIN_ANGLE + (el + MIN_ANGLE) / 2;

                CameraNode cam = self.Cam().Clone();
                cam.Yaw(az);
                cam.Pitch(-el);

                result = new Point(Vector3D.Dot(obj_t , cam.vrt()),
                                    Vector3D.Dot(obj_t, cam.vup()),
                                    Vector3D.Dot(obj_t, cam.vpn()));
            }
            else
            {
                CameraNode cam = self.Cam();   // cast away const

                result = new Point(Vector3D.Dot(obj_t, cam.vrt()),
                                   Vector3D.Dot(obj_t, cam.vup()),
                                   Vector3D.Dot(obj_t, cam.vpn()));
            }

            return result;
        }

        protected virtual Point AimTransform(Point pt)
        {
            CameraNode cam = self.Cam();   // cast away const
            Point obj_t = pt - self.Location();

            Point result = new Point(Vector3D.Dot(obj_t, cam.vrt()),
                                     Vector3D.Dot(obj_t, cam.vup()),
                                     Vector3D.Dot(obj_t, cam.vpn()));

            return result;
        }


        protected bool seeking;

        protected SimObject self;
        protected SimObject target;
        protected ShipSystem subtarget;
        protected SimObject other;

        protected Point obj_w;
        protected Point objective;

        protected double distance;
        protected double[] az = new double[3], el = new double[3];

        protected Steer accumulator;
        protected double magnitude;
        protected uint evade_time;

        protected double seek_gain;
        protected double seek_damp;

        protected SteerType ai_type;
    }
}
