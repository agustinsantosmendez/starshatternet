﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Scene.h/Scene.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    A 3D Scene
*/
using System;
using System.Collections.Generic;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using UnityEngine.Rendering;

namespace StarshatterNet.Gen.Graphics
{
    public class Scene
    {
        public Scene()
        {
            root = new GameObject("Scene Root");
            camObj = new GameObject("Camera");
            camera = camObj.AddComponent<Camera>();
            camera.orthographic = false;
            camera.backgroundColor = Color.black;
            camera.clearFlags = CameraClearFlags.Color;

            //camera.transform.parent = root.transform;
            this.AddCamera(camObj);
            cameraNode = new CameraNode(camObj);
        }

        public void AddBackground(Graphic g)
        {
            if (g != null)
            {
                if (!background.Contains(g))
                    background.Add(g);

                g.SetScene(this);
            }
        }
        public void DelBackground(Graphic g)
        {
            if (g != null)
            {
                background.Remove(g);
                g.SetScene(null);
            }
        }
        public void AddForeground(Graphic g)
        {
            if (g != null)
            {
                if (!foreground.Contains(g))
                    foreground.Add(g);

                g.SetScene(this);
            }
        }
        public void DelForeground(Graphic g)
        {
            if (g != null)
            {
                foreground.Remove(g);
                g.SetScene(null);
            }
        }
        public void AddGraphic(Graphic g)
        {
            if (g == null || g.gameObj == null || g.gameObj.transform == null)
                return;
            if (g != null)
            {
                if (!graphics.Contains(g))
                    graphics.Add(g);

                g.SetScene(this);
            }

            g.gameObj.transform.SetParent(root.transform);
        }

        public void DelGraphic(Graphic g)
        {
            if (g != null)
            {
                graphics.Remove(g);   // it's gotta be in here somewhere!
                foreground.Remove(g); // use the logical-or operator to early
                sprites.Remove(g);    // out when we find it...
                background.Remove(g);

                g.SetScene(null);
            }
        }
        public void AddSprite(Graphic g)
        {
            if (g != null)
            {
                if (!sprites.Contains(g))
                    sprites.Add(g);

                g.SetScene(this);
            }
        }
        public void DelSprite(Graphic g)
        {
            if (g != null)
            {
                sprites.Remove(g);
                g.SetScene(null);
            }
        }

        public void AddLight(LightNode l)
        {
            if (l != null)
            {
                if (!lights.Contains(l))
                    lights.Add(l);
                // l.SetScene(this);
                l.gameObj.transform.SetParent(root.transform);
            }
        }
        public void DelLight(LightNode l)
        {
            if (l != null && lights.Contains(l))
            {
                lights.Remove(l);
                //l.SetScene(null);
            }
        }
        public void AddCamera(GameObject c)
        {
            if (c != null)
            {
                camObj = c;
                c.transform.parent = root.transform;
                camera = c.GetComponent<Camera>();
            }
        }
        public void DelCamera(GameObject c)
        {
            if (c != null)
            {
                camObj = null;
                camera = null;
            }
        }

        public Camera Camera { get => camera; }
        public CameraNode CameraNode { get => cameraNode; }
        public GameObject Root { get => root; }

        public List<Graphic> Background() { return background; }
        public List<Graphic> Foreground() { return foreground; }
        public List<Graphic> Graphics() { return graphics; }
        public List<Graphic> Sprites() { return sprites; }
        public List<LightNode> Lights() { return lights; }
        public Color Ambient() { return ambient; }
        public void SetAmbient(Color color)
        {
            ambient = color;
            RenderSettings.ambientMode = AmbientMode.Flat;
            RenderSettings.ambientIntensity = 2f;
            float intensity = 1.5f;
            Color ambientColor = new Color(intensity, intensity, intensity) + color;
            RenderSettings.ambientLight = ambientColor;
            camera.backgroundColor = color;
        }

        public virtual void Collect()
        {
            for (int i = graphics.Count - 1; i >= 0; i--)
            {
                Graphic g = graphics[i];
                if (g.Life() == 0)
                {
                    graphics.RemoveAt(i);
                    g.Destroy();
                }
            }

            for (int i = sprites.Count - 1; i >= 0; i--)
            {
                Graphic g = sprites[i];
                if (g.Life() == 0)
                {
                    sprites.RemoveAt(i);
                    g.Destroy();
                }
            }

            for (int i = lights.Count - 1; i >= 0; i--)
            {
                LightNode l = lights[i];
                if (l.Life() == 0)
                {
                    lights.RemoveAt(i);
                    l.Destroy();
                }
            }
        }

        public virtual bool IsLightObscured(Point obj_pos, Point light_pos, double obj_radius, Point imp_point)
        {
            throw new NotImplementedException();
        }
        protected List<Graphic> background = new List<Graphic>();
        protected List<Graphic> foreground = new List<Graphic>();
        protected List<Graphic> graphics = new List<Graphic>();
        protected List<Graphic> sprites = new List<Graphic>();
        protected List<LightNode> lights = new List<LightNode>();
        protected Color ambient;

        private GameObject root;
        private GameObject camObj;
        private Camera camera;
        private CameraNode cameraNode;
        private Transform sceneTrasnform;
    }
}
