﻿using System;
using System.Collections.Generic;
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Audio;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using UnityEngine;

namespace StarshatterNet.Stars.Serialization
{
    public class WeaponDesignSerializer
    {
        public static void JSONReadWeaponDesign(string path, string filename, bool mod, List<SimElements.WeaponDesign> catalog, List<SimElements.WeaponDesign> mod_catalog)
        {
            var jsoncatalog = ReaderSupport.DeserializeAsset<WeaponsCollection>(path + filename);
            if (jsoncatalog != null && jsoncatalog.Weapons.Count > 0)
                foreach (var entry in jsoncatalog.Weapons)
                {
                    SimElements.WeaponDesign design = new SimElements.WeaponDesign();

                    switch (entry.type)
                    {
                        case "primary":
                            design.type = SimElements.WeaponDesign.WeaponType.primary;
                            design.primary = true;
                            break;
                        case "beam":
                            design.type = SimElements.WeaponDesign.WeaponType.beam;
                            design.primary = true;
                            design.beam = true;
                            design.guided = 1;

                            design.spread_az = 0.15f;
                            design.spread_el = 0.15f;

                            break;
                        case "drone":
                            design.type = SimElements.WeaponDesign.WeaponType.drone;
                            design.primary = false;
                            design.drone = true;
                            design.penetration = 5.0f;
                            break;
                        case "missile":
                            design.type = SimElements.WeaponDesign.WeaponType.missile;
                            design.primary = false;
                            design.penetration = 5.0f;
                            break;
                        default:
                            ErrLogger.PrintLine("ERROR: Unkown weapon type '{0}'", entry.name);
                            break;
                    }

                    design.name = entry.name;
                    design.group = entry.group;
                    design.description = entry.description;
                    design.guided = entry.guided;
                    design.self_aiming = entry.self_aiming;
                    design.flak = entry.flak;
                    design.syncro = entry.syncro;
                    design.visible_stores = entry.visible_stores;
                    design.value = entry.value;
                    design.secret = entry.secret;

                    //if (degrees) design.aim_az_max *= (float)DEGREES;
                    design.aim_az_min = 0.0f - entry.aim_az_max;
                    design.aim_el_min = 0.0f - entry.aim_el_max;
                    design.aim_az_min = entry.aim_az_min;
                    design.aim_el_min = entry.aim_el_min;
                    design.aim_az_rest = entry.aim_az_rest;
                    design.aim_el_rest = entry.aim_el_rest;
                    design.spread_az = entry.spread_az;
                    design.spread_el = entry.spread_el;

                    design.capacity = entry.capacity;
                    design.recharge_rate = entry.recharge_rate;
                    design.refire_delay = entry.refire_delay;
                    design.salvo_delay = entry.salvo_delay;
                    design.ammo = entry.ammo;
                    design.ripple_count = entry.ripple_count;
                    design.charge = entry.charge;
                    design.min_charge = entry.min_charge;
                    design.carry_mass = entry.carry_mass;
                    design.carry_resist = entry.carry_resist;
                    design.damage = entry.damage;
                    design.penetration = entry.penetration;
                    design.speed = entry.speed;
                    design.life = entry.life;
                    design.mass = entry.mass;
                    design.drag = entry.drag;
                    design.thrust = entry.thrust;
                    design.roll_rate = entry.roll_rate;
                    design.pitch_rate = entry.pitch_rate;
                    design.yaw_rate = entry.yaw_rate;
                    design.roll_drag = entry.roll_drag;
                    design.pitch_drag = entry.pitch_drag;
                    design.yaw_drag = entry.yaw_drag;
                    design.lethal_radius = entry.lethal_radius;
                    design.integrity = entry.integrity;

                    design.det_range = entry.det_range;
                    design.det_count = entry.det_count;
                    design.det_spread = entry.det_spread;
                    design.det_child = entry.det_child;

                    design.slew_rate = entry.slew_rate;

                    design.min_range = entry.min_range;
                    design.max_range = entry.max_range;
                    design.max_track = entry.max_track;

                    switch (entry.graphic_type)
                    {
                        case "solid":
                            design.graphic_type = Graphic.TYPE.SOLID;
                            break;
                        case "bolt":
                            design.graphic_type = Graphic.TYPE.BOLT;
                            break;
                        case "sprite":
                            design.graphic_type = Graphic.TYPE.SPRITE;
                            break;
                        case "quad":
                            design.graphic_type = Graphic.TYPE.QUAD;
                            break;
                        default:
                            design.graphic_type = Graphic.TYPE.OTHER;
                            break;

                    }
                    design.width = entry.width;
                    design.length = entry.length;
                    design.scale = entry.scale;
                    design.explosion_scale = entry.explosion_scale;
                    design.light = entry.light;
                    design.flash_scale = entry.flash_scale;
                    design.flare_scale = entry.flare_scale;

                    design.trail_length = entry.trail_length;
                    design.trail_width = entry.trail_width;
                    design.trail_dim = entry.trail_dim;

                    design.beauty = entry.beauty;
                    design.bitmap = entry.bitmap;
                    design.turret = entry.turret;
                    design.turret_base = entry.turret_base;
                    design.model = entry.model;
                    design.trail = entry.trail;
                    design.flash = entry.flash;
                    design.flare = entry.flare;
                    design.sound = entry.sound;
                    design.probe = entry.probe;
                    design.turret_axis = entry.turret_axis;
                    if (string.IsNullOrEmpty(entry.target_type))
                        design.target_type = default(SimElements.CLASSIFICATION);
                    else
                        if (entry.target_type.ToLower().StartsWith("0x"))
                    {
                        entry.target_type = entry.target_type.Substring(2);
                        design.target_type = (SimElements.CLASSIFICATION)UInt32.Parse(entry.target_type, System.Globalization.NumberStyles.HexNumber);
                    }
                    else
                        design.target_type = (SimElements.CLASSIFICATION)UInt32.Parse(entry.target_type);

                    //TODO "animation"

                    design.light_color = entry.light_color;

                    //TODO sound_min_dist, sound_max_dist
                    float sound_min_dist = 1.0f;
                    float sound_max_dist = 100.0e3f;
                    if (entry.sound_min_dist >= 0)
                        sound_min_dist = entry.sound_min_dist;
                    if (entry.sound_max_dist >= 0)
                        sound_max_dist = entry.sound_max_dist;
                    design.muzzle_pts = entry.muzzle_pts;
                    design.attachments = entry.attachments;
                    design.eject = entry.eject;
                    if (!string.IsNullOrEmpty(entry.decoy_type))
                        for (int i = 0; i < SimElements.ShipDesign.ship_design_class_name.Length; i++)
                        {
                            if (entry.decoy_type.ToLower() == SimElements.ShipDesign.ship_design_class_name[i].ToLower())
                            {
                                design.decoy_type = i;
                                break;
                            }
                        }
                    else
                        design.decoy_type = 0;
                    if (!string.IsNullOrEmpty(entry.damage_type))
                        switch (entry.damage_type)
                        {
                            case "normal":
                                design.damage_type = SimElements.WeaponDesign.DAMAGE.DMG_POWER;
                                break;
                            case "emp":
                                design.damage_type = SimElements.WeaponDesign.DAMAGE.DMG_EMP;
                                break;
                            case "power":
                                design.damage_type = SimElements.WeaponDesign.DAMAGE.DMG_POWER;
                                break;
                            default:
                                ErrLogger.PrintLine("WARNING: unknown weapon damage type '{0}' in {1}", entry.damage_type, entry.name);
                                break;
                        }
                    if (!string.IsNullOrEmpty(entry.description))
                        design.description = Game.GetText(entry.description);

                    if (design.anim_length > 0)
                    {
                        design.animation = new Bitmap[design.anim_length];
                        for (int i = 0; i < design.anim_length; i++)
                        {
                            //Bitmap  p = design.animation[i];
                            //loader.LoadBitmap(design.anim_frames[i], *p, Bitmap::BMP_TRANSLUCENT);
                            //p.MakeTexture();
                            Bitmap p = new Bitmap(design.anim_frames[i], Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
                            design.animation[i] = p;

                        }
                    }

                    else if (!string.IsNullOrEmpty(design.bitmap))
                    {
                        //loader.LoadTexture(design.bitmap, design.shot_img, Bitmap::BMP_TRANSLUCENT);
                        design.shot_img = new Bitmap(design.bitmap, Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
                    }

                    if (!string.IsNullOrEmpty(design.beauty))
                    {
                        //loader.LoadTexture(design.beauty, design.beauty_img, Bitmap::BMP_TRANSLUCENT);
                        design.beauty_img = new Bitmap(design.beauty, Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
                    }

                    if (!string.IsNullOrEmpty(design.turret))
                    {
                        design.turret_model = new Model();
                        design.turret_model.Load(design.turret, design.scale);

                        if (!string.IsNullOrEmpty(design.turret_base))
                        {
                            design.turret_base_model = new Model();
                            design.turret_base_model.Load(design.turret_base, design.scale);
                        }
                    }
                    if (!string.IsNullOrEmpty(design.model))
                    {
                        design.shot_model = new Model();
                        design.shot_model.Load(design.model, design.scale);
                    }

                    if (!string.IsNullOrEmpty(design.trail))
                    {
                        //loader.LoadTexture(design.trail, design.trail_img, Bitmap::BMP_TRANSLUCENT);
                        design.trail_img = new Bitmap(design.trail, Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
                    }

                    if (!string.IsNullOrEmpty(design.flash))
                    {
                        //loader.LoadTexture(design.flash, design.flash_img, Bitmap::BMP_TRANSLUCENT);
                        design.flash_img = new Bitmap(design.flash, Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
                    }

                    if (!string.IsNullOrEmpty(design.flare))
                    {
                        //loader.LoadTexture(design.flare, design.flare_img, Bitmap::BMP_TRANSLUCENT);
                        design.flare_img = new Bitmap(design.flare, Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
                    }

                    if (!string.IsNullOrEmpty(design.sound))
                    {
                        Sound.FlagEnum SOUND_FLAGS = Sound.FlagEnum.LOCALIZED | Sound.FlagEnum.LOC_3D;


                        if (design.beam)
                            SOUND_FLAGS = Sound.FlagEnum.LOCALIZED | Sound.FlagEnum.LOC_3D | Sound.FlagEnum.LOCKED;

                        //if (strstr(path, "Mods") == 0)
                        //    loader.SetDataPath("Sounds/");
                        var soundpath = "Sounds/" + entry.sound;
                        design.sound_resource = Sound.CreateStream(soundpath, SOUND_FLAGS);

                        if (design.sound_resource != null)
                        {
                            design.sound_resource.SetMinDistance(sound_min_dist);
                            design.sound_resource.SetMaxDistance(sound_max_dist);
                        }
                    }

                    if (design.max_range == 0.0f)
                        design.max_range = design.speed * design.life;

                    if (design.max_track == 0.0f)
                        design.max_track = 3.0f * design.max_range;

                    if (design.probe && design.lethal_radius < 1e3)
                        design.lethal_radius = 50e3f;

                    if (design.beam)
                        design.flak = false;

                    if (design.self_aiming)
                    {
                        if (Math.Abs(design.aim_az_max) > design.firing_cone)
                            design.firing_cone = Math.Abs(design.aim_az_max);

                        if (Math.Abs(design.aim_az_min) > design.firing_cone)
                            design.firing_cone = Math.Abs(design.aim_az_min);

                        if (Math.Abs(design.aim_el_max) > design.firing_cone)
                            design.firing_cone = Math.Abs(design.aim_el_max);

                        if (Math.Abs(design.aim_el_min) > design.firing_cone)
                            design.firing_cone = Math.Abs(design.aim_el_min);
                    }

                    if (mod)
                        mod_catalog.Add(design);
                    else
                        catalog.Add(design);
                }
            // TODO ErrLogger.PrintLine("WARNING: WeaponDesign not yet implemented '{0}'", path + filename);
        }
    }
    [Serializable]
    public class WeaponsCollection
    {
        public List<WeaponDesignEntry> Weapons;
    }
    public enum WeaponType
    {
        primary = 0,
        missile,
        beam,
        drone
    }

    [Serializable]
    public class WeaponDesignEntry
    {
        public const int MAX_STORES = 8;

        public string type;             // unique id
        public string name;
        public string group;
        public string description;      // background info for tactical reference
        public bool secret;           // don't display in the tactical reference

        public bool drone;            // visible to sensors?
        public bool primary;          // laser or missile?
        public bool beam;             // if laser, beam or bolt?
        public bool self_aiming;      // turret or fixed?
        public bool syncro;           // fire all barrels?
        public bool flak;             // splash damage
        public int guided;           // straight, pure pursuit, lead pursuit
        public int value;            // AI importance of system
        public string decoy_type;       // Ship Classifcation of decoy signature
        public string decoy;
        public bool probe;            // is sensor probe?
        public String target_type;      // bitmask of acceptable target classes

        // for turrets:
        public Vector3F[] muzzle_pts = new Vector3F[MAX_STORES];    // default turret muzzle points
        public int nbarrels;                  // number of barrels on the turret

        // for missile hard points:
        public bool visible_stores;            // are external stores visible?
        public Vector3F[] attachments = new Vector3F[MAX_STORES];   // attachment points on the rail
        public int nstores;                   // number of stores on this hard point
        public Vector3F eject = new Vector3F(0.0f, -100.0f, 0.0f);                     // eject velocity from rail in 3D

        // auto-aiming arc
        public float firing_cone;      // maximum deflection in any orientation
        public float aim_az_max = 1.5f;       // maximum deflection in azimuth
        public float aim_az_min = -1.5f;       // minimum deflection in azimuth
        public float aim_az_rest = 0.0f;      // azimuth of turret at rest
        public float aim_el_max = 1.5f;       // maximum deflection in elevation
        public float aim_el_min = -1.5f;       // minimum deflection in elevation
        public float aim_el_rest = 0.0f;      // elevation of turret at rest
        public float slew_rate = MathHelper.ToRadians(60);        // max rate of turret slew in rad/sec
        public int turret_axis = 0;     // 0=az  1=el  2=not supported

        // functional parameters:
        public float capacity = 100.0f;         // full charge (joules)
        public float recharge_rate;    // watts
        public float refire_delay;     // seconds - mechanical limit
        public float salvo_delay;      // seconds - ai refire time
        public int ammo = -1;
        public int ripple_count;     // number of rounds per salvo

        // carrying costs per shot:
        public float charge;           // energy cost of full charge
        public float min_charge;       // minimum energy needed to fire
        public float carry_mass;
        public float carry_resist;

        // shot parameters:
        public string damage_type;      // 0: normal, 1: EMP, 2: power drain
        public float damage = 1.0f;           // if beam, damage per second;
                                              // else,    damage per shot.
        public float penetration = 1.0f;      // ability to pierce shields, 1 is default
        public float speed;
        public float life;
        public float mass;
        public float drag;
        public float thrust;
        public float roll_rate;
        public float pitch_rate;
        public float yaw_rate;
        public float roll_drag;
        public float pitch_drag;
        public float yaw_drag;
        public float integrity = 100.0f;        // hit points for drones = 100
        public float lethal_radius = 500.0f;    // detonation range for missiles

        public float det_range = 0.0f;        // detonation range for cluster weapons
        public string det_child;        // type of submunition
        public int det_count = 0;        // number of submunitions
        public float det_spread = (float)(Math.PI / 8);       // spread of submunition deployment

        // HUD parameters:
        public float min_range;
        public float max_range;
        public float max_track;

        // shot representation:
        public string graphic_type;     // sprite or blob?
        public float width;            // blob width
        public float length;           // blob length
        public float scale = 1.0f;            // sprite scale
        public float explosion_scale;  // scale factor for damage to this drone
        public float light;            // light emitted by shot
        public Color light_color = Color.white;      // color of light emitted by shot
        public float flash_scale;      // size of muzzle flash sprite
        public float flare_scale;      // size of drive flare sprite

        public float spread_az;        // spread range in radians
        public float spread_el;        // spread range in radians

        public string[] anim_frames = new string[16];
        public int anim_length;
        public string beauty;
        public string bitmap;
        public string model;
        public string turret;
        public string turret_base;
        public string trail;
        public string flash;
        public string flare;
        public string sound;
        public float sound_min_dist = float.MinValue;
        public float sound_max_dist = float.MinValue;

        public int trail_length = 0;
        public float trail_width = 0;
        public int trail_dim = 0;
    }
}