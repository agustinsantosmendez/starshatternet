﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NavLight.h/NavLight.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Navigation Lights System class
*/
using System;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using DWORD = System.UInt32;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.SimElements
{
    public class NavLight : ShipSystem
    {
        public const int MAX_LIGHTS = 8;

        public NavLight(double period, double scale)
            : base(CATEGORY.COMPUTER, 32, "Navigation Lights", 1, 0)
        {
            this.period = period;
            nlights = 0;
            this.scale = scale;
            enable = true;
            name = Game.GetText("sys.nav-light");
            abrv = Game.GetText("sys.nav-light.abrv");

            //ZeroMemory(beacon, sizeof(beacon));
            //ZeroMemory(beacon_type, sizeof(beacon_type));
            //ZeroMemory(pattern, sizeof(pattern));
        }
        public NavLight(NavLight c) : base(c)
        {
            period = c.period;
            scale = c.scale;
            nlights = 0;
            enable = true;
            Mount(c);
            SetAbbreviation(c.Abbreviation());
            //ZeroMemory(beacon, sizeof(beacon));
            //ZeroMemory(beacon_type, sizeof(beacon_type));

            nlights = c.nlights;

            for (int i = 0; i < nlights; i++)
            {
                loc[i] = c.loc[i];
                pattern[i] = c.pattern[i];
                beacon_type[i] = c.beacon_type[i];

                DriveSprite rep = new DriveSprite(images[beacon_type[i]]);
                rep.Scale(c.scale);

                beacon[i] = rep;
            }

            offset = (uint)RandomHelper.Rand();
        }

        ~NavLight()
        {
#if TODO
             for (int i = 0; i < nlights; i++)
                GRAPHIC_DESTROY(beacon[i]);
#endif
            throw new NotImplementedException();
        }


        private static bool initialized = false;
        public static void Initialize()
        {
            if (initialized) return;

            images[0] = new Bitmap("beacon1");
            images[1] = new Bitmap("beacon2");
            images[2] = new Bitmap("beacon3");
            images[3] = new Bitmap("beacon4");

            initialized = true;
        }

        public static void Close()
        {
        }

        public override void ExecFrame(double seconds)
        {
            if (enable && power_on)
            {
                double t = (Game.GameTime() + offset) / 1000.0;
                DWORD n = (uint)(Math.IEEERemainder(t, period) * 32 / period);
                DWORD code = (uint)(1 << (int)n);

                for (int i = 0; i < nlights; i++)
                {
                    if (beacon[i] != null)
                    {
                        if ((pattern[i] & code) != 0)
                            beacon[i].SetShade(1);
                        else
                            beacon[i].SetShade(0);
                    }
                }
            }
            else
            {
                for (int i = 0; i < nlights; i++)
                {
                    if (beacon[i] != null)
                    {
                        beacon[i].SetShade(0);
                    }
                }
            }
        }

        public int NumBeacons() { return nlights; }
        public SpriteGraphic Beacon(int index) { return beacon[index]; }
        public bool IsEnabled() { return enable; }

        public virtual void Enable()
        {
            enable = true;
        }

        public virtual void Disable()
        {
            enable = false;
        }

        public virtual void AddBeacon(Point l, DWORD ptn, int t = 1)
        {
            if (nlights < MAX_LIGHTS)
            {
                loc[nlights] = l;
                pattern[nlights] = ptn;
                beacon_type[nlights] = t;

                DriveSprite rep = new DriveSprite(images[t]);
                rep.Scale(scale);

                beacon[nlights] = rep;
                nlights++;
            }
        }
        public virtual void SetPeriod(double p)
        {
            period = p;
        }

        public virtual void SetPattern(int index, DWORD ptn)
        {
            if (index >= 0 && index < nlights)
                pattern[index] = ptn;
        }

        public virtual void SetOffset(DWORD o)
        {
            offset = o;
        }

        public override void Orient(Physical rep)
        {
            base.Orient(rep);

            Matrix33D orientation = rep.Cam().Orientation();
            Point ship_loc = rep.Location();

            for (int i = 0; i < nlights; i++)
            {
                Point projector = (orientation * loc[i]) + ship_loc; // TODO matrix * vector multiplication order
                if (beacon[i] != null) beacon[i].MoveTo(projector);
            }
        }


        protected double period;
        protected double scale;
        protected bool enable;

        protected int nlights;

        protected Point[] loc = new Point[MAX_LIGHTS];
        protected DriveSprite[] beacon = new DriveSprite[MAX_LIGHTS];
        protected DWORD[] pattern = new DWORD[MAX_LIGHTS];
        protected int[] beacon_type = new int[MAX_LIGHTS];
        protected DWORD offset;

        protected static Bitmap[] images = new Bitmap[4];
    }
}