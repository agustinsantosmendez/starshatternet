# Introduction  [From Wikipedia]
Starshatter: The Gathering Storm is a strategy-oriented space combat simulator created by Destroyer Studios and released on 7 July 2004.

Starshatter is a result of a project begun in 1997 by creator John DiCamillo as a fusion of many space sims available at the time.

Starshatter: The Gathering Storm is the 2004 remake of the original Starshatter from 1997, and the basic gameplay style is not much different, though both graphics and sound effects have been redone with full DirectX 9 support.

In September 2011, a post in the Starshatter Mods modding forum announced that Starshatter was being released as open source by the game's original creator.

This project is a port of the original game to C# and Unity3D.

