﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MenuScreen.h/MenuScreen.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

 */
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Network;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MenuScreen : BaseScreen
    {
        public MenuScreen()
        {
            screen = null; menudlg = null;
            fadewin = null; exitdlg = null; // TODO  fadeview = null; 
            auddlg = null; viddlg = null; optdlg = null; ctldlg = null; joydlg = null; keydlg = null;
            playdlg = null; confirmdlg = null; firstdlg = null; awarddlg = null; cmpSelectDlg = null;
            msnSelectDlg = null; modDlg = null; modInfoDlg = null;
            msnEditDlg = null; msnElemDlg = null; msnEventDlg = null; msnEditNavDlg = null;
            netClientDlg = null; netAddrDlg = null; netPassDlg = null; netLobbyDlg = null; netServerDlg = null;
            netUnitDlg = null; loadDlg = null; tacRefDlg = null; current_dlg = null; isShown = false;
        }
        //public virtual ~MenuScreen();

        public virtual void Setup(Screen screen, Camera camera, GameObject gameObj)
        {
            FormDef menu_def = new FormDef("MenuDlg", 0);
            menu_def.Load("MenuDlg");
            menudlg = new MenuDlg(screen, camera, menu_def, this, gameObj);

            FormDef exit_def = new FormDef("ExitDlg", 0);
            exit_def.Load("ExitDlg");
            exitdlg = new ExitDlg(screen, camera, exit_def, this, gameObj);

            FormDef aud_def = new FormDef("AudDlg", 0);
            aud_def.Load("AudDlg");
            auddlg = new AudDlg(screen, camera, aud_def, this, gameObj);

            FormDef ctl_def = new FormDef("CtlDlg", 0);
            ctl_def.Load("CtlDlg");
            ctldlg = new CtlDlg(screen, camera, ctl_def, this, gameObj);

            FormDef opt_def = new FormDef("OptDlg", 0);
            opt_def.Load("OptDlg");
            optdlg = new OptDlg(screen, camera, opt_def, this, gameObj);

            FormDef vid_def = new FormDef("VidDlg", 0);
            vid_def.Load("VidDlg");
            viddlg = new VidDlg(screen, camera, vid_def, this, gameObj);

            FormDef mod_def = new FormDef("ModDlg", 0);
            mod_def.Load("ModDlg");
            modDlg = new ModDlg(screen, camera, mod_def, this, gameObj);

            FormDef tac_ref_def = new FormDef("TacRefDlg", 0);
            tac_ref_def.Load("TacRefDlg");
            tacRefDlg = new TacRefDlg(screen, camera, tac_ref_def, this, gameObj);

            FormDef cmp_sel_def = new FormDef("CmpSelectDlg", 0);
            cmp_sel_def.Load("CmpSelectDlg");
            cmpSelectDlg = new CmpSelectDlg(screen, camera, cmp_sel_def, this, gameObj);
#if TODO
            FormDef net_lobby_def = new FormDef("NetLobbyDlg", 0);
            net_lobby_def.Load("NetLobbyDlg");
            netLobbyDlg = new NetLobbyDlg(screen, camera, net_lobby_def, this, gameObj);


            FormDef net_client_def = new FormDef("NetClientDlg", 0);
            net_client_def.Load("NetClientDlg");
            netClientDlg = new NetClientDlg(screen, net_client_def, this, p);

            FormDef net_server_def = new FormDef("NetServerDlg", 0);
            net_server_def.Load("NetServerDlg");
            netServerDlg = new NetServerDlg(screen, net_server_def, this, p);

            FormDef net_unit_def = new FormDef("NetUnitDlg", 0);
            net_unit_def.Load("NetUnitDlg");
            netUnitDlg = new NetUnitDlg(screen, net_unit_def, this, p);

            FormDef net_addr_def = new FormDef("NetAddrDlg", 0);
            net_addr_def.Load("NetAddrDlg");
            netAddrDlg = new NetAddrDlg(screen, net_addr_def, this, p);

            FormDef net_pass_def = new FormDef("NetPassDlg", 0);
            net_pass_def.Load("NetPassDlg");
            netPassDlg = new NetPassDlg(screen, net_pass_def, this, p);

#endif
            FormDef msn_edit_def = new FormDef("MsnEditDlg", 0);
            msn_edit_def.Load("MsnEditDlg");
            msnEditDlg = new MsnEditDlg(screen, camera, msn_edit_def, this, gameObj);

            FormDef msn_nav_def = new FormDef("MsnEditNavDlg", 0);
            msn_nav_def.Load("MsnEditNavDlg");
            msnEditNavDlg = new MsnEditNavDlg(screen, camera, msn_nav_def, this, gameObj);

            FormDef msn_elem_def = new FormDef("MsnElemDlg", 0);
            msn_elem_def.Load("MsnElemDlg");
            msnElemDlg = new MsnElemDlg(screen, camera, msn_elem_def, this, gameObj);

            FormDef msn_event_def = new FormDef("MsnEventDlg", 0);
            msn_event_def.Load("MsnEventDlg");
            msnEventDlg = new MsnEventDlg(screen, camera, msn_event_def, this, gameObj);

            FormDef msn_def = new FormDef("MsnSelectDlg", 0);
            msn_def.Load("MsnSelectDlg");
            msnSelectDlg = new MsnSelectDlg(screen, camera, msn_def, this, gameObj);

            FormDef player_def = new FormDef("PlayerDlg", 0);
            player_def.Load("PlayerDlg");
            playdlg = new PlayerDlg(screen, camera, player_def, this, gameObj);

            FormDef award_def = new FormDef("AwardDlg", 0);
            award_def.Load("AwardDlg");
            awarddlg = new AwardShowDlg(screen, camera, award_def, this, gameObj);

            FormDef joy_def = new FormDef("JoyDlg", 0);
            joy_def.Load("JoyDlg");
            joydlg = new JoyDlg(screen, camera, joy_def, this, gameObj);

            FormDef key_def = new FormDef("KeyDlg", 0);
            key_def.Load("KeyDlg");
            keydlg = new KeyDlg(screen, camera, key_def, this, gameObj);

            FormDef first_def = new FormDef("FirstTimeDlg", 0);
            first_def.Load("FirstTimeDlg");
            firstdlg = new FirstTimeDlg(screen, camera, first_def, this, gameObj);

            FormDef mod_info_def = new FormDef("ModInfoDlg", 0);
            mod_info_def.Load("ModInfoDlg");
            modInfoDlg = new ModInfoDlg(screen, camera, mod_info_def, this, gameObj);

            FormDef confirm_def = new FormDef("ConfirmDlg", 0);
            confirm_def.Load("ConfirmDlg");
            confirmdlg = new ConfirmDlg(screen, camera, confirm_def, this, gameObj);

            FormDef load_def = new FormDef("LoadDlg", 0);
            load_def.Load("LoadDlg");
            loadDlg = new LoadDlg(screen, camera, load_def, gameObj);
#if TODO
            fadewin = new Window(screen, 0, 0, 1, 1);
            fadeview = new FadeView(fadewin, 2, 0, 0);
            fadewin.AddView(fadeview);
            screen.AddWindow(fadewin);
#endif
        }
        public virtual void TearDown()
        {
#if TODO
            if (menudlg != null) screen.DelWindow(menudlg.Destroy();
            if (netClientDlg != null) screen.DelWindow(netClientDlg);
            if (netAddrDlg != null) screen.DelWindow(netAddrDlg);
            if (netPassDlg != null) screen.DelWindow(netPassDlg);
            if (netLobbyDlg != null) screen.DelWindow(netLobbyDlg);
            if (netServerDlg  != null) screen.DelWindow(netServerDlg);
            if (netUnitDlg != null) screen.DelWindow(netUnitDlg);

            if (cmpSelectDlg != null) screen.DelWindow(cmpSelectDlg);
            if (awarddlg != null) screen.DelWindow(awarddlg);
            if (firstdlg != null) screen.DelWindow(firstdlg);
            if (msnSelectDlg != null) screen.DelWindow(msnSelectDlg);
            if (msnEditDlg != null) screen.DelWindow(msnEditDlg);
            if (msnElemDlg != null) screen.DelWindow(msnElemDlg);
            if (msnEventDlg != null) screen.DelWindow(msnEventDlg);
            if (msnEditNavDlg != null) screen.DelWindow(msnEditNavDlg);
            if (tacRefDlg != null) screen.DelWindow(tacRefDlg);
            if (loadDlg != null) screen.DelWindow(loadDlg);

            if (auddlg != null) screen.DelWindow(auddlg);
            if (viddlg != null) screen.DelWindow(viddlg);
            if (optdlg != null) screen.DelWindow(optdlg);
            if (ctldlg != null) screen.DelWindow(ctldlg);
            if (modDlg != null) screen.DelWindow(modDlg);
            if (modInfoDlg != null) screen.DelWindow(modInfoDlg);
            if (joydlg != null) screen.DelWindow(joydlg);
            if (keydlg != null) screen.DelWindow(keydlg);
            if (exitdlg != null) screen.DelWindow(exitdlg);
            if (playdlg != null) screen.DelWindow(playdlg);
            if (confirmdlg != null) screen.DelWindow(confirmdlg);
            if (fadewin != null) screen.DelWindow(fadewin);  
            
            fadewin.Destroy();
#endif
            menudlg.Destroy();
            exitdlg.Destroy();
            auddlg.Destroy();
            viddlg.Destroy();
            optdlg.Destroy();
            ctldlg.Destroy();
            modDlg.Destroy();
            modInfoDlg.Destroy();
            joydlg.Destroy();
            keydlg.Destroy();
            playdlg.Destroy();
            confirmdlg.Destroy();

            netClientDlg.Destroy();
            netAddrDlg.Destroy();
            netPassDlg.Destroy();
            netLobbyDlg.Destroy();
            netServerDlg.Destroy();
            netUnitDlg.Destroy();
            msnSelectDlg.Destroy();
            msnEditDlg.Destroy();
            msnElemDlg.Destroy();
            msnEventDlg.Destroy();
            msnEditNavDlg.Destroy();
            tacRefDlg.Destroy();
            loadDlg.Destroy();
            firstdlg.Destroy();
            awarddlg.Destroy();
            cmpSelectDlg.Destroy();

            screen = null;
            fadewin = null;
            //fadeview  =  null;
            menudlg = null;
            exitdlg = null;
            playdlg = null;
            confirmdlg = null;
            msnSelectDlg = null;
            msnEditDlg = null;
            msnElemDlg = null;
            msnEventDlg = null;
            msnEditNavDlg = null;
            cmpSelectDlg = null;
            awarddlg = null;
            firstdlg = null;
            netClientDlg = null;
            netAddrDlg = null;
            netPassDlg = null;
            netLobbyDlg = null;
            netServerDlg = null;
            netUnitDlg = null;
            loadDlg = null;
            tacRefDlg = null;

            auddlg = null;
            viddlg = null;
            optdlg = null;
            ctldlg = null;
            modDlg = null;
            modInfoDlg = null;
            joydlg = null;
            keydlg = null;

            screen = null;
        }
        public virtual bool CloseTopmost()
        {
            bool processed = false;
            if (joydlg != null && joydlg.IsShown())
            {
                ShowCtlDlg();
                processed = true;
            }

            else if (keydlg != null && keydlg.IsShown())
            {
                ShowCtlDlg();
                processed = true;
            }

            else if (msnElemDlg != null && msnElemDlg.IsShown())
            {
                HideMsnElemDlg();
                processed = true;
            }

            else if (msnEventDlg != null && msnEventDlg.IsShown())
            {
                HideMsnEventDlg();
                processed = true;
            }

            else if (netAddrDlg != null && netAddrDlg.IsShown())
            {
                ShowNetClientDlg();
                processed = true;
            }

            else if (netPassDlg != null && netPassDlg.IsShown())
            {
                ShowNetClientDlg();
                processed = true;
            }

            else if (netServerDlg != null && netServerDlg.IsShown())
            {
                ShowNetClientDlg();
                processed = true;
            }

            else if (netUnitDlg != null && netUnitDlg.IsShown())
            {
                netUnitDlg.OnCancel(null);
                processed = true;
            }

            else if (netLobbyDlg != null && netLobbyDlg.IsShown())
            {
                netLobbyDlg.OnCancel(null, null);
                processed = true;
            }

            else if (netClientDlg != null && netClientDlg.IsShown())
            {
                netClientDlg.OnCancel(null);
                processed = true;
            }

            else if (exitdlg != null && exitdlg.IsShown())
            {
                // key_exit is handled in the exit dlg...
            }

            else if (cmpSelectDlg != null && cmpSelectDlg.IsShown())
            {
                if (cmpSelectDlg.CanClose())
                    ShowMenuDlg();

                processed = true;
            }

            else if (menudlg != null && !menudlg.IsShown())
            {
                ShowMenuDlg();
                processed = true;
            }

            return processed;
        }

        public virtual bool IsShown() { return isShown; }
        public virtual void Show()
        {
            if (!isShown)
            {
                Starshatter stars = Starshatter.GetInstance();
                NetLobby lobby = NetLobby.GetInstance();

                if (lobby != null)
                {
                    ShowNetLobbyDlg();
                }
                else if (current_dlg == msnSelectDlg)
                {
                    ShowMsnSelectDlg();
                }
                else
                {
                    if (Player.ConfigExists())
                    {
                        ShowMenuDlg();
                    }
                    else
                    {
                        ShowFirstTimeDlg();
                    }
                }

                isShown = true;
            }
        }
        public virtual void Hide()
        {
            if (isShown)
            {
                HideAll();
                isShown = false;
            }
        }

        public virtual void ExecFrame()
        {
            Game.SetScreenColor(Color.black);

            if (menudlg != null && menudlg.IsShown())
                menudlg.ExecFrame();

            if (exitdlg != null && exitdlg.IsShown())
                exitdlg.ExecFrame();

            if (joydlg != null && joydlg.IsShown())
                joydlg.ExecFrame();

            if (keydlg != null && keydlg.IsShown())
                keydlg.ExecFrame();

            if (ctldlg != null && ctldlg.IsShown())
                ctldlg.ExecFrame();

            if (optdlg != null && optdlg.IsShown())
                optdlg.ExecFrame();

            if (auddlg != null && auddlg.IsShown())
                auddlg.ExecFrame();

            if (viddlg != null && viddlg.IsShown())
                viddlg.ExecFrame();

            if (confirmdlg != null && confirmdlg.IsShown())
                confirmdlg.ExecFrame();

            if (playdlg != null && playdlg.IsShown())
                playdlg.ExecFrame();

            if (msnSelectDlg != null && msnSelectDlg.IsShown())
                msnSelectDlg.ExecFrame();

            if (msnEditNavDlg != null && msnEditNavDlg.IsShown())
                msnEditNavDlg.ExecFrame();

            if (firstdlg != null && firstdlg.IsShown())
                firstdlg.ExecFrame();

            if (awarddlg != null && awarddlg.IsShown())
                awarddlg.ExecFrame();

            if (cmpSelectDlg != null && cmpSelectDlg.IsShown())
                cmpSelectDlg.ExecFrame();

            if (netClientDlg != null && netClientDlg.IsShown())
                netClientDlg.ExecFrame();

            if (netAddrDlg != null && netAddrDlg.IsShown())
                netAddrDlg.ExecFrame();

            if (netPassDlg != null && netPassDlg.IsShown())
                netPassDlg.ExecFrame();

            if (netLobbyDlg != null && netLobbyDlg.IsShown())
                netLobbyDlg.ExecFrame();

            if (netServerDlg != null && netServerDlg.IsShown())
                netServerDlg.ExecFrame();

            if (netUnitDlg != null && netUnitDlg.IsShown())
                netUnitDlg.ExecFrame();

            if (loadDlg != null && loadDlg.IsShown())
                loadDlg.ExecFrame();

            if (tacRefDlg != null && tacRefDlg.IsShown())
                tacRefDlg.ExecFrame();
        }

        public virtual void ShowMenuDlg()
        {
            HideAll();
            if (menudlg != null)
            {
                menudlg.Show();
                menudlg.SetTopMost(true);
            }
            Mouse.Show(true);
        }
        public virtual void ShowCmpSelectDlg()
        {
            HideAll();
            current_dlg = cmpSelectDlg;
            if (cmpSelectDlg != null)
                cmpSelectDlg.Show();
            Mouse.Show(true);
        }
        public virtual void ShowMsnSelectDlg()
        {
            HideAll();
            current_dlg = msnSelectDlg;

            if (msnSelectDlg != null)
                msnSelectDlg.Show();

            if (msnEditNavDlg != null)
                msnEditNavDlg.SetMission(null);

            Mouse.Show(true);
        }
        public override void ShowModDlg()
        {
            if (modDlg != null)
            {
                HideAll();
                modDlg.Show();
                modDlg.SetTopMost(true);
                Mouse.Show(true);
            }
            else
            {
                ShowMsnSelectDlg();
            }
        }
        public override void ShowModInfoDlg()
        {
            if (modDlg != null && modInfoDlg != null)
            {
                HideAll();
                modDlg.Show();
                modDlg.SetTopMost(false);
                modInfoDlg.Show();
                Mouse.Show(true);
            }
            else
            {
                ShowMsnSelectDlg();
            }
        }
        public virtual void ShowMsnEditDlg()
        {
            if (msnEditDlg != null)
            {
                bool nav_shown = false;
                if (msnEditNavDlg != null && msnEditNavDlg.IsShown())
                    nav_shown = true;

                HideAll();

                if (nav_shown)
                {
                    msnEditNavDlg.Show();
                    msnEditNavDlg.SetTopMost(true);
                }
                else
                {
                    msnEditDlg.Show();
                    msnEditDlg.SetTopMost(true);
                }

                Mouse.Show(true);
            }
            else
            {
                ShowMsnSelectDlg();
            }
        }
        public virtual void ShowNetClientDlg()
        {
            if (netClientDlg != null)
            {
                HideAll();
                netClientDlg.Show();
                netClientDlg.SetTopMost(true);
            }

            Mouse.Show(true);
        }
        public virtual void ShowNetAddrDlg()
        {
            if (netAddrDlg != null)
            {
                if (netClientDlg != null)
                {
                    netClientDlg.Show();
                    netClientDlg.SetTopMost(false);
                }

                netAddrDlg.Show();
            }

            Mouse.Show(true);
        }
        public virtual void ShowNetPassDlg()
        {
            if (netPassDlg != null)
            {
                ShowNetClientDlg();
                if (netClientDlg != null)
                    netClientDlg.SetTopMost(false);

                netPassDlg.Show();
            }

            Mouse.Show(true);
        }
        public virtual void ShowNetLobbyDlg()
        {
            if (netLobbyDlg != null)
            {
                HideAll();
                netLobbyDlg.Show();
                netLobbyDlg.SetTopMost(true);
            }

            Mouse.Show(true);
        }
        public virtual void ShowNetServerDlg()
        {
            if (netServerDlg != null)
            {
                netServerDlg.Show();
                netServerDlg.SetTopMost(true);
            }

            Mouse.Show(true);
        }
        public virtual void ShowNetUnitDlg()
        {
            if (netUnitDlg != null)
            {
                HideAll();
                netUnitDlg.Show();
                netUnitDlg.SetTopMost(true);
            }

            Mouse.Show(true);
        }
        public virtual void ShowFirstTimeDlg()
        {
            HideAll();
            if (menudlg != null && firstdlg != null)
            {
                menudlg.Show();
                menudlg.SetTopMost(false);

                firstdlg.Show();
                firstdlg.SetTopMost(true);
            }

            Mouse.Show(true);
        }
        public virtual void ShowPlayerDlg()
        {
            if (playdlg != null)
            {
                HideAll();
                playdlg.Show();
            }
            Mouse.Show(true);
        }
        public virtual void ShowTacRefDlg()
        {
            if (tacRefDlg != null)
            {
                HideAll();
                tacRefDlg.Show();
            }
            Mouse.Show(true);
        }
        public virtual void ShowAwardDlg()
        {
            if (awarddlg != null)
            {
                HideAll();
                awarddlg.Show();
            }
            Mouse.Show(true);
        }
        public override void ShowAudDlg()
        {
            HideAll();
            if (auddlg != null)
                auddlg.Show();
            Mouse.Show(true);
        }
        public override void ShowVidDlg()
        {
            HideAll();
            if (viddlg != null)
                viddlg.Show();
            Mouse.Show(true);
        }
        public override void ShowOptDlg()
        {
            HideAll();
            if (optdlg != null)
                optdlg.Show();
            Mouse.Show(true);
        }
        public override void ShowCtlDlg()
        {
            HideAll();
            if (ctldlg != null)
            {
                ctldlg.Show();
                ctldlg.SetTopMost(true);
            }
            Mouse.Show(true);
        }
        public override void ShowJoyDlg()
        {
            HideAll();
            if (ctldlg != null)
            {
                ctldlg.Show();
                ctldlg.SetTopMost(false);
            }

            if (joydlg != null)
                joydlg.Show();
            Mouse.Show(true);
        }
        public override void ShowKeyDlg()
        {
            HideAll();

            if (ctldlg != null)
            {
                ctldlg.Show();
                ctldlg.SetTopMost(false);
            }

            if (keydlg != null)
                keydlg.Show();
            Mouse.Show(true);
        }
        public virtual void ShowExitDlg()
        {
            HideAll();

            if (menudlg != null)
            {
                menudlg.Show();
                menudlg.SetTopMost(false);
            }

            if (exitdlg != null)
                exitdlg.Show();
            else
                Starshatter.GetInstance().Exit();

            Mouse.Show(true);
        }
        public virtual void ShowConfirmDlg()
        {
            if (confirmdlg != null)
            {
                if (msnSelectDlg != null && msnSelectDlg.IsShown())
                    msnSelectDlg.SetTopMost(false);

                if (playdlg != null && playdlg.IsShown())
                    playdlg.SetTopMost(false);

                confirmdlg.Show();
                confirmdlg.SetTopMost(true);
                Mouse.Show(true);
            }
        }
        public virtual void HideConfirmDlg()
        {
            if (confirmdlg != null)
                confirmdlg.Hide();

            if (msnSelectDlg != null && msnSelectDlg.IsShown())
                msnSelectDlg.SetTopMost(true);

            if (playdlg != null && playdlg.IsShown())
                playdlg.SetTopMost(true);
        }
        public virtual void ShowLoadDlg()
        {
            if (loadDlg != null)
            {
                if (menudlg != null && menudlg.IsShown())
                    menudlg.SetTopMost(false);

                loadDlg.Show();
                loadDlg.SetTopMost(true);
                Mouse.Show(true);
            }
        }
        public virtual void HideLoadDlg()
        {
            if (loadDlg != null)
                loadDlg.Hide();

            if (menudlg != null && menudlg.IsShown())
                menudlg.SetTopMost(true);
        }

        // base screen interface:
        public override void ShowMsnElemDlg()
        {
            if (msnElemDlg != null)
            {
                if (msnEditDlg != null && msnEditDlg.IsShown())
                    msnEditDlg.SetTopMost(false);

                if (msnEditNavDlg != null && msnEditNavDlg.IsShown())
                    msnEditNavDlg.SetTopMost(false);

                msnElemDlg.Show();
                msnElemDlg.SetTopMost(true);
                Mouse.Show(true);
            }
        }
        public override void HideMsnElemDlg()
        {
            if (msnElemDlg != null)
                msnElemDlg.Hide();

            if (msnEditDlg != null && msnEditDlg.IsShown())
                msnEditDlg.SetTopMost(true);

            if (msnEditNavDlg != null && msnEditNavDlg.IsShown())
                msnEditNavDlg.SetTopMost(true);
        }
        public override MsnElemDlg GetMsnElemDlg() { return msnElemDlg; }

        public virtual void ShowMsnEventDlg()
        {
            if (msnEventDlg != null)
            {
                if (msnEditDlg != null && msnEditDlg.IsShown())
                    msnEditDlg.SetTopMost(false);

                if (msnEditNavDlg != null && msnEditNavDlg.IsShown())
                    msnEditNavDlg.SetTopMost(false);

                msnEventDlg.Show();
                msnEventDlg.SetTopMost(true);
                Mouse.Show(true);
            }
        }
        public virtual void HideMsnEventDlg()
        {
            if (msnEventDlg != null)
                msnEventDlg.Hide();

            if (msnEditDlg != null && msnEditDlg.IsShown())
                msnEditDlg.SetTopMost(true);

            if (msnEditNavDlg != null && msnEditNavDlg.IsShown())
                msnEditNavDlg.SetTopMost(true);
        }
        public virtual MsnEventDlg GetMsnEventDlg() { return msnEventDlg; }

        public override void ShowNavDlg()
        {
            if (msnEditNavDlg != null && !msnEditNavDlg.IsShown())
            {
                HideAll();
                msnEditNavDlg.Show();
                Mouse.Show(true);
            }
        }
        public override void HideNavDlg()
        {
            if (msnEditNavDlg != null)
                msnEditNavDlg.Hide();
        }
        public override bool IsNavShown() { return msnEditNavDlg != null && msnEditNavDlg.IsShown(); }
        public override NavDlg GetNavDlg() { return msnEditNavDlg; }

        public virtual MsnSelectDlg GetMsnSelectDlg() { return msnSelectDlg; }
        public override ModDlg GetModDlg() { return modDlg; }
        public override ModInfoDlg GetModInfoDlg() { return modInfoDlg; }
        public virtual MsnEditDlg GetMsnEditDlg() { return msnEditDlg; }
        public virtual NetClientDlg GetNetClientDlg() { return netClientDlg; }
        public virtual NetAddrDlg GetNetAddrDlg() { return netAddrDlg; }
        public virtual NetPassDlg GetNetPassDlg() { return netPassDlg; }
        public virtual NetLobbyDlg GetNetLobbyDlg() { return netLobbyDlg; }
        public virtual NetServerDlg GetNetServerDlg() { return netServerDlg; }
        public virtual NetUnitDlg GetNetUnitDlg() { return netUnitDlg; }
        public virtual LoadDlg GetLoadDlg() { return loadDlg; }
        public virtual TacRefDlg GetTacRefDlg() { return tacRefDlg; }

        public override AudDlg GetAudDlg() { return auddlg; }
        public override VidDlg GetVidDlg() { return viddlg; }
        public override OptDlg GetOptDlg() { return optdlg; }
        public override CtlDlg GetCtlDlg() { return ctldlg; }
        public override JoyDlg GetJoyDlg() { return joydlg; }
        public override KeyDlg GetKeyDlg() { return keydlg; }
        public virtual ExitDlg GetExitDlg() { return exitdlg; }
        public virtual FirstTimeDlg GetFirstTimeDlg() { return firstdlg; }
        public virtual PlayerDlg GetPlayerDlg() { return playdlg; }
        public virtual AwardShowDlg GetAwardDlg() { return awarddlg; }
        public virtual ConfirmDlg GetConfirmDlg() { return confirmdlg; }

        public override void ApplyOptions()
        {
            if (ctldlg != null) ctldlg.Apply();
            if (optdlg != null) optdlg.Apply();
            if (auddlg != null) auddlg.Apply();
            if (viddlg != null) viddlg.Apply();
            if (modDlg != null) modDlg.Apply();

            ShowMenuDlg();
        }
        public override void CancelOptions()
        {
            if (ctldlg != null) ctldlg.Cancel();
            if (optdlg != null) optdlg.Cancel();
            if (auddlg != null) auddlg.Cancel();
            if (viddlg != null) viddlg.Cancel();
            if (modDlg != null) modDlg.Cancel();

            ShowMenuDlg();
        }


        private void HideAll()
        {
            Keyboard.FlushKeys();

            current_dlg = null;

            if (menudlg != null) menudlg.Hide();
            if (exitdlg != null) exitdlg.Hide();
            if (auddlg != null) auddlg.Hide();
            if (viddlg != null) viddlg.Hide();
            if (ctldlg != null) ctldlg.Hide();
            if (optdlg != null) optdlg.Hide();
            if (joydlg != null) joydlg.Hide();
            if (keydlg != null) keydlg.Hide();
            if (playdlg != null) playdlg.Hide();
            if (confirmdlg != null) confirmdlg.Hide();
            if (modDlg != null) modDlg.Hide();
            if (modInfoDlg != null) modInfoDlg.Hide();
            if (msnSelectDlg != null) msnSelectDlg.Hide();
            if (msnEditDlg != null) msnEditDlg.Hide();
            if (msnElemDlg != null) msnElemDlg.Hide();
            if (msnEventDlg != null) msnEventDlg.Hide();
            if (msnEditNavDlg != null) msnEditNavDlg.Hide();
            if (netClientDlg != null) netClientDlg.Hide();
            if (netAddrDlg != null) netAddrDlg.Hide();
            if (netPassDlg != null) netPassDlg.Hide();
            if (netLobbyDlg != null) netLobbyDlg.Hide();
            if (netServerDlg != null) netServerDlg.Hide();
            if (netUnitDlg != null) netUnitDlg.Hide();
            if (firstdlg != null) firstdlg.Hide();
            if (awarddlg != null) awarddlg.Hide();
            if (cmpSelectDlg != null) cmpSelectDlg.Hide();
            if (tacRefDlg != null) tacRefDlg.Hide();
            if (loadDlg != null) loadDlg.Hide();
        }

        private Screen screen;
        private MenuDlg menudlg;

        private Window fadewin;
        //TODO private FadeView fadeview;
        private ExitDlg exitdlg;
        private AudDlg auddlg;
        private VidDlg viddlg;
        private OptDlg optdlg;
        private CtlDlg ctldlg;
        private JoyDlg joydlg;
        private KeyDlg keydlg;
        private ConfirmDlg confirmdlg;
        private PlayerDlg playdlg;
        private AwardShowDlg awarddlg;
        private ModDlg modDlg;
        private ModInfoDlg modInfoDlg;
        private MsnSelectDlg msnSelectDlg;
        private MsnEditDlg msnEditDlg;
        private MsnElemDlg msnElemDlg;
        private MsnEventDlg msnEventDlg;
        private NavDlg msnEditNavDlg;
        private LoadDlg loadDlg;
        private TacRefDlg tacRefDlg;

        private CmpSelectDlg cmpSelectDlg;
        private FirstTimeDlg firstdlg;
        private NetClientDlg netClientDlg;
        private NetAddrDlg netAddrDlg;
        private NetPassDlg netPassDlg;
        private NetLobbyDlg netLobbyDlg;
        private NetServerDlg netServerDlg;
        private NetUnitDlg netUnitDlg;

        private FormWindow current_dlg;
        //DataLoader loader;

        private int wc, hc;
        private bool isShown;
    }
}
