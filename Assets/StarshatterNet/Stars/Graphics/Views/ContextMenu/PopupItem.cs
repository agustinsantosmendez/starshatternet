﻿using System;
using StarshatterNet.Stars.Views;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace StarshatterNet.Stars.Graphics.Views
{
    [RequireComponent(typeof(Toggle))]
    public class PopupItem : MonoBehaviour, IEventSystemHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, ICancelHandler
    {
        /// <summary>
        ///   <para>UnityEvent callback for when a popup item is clicked.</para>
        /// </summary>

        [Serializable]
        public class MenuClickedEvent : UnityEvent<GameObject>
        {
        }

        [SerializeField]
        private Text m_Text;
        [SerializeField]
        private Image m_Image;

        [SerializeField]
        private StarshatterNet.Stars.Views.Menu m_SubMenu;
        [SerializeField]
        private int m_Level;

        public MenuItem Value { get; set; }

        [SerializeField]
        private RectTransform m_RectTransform;
        [SerializeField]
        private Toggle m_Toggle;

        public MenuClickedEvent onItemClicked = new MenuClickedEvent();

        [SerializeField]
        public Color DefaultSelectableColor = new Color(1f, 1f, 1f, 1f);
        [SerializeField]
        public Color DefaultSelectedColor = new Color(0.7f, 0.7f, 0.7f, 1f);

        public Text text
        {
            get
            {
                return this.m_Text;
            }
            set
            {
                this.m_Text = value;
            }
        }

        public Image image
        {
            get
            {
                return this.m_Image;
            }
            set
            {
                this.m_Image = value;
            }
        }

        public StarshatterNet.Stars.Views.Menu submenu
        {
            get
            {
                return this.m_SubMenu;
            }
            set
            {
                this.m_SubMenu = value;
            }
        }
        public int level
        {
            get
            {
                return this.m_Level;
            }
            set
            {
                this.m_Level = value;
            }
        }

        public RectTransform rectTransform
        {
            get
            {
                return this.m_RectTransform;
            }
            set
            {
                this.m_RectTransform = value;
            }
        }

        public Toggle toggle
        {
            get
            {
                return this.m_Toggle;
            }
            set
            {
                this.m_Toggle = value;
            }
        }

        //Detect if a click occurs
        public void OnPointerClick(PointerEventData pointerEventData)
        {
            //Debug.Log("PopupItem.OnPointerClick " + this.name + " Game Object Clicked!");
            PopupItem popupitem = this.gameObject.GetComponent<PopupItem>();
            if (popupitem == null || (popupitem.submenu != null && popupitem.submenu.GetItems().Count != 0))
                return;

            Toggle toggle = this.GetComponent<Toggle>();
            toggle.isOn = !toggle.isOn;
            this.onItemClicked.Invoke(this.gameObject);
        }

        //Detect if the Cursor starts to pass over the GameObject
        public void OnPointerEnter(PointerEventData pointerEventData)
        {
            //Output to console the GameObject's name and the following message
            //Debug.Log("Cursor Entering " + name + " GameObject");
            Image imageItem = this.GetComponent<Image>();
            imageItem.color = DefaultSelectedColor;
            Toggle toggle = this.GetComponent<Toggle>();
            toggle.isOn = true;

            PopupItem popupitem = this.gameObject.GetComponent<PopupItem>();
            if (popupitem == null)
                return;

            PopupMenu popup = this.gameObject.GetComponentInParent<PopupMenu>();
            //Debug.Log("OnPointerEnter(), m_MenuObjs.Count:" + popup.MenuObjs.Count);

            StarshatterNet.Stars.Views.Menu menu = popupitem.submenu;
            if (popup.MenuObjs.Count > 0)
            {
                // submenuObj cancel??
                for (int i = popup.MenuObjs.Count; i >= popupitem.level + 1; i--)
                {
                    //Debug.Log("Sublevel " + i + " len :" + popup.MenuObjs.Count);
                    //Debug.Log("Item Sublevel" + popupitem.level);
                    GameObject.Destroy(popup.MenuObjs[i - 1]);
                    popup.MenuObjs.RemoveAt(i - 1);
                }
            }

            if (popupitem.submenu == null || popupitem.submenu.NumItems() == 0)
                return; // TODO Cancel or Hide

            RectTransform rectTransform = this.gameObject.GetComponent<RectTransform>();
            Vector3[] v = new Vector3[4];
            rectTransform.GetWorldCorners(v);

            PopupMenu parentPopupItem = this.gameObject.GetComponentInParent<PopupMenu>();
            parentPopupItem.CreatePopupMenu(v[2], menu);
        }

        //Detect when Cursor leaves the GameObject
        public void OnPointerExit(PointerEventData pointerEventData)
        {
            //Output the following message with the GameObject's name
            //Debug.Log("Cursor Exiting " + name + " GameObject");
            Image imageItem = this.GetComponent<Image>();
            imageItem.color = DefaultSelectableColor;
            Toggle toggle = this.GetComponent<Toggle>();
            toggle.isOn = false;
        }

        public virtual void OnCancel(BaseEventData eventData)
        {
            PopupMenu componentInParent = this.GetComponentInParent<PopupMenu>();
            if (!(bool)((UnityEngine.Object)componentInParent))
                return;
            componentInParent.Hide();
        }

    }
}
