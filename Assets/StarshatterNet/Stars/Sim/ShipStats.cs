﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      SimEvent.h/SimEvent.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Universe and Region classes
 */
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using System.Collections.Generic;

namespace StarshatterNet.Stars.Simulator
{
    public class ShipStats
    {
        public ShipStats(string name, int iff = 0)
        {
            this.name = name; this.iff = iff; kill1 = 0; kill2 = 0; lost = 0; coll = 0; points = 0;
            cmd_points = 0; gun_shots = 0; gun_hits = 0; missile_shots = 0; missile_hits = 0;
            combat_group = null; combat_unit = null; player = false; ship_class = 0; elem_index = -1;
            if (string.IsNullOrWhiteSpace(name))
                name = localeManager.GetText("[unknown]");
        }
        //~ShipStats();

        public static void Initialize() { records.Clear(); }
        public static void Close() { records.Clear(); }
        public static ShipStats Find(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                foreach (ShipStats st in records)
                {
                    if (st.GetName() == name)
                        return st;
                }

                ShipStats stats = new ShipStats(name);
                records.Add(stats);
                return stats;
            }

            return null;
        }

        public static int NumStats()
        {
            return records.Count;
        }


        public static ShipStats GetStats(int i)
        {
            if (i >= 0 && i < records.Count)
                return records[i];

            return null;
        }



        public void Summarize()
        {
            kill1 = 0;
            kill2 = 0;
            lost = 0;
            coll = 0;

            foreach (SimEvent evnt in events)
            {
                SimEvent.EVENT code = evnt.GetEvent();

                if (code == SimEvent.EVENT.GUNS_KILL)
                    kill1++;

                else if (code == SimEvent.EVENT.MISSILE_KILL)
                    kill2++;

                else if (code == SimEvent.EVENT.DESTROYED)
                    lost++;

                else if (code == SimEvent.EVENT.CRASH)
                    coll++;

                else if (code == SimEvent.EVENT.COLLIDE)
                    coll++;
            }
        }

        public string GetName() { return name; }

        // Original name was  GetStatType(). Changed in order to avoid conflicts
        public string GetStatType() { return type; }
        public string GetRole() { return role; }
        public string GetRegion() { return region; }
        public CombatGroup GetCombatGroup() { return combat_group; }
        public CombatUnit GetCombatUnit() { return combat_unit; }
        public int GetElementIndex() { return elem_index; }
        public CLASSIFICATION GetShipClass() { return ship_class; }
        public int GetIFF() { return iff; }
        public int GetGunKills() { return kill1; }
        public int GetMissileKills() { return kill2; }
        public int GetDeaths() { return lost; }
        public int GetColls() { return coll; }
        public int GetPoints() { return points; }
        public int GetCommandPoints() { return cmd_points; }

        public int GetGunShots() { return gun_shots; }
        public int GetGunHits() { return gun_hits; }
        public int GetMissileShots() { return missile_shots; }
        public int GetMissileHits() { return missile_hits; }

        public bool IsPlayer() { return player; }

        public List<SimEvent> GetEvents() { return events; }
        public SimEvent AddEvent(SimEvent e)
        {
            events.Add(e);
            return e;
        }

        public SimEvent AddEvent(SimEvent.EVENT evnt, string tgt = null, string info = null)
        {
            SimEvent e = new SimEvent(evnt, tgt, info);
            events.Add(e);
            return e;
        }

        public bool HasEvent(SimEvent.EVENT evnt)
        {
            for (int i = 0; i < events.Count; i++)
                if (events[i].GetEvent() == evnt)
                    return true;

            return false;
        }


        public void SetShipClass(CLASSIFICATION c) { ship_class = c; }
        public void SetIFF(int i) { iff = i; }
        public void SetType(string t)
        {
            if (!string.IsNullOrWhiteSpace(t))
                type = t;
        }

        public void SetRole(string r)
        {
            if (!string.IsNullOrWhiteSpace(r))
                role = r;
        }

        public void SetRegion(string r)
        {
            if (!string.IsNullOrWhiteSpace(r))
                region = r;
        }

        public void SetCombatGroup(CombatGroup g)
        {
            combat_group = g;
        }

        public void SetCombatUnit(CombatUnit u)
        {
            combat_unit = u;
        }

        public void SetElementIndex(int n)
        {
            elem_index = n;
        }

        public void SetPlayer(bool p)
        {
            player = p;
        }

        public void AddGunShot() { gun_shots++; }
        public void AddGunHit() { gun_hits++; }
        public void AddMissileShot() { missile_shots++; }
        public void AddMissileHit() { missile_hits++; }
        public void AddPoints(int p) { points += p; }
        public void AddCommandPoints(int p) { cmd_points += p; }


        private string name;
        private string type;
        private string role;
        private string region;
        private CombatGroup combat_group;
        private CombatUnit combat_unit;
        private bool player;
        private int elem_index;
        private CLASSIFICATION ship_class;
        private int iff;
        private int kill1;
        private int kill2;
        private int lost;
        private int coll;

        private int gun_shots;
        private int gun_hits;

        private int missile_shots;
        private int missile_hits;

        private int points;
        private int cmd_points;

        private List<SimEvent> events = new List<SimEvent>();
        private static List<ShipStats> records = new List<ShipStats>();
        protected ContentBundle localeManager;
    }
}
