﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      SimObject.h/SimObject.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Object and Observer classes
 */

using System.Collections.Generic;

namespace StarshatterNet.Stars.Simulator
{
    public interface ISimObserver
    {
        bool Update(SimObject obj);
        string GetObserverName();

        void Observe(SimObject obj);
        void Ignore(SimObject obj);
    }

    public class SimObserver : ISimObserver
    {
        public virtual bool Update(SimObject obj)
        {
            if (obj != null)
                observe_list.Remove(obj);

            return true;
        }

        public virtual string GetObserverName()
        {
            return string.Format("SimObserver 0x{0:X8}", this);
        }


        public virtual void Observe(SimObject obj)
        {
            if (obj != null)
            {
                obj.Register(this);

                if (!observe_list.Contains(obj))
                    observe_list.Add(obj);
            }
        }

        public virtual void Ignore(SimObject obj)
        {
            if (obj != null)
            {
                obj.Unregister(this);
                observe_list.Remove(obj);
            }
        }

        protected List<SimObject> observe_list = new List<SimObject>();
    }
}
