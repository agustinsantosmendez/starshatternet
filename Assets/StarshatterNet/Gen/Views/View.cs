﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      View.h/View.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Abstract View class
*/
using StarshatterNet.Gen.Graphics;
using UnityEngine;

namespace StarshatterNet.Gen.Views
{
    public class View : IView
    {
        //public View(GameObject parent, string name)
        //{
        //    this.name = name;
        //    SetWindow(parent);
        //}
        public View(Window window, string name)
        {
            this.name = name;
            this.window = window;
            SetWindow(window);
        }

        //public virtual ~View() { }

        //public int operator ==(const View& that) const { return this == &that; }

        // Operations:
        public virtual void Refresh() { }
        public virtual void OnWindowMove() { }
        public virtual void OnShow() { }
        public virtual void OnHide() { }

        public virtual void SetWindow(Window w)
        {
            windowObj = w.WindowObj;

            if (windowObj != null)
                rectTransform = windowObj.GetComponent<RectTransform>();

            OnWindowMove();
        }
        public virtual Window GetWindow() { return window; }
        public virtual Window Window { get { return window; } }

        protected virtual void BuildGamebjects() { }

        protected readonly string name;
        protected GameObject windowObj;
        protected Window window;
        protected RectTransform rectTransform;
    }
}