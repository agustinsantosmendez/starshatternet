﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Mission.h/Mission.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Universe and Region classes
*/
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.SimElements;
using System.Collections.Generic;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning MissionLoad class is still in development and is not recommended for production.
    public class MissionLoad
    {
        public MissionLoad(int s = -1, string n = null)
        {
            ship = s;
            for (int i = 0; i < 16; i++)
                load[i] = -1; // default: no weapon mounted

            if (n != null)
                name = n;
        }
        // ~MissionLoad();

        public int GetShip()
        {
            return ship;
        }

        public void SetShip(int s)
        {
            ship = s;
        }


        public string GetName()
        {
            return name;
        }
        public void SetName(string n)
        {
            name = n;
        }

        public int[] GetStations()
        {
            return load;
        }
        public int GetStation(int index)
        {
            if (index >= 0 && index < 16)
                return load[index];

            return 0;
        }
        public void SetStation(int index, int selection)
        {
            if (index >= 0 && index < 16)
                load[index] = selection;
        }


        protected int ship;
        protected string name;
        protected int[] load = new int[16];
    }
}