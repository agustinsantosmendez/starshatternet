﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      HardPoint.h/HardPoint.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Hard Point (gun or missile launcher) class
*/
using DigitalRune.Mathematics.Algebra;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
namespace StarshatterNet.Stars.SimElements
{
    public class HardPoint
    {
        public const int MAX_DESIGNS = 8;

        public HardPoint(Vector3F muzzle, double az = 0, double el = 0)
        {
            aim_azimuth = (float)az;
            aim_elevation = (float)el;
            this.muzzle = muzzle;
        }

        public HardPoint(HardPoint h)
        {
            aim_azimuth = h.aim_azimuth;
            aim_elevation = h.aim_elevation;
            muzzle = h.muzzle;
            mount_rel = h.mount_rel;
            radius = h.radius;
            hull_factor = h.hull_factor;
            for (int i = 0; i < designs.Length; i++)
                designs[i] = h.designs[i];
        }

        // ~HardPoint();

        //int operator ==(const HardPoint& w) const { return this == &w; }

        public virtual void AddDesign(WeaponDesign dsn)
        {
            for (int i = 0; i < MAX_DESIGNS; i++)
            {
                if (designs[i] == null)
                {
                    designs[i] = dsn;
                    return;
                }
            }
        }

        public virtual Weapon CreateWeapon(int type_index = 0)
        {
            if (type_index >= 0 && type_index < MAX_DESIGNS && designs[type_index] != null)
            {
                Point zero_pt = new Point(0.0f, 0.0f, 0.0f);
                Point[] muzzle_pt = { zero_pt };

                if (designs[type_index].turret.Length == 0)
                    muzzle_pt[0] = muzzle;

                Weapon missile = new Weapon(designs[type_index],
                1,
                muzzle_pt,
                aim_azimuth,
                aim_elevation);
                missile.SetAbbreviation(GetAbbreviation());
                missile.Mount(mount_rel, radius, hull_factor);
                return missile;
            }

            return null;
        }


        public virtual double GetCarryMass(int type_index = 0)
        {
            if (type_index >= 0 && type_index < MAX_DESIGNS && designs[type_index] != null)
                return designs[type_index].carry_mass;

            return 0;
        }
        public WeaponDesign GetWeaponDesign(int n) {
            if (n < 0 || n > designs.Length)
                return null;
            return designs[n];
        }

        public virtual void Mount(Point loc, float rad, float hull = 0.5f)
        {
            mount_rel = loc;
            radius = rad;
            hull_factor = hull;
        }

        public Point MountLocation() { return mount_rel; }
        public double Radius() { return radius; }
        public double HullProtection() { return hull_factor; }

        public virtual string GetName() { return name; }
        public virtual void SetName(string s) { name = s; }
        public virtual string GetAbbreviation() { return abrv; }
        public virtual void SetAbbreviation(string s) { abrv = s; }
        public virtual string GetDesign() { return sys_dsn; }
        public virtual void SetDesign(string s) { sys_dsn = s; }

        public virtual double GetAzimuth() { return aim_azimuth; }
        public virtual void SetAzimuth(double a) { aim_azimuth = (float)a; }
        public virtual double GetElevation() { return aim_elevation; }
        public virtual void SetElevation(double e) { aim_elevation = (float)e; }


        // Displayable name:
        protected string name;
        protected string abrv;
        protected string sys_dsn;

        protected WeaponDesign[] designs = new WeaponDesign[MAX_DESIGNS];
        protected Vector3F muzzle;
        protected float aim_azimuth;
        protected float aim_elevation;

        // Mounting:
        protected Point mount_rel;  // object space
        protected float radius;
        protected float hull_factor;
    }
}