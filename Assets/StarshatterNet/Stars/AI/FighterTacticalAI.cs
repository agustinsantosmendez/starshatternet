﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      FighterTacticalAI.h/FighterTacticalAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Fighter-specific mid-level (tactical) AI class
*/
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.AI
{
    public class FighterTacticalAI : TacticalAI
    {
        public FighterTacticalAI(ShipAI ai) : base(ai)
        {
            secondary_selection_time = 0;
            for (int i = 0; i < 4; i++)
                winchester[i] = false;

            ai_level = ai.GetAILevel();

            switch (ai_level)
            {
                default:
                case 2: THREAT_REACTION_TIME = 1000; break;
                case 1: THREAT_REACTION_TIME = 3000; break;
                case 0: THREAT_REACTION_TIME = 6000; break;
            }
        }
        // ~FighterTacticalAI();


        protected override bool CheckFlightPlan()
        {
            navpt = ship.GetNextNavPoint();

            Instruction.ACTION order = Instruction.ACTION.PATROL;
            roe = ROE.FLEXIBLE;

            if (navpt != null)
            {
                order = (Instruction.ACTION)navpt.Action();

                switch (order)
                {
                    case Instruction.ACTION.LAUNCH:
                    case Instruction.ACTION.DOCK:
                    case Instruction.ACTION.RTB:
                        roe = ROE.NONE;
                        break;

                    case Instruction.ACTION.VECTOR:
                        roe = ROE.SELF_DEFENSIVE;
                        if (element_index > 1)
                            roe = ROE.DEFENSIVE;
                        break;

                    case Instruction.ACTION.DEFEND:
                    case Instruction.ACTION.ESCORT:
                        roe = ROE.DEFENSIVE;
                        break;

                    case Instruction.ACTION.INTERCEPT:
                        if (element_index > 1)
                            roe = ROE.DEFENSIVE;
                        else
                            roe = ROE.DIRECTED;
                        break;

                    case Instruction.ACTION.RECON:
                    case Instruction.ACTION.STRIKE:
                    case Instruction.ACTION.ASSAULT:
                        roe = ROE.DIRECTED;
                        break;

                    case Instruction.ACTION.PATROL:
                    case Instruction.ACTION.SWEEP:
                        roe = ROE.FLEXIBLE;
                        break;

                    default: break;
                }

                if (order == Instruction.ACTION.STRIKE)
                {
                    ship.SetSensorMode(Sensor.Mode.GM);

                    if (IsStrikeComplete(navpt))
                    {
                        ship.SetNavptStatus(navpt, Instruction.STATUS.COMPLETE);
                    }
                }

                else if (order == Instruction.ACTION.ASSAULT)
                {
                    if (ship.GetSensorMode() == Sensor.Mode.GM)
                        ship.SetSensorMode(Sensor.Mode.STD);

                    if (IsStrikeComplete(navpt))
                    {
                        ship.SetNavptStatus(navpt, Instruction.STATUS.COMPLETE);
                    }
                }

                else
                {
                    if (ship.GetSensorMode() == Sensor.Mode.GM)
                        ship.SetSensorMode(Sensor.Mode.STD);
                }
            }

            switch (roe)
            {
                case ROE.NONE: ship.SetDirectorInfo(localeManager.GetText("ai.none")); break;
                case ROE.SELF_DEFENSIVE: ship.SetDirectorInfo(localeManager.GetText("ai.self-defensive")); break;
                case ROE.DEFENSIVE: ship.SetDirectorInfo(localeManager.GetText("ai.defensive")); break;
                case ROE.DIRECTED: ship.SetDirectorInfo(localeManager.GetText("ai.directed")); break;
                case ROE.FLEXIBLE: ship.SetDirectorInfo(localeManager.GetText("ai.flexible")); break;
                default: ship.SetDirectorInfo(localeManager.GetText("ai.default")); break;
            }

            return (navpt != null);
        }
        protected virtual bool IsStrikeComplete(Instruction instr = null)
        {
            // wingmen can not call a halt to a strike:
            if (ship == null || element_index > 1)
                return false;

            // if there's nothing to shoot at, we must be done:
            if (instr == null || instr.GetTarget() == null || instr.GetTarget().Life() == 0 ||
                    instr.GetTarget().Type() != (int)SimObject.TYPES.SIM_SHIP)
                return true;

            // break off strike only when ALL weapons are expended:
            // (remember to check all relevant wingmen)
            Element element = ship.GetElement();
            Ship target = (Ship)instr.GetTarget();

            if (element == null)
                return true;

            for (int i = 0; i < element.NumShips(); i++)
            {
                Ship s = element.GetShip(i + 1);

                if (s == null || s.Integrity() < 25) // || (s.Location() - target.Location()).length() > 250e3)
                    continue;

                foreach (WeaponGroup w in s.Weapons())
                {
                    if (w.Ammo() != 0 && w.CanTarget(target.Class()))
                    {
                        foreach (Weapon weapon in w.GetWeapons())
                        {
                            if (weapon.Status() > ShipSystem.STATUS.CRITICAL)
                                return false;
                        }
                    }
                }
            }

            // still here?  we must be done!
            return true;
        }

        protected override void SelectTarget()
        {
            base.SelectTarget();

            SimObject target = ship_ai.GetTarget();

            if (target != null && (target.Type() == (int)SimObject.TYPES.SIM_SHIP) &&
                    (Game.GameTime() - secondary_selection_time) > THREAT_REACTION_TIME)
            {
                SelectSecondaryForTarget((Ship)target);
                secondary_selection_time = (uint)Game.GameTime();
            }
        }

        protected override void SelectTargetDirected(Ship tgt = null)
        {
            Ship potential_target = tgt;

            if (tgt == null)
            {
                // try to target one of the element's objectives
                // (if it shows up in the contact list)

                Element elem = ship.GetElement();

                if (elem != null)
                {
                    Instruction objective = elem.GetTargetObjective();

                    if (objective != null)
                    {
                        SimObject obj_sim_obj = objective.GetTarget();
                        Ship obj_tgt = null;

                        if (obj_sim_obj != null && obj_sim_obj.Type() == (int)SimObject.TYPES.SIM_SHIP)
                            obj_tgt = (Ship)obj_sim_obj;

                        if (obj_tgt != null && ship.FindContact(obj_tgt) != null)
                            potential_target = obj_tgt;
                    }
                }
            }

            if (!CanTarget(potential_target))
                potential_target = null;

            ship_ai.SetTarget(potential_target);
            SelectSecondaryForTarget(potential_target);
        }
        protected override void SelectTargetOpportunity()
        {
            // NON-COMBATANTS do not pick targets of opportunity:
            if (ship.GetIFF() == 0)
                return;

            Ship potential_target = null;
            Shot current_shot_target = null;

            // pick the closest combatant ship with a different IFF code:
            double target_dist = 1.0e15;
            double min_dist = 5.0e3;

            // FIGHTERS are primarily anti-air platforms, but may
            // also attack smaller starships:

            Ship ward = null;
            if (element_index > 1)
                ward = ship.GetLeader();

            // commit range for patrol/sweep is 80 Km
            // (about 2 minutes for a fighter at max speed)
            if (roe == ROE.FLEXIBLE || roe == ROE.AGRESSIVE)
                target_dist = ship.Design().commit_range;

            if (roe < ROE.FLEXIBLE)
                target_dist = 0.5 * ship.Design().commit_range;

            CLASSIFICATION class_limit = CLASSIFICATION.LCA;

            if (ship.Class() == CLASSIFICATION.ATTACK)
                class_limit = CLASSIFICATION.DESTROYER;

            foreach (Contact contact in ship.ContactList())
            {
                Ship c_ship = contact.GetShip();
                Shot c_shot = contact.GetShot();
                int c_iff = contact.GetIFF(ship);
                bool rogue = false;

                if (c_ship != null)
                    rogue = c_ship.IsRogue();

                if (!rogue && (c_iff <= 0 || c_iff == ship.GetIFF() || c_iff == 1000))
                    continue;

                // reasonable target?
                if (c_ship != null && c_ship.Class() <= class_limit && !c_ship.InTransition())
                {
                    if (!rogue)
                    {
                        SimObject ttgt = c_ship.GetTarget();

                        // if we are self-defensive, is this contact engaging us?
                        if (roe == ROE.SELF_DEFENSIVE && ttgt != ship)
                            continue;

                        // if we are defending, is this contact engaging us or our ward?
                        if (roe == ROE.DEFENSIVE && ttgt != ship && ttgt != ward)
                            continue;
                    }

                    // found an enemy, check distance:
                    double dist = (ship.Location() - c_ship.Location()).Length;

                    if (dist < 0.75 * target_dist)
                    {

                        // if on patrol, check target distance from navpoint:
                        if (roe == ROE.FLEXIBLE && navpt != null)
                        {
                            double ndist = (navpt.Location().OtherHand() - c_ship.Location()).Length;
                            if (ndist > 80e3)
                                continue;
                        }

                        potential_target = c_ship;
                        target_dist = dist;
                    }
                }

                else if (c_shot != null && c_shot.IsDrone())
                {
                    // found an enemy shot, do we have enough time to engage?
                    if (c_shot.GetEta() < 10)
                        continue;

                    // found an enemy shot, check distance:
                    double dist = (ship.Location() - c_shot.Location()).Length;

                    if (current_shot_target == null)
                    {
                        current_shot_target = c_shot;
                        target_dist = dist;
                    }

                    // is this shot a better target than the one we've found?
                    else
                    {
                        Ship ward2 = ship_ai.GetWard();

                        if ((c_shot.IsTracking(ward2) && !current_shot_target.IsTracking(ward2)) ||
                                (dist < target_dist))
                        {
                            current_shot_target = c_shot;
                            target_dist = dist;
                        }
                    }
                }
            }

            if (current_shot_target != null)
            {
                ship_ai.SetTarget(current_shot_target);
            }
            else
            {
                ship_ai.SetTarget(potential_target);
                SelectSecondaryForTarget(potential_target);
            }
        }

        protected override void FindFormationSlot(Instruction.FORMATION formation)
        {
            // find the formation delta:
            int s = element_index - 1;
            Point delta = new Point(5 * s, 0, -5 * s);

            // diamond:
            if (formation == Instruction.FORMATION.DIAMOND)
            {
                switch (element_index)
                {
                    case 2: delta = new Point(12, -1, -10); break;
                    case 3: delta = new Point(-12, -1, -10); break;
                    case 4: delta = new Point(0, -2, -20); break;
                }
            }

            // spread:
            if (formation == Instruction.FORMATION.SPREAD)
            {
                switch (element_index)
                {
                    case 2: delta = new Point(15, 0, 0); break;
                    case 3: delta = new Point(-15, 0, 0); break;
                    case 4: delta = new Point(-30, 0, 0); break;
                }
            }

            // box:
            if (formation == Instruction.FORMATION.BOX)
            {
                switch (element_index)
                {
                    case 2: delta = new Point(15, 0, 0); break;
                    case 3: delta = new Point(0, -2, -20); break;
                    case 4: delta = new Point(15, -2, -20); break;
                }
            }

            // trail:
            if (formation == Instruction.FORMATION.TRAIL)
            {
                delta = new Point(0, s, -20 * s);
            }

            ship_ai.SetFormationDelta(delta * (float)ship.Radius() * 2);
        }

        protected override void FindThreat()
        {
            // pick the closest contact on Threat Warning System:
            Ship threat_ship = null;
            Shot threat_missile = null;
            double threat_dist = 1e9;

            foreach (Contact contact in ship.ContactList())
            {
                if (contact.Threat(ship) &&
                        (Game.GameTime() - contact.AcquisitionTime()) > THREAT_REACTION_TIME)
                {

                    double rng = contact.Range(ship);

                    if (contact.GetShot() != null)
                    {
                        threat_missile = contact.GetShot();
                    }

                    else if (rng < threat_dist && contact.GetShip() != null)
                    {
                        Ship candidate = contact.GetShip();

                        if (candidate.InTransition())
                            continue;

                        if (candidate.IsStarship() && rng < 50e3)
                        {
                            threat_ship = candidate;
                            threat_dist = rng;
                        }

                        else if (candidate.IsDropship() && rng < 25e3)
                        {
                            threat_ship = candidate;
                            threat_dist = rng;
                        }

                        // static and ground units:
                        else if (rng < 30e3)
                        {
                            threat_ship = candidate;
                            threat_dist = rng;
                        }
                    }
                }
            }

            ship_ai.SetThreat(threat_ship);
            ship_ai.SetThreatMissile(threat_missile);
        }

        protected virtual void SelectSecondaryForTarget(Ship tgt)
        {
            if (tgt != null)
            {
                int wix = WINCHESTER_FIGHTER;

                if (tgt.IsGroundUnit()) wix = WINCHESTER_STRIKE;
                else if (tgt.IsStatic()) wix = WINCHESTER_STATIC;
                else if (tgt.IsStarship()) wix = WINCHESTER_ASSAULT;

                WeaponGroup best = null;
                List<WeaponGroup> weps = new List<WeaponGroup>();

                if (ListSecondariesForTarget(tgt, weps) != 0)
                {
                    winchester[wix] = false;

                    // select best weapon for the job:
                    double range = (ship.Location() - tgt.Location()).Length;
                    double best_range = 0;
                    double best_damage = 0;

                    foreach (WeaponGroup w in weps)
                    {
                        if (best == null)
                        {
                            best = w;

                            WeaponDesign d = best.GetDesign();
                            best_range = d.max_range;
                            best_damage = d.damage * d.ripple_count;

                            if (best_range < range)
                                best = null;
                        }

                        else
                        {
                            WeaponDesign d = w.GetDesign();
                            double w_range = d.max_range;
                            double w_damage = d.damage * d.ripple_count;

                            if (w_range > range)
                            {
                                if (w_range < best_range || w_damage > best_damage)
                                    best = w;
                            }
                        }
                    }

                    // now cycle weapons until you pick the best one:
                    WeaponGroup current_missile = ship.GetSecondaryGroup();

                    if (current_missile != null && best != null && current_missile != best)
                    {
                        ship.CycleSecondary();
                        WeaponGroup m = ship.GetSecondaryGroup();

                        while (m != current_missile && m != best)
                        {
                            ship.CycleSecondary();
                            m = ship.GetSecondaryGroup();
                        }
                    }
                }

                else
                {
                    winchester[wix] = true;

                    // if we have NO weapons that can hit this target,
                    // just drop it:

                    Weapon primary = ship.GetPrimary();
                    if (primary == null || !primary.CanTarget(tgt.Class()))
                    {
                        ship_ai.DropTarget(3);
                        ship.DropTarget();
                    }
                }

                if (tgt.IsGroundUnit())
                    ship.SetSensorMode(Sensor.Mode.GM);

                else if (ship.GetSensorMode() == Sensor.Mode.GM)
                    ship.SetSensorMode(Sensor.Mode.STD);
            }
        }

        protected virtual int ListSecondariesForTarget(Ship tgt, List<WeaponGroup> weps)
        {
            weps.Clear();

            if (tgt != null)
            {
                foreach (WeaponGroup w in ship.Weapons())
                {
                    if (w.Ammo() != 0 && w.CanTarget(tgt.Class()))
                        weps.Add(w);
                }
            }

            return weps.Count;
        }


        protected bool[] winchester = new bool[4];
        protected uint THREAT_REACTION_TIME;
        protected uint secondary_selection_time;
        protected int ai_level;

        private const int WINCHESTER_FIGHTER = 0;
        private const int WINCHESTER_ASSAULT = 1;
        private const int WINCHESTER_STRIKE = 2;
        private const int WINCHESTER_STATIC = 3;
        protected ContentBundle localeManager;
    }
}