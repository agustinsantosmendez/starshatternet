﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      FlightPlanner.h/FlightPlanner.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Flight Planning class for creating navpoint routes for fighter elements.
    Used both by the CarrierAI class and the Flight Dialog.
*/
using DigitalRune.Mathematics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.AI
{
    public class FlightPlanner
    {

        public FlightPlanner(Ship s)
        {
            ship = s;
            sim = Sim.GetSim();

            patrol_range = (float)(250e3 + 10e3 * (int)RandomHelper.Random(0, 8.9));
        }

        // ~FlightPlanner();

        public virtual void CreatePatrolRoute(Element elem, int index)
        {
            RLoc rloc = new RLoc();
            Point dummy = new Point(0, 0, 0);
            Point loc = ship.Location();
            double zone = ship.CompassHeading();
            Instruction instr = null;

            if (ship.IsAirborne())
                loc.Y += 8e3f;
            else
                loc.Y += 1e3f;

            loc = loc.OtherHand();

            if (index > 2)
                zone += MathHelper.ToRadians(170);
            else if (index > 1)
                zone += -MathHelper.ToRadians(90);
            else if (index > 0)
                zone += MathHelper.ToRadians(90);

            rloc.SetReferenceLoc(null);
            rloc.SetBaseLocation(loc);
            rloc.SetDistance(30e3);
            rloc.SetDistanceVar(0);
            rloc.SetAzimuth(MathHelper.ToRadians(-10) + zone);
            rloc.SetAzimuthVar(0);

            instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.VECTOR);
            instr.SetSpeed(750);
            instr.RLoc = rloc;

            elem.AddNavPoint(instr);

            rloc.SetReferenceLoc(null);
            rloc.SetBaseLocation(loc);
            if (ship.IsAirborne())
                rloc.SetDistance(140e3);
            else
                rloc.SetDistance(220e3);
            rloc.SetDistanceVar(50e3);
            rloc.SetAzimuth(MathHelper.ToRadians(-20) + zone);
            rloc.SetAzimuthVar(MathHelper.ToRadians(15));

            instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.PATROL);
            instr.SetSpeed(500);
            instr.RLoc = rloc;

            elem.AddNavPoint(instr);

            rloc.SetReferenceLoc(instr.GetRLoc());
            rloc.SetDistance(120e3);
            rloc.SetDistanceVar(30e3);
            rloc.SetAzimuth(MathHelper.ToRadians(60) + zone);
            rloc.SetAzimuthVar(MathHelper.ToRadians(20));

            instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.PATROL);
            instr.SetSpeed(350);
            instr.RLoc = rloc;

            elem.AddNavPoint(instr);

            rloc.SetReferenceLoc(instr.GetRLoc());
            rloc.SetDistance(120e3);
            rloc.SetDistanceVar(30e3);
            rloc.SetAzimuth(MathHelper.ToRadians(120) + zone);
            rloc.SetAzimuthVar(MathHelper.ToRadians(20));

            instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.PATROL);
            instr.SetSpeed(350);
            instr.RLoc = rloc;

            elem.AddNavPoint(instr);

            rloc.SetReferenceLoc(null);
            rloc.SetBaseLocation(loc);
            rloc.SetDistance(40e3);
            rloc.SetDistanceVar(0);
            rloc.SetAzimuth(MathHelper.ToRadians(180) + ship.CompassHeading());
            rloc.SetAzimuthVar(MathHelper.ToRadians(0));

            instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.RTB);
            instr.SetSpeed(500);
            instr.RLoc = rloc;

            elem.AddNavPoint(instr);
        }
        public virtual void CreateStrikeRoute(Element elem, Element target)
        {
            if (elem == null) return;

            RLoc rloc = new RLoc();
            Point dummy = new Point(0, 0, 0);
            Point loc = ship.Location();
            double head = ship.CompassHeading() + MathHelper.ToRadians(15);
            double dist = 30e3;
            Instruction instr = null;
            Ship tgt_ship = null;

            if (ship.IsAirborne())
                loc += ship.Cam().vup() * 8e3f;
            else
                loc += ship.Cam().vup() * 1e3f;

            loc = loc.OtherHand();

            if (target != null)
                tgt_ship = target.GetShip(1);

            if (tgt_ship != null)
            {
                double range = (tgt_ship.Location() - ship.Location()).Length;

                if (range < 100e3)
                    dist = 20e3;
            }

            rloc.SetReferenceLoc(null);
            rloc.SetBaseLocation(loc);
            rloc.SetDistance(dist);
            rloc.SetDistanceVar(0);
            rloc.SetAzimuth(head);
            rloc.SetAzimuthVar(MathHelper.ToRadians(2));

            instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.VECTOR);
            instr.SetSpeed(750);
            instr.RLoc = rloc;

            elem.AddNavPoint(instr);

            if (tgt_ship != null)
            {
                tgt_ship = target.GetShip(1);
                Point tgt = tgt_ship.Location() + tgt_ship.Velocity() * 10;
                Point mid = ship.Location() + (tgt - ship.Location()) * 0.5f;
                double beam = tgt_ship.CompassHeading() + MathHelper.ToRadians(90);

                if (tgt_ship.IsAirborne())
                    tgt += tgt_ship.Cam().vup() * 8e3f;
                else
                    tgt += tgt_ship.Cam().vup() * 1e3f;

                tgt = tgt.OtherHand();
                mid = mid.OtherHand();

                if (tgt_ship != null && tgt_ship.IsStarship())
                {
                    rloc.SetReferenceLoc(null);
                    rloc.SetBaseLocation(tgt);
                    rloc.SetDistance(60e3);
                    rloc.SetDistanceVar(5e3);
                    rloc.SetAzimuth(beam);
                    rloc.SetAzimuthVar(MathHelper.ToRadians(5));

                    instr = new Instruction(tgt_ship.GetRegion(), dummy, Instruction.ACTION.ASSAULT);
                    instr.SetSpeed(750);
                    instr.RLoc = rloc;
                    instr.SetTarget(target.Name());
                    instr.SetFormation(Instruction.FORMATION.TRAIL);

                    elem.AddNavPoint(instr);
                }

                if (tgt_ship != null && tgt_ship.IsStatic())
                {
                    rloc.SetReferenceLoc(null);
                    rloc.SetBaseLocation(mid);
                    rloc.SetDistance(60e3);
                    rloc.SetDistanceVar(5e3);
                    rloc.SetAzimuth(beam);
                    rloc.SetAzimuthVar(MathHelper.ToRadians(15));

                    instr = new Instruction(tgt_ship.GetRegion(), dummy, Instruction.ACTION.VECTOR);
                    instr.SetSpeed(750);
                    instr.RLoc = rloc;

                    elem.AddNavPoint(instr);

                    rloc.SetReferenceLoc(null);
                    rloc.SetBaseLocation(tgt);
                    rloc.SetDistance(40e3);
                    rloc.SetDistanceVar(5e3);
                    rloc.SetAzimuth(beam);
                    rloc.SetAzimuthVar(MathHelper.ToRadians(5));

                    Instruction.ACTION action = Instruction.ACTION.ASSAULT;

                    if (tgt_ship.IsGroundUnit())
                        action = Instruction.ACTION.STRIKE;

                    instr = new Instruction(tgt_ship.GetRegion(), dummy, action);
                    instr.SetSpeed(750);
                    instr.RLoc = rloc;
                    instr.SetTarget(target.Name());
                    instr.SetFormation(Instruction.FORMATION.TRAIL);

                    elem.AddNavPoint(instr);
                }

                else if (tgt_ship != null && tgt_ship.IsDropship())
                {
                    rloc.SetReferenceLoc(null);
                    rloc.SetBaseLocation(tgt);
                    rloc.SetDistance(60e3);
                    rloc.SetDistanceVar(5e3);
                    rloc.SetAzimuth(tgt_ship.CompassHeading());
                    rloc.SetAzimuthVar(MathHelper.ToRadians(20));

                    instr = new Instruction(tgt_ship.GetRegion(), dummy, Instruction.ACTION.INTERCEPT);
                    instr.SetSpeed(750);
                    instr.RLoc = rloc;
                    instr.SetTarget(target.Name());
                    instr.SetFormation(Instruction.FORMATION.SPREAD);

                    elem.AddNavPoint(instr);
                }
            }

            rloc.SetReferenceLoc(null);
            rloc.SetBaseLocation(loc);
            rloc.SetDistance(40e3);
            rloc.SetDistanceVar(0);
            rloc.SetAzimuth(MathHelper.ToRadians(180) + ship.CompassHeading());
            rloc.SetAzimuthVar(MathHelper.ToRadians(0));

            instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.RTB);
            instr.SetSpeed(500);
            instr.RLoc = rloc;

            elem.AddNavPoint(instr);
        }
        public virtual void CreateEscortRoute(Element elem, Element ward)
        {
            if (elem == null) return;

            RLoc rloc = new RLoc();
            Point dummy = new Point(0, 0, 0);
            Point loc = ship.Location();
            double head = ship.CompassHeading();
            Instruction instr = null;

            if (ship.IsAirborne())
                loc += ship.Cam().vup() * 8e3f;
            else
                loc += ship.Cam().vup() * 1e3f;

            loc = loc.OtherHand();

            rloc.SetReferenceLoc(null);
            rloc.SetBaseLocation(loc);
            rloc.SetDistance(30e3);
            rloc.SetDistanceVar(0);
            rloc.SetAzimuth(head);
            rloc.SetAzimuthVar(0);

            instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.VECTOR);
            instr.SetSpeed(750);
            instr.RLoc = rloc;

            elem.AddNavPoint(instr);

            if (ward != null && ward.GetShip(1) != null)
            {
                // follow ward's flight plan:
                if (ward.GetFlightPlan().Count != 0)
                {

                    foreach (Instruction ward_instr in ward.GetFlightPlan())
                    {

                        if ((int)ward_instr.Action() != (int)Instruction.ACTION.RTB)
                        {
                            rloc.SetReferenceLoc(ward_instr.GetRLoc());
                            rloc.SetDistance(25e3);
                            rloc.SetDistanceVar(5e3);
                            rloc.SetAzimuth(0);
                            rloc.SetAzimuthVar(MathHelper.ToRadians(90));

                            instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.ESCORT);
                            instr.SetSpeed(350);
                            instr.RLoc = rloc;
                            instr.SetTarget(ward.Name());

                            elem.AddNavPoint(instr);
                        }
                    }
                }

                // if ward has no flight plan, just go to a point nearby:
                else
                {
                    rloc.SetReferenceLoc(null);
                    rloc.SetBaseLocation(ward.GetShip(1).Location());
                    rloc.SetDistance(25e3);
                    rloc.SetDistanceVar(5e3);
                    rloc.SetAzimuth(0);
                    rloc.SetAzimuthVar(MathHelper.ToRadians(90));

                    instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.DEFEND);
                    instr.SetSpeed(500);
                    instr.RLoc = rloc;
                    instr.SetTarget(ward.Name());
                    instr.SetHoldTime(15 * 60); // fifteen minutes

                    elem.AddNavPoint(instr);
                }
            }

            rloc.SetReferenceLoc(null);
            rloc.SetBaseLocation(loc);
            rloc.SetDistance(40e3);
            rloc.SetDistanceVar(0);
            rloc.SetAzimuth(MathHelper.ToRadians(180) + ship.CompassHeading());
            rloc.SetAzimuthVar(MathHelper.ToRadians(0));

            instr = new Instruction(ship.GetRegion(), dummy, Instruction.ACTION.RTB);
            instr.SetSpeed(500);
            instr.RLoc = rloc;

            elem.AddNavPoint(instr);
        }

        public Sim sim;
        public Ship ship;
        public float patrol_range;
    }
}