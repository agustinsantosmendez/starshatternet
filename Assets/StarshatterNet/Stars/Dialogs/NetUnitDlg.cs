﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetUnitDlg.h/NetUnitDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Multiplayer Unit Selection Dialog Active Window class
*/
using System;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class NetUnitDlg : FormWindow
    {
        public NetUnitDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
        base(s, 0, 0, s.Width(), s.Height(), c, p, "NetUnitDlg")
        {
            throw new NotImplementedException();
        }
        //public virtual ~NetUnitDlg();

        public override void RegisterControls() { throw new NotImplementedException(); }
        public override void Show() { throw new NotImplementedException(); }
        public virtual void ExecFrame() { throw new NotImplementedException(); }

        // Operations:
        public virtual void OnSelect(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnUnit(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnMap(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnUnMap(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnBan(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnBanConfirm(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnApply(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnCancel(AWEvent evnt) { throw new NotImplementedException(); }

        public virtual void ExecLobbyFrame() { throw new NotImplementedException(); }

        public virtual bool GetHostMode() { return host_mode; }
        public virtual void SetHostMode(bool h) { host_mode = h; }


        protected virtual void GetAvailable() { throw new NotImplementedException(); }
        protected virtual void GetUnits() { throw new NotImplementedException(); }
        protected virtual void GetChat() { throw new NotImplementedException(); }
        protected virtual void SendChat(string msg) { throw new NotImplementedException(); }
        protected virtual void CheckUnitMapping() { throw new NotImplementedException(); }

        protected MenuScreen manager;

        protected ListBox lst_players;
        protected ListBox lst_units;
        protected ListBox lst_chat;
        protected EditBox edt_chat;

        protected Button btn_select;
        protected Button btn_map;
        protected Button btn_unmap;
        protected Button btn_ban;
        protected Button btn_apply;
        protected Button btn_cancel;

        //protected NetLobby net_lobby;

        protected int last_chat;
        protected int unit_index;
        protected bool host_mode;
    }
}