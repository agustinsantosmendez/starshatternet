﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MsnElemDlg.h/MsnElemDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Editor Element Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;
using StarshatterNet.Gen.Misc;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MsnElemDlg : FormWindow
    {
        public MsnElemDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
              base(s, 0, 0, s.Width(), s.Height(), c, p, "MsnElemDlg")
        {
            manager = mgr;
            btn_accept = null; btn_cancel = null; edt_name = null; cmb_class = null; cmb_design = null;
            edt_size = null; edt_iff = null; cmb_role = null; cmb_region = null; cmb_skin = null;
            edt_loc_x = null; edt_loc_y = null; edt_loc_z = null; cmb_heading = null; edt_hold_time = null;
            btn_player = null; btn_playable = null; btn_alert = null; btn_command_ai = null;
            edt_respawns = null; cmb_commander = null; cmb_carrier = null; cmb_squadron = null;
            cmb_intel = null; cmb_loadout = null; cmb_objective = null; cmb_target = null;
            mission = null; elem = null; exit_latch = true;

            Init(def);
        }
        //public virtual ~MsnElemDlg();

        public override void RegisterControls()
        {
            btn_accept = (Button)FindControl(1);
            if (btn_accept != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_accept, OnAccept);

            btn_cancel = (Button)FindControl(2);
            if (btn_accept != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_cancel, OnCancel);

            edt_name = (EditBox)FindControl(201);
            cmb_class = (ComboBox)FindControl(202);
            cmb_design = (ComboBox)FindControl(203);
            cmb_skin = (ComboBox)FindControl(213);
            edt_size = (EditBox)FindControl(204);
            edt_iff = (EditBox)FindControl(205);
            cmb_role = (ComboBox)FindControl(206);
            cmb_region = (ComboBox)FindControl(207);
            edt_loc_x = (EditBox)FindControl(208);
            edt_loc_y = (EditBox)FindControl(209);
            edt_loc_z = (EditBox)FindControl(210);
            cmb_heading = (ComboBox)FindControl(211);
            edt_hold_time = (EditBox)FindControl(212);

            btn_player = (Button)FindControl(221);
            btn_alert = (Button)FindControl(222);
            btn_playable = (Button)FindControl(223);
            btn_command_ai = (Button)FindControl(224);
            edt_respawns = (EditBox)FindControl(225);
            cmb_commander = (ComboBox)FindControl(226);
            cmb_carrier = (ComboBox)FindControl(227);
            cmb_squadron = (ComboBox)FindControl(228);
            cmb_intel = (ComboBox)FindControl(229);
            cmb_loadout = (ComboBox)FindControl(230);
            cmb_objective = (ComboBox)FindControl(231);
            cmb_target = (ComboBox)FindControl(232);

            if (cmb_class != null)
                REGISTER_CLIENT(ETD.EID_SELECT, cmb_class, OnClassSelect);

            if (cmb_design != null)
                REGISTER_CLIENT(ETD.EID_SELECT, cmb_design, OnDesignSelect);

            if (cmb_objective != null)
                REGISTER_CLIENT(ETD.EID_SELECT, cmb_objective, OnObjectiveSelect);

            if (edt_iff != null)
                REGISTER_CLIENT(ETD.EID_KILL_FOCUS, edt_iff, OnIFFChange);
        }
        public override void Show()
        {
            base.Show();

            if (elem == null) return;

            SimElements.CLASSIFICATION current_class = 0;

            if (cmb_class != null)
            {
                cmb_class.ClearItems();

                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.DRONE));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.FIGHTER));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.ATTACK));

                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.LCA));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.COURIER));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.CARGO));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.CORVETTE));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.FREIGHTER));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.FRIGATE));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.DESTROYER));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.CRUISER));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.BATTLESHIP));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.CARRIER));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.SWACS));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.DREADNAUGHT));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.STATION));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.FARCASTER));

                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.MINE));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.COMSAT));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.DEFSAT));

                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.BUILDING));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.FACTORY));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.SAM));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.EWR));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.C3I));
                cmb_class.AddItem(Ship.ClassName(SimElements.CLASSIFICATION.STARBASE));

                ShipDesign design = elem.GetDesign();

                for (int i = 0; i < cmb_class.NumItems(); i++)
                {
                    string cname = cmb_class.GetItem(i);
                    SimElements.CLASSIFICATION classid = Ship.ClassForName(cname);

                    if (design != null && classid == design.type)
                    {
                        cmb_class.SetSelection(i);
                        current_class = classid;
                        break;
                    }
                }
            }

            if (cmb_design != null)
            {
                OnClassSelect(null, null);
                OnDesignSelect(null, null);
            }

            if (cmb_role != null)
            {
                cmb_role.ClearItems();

                for (Mission.TYPE i = Mission.TYPE.PATROL; i <= Mission.TYPE.OTHER; i++)
                {
                    cmb_role.AddItem(Mission.RoleName(i));

                    if (i == 0)
                        cmb_role.SetSelection(0);

                    else if (elem.MissionRole() == i)
                        cmb_role.SetSelection(cmb_role.NumItems() - 1);
                }
            }

            if (cmb_region != null)
            {
                cmb_region.ClearItems();

                if (mission != null)
                {
                    StarSystem sys = mission.GetStarSystem();
                    if (sys != null)
                    {
                        List<OrbitalRegion> regions = new List<OrbitalRegion>();
                        regions.AddRange(sys.AllRegions());
                        regions.Sort();

                        foreach (OrbitalRegion region in regions)
                        {
                            cmb_region.AddItem(region.Name());

                            if (elem.Region() == region.Name())
                                cmb_region.SetSelection(cmb_region.NumItems() - 1);
                        }
                    }
                }
            }

            string buf = null;

            if (edt_name != null) edt_name.SetText(elem.Name());

            buf = string.Format("{0}", elem.Count());
            if (edt_size != null) edt_size.SetText(buf);

            buf = string.Format("{0}", elem.GetIFF());
            if (edt_iff != null) edt_iff.SetText(buf);

            buf = string.Format("{0}", (int)elem.Location().X / 1000);
            if (edt_loc_x != null) edt_loc_x.SetText(buf);

            buf = string.Format("{0}", (int)elem.Location().Y / 1000);
            if (edt_loc_y != null) edt_loc_y.SetText(buf);

            buf = string.Format("{0}", (int)elem.Location().Z / 1000);
            if (edt_loc_z != null) edt_loc_z.SetText(buf);

            buf = string.Format("{0}", elem.RespawnCount());
            if (edt_respawns != null) edt_respawns.SetText(buf);

            buf = string.Format("{0}", elem.HoldTime());
            if (edt_hold_time != null) edt_hold_time.SetText(buf);

            if (btn_player != null) btn_player.SetButtonState(elem.Player() > 0 ? (short)1 : (short)0);
            if (btn_playable != null) btn_playable.SetButtonState(elem.IsPlayable() ? (short)1 : (short)0);
            if (btn_alert != null) btn_alert.SetButtonState(elem.IsAlert() ? (short)1 : (short)0);
            if (btn_command_ai != null) btn_command_ai.SetButtonState((short)elem.CommandAI());

            UpdateTeamInfo();

            if (cmb_intel != null)
            {
                cmb_intel.ClearItems();

                for (Intel.INTEL_TYPE i = Intel.INTEL_TYPE.RESERVE; i < Intel.INTEL_TYPE.TRACKED; i++)
                {
                    cmb_intel.AddItem(Intel.NameFromIntel(i));

                    if (i == 0)
                        cmb_intel.SetSelection(0);

                    else if (elem.IntelLevel() == i)
                        cmb_intel.SetSelection(cmb_intel.NumItems() - 1);
                }
            }

            Instruction instr = null;
            if (elem.Objectives().Count > 0)
                instr = elem.Objectives()[0];

            if (cmb_objective != null)
            {
                cmb_objective.ClearItems();
                cmb_objective.AddItem("");
                cmb_objective.SetSelection(0);

                for (Instruction.ACTION i = 0; i < Instruction.ACTION.NUM_ACTIONS; i++)
                {
                    cmb_objective.AddItem(Instruction.ActionName(i));

                    if (instr != null && instr.Action() == i)
                        cmb_objective.SetSelection((int)(i + 1));
                }
            }

            if (cmb_target != null)
            {
                OnObjectiveSelect(null, null);
            }

            if (cmb_heading != null)
            {
                double heading = elem.Heading();

                while (heading > 2 * Math.PI)
                    heading -= 2 * Math.PI;

                while (heading < 0)
                    heading += 2 * Math.PI;

                if (heading >= Math.PI / 4 && heading < 3 * Math.PI / 4)
                    cmb_heading.SetSelection(1);

                else if (heading >= 3 * Math.PI / 4 && heading < 5 * Math.PI / 4)
                    cmb_heading.SetSelection(2);

                else if (heading >= 5 * Math.PI / 4 && heading < 7 * Math.PI / 4)
                    cmb_heading.SetSelection(3);

                else
                    cmb_heading.SetSelection(0);
            }

            exit_latch = true;
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                if (!exit_latch && btn_accept != null && btn_accept.IsEnabled())
                    OnAccept(null, null);
            }

            else if (Keyboard.KeyDown(KeyMap.VK_ESCAPE))
            {
                if (!exit_latch)
                    OnCancel(null, null);
            }

            else
            {
                exit_latch = false;
            }
        }


        // Operations:
        public virtual void SetMission(Mission m)
        {
            mission = m;
        }

        public virtual void SetMissionElement(MissionElement e)
        {
            elem = e;
        }
        public virtual void OnAccept(ActiveWindow obj, AWEvent evnt)
        {
            if (mission != null && elem != null)
            {
                string buf = null;
                int val;

                if (edt_name != null)
                {
                    elem.SetName(edt_name.GetText());
                }

                if (edt_size != null)
                {
                    buf = edt_size.GetText();

                    if (!int.TryParse(buf, out val))
                        val = 1;

                    elem.SetCount(val);
                }

                if (edt_iff != null)
                {
                    buf = edt_iff.GetText();

                    if (!int.TryParse(buf, out val))
                        val = 1;

                    elem.SetIFF(val);
                }

                if (edt_loc_x != null && edt_loc_y != null && edt_loc_z != null)
                {
                    Point loc = new Point();

                    buf = edt_loc_x.GetText();

                    if (!int.TryParse(buf, out val))
                        val = 0;

                    loc.X = val * 1000;

                    buf = edt_loc_y.GetText();

                    if (!int.TryParse(buf, out val))
                        val = 0;

                    loc.Y = val * 1000;

                    buf = edt_loc_z.GetText();

                    if (!int.TryParse(buf, out val))
                        val = 0;

                    loc.Z = val * 1000;

                    elem.SetLocation(loc);
                }

                if (edt_respawns != null)
                {
                    buf = edt_respawns.GetText();

                    if (!int.TryParse(buf, out val))
                        val = 0;

                    elem.SetRespawnCount(val);
                }

                if (edt_hold_time != null)
                {
                    buf = edt_hold_time.GetText();

                    if (!int.TryParse(buf, out val))
                        val = 0;

                    elem.SetHoldTime(val);
                }

                if (btn_player != null)
                {
                    if (btn_player.GetButtonState() > 0)
                    {
                        mission.SetPlayer(elem);
                    }
                    else
                    {
                        elem.SetPlayer(0);
                    }
                }

                if (btn_playable != null)
                    elem.SetPlayable(btn_playable.GetButtonState() != 0 ? true : false);

                if (btn_alert != null)
                    elem.SetAlert(btn_alert.GetButtonState() != 0 ? true : false);

                if (btn_command_ai != null)
                    elem.SetCommandAI(btn_command_ai.GetButtonState() != 0 ? 1 : 0);

                if (cmb_design != null)
                {
                    ShipDesign d = ShipDesign.Get(cmb_design.GetSelectedItem());

                    if (d != null)
                    {
                        elem.SetDesign(d);

                        if (cmb_skin != null)
                        {
                            string skin_name = cmb_skin.GetSelectedItem();

                            if (skin_name != Game.GetText("MsnDlg.default"))
                            {
                                elem.SetSkin(d.FindSkin(skin_name));
                            }
                            else
                            {
                                elem.SetSkin(null);
                            }
                        }
                    }
                }

                if (cmb_role != null)
                {
                    elem.SetMissionRole((Mission.TYPE)cmb_role.GetSelectedIndex());
                }

                if (cmb_region != null)
                {
                    elem.SetRegion(cmb_region.GetSelectedItem());

                    if (elem.Player() > 0)
                    {
                        mission.SetRegion(cmb_region.GetSelectedItem());
                    }
                }

                if (cmb_heading != null)
                {
                    switch (cmb_heading.GetSelectedIndex())
                    {
                        default:
                        case 0: elem.SetHeading(0); break;
                        case 1: elem.SetHeading(Math.PI / 2); break;
                        case 2: elem.SetHeading(Math.PI); break;
                        case 3: elem.SetHeading(3 * Math.PI / 2); break;
                    }
                }

                if (cmb_commander != null)
                {
                    elem.SetCommander(cmb_commander.GetSelectedItem());
                }

                if (cmb_squadron != null)
                {
                    elem.SetSquadron(cmb_squadron.GetSelectedItem());
                }

                if (cmb_carrier != null)
                {
                    elem.SetCarrier(cmb_carrier.GetSelectedItem());
                }

                if (cmb_intel != null)
                {
                    elem.SetIntelLevel(Intel.IntelFromName(cmb_intel.GetSelectedItem()));
                }

                if (cmb_loadout != null && cmb_loadout.NumItems() != 0)
                {
                    elem.Loadouts().Destroy();

                    string loadname = cmb_loadout.GetSelectedItem();
                    if (!string.IsNullOrEmpty(loadname))
                    {
                        MissionLoad mload = new MissionLoad(-1, cmb_loadout.GetSelectedItem());
                        elem.Loadouts().Add(mload);
                    }
                }

                if (cmb_objective != null && cmb_target != null)
                {
                    List<Instruction> objectives = elem.Objectives();
                    objectives.Destroy();

                    Instruction.ACTION action = (Instruction.ACTION)(cmb_objective.GetSelectedIndex() - 1);
                    string target = cmb_target.GetSelectedItem();

                    if (action >= Instruction.ACTION.VECTOR)
                    {
                        Instruction obj2 = new Instruction(action, target);
                        objectives.Add(obj2);
                    }
                }

                if (elem.Player() != 0)
                {
                    mission.SetTeam(elem.GetIFF());
                }
            }

            manager.ShowMsnEditDlg();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            manager.ShowMsnEditDlg();
        }
        public virtual void OnClassSelect(ActiveWindow obj, AWEvent evnt)
        {
            if (cmb_class == null || cmb_design == null) return;

            string cname = cmb_class.GetSelectedItem();
            CLASSIFICATION classid = Ship.ClassForName(cname);

            cmb_design.ClearItems();

            List<string> designs = new List<string>();
            ShipDesign.GetDesignList((int)classid, designs);

            if (designs.Count > 0)
            {
                ShipDesign design = elem.GetDesign();
                bool found = false;

                for (int i = 0; i < designs.Count; i++)
                {
                    string dsn = designs[i];
                    cmb_design.AddItem(dsn);

                    if (design != null && string.Equals(dsn, design.name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        cmb_design.SetSelection(i);
                        found = true;
                    }
                }

                if (!found)
                    cmb_design.SetSelection(0);
            }
            else
            {
                cmb_design.AddItem("");
                cmb_design.SetSelection(0);
            }

            OnDesignSelect(null, null);
        }
        public virtual void OnDesignSelect(ActiveWindow obj, AWEvent evnt)
        {
            if (cmb_design == null) return;

            ShipDesign design = null;

            string dname = cmb_design.GetSelectedItem();
            int load_index = 0;

            if (dname != null)
                design = ShipDesign.Get(dname);

            if (cmb_loadout != null)
            {
                cmb_loadout.ClearItems();

                if (design != null)
                {
                    MissionLoad mload = null;

                    if (elem != null && elem.Loadouts().Count > 0)
                        mload = elem.Loadouts()[0];

                    List<ShipLoad> loadouts = design.loadouts;

                    if (loadouts.Count > 0)
                    {
                        for (int i = 0; i < loadouts.Count; i++)
                        {
                            ShipLoad load = loadouts[i];
                            if (!string.IsNullOrEmpty(load.name))
                            {
                                cmb_loadout.AddItem(load.name);

                                if (mload != null && mload.GetName() == load.name)
                                {
                                    load_index = cmb_loadout.NumItems() - 1;
                                }
                            }
                        }
                    }
                }

                if (cmb_loadout.NumItems() < 1)
                    cmb_loadout.AddItem("");

                cmb_loadout.SetSelection(load_index);
            }

            if (cmb_skin != null)
            {
                cmb_skin.ClearItems();

                if (design != null)
                {
                    cmb_skin.AddItem(Game.GetText("MsnDlg.default"));
                    cmb_skin.SetSelection(0);

                    foreach (Skin s in design.skins)
                    {
                        cmb_skin.AddItem(s.Name());

                        if (elem != null && elem.GetSkin() != null && s.Name() == elem.GetSkin().Name())
                        {
                            cmb_skin.SetSelection(cmb_skin.NumItems() - 1);
                        }
                    }
                }
            }
        }
        public virtual void OnObjectiveSelect(ActiveWindow obj, AWEvent evnt)
        {
            if (cmb_objective == null || cmb_target == null) return;

            Instruction instr = null;
            if (elem.Objectives().Count > 0)
                instr = elem.Objectives()[0];

            int objid = cmb_objective.GetSelectedIndex() - 1;

            cmb_target.ClearItems();
            cmb_target.AddItem("");

            if (mission != null)
            {
                foreach (MissionElement e in mission.GetElements())
                {

                    if (e != elem)
                    {
                        bool add = false;

                        if (objid < (int)Instruction.ACTION.PATROL)
                            add = e.GetIFF() == 0 || e.GetIFF() == elem.GetIFF();
                        else
                            add = e.GetIFF() != elem.GetIFF();

                        if (add)
                        {
                            cmb_target.AddItem(e.Name());

                            if (instr != null && string.Equals(instr.TargetName(), e.Name(), StringComparison.InvariantCultureIgnoreCase))
                                cmb_target.SetSelection(cmb_target.NumItems() - 1);
                        }
                    }
                }
            }
        }
        public virtual void OnIFFChange(ActiveWindow obj, AWEvent evnt)
        {
            if (edt_iff != null && elem != null)
            {
                int elem_iff = 0;
                int.TryParse(edt_iff.GetText(), out elem_iff);

                if (elem.GetIFF() == elem_iff)
                    return;

                elem.SetIFF(elem_iff);
            }

            UpdateTeamInfo();

            if (cmb_target != null)
                OnObjectiveSelect(null, null);
        }


        public virtual void UpdateTeamInfo()
        {
            if (cmb_commander != null)
            {
                cmb_commander.ClearItems();
                cmb_commander.AddItem("");
                cmb_commander.SetSelection(0);

                if (mission != null && elem != null)
                {
                    foreach (MissionElement e in mission.GetElements())
                    {
                        if (CanCommand(e, elem))
                        {
                            cmb_commander.AddItem(e.Name());

                            if (elem.Commander() == e.Name())
                                cmb_commander.SetSelection(cmb_commander.NumItems() - 1);
                        }
                    }
                }
            }

            if (cmb_squadron != null)
            {
                cmb_squadron.ClearItems();
                cmb_squadron.AddItem("");
                cmb_squadron.SetSelection(0);

                if (mission != null && elem != null)
                {
                    foreach (MissionElement e in mission.GetElements())
                    {
                        if (e.GetIFF() == elem.GetIFF() && e != elem && e.IsSquadron())
                        {
                            cmb_squadron.AddItem(e.Name());

                            if (elem.Squadron() == e.Name())
                                cmb_squadron.SetSelection(cmb_squadron.NumItems() - 1);
                        }
                    }
                }
            }

            if (cmb_carrier != null)
            {
                cmb_carrier.ClearItems();
                cmb_carrier.AddItem("");
                cmb_carrier.SetSelection(0);

                if (mission != null && elem != null)
                {
                    foreach (MissionElement e in mission.GetElements())
                    {
                        if (e.GetIFF() == elem.GetIFF() && e != elem &&
                            e.GetDesign() != null && e.GetDesign().flight_decks.Count != 0)
                        {
                            cmb_carrier.AddItem(e.Name());

                            if (elem.Carrier() == e.Name())
                                cmb_carrier.SetSelection(cmb_carrier.NumItems() - 1);
                        }
                    }
                }
            }
        }
        public virtual bool CanCommand(MissionElement commander, MissionElement subordinate)
        {
            if (mission != null && commander != null && subordinate != null && commander != subordinate)
            {
                if (commander.GetIFF() != subordinate.GetIFF())
                    return false;

                if (commander.IsSquadron())
                    return false;

                if (commander.Commander().Length == 0)
                    return true;

                if (subordinate.Name() == commander.Commander())
                    return false;

                MissionElement elem = mission.FindElement(commander.Commander());

                if (elem != null)
                    return CanCommand(elem, subordinate);
            }

            return false;
        }


        protected MenuScreen manager;

        protected EditBox edt_name;
        protected ComboBox cmb_class;
        protected ComboBox cmb_design;
        protected ComboBox cmb_skin;
        protected EditBox edt_size;
        protected EditBox edt_iff;
        protected ComboBox cmb_role;
        protected ComboBox cmb_region;
        protected EditBox edt_loc_x;
        protected EditBox edt_loc_y;
        protected EditBox edt_loc_z;
        protected ComboBox cmb_heading;
        protected EditBox edt_hold_time;

        protected Button btn_player;
        protected Button btn_playable;
        protected Button btn_alert;
        protected Button btn_command_ai;
        protected EditBox edt_respawns;
        protected ComboBox cmb_carrier;
        protected ComboBox cmb_squadron;
        protected ComboBox cmb_commander;
        protected ComboBox cmb_intel;
        protected ComboBox cmb_loadout;
        protected ComboBox cmb_objective;
        protected ComboBox cmb_target;

        protected Button btn_accept;
        protected Button btn_cancel;

        protected Mission mission;
        protected MissionElement elem;
        protected bool exit_latch;
    }
}