﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      Physical.h/Physical.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Abstract Physical Object
 */
using System;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.AI;
using StarshatterNet.Stars.Graphics;
using DigitalRune.Mathematics;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars
{
#warning Physical class is still in development and is not recommended for production.
    public class Physical
    {
        public Physical() : this("unknown object", 0)
        {
        }

        public Physical(string n, int t = 0)
        {
            id = id_key++; obj_type = t; rep = null; light = null;
            thrust = 0.0f; drag = 0.0f; lat_thrust = false;
            trans_x = 0.0f; trans_y = 0.0f; trans_z = 0.0f; straight = false;
            roll = 0.0f; pitch = 0.0f; yaw = 0.0f; dr = 0.0f; dp = 0.0f; dy = 0.0f;
            dr_acc = 0.0f; dp_acc = 0.0f; dy_acc = 0.0f;
            dr_drg = 0.0f; dp_drg = 0.0f; dy_drg = 0.0f;
            flight_path_yaw = 0.0f; flight_path_pitch = 0.0f; primary_mass = 0;
            roll_rate = 1.0f; pitch_rate = 1.0f; yaw_rate = 1.0f; shake = 0.0f;
            radius = 0.0f; mass = 1.0f; integrity = 1.0f; life = -1; dir = null;
            g_accel = 0.0f; Do = 0.0f; CL = 0.0f; CD = 0.0f; alpha = 0.0f; stall = 0.0f;
            name = n;
        }
        // ~Physical();

        //int operator ==(const Physical& p) const { return id == p.id; }
        public static bool operator ==(Physical a, Physical b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.id == b.id;
        }

        public static bool operator !=(Physical a, Physical b)
        {
            return !(a == b);
        }
        public override bool Equals(System.Object obj)
        {
            // If parameter cannot be cast to Ship return false:
            Physical p = obj as Physical;
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return id == p.id;
        }

        public bool Equals(Physical p)
        {
            // Return true if the fields match:
            return base.Equals((Physical)p) && id == p.id;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
        // Integration Loop Control:
        public static void SetSubFrameLength(double seconds) { sub_frame = seconds; }
        public static double GetSubFrameLength() { return sub_frame; }

        // operations
        public virtual void ExecFrame(double s)
        {
            Point orig_velocity = Velocity();
            arcade_velocity = new Point();

            // if this object is under direction,
            // but doesn't need subframe accuracy,
            // update the control parameters:
            if (dir != null && !dir.Subframe())
                dir.ExecFrame(s);

            // decrement life before destroying the frame time:
            if (life > 0)
                life -= s;

            // integrate equations
            // using slices no larger
            // than sub_frame:

            double seconds = s;

            while (s > 0.0)
            {
                if (s > sub_frame)
                    seconds = sub_frame;
                else
                    seconds = s;

                // if the director needs subframe accuracy, run it now:
                if (dir != null && dir.Subframe())
                    dir.ExecFrame(seconds);

                if (!straight)
                    AngularFrame(seconds);

                // LINEAR MOVEMENT ----------------------------
                Point pos = cam.Pos();

                // if the object is thrusting,
                // accelerate along the camera normal:
                if (thrust != 0)
                {
                    Point thrustvec = cam.vpn();
                    thrustvec *= ((thrust / mass) * seconds);
                    velocity += thrustvec;
                }

                LinearFrame(seconds);

                // move the position by the (time-frame scaled) velocity:
                pos += velocity * seconds;
                cam.MoveTo(pos);

                s -= seconds;
            }

            alpha = 0.0f;

            // now update the graphic rep and light sources:
            if (rep != null)
            {
                rep.MoveTo(cam.Pos());
                rep.SetOrientation(cam.Orientation());
            }

            if (light != null)
            {
                light.MoveTo(cam.Pos());
            }

            if (!straight)
                CalcFlightPath();

            accel = (Velocity() - orig_velocity) * (1 / seconds);
            if (!accel.IsFinite)
                accel = new Point();
        }
        public virtual void AeroFrame(double s)
        {
            arcade_velocity = new Point();

            // if this object is under direction,
            // but doesn't need subframe accuracy,
            // update the control parameters:
            if (dir != null && !dir.Subframe())
                dir.ExecFrame(s);

            // decrement life before destroying the frame time:
            if (life > 0)
                life -= s;

            // integrate equations
            // using slices no larger
            // than sub_frame:

            double seconds = s;

            while (s > 0.0)
            {
                if (s > sub_frame)
                    seconds = sub_frame;
                else
                    seconds = s;

                // if the director needs subframe accuracy, run it now:
                if (dir != null && dir.Subframe())
                    dir.ExecFrame(seconds);

                AngularFrame(seconds);

                // LINEAR MOVEMENT ----------------------------
                Point pos = cam.Pos();

                // if the object is thrusting,
                // accelerate along the camera normal:
                if (thrust != 0)
                {
                    Point thrustvec = cam.vpn();
                    thrustvec *= ((thrust / mass) * seconds);
                    velocity += thrustvec;
                }

                // AERODYNAMICS ------------------------------

                if (lat_thrust)
                    LinearFrame(seconds);

                // if no thrusters, do constant gravity:
                else if (g_accel > 0)
                    velocity += new Point(0, -g_accel, 0) * seconds;

                // compute alpha, rho, drag, and lift:

                Point vfp = velocity;
                double v = vfp.Normalize();
                double v_2 = 0;
                double rho = GetDensity();
                double lift = 0;

                if (v > 150)
                {
                    v_2 = (v - 150) * (v - 150);

                    Point vfp1 = vfp - cam.vrt() * (vfp * cam.vrt());
                    vfp1.Normalize();

                    double cos_alpha = Point.Dot(vfp1, cam.vpn());

                    if (cos_alpha >= 1)
                    {
                        alpha = 0.0f;
                    }
                    else
                    {
                        alpha = (float)Math.Acos(cos_alpha);
                    }

                    // if flight path is above nose, alpha is negative:
                    if (Point.Dot(vfp1, cam.vup()) > 0)
                        alpha = -alpha;

                    if (alpha <= stall)
                    {
                        lift = CL * alpha * rho * v_2;
                    }
                    else
                    {
                        lift = CL * (2 * stall - alpha) * rho * v_2;
                    }

                    // add lift to velocity:
                    if (Numeric.IsFinite(lift))
                        velocity += cam.vup() * lift * seconds;
                    else
                        lift = 0;

                    // if drag applies, decellerate:
                    double alpha_2 = alpha * alpha;
                    double drag_eff = (drag + (CD * alpha_2)) * rho * v_2;

                    Point vn = velocity;
                    vn.Normalize();

                    velocity += vn * -drag_eff * seconds;
                }
                else
                {
                    velocity *= Math.Exp(-drag * seconds);
                }

                // move the position by the (time-frame scaled) velocity:
                pos += velocity * seconds;
                cam.MoveTo(pos);

                s -= seconds;
            }

            // now update the graphic rep and light sources:
            if (rep != null)
            {
                rep.MoveTo(cam.Pos());
                rep.SetOrientation(cam.Orientation());
            }

            if (light != null)
            {
                light.MoveTo(cam.Pos());
            }
        }
        public virtual void ArcadeFrame(double s)
        {     // if this object is under direction,
              // but doesn't need subframe accuracy,
              // update the control parameters:
            if (dir != null && !dir.Subframe())
                dir.ExecFrame(s);

            // decrement life before destroying the frame time:
            if (life > 0)
                life -= s;

            // integrate equations
            // using slices no larger
            // than sub_frame:

            double seconds = s;

            while (s > 0.0)
            {
                if (s > sub_frame)
                    seconds = sub_frame;
                else
                    seconds = s;

                // if the director needs subframe accuracy, run it now:
                if (dir != null && dir.Subframe())
                    dir.ExecFrame(seconds);

                if (!straight)
                    AngularFrame(seconds);

                Point pos = cam.Pos();

                // ARCADE FLIGHT MODEL:
                // arcade_velocity vector is always in line with heading

                double speed = arcade_velocity.Normalize();
                double bleed = Point.Dot(arcade_velocity, cam.vpn());

                speed *= Math.Pow(bleed, 30);
                arcade_velocity = cam.vpn() * speed;

                if (thrust != 0)
                {
                    Point thrustvec = cam.vpn();
                    thrustvec *= ((thrust / mass) * seconds);
                    arcade_velocity += thrustvec;
                }

                if (drag != 0)
                    arcade_velocity *= Math.Exp(-drag * seconds);

                LinearFrame(seconds);

                // move the position by the (time-frame scaled) velocity:
                pos += arcade_velocity * seconds +
                velocity * seconds;

                cam.MoveTo(pos);

                s -= seconds;
            }

            alpha = 0.0f;

            // now update the graphic rep and light sources:
            if (rep != null)
            {
                rep.MoveTo(cam.Pos());
                rep.SetOrientation(cam.Orientation());
            }

            if (light != null)
            {
                light.MoveTo(cam.Pos());
            }
        }

        public virtual void AngularFrame(double seconds)
        {
            if (!straight)
            {
                dr += (float)(dr_acc * seconds);
                dy += (float)(dy_acc * seconds);
                dp += (float)(dp_acc * seconds);

                dr *= (float)Math.Exp(-dr_drg * seconds);
                dy *= (float)Math.Exp(-dy_drg * seconds);
                dp *= (float)Math.Exp(-dp_drg * seconds);

                roll = (float)(dr * seconds);
                pitch = (float)(dp * seconds);
                yaw = (float)(dy * seconds);

                if (shake > 0.01)
                {
                    vibration = new Point(random(), random(), random());
                    vibration.Normalize();
                    vibration *= (float)(shake * seconds);

                    shake *= (float)Math.Exp(-1.5 * seconds);
                }
                else
                {
                    vibration.X = vibration.Y = vibration.Z = 0.0f;
                    shake = 0.0f;
                }

                cam.Aim(roll, pitch, yaw);
            }
        }
        public virtual void LinearFrame(double seconds)
        {     // deal with lateral thrusters:

            if (trans_x != 0)
            { // side-to-side
                Point transvec = cam.vrt();
                transvec *= ((trans_x / mass) * seconds);

                velocity += transvec;
            }

            if (trans_y != 0)
            { // fore-and-aft
                Point transvec = cam.vpn();
                transvec *= ((trans_y / mass) * seconds);

                velocity += transvec;
            }

            if (trans_z != 0)
            { // up-and-down
                Point transvec = cam.vup();
                transvec *= ((trans_z / mass) * seconds);

                velocity += transvec;
            }

            // if gravity applies, attract:
            if (primary_mass > 0)
            {
                Point g = primary_loc - cam.Pos();
                double r = g.Normalize();

                g *= GRAV * primary_mass / (r * r);

                velocity += g * seconds;
            }

            // constant gravity:
            else if (g_accel > 0)
            {
                velocity += new Point(0, -g_accel, 0) * seconds;
            }

            // if drag applies, decellerate:
            if (drag != 0)
                velocity *= Math.Exp(-drag * seconds);
        }

        public virtual void CalcFlightPath()
        {
            flight_path_yaw = 0.0f;
            flight_path_pitch = 0.0f;

            // transform flight path into camera frame:
            Point flight_path = velocity;
            if (flight_path.Normalize() < 1)
                return;

            Point tmp = flight_path;
            flight_path.X = Point.Dot(tmp, cam.vrt());
            flight_path.Y = Point.Dot(tmp, cam.vup());
            flight_path.Z = Point.Dot(tmp, cam.vpn());

            if (flight_path.Z < 0.1)
                return;

            // first, compute azimuth:
            flight_path_yaw = (float)Math.Atan(flight_path.X / flight_path.Z);
            if (flight_path.Z < 0) flight_path_yaw -= (float)Math.PI;
            if (flight_path_yaw < -Math.PI) flight_path_yaw += (float)(2 * Math.PI);

            // then, rotate path into azimuth frame to compute elevation:
            CameraNode yaw_cam = cam.Clone();
            yaw_cam.Yaw(flight_path_yaw);

            flight_path.X = Point.Dot(tmp, yaw_cam.vrt());
            flight_path.Y = Point.Dot(tmp, yaw_cam.vup());
            flight_path.Z = Point.Dot(tmp, yaw_cam.vpn());

            flight_path_pitch = (float)Math.Atan(flight_path.Y / flight_path.Z);
        }

        public virtual void MoveTo(Point new_loc)
        {
            cam.MoveTo(new_loc);
        }
        public virtual void TranslateBy(Point ref_)
        {
            Point new_loc = cam.Pos() - ref_;
            cam.MoveTo(new_loc);
        }
        public virtual void ApplyForce(Point force)
        {
            velocity += force / mass;
        }
        public virtual void ApplyTorque(Point torque)
        {
            dr += (float)(torque.X / mass);
            dp += (float)(torque.Y / mass);
            dy += (float)(torque.Z / mass);
        }
        public virtual void SetThrust(double t)
        {
            thrust = (float)t;
        }
        public virtual void SetTransX(double t)
        {
            trans_x = (float)t;
        }
        public virtual void SetTransY(double t)
        {
            trans_y = (float)t;
        }
        public virtual void SetTransZ(double t)
        {
            trans_z = (float)t;
        }
        public virtual void SetHeading(double r, double p, double y)
        {
            roll = (float)r;
            pitch = (float)p;
            yaw = (float)y;

            cam.Aim(roll, pitch, yaw);
        }
        public virtual void LookAt(Point dst)
        {
            cam.LookAt(dst);
        }
        public virtual void ApplyRoll(double roll_acc)
        {
            if (roll_acc > 1) roll_acc = 1;
            else if (roll_acc < -1) roll_acc = -1;

            dr_acc = (float)roll_acc * roll_rate;
        }
        public virtual void ApplyPitch(double pitch_acc)
        {
            if (pitch_acc > 1) pitch_acc = 1;
            else if (pitch_acc < -1) pitch_acc = -1;

            dp_acc = (float)pitch_acc * pitch_rate;
        }
        public virtual void ApplyYaw(double yaw_acc)
        {
            if (yaw_acc > 1) yaw_acc = 1;
            else if (yaw_acc < -1) yaw_acc = -1;

            dy_acc = (float)yaw_acc * yaw_rate;
        }

        public virtual bool CollidesWith(Physical o)
        {
            // representation collision test (will do bounding spheres first):
            if (rep != null && o.rep != null)
                return rep.CollidesWith(o.rep);

            Point delta_loc = Location() - o.Location();

            // bounding spheres test:
            if (delta_loc.Length > radius + o.radius)
                return false;

            // assume collision:
            return true;
        }
        public static void ElasticCollision(Physical a, Physical b)
        {
            double mass_sum = a.mass + b.mass;
            double mass_delta = a.mass - b.mass;

            Point vel_a = (new Point(b.velocity) * (2 * b.mass) + new Point(a.velocity) * mass_delta) * (1 / mass_sum);
            Point vel_b = (new Point(a.velocity) * (2 * a.mass) - new Point(b.velocity) * mass_delta) * (1 / mass_sum);

            a.velocity = vel_a;
            b.velocity = vel_b;
        }
        public static void InelasticCollision(Physical a, Physical b)
        {
            double mass_sum = a.mass + b.mass;

            Point vel_a = (new Point(a.velocity) * a.mass + new Point(b.velocity) * b.mass) * (1 / mass_sum);

            a.velocity = vel_a;
            b.velocity = vel_a;
        }
        public static void SemiElasticCollision(Physical a, Physical b)
        {
            double mass_sum = a.mass + b.mass;
            double mass_delta = a.mass - b.mass;

            Point avel = a.Velocity();
            Point bvel = b.Velocity();
            Point dv = avel - bvel;

            // low delta-v: stick
            if (dv.Length < 20)
            {
                if (a.mass > b.mass)
                {
                    b.velocity = a.velocity;
                }

                else
                {
                    a.velocity = b.velocity;
                }
            }

            // high delta-v: bounce
            else
            {
                Point Ve_a = (bvel * (2 * b.mass) + avel * mass_delta) * (1 / mass_sum) * 0.65;
                Point Ve_b = (avel * (2 * a.mass) - bvel * mass_delta) * (1 / mass_sum) * 0.65;
                Point Vi_ab = (avel * a.mass + bvel * b.mass) * (1 / mass_sum) * 0.35;

                a.arcade_velocity = new Point();
                b.arcade_velocity = new Point();

                a.velocity = Ve_a + Vi_ab;
                b.velocity = Ve_b + Vi_ab;
            }
        }
        public virtual void InflictDamage(double damage, int type = 0)
        {
            integrity -= (float)damage;

            if (integrity < 1.0f)
                integrity = 0.0f;
        }

        // accessors:
        public int Identity() { return id; }
        public int Type() { return obj_type; }
        public string Name() { return name; }

        public Point Location() { return cam.Pos(); }
        public Point Heading() { return cam.vpn(); }
        public Point LiftLine() { return cam.vup(); }
        public Point BeamLine() { return cam.vrt(); }
        public Point Velocity() { return velocity + arcade_velocity; }
        public Point Acceleration() { return accel; }
        public double Thrust() { return thrust; }
        public double TransX() { return trans_x; }
        public double TransY() { return trans_y; }
        public double TransZ() { return trans_z; }
        public double Drag() { return drag; }

        public double Roll() { return roll; }
        public double Pitch() { return pitch; }
        public double Yaw() { return yaw; }
        public Point Rotation() { return new Point(dp, dr, dy); }

        public double Alpha() { return alpha; }

        public double FlightPathYawAngle() { return flight_path_yaw; }
        public double FlightPathPitchAngle() { return flight_path_pitch; }

        public double Radius() { return radius; }
        public double Mass() { return mass; }
        public double Integrity() { return integrity; }
        public double Life() { return life; }

        public double Shake() { return shake; }
        public Point Vibration() { return vibration; }

        public CameraNode Cam() { return cam; }
        public Graphic Rep() { return rep; }
        public LightNode LightSrc() { return light; }

        public IDirector GetDirector() { return dir; }

        // mutators:
        public virtual void SetAngularRates(double r, double p, double y)
        {
            roll_rate = (float)r;
            pitch_rate = (float)p;
            yaw_rate = (float)y;
        }
        public virtual void GetAngularRates(out double r, out double p, out double y)
        {
            r = roll_rate;
            p = pitch_rate;
            y = yaw_rate;
        }
        public virtual void SetAngularDrag(double r, double p, double y)
        {
            dr_drg = (float)r;
            dp_drg = (float)p;
            dy_drg = (float)y;
        }
        public virtual void GetAngularDrag(out double r, out double p, out double y)
        {
            r = dr_drg;
            p = dp_drg;
            y = dy_drg;
        }
        public virtual void GetAngularThrust(out double r, out double p, out double y)
        {
            r = 0;
            p = 0;
            y = 0;

            if (dr_acc > 0.05 * roll_rate) r = 1;
            else if (dr_acc < -0.05 * roll_rate) r = -1;
            else if (dr > 0.01 * roll_rate) r = -1;
            else if (dr < -0.01 * roll_rate) r = 1;

            if (dy_acc > 0.05 * yaw_rate) y = 1;
            else if (dy_acc < -0.05 * yaw_rate) y = -1;
            else if (dy > 0.01 * yaw_rate) y = -1;
            else if (dy < -0.01 * yaw_rate) y = 1;

            if (dp_acc > 0.05 * pitch_rate) p = 1;
            else if (dp_acc < -0.05 * pitch_rate) p = -1;
            else if (dp > 0.01 * pitch_rate) p = -1;
            else if (dp < -0.01 * pitch_rate) p = 1;
        }
        public virtual void SetVelocity(Point v) { velocity = v; }
        public virtual void SetAbsoluteOrientation(double roll, double pitch, double yaw) { throw new System.NotImplementedException(); }
        public virtual void CloneCam(CameraNode cam) { throw new System.NotImplementedException(); }
        public virtual void SetDrag(double d) { drag = (float)d; }

        public virtual void SetPrimary(Point loc, double mass)
        {
            primary_loc = loc;
            primary_mass = mass;
        }
        public virtual void SetGravity(double g)
        {
            if (g >= 0)
                g_accel = (float)g;
        }
        public virtual void SetBaseDensity(double d)
        {
            if (d >= 0)
                Do = (float)d;
        }

        public virtual double GetBaseDensity() { return Do; }
        public virtual double GetDensity()
        {
            double alt = cam.Pos().Y;
            double rho = 0.75 * Do * (250e3 - alt) / 250e3;

            return rho;
        }

        protected static double random()
        {
            return rnd.Next(-16384, 16384);
        }

        protected static int id_key = 1;

        // identification:
        protected int id;
        protected int obj_type;
        protected string name;

        // position, velocity, and acceleration:
        protected CameraNode cam;
        protected Point velocity;
        protected Point arcade_velocity;
        protected Point accel;
        protected float thrust;
        protected float trans_x;
        protected float trans_y;
        protected float trans_z;
        protected float drag;

        // attitude and angular velocity:
        protected float roll, pitch, yaw;
        protected float dr, dp, dy;
        protected float dr_acc, dp_acc, dy_acc;
        protected float dr_drg, dp_drg, dy_drg;

        protected float flight_path_yaw;
        protected float flight_path_pitch;

        // gravitation:
        protected Point primary_loc;
        protected double primary_mass;

        // aerodynamics:
        protected float g_accel;    // acceleration due to gravity (constant)
        protected float Do;         // atmospheric density at sea level
        protected float CL;         // base coefficient of lift
        protected float CD;         // base coefficient of drag
        protected float alpha;      // current angle of attack (radians)
        protected float stall;      // stall angle of attack (radians)
        protected bool lat_thrust; // lateral thrusters enabled in aero mode?
        protected bool straight;

        // vibration:
        protected float shake;
        protected Point vibration;

        // scale factors for ApplyXxx():
        protected float roll_rate, pitch_rate, yaw_rate;

        // physical properties:
        protected double life;
        protected float radius;
        protected float mass;
        protected float integrity;

        // graphic representation:
        protected Graphic rep;
        protected LightNode light;

        // AI or human controller:
        protected IDirector dir;        // null implies an autonomous object

        protected static double sub_frame;
        protected const double GRAV = 6.673e-11;
        protected static Random rnd = new Random();
    }
}
