﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Player.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Award Info class
*/

using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.SimElements;

namespace StarshatterNet.Stars
{
    public class AwardInfo
    {
        public static List<AwardInfo> RankTable
        {
            get { return rank_table; }
        }
        public static List<AwardInfo> MedalTable
        {
            get { return medal_table; }
        }

        public const string ConfigurationFilename = "Awards/awards";

        public static void LoadAwardTables()
        {
            ErrLogger.PrintLine("Loading Ranks and Medals");
            var awardList = ReaderSupport.DeserializeAsset<AwardInfoList>(ConfigurationFilename);
            foreach (var awardEntry in awardList.AwardInfos)
            {
                AwardInfo awardInfo = new AwardInfo();

                if (!string.IsNullOrEmpty(awardEntry.name))
                    awardInfo.name = Game.GetText(awardEntry.name);
                if (!string.IsNullOrEmpty(awardEntry.desc))
                    awardInfo.desc = Game.GetText(awardEntry.desc);
                if (!string.IsNullOrEmpty(awardEntry.award))
                    awardInfo.grant = Game.GetText(awardEntry.award);

                if (!string.IsNullOrEmpty(awardEntry.desc_sound))
                    awardInfo.desc_sound = "Awards/" + awardEntry.desc_sound;
                if (!string.IsNullOrEmpty(awardEntry.award_sound))
                    awardInfo.grant_sound = "Awards/" + awardEntry.award_sound;

                if (!string.IsNullOrEmpty(awardEntry.large))
                    awardInfo.large_insignia = new Bitmap("Awards/" + awardEntry.large);
                if (!string.IsNullOrEmpty(awardEntry.small))
                    awardInfo.small_insignia = new Bitmap("Awards/" + awardEntry.small);

                if (!string.IsNullOrEmpty(awardEntry.type))
                {
                    if (awardEntry.type.ToLower() == "rank")
                        awardInfo.type = AwardType.RANK;
                    if (awardEntry.type.ToLower() == "medal")
                        awardInfo.type = AwardType.MEDAL;
                }

                if (!string.IsNullOrEmpty(awardEntry.id))
                    awardInfo.id = ReaderSupport.IntParse(awardEntry.id);
                if (!string.IsNullOrEmpty(awardEntry.total_points))
                    awardInfo.total_points = ReaderSupport.IntParse(awardEntry.total_points);
                if (!string.IsNullOrEmpty(awardEntry.mission_points))
                    awardInfo.mission_points = ReaderSupport.IntParse(awardEntry.mission_points);
                if (!string.IsNullOrEmpty(awardEntry.total_missions))
                    awardInfo.total_missions = ReaderSupport.IntParse(awardEntry.total_missions);
                if (!string.IsNullOrEmpty(awardEntry.kills))
                    awardInfo.kills = ReaderSupport.IntParse(awardEntry.kills);
                if (!string.IsNullOrEmpty(awardEntry.lost))
                    awardInfo.lost = ReaderSupport.IntParse(awardEntry.lost);
                if (!string.IsNullOrEmpty(awardEntry.collision))
                    awardInfo.collision = ReaderSupport.IntParse(awardEntry.collision);
                if (!string.IsNullOrEmpty(awardEntry.campaign_id))
                    awardInfo.campaign_id = ReaderSupport.IntParse(awardEntry.campaign_id);

                awardInfo.campaign_complete = awardEntry.campaign_complete;
                awardInfo.dynamic_campaign = awardEntry.dynamic_campaign;
                awardInfo.ceremony = awardEntry.ceremony;

                if (!string.IsNullOrEmpty(awardEntry.required_awards))
                    awardInfo.required_awards = ReaderSupport.IntParse(awardEntry.required_awards);
                if (!string.IsNullOrEmpty(awardEntry.lottery))
                    awardInfo.lottery = ReaderSupport.IntParse(awardEntry.lottery);
                if (!string.IsNullOrEmpty(awardEntry.min_rank))
                    awardInfo.min_rank = ReaderSupport.IntParse(awardEntry.min_rank);
                if (!string.IsNullOrEmpty(awardEntry.max_rank))
                    awardInfo.max_rank = ReaderSupport.IntParse(awardEntry.max_rank);
                if (!string.IsNullOrEmpty(awardEntry.min_ship_class))
                    awardInfo.min_ship_class = Ship.ClassForName(awardEntry.min_ship_class);
                if (!string.IsNullOrEmpty(awardEntry.max_ship_class))
                    awardInfo.min_ship_class = Ship.ClassForName(awardEntry.max_ship_class);
                if (!string.IsNullOrEmpty(awardEntry.grant))
                    awardInfo.granted_ship_classes = ReaderSupport.IntParse(awardEntry.grant);

                if (awardInfo.type == AwardType.RANK)
                {
                    rank_table.Add(awardInfo);
                }
                else if (awardInfo.type == AwardType.MEDAL)
                {
                    medal_table.Add(awardInfo);
                }
            }
        }

        public static void Clear()
        {
            rank_table.Clear();
            medal_table.Clear();
        }

        public enum AwardType { RANK, MEDAL };

        public AwardType type = AwardType.RANK;

        public int id = 0;
        public string name;
        public string abrv;
        public string desc;
        public string grant;
        public string desc_sound;
        public string grant_sound;
        public string large = null;
        public string small = null;
        public int granted_ship_classes = 0x7;
        public int total_points = 0;

        public Bitmap large_insignia = null;
        public Bitmap small_insignia = null;

        public int mission_points = 0;
        public int total_missions = 0;
        public int kills = 0;
        public int lost = 0;
        public int collision = 0;
        public int campaign_id = 0;
        public bool campaign_complete = false;
        public bool dynamic_campaign = false;
        public bool ceremony = true;

        public int required_awards = 0;
        public int lottery = 0;
        public int min_rank = 0;
        public int max_rank = int.MaxValue;
        public CLASSIFICATION min_ship_class;
        public CLASSIFICATION max_ship_class;
        public int minval_ship_class = 0;
        public int maxval_ship_class = int.MaxValue;

        private static List<AwardInfo> rank_table = new List<AwardInfo>();
        private static List<AwardInfo> medal_table = new List<AwardInfo>();
    }

    [Serializable]
    public class AwardInfoList
    {
        public List<AwardInfoEntry> AwardInfos;
    }

    [Serializable]
    public class AwardInfoEntry
    {
        public string type;
        public string id;
        public string name;
        public string abrv;
        public string desc;
        public string award;
        public string award_sound;
        public string desc_sound;
        public string grant_sound;
        public string small;
        public string large;

        public string min_rank;
        public string max_rank;
        public string min_ship_class;
        public string max_ship_class;
        public string lottery;
        public string total_points;
        public string mission_points;
        public string total_missions;
        public string kills;
        public string lost;
        public string collision;
        public string campaign_id;
        public bool campaign_complete;
        public bool dynamic_campaign;
        public bool ceremony;
        public string required_awards;
        public string grant;
    }
}
