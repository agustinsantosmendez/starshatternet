﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars.exe
    ORIGINAL FILE:      KeyMap.h/KeyMap.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Keyboard Mapping
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars;
using UnityEngine;

namespace StarshatterNet.Config
{
    public struct KeyMapEntry
    {
        public KeyMapEntry(int a, int k, int s = 0, int j = 0)
        {
            this.act = a;
            this.key = k;
            this.alt = s;
            this.joy = j;
        }

        public bool Equals(KeyMapEntry other)
        {
            return this.act == other.act && this.key == other.key && this.alt == other.alt && this.joy == other.joy;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            KeyMapEntry other = (KeyMapEntry)obj;
            return this.act == other.act && this.key == other.key && this.alt == other.alt && this.joy == other.joy;
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.act.GetHashCode();
            hash = hash * 23 + this.key.GetHashCode();
            hash = hash * 23 + this.alt.GetHashCode();
            hash = hash * 23 + this.joy.GetHashCode();
            return hash;
        }

        public static bool operator ==(KeyMapEntry l, KeyMapEntry r)
        {
            if (object.ReferenceEquals(l, r))
            {
                return true;     // if both the same object, or both null
            }
            // If one is null, but not both, return false.
            if (((object)l == null) || ((object)r == null))
            {
                return false;
            }

            return l.act == r.act && l.key == r.key && l.alt == r.alt && l.joy == r.joy;
        }

        public static bool operator !=(KeyMapEntry l, KeyMapEntry r)
        {
            return !(l == r);
        }

        public int act;
        public int key;
        public int alt;
        public int joy;
    }

    public class KeyMap
    {
        public enum KEY_CATEGORY { KEY_FLIGHT, KEY_WEAPONS, KEY_VIEW, KEY_MISC };

        public KeyMap()
        {
            int n = BuildDefaultKeyMap();
            DefaultKeyMap(n);
        }

        public int DefaultKeyMap(int max_keys = 256)
        {
            for (int i = 0; i < max_keys; i++)
                map[i] = defmap[i];

            nkeys = max_keys;
            return nkeys;
        }

        public int LoadKeyMap(string filename, int max_keys = 256)
        {
            if (!ReaderSupport.DoesDataFileExist(filename)) return nkeys;
            string fullPath = ReaderSupport.GetDataPath(filename);
            string[] lines = File.ReadAllLines(fullPath);

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;
                string trimmedline = Regex.Replace(line, @"\s+", " ").Trim();
                string[] parts = trimmedline.Split(' ');

                int act = -1, key = -1, alt = 0, joy = -1;
                string act_str = null, key_str = null, alt_str = null, joy_str = null;

                if (parts.Length < 2) continue;
                act_str = parts[0];
                key_str = parts[1];
                if (parts.Length == 4)
                {
                    alt_str = parts[2];
                    joy_str = parts[3];
                }
                act = GetKeyAction(act_str);
                key = GetKeyKey(key_str);
                alt = GetKeyKey(alt_str);
                joy = GetKeyKey(joy_str);

                if (act != -1 && key != -1)
                {
                    if (act == KEY_CONTROL_MODEL)
                        Ship.SetControlModel(key);

                    bool mapped = false;
                    for (int i = 0; i < max_keys && !mapped; i++)
                    {
                        if (map[i].act == act)
                        {
                            ErrLogger.PrintLine("  Remapping: '{0}' => {1}({3}) {3}({4}) {5}({6})",
                                                    act_str, key_str, key, alt_str, alt, joy_str, joy);

                            map[i] = new KeyMapEntry(act, key, alt, joy);
                            mapped = true;
                        }
                    }
                    if (!mapped)
                    {
                        ErrLogger.PrintLine("  Mapping: '{0}' => {1}({3}) {3}({4}) {5}({6})",
                                                act_str, key_str, key, alt_str, alt, joy_str, joy);
                        map[nkeys++] = new KeyMapEntry(act, key, alt, joy);
                    }
                }

                if (nkeys >= max_keys - 1)
                {
                    ErrLogger.PrintLine("   Too many keys in configuration...");
                    break;
                }
            }
            return nkeys;
        }

        public int SaveKeyMap(string filename, int max_keys = 256)
        {
#warning KeyMap.SaveKeyMap is not yet implemented
            throw new NotImplementedException();
        }

        public KEY_CATEGORY GetCategory(int n)
        {
            int nactions = key_action_table.Length;

            for (int i = 0; i < nactions; i++)
            {
                if (map[n].act == key_action_table[i].key)
                {
                    return key_action_table[i].category;
                }
            }

            return KEY_CATEGORY.KEY_MISC;
        }
        public string DescribeAction(int n)
        {
            int nactions = key_action_table.Length;

            for (int i = 0; i < nactions; i++)
            {
                if (map[n].act == key_action_table[i].key)
                {
                    if (key_action_table[i].desc != null)
                        return key_action_table[i].desc;

                    return key_action_table[i].name;
                }
            }

            return null;
        }
        public string DescribeKey(int n)
        {
            if (n >= 0 && n < 256)
                return DescribeKey(map[n].key, map[n].alt, map[n].joy);

            return null;
        }
        public int FindMapIndex(int act)
        {
            for (int n = 0; n < nkeys; n++)
                if (map[n].act == act)
                    return n;
            return -1;
        }

        public static string DescribeKey(int vk, int shift, int j)
        {
            string key_desc = null;
            string key = null;
            string alt = null;
            string joy = null;

            int nkeys = key_key_table.Length;

            for (int i = 0; i < nkeys; i++)
            {
                if (vk > 0 && vk == key_key_table[i].key)
                {
                    if (key_key_table[i].desc != null)
                        key = key_key_table[i].desc;
                    else
                        key = key_key_table[i].name;
                }

                if (shift > 0 && shift == key_key_table[i].key)
                {
                    if (key_key_table[i].desc != null)
                        alt = key_key_table[i].desc;
                    else
                        alt = key_key_table[i].name;
                }

                if (j > 0 && j == key_key_table[i].key)
                {
                    if (key_key_table[i].desc != null)
                        joy = key_key_table[i].desc;
                    else
                        joy = key_key_table[i].name;
                }
            }

            if (key != null)
            {
                if (alt != null)
                {
                    key_desc = string.Format("{0}+{1}", alt, key);
                }
                else
                {
                    key_desc = key;
                }

                if (joy != null)
                {
                    key_desc += ", ";
                    key_desc += joy;
                }
            }

            else if (joy != null)
            {
                key_desc = joy;
            }

            else
            {
                key_desc = string.Format("{0}", vk);
            }

            return key_desc;
        }
        public static int GetMappableVKey(int n)
        {
            int nkeys = key_key_table.Length;

            if (n >= 0 && n < nkeys)
            {
                return key_key_table[n].key;
            }

            return 0;
        }

        public int GetNumKeys() { return nkeys; }
        public KeyMapEntry[] GetMapping() { return map; }
        public KeyMapEntry GetKeyMap(int i) { return map[i]; }
        public KeyMapEntry GetDefault(int i) { return defmap[i]; }

        public void Bind(int a, int k, int s)
        {
            if (a == 0) return;

            for (int i = 0; i < nkeys; i++)
            {
                if (map[i].act == a)
                {
                    map[i].key = k;
                    map[i].alt = s;

                    return;
                }
            }

            map[nkeys++] = new KeyMapEntry(a, k, s);
        }

        public static int GetKeyAction(string act_str)
        {
            if (string.IsNullOrWhiteSpace(act_str)) return -1;

            int nactions = key_action_table.Length;

            for (int i = 0; i < nactions; i++)
                if (act_str == key_action_table[i].name)
                    return key_action_table[i].key;

            return -1;
        }
        public static int GetKeyActionIndex(int act)
        {
            int nactions = key_action_table.Length;

            for (int i = 0; i < nactions; i++)
                if (key_action_table[i].key == act)
                    return i;

            return -1;
        }
        public static int GetKeyKey(string key_str)
        {
            if (string.IsNullOrWhiteSpace(key_str)) return 0;

            if (key_str[0] == '=')
            {
                var parts = key_str.Split('=');
                int value = Convert.ToInt32(parts[1]);
                return value;
            }

            int nkeys = key_key_table.Length;

            for (int i = 0; i < nkeys; i++)
                if (key_str == key_key_table[i].name)
                    return key_key_table[i].key;

            return 0;
        }
        public static int GetKeyKeyIndex(int key)
        {
            int nkeys = key_key_table.Length;

            for (int i = 0; i < nkeys; i++)
                if (key_key_table[i].key == key)
                    return i;

            return -1;
        }


        protected int BuildDefaultKeyMap()
        {
            int i = 0;

            defmap[i++] = new KeyMapEntry(KEY_PITCH_UP, VK_DOWN);
            defmap[i++] = new KeyMapEntry(KEY_PITCH_DOWN, VK_UP);
            defmap[i++] = new KeyMapEntry(KEY_YAW_LEFT, VK_LEFT);
            defmap[i++] = new KeyMapEntry(KEY_YAW_RIGHT, VK_RIGHT);
            defmap[i++] = new KeyMapEntry(KEY_ROLL_LEFT, VK_NUMPAD7);
            defmap[i++] = new KeyMapEntry(KEY_ROLL_RIGHT, VK_NUMPAD9);

            defmap[i++] = new KeyMapEntry(KEY_PLUS_X, 190, 0, KEY_POV_0_RIGHT);    // .
            defmap[i++] = new KeyMapEntry(KEY_MINUS_X, 188, 0, KEY_POV_0_LEFT);     // ,
            defmap[i++] = new KeyMapEntry(KEY_PLUS_Y, VK_HOME);
            defmap[i++] = new KeyMapEntry(KEY_MINUS_Y, VK_END);
            defmap[i++] = new KeyMapEntry(KEY_PLUS_Z, VK_PRIOR, 0, KEY_POV_0_UP);
            defmap[i++] = new KeyMapEntry(KEY_MINUS_Z, VK_NEXT, 0, KEY_POV_0_DOWN);

            defmap[i++] = new KeyMapEntry(KEY_ACTION_0, VK_CONTROL, 0, KEY_JOY_1);
            defmap[i++] = new KeyMapEntry(KEY_ACTION_1, VK_SPACE, 0, KEY_JOY_2);


            defmap[i++] = new KeyMapEntry(KEY_THROTTLE_UP, 'A');
            defmap[i++] = new KeyMapEntry(KEY_THROTTLE_DOWN, 'Z');
            defmap[i++] = new KeyMapEntry(KEY_THROTTLE_FULL, 'A', VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_THROTTLE_ZERO, 'Z', VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_AUGMENTER, VK_TAB);
            defmap[i++] = new KeyMapEntry(KEY_FLCS_MODE_AUTO, 'M');
            defmap[i++] = new KeyMapEntry(KEY_COMMAND_MODE, 'M', VK_SHIFT);

            defmap[i++] = new KeyMapEntry(KEY_CYCLE_PRIMARY, VK_BACK, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CYCLE_SECONDARY, VK_BACK);
            defmap[i++] = new KeyMapEntry(KEY_LOCK_TARGET, 'T', 0, KEY_JOY_3);
            defmap[i++] = new KeyMapEntry(KEY_LOCK_THREAT, 'T', VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_LOCK_CLOSEST_SHIP, 'U');
            defmap[i++] = new KeyMapEntry(KEY_LOCK_CLOSEST_THREAT, 'U', VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_LOCK_HOSTILE_SHIP, 'Y');
            defmap[i++] = new KeyMapEntry(KEY_LOCK_HOSTILE_THREAT, 'Y', VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CYCLE_SUBTARGET, 186);
            defmap[i++] = new KeyMapEntry(KEY_PREV_SUBTARGET, 186, VK_SHIFT);

            defmap[i++] = new KeyMapEntry(KEY_DECOY, 'D', 0, KEY_JOY_4);
            defmap[i++] = new KeyMapEntry(KEY_GEAR_TOGGLE, 'G');
            defmap[i++] = new KeyMapEntry(KEY_NAVLIGHT_TOGGLE, 'L');

            defmap[i++] = new KeyMapEntry(KEY_AUTO_NAV, 'N', VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_DROP_ORBIT, 'O');

            defmap[i++] = new KeyMapEntry(KEY_SHIELDS_UP, 'S');
            defmap[i++] = new KeyMapEntry(KEY_SHIELDS_DOWN, 'X');
            defmap[i++] = new KeyMapEntry(KEY_SHIELDS_FULL, 'S', VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_SHIELDS_ZERO, 'X', VK_SHIFT);

            defmap[i++] = new KeyMapEntry(KEY_SENSOR_MODE, VK_F5);
            defmap[i++] = new KeyMapEntry(KEY_SENSOR_GROUND_MODE, VK_F5, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_LAUNCH_PROBE, VK_F6);
            defmap[i++] = new KeyMapEntry(KEY_SENSOR_RANGE_MINUS, VK_F7);
            defmap[i++] = new KeyMapEntry(KEY_SENSOR_RANGE_PLUS, VK_F8);
            defmap[i++] = new KeyMapEntry(KEY_EMCON_MINUS, VK_F9);
            defmap[i++] = new KeyMapEntry(KEY_EMCON_PLUS, VK_F10);

            defmap[i++] = new KeyMapEntry(KEY_EXIT, VK_ESCAPE);
            defmap[i++] = new KeyMapEntry(KEY_PAUSE, VK_PAUSE);
            // defmap[i++] = new KeyMapEntry(KEY_NEXT_VIEW,           VK_TAB);
            defmap[i++] = new KeyMapEntry(KEY_TIME_EXPAND, VK_DELETE);
            defmap[i++] = new KeyMapEntry(KEY_TIME_COMPRESS, VK_INSERT);
            defmap[i++] = new KeyMapEntry(KEY_TIME_SKIP, VK_INSERT, VK_SHIFT);

            defmap[i++] = new KeyMapEntry(KEY_CAM_BRIDGE, VK_F1);
            defmap[i++] = new KeyMapEntry(KEY_CAM_VIRT, VK_F1, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_CHASE, VK_F2);
            defmap[i++] = new KeyMapEntry(KEY_CAM_DROP, VK_F2, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_EXTERN, VK_F3);
            defmap[i++] = new KeyMapEntry(KEY_TARGET_PADLOCK, VK_F4);

            defmap[i++] = new KeyMapEntry(KEY_ZOOM_WIDE, 'K');
            defmap[i++] = new KeyMapEntry(KEY_HUD_MODE, 'H');
            defmap[i++] = new KeyMapEntry(KEY_HUD_COLOR, 'H', VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_HUD_WARN, 'C');
            defmap[i++] = new KeyMapEntry(KEY_HUD_INST, 'I');
            defmap[i++] = new KeyMapEntry(KEY_NAV_DLG, 'N');
            defmap[i++] = new KeyMapEntry(KEY_WEP_DLG, 'W');
            defmap[i++] = new KeyMapEntry(KEY_ENG_DLG, 'E');
            defmap[i++] = new KeyMapEntry(KEY_FLT_DLG, 'F');
            defmap[i++] = new KeyMapEntry(KEY_RADIO_MENU, 'R');
            defmap[i++] = new KeyMapEntry(KEY_QUANTUM_MENU, 'Q');

            defmap[i++] = new KeyMapEntry(KEY_MFD1, 219);             // [
            defmap[i++] = new KeyMapEntry(KEY_MFD2, 221);             // ]
            defmap[i++] = new KeyMapEntry(KEY_SELF_DESTRUCT, VK_ESCAPE, VK_SHIFT);

            defmap[i++] = new KeyMapEntry(KEY_CAM_CYCLE_OBJECT, VK_TAB, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_EXT_PLUS_AZ, VK_LEFT, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_EXT_MINUS_AZ, VK_RIGHT, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_EXT_PLUS_EL, VK_UP, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_EXT_MINUS_EL, VK_DOWN, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_EXT_PLUS_RANGE, VK_SUBTRACT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_EXT_MINUS_RANGE, VK_ADD);
            defmap[i++] = new KeyMapEntry(KEY_CAM_VIEW_SELECTION, 'V');
            defmap[i++] = new KeyMapEntry(KEY_CAM_VIRT_PLUS_AZ, VK_LEFT, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_VIRT_MINUS_AZ, VK_RIGHT, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_VIRT_PLUS_EL, VK_DOWN, VK_SHIFT);
            defmap[i++] = new KeyMapEntry(KEY_CAM_VIRT_MINUS_EL, VK_UP, VK_SHIFT);

            defmap[i++] = new KeyMapEntry(KEY_SWAP_ROLL_YAW, 'J');

            defmap[i++] = new KeyMapEntry(KEY_COMM_ATTACK_TGT, 'A', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_COMM_ESCORT_TGT, 'E', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_COMM_WEP_FREE, 'B', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_COMM_WEP_HOLD, 'F', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_COMM_COVER_ME, 'H', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_COMM_SKIP_NAV, 'N', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_COMM_RETURN_TO_BASE, 'R', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_COMM_CALL_INBOUND, 'I', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_COMM_REQUEST_PICTURE, 'P', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_COMM_REQUEST_SUPPORT, 'S', VK_MENU);

            defmap[i++] = new KeyMapEntry(KEY_CHAT_BROADCAST, '1', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_CHAT_TEAM, '2', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_CHAT_WING, '3', VK_MENU);
            defmap[i++] = new KeyMapEntry(KEY_CHAT_UNIT, '4', VK_MENU);

            defmap[i++] = new KeyMapEntry(KEY_CONTROL_MODEL, 0);

            defmap[i++] = new KeyMapEntry(KEY_JOY_SELECT, 1);
            defmap[i++] = new KeyMapEntry(KEY_JOY_RUDDER, 0);
            defmap[i++] = new KeyMapEntry(KEY_JOY_THROTTLE, 1);
            defmap[i++] = new KeyMapEntry(KEY_JOY_SENSE, 1);
            defmap[i++] = new KeyMapEntry(KEY_JOY_DEAD_ZONE, 500);

            defmap[i++] = new KeyMapEntry(KEY_MOUSE_SELECT, 1);
            defmap[i++] = new KeyMapEntry(KEY_MOUSE_SENSE, 10);
            defmap[i++] = new KeyMapEntry(KEY_MOUSE_ACTIVE, 192);          // ~ key

            /*** For Debug Convenience Only: ***/
            defmap[i++] = new KeyMapEntry(KEY_INC_STARDATE, VK_F11);
            defmap[i++] = new KeyMapEntry(KEY_DEC_STARDATE, VK_F11, VK_SHIFT);
            /***/

            return i;
        }

        protected KeyMapEntry[] map = new KeyMapEntry[256];
        protected KeyMapEntry[] defmap = new KeyMapEntry[256];
        protected int nkeys = 0;

        public const int KEY_MAP_SIZE = 256;
        public const int KEY_BASE_SIZE = 64;
        public const int KEY_USER_SIZE = KEY_MAP_SIZE - KEY_BASE_SIZE;

        public const int KEY_MAP_BASE = 0;
        public const int KEY_MAP_END = KEY_MAP_BASE + KEY_BASE_SIZE - 1;

        public const int KEY_USER_BASE = KEY_MAP_END + 1;
        public const int KEY_USER_END = KEY_USER_BASE + KEY_USER_SIZE - 1;

        public const int KEY_MAP_FIRST = KEY_MAP_BASE;
        public const int KEY_MAP_LAST = KEY_MAP_BASE + KEY_MAP_SIZE - 1;

        // MAP NAMES:

        public const int KEY_PLUS_X = 1;
        public const int KEY_MINUS_X = 2;
        public const int KEY_PLUS_Y = 3;
        public const int KEY_MINUS_Y = 4;
        public const int KEY_PLUS_Z = 5;
        public const int KEY_MINUS_Z = 6;

        public const int KEY_PITCH_UP = 7;
        public const int KEY_PITCH_DOWN = 8;
        public const int KEY_YAW_LEFT = 9;
        public const int KEY_YAW_RIGHT = 10;
        public const int KEY_ROLL_LEFT = 11;
        public const int KEY_ROLL_RIGHT = 12;
        public const int KEY_CENTER = 13;
        public const int KEY_ROLL_ENABLE = 14;

        public const int KEY_ACTION_0 = 15;
        public const int KEY_ACTION_1 = 16;
        public const int KEY_ACTION_2 = 17;
        public const int KEY_ACTION_3 = 18;

        public const int KEY_CONTROL_MODEL = 19;
        public const int KEY_MOUSE_SELECT = 20;
        public const int KEY_MOUSE_SENSE = 21;
        public const int KEY_MOUSE_SWAP = 22;
        public const int KEY_MOUSE_INVERT = 23;
        public const int KEY_MOUSE_ACTIVE = 24;

        public const int KEY_JOY_SELECT = 25;
        public const int KEY_JOY_THROTTLE = 26;
        public const int KEY_JOY_RUDDER = 27;
        public const int KEY_JOY_SENSE = 28;
        public const int KEY_JOY_DEAD_ZONE = 29;
        public const int KEY_JOY_SWAP = 30;

        public const int KEY_AXIS_YAW = 32;
        public const int KEY_AXIS_PITCH = 33;
        public const int KEY_AXIS_ROLL = 34;
        public const int KEY_AXIS_THROTTLE = 35;

        public const int KEY_AXIS_YAW_INVERT = 38;
        public const int KEY_AXIS_PITCH_INVERT = 39;
        public const int KEY_AXIS_ROLL_INVERT = 40;
        public const int KEY_AXIS_THROTTLE_INVERT = 41;


        // CONTROL VALUES:

        // joystick buttons and switches must use
        // ids greater than 255 so they don't interfere
        // with extended ascii numbers for keyboard keys

        public const int KEY_JOY_AXIS_X = 0x1A0;
        public const int KEY_JOY_AXIS_Y = 0x1A1;
        public const int KEY_JOY_AXIS_Z = 0x1A2;
        public const int KEY_JOY_AXIS_RX = 0x1A3;
        public const int KEY_JOY_AXIS_RY = 0x1A4;
        public const int KEY_JOY_AXIS_RZ = 0x1A5;
        public const int KEY_JOY_AXIS_S0 = 0x1A6;
        public const int KEY_JOY_AXIS_S1 = 0x1A7;

        public const int KEY_JOY_1 = 0x1C1;
        public const int KEY_JOY_2 = 0x1C2;
        public const int KEY_JOY_3 = 0x1C3;
        public const int KEY_JOY_4 = 0x1C4;
        public const int KEY_JOY_5 = 0x1C5;
        public const int KEY_JOY_6 = 0x1C6;
        public const int KEY_JOY_7 = 0x1C7;
        public const int KEY_JOY_8 = 0x1C8;
        public const int KEY_JOY_9 = 0x1C9;
        public const int KEY_JOY_10 = 0x1CA;
        public const int KEY_JOY_11 = 0x1CB;
        public const int KEY_JOY_12 = 0x1CC;
        public const int KEY_JOY_13 = 0x1CD;
        public const int KEY_JOY_14 = 0x1CE;
        public const int KEY_JOY_15 = 0x1CF;
        public const int KEY_JOY_16 = 0x1D0;

        public const int KEY_JOY_32 = 0x1E0;

        public const int KEY_POV_0_UP = 0x1F0;
        public const int KEY_POV_0_DOWN = 0x1F1;
        public const int KEY_POV_0_LEFT = 0x1F2;
        public const int KEY_POV_0_RIGHT = 0x1F3;

        public const int KEY_POV_1_UP = 0x1F4;
        public const int KEY_POV_1_DOWN = 0x1F5;
        public const int KEY_POV_1_LEFT = 0x1F6;
        public const int KEY_POV_1_RIGHT = 0x1F7;

        public const int KEY_POV_2_UP = 0x1F8;
        public const int KEY_POV_2_DOWN = 0x1F9;
        public const int KEY_POV_2_LEFT = 0x1FA;
        public const int KEY_POV_2_RIGHT = 0x1FB;

        public const int KEY_POV_3_UP = 0x1FC;
        public const int KEY_POV_3_DOWN = 0x1FD;
        public const int KEY_POV_3_LEFT = 0x1FE;
        public const int KEY_POV_3_RIGHT = 0x1FF;
        // +--------------------------------------------------------------------+


        public const int KEY_EXIT = 0 + KEY_USER_BASE;
        public const int KEY_PAUSE = 1 + KEY_USER_BASE;
        public const int KEY_NEXT_VIEW = 2 + KEY_USER_BASE;
        public const int KEY_TARGET_PADLOCK = 3 + KEY_USER_BASE;
        public const int KEY_THREAT_PADLOCK = 4 + KEY_USER_BASE;
        public const int KEY_LOCK_TARGET = 5 + KEY_USER_BASE;
        public const int KEY_LOCK_THREAT = 6 + KEY_USER_BASE;
        public const int KEY_AUTO_NAV = 7 + KEY_USER_BASE;
        public const int KEY_TIME_COMPRESS = 8 + KEY_USER_BASE;
        public const int KEY_TIME_EXPAND = 9 + KEY_USER_BASE;
        public const int KEY_TIME_SKIP = 10 + KEY_USER_BASE;

        public const int KEY_SWAP_ROLL_YAW = 11 + KEY_USER_BASE;

        public const int KEY_THROTTLE_UP = 15 + KEY_USER_BASE;
        public const int KEY_THROTTLE_DOWN = 16 + KEY_USER_BASE;
        public const int KEY_THROTTLE_ZERO = 17 + KEY_USER_BASE;
        public const int KEY_THROTTLE_FULL = 18 + KEY_USER_BASE;
        public const int KEY_CYCLE_PRIMARY = 19 + KEY_USER_BASE;
        public const int KEY_CYCLE_SECONDARY = 20 + KEY_USER_BASE;
        public const int KEY_FLCS_MODE_AUTO = 21 + KEY_USER_BASE;
        public const int KEY_DROP_ORBIT = 22 + KEY_USER_BASE;

        public const int KEY_HUD_INST = 23 + KEY_USER_BASE;
        public const int KEY_CAM_BRIDGE = 24 + KEY_USER_BASE;
        public const int KEY_CAM_CHASE = 25 + KEY_USER_BASE;
        public const int KEY_CAM_EXTERN = 26 + KEY_USER_BASE;
        public const int KEY_HUD_MODE = 27 + KEY_USER_BASE;
        public const int KEY_HUD_COLOR = 28 + KEY_USER_BASE;
        public const int KEY_HUD_WARN = 29 + KEY_USER_BASE;
        public const int KEY_NAV_DLG = 30 + KEY_USER_BASE;
        public const int KEY_WEP_DLG = 31 + KEY_USER_BASE;
        public const int KEY_FLT_DLG = 32 + KEY_USER_BASE;
        public const int KEY_ENG_DLG = 33 + KEY_USER_BASE;

        public const int KEY_ZOOM_WIDE = 34 + KEY_USER_BASE;
        public const int KEY_ZOOM_IN = 35 + KEY_USER_BASE;
        public const int KEY_ZOOM_OUT = 36 + KEY_USER_BASE;
        public const int KEY_CAM_CYCLE_OBJECT = 37 + KEY_USER_BASE;
        public const int KEY_CAM_EXT_PLUS_AZ = 38 + KEY_USER_BASE;
        public const int KEY_CAM_EXT_MINUS_AZ = 39 + KEY_USER_BASE;
        public const int KEY_CAM_EXT_PLUS_EL = 40 + KEY_USER_BASE;
        public const int KEY_CAM_EXT_MINUS_EL = 41 + KEY_USER_BASE;
        public const int KEY_CAM_EXT_PLUS_RANGE = 42 + KEY_USER_BASE;
        public const int KEY_CAM_EXT_MINUS_RANGE = 43 + KEY_USER_BASE;
        public const int KEY_CAM_VIEW_SELECTION = 44 + KEY_USER_BASE;
        public const int KEY_CAM_DROP = 45 + KEY_USER_BASE;

        public const int KEY_TARGET_SELECTION = 50 + KEY_USER_BASE;
        public const int KEY_RADIO_MENU = 51 + KEY_USER_BASE;
        public const int KEY_QUANTUM_MENU = 52 + KEY_USER_BASE;
        public const int KEY_MFD1 = 53 + KEY_USER_BASE;
        public const int KEY_MFD2 = 54 + KEY_USER_BASE;
        public const int KEY_MFD3 = 55 + KEY_USER_BASE;
        public const int KEY_MFD4 = 56 + KEY_USER_BASE;

        public const int KEY_SENSOR_MODE = 60 + KEY_USER_BASE;
        public const int KEY_SENSOR_GROUND_MODE = 61 + KEY_USER_BASE;
        public const int KEY_SENSOR_BEAM = 62 + KEY_USER_BASE;
        public const int KEY_SENSOR_RANGE_PLUS = 63 + KEY_USER_BASE;
        public const int KEY_SENSOR_RANGE_MINUS = 64 + KEY_USER_BASE;
        public const int KEY_EMCON_PLUS = 65 + KEY_USER_BASE;
        public const int KEY_EMCON_MINUS = 66 + KEY_USER_BASE;

        public const int KEY_SHIELDS_UP = 67 + KEY_USER_BASE;
        public const int KEY_SHIELDS_DOWN = 68 + KEY_USER_BASE;
        public const int KEY_SHIELDS_FULL = 69 + KEY_USER_BASE;
        public const int KEY_SHIELDS_ZERO = 70 + KEY_USER_BASE;
        public const int KEY_DECOY = 71 + KEY_USER_BASE;
        public const int KEY_ECM_TOGGLE = 72 + KEY_USER_BASE;
        public const int KEY_LAUNCH_PROBE = 73 + KEY_USER_BASE;
        public const int KEY_GEAR_TOGGLE = 74 + KEY_USER_BASE;

        public const int KEY_LOCK_CLOSEST_SHIP = 75 + KEY_USER_BASE;
        public const int KEY_LOCK_CLOSEST_THREAT = 76 + KEY_USER_BASE;
        public const int KEY_LOCK_HOSTILE_SHIP = 77 + KEY_USER_BASE;
        public const int KEY_LOCK_HOSTILE_THREAT = 78 + KEY_USER_BASE;
        public const int KEY_CYCLE_SUBTARGET = 79 + KEY_USER_BASE;
        public const int KEY_PREV_SUBTARGET = 80 + KEY_USER_BASE;

        public const int KEY_AUGMENTER = 81 + KEY_USER_BASE;
        public const int KEY_NAVLIGHT_TOGGLE = 82 + KEY_USER_BASE;

        public const int KEY_CAM_VIRT = 85 + KEY_USER_BASE;
        public const int KEY_CAM_VIRT_PLUS_AZ = 86 + KEY_USER_BASE;
        public const int KEY_CAM_VIRT_MINUS_AZ = 87 + KEY_USER_BASE;
        public const int KEY_CAM_VIRT_PLUS_EL = 88 + KEY_USER_BASE;
        public const int KEY_CAM_VIRT_MINUS_EL = 89 + KEY_USER_BASE;

        public const int KEY_COMM_ATTACK_TGT = 90 + KEY_USER_BASE;
        public const int KEY_COMM_ESCORT_TGT = 91 + KEY_USER_BASE;
        public const int KEY_COMM_WEP_FREE = 92 + KEY_USER_BASE;
        public const int KEY_COMM_WEP_HOLD = 93 + KEY_USER_BASE;
        public const int KEY_COMM_COVER_ME = 94 + KEY_USER_BASE;
        public const int KEY_COMM_SKIP_NAV = 95 + KEY_USER_BASE;
        public const int KEY_COMM_RETURN_TO_BASE = 96 + KEY_USER_BASE;
        public const int KEY_COMM_CALL_INBOUND = 97 + KEY_USER_BASE;
        public const int KEY_COMM_REQUEST_PICTURE = 98 + KEY_USER_BASE;
        public const int KEY_COMM_REQUEST_SUPPORT = 99 + KEY_USER_BASE;

        public const int KEY_CHAT_BROADCAST = 100 + KEY_USER_BASE;
        public const int KEY_CHAT_TEAM = 101 + KEY_USER_BASE;
        public const int KEY_CHAT_WING = 102 + KEY_USER_BASE;
        public const int KEY_CHAT_UNIT = 103 + KEY_USER_BASE;

        public const int KEY_COMMAND_MODE = 104 + KEY_USER_BASE;
        public const int KEY_SELF_DESTRUCT = 105 + KEY_USER_BASE;

        /*** For Debug Convenience Only: ***/
        public const int KEY_INC_STARDATE = 120 + KEY_USER_BASE;
        public const int KEY_DEC_STARDATE = 121 + KEY_USER_BASE;

        public const int VK_BACK = 0x08;
        public const int VK_TAB = 0x09;

        public const int VK_CANCEL = 0x03;
        public const int VK_ICO_HELP = 0xE3;
        public const int VK_KEY_0 = 0x30;// ('0')	0
        public const int VK_KEY_1 = 0x31;// ('1')	1
        public const int VK_KEY_2 = 0x32;// ('2')	2
        public const int VK_KEY_3 = 0x33;// ('3')	3
        public const int VK_KEY_4 = 0x34;// ('4')	4
        public const int VK_KEY_5 = 0x35;// ('5')	5
        public const int VK_KEY_6 = 0x36;// ('6')	6
        public const int VK_KEY_7 = 0x37;// ('7')	7
        public const int VK_KEY_8 = 0x38;// ('8')	8
        public const int VK_KEY_9 = 0x39;// ('9')	9
        public const int VK_KEY_A = 0x41;// ('A')	A
        public const int VK_KEY_B = 0x42;// ('B')	B
        public const int VK_KEY_C = 0x43;// ('C')	C
        public const int VK_KEY_D = 0x44;// ('D')	D
        public const int VK_KEY_E = 0x45;// ('E')	E
        public const int VK_KEY_F = 0x46;// ('F')	F
        public const int VK_KEY_G = 0x47;// ('G')	G
        public const int VK_KEY_H = 0x48;// ('H')	H
        public const int VK_KEY_I = 0x49;// ('I')	I
        public const int VK_KEY_J = 0x4A;// ('J')	J
        public const int VK_KEY_K = 0x4B;// ('K')	K
        public const int VK_KEY_L = 0x4C;// ('L')	L
        public const int VK_KEY_M = 0x4D;// ('M')	M
        public const int VK_KEY_N = 0x4E;// ('N')	N
        public const int VK_KEY_O = 0x4F;// ('O')	O
        public const int VK_KEY_P = 0x50;// ('P')	P
        public const int VK_KEY_Q = 0x51;// ('Q')	Q
        public const int VK_KEY_R = 0x52;// ('R')	R
        public const int VK_KEY_S = 0x53;// ('S')	S
        public const int VK_KEY_T = 0x54;// ('T')	T
        public const int VK_KEY_U = 0x55;// ('U')	U
        public const int VK_KEY_V = 0x56;// ('V')	V
        public const int VK_KEY_W = 0x57;// ('W')	W
        public const int VK_KEY_X = 0x58;// ('X')	X
        public const int VK_KEY_Y = 0x59;// ('Y')	Y
        public const int VK_KEY_Z = 0x5A;// ('Z')	Z  

        public const int VK_OEM_1 = 0xBA;//	OEM_1(: ;)
        public const int VK_OEM_102 = 0xE2;//	OEM_102(> <)
        public const int VK_OEM_2 = 0xBF;//	OEM_2(? /)
        public const int VK_OEM_3 = 0xC0;//	OEM_3(~ `)
        public const int VK_OEM_4 = 0xDB;//	OEM_4({ [)
        public const int VK_OEM_5 = 0xDC;//	OEM_5(| \)
        public const int VK_OEM_6 = 0xDD;//	OEM_6(} ])
        public const int VK_OEM_7 = 0xDE;//	OEM_7(" ')
        public const int VK_OEM_8 = 0xDF;//	OEM_8 (§ !)
        public const int VK_OEM_ATTN = 0xF0;//	Oem Attn
        public const int VK_OEM_CLEAR = 0xFE;//	OemClr
        public const int VK_OEM_COMMA = 0xBC;//	OEM_COMMA(< ,)
        public const int VK_OEM_MINUS = 0xBD;//	OEM_MINUS(_ -)
        public const int VK_OEM_PERIOD = 0xBE;//	OEM_PERIOD(> .)
        public const int VK_OEM_PLUS = 0xBB;//	OEM_PLUS(+ =)

        public const int VK__none_ = 0xFF;//	no VK mapping
        public const int VK_NONAME = 0xFC;//	NoName

        public const int VK_LCONTROL = 0xA2;//	Left Ctrl
        public const int VK_LMENU = 0xA4;//	Left Alt
        public const int VK_LSHIFT = 0xA0;//	Left Shift
        public const int VK_LWIN = 0x5B;//	Left Win
        public const int VK_RCONTROL = 0xA3;//	Right Ctrl
        public const int VK_RMENU = 0xA5;//	Right Alt
        public const int VK_RSHIFT = 0xA1;//	Right Shift
        public const int VK_RWIN = 0x5C;//	Right Win

        /*
         * 0x0A - 0x0B : reserved
         */
        public const int VK_CLEAR = 0x0C;
        public const int VK_RETURN = 0x0D;

        public const int VK_SHIFT = 0x10;
        public const int VK_CONTROL = 0x11;
        public const int VK_MENU = 0x12;
        public const int VK_PAUSE = 0x13;
        public const int VK_CAPITAL = 0x14;

        public const int VK_ESCAPE = 0x1B;

        public const int VK_CONVERT = 0x1C;
        public const int VK_NONCONVERT = 0x1D;
        public const int VK_ACCEPT = 0x1E;
        public const int VK_MODECHANGE = 0x1F;

        public const int VK_SPACE = 0x20;
        public const int VK_PRIOR = 0x21;
        public const int VK_NEXT = 0x22;
        public const int VK_END = 0x23;
        public const int VK_HOME = 0x24;
        public const int VK_LEFT = 0x25;
        public const int VK_UP = 0x26;
        public const int VK_RIGHT = 0x27;
        public const int VK_DOWN = 0x28;
        public const int VK_SELECT = 0x29;
        public const int VK_PRINT = 0x2A;
        public const int VK_EXECUTE = 0x2B;
        public const int VK_SNAPSHOT = 0x2C;
        public const int VK_INSERT = 0x2D;
        public const int VK_DELETE = 0x2E;
        public const int VK_HELP = 0x2F;
        public const int VK_SLEEP = 0x5F;

        public const int VK_NUMPAD0 = 0x60;
        public const int VK_NUMPAD1 = 0x61;
        public const int VK_NUMPAD2 = 0x62;
        public const int VK_NUMPAD3 = 0x63;
        public const int VK_NUMPAD4 = 0x64;
        public const int VK_NUMPAD5 = 0x65;
        public const int VK_NUMPAD6 = 0x66;
        public const int VK_NUMPAD7 = 0x67;
        public const int VK_NUMPAD8 = 0x68;
        public const int VK_NUMPAD9 = 0x69;
        public const int VK_MULTIPLY = 0x6A;
        public const int VK_ADD = 0x6B;
        public const int VK_SEPARATOR = 0x6C;
        public const int VK_SUBTRACT = 0x6D;
        public const int VK_DECIMAL = 0x6E;
        public const int VK_DIVIDE = 0x6F;
        public const int VK_F1 = 0x70;
        public const int VK_F2 = 0x71;
        public const int VK_F3 = 0x72;
        public const int VK_F4 = 0x73;
        public const int VK_F5 = 0x74;
        public const int VK_F6 = 0x75;
        public const int VK_F7 = 0x76;
        public const int VK_F8 = 0x77;
        public const int VK_F9 = 0x78;
        public const int VK_F10 = 0x79;
        public const int VK_F11 = 0x7A;
        public const int VK_F12 = 0x7B;
        public const int VK_F13 = 0x7C;
        public const int VK_F14 = 0x7D;
        public const int VK_F15 = 0x7E;
        public const int VK_F16 = 0x7F;
        public const int VK_F17 = 0x80;
        public const int VK_F18 = 0x81;
        public const int VK_F19 = 0x82;
        public const int VK_F20 = 0x83;
        public const int VK_F21 = 0x84;
        public const int VK_F22 = 0x85;
        public const int VK_F23 = 0x86;
        public const int VK_F24 = 0x87;

        public const int VK_MBUTTON = 0x0; //undefined??
        /*
         * 0x88 - 0x8F : unassigned
         */

        public const int VK_NUMLOCK = 0x90;
        public const int VK_SCROLL = 0x91;

        protected static KeyName[] key_action_table = new KeyName[]{
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_PLUS_X,          "KEY_PLUS_X",        "Translate Right"       ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_MINUS_X,         "KEY_MINUS_X",       "Translate Left"        ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_PLUS_Y,          "KEY_PLUS_Y",        "Translate Forward"     ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_MINUS_Y,         "KEY_MINUS_Y",       "Translate Aft"         ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_PLUS_Z,          "KEY_PLUS_Z",        "Translate Up"          ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_MINUS_Z,         "KEY_MINUS_Z",       "Translate Down"        ),

                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_PITCH_UP,        "KEY_PITCH_UP",      "Pitch Up"              ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_PITCH_DOWN,      "KEY_PITCH_DOWN",    "Pitch Down"            ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_YAW_LEFT,        "KEY_YAW_LEFT",      "Yaw Left"              ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_YAW_RIGHT,       "KEY_YAW_RIGHT",     "Yaw Right"             ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_ROLL_LEFT,       "KEY_ROLL_LEFT",     "Roll Left"             ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_ROLL_RIGHT,      "KEY_ROLL_RIGHT",    "Roll Right"            ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_CENTER,          "KEY_CENTER",        "Center"                ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_ROLL_ENABLE,     "KEY_ROLL_ENABLE",   "Roll Enable"           ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_SWAP_ROLL_YAW,   "KEY_SWAP_ROLL_YAW", "Swap Ctrl Axes"        ),

                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_ACTION_0,        "KEY_ACTION_0",      "Fire Primary"   ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_ACTION_1,        "KEY_ACTION_1",      "Fire Secondary" ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_ACTION_2,        "KEY_ACTION_2",      "Action 2"       ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_ACTION_3,        "KEY_ACTION_3",      "Action 3"       ),

                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_CONTROL_MODEL,   "KEY_CONTROL_MODEL"  ),

                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_MOUSE_SELECT,    "KEY_MOUSE_SELECT"   ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_MOUSE_SENSE,     "KEY_MOUSE_SENSE"    ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_MOUSE_SWAP,      "KEY_MOUSE_SWAP"     ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_MOUSE_INVERT,    "KEY_MOUSE_INVERT"   ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_MOUSE_ACTIVE,    "KEY_MOUSE_ACTIVE",  "Toggle Mouse Ctrl"     ),

                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_JOY_SELECT,      "KEY_JOY_SELECT"     ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_JOY_RUDDER,      "KEY_JOY_RUDDER"     ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_JOY_THROTTLE,    "KEY_JOY_THROTTLE"   ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_JOY_SENSE,       "KEY_JOY_SENSE"      ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_JOY_DEAD_ZONE,   "KEY_JOY_DEAD_ZONE"  ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_JOY_SWAP,        "KEY_JOY_SWAP"       ),

                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_AXIS_YAW,               "KEY_AXIS_YAW"             ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_AXIS_PITCH,             "KEY_AXIS_PITCH"           ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_AXIS_ROLL,              "KEY_AXIS_ROLL"            ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_AXIS_THROTTLE,          "KEY_AXIS_THROTTLE"        ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_AXIS_YAW_INVERT,        "KEY_AXIS_YAW_INVERT"      ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_AXIS_PITCH_INVERT,      "KEY_AXIS_PITCH_INVERT"    ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_AXIS_ROLL_INVERT,       "KEY_AXIS_ROLL_INVERT"     ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_AXIS_THROTTLE_INVERT,   "KEY_AXIS_THROTTLE_INVERT" ),

                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_EXIT,            "KEY_EXIT",          "Exit"           ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_PAUSE,           "KEY_PAUSE",         "Pause"          ),
                // new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_NEXT_VIEW,       "KEY_NEXT_VIEW",     "Next View"      ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_TARGET_PADLOCK,  "KEY_TARGET_PADLOCK","Padlock Target" ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_THREAT_PADLOCK,  "KEY_THREAT_PADLOCK","Padlock Threat" ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_LOCK_TARGET,     "KEY_LOCK_TARGET",   "Lock Target"    ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_LOCK_THREAT,     "KEY_LOCK_THREAT",   "Lock Threat"    ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_LOCK_CLOSEST_SHIP,   "KEY_LOCK_CLOSEST_SHIP",     "Lock Closest Ship"    ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_LOCK_CLOSEST_THREAT, "KEY_LOCK_CLOSEST_THREAT",   "Lock Closest Threat"  ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_LOCK_HOSTILE_SHIP,   "KEY_LOCK_HOSTILE_SHIP",     "Lock Hostile Ship"    ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_LOCK_HOSTILE_THREAT, "KEY_LOCK_HOSTILE_THREAT",   "Lock Hostile Threat"  ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_CYCLE_SUBTARGET, "KEY_CYCLE_SUBTARGET",   "Target Subsystem"  ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_PREV_SUBTARGET,  "KEY_PREV_SUBTARGET",    "Previous Subsystem"  ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_AUTO_NAV,        "KEY_AUTO_NAV",      "Autonav"        ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_TIME_COMPRESS,   "KEY_TIME_COMPRESS", "Compress Time"  ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_TIME_EXPAND,     "KEY_TIME_EXPAND",   "Expand Time"    ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_TIME_SKIP,       "KEY_TIME_SKIP",     "Skip to Next Event" ),

                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_THROTTLE_UP,     "KEY_THROTTLE_UP",   "Throttle Up"    ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_THROTTLE_DOWN,   "KEY_THROTTLE_DOWN", "Throttle Down"  ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_THROTTLE_ZERO,   "KEY_THROTTLE_ZERO", "All Stop"       ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_THROTTLE_FULL,   "KEY_THROTTLE_FULL", "Full Throttle"  ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_AUGMENTER,       "KEY_AUGMENTER",     "Augmenter"      ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_FLCS_MODE_AUTO,  "KEY_FLCS_MODE_AUTO","FLCS Mode Toggle"  ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_COMMAND_MODE,    "KEY_COMMAND_MODE",  "CMD Mode Toggle"   ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_DROP_ORBIT,      "KEY_DROP_ORBIT",    "Break Orbit"    ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_GEAR_TOGGLE,     "KEY_GEAR_TOGGLE",   "Landing Gear"   ),
                new KeyName( KEY_CATEGORY.KEY_FLIGHT,  KEY_NAVLIGHT_TOGGLE, "KEY_NAVLIGHT_TOGGLE","Nav Lights"    ),

                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_CYCLE_PRIMARY,   "KEY_CYCLE_PRIMARY", "Cycle Primary"  ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_CYCLE_SECONDARY, "KEY_CYCLE_SECONDARY", "Cycle Secondary" ),

                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_HUD_INST,        "KEY_HUD_INST",      "Instr. Display" ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_BRIDGE,      "KEY_CAM_BRIDGE",    "Bridge Cam"     ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_VIRT,        "KEY_CAM_VIRT",      "Virtual Cockpit" ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_CHASE,       "KEY_CAM_CHASE",     "Chase Cam"      ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_DROP,        "KEY_CAM_DROP",      "Drop Cam"       ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_EXTERN,      "KEY_CAM_EXTERN",    "Orbit Cam"      ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_HUD_MODE,        "KEY_HUD_MODE",      "HUD Mode"       ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_HUD_COLOR,       "KEY_HUD_COLOR",     "HUD Color"      ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_HUD_WARN,        "KEY_HUD_WARN",      "Master Caution" ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_NAV_DLG,         "KEY_NAV_DLG",       "NAV Window"     ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_WEP_DLG,         "KEY_WEP_DLG",       "TAC Overlay"    ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_FLT_DLG,         "KEY_FLT_DLG",       "FLT Window"     ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_ENG_DLG,         "KEY_ENG_DLG",       "ENG Window"     ),

                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_ZOOM_WIDE,       "KEY_ZOOM_WIDE",     "Toggle Wide Angle"      ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_ZOOM_IN,         "KEY_ZOOM_IN",       "Zoom In"        ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_ZOOM_OUT,        "KEY_ZOOM_OUT",      "Zoom Out"       ),

                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_VIRT_PLUS_AZ,   "KEY_CAM_VIRT_PLUS_AZ",       "Look Left"        ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_VIRT_MINUS_AZ,  "KEY_CAM_VIRT_MINUS_AZ",      "Look Right"       ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_VIRT_PLUS_EL,   "KEY_CAM_VIRT_PLUS_EL",       "Look Up"          ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_VIRT_MINUS_EL,  "KEY_CAM_VIRT_MINUS_EL",      "Look Down"        ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_CYCLE_OBJECT,   "KEY_CAM_CYCLE_OBJECT",       "View Next Object" ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_EXT_PLUS_AZ,    "KEY_CAM_EXT_PLUS_AZ",        "View Spin Left"   ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_EXT_MINUS_AZ,   "KEY_CAM_EXT_MINUS_AZ",       "View Spin Right"  ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_EXT_PLUS_EL,    "KEY_CAM_EXT_PLUS_EL",        "View Raise"       ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_EXT_MINUS_EL,   "KEY_CAM_EXT_MINUS_EL",       "View Lower"       ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_EXT_PLUS_RANGE, "KEY_CAM_EXT_PLUS_RANGE",     "View Farther"     ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_EXT_MINUS_RANGE,"KEY_CAM_EXT_MINUS_RANGE",    "View Closer"      ),
                new KeyName( KEY_CATEGORY.KEY_VIEW,    KEY_CAM_VIEW_SELECTION, "KEY_CAM_VIEW_SELECTION",     "View Selection"   ),

                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_TARGET_SELECTION,   "KEY_TARGET_SELECTION",       "Target Selection" ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_RADIO_MENU,         "KEY_RADIO_MENU",             "Radio Call"       ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_QUANTUM_MENU,       "KEY_QUANTUM_MENU",           "Quantum Drive"    ),

                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_MFD1,               "KEY_MFD1", "MFD 1"                 ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_MFD2,               "KEY_MFD2", "MFD 2"                 ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_MFD3,               "KEY_MFD3", "MFD 3"                 ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_MFD4,               "KEY_MFD4", "MFD 4"                 ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_SELF_DESTRUCT,      "KEY_SELF_DESTRUCT",       "Self Destruct"                 ),

                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_COMM_ATTACK_TGT,    "KEY_COMM_ATTACK_TGT",     "'Attack Tgt'"       ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_COMM_ESCORT_TGT,    "KEY_COMM_ESCORT_TGT",     "'Escort Tgt'"       ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_COMM_WEP_FREE,      "KEY_COMM_WEP_FREE",       "'Break & Attack'"   ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_COMM_WEP_HOLD,      "KEY_COMM_WEP_HOLD",       "'Form Up'"          ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_COMM_COVER_ME,      "KEY_COMM_COVER_ME",       "'Help Me Out!'"     ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_COMM_SKIP_NAV,      "KEY_COMM_SKIP_NAV",       "'Skip Navpoint'"    ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_COMM_RETURN_TO_BASE,"KEY_COMM_RETURN_TO_BASE", "'Return to Base'"   ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_COMM_CALL_INBOUND,  "KEY_COMM_CALL_INBOUND",   "'Call Inbound'"     ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_COMM_REQUEST_PICTURE, "KEY_COMM_REQUEST_PICTURE", "'Request Picture'"     ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_COMM_REQUEST_SUPPORT, "KEY_COMM_REQUEST_SUPPORT", "'Request Support'"     ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_CHAT_BROADCAST,     "KEY_CHAT_BROADCAST",      "Chat Broadcast" ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_CHAT_TEAM,          "KEY_CHAT_TEAM",           "Chat Team" ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_CHAT_WING,          "KEY_CHAT_WING",           "Chat Wingman" ),
                new KeyName( KEY_CATEGORY.KEY_MISC,    KEY_CHAT_UNIT,          "KEY_CHAT_UNIT",           "Chat Unit" ),

                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_SHIELDS_UP,         "KEY_SHIELDS_UP",          "Raise Shields" ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_SHIELDS_DOWN,       "KEY_SHIELDS_DOWN",        "Lower Shields" ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_SHIELDS_FULL,       "KEY_SHIELDS_FULL",        "Full Shields" ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_SHIELDS_ZERO,       "KEY_SHIELDS_ZERO",        "Zero Shields" ),

                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_SENSOR_MODE,        "KEY_SENSOR_MODE",         "Sensor Mode"  ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_SENSOR_GROUND_MODE, "KEY_SENSOR_GROUND_MODE",  "Sensor AGM"   ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_SENSOR_BEAM,        "KEY_SENSOR_BEAM",         "Sensor Sweep Angle" ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_SENSOR_RANGE_PLUS,  "KEY_SENSOR_RANGE_PLUS",   "Inc Sensor Range" ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_SENSOR_RANGE_MINUS, "KEY_SENSOR_RANGE_MINUS",  "Dec Sensor Range" ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_EMCON_PLUS,         "KEY_EMCON_PLUS",          "Inc EMCON Level" ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_EMCON_MINUS,        "KEY_EMCON_MINUS",         "Dec EMCON Level" ),

                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_DECOY,              "KEY_DECOY",               "Launch Decoy" ),
                new KeyName( KEY_CATEGORY.KEY_WEAPONS, KEY_LAUNCH_PROBE,       "KEY_LAUNCH_PROBE",        "Launch Probe" ),

            };

        static KeyName[] key_key_table = new KeyName[]{
                    new KeyName( 0, VK_ESCAPE,            "VK_ESCAPE",      "Escape"       ),
                    new KeyName( 0, VK_UP,                "VK_UP",          "Up"           ),
                    new KeyName( 0, VK_DOWN,              "VK_DOWN",        "Down"         ),
                    new KeyName( 0, VK_LEFT,              "VK_LEFT",        "Left"         ),
                    new KeyName( 0, VK_RIGHT,             "VK_RIGHT",       "Right"        ),
                    new KeyName( 0, VK_NEXT,              "VK_NEXT",        "Pg Down"      ),
                    new KeyName( 0, VK_PRIOR,             "VK_PRIOR",       "Pg Up"        ),
                    new KeyName( 0, VK_ADD,               "VK_ADD",         "+"            ),
                    new KeyName( 0, VK_SUBTRACT,          "VK_SUBTRACT",    "-"            ),
                    new KeyName( 0, VK_MULTIPLY,          "VK_MULTIPLY",    "*"            ),
                    new KeyName( 0, VK_DIVIDE,            "VK_DIVIDE",      "/"            ),
                    new KeyName( 0, VK_SPACE,             "VK_SPACE",       "Space"        ),
                    new KeyName( 0, VK_TAB,               "VK_TAB",         "Tab"          ),
                    new KeyName( 0, VK_RETURN,            "VK_RETURN",      "Enter"        ),
                    new KeyName( 0, VK_HOME,              "VK_HOME",        "Home"         ),
                    new KeyName( 0, VK_END,               "VK_END",         "End"          ),
                    new KeyName( 0, VK_SHIFT,             "VK_SHIFT",       "Shift"        ),
                    new KeyName( 0, VK_CONTROL,           "VK_CONTROL",     "Ctrl"         ),
                    new KeyName( 0, VK_MENU,              "VK_MENU",        "Alt"          ),

                    new KeyName( 0, VK_DECIMAL,           "VK_DECIMAL",     "."            ),
                    new KeyName( 0, VK_SEPARATOR,         "VK_SEPARATOR",   "Separator"    ),
                    new KeyName( 0, VK_PAUSE,             "VK_PAUSE",       "Pause"        ),
                    new KeyName( 0, VK_BACK,              "VK_BACK",        "Backspace"    ),
                    new KeyName( 0, VK_INSERT,            "VK_INSERT",      "Insert"       ),
                    new KeyName( 0, VK_DELETE,            "VK_DELETE",      "Delete"       ),
                    new KeyName( 0, 20,                   "CAP",            "CapsLock"     ),
                    new KeyName( 0, 144,                  "NUM",            "NumLock"      ),
                    new KeyName( 0, 145,                  "SCROLL",         "Scroll"       ),
                    new KeyName( 0, 44,                   "PRINT",          "PrintScr"     ),

                    new KeyName( 0, VK_NUMPAD0,           "VK_NUMPAD0",     "Num 0"        ),
                    new KeyName( 0, VK_NUMPAD1,           "VK_NUMPAD1",     "Num 1"        ),
                    new KeyName( 0, VK_NUMPAD2,           "VK_NUMPAD2",     "Num 2"        ),
                    new KeyName( 0, VK_NUMPAD3,           "VK_NUMPAD3",     "Num 3"        ),
                    new KeyName( 0, VK_NUMPAD4,           "VK_NUMPAD4",     "Num 4"        ),
                    new KeyName( 0, VK_NUMPAD5,           "VK_NUMPAD5",     "Num 5"        ),
                    new KeyName( 0, VK_NUMPAD6,           "VK_NUMPAD6",     "Num 6"        ),
                    new KeyName( 0, VK_NUMPAD7,           "VK_NUMPAD7",     "Num 7"        ),
                    new KeyName( 0, VK_NUMPAD8,           "VK_NUMPAD8",     "Num 8"        ),
                    new KeyName( 0, VK_NUMPAD9,           "VK_NUMPAD9",     "Num 9"        ),

                    new KeyName( 0, VK_F1,                "VK_F1",          "F1"           ),
                    new KeyName( 0, VK_F2,                "VK_F2",          "F2"           ),
                    new KeyName( 0, VK_F3,                "VK_F3",          "F3"           ),
                    new KeyName( 0, VK_F4,                "VK_F4",          "F4"           ),
                    new KeyName( 0, VK_F5,                "VK_F5",          "F5"           ),
                    new KeyName( 0, VK_F6,                "VK_F6",          "F6"           ),
                    new KeyName( 0, VK_F7,                "VK_F7",          "F7"           ),
                    new KeyName( 0, VK_F8,                "VK_F8",          "F8"           ),
                    new KeyName( 0, VK_F9,                "VK_F9",          "F9"           ),
                    new KeyName( 0, VK_F10,               "VK_F10",         "F10"          ),
                    new KeyName( 0, VK_F11,               "VK_F11",         "F11"          ),
                    new KeyName( 0, VK_F12,               "VK_F12",         "F12"          ),

                    new KeyName( 0, KEY_JOY_1,            "KEY_JOY_1",      "Joy 1"        ),
                    new KeyName( 0, KEY_JOY_2,            "KEY_JOY_2",      "Joy 2"        ),
                    new KeyName( 0, KEY_JOY_3,            "KEY_JOY_3",      "Joy 3"        ),
                    new KeyName( 0, KEY_JOY_4,            "KEY_JOY_4",      "Joy 4"        ),
                    new KeyName( 0, KEY_JOY_5,            "KEY_JOY_5",      "Joy 5"        ),
                    new KeyName( 0, KEY_JOY_6,            "KEY_JOY_6",      "Joy 6"        ),
                    new KeyName( 0, KEY_JOY_7,            "KEY_JOY_7",      "Joy 7"        ),
                    new KeyName( 0, KEY_JOY_8,            "KEY_JOY_8",      "Joy 8"        ),
                    new KeyName( 0, KEY_JOY_9,            "KEY_JOY_9",      "Joy 9"        ),
                    new KeyName( 0, KEY_JOY_10,           "KEY_JOY_10",     "Joy 10"       ),
                    new KeyName( 0, KEY_JOY_11,           "KEY_JOY_11",     "Joy 11"       ),
                    new KeyName( 0, KEY_JOY_12,           "KEY_JOY_12",     "Joy 12"       ),
                    new KeyName( 0, KEY_JOY_13,           "KEY_JOY_13",     "Joy 13"       ),
                    new KeyName( 0, KEY_JOY_14,           "KEY_JOY_14",     "Joy 14"       ),
                    new KeyName( 0, KEY_JOY_15,           "KEY_JOY_15",     "Joy 15"       ),
                    new KeyName( 0, KEY_JOY_16,           "KEY_JOY_16",     "Joy 16"       ),

                    new KeyName( 0, KEY_POV_0_UP,         "KEY_POV_0_UP",      "Hat 1 Up"     ),
                    new KeyName( 0, KEY_POV_0_DOWN,       "KEY_POV_0_DOWN",    "Hat 1 Down"   ),
                    new KeyName( 0, KEY_POV_0_LEFT,       "KEY_POV_0_LEFT",    "Hat 1 Left"   ),
                    new KeyName( 0, KEY_POV_0_RIGHT,      "KEY_POV_0_RIGHT",   "Hat 1 Right"  ),
                    new KeyName( 0, KEY_POV_1_UP,         "KEY_POV_1_UP",      "Hat 2 Up"     ),
                    new KeyName( 0, KEY_POV_1_DOWN,       "KEY_POV_1_DOWN",    "Hat 2 Down"   ),
                    new KeyName( 0, KEY_POV_1_LEFT,       "KEY_POV_1_LEFT",    "Hat 2 Left"   ),
                    new KeyName( 0, KEY_POV_1_RIGHT,      "KEY_POV_1_RIGHT",   "Hat 2 Right"  ),
                    new KeyName( 0, KEY_POV_2_UP,         "KEY_POV_2_UP",      "Hat 3 Up"     ),
                    new KeyName( 0, KEY_POV_2_DOWN,       "KEY_POV_2_DOWN",    "Hat 3 Down"   ),
                    new KeyName( 0, KEY_POV_2_LEFT,       "KEY_POV_2_LEFT",    "Hat 3 Left"   ),
                    new KeyName( 0, KEY_POV_2_RIGHT,      "KEY_POV_2_RIGHT",   "Hat 3 Right"  ),
                    new KeyName( 0, KEY_POV_3_UP,         "KEY_POV_3_UP",      "Hat 4 Up"     ),
                    new KeyName( 0, KEY_POV_3_DOWN,       "KEY_POV_3_DOWN",    "Hat 4 Down"   ),
                    new KeyName( 0, KEY_POV_3_LEFT,       "KEY_POV_3_LEFT",    "Hat 4 Left"   ),
                    new KeyName( 0, KEY_POV_3_RIGHT,      "KEY_POV_3_RIGHT",   "Hat 4 Right"  ),

                    new KeyName( 0, 186,                  ";",   ";" ),
                    new KeyName( 0, 188,                  ",",   "<" ),
                    new KeyName( 0, 190,                  ".",   ">" ),
                    new KeyName( 0, 191,                  "/",   "/" ),
                    new KeyName( 0, 192,                  "~",   "~" ),
                    new KeyName( 0, 219,                  "[",   "[" ),
                    new KeyName( 0, 220,                  "\\",  "\\" ),
                    new KeyName( 0, 221,                  "]",   "]" ),
                    new KeyName( 0, 222,                  "'",   "'" ),

                    new KeyName( 0, '0',                  "0",   "0" ),
                    new KeyName( 0, '1',                  "1",   "1" ),
                    new KeyName( 0, '2',                  "2",   "2" ),
                    new KeyName( 0, '3',                  "3",   "3" ),
                    new KeyName( 0, '4',                  "4",   "4" ),
                    new KeyName( 0, '5',                  "5",   "5" ),
                    new KeyName( 0, '6',                  "6",   "6" ),
                    new KeyName( 0, '7',                  "7",   "7" ),
                    new KeyName( 0, '8',                  "8",   "8" ),
                    new KeyName( 0, '9',                  "9",   "9" ),

                    new KeyName( 0, 'A',                  "A",   "A" ),
                    new KeyName( 0, 'B',                  "B",   "B" ),
                    new KeyName( 0, 'C',                  "C",   "C" ),
                    new KeyName( 0, 'D',                  "D",   "D" ),
                    new KeyName( 0, 'E',                  "E",   "E" ),
                    new KeyName( 0, 'F',                  "F",   "F" ),
                    new KeyName( 0, 'G',                  "G",   "G" ),
                    new KeyName( 0, 'H',                  "H",   "H" ),
                    new KeyName( 0, 'I',                  "I",   "I" ),
                    new KeyName( 0, 'J',                  "J",   "J" ),
                    new KeyName( 0, 'K',                  "K",   "K" ),
                    new KeyName( 0, 'L',                  "L",   "L" ),
                    new KeyName( 0, 'M',                  "M",   "M" ),
                    new KeyName( 0, 'N',                  "N",   "N" ),
                    new KeyName( 0, 'O',                  "O",   "O" ),
                    new KeyName( 0, 'P',                  "P",   "P" ),
                    new KeyName( 0, 'Q',                  "Q",   "Q" ),
                    new KeyName( 0, 'R',                  "R",   "R" ),
                    new KeyName( 0, 'S',                  "S",   "S" ),
                    new KeyName( 0, 'T',                  "T",   "T" ),
                    new KeyName( 0, 'U',                  "U",   "U" ),
                    new KeyName( 0, 'V',                  "V",   "V" ),
                    new KeyName( 0, 'W',                  "W",   "W" ),
                    new KeyName( 0, 'X',                  "X",   "X" ),
                    new KeyName( 0, 'Y',                  "Y",   "Y" ),
                    new KeyName( 0, 'Z',                  "Z",   "Z" ),

                };

        protected struct KeyName
        {
            public KeyName(KEY_CATEGORY c, int k, string n)
            {
                this.category = c;
                this.key = k;
                this.name = n;
                this.desc = null;
            }

            public KeyName(KEY_CATEGORY c, int k, string n, string d)
            {
                this.category = c;
                this.key = k;
                this.name = n;
                this.desc = d;
            }
            public KEY_CATEGORY category;
            public int key;
            public string name;
            public string desc;
        }
    }

    public static class UnityKeyMap
    {
        public static KeyCode ToKeyCode(int key)
        {
            KeyCode code;
            if (directDict.TryGetValue(key, out code))
                return code;
            else
                return KeyCode.None;
        }

        public static readonly Dictionary<int, KeyCode> directDict = SetDirectMappings();

        public static Dictionary<int, KeyCode> SetDirectMappings()
        {
            Dictionary<int, KeyCode> map = new Dictionary<int, KeyCode>();

            map[KeyMap.VK_ADD] = KeyCode.Plus;// 0x6B;//	Numpad +
            map[KeyMap.VK_BACK] = KeyCode.Backspace;// 0x08;//	Backspace
            map[KeyMap.VK_CANCEL] = KeyCode.Break;// 0x03;//	Break
            map[KeyMap.VK_CLEAR] = KeyCode.Clear;//0x0C;//	Clear
            map[KeyMap.VK_DECIMAL] = KeyCode.Numlock;//0x6E;//	Numpad.
            map[KeyMap.VK_DIVIDE] = KeyCode.KeypadDivide;//0x6F;//	Numpad /
            map[KeyMap.VK_ESCAPE] = KeyCode.Escape;//0x1B;//	Esc
            map[KeyMap.VK_ICO_HELP] = KeyCode.Help;//0xE3;//	IcoHlp

            map[KeyMap.VK_KEY_0] = KeyCode.Alpha0;//0x30;// ('0')	0
            map[KeyMap.VK_KEY_1] = KeyCode.Alpha1;//0x31;// ('1')	1
            map[KeyMap.VK_KEY_2] = KeyCode.Alpha2;//0x32;// ('2')	2
            map[KeyMap.VK_KEY_3] = KeyCode.Alpha3;//0x33;// ('3')	3
            map[KeyMap.VK_KEY_4] = KeyCode.Alpha4;//0x34;// ('4')	4
            map[KeyMap.VK_KEY_5] = KeyCode.Alpha5;//0x35;// ('5')	5
            map[KeyMap.VK_KEY_6] = KeyCode.Alpha6;//0x36;// ('6')	6
            map[KeyMap.VK_KEY_7] = KeyCode.Alpha7;//0x37;// ('7')	7
            map[KeyMap.VK_KEY_8] = KeyCode.Alpha8;//0x38;// ('8')	8
            map[KeyMap.VK_KEY_9] = KeyCode.Alpha0;//0x39;// ('9')	9
            map[KeyMap.VK_KEY_A] = KeyCode.A;//0x41;// ('A')	A
            map[KeyMap.VK_KEY_B] = KeyCode.B;//0x42;// ('B')	B
            map[KeyMap.VK_KEY_C] = KeyCode.C;//0x43;// ('C')	C
            map[KeyMap.VK_KEY_D] = KeyCode.D;//0x44;// ('D')	D
            map[KeyMap.VK_KEY_E] = KeyCode.E;//0x45;// ('E')	E
            map[KeyMap.VK_KEY_F] = KeyCode.F;//0x46;// ('F')	F
            map[KeyMap.VK_KEY_G] = KeyCode.G;//0x47;// ('G')	G
            map[KeyMap.VK_KEY_H] = KeyCode.H;//0x48;// ('H')	H
            map[KeyMap.VK_KEY_I] = KeyCode.I;//0x49;// ('I')	I
            map[KeyMap.VK_KEY_J] = KeyCode.J;//0x4A;// ('J')	J
            map[KeyMap.VK_KEY_K] = KeyCode.K;//0x4B;// ('K')	K
            map[KeyMap.VK_KEY_L] = KeyCode.L;//0x4C;// ('L')	L
            map[KeyMap.VK_KEY_M] = KeyCode.M;//0x4D;// ('M')	M
            map[KeyMap.VK_KEY_N] = KeyCode.N;//0x4E;// ('N')	N
            map[KeyMap.VK_KEY_O] = KeyCode.O;//0x4F;// ('O')	O
            map[KeyMap.VK_KEY_P] = KeyCode.P;//0x50;// ('P')	P
            map[KeyMap.VK_KEY_Q] = KeyCode.Q;//0x51;// ('Q')	Q
            map[KeyMap.VK_KEY_R] = KeyCode.R;//0x52;// ('R')	R
            map[KeyMap.VK_KEY_S] = KeyCode.S;//0x53;// ('S')	S
            map[KeyMap.VK_KEY_T] = KeyCode.T;//0x54;// ('T')	T
            map[KeyMap.VK_KEY_U] = KeyCode.U;//0x55;// ('U')	U
            map[KeyMap.VK_KEY_V] = KeyCode.V;//0x56;// ('V')	V
            map[KeyMap.VK_KEY_W] = KeyCode.W;//0x57;// ('W')	W
            map[KeyMap.VK_KEY_X] = KeyCode.X;//0x58;// ('X')	X
            map[KeyMap.VK_KEY_Y] = KeyCode.Y;//0x59;// ('Y')	Y
            map[KeyMap.VK_KEY_Z] = KeyCode.Z;// 0x5A;// ('Z')	Z

            map[KeyMap.VK_MULTIPLY] = KeyCode.KeypadMultiply;// 0x6A;//	Numpad*
            map[KeyMap.VK_NONAME] = KeyCode.None;// 0xFC;//	NoName

            map[KeyMap.VK_NUMPAD0] = KeyCode.Keypad0;//0x60;//	Numpad 0
            map[KeyMap.VK_NUMPAD1] = KeyCode.Keypad1;//0x61;//	Numpad 1
            map[KeyMap.VK_NUMPAD2] = KeyCode.Keypad2;//0x62;//	Numpad 2
            map[KeyMap.VK_NUMPAD3] = KeyCode.Keypad3;//0x63;//	Numpad 3
            map[KeyMap.VK_NUMPAD4] = KeyCode.Keypad4;//0x64;//	Numpad 4
            map[KeyMap.VK_NUMPAD5] = KeyCode.Keypad5;//0x65;//	Numpad 5
            map[KeyMap.VK_NUMPAD6] = KeyCode.Keypad6;//0x66;//	Numpad 6
            map[KeyMap.VK_NUMPAD7] = KeyCode.Keypad7;//0x67;//	Numpad 7
            map[KeyMap.VK_NUMPAD8] = KeyCode.Keypad8;//0x68;//	Numpad 8
            map[KeyMap.VK_NUMPAD9] = KeyCode.Keypad9;//0x69;//	Numpad 9

            map[KeyMap.VK_OEM_1] = KeyCode.Colon;// 0xBA;//	OEM_1(: ;)
            map[KeyMap.VK_OEM_102] = KeyCode.Greater;//0xE2;//	OEM_102(> <)
            map[KeyMap.VK_OEM_2] = KeyCode.Question;//0xBF;//	OEM_2(? /)
            map[KeyMap.VK_OEM_3] = KeyCode.BackQuote;//0xC0;//	OEM_3(~ `)
            map[KeyMap.VK_OEM_4] = KeyCode.LeftBracket;//0xDB;//	OEM_4({ [)
            map[KeyMap.VK_OEM_5] = KeyCode.Backslash;//0xDC;//	OEM_5(| \)
            map[KeyMap.VK_OEM_6] = KeyCode.RightBracket;//0xDD;//	OEM_6(} ])
            map[KeyMap.VK_OEM_7] = KeyCode.DoubleQuote;//0xDE;//	OEM_7(" ')
            map[KeyMap.VK_OEM_8] = KeyCode.Exclaim;//0xDF;//	OEM_8 (§ !)
            map[KeyMap.VK_OEM_ATTN] = KeyCode.At;//0xF0;//	Oem Attn
            map[KeyMap.VK_OEM_CLEAR] = KeyCode.Clear;//0xFE;//	OemClr
            map[KeyMap.VK_OEM_COMMA] = KeyCode.Comma;//0xBC;//	OEM_COMMA(< ,)
            map[KeyMap.VK_OEM_MINUS] = KeyCode.Minus;//0xBD;//	OEM_MINUS(_ -)
            map[KeyMap.VK_OEM_PERIOD] = KeyCode.Period;//0xBE;//	OEM_PERIOD(> .)
            map[KeyMap.VK_OEM_PLUS] = KeyCode.Plus;//0xBB;//	OEM_PLUS(+ =)
            map[KeyMap.VK_RETURN] = KeyCode.Return;//0x0D;//	Enter
            map[KeyMap.VK_SPACE] = KeyCode.Space;//0x20;//	Space
            map[KeyMap.VK_SUBTRACT] = KeyCode.KeypadMinus;//0x6D;//	Num -
            map[KeyMap.VK_TAB] = KeyCode.Tab;//0x09;//	Tab

            map[KeyMap.VK__none_] = KeyCode.None;//0xFF;//	no VK mapping
            map[KeyMap.VK_CAPITAL] = KeyCode.CapsLock;//0x14;//	Caps Lock
            map[KeyMap.VK_DELETE] = KeyCode.Delete;//0x2E;//	Delete
            map[KeyMap.VK_DOWN] = KeyCode.DownArrow;//0x28;//	Arrow Down
            map[KeyMap.VK_END] = KeyCode.End;//0x23;//	End

            map[KeyMap.VK_F1] = KeyCode.F1;//0x70;//	F1
            map[KeyMap.VK_F2] = KeyCode.F2;//0x71;//	F2
            map[KeyMap.VK_F3] = KeyCode.F3;//0x72;//	F3
            map[KeyMap.VK_F4] = KeyCode.F4;//0x73;//	F4
            map[KeyMap.VK_F5] = KeyCode.F5;//0x74;//	F5
            map[KeyMap.VK_F6] = KeyCode.F6;//0x75;//	F6
            map[KeyMap.VK_F7] = KeyCode.F7;//0x76;//	F7
            map[KeyMap.VK_F8] = KeyCode.F8;//0x77;//	F8
            map[KeyMap.VK_F9] = KeyCode.F9;//0x78;//	F9
            map[KeyMap.VK_F10] = KeyCode.F10;//0x79;//	F10
            map[KeyMap.VK_F11] = KeyCode.F11;//0x7A;//	F11
            map[KeyMap.VK_F12] = KeyCode.F12;//0x7B;//	F12
            map[KeyMap.VK_F13] = KeyCode.F13;//0x7C;//	F13
            map[KeyMap.VK_F14] = KeyCode.F14;//0x7D;//	F14
            map[KeyMap.VK_F15] = KeyCode.F15;//0x7E;//	F15

            map[KeyMap.VK_HELP] = KeyCode.Help;//0x2F;//	Help
            map[KeyMap.VK_HOME] = KeyCode.Home;//0x24;//	Home
            map[KeyMap.VK_INSERT] = KeyCode.Insert;//0x2D;//	Insert
            map[KeyMap.VK_LCONTROL] = KeyCode.LeftControl;//0xA2;//	Left Ctrl
            map[KeyMap.VK_LEFT] = KeyCode.LeftArrow;//0x25;//	Arrow Left
            map[KeyMap.VK_LMENU] = KeyCode.LeftAlt;//0xA4;//	Left Alt
            map[KeyMap.VK_LSHIFT] = KeyCode.LeftShift;//0xA0;//	Left Shift
            map[KeyMap.VK_LWIN] = KeyCode.LeftWindows;//0x5B;//	Left Win
            map[KeyMap.VK_NEXT] = KeyCode.PageDown;//0x22;//	Page Down
            map[KeyMap.VK_NUMLOCK] = KeyCode.Numlock;//0x90;//	Num Lock
            map[KeyMap.VK_PAUSE] = KeyCode.Pause;//0x13;//	Pause
            map[KeyMap.VK_PRINT] = KeyCode.Print;//0x2A;//	Print
            map[KeyMap.VK_PRIOR] = KeyCode.PageUp;//0x21;//	Page Up
            map[KeyMap.VK_RCONTROL] = KeyCode.RightControl;//0xA3;//	Right Ctrl
            map[KeyMap.VK_RIGHT] = KeyCode.RightArrow;//0x27;//	Arrow Right
            map[KeyMap.VK_RMENU] = KeyCode.RightAlt;//0xA5;//	Right Alt
            map[KeyMap.VK_RSHIFT] = KeyCode.RightShift;//0xA1;//	Right Shift
            map[KeyMap.VK_RWIN] = KeyCode.RightWindows;//0x5C;//	Right Win
            map[KeyMap.VK_SCROLL] = KeyCode.ScrollLock;//0x91;//	Scrol Lock
            map[KeyMap.VK_SNAPSHOT] = KeyCode.SysReq;//0x2C;//	Print Screen
            map[KeyMap.VK_UP] = KeyCode.UpArrow;//0x26;//	Arrow Up

            return map;
        }
    }
}
