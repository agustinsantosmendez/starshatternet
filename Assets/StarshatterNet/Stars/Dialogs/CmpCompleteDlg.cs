﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmpCompleteDlg.h/CmpCompleteDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Campaign title card and load progress dialog
*/
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmpCompleteDlg : CmdDlg
    {
        public CmpCompleteDlg(Screen s, Camera c, FormDef def, CmpnScreen mgr, GameObject p) :
               base(s, mgr, 0, 0, s.Width(), s.Height(), c, p, "CmpCompleteDlg")
        {
            lbl_info = null; img_title = null; btn_close = null;

            Init(def);
        }
        //public virtual ~CmpCompleteDlg();
        public override void RegisterControls()
        {
            img_title = (ImageBox)FindControl(100);
            lbl_info = FindControl(101);
            btn_close = (Button)FindControl(1);

            REGISTER_CLIENT(ETD.EID_CLICK, btn_close, OnClose);
        }

        public override void Show()
        {
            base.Show();

            Campaign c = Campaign.GetCampaign();

            if (img_title != null && c != null)
            {
                CombatEvent cevent = c.GetLastEvent();
                string img_name;

                if (cevent != null)
                {
                    img_name = cevent.ImageFile();

                    //if (!strstr(img_name, ".pcx"))
                    //{
                    //    strcat_s(img_name, ".pcx");
                    //}
#if TODO
                    if (loader)
                    {
                        loader.SetDataPath(c.Path());
                        loader.LoadBitmap(img_name, banner);
                        loader.SetDataPath(0);

                        Rect tgt_rect = new Rect();
                        tgt_rect.w = img_title.Width();
                        tgt_rect.h = img_title.Height();

                        img_title.SetTargetRect(tgt_rect);
                        img_title.SetPicture(banner);
                    }
#endif
                    ErrLogger.PrintLine("WARNING: CmpCompleteDlg.Show is not yet implemented.");
                }
            }
        }
        // Operations:
        public override void ExecFrame() { }
        public virtual void OnClose(ActiveWindow obj, AWEvent evnt)
        {
            if (cmpn_screen != null)
                cmpn_screen.ShowCmdDlg();
        }

        protected ImageBox img_title;
        protected ActiveWindow lbl_info;
        protected Button btn_close;
        protected Bitmap banner;

        protected CmpnScreen manager;
    }
}