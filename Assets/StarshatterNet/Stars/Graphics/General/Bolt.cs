﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    FILE:         Bolt.h/Bolt.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    3D Bolt (Polygon) Object
*/
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Stars.Graphics.General;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using DWORD = System.UInt32;
using StarshatterNet.Stars;
using System;
using StarshatterNet.Gen.Graphics;

namespace Assets.StarshatterNet.Stars.Graphics.General
{
    public class Bolt : Graphic
    {
        public Bolt(double len = 16, double wid = 1, Bitmap tex = null, bool share = false)
        {

            texture = tex; length = len; width = wid; shade = 1.0;
            vpn = new Point(0, 1, 0); shared = share;
            trans = true;

            loc = new Point(0.0f, 0.0f, 1000.0f);
#if TODO
            vset = 4; poly = 0;           
            vset.nverts = 4;

            vset.loc[0] = Point(width, 0, 1000);
            vset.loc[1] = Point(width, -length, 1000);
            vset.loc[2] = Point(-width, -length, 1000);
            vset.loc[3] = Point(-width, 0, 1000);

            vset.tu[0] = 0.0f;
            vset.tv[0] = 0.0f;
            vset.tu[1] = 1.0f;
            vset.tv[1] = 0.0f;
            vset.tu[2] = 1.0f;
            vset.tv[2] = 1.0f;
            vset.tu[3] = 0.0f;
            vset.tv[3] = 1.0f;

            Plane plane(vset.loc[0], vset.loc[1], vset.loc[2]);

            for (int i = 0; i < 4; i++)
            {
                vset.nrm[i] = plane.normal;
            }

            mtl.Ka = Color::White;
            mtl.Kd = Color::White;
            mtl.Ks = Color::Black;
            mtl.Ke = Color::White;
            mtl.tex_diffuse = texture;
            mtl.tex_emissive = texture;
            mtl.blend = Video::BLEND_ADDITIVE;

            poly.nverts = 4;
            poly.vertex_set = &vset;
            poly.material = &mtl;
            poly.verts[0] = 0;
            poly.verts[1] = 1;
            poly.verts[2] = 2;
            poly.verts[3] = 3;
#endif
            radius = (float)((length > width) ? (length) : (width * 2));

            if (texture != null)
            {
                name = texture.GetFilename();
            }
        }
        //public virtual ~Bolt();

        // operations
        // public virtual void Render(Video  video, DWORD flags);
        public virtual void Update() { }

        // accessors / mutators
        public override void SetOrientation(Matrix33D o)
        {
            vpn = new Point(o[2, 0], o[2, 1], o[2, 2]);
            origin = loc + (vpn * -length);
        }
        public void SetDirection(Point v)
        {
            vpn = v;
            origin = loc + (vpn * -length);
        }
        public void SetEndPoints(Point from, Point to)
        {
            loc = to;
            origin = from;
            vpn = to - from;
            length = vpn.Normalize();
            radius = (float)length;
        }
        public void SetTextureOffset(double from, double to)
        {
            throw new NotImplementedException();
        }

        public override void TranslateBy(Point ref_)
        {
            loc = loc - ref_;
            origin = origin - ref_;
        }


        public double Shade() { return shade; }
        public void SetShade(double s) { shade = s; }
        public override bool IsBolt() { return true; }


        protected double length;
        protected double width;
        protected double shade;

        // protected Poly poly;
        protected Material mtl;
        // protected VertexSet vset;
        protected Bitmap texture;
        protected bool shared;

        protected Point vpn;
        protected Point origin;
    }
}
