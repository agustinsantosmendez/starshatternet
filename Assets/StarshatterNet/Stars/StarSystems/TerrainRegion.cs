﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars.exe
    ORIGINAL FILE:      StarSystem.h/StarSystem.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Various heavenly bodies
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.StarSystems
{
    public class TerrainRegion : OrbitalRegion
    {

        public TerrainRegion(StarSystem sys, string n, float r, Orbital prime = null)
            : base(sys, n, 0.0, r, 0.0, prime)
        {
            scale = 0e3;
            mtnscale = 1e3;
            fog_density = 0;
            fog_scale = 0;
            day_phase = 0;
            eclipsed = false;
            type = OrbitalType.TERRAIN;

            if (primary != null)
            {
                orbit = primary.Radius();
                period = primary.Rotation();
            }

            clouds_alt_high = 12.0e3;
            clouds_alt_low = 8.0e3;

            Update();
        }
        // public virtual ~TerrainRegion();

        // operations:
        public override void Update()
        {
            if (system != null && primary != null)
            {
                phase = primary.RotationPhase();
                loc = primary.Location() + new Point((double)(orbit * Math.Cos(phase)),
                (double)(orbit * Math.Sin(phase)),
                0);
            }

            if (rep != null)
                rep.MoveTo(loc.OtherHand());

            // calc day phase:
            OrbitalBody star = system.Bodies()[0];

            Point tvpn = Location() - primary.Location();
            Point tvst = star.Location() - primary.Location();

            tvpn.Normalize();
            tvst.Normalize();

            day_phase = Math.Acos(Point.Dot(tvpn, tvst)) + Math.PI;

            Point meridian = Point.Cross(tvpn, tvst);

            if (meridian.Z < 0)
            {
                day_phase = 2 * Math.PI - day_phase;
            }

            day_phase *= 24 / (2 * Math.PI);

            if (system == null || system.ActiveRegion() != this)
                return;

            // set sky color:
            int base_hour = (int)day_phase;
            double fraction = day_phase - base_hour;

            sky_color[24] = ColorExtensions.Scale(sky_color[base_hour], sky_color[base_hour + 1], fraction);
            sun_color[24] = ColorExtensions.Scale(sun_color[base_hour], sun_color[base_hour + 1], fraction);
            fog_color[24] = ColorExtensions.Scale(fog_color[base_hour], fog_color[base_hour + 1], fraction);
            ambient[24] = ColorExtensions.Scale(ambient[base_hour], ambient[base_hour + 1], fraction);
            overcast[24] = ColorExtensions.Scale(overcast[base_hour], overcast[base_hour + 1], fraction);
            cloud_color[24] = ColorExtensions.Scale(cloud_color[base_hour], cloud_color[base_hour + 1], fraction);
            shade_color[24] = ColorExtensions.Scale(shade_color[base_hour], shade_color[base_hour + 1], fraction);

            CameraDirector cam_dir = CameraDirector.GetInstance();
            Sim sim = Sim.GetSim();
            double alt = 0;
            double dim = 1;

            if (cam_dir != null && cam_dir.GetCamera() != null)
                alt = cam_dir.GetCamera().Pos().Y;

            if (alt > 0)
            {
                if (alt < TerrainRegion.TERRAIN_ALTITUDE_LIMIT)
                {
                    if (weather.Ceiling() > 0)
                    {
                        fog_color[24] = overcast[24];
                        sky_color[24] = overcast[24];
                        dim = 0.125;

                        /***
                    if (alt < weather.Ceiling()) {
                    sky_color[24] = overcast[24];
                    dim = 0.125;
                    }
                    else if (alt < weather.Ceiling() + 500) {
                    double thick = (weather.Ceiling() + 500.0 - alt) / 500.0;
                    sky_color[24] =  sky_color[24] * (1.0 - thick);
                    sky_color[24] += overcast[24] * thick;

                    dim = 1 - thick * 0.875;
                    }
                    else {
                    sky_color[24] = sky_color[24] * (1 - alt/TERRAIN_ALTITUDE_LIMIT);
                    }
                    ***/
                    }

                    else
                    {
                        sky_color[24] = sky_color[24] * (float)(1 - alt / TERRAIN_ALTITUDE_LIMIT);
                    }
                }
                else
                {
                    sky_color[24] = Color.black;
                }
            }

            if (system != null)
            {
                system.SetSunlight(sun_color[24], dim);
                system.SetBacklight(sky_color[24], dim);

                HUDView hud = HUDView.GetInstance();
                if (hud != null)
                {
                    Color night_vision = hud.Ambient();
                    sky_color[24] += night_vision * 0.15f;
                }
            }
        }

        // accessors:
        public string PatchName() { return patch_name; }
        public string PatchTexture() { return patch_texture; }
        public string ApronName() { return apron_name; }
        public string ApronTexture() { return apron_texture; }
        public string WaterTexture() { return water_texture; }
        public string DetailTexture0() { return noise_tex0; }
        public string DetailTexture1() { return noise_tex1; }
        public string HazeName() { return haze_name; }
        public string CloudsHigh() { return clouds_high; }
        public string ShadesHigh() { return shades_high; }
        public string CloudsLow() { return clouds_low; }
        public string ShadesLow() { return shades_low; }
        public string EnvironmentTexture(int face)
        {
            switch (face)
            {
                case 0: return env_texture_positive_x;

                case 1:
                    if (!string.IsNullOrWhiteSpace(env_texture_negative_x))
                        return env_texture_negative_x;
                    return env_texture_positive_x;

                case 2: return env_texture_positive_y;

                case 3:
                    if (!string.IsNullOrWhiteSpace(env_texture_negative_y))
                        return env_texture_negative_y;
                    return env_texture_positive_y;

                case 4:
                    if (!string.IsNullOrWhiteSpace(env_texture_positive_z))
                        return env_texture_positive_z;
                    return env_texture_positive_x;

                case 5:
                    if (!string.IsNullOrWhiteSpace(env_texture_negative_z))
                        return env_texture_negative_z;
                    if (!string.IsNullOrWhiteSpace(env_texture_positive_z))
                        return env_texture_positive_z;
                    return env_texture_positive_x;
            }

            return env_texture_positive_x;
        }

        public Color SunColor() { return sun_color[24]; }
        public Color SkyColor() { return sky_color[24]; }
        public Color FogColor() { return fog_color[24]; }
        public Color Ambient() { return ambient[24]; }
        public Color Overcast() { return overcast[24]; }
        public Color CloudColor() { return cloud_color[24]; }
        public Color ShadeColor() { return shade_color[24]; }

        public double LateralScale() { return scale; }
        public double MountainScale() { return mtnscale; }
        public double FogDensity() { return fog_density; }
        public double FogScale() { return fog_scale; }
        public double DayPhase() { return day_phase; }
        public double HazeFade() { return haze_fade; }
        public double CloudAltHigh() { return clouds_alt_high; }
        public double CloudAltLow() { return clouds_alt_low; }
        public Weather GetWeather() { return weather; }
        public List<TerrainLayer> GetLayers() { return layers; }

        public bool IsEclipsed() { return eclipsed; }
        public void SetEclipsed(bool e) { eclipsed = e; }

        public void LoadSkyColors(string bmp_name)
        {
            ErrLogger.PrintLine("WARNING: LoadSkyColors is not yet implemented, bmp_name = '{0}'", bmp_name);
#if TODO
            Bitmap sky_colors_bmp;

            DataLoader* loader = DataLoader::GetLoader();
            loader.LoadBitmap(bmp_name, sky_colors_bmp);

            int max_color = sky_colors_bmp.Width();

            if (max_color > 24)
                max_color = 24;

            for (int i = 0; i < 25; i++)
            {
                sun_color[i] = Color.white;
                sky_color[i] = Color.black;
                fog_color[i] = Color.white;
                ambient[i] = ColorExtensions.DarkGray;
                cloud_color[i] = Color.white;
                shade_color[i] = Color.gray;
            }

            for (int i = 0; i < max_color; i++)
                sky_color[i] = sky_colors_bmp.GetColor(i, 0);

            if (sky_colors_bmp.Height() > 1)
                for (int i = 0; i < max_color; i++)
                    fog_color[i].Set(sky_colors_bmp.GetColor(i, 1).Value() | Color(0, 0, 0, 255).Value());

            if (sky_colors_bmp.Height() > 2)
                for (int i = 0; i < max_color; i++)
                    ambient[i] = sky_colors_bmp.GetColor(i, 2);

            if (sky_colors_bmp.Height() > 3)
                for (int i = 0; i < max_color; i++)
                    sun_color[i] = sky_colors_bmp.GetColor(i, 3);

            if (sky_colors_bmp.Height() > 4)
                for (int i = 0; i < max_color; i++)
                    overcast[i] = sky_colors_bmp.GetColor(i, 4);

            if (sky_colors_bmp.Height() > 5)
                for (int i = 0; i < max_color; i++)
                    cloud_color[i] = sky_colors_bmp.GetColor(i, 5);

            if (sky_colors_bmp.Height() > 6)
                for (int i = 0; i < max_color; i++)
                    shade_color[i] = sky_colors_bmp.GetColor(i, 6);
#endif
        }
        public void AddLayer(double h, string tile, string detail = null)
        {
            TerrainLayer layer = new TerrainLayer();

            layer.min_height = h;
            layer.tile_name = tile;

            if (!string.IsNullOrEmpty(detail))
                layer.detail_name = detail;

            layers.Add(layer);
        }


        internal protected string patch_name;
        internal protected string patch_texture;
        internal protected string apron_name;
        internal protected string apron_texture;
        internal protected string water_texture;
        internal protected string env_texture_positive_x;
        internal protected string env_texture_negative_x;
        internal protected string env_texture_positive_y;
        internal protected string env_texture_negative_y;
        internal protected string env_texture_positive_z;
        internal protected string env_texture_negative_z;
        internal protected string noise_tex0;
        internal protected string noise_tex1;
        internal protected string haze_name;
        internal protected string clouds_high;
        internal protected string clouds_low;
        internal protected string shades_high;
        internal protected string shades_low;

        protected Color[] sun_color = new Color[25];
        protected Color[] sky_color = new Color[25];
        protected Color[] fog_color = new Color[25];
        protected Color[] ambient = new Color[25];
        protected Color[] overcast = new Color[25];
        protected Color[] cloud_color = new Color[25];
        protected Color[] shade_color = new Color[25];

        internal protected double scale;
        internal protected double mtnscale;

        internal protected double fog_density;
        internal protected double fog_scale;
        internal protected double day_phase;
        internal protected double haze_fade;
        internal protected double clouds_alt_high;
        internal protected double clouds_alt_low;

        protected Weather weather = new Weather();
        protected bool eclipsed;

        protected List<TerrainLayer> layers = new List<TerrainLayer>();

        public const double TERRAIN_ALTITUDE_LIMIT = 35e3;

    }
}
