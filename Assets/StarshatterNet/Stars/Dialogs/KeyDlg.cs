﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      KeyDlg.h/KeyDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Navigation Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class KeyDlg : FormWindow
    {
        public KeyDlg(Screen s, Camera c, FormDef def, BaseScreen mgr, GameObject p) :
           base(s, 0, 0, s.Width(), s.Height(), c, p, "KeyDlg")
        {
            manager = mgr;
            key_key = 0; key_shift = 0; key_joy = 0; key_clear = false;
            command = null; current_key = null; new_key = null; clear = null;
            apply = null; cancel = null;

            Init(def);
        }
        //public virtual ~KeyDlg();
        public override void RegisterControls()
        {
            if (apply != null)
                return;

            command = FindControl(201);
            current_key = FindControl(202);
            new_key = FindControl(203);

            clear = (Button)FindControl(300);
            REGISTER_CLIENT(ETD.EID_CLICK, clear, OnClear);

            apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, apply, OnApply);

            cancel = (Button)FindControl(2);
            REGISTER_CLIENT(ETD.EID_CLICK, cancel, OnCancel);
        }
        public override void Show()
        {
            base.Show();

            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                KeyMap keymap = stars.GetKeyMap();

                if (command != null)
                    command.SetText(keymap.DescribeAction(key_index));

                if (current_key != null)
                    current_key.SetText(keymap.DescribeKey(key_index));
            }

            key_clear = false;
            if (new_key != null)
                new_key.SetText("");
            //TODO SetFocus();
        }
        // Operations:
        public virtual void ExecFrame()
        {
            int key = 0;
            int shift = 0;
            int joy = 0;
            Joystick joystick = Joystick.GetInstance();

            if (joystick != null) joystick.Acquire();

            for (int i = 0; i < 256; i++)
            {
                int vk = KeyMap.GetMappableVKey(i);

                if (vk >= KeyMap.KEY_JOY_1 && vk <= KeyMap.KEY_JOY_16)
                {
                    if (joystick != null && Joystick.KeyDown(vk))
                        joy = vk;
                }

                else if (vk >= KeyMap.KEY_POV_0_UP && vk <= KeyMap.KEY_POV_3_RIGHT)
                {
                    if (joystick != null && Joystick.KeyDown(vk))
                        joy = vk;
                }
#if TODO
                else if (GetAsyncKeyState(vk))
                {
                    if (vk == KeyMap.VK_SHIFT || vk == KeyMap.VK_MENU)
                        shift = vk;
                    else
                        key = vk;
                }
#endif
            }

            if (key != 0)
            {
                key_key = key;
                key_shift = shift;

                new_key.SetText(KeyMap.DescribeKey(key, shift, joy));
            }

            else if (joy != 0)
            {
                key_joy = joy;
                new_key.SetText(KeyMap.DescribeKey(key, shift, joy));
            }
        }

        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                KeyMap keymap = stars.GetKeyMap();
                KeyMapEntry map = keymap.GetKeyMap(key_index);

                if (key_clear)
                {
                    map.key = 0;
                    map.alt = 0;
                    map.joy = 0;
                }

                if (key_key != 0)
                {
                    map.key = key_key;
                    map.alt = key_shift;
                }

                if (key_joy != 0)
                {
                    map.joy = key_joy;
                }
            }

            if (manager != null)
                manager.ShowCtlDlg();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.ShowCtlDlg();
        }
        public virtual void OnClear(ActiveWindow obj, AWEvent evnt)
        {
            key_clear = true;

            key_key = 0;
            key_shift = 0;
            key_joy = 0;
        }

        public int GetKeyMapIndex() { return key_index; }
        public void SetKeyMapIndex(int i)
        {
            key_index = i;
            key_key = 0;
            key_shift = 0;
        }



        protected BaseScreen manager;

        protected int key_index;
        protected int key_key;
        protected int key_shift;
        protected int key_joy;
        protected bool key_clear;

        protected Button clear;
        protected Button apply;
        protected Button cancel;

        protected ActiveWindow command;
        protected ActiveWindow current_key;
        protected ActiveWindow new_key;

    }
}