﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      Screen.h/Screen.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    General Screen class - maintains and displays a list of windows
*/
using System.Collections.Generic;
using StarshatterNet.Controllers;
using UnityEngine;
using Rect = StarshatterNet.Stars.Rect;

namespace StarshatterNet.Gen.Graphics
{
    public class Screen
    {
        public Screen(Video v)
        {
            width = 0; height = 0; video = v; clear = 0; closed = false;
            if (video != null)
            {
                width = video.Width();
                height = video.Height();
            }
            // TODO Mouse.Create(this);
        }


        //virtual ~Screen();

        public virtual bool SetBackgroundColor(Color c)
        {
            if (video != null)
                return video.SetBackgroundColor(c);
            else
                return false;
        }

        public virtual bool Resize(int w, int h)
        {
            // scale all root-level windows to new screen size:

            foreach (Window win in window_list)
            {
                Rect tmprect = win.GetRect();

                double w_x = tmprect.x / (double)width;
                double w_y = tmprect.y / (double)height;
                double w_w = tmprect.w / (double)width;
                double w_h = tmprect.h / (double)height;

                Rect r = new Rect();

                r.x = (int)(w_x * w);
                r.y = (int)(w_y * h);
                r.w = (int)(w_w * w);
                r.h = (int)(w_h * h);

                win.MoveTo(r);
            }

            width = w;
            height = h;

            return true;
        }
        public virtual bool Refresh()
        {
            if (clear != 0 && !video.ClearAll())
                return false;

            video.StartFrame();

            foreach (Window win in window_list)
            {

                if (win.IsShown())
                {
                    win.Paint();
                }
            }

            Mouse.Paint();

            video.EndFrame();

            if (clear > 0) clear--;
            return true;
        }
        public virtual bool AddWindow(Window c)
        {
            if (c == null || closed) return false;

            if (c.X() < 0) return false;
            if (c.Y() < 0) return false;
            if (c.X() + c.Width() > Width()) return false;
            if (c.Y() + c.Height() > Height()) return false;

            if (!window_list.Contains(c))
                window_list.Add(c);

            return true;
        }

        public virtual bool DelWindow(Window c)
        {
            if (c == null || closed) return false;

            return window_list.Remove(c);
        }

        public int Width() { return width; }
        public int Height() { return height; }

        public virtual void ClearAllFrames(bool clear_all)
        {
            if (clear_all)
                clear = -1;
            else
                clear = 0;
        }


        public virtual void ClearNextFrames(int num_frames)
        {
            if (clear >= 0 && clear < num_frames)
                clear = num_frames;
        }

        public virtual Video GetVideo() { return video; }

        protected int width;
        protected int height;
        protected int clear;
        protected bool closed;

        protected Video video;

        protected List<Window> window_list = new List<Window>();
    }
}