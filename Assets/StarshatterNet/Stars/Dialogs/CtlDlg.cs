﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CtlDlg.h/CtlDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Control Options (keyboard/joystick) Dialog Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using DWORD = System.UInt32;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CtlDlg : FormWindow
    {
        public CtlDlg(Screen s, Camera c, FormDef def, BaseScreen mgr, GameObject p) :
           base(s, 0, 0, s.Width(), s.Height(), c, p, "CtlDlg")
        {
            manager = mgr;
            selected_category = 0; command_index = 0; control_model = 0;
            joy_select = 0; joy_throttle = 0; joy_rudder = 0; joy_sensitivity = 0;
            mouse_select = 0; mouse_sensitivity = 0; mouse_invert = 0;
            apply = null; cancel = null; vid_btn = null; aud_btn = null; ctl_btn = null; opt_btn = null; mod_btn = null;
            closed = true;
            Init(def);
        }
        // public virtual ~CtlDlg();
        public override void RegisterControls()
        {
            if (apply != null)
                return;

            for (int i = 0; i < 4; i++)
            {
                category[i] = (Button)FindControl(101 + i);
                REGISTER_CLIENT(ETD.EID_CLICK, category[i], OnCategory);
            }

            commands = (ListBox)FindControl(200);
            REGISTER_CLIENT(ETD.EID_SELECT, commands, OnCommand);

            control_model_combo = (ComboBox)FindControl(210);
            REGISTER_CLIENT(ETD.EID_SELECT, control_model_combo, OnControlModel);

            joy_select_combo = (ComboBox)FindControl(211);
            REGISTER_CLIENT(ETD.EID_SELECT, joy_select_combo, OnJoySelect);

            joy_throttle_combo = (ComboBox)FindControl(212);
            REGISTER_CLIENT(ETD.EID_SELECT, joy_throttle_combo, OnJoyThrottle);

            joy_rudder_combo = (ComboBox)FindControl(213);
            REGISTER_CLIENT(ETD.EID_SELECT, joy_rudder_combo, OnJoyRudder);

            joy_sensitivity_slider = (Slider)FindControl(214);

            if (joy_sensitivity_slider != null)
            {
                joy_sensitivity_slider.SetRangeMin(0);
                joy_sensitivity_slider.SetRangeMax(10);
                joy_sensitivity_slider.SetStepSize(1);
                REGISTER_CLIENT(ETD.EID_CLICK, joy_sensitivity_slider, OnJoySensitivity);
            }

            joy_axis_button = (Button)FindControl(215);
            REGISTER_CLIENT(ETD.EID_CLICK, joy_axis_button, OnJoyAxis);

            mouse_select_combo = (ComboBox)FindControl(511);
            REGISTER_CLIENT(ETD.EID_SELECT, mouse_select_combo, OnMouseSelect);

            mouse_sensitivity_slider = (Slider)FindControl(514);
            if (mouse_sensitivity_slider != null)
            {
                mouse_sensitivity_slider.SetRangeMin(0);
                mouse_sensitivity_slider.SetRangeMax(50);
                mouse_sensitivity_slider.SetStepSize(1);
                REGISTER_CLIENT(ETD.EID_CLICK, mouse_sensitivity_slider, OnMouseSensitivity);
            }

            mouse_invert_checkbox = (Button)FindControl(515);
            REGISTER_CLIENT(ETD.EID_CLICK, mouse_invert_checkbox, OnMouseInvert);

            apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, apply, OnApply);

            cancel = (Button)FindControl(2);
            REGISTER_CLIENT(ETD.EID_CLICK, cancel, OnCancel);

            vid_btn = (Button)FindControl(901);
            REGISTER_CLIENT(ETD.EID_CLICK, vid_btn, OnVideo);

            aud_btn = (Button)FindControl(902);
            REGISTER_CLIENT(ETD.EID_CLICK, aud_btn, OnAudio);

            ctl_btn = (Button)FindControl(903);
            REGISTER_CLIENT(ETD.EID_CLICK, ctl_btn, OnControls);

            opt_btn = (Button)FindControl(904);
            REGISTER_CLIENT(ETD.EID_CLICK, opt_btn, OnOptions);

            mod_btn = (Button)FindControl(905);
            if (mod_btn != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, mod_btn, OnMod);
            }
        }
        public override void Show()
        {
            if (!IsShown())
                base.Show();

            if (closed)
                ShowCategory();
            else
                UpdateCategory();

            if (vid_btn != null) vid_btn.SetButtonState(0);
            if (aud_btn != null) aud_btn.SetButtonState(0);
            if (ctl_btn != null) ctl_btn.SetButtonState(1);
            if (opt_btn != null) opt_btn.SetButtonState(0);
            if (mod_btn != null) mod_btn.SetButtonState(0);

            closed = false;
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnApply(null, null);
            }
        }

        // Operations:
        public virtual void OnCommand(ActiveWindow obj, AWEvent evnt)
        {
            int list_index = commands.GetListIndex();

            // double-click:
            if (list_index == command_index && Game.RealTime() - command_click_time < 350)
            {
                KeyDlg key_dlg = null;

                if (manager != null)
                    key_dlg = manager.GetKeyDlg();

                if (key_dlg != null)
                {
                    key_dlg.SetKeyMapIndex((int)commands.GetItemData(list_index));
                }

                if (manager != null)
                    manager.ShowKeyDlg();
            }

            command_click_time = (DWORD)Game.RealTime();
            command_index = list_index;
        }
        public virtual void OnCategory(ActiveWindow obj, AWEvent evnt)
        {
            selected_category = 0;

            for (int i = 0; i < 4; i++)
                if (evnt.window == category[i])
                    selected_category = i;

            ShowCategory();
        }

        public virtual void OnControlModel(ActiveWindow obj, AWEvent evnt)
        {
            control_model = control_model_combo.GetSelectedIndex();
        }

        public virtual void OnJoySelect(ActiveWindow obj, AWEvent evnt)
        {
            joy_select = joy_select_combo.GetSelectedIndex();
        }
        public virtual void OnJoyThrottle(ActiveWindow obj, AWEvent evnt)
        {
            joy_throttle = joy_throttle_combo.GetSelectedIndex();
        }
        public virtual void OnJoyRudder(ActiveWindow obj, AWEvent evnt)
        {
            joy_rudder = joy_rudder_combo.GetSelectedIndex();
        }
        public virtual void OnJoySensitivity(ActiveWindow obj, AWEvent evnt)
        {
            joy_sensitivity = joy_sensitivity_slider.GetValue();
        }
        public virtual void OnJoyAxis(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.ShowJoyDlg();
        }

        public virtual void OnMouseSelect(ActiveWindow obj, AWEvent evnt)
        {
            mouse_select = mouse_select_combo.GetSelectedIndex();
        }
        public virtual void OnMouseSensitivity(ActiveWindow obj, AWEvent evnt)
        {
            mouse_sensitivity = mouse_sensitivity_slider.GetValue();
        }
        public virtual void OnMouseInvert(ActiveWindow obj, AWEvent evnt)
        {
            mouse_invert = (short)(mouse_invert_checkbox.GetButtonState() > 0 ? 1 : 0);
        }

        public virtual void Apply()
        {
            if (closed) return;

            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                KeyMap keymap = stars.GetKeyMap();

                keymap.Bind(KeyMap.KEY_CONTROL_MODEL, control_model, 0);

                keymap.Bind(KeyMap.KEY_JOY_SELECT, joy_select, 0);
                keymap.Bind(KeyMap.KEY_JOY_RUDDER, joy_rudder, 0);
                keymap.Bind(KeyMap.KEY_JOY_THROTTLE, joy_throttle, 0);
                keymap.Bind(KeyMap.KEY_JOY_SENSE, joy_sensitivity, 0);

                keymap.Bind(KeyMap.KEY_MOUSE_SELECT, mouse_select, 0);
                keymap.Bind(KeyMap.KEY_MOUSE_SENSE, mouse_sensitivity, 0);
                keymap.Bind(KeyMap.KEY_MOUSE_INVERT, mouse_invert, 0);

                keymap.SaveKeyMap("key.cfg", 256);

                stars.MapKeys();
                Ship.SetControlModel(control_model);
            }

            closed = true;
        }
        public virtual void Cancel()
        {
            closed = true;
        }

        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.ApplyOptions();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.CancelOptions();
        }

        public virtual void OnAudio(ActiveWindow obj, AWEvent evnt) { manager.ShowAudDlg(); }
        public virtual void OnVideo(ActiveWindow obj, AWEvent evnt) { manager.ShowVidDlg(); }
        public virtual void OnOptions(ActiveWindow obj, AWEvent evnt) { manager.ShowOptDlg(); }
        public virtual void OnControls(ActiveWindow obj, AWEvent evnt) { manager.ShowCtlDlg(); }
        public virtual void OnMod(ActiveWindow obj, AWEvent evnt) { manager.ShowModDlg(); }



        protected void ShowCategory()
        {
            if (commands == null || category[0] == null)
                return;

            for (int i = 0; i < 4; i++)
            {
                if (i == selected_category)
                    category[i].SetButtonState(1);
                else
                    category[i].SetButtonState(0);
            }

            commands.ClearItems();
            commands.SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_FILLED_BOX);
            commands.SetLeading(2);

            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                KeyMap keymap = stars.GetKeyMap();
                int n = 0;

                for (int i = 0; i < 256; i++)
                {
                    KeyMapEntry k = keymap.GetKeyMap(i);

                    switch (k.act)
                    {
                        case 0:
                            break;

                        case KeyMap.KEY_CONTROL_MODEL:
                            control_model = k.key;
                            control_model_combo.SetSelection(control_model);
                            break;

                        case KeyMap.KEY_JOY_SELECT:
                            joy_select = k.key;
                            joy_select_combo.SetSelection(joy_select);
                            break;

                        case KeyMap.KEY_JOY_RUDDER:
                            joy_rudder = k.key;
                            joy_rudder_combo.SetSelection(joy_rudder);
                            break;

                        case KeyMap.KEY_JOY_THROTTLE:
                            joy_throttle = k.key;
                            joy_throttle_combo.SetSelection(joy_throttle);
                            break;

                        case KeyMap.KEY_JOY_SENSE:
                            joy_sensitivity = k.key;
                            joy_sensitivity_slider.SetValue(joy_sensitivity);
                            break;

                        case KeyMap.KEY_JOY_SWAP:
                            break;

                        case KeyMap.KEY_JOY_DEAD_ZONE:
                            break;

                        case KeyMap.KEY_MOUSE_SELECT:
                            mouse_select = k.key;
                            mouse_select_combo.SetSelection(mouse_select);
                            break;

                        case KeyMap.KEY_MOUSE_SENSE:
                            mouse_sensitivity = k.key;
                            mouse_sensitivity_slider.SetValue(mouse_sensitivity);
                            break;

                        case KeyMap.KEY_MOUSE_INVERT:
                            mouse_invert = k.key;
                            mouse_invert_checkbox.SetButtonState((short)(mouse_invert > 0 ? 1 : 0));
                            break;

                        case KeyMap.KEY_AXIS_YAW:
                        case KeyMap.KEY_AXIS_PITCH:
                        case KeyMap.KEY_AXIS_ROLL:
                        case KeyMap.KEY_AXIS_THROTTLE:

                        case KeyMap.KEY_AXIS_YAW_INVERT:
                        case KeyMap.KEY_AXIS_PITCH_INVERT:
                        case KeyMap.KEY_AXIS_ROLL_INVERT:
                        case KeyMap.KEY_AXIS_THROTTLE_INVERT:
                            break;

                        default:
                            if ((int)keymap.GetCategory(i) == selected_category)
                            {
                                commands.AddItemWithData(keymap.DescribeAction(i), i);
                                commands.SetItemText(n++, 1, keymap.DescribeKey(i));
                            }
                            break;
                    }
                }
            }
            commands.RebuildListObjets();
        }
        protected void UpdateCategory()
        {
            if (commands == null)
                return;

            Starshatter stars = Starshatter.GetInstance();
            KeyMap keymap = stars.GetKeyMap();

            for (int i = 0; i < commands.NumItems(); i++)
            {
                int key = (int)commands.GetItemData(i);
                commands.SetItemText(i, 1, keymap.DescribeKey(key));
            }
            commands.RebuildListObjets();
        }

        protected BaseScreen manager;

        protected Button[] category = new Button[4];
        protected ListBox commands;
        protected int command_index;

        protected ComboBox control_model_combo;

        protected ComboBox joy_select_combo;
        protected ComboBox joy_throttle_combo;
        protected ComboBox joy_rudder_combo;
        protected Slider joy_sensitivity_slider;
        protected Button joy_axis_button;

        protected ComboBox mouse_select_combo;
        protected Slider mouse_sensitivity_slider;
        protected Button mouse_invert_checkbox;

        protected Button aud_btn;
        protected Button vid_btn;
        protected Button opt_btn;
        protected Button ctl_btn;
        protected Button mod_btn;

        protected Button apply;
        protected Button cancel;

        protected int selected_category;
        protected int control_model;

        protected int joy_select;
        protected int joy_throttle;
        protected int joy_rudder;
        protected int joy_sensitivity;

        protected int mouse_select;
        protected int mouse_sensitivity;
        protected int mouse_invert;

        protected bool closed;
        static DWORD command_click_time = 0;

    }
}