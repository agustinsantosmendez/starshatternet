﻿using StarshatterNet.Gen.Graphics;
using UnityEngine;

namespace StarshatterNet.Stars.Graphics.General
{
    public class SkyNebula : Graphic
    {
        public SkyNebula(string filename) : base("SkyNebula", false)
        {
            name = "SkyNebula";

            infinite = true;
            luminous = false;
            shadow = false;

            if (!string.IsNullOrEmpty(filename))
                material = Resources.Load("Galaxy/" + filename, typeof(UnityEngine.Material)) as UnityEngine.Material;
        }
        public override void Show()
        {
            RenderSettings.skybox = material;
        }
        public override void Hide()
        {
            RenderSettings.skybox = null;
        }

        protected UnityEngine.Material material;
    }
}