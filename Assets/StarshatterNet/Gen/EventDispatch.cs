﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      EventDispatch.h/EventDispatch.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Event Dispatch class
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Stars;

namespace StarshatterNet.Gen
{
    public class EventDispatch
    {
        public EventDispatch()
        {
            capture = null; current = null; focus = null; click_tgt = null;
            mouse_x = 0; mouse_y = 0; mouse_l = 0; mouse_r = 0;
        }
        //virtual ~EventDispatch();

        public static void Create()
        {
            dispatcher = new EventDispatch();
        }

        public static void Close()
        {
            //delete dispatcher;
            dispatcher = null;
        }

        public static EventDispatch GetInstance() { return dispatcher; }

        public virtual void Dispatch()
        {
            int ml = Mouse.LButton();
            int mr = Mouse.RButton();
            int mx = (int)Mouse.X();
            int my = (int)Mouse.Y();
            int mw = (int)Mouse.Wheel();

            IEventTarget mouse_tgt = capture;
            IEventTarget key_tgt = focus;
            IEventTarget do_click = null;

            if (mouse_tgt == null)
            {
                foreach (EventTarget test in clients)
                {
                    if (test.IsFormActive())
                    {
                        if (test.TargetRect().Contains(mx, my))
                            mouse_tgt = test;

                        if (test.HasFocus())
                            key_tgt = test;
                    }
                }
            }

            // Mouse Events:

            if (mouse_tgt != current)
            {
                if (current != null && current.IsEnabled() && current.IsVisible())
                    current.OnMouseExit(mx, my);

                current = mouse_tgt;

                if (current != null && current.IsEnabled() && current.IsVisible())
                    current.OnMouseEnter(mx, my);
            }

            if (mouse_tgt != null && mouse_tgt.IsEnabled())
            {
                if (mx != mouse_x || my != mouse_y)
                    mouse_tgt.OnMouseMove(mx, my);

                if (mw != 0)
                    mouse_tgt.OnMouseWheel(mw);

                if (ml != mouse_l)
                {
                    if (ml != 0)
                    {
                        mouse_tgt.OnLButtonDown(mx, my);
                        click_tgt = mouse_tgt;
                    }
                    else
                    {
                        mouse_tgt.OnLButtonUp(mx, my);

                        if (click_tgt == mouse_tgt)
                        {
                            if (click_tgt.TargetRect().Contains(mx, my))
                                do_click = click_tgt;
                            click_tgt = null;
                        }
                    }
                }

                if (mr != mouse_r)
                {
                    if (mr != 0)
                        mouse_tgt.OnRButtonDown(mx, my);
                    else
                        mouse_tgt.OnRButtonUp(mx, my);
                }
            }

            mouse_l = ml;
            mouse_r = mr;
            mouse_x = mx;
            mouse_y = my;

            // Keyboard Events:

            if (click_tgt != null && click_tgt != key_tgt)
            {
                if (key_tgt != null) key_tgt.KillFocus();
                key_tgt = click_tgt;

                if (key_tgt != focus)
                {
                    if (focus != null) focus.KillFocus();

                    if (key_tgt != null && key_tgt.IsEnabled() && key_tgt.IsVisible())
                        focus = key_tgt;
                    else
                        key_tgt = null;

                    if (focus != null) focus.SetFocus();
                }
            }

            if (key_tgt != null && key_tgt.IsEnabled())
            {
                int key = 0;
                int shift = 0;

                while (Game.GetKeyPlus(ref key, ref shift) != 0)
                {
                    if (key == KeyMap.VK_ESCAPE)
                    {
                        key_tgt.KillFocus();
                        focus = null;
                        break;
                    }

                    else if (key == KeyMap.VK_TAB && focus != null)
                    {
                        int key_index = clients.IndexOf(focus) + 1;

                        if ((shift & 1) != 0) key_index -= 2;

                        if (key_index >= clients.Count)
                            key_index = 0;
                        else if (key_index < 0)
                            key_index = clients.Count - 1;

                        if (focus != null) focus.KillFocus();
                        focus = clients[key_index];
                        if (focus != null) focus.SetFocus();

                        break;
                    }

                    key_tgt.OnKeyDown(key, shift);
                }
            }

            if (do_click != null)
                do_click.OnClick();
        }
        public virtual void Register(IEventTarget tgt)
        {
            if (!clients.Contains(tgt))
                clients.Add(tgt);
        }
        public virtual void Unregister(IEventTarget tgt)
        {
            clients.Remove(tgt);

            if (capture == tgt) capture = null;
            if (current == tgt) current = null;
            if (focus == tgt) focus = null;
            if (click_tgt == tgt) click_tgt = null;
        }

        public virtual IEventTarget GetCapture()
        {
            return capture;
        }
        public virtual int CaptureMouse(IEventTarget tgt)
        {
            if (tgt != null)
            {
                capture = tgt;
                return 1;
            }

            return 0;
        }
        public virtual int ReleaseMouse(IEventTarget tgt)
        {
            if (capture == tgt)
            {
                capture = null;
                return 1;
            }

            return 0;
        }

        public virtual IEventTarget GetFocus()
        {
            return focus;
        }
        public virtual void SetFocus(EventTarget tgt)
        {
            if (focus != tgt)
            {
                if (focus != null)
                    focus.KillFocus();

                focus = tgt;

                if (focus != null && focus.IsEnabled() && focus.IsVisible())
                    focus.SetFocus();
            }
        }
        public virtual void KillFocus(EventTarget tgt)
        {
            if (focus != null && focus == tgt)
            {
                focus = null;
                tgt.KillFocus();
            }
        }

        public virtual void MouseEnter(EventTarget mouse_tgt)
        {
            if (mouse_tgt != current)
            {
                if (current != null) current.OnMouseExit(0, 0);
                current = mouse_tgt;
                if (current != null) current.OnMouseEnter(0, 0);
            }
        }


        protected int mouse_x, mouse_y, mouse_l, mouse_r;
        protected List<IEventTarget> clients;
        protected IEventTarget capture;
        protected IEventTarget current;
        protected IEventTarget focus;
        protected IEventTarget click_tgt;

        protected static EventDispatch dispatcher = null;
    }
}
