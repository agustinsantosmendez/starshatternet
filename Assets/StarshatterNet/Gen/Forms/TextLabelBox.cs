﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:       
    ORIGINAL AUTHOR:     
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    TextLabelBox base class for List, Edit, and Rich Text controls
*/
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Views;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.Int32;


namespace StarshatterNet.Gen.Forms
{
    public class TextLabelBox : ActiveWindow
    {
        public TextLabelBox(ActiveWindow p, int ax, int ay, int aw, int ah, DWORD aid) :
            base(p.GetScreen(), ax, ay, aw, ah, aid, 0, p)
        {
            text_align = TextFormat.DT_CENTER;

            desc = "TextLabelBox " + id;
        }
        public override void SetText(string t)
        {
            base.SetText(t);
            SetText();
        }
        public override void SetFont(FontItem f)
        {
            base.SetFont(f);
            SetFont();
        }
        public override void SetFontSize(int s)
        {
            base.SetFontSize(s);
            SetFont();
        }

        protected void SetText()
        {
            if (textCmp == null) return;
            textCmp.text = this.GetText();
        }
        protected void SetFont()
        {
            if (textCmp == null) return;
            textCmp.font = this.GetFont().font;
            textCmp.fontSize = this.GetFontSize();
        }


        public override void SetTextAlign(TextFormat a)
        {
            base.SetTextAlign(a);
            SetTextAlign();
        }
        protected void SetTextAlign()
        {
            if (textCmp == null) return;

            if (text_align.HasFlag(TextFormat.DT_VCENTER) || text_align.HasFlag(TextFormat.DT_SINGLELINE))
            {
                if (text_align.HasFlag(TextFormat.DT_LEFT))
                    textCmp.alignment = TextAnchor.MiddleLeft;
                else if (text_align.HasFlag(TextFormat.DT_RIGHT))
                    textCmp.alignment = TextAnchor.MiddleRight;
                else if (text_align.HasFlag(TextFormat.DT_CENTER))
                    textCmp.alignment = TextAnchor.MiddleCenter;
            }
            else if (text_align.HasFlag(TextFormat.DT_TOP))
            {
                if (text_align.HasFlag(TextFormat.DT_LEFT))
                    textCmp.alignment = TextAnchor.UpperLeft;
                else if (text_align.HasFlag(TextFormat.DT_RIGHT))
                    textCmp.alignment = TextAnchor.UpperRight;
                else if (text_align.HasFlag(TextFormat.DT_CENTER))
                    textCmp.alignment = TextAnchor.UpperCenter;
            }
            else if (text_align.HasFlag(TextFormat.DT_BOTTOM))
            {
                if (text_align.HasFlag(TextFormat.DT_LEFT))
                    textCmp.alignment = TextAnchor.LowerLeft;
                else if (text_align.HasFlag(TextFormat.DT_RIGHT))
                    textCmp.alignment = TextAnchor.LowerRight;
                else if (text_align.HasFlag(TextFormat.DT_CENTER))
                    textCmp.alignment = TextAnchor.LowerCenter;
            }
            else
            {
                if (text_align.HasFlag(TextFormat.DT_LEFT))
                    textCmp.alignment = TextAnchor.MiddleLeft;
                else if (text_align.HasFlag(TextFormat.DT_RIGHT))
                    textCmp.alignment = TextAnchor.MiddleRight;
                else if (text_align.HasFlag(TextFormat.DT_CENTER))
                    textCmp.alignment = TextAnchor.MiddleCenter;
            }
        }
        public override void SetForeColor(Color c)
        {
            base.SetForeColor(c);
            SetForeColor();
        }
        protected void SetForeColor()
        {
            if (textCmp != null)
                textCmp.color = fore_color;
        }
        protected internal override void BuildGamebjects()
        {
            base.BuildGamebjects();

            obj = new GameObject(); //Create the GameObject
            textCmp = obj.AddComponent<Text>(); //Add the Text Component script
            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            rectTransform.SetParent(this.parentObj.transform); //Assign the newly created Image GameObject as a Child of the Parent Panel.
            obj.name = desc;
            obj.SetActive(true); //Activate the GameObject

            SetText();
            SetFont();
            SetTextAlign();
            SetForeColor();

            // update children windows:
            foreach (var child in children)
                child.BuildGamebjects();
        }
        protected Text textCmp;
    }

}
