﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Component.h/Component.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Generic ship system sub-component class
*/
using System;

namespace StarshatterNet.Stars.SimElements
{
    [Serializable]
    public class ComponentDesign
    {
        // identification:
        public string name;
        public string abrv;

        public float repair_time = 0.0f;
        public float replace_time = 0.0f;
        public int spares = 0;
        //public Component.DAMAGE affects = Component.DAMAGE.NONE;
        public int affects = (int)Component.DAMAGE.NONE;
    }

    public class Component
    {
#warning Status is also defined at ShipSystem
        public enum STATUS { DESTROYED, CRITICAL, DEGRADED, NOMINAL, REPLACE, REPAIR }

        [Flags]
        public enum DAMAGE
        {
            NONE = 0,
            DAMAGE_EFFICIENCY = 0x01,
            DAMAGE_SAFETY = 0x02,
            DAMAGE_STABILITY = 0x04
        }

        public Component(ComponentDesign d, ShipSystem s)
        {
            this.design = d;
            this.system = s;
            if (design != null)
                spares = design.spares;

        }
        public Component(Component c)
        {
            this.design = c.design;
            this.system = c.system;
            this.status = c.status;
            this.availability = c.availability;
            this.time_remaining = c.time_remaining;
            this.spares = c.spares;
            this.jerried = c.jerried;
        }

        public string Name() { return design.name; }
        public string Abbreviation() { return design.abrv; }
        public float RepairTime() { return design.repair_time; }
        public float ReplaceTime() { return design.replace_time; }

        public bool DamageEfficiency() { return ((DAMAGE)design.affects & DAMAGE.DAMAGE_EFFICIENCY) == DAMAGE.DAMAGE_EFFICIENCY; }
        public bool DamageSafety() { return ((DAMAGE)design.affects & DAMAGE.DAMAGE_SAFETY) == DAMAGE.DAMAGE_SAFETY; }
        public bool DamageStability() { return ((DAMAGE)design.affects & DAMAGE.DAMAGE_STABILITY) == DAMAGE.DAMAGE_STABILITY; }

        public STATUS Status() { return status; }

        public float Availability()
        {
            if (status > STATUS.NOMINAL && availability > 50)
                return 50.0f;

            return availability;
        }

        public float TimeRemaining()
        {
            return (float)time_remaining;
        }

        public int SpareCount()
        {
            return spares;
        }

        public bool IsJerried()
        {
            return jerried != 0 ? true : false;
        }

        public int NumJerried()
        {
            return jerried;
        }

        public void SetSystem(ShipSystem s) { system = s; }
        public ShipSystem GetSystem() { return system; }

        public virtual void ApplyDamage(double damage)
        {
            availability -= (float)damage;
            if (availability < 1) availability = 0.0f;

            if (status < STATUS.REPLACE)
            {
                if (availability > 99)
                    status = STATUS.NOMINAL;
                else if (availability > 49)
                    status = STATUS.DEGRADED;
                else
                    status = STATUS.CRITICAL;
            }

            if (system != null)
                system.CalcStatus();
        }

        public virtual void ExecMaintFrame(double seconds)
        {
            if (status > STATUS.NOMINAL)
            {
                time_remaining -= (float)seconds;

                // when repairs are complete:
                if (time_remaining <= 0)
                {
                    if (status == STATUS.REPAIR)
                    {
                        // did we just jerry-rig a failed component?
                        if (availability < 50)
                            jerried++;

                        if (jerried < 5)
                            availability += 50.0f - 10 * jerried;
                        if (availability > 100) availability = 100.0f;
                    }
                    else
                    {
                        availability = 100.0f;
                    }

                    if (availability > 99)
                        status = STATUS.NOMINAL;
                    else if (availability > 49)
                        status = STATUS.DEGRADED;
                    else
                        status = STATUS.CRITICAL;

                    time_remaining = 0.0f;

                    if (system != null)
                        system.CalcStatus();
                }
            }
        }

        public virtual void Repair()
        {
            if (status < STATUS.NOMINAL)
            {
                status = STATUS.REPAIR;
                time_remaining = design.repair_time;

                if (system != null)
                    system.CalcStatus();
            }
        }

        public virtual void Replace()
        {
            if (status <= STATUS.NOMINAL)
            {
                status = STATUS.REPLACE;
                spares--;
                time_remaining = design.replace_time;

                if (system != null)
                    system.CalcStatus();
            }
        }


        protected ComponentDesign design;

        // Component health status:
        protected STATUS status = STATUS.NOMINAL;
        protected float availability = 100.0f;
        protected float time_remaining = 0.0f;
        protected int spares = 0;
        protected int jerried = 0;
        protected ShipSystem system;
    }
}
