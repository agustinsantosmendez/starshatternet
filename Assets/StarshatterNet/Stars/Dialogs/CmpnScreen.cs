﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmpnScreen.h/CmpnScreen.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

*/
using System.Collections.Generic;
using StarshatterNet.Audio;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmpnScreen
    {
        public CmpnScreen()
        {
            camera = null;
            cmd_force_dlg = null; cmd_missions_dlg = null; cmd_orders_dlg = null;
            cmd_intel_dlg = null; cmd_theater_dlg = null; cmd_msg_dlg = null; cmp_file_dlg = null;
            cmp_end_dlg = null; cmp_scene_dlg = null;
            isShown = false; campaign = null; stars = null; completion_stage = 0;

            //loader = DataLoader.GetLoader();
            stars = Starshatter.GetInstance();
        }
        //public virtual ~CmpnScreen();

        public virtual void Setup(Screen screen, Camera s, GameObject p)
        {
            if (!s)
                return;

            camera = s;
            gameObj = p;

            //loader.UseFileSystem(true);

            FormDef cmd_orders_def = new FormDef("CmdOrdersDlg", 0);
            cmd_orders_def.Load("CmdOrdersDlg");
            cmd_orders_dlg = new CmdOrdersDlg(screen, camera, cmd_orders_def, this, gameObj);

            FormDef cmd_force_def = new FormDef("CmdForceDlg", 0);
            cmd_force_def.Load("CmdForceDlg");
            cmd_force_dlg = new CmdForceDlg(screen, camera, cmd_force_def, this, gameObj);

            FormDef cmd_theater_def = new FormDef("CmdTheaterDlg", 0);
            cmd_theater_def.Load("CmdTheaterDlg");
            cmd_theater_dlg = new CmdTheaterDlg(screen, camera, cmd_theater_def, this, gameObj);

            FormDef cmd_intel_def = new FormDef("CmdIntelDlg", 0);
            cmd_intel_def.Load("CmdIntelDlg");
            cmd_intel_dlg = new CmdIntelDlg(screen, camera, cmd_intel_def, this, gameObj);

            FormDef cmd_missions_def = new FormDef("CmdMissionsDlg", 0);
            cmd_missions_def.Load("CmdMissionsDlg");
            cmd_missions_dlg = new CmdMissionsDlg(screen, camera, cmd_missions_def, this, gameObj);

            FormDef file_def = new FormDef("FileDlg", 0);
            file_def.Load("FileDlg");
            cmp_file_dlg = new CmpFileDlg(screen, camera, file_def, this, gameObj);

            FormDef msg_def = new FormDef("CmdMsgDlg", 0);
            msg_def.Load("CmdMsgDlg");
            cmd_msg_dlg = new CmdMsgDlg(screen, camera, msg_def, this, gameObj);

            FormDef end_def = new FormDef("CmpCompleteDlg", 0);
            end_def.Load("CmpCompleteDlg");
            cmp_end_dlg = new CmpCompleteDlg(screen, camera, end_def, this, gameObj);

            FormDef scene_def = new FormDef("CmpSceneDlg", 0);
            scene_def.Load("CmpSceneDlg");
            cmp_scene_dlg = new CmpSceneDlg(screen, camera, scene_def, this, gameObj);

            // loader.UseFileSystem(Starshatter.UseFileSystem());

            HideAll();
        }
        public virtual void TearDown()
        {
            //if (screen)
            //{
            //    screen.DelWindow(cmd_force_dlg);
            //    screen.DelWindow(cmd_missions_dlg);
            //    screen.DelWindow(cmd_orders_dlg);
            //    screen.DelWindow(cmd_intel_dlg);
            //    screen.DelWindow(cmd_theater_dlg);
            //    screen.DelWindow(cmd_msg_dlg);
            //    screen.DelWindow(cmp_file_dlg);
            //    screen.DelWindow(cmp_end_dlg);
            //    screen.DelWindow(cmp_scene_dlg);
            //}

            //delete cmd_force_dlg;
            //delete cmd_missions_dlg;
            //delete cmd_orders_dlg;
            //delete cmd_intel_dlg;
            //delete cmd_theater_dlg;
            //delete cmd_msg_dlg;
            //delete cmp_file_dlg;
            //delete cmp_end_dlg;
            //delete cmp_scene_dlg;

            cmd_force_dlg = null;
            cmd_missions_dlg = null;
            cmd_orders_dlg = null;
            cmd_intel_dlg = null;
            cmd_theater_dlg = null;
            cmd_msg_dlg = null;
            cmp_file_dlg = null;
            cmp_end_dlg = null;
            cmp_scene_dlg = null;
            camera = null;
        }
        public virtual bool CloseTopmost()
        {
            bool processed = false;

            if (IsCmdMsgShown())
            {
                HideCmdMsgDlg();
                processed = true;
            }

            else if (IsCmpFileShown())
            {
                HideCmpFileDlg();
                processed = true;
            }

            return processed;
        }


        public virtual bool IsShown() { return isShown; }
        public virtual void Show()
        {
            if (!isShown)
            {
                isShown = true;

                campaign = Campaign.GetCampaign();
                completion_stage = 0;

                bool cutscene = false;
                CombatEvent evnt = null;

                if (campaign.IsActive() && !campaign.GetEvents().IsEmpty())
                {
                    foreach (CombatEvent iter in campaign.GetEvents())
                    {
                        evnt = iter;

                        if (evnt != null && !evnt.Visited() && !string.IsNullOrEmpty(evnt.SceneFile()))
                        {
                            stars.ExecCutscene(evnt.SceneFile(), campaign.Path());

                            if (stars.InCutscene())
                            {
                                cutscene = true;
                                ShowCmpSceneDlg();
                            }

                            evnt.SetVisited(true);
                            break;
                        }
                    }
                }

                if (!cutscene)
                    ShowCmdDlg();
            }
        }
        public virtual void Hide()
        {
            if (isShown)
            {
                HideAll();
                isShown = false;
            }
        }



        public virtual void ShowCmdDlg()
        {
            ShowCmdOrdersDlg();
        }


        public virtual void ShowCmdForceDlg()
        {
            HideAll();
            cmd_force_dlg.Show();
            Mouse.Show(true);
        }

        public virtual void HideCmdForceDlg()
        {
            if (IsCmdForceShown())
                cmd_force_dlg.Hide();
        }

        public virtual bool IsCmdForceShown()
        {
            return cmd_force_dlg != null && cmd_force_dlg.IsShown();
        }

        public virtual CmdForceDlg GetCmdForceDlg() { return cmd_force_dlg; }

        public virtual void ShowCmdMissionsDlg()
        {
            HideAll();
            cmd_missions_dlg.Show();
            Mouse.Show(true);
        }

        public virtual void HideCmdMissionsDlg()
        {
            if (IsCmdMissionsShown())
                cmd_missions_dlg.Hide();
        }

        public virtual bool IsCmdMissionsShown()
        {
            return cmd_missions_dlg != null && cmd_missions_dlg.IsShown();
        }

        public virtual CmdMissionsDlg GetCmdMissionsDlg() { return cmd_missions_dlg; }

        public virtual void ShowCmdOrdersDlg()
        {
            HideAll();
            cmd_orders_dlg.Show();
            Mouse.Show(true);
        }


        public virtual void HideCmdOrdersDlg()
        {
            if (IsCmdOrdersShown())
                cmd_orders_dlg.Hide();
        }

        public virtual bool IsCmdOrdersShown()
        {
            return cmd_orders_dlg != null && cmd_orders_dlg.IsShown();
        }

        public virtual CmdOrdersDlg GetCmdOrdersDlg() { return cmd_orders_dlg; }

        public virtual void ShowCmdIntelDlg()
        {
            HideAll();
            cmd_intel_dlg.Show();
            Mouse.Show(true);
        }

        public virtual void HideCmdIntelDlg()
        {
            if (IsCmdIntelShown())
                cmd_intel_dlg.Hide();
        }
        public virtual bool IsCmdIntelShown()
        {
            return cmd_intel_dlg != null && cmd_intel_dlg.IsShown();
        }
        public virtual CmdIntelDlg GetCmdIntelDlg() { return cmd_intel_dlg; }

        public virtual void ShowCmdTheaterDlg()
        {
            HideAll();
            cmd_theater_dlg.Show();
            Mouse.Show(true);
        }
        public virtual void HideCmdTheaterDlg()
        {
            if (IsCmdTheaterShown())
                cmd_theater_dlg.Hide();
        }
        public virtual bool IsCmdTheaterShown()
        {
            return cmd_theater_dlg != null && cmd_theater_dlg.IsShown();
        }
        public virtual CmdTheaterDlg GetCmdTheaterDlg() { return cmd_theater_dlg; }

        public virtual void ShowCmpFileDlg()
        {
            cmp_file_dlg.Show();
            Mouse.Show(true);
        }
        public virtual void HideCmpFileDlg()
        {
            if (IsCmpFileShown())
                cmp_file_dlg.Hide();
        }
        public virtual bool IsCmpFileShown()
        {
            return cmp_file_dlg != null && cmp_file_dlg.IsShown();
        }
        public virtual CmpFileDlg GetCmpFileDlg() { return cmp_file_dlg; }

        public virtual void ShowCmdMsgDlg()
        {
            cmd_msg_dlg.Show();
            Mouse.Show(true);
        }
        public virtual void HideCmdMsgDlg()
        {
            if (IsCmdMsgShown())
                cmd_msg_dlg.Hide();
        }
        public virtual bool IsCmdMsgShown()
        {
            return cmd_msg_dlg != null && cmd_msg_dlg.IsShown();
        }
        public virtual CmdMsgDlg GetCmdMsgDlg() { return cmd_msg_dlg; }

        public virtual void ShowCmpCompleteDlg()
        {
            cmp_end_dlg.Show();
            Mouse.Show(true);
        }
        public virtual void HideCmpCompleteDlg()
        {
            if (IsCmpCompleteShown())
                cmp_end_dlg.Hide();
        }
        public virtual bool IsCmpCompleteShown()
        {
            return cmp_end_dlg != null && cmp_end_dlg.IsShown();
        }
        public virtual CmpCompleteDlg GetCmpCompleteDlg() { return cmp_end_dlg; }

        public virtual void ShowCmpSceneDlg()
        {
            cmp_scene_dlg.Show();
            Mouse.Show(false);
        }
        public virtual void HideCmpSceneDlg()
        {
            if (IsCmpSceneShown())
                cmp_scene_dlg.Hide();
        }
        public virtual bool IsCmpSceneShown()
        {
            return cmp_scene_dlg != null && cmp_scene_dlg.IsShown();
        }
        public virtual CmpSceneDlg GetCmpSceneDlg() { return cmp_scene_dlg; }

        public virtual void HideAll()
        {
            if (cmd_force_dlg != null) cmd_force_dlg.Hide();
            if (cmd_missions_dlg != null) cmd_missions_dlg.Hide();
            if (cmd_orders_dlg != null) cmd_orders_dlg.Hide();
            if (cmd_intel_dlg != null) cmd_intel_dlg.Hide();
            if (cmd_theater_dlg != null) cmd_theater_dlg.Hide();
            if (cmd_msg_dlg != null) cmd_msg_dlg.Hide();
            if (cmp_file_dlg != null) cmp_file_dlg.Hide();
            if (cmp_end_dlg != null) cmp_end_dlg.Hide();
            if (cmp_scene_dlg != null) cmp_scene_dlg.Hide();
        }

        public virtual void ExecFrame()
        {
            Game.SetScreenColor(Color.black);

            if (cmd_orders_dlg != null && cmd_orders_dlg.IsShown())
            {
                cmd_orders_dlg.ExecFrame();
            }

            else if (cmd_force_dlg != null && cmd_force_dlg.IsShown())
            {
                cmd_force_dlg.ExecFrame();
            }

            else if (cmd_theater_dlg != null && cmd_theater_dlg.IsShown())
            {
                cmd_theater_dlg.ExecFrame();
            }

            else if (cmd_missions_dlg != null && cmd_missions_dlg.IsShown())
            {
                cmd_missions_dlg.ExecFrame();
            }

            else if (cmd_intel_dlg != null && cmd_intel_dlg.IsShown())
            {
                cmd_intel_dlg.ExecFrame();
            }

            if (cmp_file_dlg != null && cmp_file_dlg.IsShown())
            {
                cmp_file_dlg.ExecFrame();
            }

            if (cmd_msg_dlg != null && cmd_msg_dlg.IsShown())
            {
                cmd_msg_dlg.ExecFrame();
            }

            else if (cmp_end_dlg != null && cmp_end_dlg.IsShown())
            {
                cmp_end_dlg.ExecFrame();
                completion_stage = 2;
            }

            else if (cmp_scene_dlg != null && cmp_scene_dlg.IsShown())
            {
                cmp_scene_dlg.ExecFrame();

                if (completion_stage > 0)
                    completion_stage = 2;
            }

            else if (campaign != null)
            {
                // if campaign is complete
                if (completion_stage == 0)
                {
                    Player player = Player.GetCurrentPlayer();
                    string msg_info;

                    if (player == null)
                        return;

                    if (campaign.IsTraining())
                    {
                        int all_missions = (1 << campaign.GetMissionList().Count) - 1;

                        if (player.Trained() >= all_missions && player.Trained() < 255)
                        {
                            player.SetTrained(255);
                            cmd_msg_dlg.Title().SetText(Game.GetText("CmpnScreen.training"));
                            msg_info = string.Format(Game.GetText("CmpnScreen.congrats"),
                                                    Player.RankName(player.Rank()),
                                                    player.Name());

                            cmd_msg_dlg.Message().SetText(msg_info);
                            cmd_msg_dlg.Message().SetTextAlign(TextFormat.DT_LEFT);

                            ShowCmdMsgDlg();
                            completion_stage = 1;
                        }
                    }

                    else if (campaign.IsComplete() || campaign.IsFailed())
                    {
                        bool cutscene = false;
                        CombatEvent evnt = campaign.GetLastEvent();

                        if (evnt != null && !evnt.Visited() && !string.IsNullOrEmpty(evnt.SceneFile()))
                        {
                            stars.ExecCutscene(evnt.SceneFile(), campaign.Path());

                            if (stars.InCutscene())
                            {
                                cutscene = true;
                                ShowCmpSceneDlg();
                            }
                        }

                        if (!cutscene)
                        {
                            ShowCmpCompleteDlg();
                        }

                        if (campaign.IsComplete())
                            MusicDirector.SetMode(MusicDirector.MODES.VICTORY);
                        else
                            MusicDirector.SetMode(MusicDirector.MODES.DEFEAT);

                        completion_stage = 1;
                    }
                }

                // if message has been shown, restart
                else if (completion_stage > 1)
                {
                    completion_stage = 0;

                    if (campaign.IsTraining())
                    {
                        List<Campaign> list = Campaign.GetAllCampaigns();
                        Campaign c = list[1];

                        if (c != null)
                        {
                            c.Load();
                            Campaign.SelectCampaign(c.Name());
                            stars.SetGameMode(Starshatter.MODE.CLOD_MODE);
                            return;
                        }
                    }

                    // continue on to the next available campaign:
                    if (campaign.GetCampaignId() < Campaign.GetLastCampaignId())
                    {
                        stars.StartOrResumeGame();
                    }

                    // if this was the last campaign, just go back to the menu:
                    else
                    {
                        Mouse.Show(false);
                        MusicDirector.SetMode(MusicDirector.MODES.MENU);
                        stars.SetGameMode(Starshatter.MODE.MENU_MODE);
                        return;
                    }
                }
            }

            if (completion_stage < 1)
            {
                MusicDirector.SetMode(MusicDirector.MODES.MENU);
                Mouse.Show(!IsCmpSceneShown());
            }
        }

        public void SetFieldOfView(double fov)
        {
#if TODO
            if (cmp_scene_dlg!= null)
                cmp_scene_dlg.GetCameraView().SetFieldOfView(fov);
#endif
        }

        public double GetFieldOfView()
        {
#if TODO

            if (cmp_scene_dlg != null)
                return cmp_scene_dlg.GetCameraView().GetFieldOfView();
#endif

            return 2;
        }



        private Camera camera;
        private GameObject gameObj;

        private CmdForceDlg cmd_force_dlg;
        private CmdOrdersDlg cmd_orders_dlg;
        private CmdMissionsDlg cmd_missions_dlg;
        private CmdIntelDlg cmd_intel_dlg;
        private CmdTheaterDlg cmd_theater_dlg;

        private CmdMsgDlg cmd_msg_dlg;
        private CmpFileDlg cmp_file_dlg;
        private CmpCompleteDlg cmp_end_dlg;
        private CmpSceneDlg cmp_scene_dlg;

        //TODO private DataLoader loader;

        private bool isShown;

        private Campaign campaign;
        private Starshatter stars;
        private int completion_stage;
    }
}