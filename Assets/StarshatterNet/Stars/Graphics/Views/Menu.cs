﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx.lib
    ORIGINAL FILE:      Menu.h/Menu.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simple menu hierarchy class
*/
using System;
using System.Collections.Generic;
using DWORD = System.UInt32;

namespace StarshatterNet.Stars.Views
{
    [Serializable]
    public class Menu
    {
        public Menu() { }
        public Menu(string t)
        {
            title = t;
        }

        //public virtual ~Menu() { items.destroy(); }

        public virtual string GetTitle() { return title; }
        public virtual void SetTitle(string t) { title = t; }
        public virtual Menu GetParent() { return parent; }
        public virtual void SetParent(Menu p) { parent = p; }

        public virtual void AddItem(string label, object value = null, bool enabled = true)
        {
            MenuItem item = new MenuItem(label, value, enabled);

            if (item != null)
            {
                item.menu = this;
                items.Add(item);
            }
        }

        public virtual void AddItem(MenuItem item)
        {
            if (item.submenu != null)
                item.submenu.SetParent(this);
            item.menu = this;
            items.Add(item);
        }
        public virtual void AddMenu(string label, Menu menu, object value = null)
        {
            MenuItem item = new MenuItem(label, value);

            if (item != null)
            {
                item.menu = this;
                item.submenu = menu;
                menu.parent = this;

                items.Add(item);
            }
        }
        public virtual MenuItem GetItem(int index)
        {
            if (index >= 0 && index < items.Count)
                return items[index];

            return null;
        }
        public virtual void SetItem(int index, MenuItem item)
        {
            if (item != null && index >= 0 && index < items.Count)
                items[index] = item;
        }

        public virtual int NumItems()
        {
            return items.Count;
        }
        public virtual void ClearItems()
        {
            items.Clear();
        }


        public List<MenuItem> GetItems() { return items; }

        public int GetLevel()
        {
            int level = 0;
            if (parent != null)
                level = parent.GetLevel() + 1;
            return level;
        }

        protected string title;
        protected List<MenuItem> items = new List<MenuItem>();
        protected Menu parent;

    }

    // +-------------------------------------------------------------------+

    [Serializable]
    public class MenuItem
    {
        public MenuItem(string label, object value = null, bool e = true)
        {
            text = label;
            data = value;
            enabled = e;
            submenu = null;
            selected = 0;
        }

        //public virtual ~MenuItem();

        public virtual string GetText() { return text; }
        public virtual void SetText(string t) { text = t; }

        public virtual object GetData() { return data; }
        public virtual void SetData(object d) { data = d; }

        public virtual bool GetEnabled() { return enabled; }
        public virtual void SetEnabled(bool e) { enabled = e; }

        public virtual int GetSelected() { return selected; }
        public virtual void SetSelected(int s) { selected = s; }

        public virtual Menu GetMenu() { return menu; }
        public virtual void SetMenu(Menu m) { menu = m; }

        public virtual Menu GetSubmenu() { return submenu; }
        public virtual void SetSubmenu(Menu s) { submenu = s; }

        public virtual int GetLevel()
        {
            if (menu != null)
                return menu.GetLevel();
            else
                return -1;
        }

        protected string text;
        protected object data;
        protected bool enabled;
        protected int selected;

        internal protected Menu menu;
        internal protected Menu submenu;
    }

    // +-------------------------------------------------------------------+

    public class MenuHistory
    {
        public MenuHistory() { }
        // public virtual ~MenuHistory() { history.clear(); }

        public virtual Menu GetCurrent()
        {
            int n = history.Count;

            if (n != 0)
                return history[n - 1];

            return null;
        }
        public virtual Menu GetLevel(int n)
        {
            if (n >= 0 && n < history.Count)
                return history[n];

            return null;
        }

        public virtual Menu Find(string title)
        {
            for (int i = 0; i < history.Count; i++)
                if (history[i].GetTitle() == title)
                    return history[i];

            return null;
        }
        public virtual void Pop()
        {
            int n = history.Count;

            if (n != 0)
                history.RemoveAt(n - 1);
        }
        public virtual void Push(Menu menu)
        {
            history.Add(menu);
        }

        public virtual void Clear()
        {
            history.Clear();
        }


        private List<Menu> history = new List<Menu>();
    }
}
