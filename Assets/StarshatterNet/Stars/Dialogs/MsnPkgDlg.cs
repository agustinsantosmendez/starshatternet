﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MsnPkgDlg.h/MsnPkgDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Briefing Dialog Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MsnPkgDlg : FormWindow
    {
        public MsnPkgDlg(Screen s, Camera c, FormDef def, PlanScreen mgr, GameObject p) :
                    base(s, 0, 0, s.Width(), s.Height(), c, p, "MsnPkgDlg")
        {
            msnDlg = new MsnDlg(mgr);

            msnDlg.campaign = Campaign.GetCampaign();

            if (msnDlg.campaign != null)
                msnDlg.mission = msnDlg.campaign.GetMission();

            Init(def);
            Show();
        }
        //public virtual ~MsnPkgDlg();
        public override void RegisterControls()
        {
            pkg_list = (ListBox)FindControl(320);
            nav_list = (ListBox)FindControl(330);

            for (int i = 0; i < 5; i++)
                threat[i] = FindControl(251 + i);

            msnDlg.RegisterMsnControls(this);

            if (pkg_list != null)
                REGISTER_CLIENT(ETD.EID_SELECT, pkg_list, OnPackage);

            if (msnDlg.commit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.commit, OnCommit);

            if (msnDlg.cancel != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.cancel, OnCancel);

            if (msnDlg.sit_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.sit_button, OnTabButton);

            if (msnDlg.pkg_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.pkg_button, OnTabButton);

            if (msnDlg.nav_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.nav_button, OnTabButton);

            if (msnDlg.wep_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.wep_button, OnTabButton);
        }
        public override void Show()
        {
            if (msnDlg == null) return;

            base.Show();
            msnDlg.ShowMsnDlg();

            DrawPackages();
            DrawNavPlan();
            DrawThreats();
        }

        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnCommit(null, null);
            }
        }



        // Operations:
        public virtual void OnCommit(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnCommit(obj, evnt);
        }

        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnCancel(obj, evnt);
        }
        public virtual void OnTabButton(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnTabButton(obj, evnt);
        }
        public virtual void OnPackage(ActiveWindow obj, AWEvent evnt)
        {
            if (pkg_list == null || msnDlg.mission == null)
                return;

            int seln = pkg_list.GetListIndex();
            int pkg = (int)pkg_list.GetItemData(seln);

            int i = 0;
            foreach (MissionElement elem in msnDlg.mission.GetElements())
            {
                if (elem.ElementID() == pkg)
                {
                    msnDlg.pkg_index = i;
                    //mission.SetPlayer(elem.value());
                }

                i++;
            }

            //DrawPackages();
            DrawNavPlan();
        }


        protected virtual void DrawPackages()
        {
            if (msnDlg.mission != null)
            {
                if (pkg_list != null)
                {
                    pkg_list.ClearItems();

                    int i = 0;
                    int elem_index = 0;
                    foreach (MissionElement elem in msnDlg.mission.GetElements())
                    {
                        // display this element?
                        if (elem.GetIFF() == msnDlg.mission.Team() &&
                                !elem.IsSquadron() &&
                                elem.Region() == msnDlg.mission.GetRegion() &&
                                elem.GetDesign().type < CLASSIFICATION.STATION)
                        {

                            string txt;

                            if (elem.Player() > 0)
                            {
                                txt = "==>";
                                if (msnDlg.pkg_index < 0)
                                    msnDlg.pkg_index = elem_index;
                            }
                            else
                            {
                                txt = " ";
                            }

                            pkg_list.AddItemWithData(txt, elem.ElementID());
                            pkg_list.SetItemText(i, 1, elem.Name());
                            pkg_list.SetItemText(i, 2, elem.RoleName());

                            ShipDesign design = elem.GetDesign();

                            if (elem.Count() > 1)
                                txt = string.Format("{0} {1}", elem.Count(), design.abrv);
                            else
                                txt = string.Format("{0} {1}", design.abrv, design.name);
                            pkg_list.SetItemText(i, 3, txt);

                            i++;
                        }

                        elem_index++;
                    }
                }
            }
        }
        protected virtual void DrawNavPlan()
        {
            if (msnDlg.mission != null)
            {
                if (msnDlg.pkg_index < 0 || msnDlg.pkg_index >= msnDlg.mission.GetElements().Count)
                    msnDlg.pkg_index = 0;

                MissionElement element = msnDlg.mission.GetElements()[msnDlg.pkg_index];
                if (nav_list != null && element != null)
                {
                    nav_list.ClearItems();

                    Point loc = element.Location();
                    int i = 0;

                    foreach (Instruction navpt in element.NavList())
                    {
                        string txt;
                        txt = string.Format("{0}", i + 1);

                        nav_list.AddItem(txt);
                        nav_list.SetItemText(i, 1, Instruction.ActionName(navpt.Action()));
                        nav_list.SetItemText(i, 2, navpt.RegionName());

                        double dist = new Point(loc - navpt.Location()).Length;
                        FormatUtil.FormatNumber(out txt, dist);
                        nav_list.SetItemText(i, 3, txt);

                        txt = string.Format("{0}", navpt.Speed());
                        nav_list.SetItemText(i, 4, txt);

                        loc = navpt.Location();
                        i++;
                    }
                }
            }
        }

        protected virtual void DrawThreats()
        {
            for (int i = 0; i < 5; i++)
                if (threat[i] != null)
                    threat[i].SetText("");

            if (msnDlg.mission == null) return;

            MissionElement player = msnDlg.mission.GetPlayer();

            if (player == null) return;

            string rgn0 = player.Region();
            string rgn1 = "";
            int iff = player.GetIFF();

            foreach (Instruction nav in player.NavList())
            {
                if (rgn0 != nav.RegionName())
                    rgn1 = nav.RegionName();
            }

            if (threat[0] != null)
            {
                Point base_loc = msnDlg.mission.GetElements()[0].Location();

                int i = 0;
                foreach (MissionElement elem in msnDlg.mission.GetElements())
                {

                    if (elem.GetIFF() == 0 || elem.GetIFF() == iff || elem.IntelLevel() <= Intel.INTEL_TYPE.SECRET)
                        continue;

                    if (elem.IsSquadron())
                        continue;

                    if (elem.IsGroundUnit())
                    {
                        if (elem.GetDesign() == null ||
                                elem.GetDesign().type != CLASSIFICATION.SAM)
                            continue;

                        if (elem.Region() != rgn0 &&
                                elem.Region() != rgn1)
                            continue;
                    }

                    Mission.TYPE mission_role = elem.MissionRole();

                    if (mission_role == Mission.TYPE.STRIKE ||
                            mission_role == Mission.TYPE.INTEL ||
                            mission_role >= Mission.TYPE.TRANSPORT)
                        continue;

                    string rng = "";
                    string role;
                    string txt;

                    if (mission_role == Mission.TYPE.SWEEP ||
                            mission_role == Mission.TYPE.INTERCEPT ||
                            mission_role == Mission.TYPE.FLEET ||
                            mission_role == Mission.TYPE.BOMBARDMENT)
                        role = Game.GetText("MsnDlg.ATTACK");
                    else
                        role = Game.GetText("MsnDlg.PATROL");

                    double dist = new Point(base_loc - elem.Location()).Length;
                    FormatUtil.FormatNumber(out rng, dist);

                    txt = string.Format("{0} - {1} {2} - {3}", role,
                                       elem.Count(),
                                       elem.GetDesign().abrv,
                                       rng);
                    if (threat[i] != null)
                        threat[i].SetText(txt);

                    i++;

                    if (i >= 5)
                        break;
                }
            }
        }

        protected ListBox pkg_list;
        protected ListBox nav_list;

        protected ActiveWindow[] threat = new ActiveWindow[5];

        protected MsnDlg msnDlg;
    }
}