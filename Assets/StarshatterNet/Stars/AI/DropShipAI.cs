﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      DropShipAI.h/DropShipAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Drop Ship (orbit/surface and surface/orbit) AI class
*/
using System;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.AI
{
    public class DropShipAI : ShipAI
    {

        public DropShipAI(Ship s): base(s)
        {
            seek_gain = 20;
            seek_damp = 0.5;

            //delete tactical;
            tactical = null;
        }
        // public virtual ~DropShipAI();
        public const int DIR_TYPE = 2001;

        public override SteerType Type() { return (SteerType)DIR_TYPE; }


        // accumulate behaviors:
        protected override void Navigator()
        {
            accumulator.Clear();
            magnitude = 0;

            if (other != null)
                ship.SetFLCSMode(FLCS_MODE.FLCS_AUTO);
            else
                ship.SetFLCSMode(FLCS_MODE.FLCS_MANUAL);

            Accumulate(AvoidCollision());
            Accumulate(Seek(objective));

            // are we being asked to flee?
            if (Math.Abs(accumulator.yaw) == 1.0 && accumulator.pitch == 0.0)
            {
                accumulator.pitch = -0.7f;
                accumulator.yaw *= 0.25f;
            }

            self.ApplyRoll((float)(accumulator.yaw * -0.4));
            self.ApplyYaw((float)(accumulator.yaw * 0.2));

            if (Math.Abs(accumulator.yaw) > 0.5 && Math.Abs(accumulator.pitch) < 0.1)
                accumulator.pitch -= 0.1f;

            if (accumulator.pitch != 0)
                self.ApplyPitch((float)accumulator.pitch);

            // if not turning, roll to orient with world coords:
            if (Math.Abs(accumulator.yaw) < 0.1)
            {
                Point vrt = ((CameraNode) (self.Cam())).vrt();
                double deflection = vrt.Y;
                if (deflection != 0)
                {
                    double theta = Math.Asin(deflection / vrt.Length);
                    self.ApplyRoll(-theta);
                }
            }

            ship.SetThrottle(100);
            ship.ExecFLCSFrame();
        }
        public override void FindObjective()
        {
            distance = 0;

            if ( ship == null) return;

            Sim  sim = Sim.GetSim();
            SimRegion  self_rgn = ship.GetRegion();

            // if making orbit, go up:
            if (self_rgn.Type() == SimRegion.TYPES.AIR_SPACE)
            {
                obj_w = self.Location() + new Point(0, 1e3, 0);
            }

            // if breaking orbit, head for terrain region:
            else
            {
                SimRegion  dst_rgn = sim.FindNearestTerrainRegion(ship);
                Point dst = dst_rgn.GetOrbitalRegion().Location() -
                self_rgn.GetOrbitalRegion().Location() +
                new Point(0, 0, -1e6);

                obj_w = dst.OtherHand();
            }

            // distance from self to navpt:
            distance = new Point(obj_w - self.Location()).Length;

            // transform into camera coords:
            objective = Transform(obj_w);
            objective.Normalize();
        }

    }
}
