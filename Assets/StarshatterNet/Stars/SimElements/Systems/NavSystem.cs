﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NavSystem.h/NavSystem.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Nav Points and so on...
*/
using StarshatterNet.Stars;
using System;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using DWORD = System.UInt32;
using StarshatterNet.Audio;

namespace StarshatterNet.Stars.SimElements
{
    public class NavSystem : ShipSystem
    {
        public NavSystem() : base(CATEGORY.COMPUTER, 2, "Auto Nav System", 1, 1, 1, 1)
        {
            autonav = false;
            name = Game.GetText("sys.nav-system");
            abrv = Game.GetText("sys.nav-system.abrv");

            power_flags = POWER_FLAGS.POWER_WATTS | POWER_FLAGS.POWER_CRITICAL;
        }

        public NavSystem(NavSystem s) : base(s) 
        {
            autonav = false;
            Mount(s);

            power_flags = POWER_FLAGS.POWER_WATTS | POWER_FLAGS.POWER_CRITICAL;
        }

 
        public override void ExecFrame(double seconds)
        {
            if (autonav  && ship != null &&  ship.GetNextNavPoint() == null)
                autonav = false;

            energy = 0.0f;
            base.ExecFrame(seconds);
        }

        public override void Distribute(double delivered_energy, double seconds)
        {
            if (IsPowerOn())
            {
                // convert Joules to Watts:
                energy = (float)(delivered_energy / seconds);

                // brown out:
                if (energy < capacity * 0.75f)
                    power_on = false;

                // spike:
                else if (energy > capacity * 1.5f)
                {
                    power_on = false;
                    ApplyDamage(50);
                }
            }
        }

        public bool AutoNavEngaged()
        {
            return ship != null && autonav && IsPowerOn();
        }

        public void EngageAutoNav()
        {
#if TODO
            if (IsPowerOn() && !autonav)
            {
                if ( ship.GetNextNavPoint() == null)
                {
                    Button.PlaySound(Button.SND_REJECT);
                }
                else
                {
                    HUDSounds.PlaySound(HUDSounds.SND_NAV_MODE);
                    autonav = true;
                }
            }
#endif
            throw new NotImplementedException();
        }
        public void DisengageAutoNav()
        {
#if TODO
            if (autonav)
                HUDSounds.PlaySound(HUDSounds.SND_NAV_MODE);

            autonav = false;
#endif
            throw new NotImplementedException();
        }


        protected bool autonav;

    }
}