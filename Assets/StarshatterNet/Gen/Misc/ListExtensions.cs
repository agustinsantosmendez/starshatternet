﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace StarshatterNet.Gen.Misc
{
    public static class ListExtensions
    {
        private static Random rng = new Random();
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void Destroy<T>(this IList<T> list)
        {
            foreach (var elem in list)
                if (elem is IDisposable)
                    ((IDisposable)elem).Dispose();
            list.Clear();
        }
        public static bool IsEmpty<T>(this IList<T> list)
        {
            return list == null || list.Count == 0;
        }

		/// <summary>
		/// Insert a value into an IList{T} that is presumed to be already sorted such that sort
		/// ordering is preserved
		/// </summary>
		/// <param name="list">List to insert into</param>
		/// <param name="value">Value to insert</param>
		/// <typeparam name="T">Type of element to insert and type of elements in the list</typeparam>
		public static void InsertIntoSortedList<T>(this IList<T> list, T value)
			where T : IComparable<T>
		{
			InsertIntoSortedList(list, value, (a, b) => a.CompareTo(b));
		}

		/// <summary>
		/// Insert a value into an IList{T} that is presumed to be already sorted such that sort
		/// ordering is preserved
		/// </summary>
		/// <param name="list">List to insert into</param>
		/// <param name="value">Value to insert</param>
		/// <param name="comparison">Comparison to determine sort order with</param>
		/// <typeparam name="T">Type of element to insert and type of elements in the list</typeparam>
		public static void InsertIntoSortedList<T>(
			this IList<T> list,
			T value,
			Comparison<T> comparison
		)
		{
			var startIndex = 0;
			var endIndex = list.Count;
			while (endIndex > startIndex)
			{
				var windowSize = endIndex - startIndex;
				var middleIndex = startIndex + (windowSize / 2);
				var middleValue = list[middleIndex];
				var compareToResult = comparison(middleValue, value);
				if (compareToResult == 0)
				{
					list.Insert(middleIndex, value);
					return;
				}
				else if (compareToResult < 0)
				{
					startIndex = middleIndex + 1;
				}
				else
				{
					endIndex = middleIndex;
				}
			}
			list.Insert(startIndex, value);
		}

		/// <summary>
		/// Insert a value into an IList that is presumed to be already sorted such that sort ordering is preserved
		/// </summary>
		/// <param name="list">List to insert into</param>
		/// <param name="value">Value to insert</param>
		public static void InsertIntoSortedList(this IList list, IComparable value)
		{
			InsertIntoSortedList(list, value, (a, b) => a.CompareTo(b));
		}

		/// <summary>
		/// Insert a value into an IList that is presumed to be already sorted such that sort ordering is preserved
		/// </summary>
		/// <param name="list">List to insert into</param>
		/// <param name="value">Value to insert</param>
		/// <param name="comparison">Comparison to determine sort order with</param>
		public static void InsertIntoSortedList(
			this IList list,
			IComparable value,
			Comparison<IComparable> comparison
		)
		{
			var startIndex = 0;
			var endIndex = list.Count;
			while (endIndex > startIndex)
			{
				var windowSize = endIndex - startIndex;
				var middleIndex = startIndex + (windowSize / 2);
				var middleValue = (IComparable)list[middleIndex];
				var compareToResult = comparison(middleValue, value);
				if (compareToResult == 0)
				{
					list.Insert(middleIndex, value);
					return;
				}
				else if (compareToResult < 0)
				{
					startIndex = middleIndex + 1;
				}
				else
				{
					endIndex = middleIndex;
				}
			}
			list.Insert(startIndex, value);
		}

		public static T RemoveIndex<T>(this IList<T> list, int index)
		{
			lock (list)
			{
				T value = list[index];
				list.RemoveAt(index);
				return value;
			}
		}
	}
}
