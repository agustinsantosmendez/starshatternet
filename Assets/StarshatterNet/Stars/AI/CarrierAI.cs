﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CarrierAI.h/CarrierAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    "Air Boss" AI class for managing carrier fighter squadrons
*/
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.SimElements;
using System.Collections.Generic;
using StarshatterNet.Stars.MissionCampaign;
 
namespace StarshatterNet.Stars.AI
{
    public class CarrierAI : Director
    {

        public CarrierAI(Ship s, int level)
        {
            sim = null; ship = s; hangar = null; exec_time = 0; flight_planner = null;
            hold_time = 0; ai_level = level;

            if (ship != null)
            {
                sim = Sim.GetSim();
                hangar = ship.GetHangar();

                for (int i = 0; i < 4; i++)
                    patrol_elem[i] = null;

                if (ship != null)
                    flight_planner = new FlightPlanner(ship);

                hold_time = (int)Game.GameTime();
            }
        }


        //~CarrierAI();

        private const int INIT_HOLD = 15000;
        private const int EXEC_PERIOD = 3000;
        private const uint PATROL_PERIOD = 900 * 1000;
        public override void ExecFrame(double seconds)
        {

            if (sim == null || ship == null || hangar == null)
                return;

            if (((int)Game.GameTime() - hold_time >= INIT_HOLD) &&
                    ((int)Game.GameTime() - exec_time > EXEC_PERIOD))
            {

                CheckHostileElements();
                CheckPatrolCoverage();

                exec_time = (int)Game.GameTime();
            }
        }

        protected virtual bool CheckPatrolCoverage()
        {

            // pick up existing patrol elements:
            foreach (Element elem in sim.GetElements())
            {

                if (elem.GetCarrier() == ship &&
                        (elem.Type() == Mission.TYPE.PATROL ||
                            elem.Type() == Mission.TYPE.SWEEP ||
                            elem.Type() == Mission.TYPE.AIR_PATROL ||
                            elem.Type() == Mission.TYPE.AIR_SWEEP) &&
                        !elem.IsSquadron() &&
                        !elem.IsFinished())
                {

                    bool found = false;
                    int open = -1;

                    for (int i = 0; i < 4; i++)
                    {
                        if (patrol_elem[i] == elem)
                            found = true;

                        else if (patrol_elem[i] == null && open < 0)
                            open = i;
                    }

                    if (!found && open >= 0)
                    {
                        patrol_elem[open] = elem;
                    }
                }
            }

            // manage the four screening patrols:
            for (int i = 0; i < 4; i++)
            {
                Element elem = patrol_elem[i];

                if (elem != null)
                {
                    if (elem.IsFinished())
                    {
                        patrol_elem[i] = null;
                    }

                    else
                    {
                        LaunchElement(elem);
                    }
                }

                else if (Game.GameTime() - hangar.GetLastPatrolLaunch() > PATROL_PERIOD ||
                        hangar.GetLastPatrolLaunch() == 0)
                {
                    Element patrol = CreatePackage(0, 2, Mission.TYPE.PATROL, null, "ACM Medium Range");
                    if (patrol != null)
                    {
                        patrol_elem[i] = patrol;

                        if (flight_planner != null)
                            flight_planner.CreatePatrolRoute(patrol, i);

                        hangar.SetLastPatrolLaunch((uint)Game.GameTime());
                        return true;
                    }
                }
            }

            return false;
        }

        protected virtual bool CheckHostileElements()
        {
            List<Element> assigned = new List<Element>();
            foreach (Element elem in sim.GetElements())
            {
                // if this element is hostile to us
                // or if the element is a target objective
                // of the carrier, or is hostile to any
                // of our squadrons...

                bool hostile = false;

                if (elem.IsHostileTo(ship) || elem.IsObjectiveTargetOf(ship))
                {
                    hostile = true;
                }
                else
                {
                    for (int i = 0; i < hangar.NumSquadrons() && !hostile; i++)
                    {
                        int squadron_iff = hangar.SquadronIFF(i);

                        if (elem.IsHostileTo(squadron_iff))
                            hostile = true;
                    }
                }

                if (hostile)
                {
                    sim.GetAssignedElements(elem, assigned);

                    // is one of our fighter elements already assigned to this target?
                    bool found = false;
                    foreach (Element a in assigned)
                    {
                        if (found) break;

                        if (a.GetCarrier() == ship)
                            found = true;
                    }

                    // nobody is assigned yet, create an attack package
                    if (!found && CreateStrike(elem))
                    {
                        hold_time = (int)Game.GameTime() + 30000;
                        return true;
                    }
                }
            }

            return false;
        }

        protected virtual bool CreateStrike(Element elem)
        {
            Element strike = null;
            Ship target = elem.GetShip(1);

            if (target != null && !target.IsGroundUnit())
            {
                Contact contact = ship.FindContact(target);
                if (contact != null && contact.GetIFF(ship) > 0)
                {

                    // fighter intercept
                    if (target.IsDropship())
                    {
                        int squadron = 0;
                        if (hangar.NumShipsReady(1) >= hangar.NumShipsReady(0))
                            squadron = 1;

                        int count = 2;

                        if (count < elem.NumShips())
                            count = elem.NumShips();

                        strike = CreatePackage(squadron, count, Mission.TYPE.INTERCEPT, elem.Name(), "ACM Medium Range");

                        if (strike != null)
                        {
                            strike.SetAssignment(elem);

                            if (flight_planner != null)
                                flight_planner.CreateStrikeRoute(strike, elem);
                        }
                    }

                    // starship or station assault
                    else
                    {
                        int squadron = 0;
                        if (hangar.NumSquadrons() > 1)
                            squadron = 1;
                        if (hangar.NumSquadrons() > 2)
                            squadron = 2;

                        int count = 2;

                        if (target.Class() > CLASSIFICATION.FRIGATE)
                        {
                            count = 4;
                            strike = CreatePackage(squadron, count, Mission.TYPE.ASSAULT, elem.Name(), "Hvy Ship Strike");
                        }
                        else
                        {
                            count = 2;
                            strike = CreatePackage(squadron, count, Mission.TYPE.ASSAULT, elem.Name(), "Ship Strike");
                        }

                        if (strike != null)
                        {
                            strike.SetAssignment(elem);

                            if (flight_planner != null)
                                flight_planner.CreateStrikeRoute(strike, elem);

                            // strike escort if target has fighter protection:
                            if (target.GetHangar() != null)
                            {
                                if (squadron > 1) squadron--;
                                Element escort = CreatePackage(squadron, 2, Mission.TYPE.ESCORT_STRIKE, strike.Name(), "ACM Short Range");

                                if (escort != null && flight_planner != null)
                                    flight_planner.CreateEscortRoute(escort, strike);
                            }
                        }
                    }
                }
            }

            return strike != null;
        }


        protected virtual Element CreatePackage(int squadron, int size, Mission.TYPE code, string target = null, string loadname = null)
        {
            if (squadron < 0 || size < 1 || code < Mission.TYPE.PATROL || hangar.NumShipsReady(squadron) < size)
                return null;

            Sim sim = Sim.GetSim();
            string call = sim.FindAvailCallsign(ship.GetIFF());
            Element elem = sim.CreateElement(call, ship.GetIFF(), code);
            FlightDeck deck = null;
            int queue = 1000;
            int[] load = null;
            ShipDesign design = hangar.SquadronDesign(squadron);

            elem.SetSquadron(hangar.SquadronName(squadron));
            elem.SetCarrier(ship);

            if (target != null)
            {
                Instruction.ACTION i_code = 0;

                switch (code)
                {
                    case Mission.TYPE.ASSAULT: i_code = Instruction.ACTION.ASSAULT; break;
                    case Mission.TYPE.STRIKE: i_code = Instruction.ACTION.STRIKE; break;

                    case Mission.TYPE.AIR_INTERCEPT:
                    case Mission.TYPE.INTERCEPT: i_code = Instruction.ACTION.INTERCEPT; break;

                    case Mission.TYPE.ESCORT:
                    case Mission.TYPE.ESCORT_STRIKE:
                    case Mission.TYPE.ESCORT_FREIGHT:
                        i_code = Instruction.ACTION.ESCORT; break;

                    case Mission.TYPE.DEFEND: i_code = Instruction.ACTION.DEFEND; break;
                }

                Instruction objective = new Instruction(i_code, target);
                if (objective != null)
                    elem.AddObjective(objective);
            }

            if (design != null && loadname != null)
            {
                string name = loadname;


                foreach (ShipLoad sl in design.loadouts)
                {
                    if (name.Equals(sl.name, System.StringComparison.InvariantCultureIgnoreCase))
                    {
                        load = sl.load;
                        elem.SetLoadout(load);
                    }
                }
            }

            for (int i = 0; i < ship.NumFlightDecks(); i++)
            {
                FlightDeck d = ship.GetFlightDeck(i);

                if (d != null && d.IsLaunchDeck())
                {
                    int dq = hangar.PreflightQueue(d);

                    if (dq < queue)
                    {
                        queue = dq;
                        deck = d;
                    }
                }
            }

            int npackage = 0;
            int[] slots = new int[4];

            for (int i = 0; i < 4; i++)
                slots[i] = -1;

            for (int slot = 0; slot < hangar.SquadronSize(squadron); slot++)
            {
                HangarSlot s = hangar.GetSlot(squadron, slot);

                if (hangar.GetState(s) == Hangar.HANGAR_STATE.STORAGE)
                {
                    if (npackage < 4)
                        slots[npackage] = slot;

                    hangar.GotoAlert(squadron, slot, deck, elem, load, code > Mission.TYPE.SWEEP);
                    npackage++;

                    if (npackage >= size)
                        break;
                }
            }

#if TODO
            NetUtil.SendElemCreate(elem, squadron, slots, code <= Mission.TYPE.SWEEP);
#endif
            return elem;
        }

        protected virtual bool LaunchElement(Element elem)
        {
            bool result = false;

            if (elem == null)
                return result;

            for (int squadron = 0; squadron < hangar.NumSquadrons(); squadron++)
            {
                for (int slot = 0; slot < hangar.SquadronSize(squadron); slot++)
                {
                    HangarSlot s = hangar.GetSlot(squadron, slot);

                    if (hangar.GetState(s) == Hangar.HANGAR_STATE.ALERT &&
                            hangar.GetPackageElement(s) == elem)
                    {

                        hangar.Launch(squadron, slot);
#if TODO
                        NetUtil.SendShipLaunch(ship, squadron, slot);
#endif

                        result = true;
                    }
                }
            }

            return result;
        }


        protected Sim sim;
        protected Ship ship;
        protected Hangar hangar;
        protected FlightPlanner flight_planner;
        protected int exec_time;
        protected int hold_time;
        protected int ai_level;

        protected Element[] patrol_elem = new Element[4];
    }
}