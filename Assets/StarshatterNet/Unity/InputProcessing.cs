﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StarshatterNet.Controllers;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.StarshatterNet.Unity
{
    public class InputProcessing : MonoBehaviour, IPointerClickHandler
    {
        public float mouseSensitivity = 10.0f;
        public float scrollSpeed = 10.0f;
        public bool useFixedUpdate = false;
        public bool invert = true;

        void Update()
        {
            // Get the mouse delta. This is not in the range -1...1
            Mouse.x = Input.GetAxis("Mouse X") * mouseSensitivity;
            Mouse.y = Input.GetAxis("Mouse Y") * mouseSensitivity;

            Mouse.w = Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;
            if (Input.GetMouseButton(0))
            {
                Mouse.l = 1;
                Debug.Log("Pressed left click.");
            }
            if (Input.GetMouseButton(1))
            {
                Mouse.r = 1;
                Debug.Log("Pressed right click.");
            }

            if (Input.GetMouseButton(2))
            {
                Mouse.m = 1;
                Debug.Log("Pressed middle click.");
            }
        }
        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.clickCount >= 2)
            {
                Debug.Log("double click");
                // do something ..
            }
        }
    }
}
