﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    FILE:         Keyboard.h/Keyboard.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Keyboard Input class
*/
using System;
using StarshatterNet.Config;
using UnityEngine;

namespace StarshatterNet.Controllers
{
    public class Keyboard : MotionController
    {
        public Keyboard()
        {
            sensitivity = 25;
            dead_zone = 100;

            for (int i = 0; i < MotionController.MaxActions; i++)
                action[i] = false;

            map[KeyMap.KEY_PLUS_X] = 'R';
            map[KeyMap.KEY_MINUS_X] = 'E';
            map[KeyMap.KEY_PLUS_Y] = KeyMap.VK_HOME;
            map[KeyMap.KEY_MINUS_Y] = KeyMap.VK_END;
            map[KeyMap.KEY_PLUS_Z] = KeyMap.VK_PRIOR;    // page up
            map[KeyMap.KEY_MINUS_Z] = KeyMap.VK_NEXT;     // page down

            map[KeyMap.KEY_PITCH_UP] = KeyMap.VK_DOWN;
            map[KeyMap.KEY_PITCH_DOWN] = KeyMap.VK_UP;
            map[KeyMap.KEY_YAW_LEFT] = KeyMap.VK_LEFT;
            map[KeyMap.KEY_YAW_RIGHT] = KeyMap.VK_RIGHT;
            map[KeyMap.KEY_ROLL_ENABLE] = 0;           // used to be VK_CONTROL
        }
        //public virtual ~Keyboard();

        // setup
        public override void MapKeys(KeyMapEntry[] mapping, int nkeys)
        {
            for (int i = 0; i < nkeys; i++)
            {
                KeyMapEntry k = mapping[i];

                if (k.act >= KeyMap.KEY_MAP_FIRST && k.act <= KeyMap.KEY_MAP_LAST)
                {
                    if (k.key == 0 || k.key > KeyMap.VK_MBUTTON && k.key < KeyMap.KEY_JOY_1)
                    {
                        map[k.act] = k.key;
                        alt[k.act] = k.alt;
                    }
                }
            }
        }

        // sample the physical device
        public override void Acquire() {  }

        // translations
        public override double X() { return x; }
        public override double Y() { return y; }
        public override double Z() { return z; }

        // rotations
        public override double Pitch() { return p; }
        public override double Roll() { return r; }
        public override double Yaw() { return w; }
        public override int Center() { return c; }

        // throttle
        public override double Throttle() { return t; }
        public override void SetThrottle(double throttle) { t = throttle; }

        // actions
        public override bool Action(int n) { return action[n]; }
        public override bool ActionMap(int n) { return KeyDownMap(n); }

        public static bool KeyDown(int key)
        {
            return Input.GetKeyDown(UnityKeyMap.ToKeyCode(key));
        }

        public static bool KeyDownMap(int key)
        {
            return Input.GetKeyDown(UnityKeyMap.ToKeyCode(map[key]));
        }

        public static void FlushKeys()
        {
            //TODO
        }

        public static Keyboard GetInstance()
        {
            return instance;
        }

        public static bool GetAsyncKeyState(int key)
        {
            return Input.GetKeyDown(UnityKeyMap.ToKeyCode(key)); // TODO

        }

        protected double x = 0, y = 0, z = 0, p = 0, r = 0, w = 0, t = 0;
        protected double p1 = 0, r1 = 0, w1 = 0;
        protected int c;
        protected bool[] action = new bool[MotionController.MaxActions];

        protected static int[] map = new int[KeyMap.KEY_MAP_SIZE];
        protected static int[] alt = new int[KeyMap.KEY_MAP_SIZE];
        protected static Keyboard instance = new Keyboard();
    }
}
