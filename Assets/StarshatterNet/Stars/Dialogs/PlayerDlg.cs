﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      PlayerDlg.h/PlayerDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class PlayerDlg : FormWindow
    {
        public PlayerDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
              base(s, 0, 0, s.Width(), s.Height(), c, p, "PlayerDlg")
        {
            manager = mgr;
            lst_roster = null; btn_add = null; btn_del = null;
            txt_name = null; txt_password = null; txt_squadron = null; txt_signature = null;
            img_rank = null;

            Init(def);
        }
        //public virtual ~PlayerDlg();
        public override void RegisterControls()
        {
            lst_roster = (ListBox)FindControl(200);
            btn_add = (Button)FindControl(101);
            btn_del = (Button)FindControl(102);
            txt_name = (EditBox)FindControl(201);
            txt_password = (EditBox)FindControl(202);
            txt_squadron = (EditBox)FindControl(203);
            txt_signature = (EditBox)FindControl(204);

            lbl_createdate = FindControl(205);
            lbl_rank = FindControl(206);
            lbl_flighttime = FindControl(207);
            lbl_missions = FindControl(208);
            lbl_kills = FindControl(209);
            lbl_losses = FindControl(210);
            lbl_points = FindControl(211);

            img_rank = (ImageBox)FindControl(220);
            REGISTER_CLIENT(ETD.EID_CLICK, img_rank, OnRank);

            for (int i = 0; i < 15; i++)
            {
                medals[i] = -1;
                img_medals[i] = (ImageBox)FindControl(230 + i);
                if (img_medals[i] != null)
                    REGISTER_CLIENT(ETD.EID_CLICK, img_medals[i], OnMedal);
            }

            for (int i = 0; i < 10; i++)
            {
                txt_chat[i] = (EditBox)FindControl(300 + i);
            }

            REGISTER_CLIENT(ETD.EID_SELECT, lst_roster, OnSelectPlayer);
            REGISTER_CLIENT(ETD.EID_CLICK, btn_add, OnAdd);
            REGISTER_CLIENT(ETD.EID_CLICK, btn_del, OnDel);
            REGISTER_CLIENT(ETD.EID_USER_1, btn_del, OnDelConfirm);

            apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, apply, OnApply);

            cancel = (Button)FindControl(2);
            REGISTER_CLIENT(ETD.EID_CLICK, cancel, OnCancel);
        }
        public override void Show()
        {
            base.Show();

            if (lst_roster == null) return;
            lst_roster.ClearItems();
            lst_roster.SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_FILLED_BOX);
            lst_roster.SetLeading(2);

            int current_index = 0;

            List<Player> roster = Player.GetRoster();
            for (int i = 0; i < roster.Count; i++)
            {
                Player p = roster[i];
                lst_roster.AddItem(p.Name());
                if (p == Player.GetCurrentPlayer())
                    current_index = i;
            }

            lst_roster.SetSelected(current_index);
            ShowPlayer();

            apply.SetEnabled(roster.Count > 0);
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnApply(null, null);
            }
        }

        // Operations:
        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            Player player = Player.GetCurrentPlayer();
            if (player != null)
            {
                UpdatePlayer();
                Player.Save();
            }

            Keyboard.FlushKeys();
            manager.ShowMenuDlg();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            Player.Load();
            Keyboard.FlushKeys();
            manager.ShowMenuDlg();
        }
        public virtual void OnSelectPlayer(ActiveWindow obj, AWEvent evnt)
        {
            if (lst_roster == null) return;

            UpdatePlayer();

            int index = lst_roster.GetSelection();

            List<Player> roster = Player.GetRoster();
            if (index >= 0 && index < roster.Count)
            {
                Player.SelectPlayer(roster[index]);
            }

            ShowPlayer();

            apply.SetEnabled(roster.Count > 0);
        }
        public virtual void OnAdd(ActiveWindow obj, AWEvent evnt)
        {
            Player.Create("Pilot");
            ShowPlayer();

            if (lst_roster == null) return;
            lst_roster.ClearItems();

            List<Player> roster = Player.GetRoster();
            for (int i = 0; i < roster.Count; i++)
            {
                Player p = roster[i];
                lst_roster.AddItem(p.Name());
                lst_roster.SetSelected(i, (p == Player.GetCurrentPlayer()));
            }

            apply.SetEnabled(roster.Count > 0);
        }
        public virtual void OnDel(ActiveWindow obj, AWEvent evnt)
        {
            if (Player.GetCurrentPlayer() == null)
                return;

            ConfirmDlg confirm = manager.GetConfirmDlg();
            if (confirm != null)
            {
                string msg = string.Format(Game.GetText("PlayerDlg.are-you-sure"),
                                            Player.GetCurrentPlayer().Name());
                confirm.SetMessage(msg);
                confirm.SetTitle(Game.GetText("PlayerDlg.confirm-delete"));
                confirm.SetParentControl(btn_del);

                manager.ShowConfirmDlg();
            }

            else
            {
                OnDelConfirm(obj, evnt);
            }
        }
        public virtual void OnDelConfirm(ActiveWindow obj, AWEvent evnt)
        {
            Player.Destroy(Player.GetCurrentPlayer());
            ShowPlayer();

            if (lst_roster == null) return;
            lst_roster.ClearItems();

            List<Player> roster = Player.GetRoster();
            for (int i = 0; i < roster.Count; i++)
            {
                Player p = roster[i];
                lst_roster.AddItem(p.Name());
                lst_roster.SetSelected(i, (p == Player.GetCurrentPlayer()));
            }

            apply.SetEnabled(roster.Count > 0);
        }
        public virtual void OnRank(ActiveWindow obj, AWEvent evnt)
        {
            Player p = Player.GetCurrentPlayer();
            AwardShowDlg award_dlg = manager.GetAwardDlg();

            if (p != null && award_dlg != null)
            {
                award_dlg.SetRank(p.Rank());
                manager.ShowAwardDlg();
            }
        }
        public virtual void OnMedal(ActiveWindow obj, AWEvent evnt)
        {
            Player p = Player.GetCurrentPlayer();
            AwardShowDlg award_dlg = manager.GetAwardDlg();

            if (p != null && award_dlg != null)
            {
                int m = -1, i;

                for (i = 0; i < 15; i++)
                {
                    if (evnt.window == img_medals[i])
                    {
                        m = i;
                        break;
                    }
                }

                if (m >= 0)
                {
                    award_dlg.SetMedal(medals[i]);
                    manager.ShowAwardDlg();
                }
            }
        }

        public virtual void UpdatePlayer()
        {
            Player p = Player.GetCurrentPlayer();

            if (p != null)
            {
                if (txt_name != null) p.SetName(txt_name.GetText());
                if (txt_password != null) p.SetPassword(txt_password.GetText());
                if (txt_squadron != null) p.SetSquadron(txt_squadron.GetText());
                if (txt_signature != null) p.SetSignature(txt_signature.GetText());

                for (int i = 0; i < 10; i++)
                {
                    if (txt_chat[i] != null)
                        p.SetChatMacro(i, txt_chat[i].GetText());
                }
            }
        }
        public virtual void ShowPlayer()
        {
            Player p = Player.GetCurrentPlayer();

            if (p != null)
            {
                if (txt_name != null) txt_name.SetText(p.Name());
                if (txt_password != null) txt_password.SetText(p.Password());
                if (txt_squadron != null) txt_squadron.SetText(p.Squadron());
                if (txt_signature != null) txt_signature.SetText(p.Signature());

                string flight_time, missions, kills, losses, points;
                FormatUtil.FormatTime(out flight_time, p.FlightTime().TotalSeconds);
                missions = string.Format("{0}", p.Missions());
                kills = string.Format("{0}", p.Kills());
                losses = string.Format("{0}", p.Losses());
                points = string.Format("{0}", p.Points());

                if (lbl_createdate != null) lbl_createdate.SetText(FormatUtil.FormatTimeString(p.CreateDate()));
                if (lbl_rank != null) lbl_rank.SetText(Player.RankName(p.Rank()));
                if (lbl_flighttime != null) lbl_flighttime.SetText(flight_time);
                if (lbl_missions != null) lbl_missions.SetText(missions);
                if (lbl_kills != null) lbl_kills.SetText(kills);
                if (lbl_losses != null) lbl_losses.SetText(losses);
                if (lbl_points != null) lbl_points.SetText(points);

                if (img_rank != null)
                {
                    img_rank.SetPicture( Player.RankInsignia(p.Rank(), 0).Texture);
                    img_rank.Show();
                }

                for (int i = 0; i < 15; i++)
                {
                    if (img_medals[i] != null)
                    {
                        int medal = p.Medal(i);
                        if (medal != 0)
                        {
                            medals[i] = medal;
                            img_medals[i].SetPicture( Player.MedalInsignia(medal, 0).Texture);
                            img_medals[i].Show();
                        }
                        else
                        {
                            medals[i] = -1;
                            img_medals[i].Hide();
                        }
                    }
                }

                for (int i = 0; i < 10; i++)
                {
                    if (txt_chat[i] != null)
                        txt_chat[i].SetText(p.ChatMacro(i));
                }
            }
            else
            {
                if (txt_name != null) txt_name.SetText("");
                if (txt_password != null) txt_password.SetText("");
                if (txt_squadron != null) txt_squadron.SetText("");
                if (txt_signature != null) txt_signature.SetText("");

                if (lbl_createdate != null) lbl_createdate.SetText("");
                if (lbl_rank != null) lbl_rank.SetText("");
                if (lbl_flighttime != null) lbl_flighttime.SetText("");
                if (lbl_missions != null) lbl_missions.SetText("");
                if (lbl_kills != null) lbl_kills.SetText("");
                if (lbl_losses != null) lbl_losses.SetText("");
                if (lbl_points != null) lbl_points.SetText("");

                if (img_rank != null) img_rank.Hide();

                for (int i = 0; i < 15; i++)
                {
                    medals[i] = -1;
                    if (img_medals[i] != null)
                        img_medals[i].Hide();
                }

                for (int i = 0; i < 10; i++)
                {
                    if (txt_chat[i] != null)
                        txt_chat[i].SetText("");
                }
            }
        }


        protected MenuScreen manager;

        protected ListBox lst_roster;
        protected Button btn_add;
        protected Button btn_del;

        protected EditBox txt_name;
        protected EditBox txt_password;
        protected EditBox txt_squadron;
        protected EditBox txt_signature;

        protected EditBox[] txt_chat = new EditBox[10];

        protected ActiveWindow lbl_createdate;
        protected ActiveWindow lbl_rank;
        protected ActiveWindow lbl_flighttime;
        protected ActiveWindow lbl_missions;
        protected ActiveWindow lbl_kills;
        protected ActiveWindow lbl_losses;
        protected ActiveWindow lbl_points;
        protected ImageBox img_rank;
        protected ImageBox[] img_medals = new ImageBox[15];
        protected int[] medals = new int[15];

        protected Button apply;
        protected Button cancel;
    }
}