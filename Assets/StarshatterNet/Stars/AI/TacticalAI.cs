﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      TacticalAI.h/TacticalAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Common base class and interface for mid-level (tactical) AI
*/
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.AI
{
    public class TacticalAI : Director
    {

        public TacticalAI(ShipAI ai)
        {
            ship = null; ship_ai = null; carrier_ai = null; navpt = null; orders = null;
            action = 0; threat_level = 0; support_level = 1;
            directed_tgtid = 0;

            if (ai != null)
            {
                ship_ai = ai;
                ship = ai.GetShip();

                Sim sim = Sim.GetSim();

                if (ship != null && ship.GetHangar() != null && ship.GetCommandAILevel() > 0 &&
                        ship != sim.GetPlayerShip())
                    carrier_ai = new CarrierAI(ship, ship_ai.GetAILevel());
            }

            agression = 0;
            roe = ROE.FLEXIBLE;
            element_index = 1;
            exec_time = exec_time_seed;
            exec_time_seed += 17;
        }
        //~TacticalAI();

        public enum ROE
        {
            NONE,
            SELF_DEFENSIVE,
            DEFENSIVE,
            DIRECTED,
            FLEXIBLE,
            AGRESSIVE
        };

        public override void ExecFrame(double secs)
        {
            const int exec_period = 1000;

            if (ship == null || ship_ai == null)
                return;

            navpt = ship.GetNextNavPoint();
            orders = ship.GetRadioOrders();

            if ((int)Game.GameTime() - exec_time > exec_period)
            {
                element_index = ship.GetElementIndex();

                CheckOrders();
                SelectTarget();
                FindThreat();
                FindSupport();

                if (element_index > 1)
                {
                    Instruction.FORMATION formation = 0;

                    if (orders != null && orders.Formation() >= 0)
                        formation = orders.Formation();

                    else if (navpt != null)
                        formation = navpt.Formation();

                    FindFormationSlot(formation);
                }

                ship_ai.SetNavPoint(navpt);

                if (carrier_ai != null)
                    carrier_ai.ExecFrame(secs);

                exec_time += exec_period;
            }
        }

        public virtual ROE RulesOfEngagement() { return roe; }
        public virtual double ThreatLevel() { return threat_level; }
        public virtual double SupportLevel() { return support_level; }


        // pick the best target if we don't have one yet:
        protected virtual void CheckOrders()
        {
            directed_tgtid = 0;

            if (CheckShipOrders())
                return;

            if (CheckFlightPlan())
                return;

            if (CheckObjectives())
                return;
        }

        protected virtual bool CheckShipOrders()
        {
            return ProcessOrders();
        }

        protected virtual bool ProcessOrders()
        {
            if (ship_ai != null)
                ship_ai.ClearPatrol();

            if (orders != null && orders.EMCON() > 0)
            {
                int desired_emcon = orders.EMCON();

                if (ship_ai != null && (ship_ai.GetThreat() != null || ship_ai.GetThreatMissile() != null))
                    desired_emcon = 3;

                if (ship.GetEMCON() != desired_emcon)
                    ship.SetEMCON(desired_emcon);
            }

            if (orders != null && orders.Action() != 0)
            {
                switch ((RadioMessage.ACTION)orders.Action())
                {
                    case RadioMessage.ACTION.ATTACK:
                    case RadioMessage.ACTION.BRACKET:
                    case RadioMessage.ACTION.IDENTIFY:
                        {
                            bool tgt_ok = false;
                            SimObject tgt = orders.GetTarget();

                            if (tgt != null && tgt.Type() == (int)SimObject.TYPES.SIM_SHIP)
                            {
                                Ship tgt_ship = (Ship)tgt;

                                if (CanTarget(tgt_ship))
                                {
                                    roe = ROE.DIRECTED;
                                    SelectTargetDirected((Ship)tgt);

                                    ship_ai.SetBracket((int)orders.Action() == (int)RadioMessage.ACTION.BRACKET);
                                    ship_ai.SetIdentify((int)orders.Action() == (int)RadioMessage.ACTION.IDENTIFY);
                                    ship_ai.SetNavPoint(null);

                                    tgt_ok = true;
                                }
                            }

                            if (!tgt_ok)
                                ClearRadioOrders();
                        }
                        break;

                    case RadioMessage.ACTION.ESCORT:
                    case RadioMessage.ACTION.COVER_ME:
                        {
                            SimObject tgt = orders.GetTarget();
                            if (tgt != null && tgt.Type() == (int)SimObject.TYPES.SIM_SHIP)
                            {
                                roe = ROE.DEFENSIVE;
                                ship_ai.SetWard((Ship)tgt);
                                ship_ai.SetNavPoint(null);
                            }
                            else
                            {
                                ClearRadioOrders();
                            }
                        }
                        break;

                    case RadioMessage.ACTION.WEP_FREE:
                        roe = ROE.AGRESSIVE;
                        ship_ai.DropTarget(0.1);
                        break;

                    case RadioMessage.ACTION.WEP_HOLD:
                    case RadioMessage.ACTION.FORM_UP:
                        roe = ROE.NONE;
                        ship_ai.DropTarget(5);
                        break;

                    case RadioMessage.ACTION.MOVE_PATROL:
                        roe = ROE.SELF_DEFENSIVE;
                        ship_ai.SetPatrol(orders.Location());
                        ship_ai.SetNavPoint(null);
                        ship_ai.DropTarget(RandomHelper.Random(5, 10));
                        break;

                    case RadioMessage.ACTION.RTB:
                    case RadioMessage.ACTION.DOCK_WITH:
                        roe = ROE.NONE;

                        ship_ai.DropTarget(10);

                        if (ship.GetInbound() == null)
                        {
                            RadioMessage msg = null;
                            Ship controller = ship.GetController();

                            if ((int)orders.Action() == (int)RadioMessage.ACTION.DOCK_WITH && orders.GetTarget() != null)
                            {
                                controller = (Ship)orders.GetTarget();
                            }

                            if (controller == null)
                            {
                                Element elem = ship.GetElement();
                                if (elem != null && elem.GetCommander() != null)
                                {
                                    Element cmdr = elem.GetCommander();
                                    controller = cmdr.GetShip(1);
                                }
                            }

                            if (controller != null && controller.GetHangar() != null &&
                                    controller.GetHangar().CanStow(ship))
                            {
                                SimRegion self_rgn = ship.GetRegion();
                                SimRegion rtb_rgn = controller.GetRegion();

                                if (self_rgn == rtb_rgn)
                                {
                                    double range = (controller.Location() - ship.Location()).Length;

                                    if (range < 50e3)
                                    {
                                        msg = new RadioMessage(controller, ship, RadioMessage.ACTION.CALL_INBOUND);
                                        RadioTraffic.Transmit(msg);
                                    }
                                }
                            }
                            else
                            {
                                ship.ClearRadioOrders();
                            }

                            ship_ai.SetNavPoint(null);
                        }
                        break;

                    case RadioMessage.ACTION.QUANTUM_TO:
                    case RadioMessage.ACTION.FARCAST_TO:
                        roe = ROE.NONE;
                        ship_ai.DropTarget(10);
                        break;

                }

                action = orders.Action();
                return true;
            }

            // if we had an action before, this must be a "cancel orders" 
            else if (action != 0)
            {
                ClearRadioOrders();
            }

            return false;
        }
        protected virtual bool CheckFlightPlan()
        {
            Ship ward = null;

            // Find next Instruction:
            navpt = ship.GetNextNavPoint();

            roe = ROE.FLEXIBLE;

            if (navpt != null)
            {
                switch (navpt.Action())
                {
                    case Instruction.ACTION.LAUNCH:
                    case Instruction.ACTION.DOCK:
                    case Instruction.ACTION.RTB:
                        roe = ROE.NONE;
                        break;

                    case Instruction.ACTION.VECTOR:
                        roe = ROE.SELF_DEFENSIVE;
                        break;

                    case Instruction.ACTION.DEFEND:
                    case Instruction.ACTION.ESCORT:
                        roe = ROE.DEFENSIVE;
                        break;

                    case Instruction.ACTION.INTERCEPT:
                        roe = ROE.DIRECTED;
                        break;

                    case Instruction.ACTION.RECON:
                    case Instruction.ACTION.STRIKE:
                    case Instruction.ACTION.ASSAULT:
                        roe = ROE.DIRECTED;
                        break;

                    case Instruction.ACTION.PATROL:
                    case Instruction.ACTION.SWEEP:
                        roe = ROE.FLEXIBLE;
                        break;

                    default: break;
                }

                if (roe == ROE.DEFENSIVE)
                {
                    SimObject tgt = navpt.GetTarget();

                    if (tgt != null && tgt.Type() == (int)SimObject.TYPES.SIM_SHIP)
                        ward = (Ship)tgt;
                }


                if (navpt.EMCON() > 0)
                {
                    int desired_emcon = navpt.EMCON();

                    if (ship_ai != null && (ship_ai.GetThreat() != null || ship_ai.GetThreatMissile() != null))
                        desired_emcon = 3;

                    if (ship.GetEMCON() != desired_emcon)
                        ship.SetEMCON(desired_emcon);
                }
            }

            if (ship_ai != null)
                ship_ai.SetWard(ward);

            return (navpt != null);
        }

        protected virtual bool CheckObjectives()
        {
            bool processed = false;
            Ship ward = null;
            Element elem = ship.GetElement();

            if (elem != null)
            {
                Instruction obj = elem.GetTargetObjective();

                if (obj != null)
                {
                    ship_ai.ClearPatrol();

                    if (obj.Action() != 0)
                    {
                        switch (obj.Action())
                        {
                            case Instruction.ACTION.INTERCEPT:
                            case Instruction.ACTION.STRIKE:
                            case Instruction.ACTION.ASSAULT:
                                {
                                    SimObject tgt = obj.GetTarget();
                                    if (tgt != null && tgt.Type() == (int)SimObject.TYPES.SIM_SHIP)
                                    {
                                        roe = ROE.DIRECTED;
                                        SelectTargetDirected((Ship)tgt);
                                    }
                                }
                                break;

                            case Instruction.ACTION.DEFEND:
                            case Instruction.ACTION.ESCORT:
                                {
                                    SimObject tgt = obj.GetTarget();
                                    if (tgt != null && tgt.Type() == (int)SimObject.TYPES.SIM_SHIP)
                                    {
                                        roe = ROE.DEFENSIVE;
                                        ward = (Ship)tgt;
                                    }
                                }
                                break;

                            default:
                                break;
                        }
                    }

                    orders = obj;
                    processed = true;
                }
            }

            ship_ai.SetWard(ward);
            return processed;
        }

        protected virtual void SelectTarget()
        {
            if (ship == null)
            {
                roe = ROE.NONE;
                return;
            }

            // unarmed vessels should never engage an enemy:
            if (ship.Weapons().Count < 1)
                roe = ROE.NONE;

            SimObject target = ship_ai.GetTarget();
            SimObject ward = ship_ai.GetWard();

            // if not allowed to engage, drop and return:
            if (roe == ROE.NONE)
            {
                if (target != null)
                    ship_ai.DropTarget();
                return;
            }

            // if we have abandoned our ward, drop and return:
            if (ward != null && roe != ROE.AGRESSIVE)
            {
                double d = (ward.Location() - ship.Location()).Length;
                double safe_zone = 50e3;

                if (target != null)
                {
                    if (ship.IsStarship())
                        safe_zone = 100e3;

                    if (d > safe_zone)
                    {
                        ship_ai.DropTarget();
                        return;
                    }
                }
                else
                {
                    if (d > safe_zone)
                    {
                        return;
                    }
                }
            }

            // already have a target, keep it:
            if (target != null)
            {
                if (target.Life() != 0)
                {
                    CheckTarget();

                    // frigates need to be ready to abandon ship-type targets
                    // in favor of drone-type targets, others should just go
                    // with what they have:
                    if (ship.Class() != CLASSIFICATION.CORVETTE && ship.Class() != CLASSIFICATION.FRIGATE)
                        return;

                    // in case the check decided to drop the target:
                    target = ship_ai.GetTarget();
                }

                // if the old target is dead, forget it:
                else
                {
                    ship_ai.DropTarget();
                    target = null;
                }
            }

            // if not allowed to acquire, forget it:
            if (ship_ai.DropTime() > 0)
                return;

            if (roe == ROE.DIRECTED)
            {
                if (target != null && target.Type() == (int)SimObject.TYPES.SIM_SHIP)
                    SelectTargetDirected((Ship)target);
                else if (navpt != null && navpt.GetTarget() != null && navpt.GetTarget().Type() == (int)SimObject.TYPES.SIM_SHIP)
                    SelectTargetDirected((Ship)navpt.GetTarget());
                else
                    SelectTargetDirected();
            }

            else
            {
                SelectTargetOpportunity();

                // don't switch one ship target for another...
                if (ship.Class() == CLASSIFICATION.CORVETTE || ship.Class() == CLASSIFICATION.FRIGATE)
                {
                    SimObject potential_target = ship_ai.GetTarget();
                    if (target != null && potential_target != null && target != potential_target)
                    {
                        if (target.Type() == (int)SimObject.TYPES.SIM_SHIP &&
                                potential_target.Type() == (int)SimObject.TYPES.SIM_SHIP)
                        {

                            ship_ai.SetTarget(target);
                        }
                    }
                }
            }
        }
        protected virtual void SelectTargetDirected(Ship tgt = null)
        {
            Ship potential_target = tgt;

            // try to target one of the element's objectives
            // (if it shows up in the contact list)

            if (tgt == null)
            {
                Element elem = ship.GetElement();

                if (elem != null)
                {
                    Instruction objective = elem.GetTargetObjective();

                    if (objective != null)
                    {
                        SimObject obj_sim_obj = objective.GetTarget();
                        Ship obj_tgt = null;

                        if (obj_sim_obj != null && obj_sim_obj.Type() == (int)SimObject.TYPES.SIM_SHIP)
                            obj_tgt = (Ship)obj_sim_obj;

                        if (obj_tgt != null)
                        {
                            foreach (Contact contact in ship.ContactList())
                            {
                                if (potential_target != null) break;
                                Ship test = contact.GetShip();

                                if (obj_tgt == test)
                                {
                                    potential_target = test;
                                }
                            }
                        }
                    }
                }
            }

            if (!CanTarget(potential_target))
                potential_target = null;

            ship_ai.SetTarget(potential_target);

            if (tgt != null && tgt == ship_ai.GetTarget())
                directed_tgtid = tgt.Identity();
            else
                directed_tgtid = 0;
        }

        protected virtual void SelectTargetOpportunity()
        {
            // NON-COMBATANTS do not pick targets of opportunity:
            if (ship.GetIFF() == 0)
                return;

            SimObject potential_target = null;

            // pick the closest combatant ship with a different IFF code:
            double target_dist = ship.Design().commit_range;

            SimObject ward = ship_ai.GetWard();

            // FRIGATES are primarily anti-air platforms, but may
            // also attack smaller starships:

            if (ship.Class() == CLASSIFICATION.CORVETTE || ship.Class() == CLASSIFICATION.FRIGATE)
            {
                Ship current_ship_target = null;
                Shot current_shot_target = null;

                // if we are escorting a larger warship, it is good to attack
                // the same target as our ward:

                if (ward != null)
                {
                    Ship s = (Ship)ward;

                    if (s.Class() > ship.Class())
                    {
                        SimObject obj = s.GetTarget();

                        if (obj != null && obj.Type() == (int)SimObject.TYPES.SIM_SHIP)
                        {
                            current_ship_target = (Ship)obj;
                            target_dist = (ship.Location() - obj.Location()).Length;
                        }
                    }
                }

                foreach (Contact contact in ship.ContactList())
                {
                    Ship c_ship = contact.GetShip();
                    Shot c_shot = contact.GetShot();

                    if (c_ship == null && c_shot == null)
                        continue;

                    int c_iff = contact.GetIFF(ship);
                    bool rogue = c_ship != null && c_ship.IsRogue();
                    bool tgt_ok = c_iff > 0 &&
                    c_iff != ship.GetIFF() &&
                    c_iff < 1000;

                    if (rogue || tgt_ok)
                    {
                        if (c_ship != null && c_ship != ship && !c_ship.InTransition())
                        {
                            if (c_ship.Class() < CLASSIFICATION.DESTROYER ||
                                    (c_ship.Class() >= CLASSIFICATION.MINE && c_ship.Class() <= CLASSIFICATION.DEFSAT))
                            {
                                // found an enemy, check distance:
                                double dist = (ship.Location() - c_ship.Location()).Length;

                                if (dist < 0.75 * target_dist &&
                                        (current_ship_target == null || c_ship.Class() <= current_ship_target.Class()))
                                {
                                    current_ship_target = c_ship;
                                    target_dist = dist;
                                }
                            }
                        }

                        else if (c_shot != null)
                        {
                            // found an enemy shot, is there enough time to engage?
                            if (c_shot.GetEta() < 3)
                                continue;

                            // found an enemy shot, check distance:
                            double dist = (ship.Location() - c_shot.Location()).Length;

                            if (current_shot_target == null)
                            {
                                current_shot_target = c_shot;
                                target_dist = dist;
                            }

                            // is this shot a better target than the one we've found?
                            else
                            {
                                Ship ward2 = ship_ai.GetWard();

                                if ((c_shot.IsTracking(ward2) || c_shot.IsTracking(ship)) &&
                                        (!current_shot_target.IsTracking(ward2) ||
                                            !current_shot_target.IsTracking(ship)))
                                {
                                    current_shot_target = c_shot;
                                    target_dist = dist;
                                }
                                else if (dist < target_dist)
                                {
                                    current_shot_target = c_shot;
                                    target_dist = dist;
                                }
                            }
                        }
                    }
                }

                if (current_shot_target != null)
                    potential_target = current_shot_target;
                else
                    potential_target = current_ship_target;
            }

            // ALL OTHER SHIP CLASSES ignore fighters and only engage
            // other starships:

            else
            {
                List<Ship> ward_threats = new List<Ship>();

                foreach (Contact contact in ship.ContactList())
                {
                    Ship c_ship = contact.GetShip();

                    if (c_ship == null)
                        continue;

                    int c_iff = contact.GetIFF(ship);
                    bool rogue = c_ship.IsRogue();
                    bool tgt_ok = c_ship != ship &&
                    c_iff > 0 &&
                    c_iff != ship.GetIFF() &&
                    !c_ship.InTransition();

                    if (rogue || tgt_ok)
                    {
                        if (c_ship.IsStarship() || c_ship.IsStatic())
                        {
                            // found an enemy, check distance:
                            double dist = (ship.Location() - c_ship.Location()).Length;

                            if (dist < 0.75 * target_dist)
                            {
                                potential_target = c_ship;
                                target_dist = dist;
                            }

                            if (ward != null && c_ship.IsTracking(ward))
                            {
                                ward_threats.Add(c_ship);
                            }
                        }
                    }
                }

                // if this ship is protecting a ward,
                // prefer targets that are threatening that ward:
                if (potential_target != null && ward_threats.Count != 0 && !ward_threats.Contains((Ship)potential_target))
                {
                    target_dist *= 2;

                    foreach (Ship threat in ward_threats)
                    {
                        double dist = (ward.Location() - threat.Location()).Length;

                        if (dist < target_dist)
                        {
                            potential_target = threat;
                            target_dist = dist;
                        }
                    }
                }
            }

            if (ship.Class() != CLASSIFICATION.CARRIER && ship.Class() != CLASSIFICATION.SWACS)
                ship_ai.SetTarget(potential_target);
        }

        protected virtual void CheckTarget()
        {
            SimObject tgt = ship_ai.GetTarget();

            if (tgt == null) return;

            if (tgt.GetRegion() != ship.GetRegion())
            {
                ship_ai.DropTarget();
                return;
            }

            if (tgt.Type() == (int)SimObject.TYPES.SIM_SHIP)
            {
                Ship target = (Ship)tgt;

                // has the target joined our side?
                if (target.GetIFF() == ship.GetIFF() && !target.IsRogue())
                {
                    ship_ai.DropTarget();
                    return;
                }

                // is the target already jumping/breaking/dying?
                if (target.InTransition())
                {
                    ship_ai.DropTarget();
                    return;
                }

                // have we been ordered to pursue the target?
                if (directed_tgtid != 0)
                {
                    if (directed_tgtid != target.Identity())
                    {
                        ship_ai.DropTarget();
                    }

                    return;
                }

                // can we catch the target?
                if (target.Design().vlimit <= ship.Design().vlimit ||
                        ship.Velocity().Length <= ship.Design().vlimit)
                    return;

                // is the target now out of range?
                WeaponDesign wep_dsn = ship.GetPrimaryDesign();
                if (wep_dsn == null)
                    return;

                // compute the "give up" range:
                double drop_range = 3 * wep_dsn.max_range;
                if (drop_range > 0.75 * ship.Design().commit_range)
                    drop_range = 0.75 * ship.Design().commit_range;

                double range = (target.Location() - ship.Location()).Length;
                if (range < drop_range)
                    return;

                // is the target closing or separating?
                Point delta = (target.Location() + target.Velocity()) -
                (ship.Location() + ship.Velocity());

                if (delta.Length < range)
                    return;

                ship_ai.DropTarget();
            }

            else if (tgt.Type() == (int)SimObject.TYPES.SIM_DRONE)
            {
                Drone drone = (Drone)tgt;

                // is the target still a threat?
                if (drone.GetEta() < 1 || drone.GetTarget() == null)
                    ship_ai.DropTarget();
            }
        }

        protected virtual void FindThreat()
        {
            // pick the closest contact on Threat Warning System:
            Ship threat = null;
            Shot threat_missile = null;
            Ship rumor = null;
            double threat_dist = 1e9;
            const uint THREAT_REACTION_TIME = 1000; // 1 second

            foreach (Contact contact in ship.ContactList())
            {
                if (contact.Threat(ship) &&
                        (Game.GameTime() - contact.AcquisitionTime()) > THREAT_REACTION_TIME)
                {

                    if (contact.GetShot() != null)
                    {
                        threat_missile = contact.GetShot();
                        rumor = (Ship)threat_missile.Owner();
                    }
                    else
                    {
                        double rng = contact.Range(ship);

                        Ship c_ship = contact.GetShip();
                        if (c_ship != null && !c_ship.InTransition() &&
                                c_ship.Class() != CLASSIFICATION.FREIGHTER &&
                                c_ship.Class() != CLASSIFICATION.FARCASTER)
                        {

                            if (c_ship.GetTarget() == ship)
                            {
                                if (threat == null || c_ship.Class() > threat.Class())
                                {
                                    threat = c_ship;
                                    threat_dist = 0;
                                }
                            }
                            else if (rng < threat_dist)
                            {
                                threat = c_ship;
                                threat_dist = rng;
                            }
                        }
                    }
                }
            }

            if (rumor != null && !rumor.InTransition())
            {
                foreach (Contact contact in ship.ContactList())
                {
                    if (contact.GetShip() == rumor)
                    {
                        rumor = null;
                        ship_ai.ClearRumor();
                        break;
                    }
                }
            }
            else
            {
                rumor = null;
                ship_ai.ClearRumor();
            }

            ship_ai.SetRumor(rumor);
            ship_ai.SetThreat(threat);
            ship_ai.SetThreatMissile(threat_missile);
        }

        protected virtual void FindSupport()
        {
            if (ship_ai.GetThreat() == null)
            {
                ship_ai.SetSupport(null);
                return;
            }

            // pick the biggest friendly contact in the sector:
            Ship support = null;
            double support_dist = 1e9;

            foreach (Contact   contact in ship.ContactList())
            {
                if (contact.GetShip() != null && contact.GetIFF(ship) == ship.GetIFF())
                {
                    Ship c_ship = contact.GetShip();

                    if (c_ship != ship && c_ship.Class() >= ship.Class() && !c_ship.InTransition())
                    {
                        if (support == null || c_ship.Class() > support.Class())
                            support = c_ship;
                    }
                }
            }

            ship_ai.SetSupport(support);
        }

        protected virtual void FindFormationSlot(Instruction.FORMATION formation)
        {
            // find the formation delta:
            int s = element_index - 1;
            Point delta = new Point(10 * s, 0, 10 * s);

            // diamond:
            if (formation == Instruction.FORMATION.DIAMOND)
            {
                switch (element_index)
                {
                    case 2: delta = new Point(10, 0, -12); break;
                    case 3: delta = new Point(-10, 0, -12); break;
                    case 4: delta = new Point(0, 0, -24); break;
                }
            }

            // spread:
            if (formation == Instruction.FORMATION.SPREAD)
            {
                switch (element_index)
                {
                    case 2: delta = new Point(15, 0, 0); break;
                    case 3: delta = new Point(-15, 0, 0); break;
                    case 4: delta = new Point(-30, 0, 0); break;
                }
            }

            // box:
            if (formation == Instruction.FORMATION.BOX)
            {
                switch (element_index)
                {
                    case 2: delta = new Point(15, 0, 0); break;
                    case 3: delta = new Point(0, -1, -15); break;
                    case 4: delta = new Point(15, -1, -15); break;
                }
            }

            // trail:
            if (formation == Instruction.FORMATION.TRAIL)
            {
                delta = new Point(0, 0, -15 * s);
            }

            ship_ai.SetFormationDelta(delta * (float)ship.Radius() * 2);
        }

        protected virtual bool CanTarget(Ship tgt)
        {
            bool result = false;

            if (tgt != null && !tgt.InTransition())
            {
                if (tgt.IsRogue() || tgt.GetIFF() != ship.GetIFF())
                    result = true;
            }

            return result;
        }

        protected virtual void ClearRadioOrders()
        {
            action = 0;
            roe = ROE.FLEXIBLE;

            if (ship_ai != null)
                ship_ai.DropTarget(0.1);

            if (ship != null)
                ship.ClearRadioOrders();

        }


        protected Ship ship;
        protected ShipAI ship_ai;
        protected CarrierAI carrier_ai;

        protected Instruction navpt;
        protected Instruction orders;

        protected double agression;
        protected ROE roe;
        protected int element_index;
        protected Instruction.ACTION action;
        protected int exec_time;
        protected int directed_tgtid;

        protected double threat_level;
        protected double support_level;

        private static int exec_time_seed = 0;

    }
}