﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      Window.h/Window.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Window class (a region of a screen or buffer)
    In Unity this is converted as a GameObject with a RectTransform
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Stars;
using UnityEngine;
using Rect = StarshatterNet.Stars.Rect;
using POINT = UnityEngine.Vector2;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics.General;
using DigitalRune.Mathematics;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Views;
using UnityEngine.UI;

namespace StarshatterNet.Gen.Graphics
{
    public class Window
    {
        public Window(Screen s, int ax, int ay, int aw, int ah)
        {
            screen = s; shown = true; font = null;
            rect = new Rect(ax, ay, aw, ah);
            CreateGameObject();
        }
        public Window(Screen s, Canvas canvas, int ax, int ay, int aw, int ah)
        {
            screen = s; shown = true; font = null;
            rect = new Rect(ax, ay, aw, ah);
            this.canvas = canvas;
            CreateGameObject();
        }

        //virtual ~Window();

        //int operator ==(const Window& that)   { return this == &that; }
        protected void CreateGameObject()
        {
            windowObj = new GameObject();
            windowObj.name = "Window";
            rectTransform = windowObj.AddComponent<RectTransform>();
            //rectTransform.transform.SetParent(canvas.transform);
            rectTransform.pivot = Vector2.zero;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.zero;
            this.MoveTo();
        }
        public void CreateCanvas()
        {
            GameObject g = new GameObject();
            g.name = "WindowCanvas";
            canvas = g.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            CanvasScaler cs = g.AddComponent<CanvasScaler>();
            cs.scaleFactor = 1.0f;
            cs.dynamicPixelsPerUnit = 100f;
            GraphicRaycaster gr = g.AddComponent<GraphicRaycaster>();

            rectTransform.transform.SetParent(canvas.transform);
            rectTransform.pivot = Vector2.zero;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.zero;
            this.MoveTo();
        }

        // Screen dimensions:
        public Screen GetScreen() { return screen; }
        public Rect GetRect() { return rect; }
        public int X() { return rect.x; }
        public int Y() { return rect.y; }
        public int Width() { return rect.w; }
        public int Height() { return rect.h; }

        // Operations:
        public virtual void Paint()
        {
            foreach (View v in view_list)
                v.Refresh();
        }
        public virtual void Show() { shown = true; }
        public virtual void Hide() { shown = false; }
        public virtual bool IsShown() { return shown; }

        protected virtual void MoveTo()
        {
            rectTransform.sizeDelta = new Vector2(rect.w, rect.h);
            rectTransform.anchoredPosition = new Vector2(rect.x, rect.y);
            //Debug.Log("Rect pos" + new Vector2(rect.x, rect.y));
            //Debug.Log("Rect localPosition" + rectTransform.localPosition);

            foreach (View v in view_list)
                v.OnWindowMove();

            //Debug.Log("Rect localPosition" + rectTransform.localPosition);
        }
        public virtual void MoveTo(Rect r)
        {
            if (rect.x == r.x &&
                    rect.y == r.y &&
                    rect.w == r.w &&
                    rect.h == r.h)
                return;

            rect = r;
            MoveTo();
        }

        public virtual bool AddView(IView v)
        {
            if (v == null) return false;

            if (!view_list.Contains(v))
                view_list.Add(v);

            return true;
        }

        public virtual bool DelView(IView v)
        {
            if (v == null) return false;

            return view_list.Remove(v);
        }


        public Rect ClipRect(Rect r)
        {
            Rect clip_rect = r;

            clip_rect.x += rect.x;
            clip_rect.y += rect.y;

            if (clip_rect.x < rect.x)
            {
                clip_rect.w -= rect.x - clip_rect.x;
                clip_rect.x = rect.x;
            }

            if (clip_rect.y < rect.y)
            {
                clip_rect.h -= rect.y - clip_rect.y;
                clip_rect.y = rect.y;
            }

            if (clip_rect.x + clip_rect.w > rect.x + rect.w)
                clip_rect.w = rect.x + rect.w - clip_rect.x;

            if (clip_rect.y + clip_rect.h > rect.y + rect.h)
                clip_rect.h = rect.y + rect.h - clip_rect.y;

            return clip_rect;
        }
        private static void swap(ref int a, ref int b) { int tmp = a; a = b; b = tmp; }
        private static void sort(ref int a, ref int b) { if (a > b) swap(ref a, ref b); }
        private static void swap(ref double a, ref double b) { double tmp = a; a = b; b = tmp; }
        private static void sort(ref double a, ref double b) { if (a > b) swap(ref a, ref b); }

        public bool ClipLine(ref int x1, ref int y1, ref int x2, ref int y2)
        {
            // vertical lines:
            clip_vertical:
            if (x1 == x2)
            {
                sort(ref y1, ref y2);
                if (x1 < 0 || x1 >= rect.w) return false;
                if (y1 < 0) y1 = 0;
                if (y2 >= rect.h) y2 = rect.h;
                return true;
            }

            // horizontal lines:
            clip_horizontal:
            if (y1 == y2)
            {
                sort(ref x1, ref x2);
                if (y1 < 0 || y1 >= rect.h) return false;
                if (x1 < 0) x1 = 0;
                if (x2 > rect.w) x2 = rect.w;
                return true;
            }

            // general lines:

            // sort left to right:
            if (x1 > x2)
            {
                swap(ref x1, ref x2);
                swap(ref y1, ref y2);
            }

            double m = (double)(y2 - y1) / (double)(x2 - x1);
            double b = (double)y1 - (m * x1);

            // clip:
            if (x1 < 0) { x1 = 0; y1 = (int)b; }
            if (x1 >= rect.w) return false;
            if (x2 < 0) return false;
            if (x2 > rect.w - 1) { x2 = rect.w - 1; y2 = (int)(m * x2 + b); }

            if (y1 < 0 && y2 < 0) return false;
            if (y1 >= rect.h && y2 >= rect.h) return false;

            if (y1 < 0) { y1 = 0; x1 = (int)(-b / m); }
            if (y1 >= rect.h) { y1 = rect.h - 1; x1 = (int)((y1 - b) / m); }
            if (y2 < 0) { y2 = 0; x2 = (int)(-b / m); }
            if (y2 >= rect.h) { y2 = rect.h - 1; x2 = (int)((y2 - b) / m); }

            if (x1 == x2)
                goto clip_vertical;

            if (y1 == y2)
                goto clip_horizontal;

            return true;
        }

        public bool ClipLine(ref double x1, ref double y1, ref double x2, ref double y2)
        {
            // vertical lines:
            clip_vertical:
            if (x1 == x2)
            {
                sort(ref y1, ref y2);
                if (x1 < 0 || x1 >= rect.w) return false;
                if (y1 < 0) y1 = 0;
                if (y2 >= rect.h) y2 = rect.h;
                return true;
            }

            // horizontal lines:
            clip_horizontal:
            if (y1 == y2)
            {
                sort(ref x1, ref x2);
                if (y1 < 0 || y1 >= rect.h) return false;
                if (x1 < 0) x1 = 0;
                if (x2 > rect.w) x2 = rect.w;
                return true;
            }

            // general lines:

            // sort left to right:
            if (x1 > x2)
            {
                swap(ref x1, ref x2);
                swap(ref y1, ref y2);
            }

            double m = (double)(y2 - y1) / (double)(x2 - x1);
            double b = (double)y1 - (m * x1);

            // clip:
            if (x1 < 0) { x1 = 0; y1 = b; }
            if (x1 >= rect.w) return false;
            if (x2 < 0) return false;
            if (x2 > rect.w - 1) { x2 = rect.w - 1; y2 = (m * x2 + b); }

            if (y1 < 0 && y2 < 0) return false;
            if (y1 >= rect.h && y2 >= rect.h) return false;

            if (y1 < 0) { y1 = 0; x1 = (-b / m); }
            if (y1 >= rect.h) { y1 = rect.h - 1; x1 = ((y1 - b) / m); }
            if (y2 < 0) { y2 = 0; x2 = (-b / m); }
            if (y2 >= rect.h) { y2 = rect.h - 1; x2 = ((y2 - b) / m); }

            if (x1 == x2)
                goto clip_vertical;

            if (y1 == y2)
                goto clip_horizontal;

            return true;
        }

        public void DrawLine(int x1, int y1, int x2, int y2, Color color, Video.BLEND_TYPE blend = 0)
        {
            if (ClipLine(ref x1, ref y1, ref x2, ref y2))
            {
                Vector2[] points = new Vector2[2];
                points[0] = new Vector2(x1, y1);
                points[1] = new Vector2(x2, y2);

                DrawLineObj(points, color, false);
            }
        }
        public void DrawLineObj(Vector2[] pts, Color color, bool isLineList = false)
        {
            GameObject lineObj = new GameObject();
            RectTransform rect = lineObj.AddComponent<RectTransform>();
            rect.SetParent(windowObj.transform);
            rect.anchoredPosition = Vector2.zero;
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.zero;
            //rect.sizeDelta = new Vector2(x1 + y1, x2 + y2);
            rect.pivot = Vector2.zero;
            lineObj.name = string.Format("Line{0:X2}", numfig++);
            var line = lineObj.AddComponent<UILineRenderer>();
            line.LineThickness = 1;
            line.Points = pts;
            line.lineList = isLineList;
            line.color = color;
        }

        public void DrawRect(int x1, int y1, int x2, int y2, Color color, Video.BLEND_TYPE blend = 0)
        {
            sort(ref x1, ref x2);
            sort(ref y1, ref y2);

            if (x1 > rect.w || x2 < 0 || y1 > rect.h || y2 < 0)
                return;

            Vector2[] points = new Vector2[5];
            int pos = 0;
            points[pos++] = new Vector2(x1, y1);
            points[pos++] = new Vector2(x2, y1);
            points[pos++] = new Vector2(x2, y2);
            points[pos++] = new Vector2(x1, y2);
            points[pos++] = new Vector2(x1, y1);

            DrawLineObj(points, color, false);
        }
        public void DrawRect(Rect r, Color color, Video.BLEND_TYPE blend = 0)
        {
            DrawRect(r.x, r.y, r.x + r.w, r.y + r.h, color, blend);
        }
        public void FillRectObj(Rect r, Color color)
        {
            GameObject imageObj = new GameObject();
            RectTransform rect = imageObj.AddComponent<RectTransform>();
            rect.SetParent(windowObj.transform);
            rect.anchoredPosition = new Vector2(r.x, r.y);
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.zero;
            rect.sizeDelta = new Vector2(r.w, r.h);
            rect.pivot = Vector2.zero;
            imageObj.name = string.Format("FillRect{0:X2}", numfig++);
            var rectangle = imageObj.AddComponent<Image>();
            rectangle.color = color;
        }
        public void FillRect(int x1, int y1, int x2, int y2, Color color, Video.BLEND_TYPE blend = 0)
        {
            sort(ref x1, ref x2);
            sort(ref y1, ref y2);

            if (x1 > rect.w || x2 < 0 || y1 > rect.h || y2 < 0)
                return;

            Rect rectf = new Rect(x1, y1, x2 - x1, y2 - y1);
            FillRectObj(rectf, color);
        }
        public void FillRect(Rect r, Color color, Video.BLEND_TYPE alpha = 0)
        {
            FillRectObj(r, color);
        }
        public void BitmapObj(int x1, int y1, int x2, int y2, Bitmap img, Color c, UnityEngine.Rect clip)
        {
            GameObject imageObj = new GameObject();
            RectTransform rect = imageObj.AddComponent<RectTransform>();
            rect.SetParent(windowObj.transform);
            rect.anchoredPosition = new Vector2(x1, y1);
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.zero;
            rect.sizeDelta = new Vector2(x2 - x1, y2 - y1);
            rect.pivot = Vector2.zero;
            imageObj.name = string.Format("BitmapObj{0:X2}", numfig++);
            var image = imageObj.AddComponent<RawImage>();
            image.texture = img.Texture;
            image.color = c;
            image.uvRect = clip;
        }

        public void DrawBitmap(int x1, int y1, int x2, int y2, Bitmap img, Video.BLEND_TYPE blend = 0)
        {
            Rect clip_rect = new Rect(0, 0, rect.w, rect.h);

            ClipBitmap(x1, y1, x2, y2, img, Color.white, blend, clip_rect);
        }
        public void FadeBitmap(int x1, int y1, int x2, int y2, Bitmap img, Color c, Video.BLEND_TYPE blend = 0)
        {
            Rect clip_rect = new Rect(0, 0, rect.w, rect.h);

            ClipBitmap(x1, y1, x2, y2, img, c, blend, clip_rect);
        }
        public void ClipBitmap(int x1, int y1, int x2, int y2, Bitmap img, Color c, Video.BLEND_TYPE blend, Rect clip_rect)
        {
            Rect clip = clip_rect;

            // clip the clip rect to the window rect:
            if (clip.x < 0)
            {
                clip.w -= clip.x;
                clip.x = 0;
            }

            if (clip.x + clip.w > rect.w)
            {
                clip.w -= (clip.x + clip.w - rect.w);
            }

            if (clip.y < 0)
            {
                clip.h -= clip.y;
                clip.y = 0;
            }

            if (clip.y + clip.h > rect.h)
            {
                clip.h -= (clip.y + clip.h - rect.h);
            }

            // now clip the bitmap to the validated clip rect:
            sort(ref x1, ref x2);
            sort(ref y1, ref y2);

            if (x1 > clip.x + clip.w || x2 < clip.x || y1 > clip.y + clip.h || y2 < clip.y)
                return;

            float u1 = 0.0f;
            float u2 = 1.0f;
            float v1 = 0.0f;
            float v2 = 1.0f;
            float iw = (float)(x2 - x1);
            float ih = (float)(y2 - y1);
            int x3 = clip.x + clip.w;
            int y3 = clip.y + clip.h;

            if (x1 < clip.x)
            {
                u1 = (clip.x - x1) / iw;
                x1 = clip.x;
            }

            if (x2 > x3)
            {
                u2 = 1.0f - (x2 - x3) / iw;
                x2 = x3;
            }

            if (y1 < clip.y)
            {
                v1 = (clip.y - y1) / ih;
                y1 = clip.y;
            }

            if (y2 > y3)
            {
                v2 = 1.0f - (y2 - y3) / ih;
                y2 = y3;
            }
            UnityEngine.Rect uvclip = new UnityEngine.Rect(u1, v1, u2, v2);
            BitmapObj(x1, y1, x2, y2, img, c, uvclip);
        }
        public void TileBitmap(int x1, int y1, int x2, int y2, Bitmap img, Video.BLEND_TYPE blend = 0) { throw new NotImplementedException(); }
        public void DrawLines(int nPts, POINT[] pts, Color color, Video.BLEND_TYPE blend = 0)
        {
            if (nPts < 2 || nPts > 16)
                return;

            Vector2[] f = new Vector2[nPts];
            int n = 0;

            for (int i = 0; i < nPts - 1; i++)
            {
                //f[n++] = (float)rect.x + pts[i].x;
                //f[n++] = (float)rect.y + pts[i].y;
                //f[n++] = (float)rect.x + pts[i + 1].x;
                //f[n++] = (float)rect.y + pts[i + 1].y;
                f[n++] = new Vector2(rect.x + pts[i].x, rect.y + pts[i].y);
                f[n++] = new Vector2(rect.x + pts[i + 1].x, rect.y + pts[i + 1].y);
            }
            DrawLineObj(pts, color, false);
        }

        public void DrawPoly(int nPts, POINT[] pts, Color color, Video.BLEND_TYPE blend = 0)
        {
            if (nPts < 3 || nPts > 8)
                return;

            Vector2[] f = new Vector2[nPts + 2];
            int n = 0;

            for (int i = 0; i < nPts - 1; i++)
            {
                //f[n++] = (float)rect.x + pts[i].x;
                //f[n++] = (float)rect.y + pts[i].y;
                //f[n++] = (float)rect.x + pts[i + 1].x;
                //f[n++] = (float)rect.y + pts[i + 1].y;
                f[n++] = new Vector2(rect.x + pts[i].x, rect.y + pts[i].y);
                f[n++] = new Vector2(rect.x + pts[i + 1].x, rect.y + pts[i + 1].y);
            }

            //f[n++] = (float)rect.x + pts[nPts - 1].x;
            //f[n++] = (float)rect.y + pts[nPts - 1].y;
            //f[n++] = (float)rect.x + pts[0].x;
            //f[n++] = (float)rect.y + pts[0].y;
            f[n++] = new Vector2(rect.x + pts[nPts - 1].x, rect.y + pts[nPts - 1].y);
            f[n++] = new Vector2(rect.x + pts[0].x, rect.y + pts[0].y);

            DrawLineObj(pts, color, true);
        }


        public void FillPoly(int nPts, POINT[] pts, Color color, Video.BLEND_TYPE blend = 0) { throw new NotImplementedException(); }

        public void DrawEllipseObject(int x1, int y1, int x2, int y2, Color c, UIEllipse.Mode mode)
        {
            GameObject ellipseObj = new GameObject();
            RectTransform rect = ellipseObj.AddComponent<RectTransform>();
            rect.SetParent(windowObj.transform);
            rect.anchoredPosition = new Vector2(x1, y1);
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.zero;
            rect.sizeDelta = new Vector2(x2 - x1, y2 - y1);
            rect.pivot = Vector2.zero;
            ellipseObj.name = string.Format("EllipseObj{0:X2}", numfig++);
            var ellipse = ellipseObj.AddComponent<UIEllipse>();
            ellipse.color = c;
            ellipse.mode = mode;
        }
        public void DrawEllipse(int x1, int y1, int x2, int y2, Color color, Video.BLEND_TYPE blend = 0)
        {
            sort(ref x1, ref x2);
            sort(ref y1, ref y2);

            if (x1 > rect.w || x2 < 0 || y1 > rect.h || y2 < 0)
                return;

            DrawEllipseObject(x1, y1, x2, y2, color, UIEllipse.Mode.Edge);
        }
        public void FillEllipse(int x1, int y1, int x2, int y2, Color color, int blend = 0)
        {
            sort(ref x1, ref x2);
            sort(ref y1, ref y2);

            if (x1 > rect.w || x2 < 0 || y1 > rect.h || y2 < 0)
                return;

            DrawEllipseObject(x1, y1, x2, y2, color, UIEllipse.Mode.FillInside);
        }

        // text methods:
        public virtual void SetFont(FontItem f) { font = f; }
        public FontItem GetFont() { return font; }

        public void DrawTextObject(int x1, int y1, string txt, FontItem font)
        {
            GameObject lineObj = new GameObject();
            RectTransform rect = lineObj.AddComponent<RectTransform>();
            rect.SetParent(windowObj.transform);
            rect.anchoredPosition = new Vector2(x1, y1);
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.zero;
            //rect.sizeDelta = new Vector2(x2 - x1, y2 - y1);
            rect.pivot = Vector2.zero;
            lineObj.name = string.Format("EllipseObj{0:X2}", numfig++);
            var text = lineObj.AddComponent<Text>();
            text.color = font.color;
            text.fontSize = font.size;
            text.font = font.font;
            text.text = txt;
            var sizeFitter = lineObj.AddComponent<ContentSizeFitter>();
            sizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
            sizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        }


        public void Print(int x1, int y1, string fmt, params object[] args)
        {
            if (font == null || x1 < 0 || y1 < 0 || x1 >= rect.w || y1 >= rect.h)
                return;

            string msgbuf = string.Format(fmt, args);
            DrawTextObject(x1, y1, msgbuf, font);
        }

        public void DrawText(string txt, int count, ref Rect txt_rect, TextFormat flags)
        {
            if (font == null)
                return;

            if (!string.IsNullOrEmpty(txt) && count == 0)
                count = txt.Length;

            // clip the rect:
            Rect clip_rect = txt_rect;

            if (clip_rect.x < 0)
            {
                int dx = -clip_rect.x;
                clip_rect.x += dx;
                clip_rect.w -= dx;
            }

            if (clip_rect.y < 0)
            {
                int dy = -clip_rect.y;
                clip_rect.y += dy;
                clip_rect.h -= dy;
            }

            if (clip_rect.w < 1 || clip_rect.h < 1)
                return;

            if (clip_rect.x + clip_rect.w > rect.w)
                clip_rect.w = rect.w - clip_rect.x;

            if (clip_rect.y + clip_rect.h > rect.h)
                clip_rect.h = rect.h - clip_rect.y;

            clip_rect.x += rect.x;
            clip_rect.y += rect.y;

            if (font != null && !string.IsNullOrEmpty(txt) && count != 0)
            {
                font.DrawText(txt, count, clip_rect, flags);
                font.SetAlpha(1);
            }

            // if calc only, update the rectangle:
            if ((flags & TextFormat.DT_CALCRECT) != 0)
            {
                txt_rect.h = clip_rect.h;
                txt_rect.w = clip_rect.w;
            }
        }

        public Canvas Canvas { get { return canvas; } }
        public GameObject WindowObj { get { return windowObj; } }

        protected Rect rect;
        protected Screen screen;
        protected bool shown;
        protected FontItem font;
        protected GameObject windowObj;
        protected Canvas canvas;
        protected RectTransform rectTransform;

        private int numfig = 0;
        protected List<IView> view_list = new List<IView>();
        private static float[] ellipse_pts = new float[256];

    }
}
