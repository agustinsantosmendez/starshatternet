﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      DebriefDlg.h/DebriefDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Debriefing Dialog Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class DebriefDlg : FormWindow
    {
        public DebriefDlg(Screen s, Camera c, FormDef def, PlanScreen mgr, GameObject p) :
               base(s, 0, 0, s.Width(), s.Height(), c, p, "DebriefDlg")
        {
            manager = mgr;
            close_btn = null; campaign = null; mission = null;
            unit_index = 0; info = null; sim = null; ship = null;

            campaign = Campaign.GetCampaign();

            if (campaign != null)
                mission = campaign.GetMission();

            Init(def);
        }
        //public virtual ~DebriefDlg();
        public override void RegisterControls()
        {
            mission_name = FindControl(200);
            mission_system = FindControl(202);
            mission_sector = FindControl(204);
            mission_time_start = FindControl(206);

            objectives = FindControl(210);
            situation = FindControl(240);
            mission_score = FindControl(211);
            unit_list = (ListBox)FindControl(320);
            summary_list = (ListBox)FindControl(330);
            event_list = (ListBox)FindControl(340);

            if (unit_list != null)
                REGISTER_CLIENT(ETD.EID_SELECT, unit_list, OnUnit);

            close_btn = (Button)FindControl(1);

            if (close_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, close_btn, OnClose);
        }
        public override void Show()
        {
            base.Show();
            Game.SetTimeCompression(1);

            mission = null;
            campaign = Campaign.GetCampaign();
            sim = Sim.GetSim();

            if (sim != null)
                ship = sim.GetPlayerShip();

            if (campaign != null)
                mission = campaign.GetMission();

            if (mission_name != null)
            {
                if (mission != null)
                    mission_name.SetText(mission.Name());
                else
                    mission_name.SetText(Game.GetText("DebriefDlg.mission-name"));
            }

            if (mission_system != null)
            {
                mission_system.SetText("");

                if (mission != null)
                {
                    StarSystem sys = mission.GetStarSystem();

                    if (sys != null)
                        mission_system.SetText(sys.Name());
                }
            }

            if (mission_sector != null)
            {
                mission_sector.SetText("");

                if (mission != null)
                {
                    MissionElement elem = mission.GetElements()[0];

                    if (elem != null)
                        mission_sector.SetText(elem.Region());
                }
            }

            if (mission_time_start != null)
            {
                if (mission != null)
                {
                    string txt;
                    FormatUtil.FormatDayTime(out txt, mission.Start());
                    mission_time_start.SetText(txt);
                }
            }

            if (objectives != null)
            {
                bool found_objectives = false;

                if (sim != null && sim.GetPlayerElement() != null)
                {
                    string text = "";
                    Element elem = sim.GetPlayerElement();

                    for (int i = 0; i < elem.NumObjectives(); i++)
                    {
                        Instruction obj = elem.GetObjective(i);
                        text += "* " + obj.GetDescription() + "\n";

                        found_objectives = true;
                    }

                    objectives.SetText(text);
                }

                if (!found_objectives)
                {
                    if (mission != null)
                        objectives.SetText(mission.Objective());
                    else
                        objectives.SetText(Game.GetText("DebriefDlg.unspecified"));
                }
            }

            if (situation != null)
            {
                if (mission != null)
                    situation.SetText(mission.Situation());
                else
                    situation.SetText(Game.GetText("DebriefDlg.unknown"));
            }

            if (mission_score != null)
            {
                mission_score.SetText(Game.GetText("DebriefDlg.no-stats"));

                if (ship != null)
                {
                    for (int i = 0; i < ShipStats.NumStats(); i++)
                    {
                        ShipStats stats = ShipStats.GetStats(i);
                        if (stats != null && ship.Name() == stats.GetName())
                        {
                            stats.Summarize();

                            Player player = Player.GetCurrentPlayer();
                            int points = stats.GetPoints() + stats.GetCommandPoints();

                            if (player != null && sim != null)
                                points = player.GetMissionPoints(stats, sim.StartTime()) + stats.GetCommandPoints();

                            string score;
                            score = string.Format("{0} {1}", points, Game.GetText("DebriefDlg.points"));
                            mission_score.SetText(score);
                            break;
                        }
                    }
                }
            }

            DrawUnits();
        }
        public virtual void ExecFrame()
        {
            if (unit_list != null && unit_list.NumItems() != 0 && unit_list.GetSelCount() < 1)
            {
                unit_list.SetSelected(0);
                OnUnit(null, null);
            }

            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnClose(null, null);
            }
        }


        // Operations:
        public virtual void OnClose(ActiveWindow obj, AWEvent evnt)
        {
            Sim sim = Sim.GetSim();

            sim.CommitMission();
            sim.UnloadMission();

            NetLobby lobby = NetLobby.GetInstance();
            if (lobby != null && lobby.IsHost())
            {
                lobby.SelectMission(0);
                lobby.ExecFrame();
            }

            Player player = Player.GetCurrentPlayer();
            if (player != null && player.ShowAward())
            {
                manager.ShowAwardDlg();
            }

            else
            {
                Starshatter stars = Starshatter.GetInstance();

                if (stars != null)
                {
                    Mouse.Show(false);

                    Campaign campaign = Campaign.GetCampaign();
                    if (campaign != null && campaign.GetCampaignId() < (int)Campaign.CONSTANTS.SINGLE_MISSIONS)
                        stars.SetGameMode(Starshatter.MODE.CMPN_MODE);
                    else
                        stars.SetGameMode(Starshatter.MODE.MENU_MODE);
                }

                else
                {
                    ErrLogger.PrintLine("PANIC: DebriefDlg.OnClose() - Game instance not found");
                }
            }
        }
        public virtual void OnUnit(ActiveWindow obj, AWEvent evnt)
        {
            if (unit_list == null || event_list == null || summary_list == null)
                return;

            summary_list.ClearItems();
            event_list.ClearItems();

            int seln = unit_list.GetSelection();
            int unit = (int)unit_list.GetItemData(seln);

            ShipStats stats = ShipStats.GetStats(unit);
            if (stats != null)
            {
                stats.Summarize();

                string txt;
                int i = 0;

                txt = string.Format("{0}", stats.GetGunShots());
                summary_list.AddItem("Guns Fired: ");
                summary_list.SetItemText(i++, 1, txt);

                txt = string.Format("{0}", stats.GetGunHits());
                summary_list.AddItem("Gun Hits: ");
                summary_list.SetItemText(i++, 1, txt);

                txt = string.Format("{0}", stats.GetGunKills());
                summary_list.AddItem("Gun Kills: ");
                summary_list.SetItemText(i++, 1, txt);

                // one line spacer:
                summary_list.AddItem(" ");
                i++;

                txt = string.Format("{0}", stats.GetMissileShots());
                summary_list.AddItem("Missiles Fired: ");
                summary_list.SetItemText(i++, 1, txt);

                txt = string.Format("{0}", stats.GetMissileHits());
                summary_list.AddItem("Missile Hits: ");
                summary_list.SetItemText(i++, 1, txt);

                txt = string.Format("{0}", stats.GetMissileKills());
                summary_list.AddItem("Missile Kills: ");
                summary_list.SetItemText(i++, 1, txt);

                i = 0;
                foreach (SimEvent simevent in stats.GetEvents())
                {

                    string txt2;
                    int time = simevent.GetTime();

                    if (time > 24 * 60 * 60)
                        FormatUtil.FormatDayTime(out txt2, time);
                    else
                        FormatUtil.FormatTime(out txt2, time);

                    event_list.AddItem(txt2);
                    event_list.SetItemText(i, 1, simevent.GetEventDesc());

                    if (simevent.GetTarget() != null)
                        event_list.SetItemText(i, 2, simevent.GetTarget());

                    i++;
                }
            }
        }


        protected virtual void DrawUnits()
        {
            if (mission != null)
            {
                if (unit_list != null)
                {
                    unit_list.ClearItems();

                    int seln = -1;
                    bool netgame = false;

                    if (sim != null && sim.IsNetGame())
                        netgame = true;

                    for (int i = 0; i < ShipStats.NumStats(); i++)
                    {
                        ShipStats stats = ShipStats.GetStats(i);
                        stats.Summarize();

                        if (netgame != null || (stats.GetIFF() == mission.Team() &&
                                     stats.GetRegion() == mission.GetRegion()))
                        {
                            int n = unit_list.AddItemWithData(" ", i) - 1;
                            unit_list.SetItemText(n, 1, stats.GetName());
                            unit_list.SetItemText(n, 2, stats.GetRole());
                            unit_list.SetItemText(n, 3, stats.GetStatType());

                            if (ship != null && ship.Name() == stats.GetName())
                                seln = n;
                        }
                    }

                    if (seln >= 0)
                    {
                        unit_list.SetSelected(seln);
                        OnUnit(null, null);
                    }
                }
            }
        }

        protected PlanScreen manager;
        protected Button close_btn;

        protected ActiveWindow mission_name;
        protected ActiveWindow mission_system;
        protected ActiveWindow mission_sector;
        protected ActiveWindow mission_time_start;

        protected ActiveWindow objectives;
        protected ActiveWindow situation;
        protected ActiveWindow mission_score;

        protected ListBox unit_list;
        protected ListBox summary_list;
        protected ListBox event_list;

        protected Campaign campaign;
        protected Mission mission;
        protected MissionInfo info;
        protected int unit_index;

        protected Sim sim;
        protected Ship ship;
    }
}