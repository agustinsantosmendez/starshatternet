﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetClientConfig.h/NetClientConfig.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Config
{
    public class NetClientConfig
    {
        public NetClientConfig()
        {
            ErrLogger.PrintLine("NetClientConfig NetClientConfig() is not yet implemented");
        }


        public void AddServer(string name, string addr, int port, string password, bool save = false)
        {
            ErrLogger.PrintLine("NetClientConfig AddServer() is not yet implemented");
        }

        public void DelServer(int index)
        {
            ErrLogger.PrintLine("NetClientConfig DelServer(int index) is not yet implemented");
        }

        public List<NetServerInfo> GetServerList() { return servers; }
        public NetServerInfo GetServerInfo(int n)
        {
            ErrLogger.PrintLine("NetClientConfig GetServerInfo(int n) is not yet implemented");
            throw new NotImplementedException();
        }

        public void Download()
        {
            ErrLogger.PrintLine("NetClientConfig  Download() is not yet implemented");
        }

        public void Load()
        {
            ErrLogger.PrintLine("NetClientConfig Load() is not yet implemented");
        }

        public void Save()
        {
            ErrLogger.PrintLine("NetClientConfig Save() is not yet implemented");
        }


        public void SetServerIndex(int n) { server_index = n; }
        public int GetServerIndex() { return server_index; }
        public void SetHostRequest(bool n) { host_request = n; }
        public bool GetHostRequest() { return host_request; }
        public NetServerInfo GetSelectedServer()
        {
            ErrLogger.PrintLine("NetClientConfig GetSelectedServer() is not yet implemented");
            throw new NotImplementedException();
        }


        public void CreateConnection()
        {
            ErrLogger.PrintLine("NetClientConfig GetSelectedServer() is not yet implemented");
        }

#if TODO
        public NetLobbyClient GetConnection()
        {
            ErrLogger.PrintLine("NetClientConfig GetConnection() is not yet implemented");
            throw new NotImplementedException();
        }
#endif
        public bool Login()
        {
            ErrLogger.PrintLine("NetClientConfig Login() is not yet implemented");
            throw new NotImplementedException();
        }

        public bool Logout()
        {
            ErrLogger.PrintLine("NetClientConfig Logout() is not yet implemented");
            throw new NotImplementedException();
        }

        public void DropConnection()
        {
            ErrLogger.PrintLine("NetClientConfig DropConnection() is not yet implemented");
        }


        public static void Initialize()
        {
            ErrLogger.PrintLine("NetClientConfig Initialize() is not yet implemented");
        }

        public static void Close()
        {
            ErrLogger.PrintLine("NetClientConfig Close() is not yet implemented");
        }

        public static NetClientConfig GetInstance() { return instance; }


        private List<NetServerInfo> servers;
        private int server_index;
        private bool host_request;

#if TODO
        private NetLobbyClient conn;
#endif

        private static NetClientConfig instance;
    }
}
