﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars.exe
    ORIGINAL FILE:      Asteroid.h/Asteroid.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Asteroid Sprite class
*/
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using System;
using System.Collections.Generic;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars
{
#warning Asteroid class is still in development and is not recommended for production.
    public class Asteroid : Debris
    {
        public Asteroid(int type, Point pos, double mass) :
            base(asteroid_model[type % 6], pos, asteroid_velocity, mass)
        {
            life = -1;
        }


        public static void Initialize()
        {
            asteroid_model.Initialize();

            int n = 0;
            string path = "Galaxy/Asteroids/";
            Model a = new Model();
            a.Load(path+"a1", 100);
            asteroid_model[n++] = a;

            a = new Model();
            a.Load(path + "a2", 50);
            asteroid_model[n++] = a;

            a = new Model();
            a.Load(path + "a1", 8);
            asteroid_model[n++] = a;

            a = new Model();
            a.Load(path + "a2", 10);
            asteroid_model[n++] = a;

            a = new Model();
            a.Load(path + "a3", 30);
            asteroid_model[n++] = a;

            a = new Model();
            a.Load(path + "a4", 20);
            asteroid_model[n++] = a;

            List<string> mod_asteroids = new List<string>();
#if TODO
            loader.SetDataPath("Mods/Galaxy/Asteroids/");
            loader.ListFiles("*.mag", mod_asteroids);

            ListIter<string> iter = mod_asteroids;
            while (++iter && n < 32)
            {
                a = new Model();
                a.Load( iter , 50);
                    asteroid_model[n++] = a;
             }

            loader.SetDataPath(old_path);
#endif
         }
        public static void Close()
        {
            for (int i = 0; i < 32; i++)
            {
                //delete asteroid_model[i];
                asteroid_model[i] = null;
            }
            //ZeroMemory(asteroid_model, sizeof(asteroid_model));
        }

        private static Point asteroid_velocity = new Point(0, 0, 0);
        private static Model[] asteroid_model = new Model[32];


    }
}