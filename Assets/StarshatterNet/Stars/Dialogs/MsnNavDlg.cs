﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MsnNavDlg.h/MsnNavDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Briefing Dialog Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MsnNavDlg : NavDlg //, MsnDlg
    {
        public MsnNavDlg(Screen s, Camera c, FormDef def, PlanScreen mgr, GameObject p) :
            base(s, c, def, null, p)
        {
            msnDlg = new MsnDlg(mgr);

            RegisterControls();
            Show();
        }
        //public virtual ~MsnNavDlg();

        public override void RegisterControls()
        {
            if (msnDlg == null) return;

            base.RegisterControls();
            msnDlg.RegisterMsnControls(this);

            if (msnDlg.commit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.commit, OnCommit);

            if (msnDlg.cancel != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.cancel, OnCancel);

            if (msnDlg.sit_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.sit_button, msnDlg.OnTabButton);

            if (msnDlg.pkg_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.pkg_button, msnDlg.OnTabButton);

            if (msnDlg.nav_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.nav_button, msnDlg.OnTabButton);

            if (msnDlg.wep_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.wep_button, msnDlg.OnTabButton);
        }
        public override void Show()
        {
            if (msnDlg == null) return;

            base.Show();
            msnDlg.ShowMsnDlg();
        }
        public override void ExecFrame()
        {
            base.ExecFrame();

            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnCommit(null, null);
            }
        }

        // Operations:
        public override void OnCommit(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnCommit(obj, evnt);
            base.OnCommit(obj, evnt);
        }
        public override void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnCancel(obj, evnt);
            base.OnCancel(obj, evnt);
        }


        protected MsnDlg msnDlg;
    }
}
