﻿using DigitalRune.Mathematics.Algebra;
using System;

namespace StarshatterNet.Stars.Graphics.General
{
    public static class Matrix33Extensions
    {
        public static void Aim(this Matrix33F cam, double roll, double pitch, double yaw) { throw new NotImplementedException(); }
        public static void Roll(this Matrix33F m, double roll)
        {
            double s = Math.Sin(roll);
            double c = Math.Cos(roll);

            double e00 = m.M00;
            double e01 = m.M01;
            double e02 = m.M02;
            double e10 = m.M10;
            double e11 = m.M11;
            double e12 = m.M12;

            m.M00 = (float)(c * e00 + s * e10);
            m.M01 = (float)(c * e01 + s * e11);
            m.M02 = (float)(c * e02 + s * e12);

            m.M10 = (float)(-s * e00 + c * e10);
            m.M11 = (float)(-s * e01 + c * e11);
            m.M12 = (float)(-s * e02 + c * e12);
        }
        public static void Pitch(this Matrix33F cam, double pitch) { throw new NotImplementedException(); }
        public static void Yaw(this Matrix33F cam, double yaw) { throw new NotImplementedException(); }
        public static void ComputeEulerAngles(this Matrix33F m, out double roll, out double pitch, out double yaw)
        {
            double cy;

            yaw = Math.Asin(-m.M02);
            cy = Math.Cos(yaw);
            roll = Math.Asin(m.M01 / cy);
            pitch = Math.Asin(m.M12 / cy);

            if (Math.Sign(Math.Cos(roll) * cy) != Math.Sign(m.M00))
                roll = Math.PI - roll;

            if (Math.Sign(Math.Cos(pitch) * cy) != Math.Sign(m.M22))
                pitch = Math.PI - pitch;
        }

        public static void Aim(this Matrix33D cam, double roll, double pitch, double yaw) { throw new NotImplementedException(); }
        public static void Roll(this Matrix33D m, double roll)
        {
            double s = Math.Sin(roll);
            double c = Math.Cos(roll);

            double e00 = m.M00;
            double e01 = m.M01;
            double e02 = m.M02;
            double e10 = m.M10;
            double e11 = m.M11;
            double e12 = m.M12;

            m.M00 = c * e00 + s * e10;
            m.M01 = c * e01 + s * e11;
            m.M02 = c * e02 + s * e12;

            m.M10 = -s * e00 + c * e10;
            m.M11 = -s * e01 + c * e11;
            m.M12 = -s * e02 + c * e12;
        }
        public static void Pitch(this Matrix33D m, double pitch)
        {
            double s = Math.Sin(pitch);
            double c = Math.Cos(pitch);

            double e10 = m.M10;
            double e11 = m.M11;
            double e12 = m.M12;
            double e20 = m.M20;
            double e21 = m.M21;
            double e22 = m.M22;

            m.M10 = c * e10 + s * e20;
            m.M11 = c * e11 + s * e21;
            m.M12 = c * e12 + s * e22;

            m.M20 = -s * e10 + c * e20;
            m.M21 = -s * e11 + c * e21;
            m.M22 = -s * e12 + c * e22;
        }
        public static void Yaw(this Matrix33D m, double yaw)
        {
            double s = Math.Sin(yaw);
            double c = Math.Cos(yaw);

            double e00 = m.M00;
            double e01 = m.M01;
            double e02 = m.M02;
            double e20 = m.M20;
            double e21 = m.M21;
            double e22 = m.M22;

            m.M00 = c * e00 - s * e20;
            m.M01 = c * e01 - s * e21;
            m.M02 = c * e02 - s * e22;

            m.M20 = s * e00 + c * e20;
            m.M21 = s * e01 + c * e21;
            m.M22 = s * e02 + c * e22;
        }
        public static void Rotate(this Matrix33D m, double roll, double pitch, double yaw)
        {
            Matrix33D e = m;

            double sr = Math.Sin(roll);
            double cr = Math.Cos(roll);
            double sp = Math.Sin(pitch);
            double cp = Math.Cos(pitch);
            double sy = Math.Sin(yaw);
            double cy = Math.Cos(yaw);

            double a, b, c;

            a = cy * cr;
            b = cy * sr;
            c = -sy;

            m.M00 = a * e.M00 + b * e.M10 + c * e.M20;
            m.M01 = a * e.M01 + b * e.M11 + c * e.M21;
            m.M02 = a * e.M02 + b * e.M12 + c * e.M22;

            a = cp * -sr + sp * sy * cr;
            b = cp * cr + sp * sy * sr;
            c = sp * cy;

            m.M10 = a * e.M00 + b * e.M10 + c * e.M20;
            m.M11 = a * e.M01 + b * e.M11 + c * e.M21;
            m.M12 = a * e.M02 + b * e.M12 + c * e.M22;

            a = -sp * -sr + cp * sy * cr;
            b = -sp * cr + cp * sy * sr;
            c = cp * cy;

            m.M20 = a * e.M00 + b * e.M10 + c * e.M20;
            m.M21 = a * e.M01 + b * e.M11 + c * e.M21;
            m.M22 = a * e.M02 + b * e.M12 + c * e.M22;
        }
        public static void ComputeEulerAngles(this Matrix33D m, out double roll, out double pitch, out double yaw)
        {
            double cy;

            yaw = Math.Asin(-m.M02);
            cy = Math.Cos(yaw);
            roll = Math.Asin(m.M01 / cy);
            pitch = Math.Asin(m.M12 / cy);

            if (Math.Sign(Math.Cos(roll) * cy) != Math.Sign(m.M00))
                roll = Math.PI - roll;

            if (Math.Sign(Math.Cos(pitch) * cy) != Math.Sign(m.M22))
                pitch = Math.PI - pitch;
        }
    }
}