﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CampaignPlanMission.h/CampaignPlanMission.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CampaignPlanMission generates missions and mission
    info for the player's combat group as part of a
    dynamic campaign.
*/

using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.StarSystems;

namespace StarshatterNet.Stars.MissionCampaign.Planning
{
#warning CampaignPlanMission class is still in development and is not recommended for production.
    public class CampaignPlanMission : CampaignPlan
    {
        public CampaignPlanMission(Campaign c) : base(c) { }
        // ~CampaignPlanMission() { }

        // operations:
        public override void ExecFrame()
        {
            if (campaign != null && campaign.IsActive())
            {
                player_group = campaign.GetPlayerGroup();
                if (player_group == null) return;

                int missionCount = campaign.GetMissionList().Count;

                if (missionCount > 0)
                {
                    // starships only get one mission to pick from:
                    if (player_group.IsStarshipGroup())
                        return;

                    // fighters get a maximum of five missions:
                    if (missionCount >= 5)
                        return;

                    // otherwise, once every few seconds is plenty:
                    if (Campaign.Stardate() - exec_time < 1)
                        return;
                }

                SelectStartTime();

                if (player_group.IsFighterGroup())
                {
                    slot++;
                    if (slot > 2) slot = 0;

                    CampaignMissionRequest request = PlanFighterMission();
                    CampaignMissionFighter generator = new CampaignMissionFighter(campaign);
                    generator.CreateMission(request);
                    //delete request;
                }

                else if (player_group.IsStarshipGroup())
                {
                    // starships should always check for campaign and strategic missions
                    slot = 0;

                    CampaignMissionRequest request = PlanStarshipMission();
                    CampaignMissionStarship generator = new CampaignMissionStarship(campaign);
                    generator.CreateMission(request);
                    //delete request;
                }

                exec_time = Campaign.Stardate();
            }
        }


        protected virtual void SelectStartTime()
        {
            const int HOUR = 3600;  // 60 minutes
            const int MISSION_DELAY = 1800;  // 30 minutes
            double base_time = 0;

            List<MissionInfo> info_list = campaign.GetMissionList();

            if (info_list.Count > 0)
            {
                MissionInfo info = info_list[info_list.Count - 1];
                base_time = info.start;
            }

            if (base_time == 0)
                base_time = campaign.GetTime() + MISSION_DELAY;

            start = (int)base_time + MISSION_DELAY;
            start -= start % MISSION_DELAY;
        }

        protected virtual CampaignMissionRequest PlanCampaignMission()
        {
            CampaignMissionRequest request = null;

            foreach (CombatAction action in campaign.GetActions())
            {
                if (request != null) break;
                if (action.Type() != CombatAction.TYPE.MISSION_TEMPLATE)
                    continue;

                if (action.IsAvailable())
                {

                    // only fire each action once every two hours:
                    if (action.ExecTime() > 0 && campaign.GetTime() - action.ExecTime() < 7200)
                        continue;

                    CombatGroup g = campaign.FindGroup(action.GetIFF(),
                    (CombatGroup.GROUP_TYPE)action.AssetType(),
                    action.AssetId());

                    if (g != null && (g == player_group ||
                                (player_group.Type() == CombatGroup.GROUP_TYPE.WING &&
                                    player_group.FindGroup(g.Type(), g.GetID()) != null)))
                    {

                        request = new CampaignMissionRequest(campaign,
                                                            (Mission.TYPE)action.Subtype(),
                                                            start,
                                                            g);

                        if (request != null)
                        {
                            request.SetOpposingType(action.OpposingType());
                            request.SetScript(action.GetText());
                        }

                        action.FireAction();
                    }
                }
            }

            return request;
        }

        static int assignment_index = 0;
        protected virtual CampaignMissionRequest PlanStrategicMission()
        {
            CampaignMissionRequest request = null;

            if (slot > 1)
                return request;

            // build list of assignments:
            List<CombatAssignment> assignments = new List<CombatAssignment>();
            assignments.AddRange(player_group.GetAssignments());

            if (player_group.Type() == CombatGroup.GROUP_TYPE.WING)
            {
                foreach (CombatGroup g in player_group.GetComponents())
                {
                    assignments.AddRange(g.GetAssignments());
                }
            }

            // pick next assignment as basis for mission:
            if (assignments.Count != 0)
            {
                if (assignment_index >= assignments.Count)
                    assignment_index = 0;

                CombatAssignment a = assignments[assignment_index++];

                request = new CampaignMissionRequest(campaign,
                                                    a.Type(),
                                                    start,
                                                    a.GetResource());

                if (request != null)
                    request.SetObjective(a.GetObjective());
            }

            return request;
        }
        protected virtual CampaignMissionRequest PlanRandomStarshipMission()
        {
            Mission.TYPE type = Mission.TYPE.PATROL;
            int r = RandomHelper.RandomIndex();
            int ownside = player_group.GetIFF();

            if (mission_type_index < 0)
                mission_type_index = r;

            else if (mission_type_index >= 16)
                mission_type_index = 0;

            type = mission_types[mission_type_index++];

            if (type == Mission.TYPE.ESCORT_FREIGHT)
            {
                CombatGroup freight = campaign.FindGroup(ownside, CombatGroup.GROUP_TYPE.FREIGHT);
                if (freight == null || freight.CountUnits() < 1)
                    type = Mission.TYPE.PATROL;
            }

            CampaignMissionRequest request = null;
            request = new CampaignMissionRequest(campaign, type, start, player_group);

            return request;
        }

        protected virtual CampaignMissionRequest PlanRandomFighterMission()
        {
            CampaignMissionRequest request = null;
            Mission.TYPE type = fighter_mission_types[fighter_mission_index++];
            int ownside = player_group.GetIFF();
            CombatGroup primary = player_group;
            CombatGroup obj = null;

            if (fighter_mission_index > 15)
                fighter_mission_index = 0;

            if (type == Mission.TYPE.ESCORT_FREIGHT)
            {
                CombatGroup freight = campaign.FindGroup(ownside, CombatGroup.GROUP_TYPE.FREIGHT);
                if (freight == null || freight.CalcValue() < 1)
                    type = Mission.TYPE.PATROL;
                else
                    obj = freight;
            }

            else if (type == Mission.TYPE.ESCORT_SHUTTLE)
            {
                CombatGroup shuttle = campaign.FindGroup(ownside, CombatGroup.GROUP_TYPE.LCA_SQUADRON);
                if (shuttle == null || shuttle.CalcValue() < 1)
                    type = Mission.TYPE.PATROL;
                else
                    obj = shuttle;
            }

            else if (primary.Type() == CombatGroup.GROUP_TYPE.WING)
            {
                if (RandomHelper.RandomChance())
                    primary = primary.FindGroup(CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON);
                else
                    primary = primary.FindGroup(CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON);
            }

            if (type >= Mission.TYPE.AIR_PATROL && type <= Mission.TYPE.AIR_INTERCEPT)
            {
                CombatZone zone = null;
                bool airborne = false;

                if (primary != null)
                    zone = primary.GetAssignedZone();

                if (zone != null && zone.GetRegions().Count > 1)
                {
                    string air_region = zone.GetRegions()[1];
                    StarSystem system = campaign.GetSystem(zone.System());

                    if (system != null)
                    {
                        OrbitalRegion rgn = system.FindRegion(air_region);

                        if (rgn != null && rgn.Type() == Orbital.OrbitalType.TERRAIN)
                            airborne = true;
                    }
                }

                if (!airborne)
                {
                    if (type == Mission.TYPE.AIR_INTERCEPT)
                        type = Mission.TYPE.INTERCEPT;

                    else if (type == Mission.TYPE.AIR_SWEEP)
                        type = Mission.TYPE.SWEEP;

                    else
                        type = Mission.TYPE.PATROL;
                }
            }

            request = new CampaignMissionRequest(campaign, type, start, primary);

            if (request != null)
                request.SetObjective(obj);

            return request;
        }
        protected virtual CampaignMissionRequest PlanStarshipMission()
        {
            CampaignMissionRequest request = null;

            if (request == null) request = PlanCampaignMission();
            if (request == null) request = PlanStrategicMission();
            if (request == null) request = PlanRandomStarshipMission();

            return request;
        }

        protected virtual CampaignMissionRequest PlanFighterMission()
        {
            CampaignMissionRequest request = null;

            if (request == null) request = PlanCampaignMission();
            if (request == null) request = PlanStrategicMission();
            if (request == null) request = PlanRandomFighterMission();

            return request;
        }


        protected CombatGroup player_group;
        protected int start = 0;
        protected int slot = 0;

        static int mission_type_index = -1;
        static Mission.TYPE[] mission_types = new Mission.TYPE[16]{
                                        Mission.TYPE.PATROL,
                                        Mission.TYPE.PATROL,
                                        Mission.TYPE.ESCORT_FREIGHT,
                                        Mission.TYPE.PATROL,
                                        Mission.TYPE.ESCORT_FREIGHT,
                                        Mission.TYPE.PATROL,
                                        Mission.TYPE.ESCORT_FREIGHT,
                                        Mission.TYPE.ESCORT_FREIGHT,
                                        Mission.TYPE.PATROL,
                                        Mission.TYPE.ESCORT_FREIGHT,
                                        Mission.TYPE.PATROL,
                                        Mission.TYPE.ESCORT_FREIGHT,
                                        Mission.TYPE.PATROL,
                                        Mission.TYPE.PATROL,
                                        Mission.TYPE.ESCORT_FREIGHT,
                                        Mission.TYPE.PATROL
                                    };

        static int fighter_mission_index = 0;
        static Mission.TYPE[] fighter_mission_types = new Mission.TYPE[16]{
                            Mission.TYPE.PATROL,
                            Mission.TYPE.SWEEP,
                            Mission.TYPE.ESCORT_SHUTTLE,
                            Mission.TYPE.AIR_PATROL,
                            Mission.TYPE.SWEEP,
                            Mission.TYPE.ESCORT_SHUTTLE,
                            Mission.TYPE.PATROL,
                            Mission.TYPE.PATROL,
                            Mission.TYPE.AIR_SWEEP,
                            Mission.TYPE.PATROL,
                            Mission.TYPE.AIR_PATROL,
                            Mission.TYPE.ESCORT_SHUTTLE,
                            Mission.TYPE.PATROL,
                            Mission.TYPE.SWEEP,
                            Mission.TYPE.PATROL,
                            Mission.TYPE.AIR_SWEEP
                        };

    }
}
