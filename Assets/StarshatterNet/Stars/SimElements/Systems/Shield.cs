﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NavSystem.h/NavSystem.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Nav Points and so on...
*/
using System;

namespace StarshatterNet.Stars.SimElements
{
    public class Shield : ShipSystem
    {
        public enum SUBTYPE { DEFLECTOR = 1, GRAV_SHIELD, HYPER_SHIELD };

        public Shield(SUBTYPE shield_type)
            : base(CATEGORY.SHIELD, (int)shield_type, "shield", shield_value[(int)shield_type], 100, 0)
        {
            shield_cutoff = 0.0f; shield_capacitor = false; shield_bubble = false;
            deflection_cost = 1.0f; shield_curve = 0.05f;

            name = Game.GetText(shield_name[(int)shield_type]);
            abrv = Game.GetText("sys.shield.abrv");

            power_flags = POWER_FLAGS.POWER_WATTS | POWER_FLAGS.POWER_CRITICAL;
            energy = 0.0f;
            power_level = 0.0f;
            shield_level = 0.0f;

            switch (shield_type)
            {
                default:
                case SUBTYPE.DEFLECTOR:
                    capacity = sink_rate = 2.0e3f;
                    shield_factor = 0.05f;
                    break;

                case SUBTYPE.GRAV_SHIELD:
                    capacity = sink_rate = 7.0e3f;
                    shield_factor = 0.01f;
                    break;

                case SUBTYPE.HYPER_SHIELD:
                    capacity = sink_rate = 10.0e3f;
                    shield_factor = 0.003f;
                    break;
            }

            emcon_power[0] = 0;
            emcon_power[1] = 0;
            emcon_power[2] = 100;
        }

        public Shield(Shield s) : base(s)
        {
            shield_factor = s.shield_factor; requested_power_level = 0.0f;
            shield_cutoff = s.shield_cutoff; shield_capacitor = s.shield_capacitor;
            shield_bubble = s.shield_bubble; deflection_cost = s.deflection_cost;
            shield_curve = s.shield_curve;

            power_flags = s.power_flags;
            energy = 0.0f;
            power_level = 0.0f;
            shield_level = 0.0f;

            Mount(s);
        }

 
        public override void ExecFrame(double seconds)
        {
            base.ExecFrame(seconds);

            if (power_level < requested_power_level)
            {
                power_level += (float)(seconds * 0.10);     // ten seconds to charge up

                if (power_level > requested_power_level)
                    power_level = (float)requested_power_level;
            }
            else if (power_level > requested_power_level)
            {
                power_level -= (float)(seconds * 0.20);     // five seconds to power down

                if (power_level < requested_power_level)
                    power_level = (float)requested_power_level;
            }

            if (power_level < 0.01 && !shield_capacitor)
            {
                shield_level = 0.0f;
                energy = 0.0f;
            }
        }

        public double DeflectDamage(Shot shot, double  damage)
        {
            double filter = 1;
            double penetration = 5;
            double leak = 0;

            if (shot != null)
                penetration = shot.Design().penetration;

            filter = 1 - shield_factor * penetration;

            if (filter < 0)
                filter = 0;

            else if (filter > 1)
                filter = 1;

            if (shield_capacitor)
            {
                if (shield_cutoff > 0 && shield_level < 1e-6)
                {
                    leak = damage;
                    energy -= (float)(damage * deflection_cost);
                }

                else
                {
                    leak = damage * (1 - Math.Pow(shield_level, shield_curve) * filter * availability);

                    double deflected = damage - leak;
                    energy -= (float)deflected * deflection_cost;
                }

            }
            else
            {
                leak = damage * (1 - Math.Pow(shield_level, shield_curve) * filter * availability);
            }

            return leak;
        }

        public double ShieldLevel() { return shield_level * 100; }
        public double ShieldFactor() { return shield_factor; }
        public double ShieldCurve() { return shield_curve; }
        public void SetShieldFactor(double f) { shield_factor = (float)f; }
        public void SetShieldCurve(double c) { shield_curve = (float)c; }
        public double ShieldCutoff() { return shield_cutoff; }
        public void SetShieldCutoff(double f) { shield_cutoff = (float)f; }
        public double Capacity() { return capacity; }
        public double Consumption() { return sink_rate; }
        public void SetConsumption(double r) { sink_rate = (float)r; }
        public bool ShieldCapacitor() { return shield_capacitor; }
        public void SetShieldCapacitor(bool c)
        {
            shield_capacitor = c;

            if (shield_capacitor)
            {
                power_flags = POWER_FLAGS.POWER_CRITICAL;
                shield_curve = 0.05f;
            }
            else
            {
                power_flags = POWER_FLAGS.POWER_WATTS | POWER_FLAGS.POWER_CRITICAL;
                shield_curve = 0.25f;
            }
        }
        public bool ShieldBubble() { return shield_bubble; }
        public void SetShieldBubble(bool b) { shield_bubble = b; }
        public double DeflectionCost() { return deflection_cost; }
        public void SetDeflectionCost(double c) { deflection_cost = (float)c; }

        // override from System:
        public override void SetPowerLevel(double level)
        {
            if (level > 100)
                level = 100;
            else if (level < 0)
                level = 0;

            level /= 100;

            if (requested_power_level != level)
            {
                // if the system is on emergency override power,
                // do not let the EMCON system use this method
                // to drop it back to normal power:
                if (power_level > 1 && level == 1)
                {
                    requested_power_level = (float)power_level;
                    return;
                }

                requested_power_level = (float)level;
            }
        }

        public virtual void SetNetShieldLevel(int level)
        {
            if (level > 100) level = 100;
            else if (level < 0) level = 0;

            requested_power_level = (float)(level / 100.0);
            power_level = requested_power_level;
        }


        public override void Distribute(double delivered_energy, double seconds)
        {
            base.Distribute(delivered_energy, seconds);

            if (shield_capacitor)
            {
                if (shield_cutoff > 0 && shield_cutoff < 0.999)
                {
                    float cutoff = shield_cutoff * capacity;

                    if (energy > cutoff)
                        shield_level = (energy - cutoff) / (capacity - cutoff);
                    else
                        shield_level = 0.0f;
                }

                else
                {
                    shield_level = energy / capacity;
                }
            }
            else
            {
                shield_level = energy / sink_rate;
                energy = 0.0f;
            }

            if (shield_level < 0)
                shield_level = 0;
        }
        public override void DoEMCON(int index)
        {
            int e = GetEMCONPower(index);

            if (power_level * 100 > e || emcon != index)
            {
                if (e == 0)
                {
                    PowerOff();
                }
                else if (emcon != index)
                {
                    PowerOn();

                    if (power_level * 100 > e)
                        SetPowerLevel(e);
                }
            }

            emcon = index;
        }



        protected bool shield_capacitor;
        protected bool shield_bubble;
        protected float shield_factor;
        protected float shield_level;
        protected float shield_curve;
        protected float shield_cutoff;
        protected float requested_power_level;
        protected float deflection_cost;
        protected static string[] shield_name = new string[] {
                "sys.shield.none",
                "sys.shield.deflector",
                "sys.shield.grav",
                "sys.shield.hyper"
            };

        protected static int[] shield_value = new int[] { 0, 2, 2, 3 };

    }
}