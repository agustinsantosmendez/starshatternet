﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    Stars.exe
    FILE:         TerrainLayer.h/TerrainLayer.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    A blended detail texture applied to a terrain patch
    through a specific range of altitudes
*/
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars
{
#warning TerrainLayer class is still in development and is not recommended for production.
    public class TerrainLayer
    {
        public TerrainLayer()
        {
            tile_texture = null;
            detail_texture = null;
            min_height = 0;
            max_height = -1;
        }
        //public ~TerrainLayer() { }

        public static bool operator <(TerrainLayer l, TerrainLayer r)
        { return l.min_height < r.min_height; }
        public static bool operator >(TerrainLayer l, TerrainLayer r)
        { return l.min_height > r.min_height; }
        public static bool operator <=(TerrainLayer l, TerrainLayer r)
        { return l.min_height <= r.min_height; }
        public static bool operator >=(TerrainLayer l, TerrainLayer r)
        { return l.min_height >= r.min_height; }
        public static bool operator ==(TerrainLayer l, TerrainLayer r)
        { return l.min_height == r.min_height; }
        public static bool operator !=(TerrainLayer l, TerrainLayer r)
        { return l.min_height != r.min_height; }


        // accessors:
        public string GetTileName() { return tile_name; }
        public string GetDetailName() { return detail_name; }
        public Bitmap GetTileTexture() { return tile_texture; }
        public Bitmap GetDetailTexture() { return detail_texture; }
        public double GetMinHeight() { return min_height; }
        public double GetMaxHeight() { return max_height; }


        internal protected string tile_name;
        internal protected string detail_name;
        protected Bitmap tile_texture;
        protected Bitmap detail_texture;
        internal protected double min_height;
        protected double max_height;

    }
}
