﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CampaignMissionFighter.h/CampaignMissionFighter.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CampaignMissionFighter generates missions and mission
    info for the player's FIGHTER SQUADRON as part of a
    dynamic campaign.
*/
using System;
using System.Collections.Generic;
using DigitalRune.Mathematics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign.Planning;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.StarSystems;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign.Planning
{
#warning CampaignMissionFighter class is still in development and is not recommended for production.
    public class CampaignMissionFighter
    {
        public static bool dump_missions = false;
        public CampaignMissionFighter(Campaign c)
        {
            campaign = c; squadron = null; mission = null; player_elem = null;
            strike_group = null; strike_target = null; prime_target = null;
            carrier_elem = null; ward = null; escort = null; airborne = false; airbase = false;
            ownside = 0; enemy = -1; mission_type = 0; mission_info = null;

            if (campaign == null || campaign.GetPlayerGroup() == null)
            {
                ErrLogger.PrintLine("ERROR - CMF campaign=0x{0:X8} player_group=0x{1:X8} ", campaign, campaign != null ? campaign.GetPlayerGroup() : null);
                return;
            }

            CombatGroup player_group = campaign.GetPlayerGroup();

            switch (player_group.Type())
            {
                case CombatGroup.GROUP_TYPE.WING:
                    {
                        CombatGroup wing = player_group;

                        foreach (CombatGroup iter in wing.GetComponents())
                        {
                            if (iter.Type() == CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON)
                            {
                                squadron = iter;
                            }
                        }
                    }
                    break;

                case CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON:
                case CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON:
                case CombatGroup.GROUP_TYPE.ATTACK_SQUADRON:
                    squadron = player_group;
                    break;

                default:
                    ErrLogger.PrintLine("ERROR - CMF invalid player group: {0} IFF {1}",
                                        player_group.GetDescription(),
                                        player_group.GetIFF());
                    break;
            }

            if (squadron != null)
            {
                CombatGroup carrier = squadron.FindCarrier();

                if (carrier != null && carrier.Type() == CombatGroup.GROUP_TYPE.STARBASE)
                {
                    airbase = true;
                }
            }
        }
        // ~CampaignMissionFighter();
        static int id_key = 1;

        public virtual void CreateMission(CampaignMissionRequest req)
        {
            if (campaign == null || squadron == null || req == null)
                return;

            ErrLogger.PrintLine("\n-----------------------------------------------");
            if (req.Script().Length > 0)
                ErrLogger.PrintLine("CMF CreateMission() request: {0} '{1}' ",
                    Mission.RoleName(req.Type()), req.Script());

            else
                ErrLogger.PrintLine("CMF CreateMission() request: {0} {1}",
                    Mission.RoleName(req.Type()), req.GetObjective() != null ? req.GetObjective().Name() : "(no target)");

            request = req;
            mission_info = null;

            if (request.GetPrimaryGroup() != null)
            {
                switch (request.GetPrimaryGroup().Type())
                {
                    case CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON:
                    case CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON:
                    case CombatGroup.GROUP_TYPE.ATTACK_SQUADRON:
                        squadron = request.GetPrimaryGroup();
                        break;
                }
            }

            ownside = squadron.GetIFF();

            for (int i = 0; i < campaign.GetCombatants().Count; i++)
            {
                int iff = campaign.GetCombatants()[i].GetIFF();
                if (iff > 0 && iff != ownside)
                {
                    enemy = iff;
                    break;
                }
            }

            GenerateMission(id_key++);
            DefineMissionObjectives();

            MissionInfo info = DescribeMission();

            if (info != null)
            {
                campaign.GetMissionList().Add(info);

                ErrLogger.PrintLine("CMF Created {0:3} '{1}'{2}\n", info.id, info.name, Mission.RoleName(mission.Type()));

                if (dump_missions)
                {
                    string script = mission.Serialize();
                    string fname;

                    fname = string.Format("msn{0:3}.def", info.id);
#if TODO
                    FILE* f;
                    fopen_s(&f, fname, "w");
                    if (f)
                    {
                        fprintf(f, "%s\n", script.data());
                        fclose(f);
                    }
#endif
                    throw new System.NotImplementedException();
                }
            }
            else
            {
                ErrLogger.PrintLine("CMF failed to create mission.");
            }
        }


        protected virtual Mission GenerateMission(int id)
        {
            bool found = false;

            SelectType();

            if (request != null && request.Script().Length > 0)
            {
                MissionTemplate mt = new MissionTemplate(id, request.Script(), campaign.Path());
                if (mt != null)
                    mt.SetPlayerSquadron(squadron);
                mission = mt;
                found = true;
            }

            else
            {
                mission_info = campaign.FindMissionTemplate(mission_type, squadron);
                found = mission_info != null;

                if (found)
                {
                    MissionTemplate mt = new MissionTemplate(id, mission_info.script, campaign.Path());
                    if (mt != null)
                        mt.SetPlayerSquadron(squadron);
                    mission = mt;
                }
                else
                {
                    mission = new Mission(id);
                    if (mission != null)
                        mission.SetType(mission_type);
                }
            }

            if (mission == null)
            {
                Exit();
                return null;
            }

            string name = "Fighter Mission " + id;

            mission.SetName(name);
            mission.SetTeam(squadron.GetIFF());
            mission.SetStart(request.StartTime());

            SelectRegion();
            GenerateStandardElements();
            CreatePatrols();

            if (!found)
            {
                GenerateMissionElements();
                mission.SetOK(true);
                mission.Validate();
            }

            else
            {
                mission.Load();

                if (mission.IsOK())
                {
                    player_elem = mission.GetPlayer();
                    prime_target = mission.GetTarget();
                    ward = mission.GetWard();

                    Player p = Player.GetCurrentPlayer();

                    if (player_elem != null && p != null)
                        player_elem.SetAlert(p.FlyingStart() == 0);
                }

                // if there was a problem, scrap the mission
                // and start over:
                else
                {
                    // delete mission;

                    mission = new Mission(id);
                    mission.SetType(mission_type);
                    mission.SetName(name);
                    mission.SetTeam(squadron.GetIFF());
                    mission.SetStart(request.StartTime());

                    SelectRegion();
                    GenerateStandardElements();
                    GenerateMissionElements();

                    mission.SetOK(true);
                    mission.Validate();
                }
            }

            return mission;
        }
        protected virtual void SelectType()
        {
            Mission.TYPE type = Mission.TYPE.PATROL;

            if (request != null)
            {
                type = request.Type();
                if (type == Mission.TYPE.STRIKE)
                {
                    strike_group = request.GetPrimaryGroup();

                    // verify that objective is a ground target:
                    if (!IsGroundObjective(request.GetObjective()))
                    {
                        type = Mission.TYPE.ASSAULT;
                    }
                }

                else if (type == Mission.TYPE.ESCORT_STRIKE)
                {
                    strike_group = request.GetSecondaryGroup();
                    if (strike_group == null || strike_group.CalcValue() < 1)
                    {
                        type = Mission.TYPE.SWEEP;
                        strike_group = null;
                    }
                }
            }

            mission_type = type;
        }
        protected virtual void SelectRegion()
        {
            CombatZone zone = squadron.GetAssignedZone();

            if (zone == null)
                zone = squadron.GetCurrentZone();

            if (zone != null)
            {
                mission.SetStarSystem(campaign.GetSystem(zone.System()));
                mission.SetRegion(zone.GetRegions()[0]);

                orb_region = mission.GetRegion();

                if (zone.GetRegions().Count > 1)
                {
                    air_region = zone.GetRegions()[1];

                    StarSystem system = mission.GetStarSystem();
                    OrbitalRegion rgn = null;

                    if (system != null)
                        rgn = system.FindRegion(air_region);

                    if (rgn == null || rgn.Type() != Orbital.OrbitalType.TERRAIN)
                        air_region = "";
                }

                if (air_region.Length > 0)
                {
                    if (request != null && IsGroundObjective(request.GetObjective()))
                    {
                        airborne = true;
                    }

                    else if (mission.Type() >= Mission.TYPE.AIR_PATROL &&
                            mission.Type() <= Mission.TYPE.AIR_INTERCEPT)
                    {
                        airborne = true;
                    }

                    else if (mission.Type() == Mission.TYPE.STRIKE ||
                            mission.Type() == Mission.TYPE.ESCORT_STRIKE)
                    {
                        if (strike_group != null)
                        {
                            strike_target = campaign.FindStrikeTarget(ownside, strike_group);

                            if (strike_target != null && strike_target.GetRegion() == air_region)
                                airborne = true;
                        }
                    }

                    if (airbase)
                    {
                        mission.SetRegion(air_region);
                    }
                }
            }

            else
            {
                ErrLogger.PrintLine("WARNING: CMF - No zone for '{0}'", squadron.Name());

                StarSystem s = campaign.GetSystemList()[0];

                mission.SetStarSystem(s);
                mission.SetRegion(s.Regions()[0].Name());
            }

            if (!airborne)
            {
                switch (mission.Type())
                {
                    case Mission.TYPE.AIR_PATROL: mission.SetType(Mission.TYPE.PATROL); break;
                    case Mission.TYPE.AIR_SWEEP: mission.SetType(Mission.TYPE.SWEEP); break;
                    case Mission.TYPE.AIR_INTERCEPT: mission.SetType(Mission.TYPE.INTERCEPT); break;
                    default: break;
                }
            }
        }
        protected virtual void GenerateStandardElements()
        {
            foreach (CombatZone z in campaign.GetZones())
            {
                foreach (ZoneForce force in z.GetForces())
                {
                    foreach (CombatGroup g in force.GetGroups())
                    {

                        switch (g.Type())
                        {
                            case CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON:
                            case CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON:
                            case CombatGroup.GROUP_TYPE.ATTACK_SQUADRON:
                            case CombatGroup.GROUP_TYPE.LCA_SQUADRON:
                                CreateSquadron(g);
                                break;

                            case CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON:
                            case CombatGroup.GROUP_TYPE.BATTLE_GROUP:
                            case CombatGroup.GROUP_TYPE.CARRIER_GROUP:
                                CreateElements(g);
                                break;

                            case CombatGroup.GROUP_TYPE.MINEFIELD:
                            case CombatGroup.GROUP_TYPE.BATTERY:
                            case CombatGroup.GROUP_TYPE.MISSILE:
                            case CombatGroup.GROUP_TYPE.STATION:
                            case CombatGroup.GROUP_TYPE.STARBASE:
                            case CombatGroup.GROUP_TYPE.SUPPORT:
                            case CombatGroup.GROUP_TYPE.COURIER:
                            case CombatGroup.GROUP_TYPE.MEDICAL:
                            case CombatGroup.GROUP_TYPE.SUPPLY:
                            case CombatGroup.GROUP_TYPE.REPAIR:
                                CreateElements(g);
                                break;

                            case CombatGroup.GROUP_TYPE.CIVILIAN:
                            case CombatGroup.GROUP_TYPE.WAR_PRODUCTION:
                            case CombatGroup.GROUP_TYPE.FACTORY:
                            case CombatGroup.GROUP_TYPE.REFINERY:
                            case CombatGroup.GROUP_TYPE.RESOURCE:
                            case CombatGroup.GROUP_TYPE.INFRASTRUCTURE:
                            case CombatGroup.GROUP_TYPE.TRANSPORT:
                            case CombatGroup.GROUP_TYPE.NETWORK:
                            case CombatGroup.GROUP_TYPE.HABITAT:
                            case CombatGroup.GROUP_TYPE.STORAGE:
                            case CombatGroup.GROUP_TYPE.NON_COM:
                                CreateElements(g);
                                break;
                        }
                    }
                }
            }
        }

        protected virtual void GenerateMissionElements()
        {
            CreateWards();
            CreatePlayer(squadron);
            CreateTargets();
            CreateEscorts();

            if (player_elem != null)
            {
                Instruction obj = new Instruction(mission.GetRegion(), new Point(0, 0, 0), Instruction.ACTION.RTB);

                if (obj != null)
                    player_elem.AddObjective(obj);
            }
        }

        protected virtual void CreateElements(CombatGroup g)
        {
            MissionElement elem = null;
            List<CombatUnit> units = g.GetUnits();

            CombatUnit cmdr = null;

            for (int i = 0; i < units.Count; i++)
            {
                elem = CreateSingleElement(g, units[i]);

                if (elem != null)
                {
                    if (cmdr == null)
                    {
                        cmdr = units[i];
                    }
                    else
                    {
                        elem.SetCommander(cmdr.Name());

                        if (g.Type() == CombatGroup.GROUP_TYPE.CARRIER_GROUP &&
                                elem.MissionRole() == Mission.TYPE.ESCORT)
                        {
                            Instruction obj = new Instruction(Instruction.ACTION.ESCORT, cmdr.Name());
                            if (obj != null)
                                elem.AddObjective(obj);
                        }
                    }

                    mission.AddElement(elem);
                }
            }
        }
        protected virtual void CreateSquadron(CombatGroup g)
        {
            if (g == null || g.IsReserve()) return;

            CombatUnit fighter = g.GetUnits()[0];
            CombatUnit carrier = FindCarrier(g);

            if (fighter == null || carrier == null) return;

            int live_count = fighter.LiveCount();
            int maint_count = (live_count > 4) ? live_count / 2 : 0;

            MissionElement elem = new MissionElement();

            if (elem == null)
            {
                Exit();
                return;
            }

            elem.SetName(g.Name());
            elem.SetElementID(pkg_id++);

            elem.SetDesign(fighter.GetDesign());
            elem.SetCount(fighter.Count());
            elem.SetDeadCount(fighter.DeadCount());
            elem.SetMaintCount(maint_count);
            elem.SetIFF(fighter.GetIFF());
            elem.SetIntelLevel(g.IntelLevel());
            elem.SetRegion(fighter.GetRegion());

            elem.SetCarrier(carrier.Name());
            elem.SetCommander(carrier.Name());
            elem.SetLocation(carrier.Location() + RandomHelper.RandomPoint());

            elem.SetCombatGroup(g);
            elem.SetCombatUnit(fighter);

            mission.AddElement(elem);
        }
        protected virtual void CreatePlayer(CombatGroup g)
        {
            int pkg_size = 2;

            if (mission.Type() == Mission.TYPE.STRIKE || mission.Type() == Mission.TYPE.ASSAULT)
            {
                if (request != null && request.GetObjective() != null)
                {
                    CombatGroup.GROUP_TYPE tgt_type = request.GetObjective().Type();

                    if (tgt_type >= CombatGroup.GROUP_TYPE.FLEET && tgt_type <= CombatGroup.GROUP_TYPE.CARRIER_GROUP)
                        pkg_size = 4;

                    if (tgt_type == CombatGroup.GROUP_TYPE.STATION || tgt_type == CombatGroup.GROUP_TYPE.STARBASE)
                        pkg_size = 4;
                }
            }

            MissionElement elem = CreateFighterPackage(g, pkg_size, mission.Type());

            if (elem != null)
            {
                Player p = Player.GetCurrentPlayer();
                elem.SetAlert(p != null ? p.FlyingStart() == 0 : true);
                elem.SetPlayer(1);

                if (ward != null)
                {
                    Point approach = elem.Location() - ward.Location();
                    approach.Normalize();

                    Point pickup = ward.Location() + approach * 50e3;
                    double delta = (pickup - elem.Location()).Length;

                    if (delta > 30e3)
                    {
                        Instruction n = new Instruction(elem.Region(), pickup, Instruction.ACTION.ESCORT);
                        n.SetTarget(ward.Name());
                        n.SetSpeed(750);
                        elem.AddNavPoint(n);
                    }

                    Instruction obj = new Instruction(Instruction.ACTION.ESCORT, ward.Name());

                    switch (mission.Type())
                    {
                        case Mission.TYPE.ESCORT_FREIGHT:
                            obj.SetTargetDesc("the star freighter " + ward.Name());
                            break;

                        case Mission.TYPE.ESCORT_SHUTTLE:
                            obj.SetTargetDesc("the shuttle " + ward.Name());
                            break;

                        case Mission.TYPE.ESCORT_STRIKE:
                            obj.SetTargetDesc("the " + ward.Name() + " strike package");
                            break;

                        case Mission.TYPE.DEFEND:
                            obj.SetTargetDesc("the " + ward.Name());
                            break;

                        default:
                            if (ward.GetCombatGroup() != null)
                            {
                                obj.SetTargetDesc("the " + ward.GetCombatGroup().GetDescription());
                            }
                            else
                            {
                                obj.SetTargetDesc("the " + ward.Name());
                            }
                            break;
                    }

                    elem.AddObjective(obj);
                }

                mission.AddElement(elem);

                player_elem = elem;
            }
        }

        protected virtual void CreatePatrols()
        {
            List<MissionElement> patrols = new List<MissionElement>();

            foreach (MissionElement squad_elem in mission.GetElements())
            {
                CombatGroup squadron = squad_elem.GetCombatGroup();
                CombatUnit unit = squad_elem.GetCombatUnit();

                if (!squad_elem.IsSquadron() || squadron == null || unit == null || unit.LiveCount() < 4)
                    continue;

                if (squadron.Type() == CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON ||
                        squadron.Type() == CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON)
                {

                    StarSystem system = mission.GetStarSystem();
                    CombatGroup base_ = squadron.FindCarrier();

                    if (base_ == null)
                        continue;

                    OrbitalRegion region = system.FindRegion(base_.GetRegion());

                    if (region == null)
                        continue;

                    Mission.TYPE patrol_type = Mission.TYPE.PATROL;
                    Point base_loc;

                    if (region.Type() == Orbital.OrbitalType.TERRAIN)
                    {
                        patrol_type = Mission.TYPE.AIR_PATROL;

                        if (RandomHelper.RandomChance(2, 3))
                            continue;
                    }

                    base_loc = base_.Location() + RandomHelper.RandomPoint() * 1.5;

                    if (region.Type() == Orbital.OrbitalType.TERRAIN)
                        base_loc += new Point(0, 0, 14.0e3);

                    MissionElement elem = CreateFighterPackage(squadron, 2, patrol_type);
                    if (elem != null)
                    {
                        elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                        elem.SetRegion(base_.GetRegion());
                        elem.SetLocation(base_loc);
                        patrols.Add(elem);
                    }
                }
            }

            foreach (var iter in patrols)
                mission.AddElement(iter);
        }
        protected virtual void CreateWards()
        {
            switch (mission.Type())
            {
                case Mission.TYPE.ESCORT_FREIGHT: CreateWardFreight(); break;
                case Mission.TYPE.ESCORT_SHUTTLE: CreateWardShuttle(); break;
                case Mission.TYPE.ESCORT_STRIKE: CreateWardStrike(); break;
                default: break;
            }
        }
        protected virtual void CreateWardFreight()
        {
            if (mission == null || mission.GetStarSystem() == null) return;

            CombatUnit carrier = FindCarrier(squadron);
            CombatGroup freight = null;

            if (request != null)
                freight = request.GetObjective();

            if (freight == null)
                freight = campaign.FindGroup(ownside, CombatGroup.GROUP_TYPE.FREIGHT);

            if (freight == null || freight.CalcValue() < 1) return;

            CombatUnit unit = freight.GetNextUnit();
            if (unit == null) return;

            MissionElement elem = CreateSingleElement(freight, unit);
            if (elem == null) return;

            elem.SetMissionRole(Mission.TYPE.CARGO);
            elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
            elem.SetRegion(squadron.GetRegion());

            if (carrier != null)
                elem.SetLocation(carrier.Location() + RandomHelper.RandomPoint() * 2);

            ward = elem;
            mission.AddElement(elem);


            StarSystem system = mission.GetStarSystem();
            OrbitalRegion rgn1 = system.FindRegion(elem.Region());
            Point delta = rgn1.Location() - rgn1.Primary().Location();
            Point npt_loc = elem.Location();
            Instruction n = null;

            delta.Normalize();
            delta *= 200.0e3;

            npt_loc += delta;

            n = new Instruction(elem.Region(), npt_loc, Instruction.ACTION.VECTOR);

            if (n != null)
            {
                n.SetSpeed(500);
                elem.AddNavPoint(n);
            }

            string rgn2 = elem.Region();
            List<CombatZone> zones = campaign.GetZones();
            if (zones[zones.Count - 1].HasRegion(rgn2))
                rgn2 = zones[0].GetRegions()[0];
            else
                rgn2 = zones[zones.Count - 1].GetRegions()[0];

            n = new Instruction(rgn2, new Point(0, 0, 0), Instruction.ACTION.VECTOR);

            if (n != null)
            {
                n.SetSpeed(750);
                elem.AddNavPoint(n);
            }
        }
        protected virtual void CreateWardShuttle()
        {
            if (mission == null || mission.GetStarSystem() == null) return;

            CombatUnit carrier = FindCarrier(squadron);
            CombatGroup shuttle = campaign.FindGroup(ownside, CombatGroup.GROUP_TYPE.LCA_SQUADRON);

            if (shuttle == null || shuttle.CalcValue() < 1) return;

            List<CombatUnit> units = shuttle.GetUnits();

            MissionElement elem = CreateFighterPackage(shuttle, 1, Mission.TYPE.CARGO);
            if (elem == null) return;

            elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
            elem.SetRegion(orb_region);
            elem.Loadouts().Clear();

            if (carrier != null)
                elem.SetLocation(carrier.Location() + RandomHelper.RandomPoint() * 2);

            ward = elem;
            mission.AddElement(elem);

            // if there is terrain nearby, then have the shuttle fly down to it:
            if (air_region.Length > 0)
            {
                StarSystem system = mission.GetStarSystem();
                OrbitalRegion rgn1 = system.FindRegion(elem.Region());
                Point delta = rgn1.Location() - rgn1.Primary().Location();
                Point npt_loc = elem.Location();
                Instruction n = null;

                delta.Normalize();
                delta *= -200.0e3;

                npt_loc += delta;

                n = new Instruction(elem.Region(), npt_loc, Instruction.ACTION.VECTOR);

                if (n != null)
                {
                    n.SetSpeed(500);
                    elem.AddNavPoint(n);
                }

                n = new Instruction(air_region, new Point(0, 0, 10.0e3), Instruction.ACTION.VECTOR);

                if (n != null)
                {
                    n.SetSpeed(500);
                    elem.AddNavPoint(n);
                }
            }

            // otherwise, escort the shuttle in for a landing on the carrier:
            else if (carrier != null)
            {
                Point src = carrier.Location() + RandomHelper.RandomDirection() * 150e3;
                Point dst = carrier.Location() + RandomHelper.RandomDirection() * 25e3;
                Instruction n = null;

                elem.SetLocation(src);

                n = new Instruction(elem.Region(), dst, Instruction.ACTION.DOCK);
                if (n != null)
                {
                    n.SetTarget(carrier.Name());
                    n.SetSpeed(500);
                    elem.AddNavPoint(n);
                }
            }
        }
        protected virtual void CreateWardStrike()
        {
            if (mission == null || mission.GetStarSystem() == null) return;

            CombatUnit carrier = FindCarrier(squadron);
            CombatGroup strike = strike_group;

            if (strike == null || strike.CalcValue() < 1) return;

            List<CombatUnit> units = strike.GetUnits();

            Mission.TYPE type = Mission.TYPE.ASSAULT;

            if (airborne)
                type = Mission.TYPE.STRIKE;

            MissionElement elem = CreateFighterPackage(strike, 2, type);
            if (elem == null) return;

            if (strike.GetParent() == squadron.GetParent())
            {
                Player p = Player.GetCurrentPlayer();
                elem.SetAlert(p != null ? p.FlyingStart() == 0 : true);
            }

            elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
            elem.SetRegion(squadron.GetRegion());

            if (strike_target != null)
            {
                Instruction obj = new Instruction(Instruction.ACTION.ASSAULT, strike_target.Name());

                if (obj != null)
                {
                    if (airborne)
                        obj.SetAction(Instruction.ACTION.STRIKE);

                    elem.AddObjective(obj);
                }
            }

            ward = elem;
            mission.AddElement(elem);


            StarSystem system = mission.GetStarSystem();
            OrbitalRegion rgn1 = system.FindRegion(elem.Region());
            Point delta = rgn1.Location() - rgn1.Primary().Location();
            Point npt_loc = elem.Location();
            Instruction n = null;

            if (airborne)
            {
                delta.Normalize();
                delta *= -30.0e3;
                npt_loc += delta;

                n = new Instruction(elem.Region(), npt_loc, Instruction.ACTION.VECTOR);
                if (n != null)
                {
                    n.SetSpeed(500);
                    elem.AddNavPoint(n);
                }

                npt_loc = new Point(0, 0, 10.0e3);

                n = new Instruction(air_region, npt_loc, Instruction.ACTION.VECTOR);
                if (n != null)
                {
                    n.SetSpeed(500);
                    elem.AddNavPoint(n);
                }
            }

            // IP:
            if (strike_target != null)
            {
                delta = strike_target.Location() - npt_loc;
                delta.Normalize();
                delta *= 15.0e3;

                npt_loc = strike_target.Location() + delta + new Point(0, 0, 8.0e3);

                n = new Instruction(strike_target.GetRegion(), npt_loc, Instruction.ACTION.STRIKE);
                if (n != null)
                {
                    n.SetSpeed(500);
                    elem.AddNavPoint(n);
                }
            }

            if (airborne)
            {
                n = new Instruction(air_region, new Point(0, 0, 30.0e3), Instruction.ACTION.VECTOR);
                if (n != null)
                {
                    n.SetSpeed(500);
                    elem.AddNavPoint(n);
                }
            }

            if (carrier != null)
            {
                n = new Instruction(elem.Region(), carrier.Location() - new Point(0, -20.0e3, 0), Instruction.ACTION.VECTOR);
                if (n != null)
                {
                    n.SetSpeed(500);
                    elem.AddNavPoint(n);
                }
            }

            // find the strike target element:
            if (strike_target != null)
            {
                prime_target = mission.FindElement(strike_target.Name());
            }
        }

        protected virtual void CreateEscorts()
        {
            bool escort_needed = false;

            if (mission.Type() == Mission.TYPE.STRIKE || mission.Type() == Mission.TYPE.ASSAULT)
            {
                if (request != null && request.GetObjective() != null)
                {
                    CombatGroup.GROUP_TYPE tgt_type = request.GetObjective().Type();

                    if (tgt_type == CombatGroup.GROUP_TYPE.CARRIER_GROUP ||
                            tgt_type == CombatGroup.GROUP_TYPE.STATION ||
                            tgt_type == CombatGroup.GROUP_TYPE.STARBASE)

                        escort_needed = true;
                }
            }

            if (player_elem != null && escort_needed)
            {
                CombatGroup s = FindSquadron(ownside, CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON);

                if (s != null && s.IsAssignable())
                {
                    MissionElement elem = CreateFighterPackage(s, 2, Mission.TYPE.ESCORT_STRIKE);

                    if (elem != null)
                    {
                        Point offset = new Point(2.0e3, 2.0e3, 1.0e3);

                        foreach (Instruction npt in player_elem.NavList())
                        {
                            Instruction n = new Instruction(npt.RegionName(), npt.Location() + offset, Instruction.ACTION.ESCORT);
                            if (n != null)
                            {
                                n.SetSpeed(npt.Speed());
                                elem.AddNavPoint(n);
                            }
                        }

                        mission.AddElement(elem);
                    }
                }
            }
        }

        protected virtual void CreateTargets()
        {
            switch (mission.Type())
            {
                default:
                case Mission.TYPE.DEFEND:
                case Mission.TYPE.PATROL: CreateTargetsPatrol(); break;
                case Mission.TYPE.SWEEP: CreateTargetsSweep(); break;
                case Mission.TYPE.INTERCEPT: CreateTargetsIntercept(); break;
                case Mission.TYPE.ESCORT_FREIGHT: CreateTargetsFreightEscort(); break;

                case Mission.TYPE.ESCORT:
                case Mission.TYPE.ESCORT_SHUTTLE: CreateTargetsShuttleEscort(); break;
                case Mission.TYPE.ESCORT_STRIKE: CreateTargetsStrikeEscort(); break;
                case Mission.TYPE.STRIKE: CreateTargetsStrike(); break;
                case Mission.TYPE.ASSAULT: CreateTargetsAssault(); break;
            }
        }
        protected virtual void CreateTargetsPatrol()
        {
            if (squadron == null || player_elem == null) return;

            string region = squadron.GetRegion();
            Point base_loc = player_elem.Location();
            Point patrol_loc;

            if (airborne)
                base_loc = RandomHelper.RandomPoint() * 2 + new Point(0, 0, 12.0e3);

            else if (carrier_elem != null)
                base_loc = carrier_elem.Location();

            if (airborne)
            {
                if (!airbase)
                    PlanetaryInsertion(player_elem);
                region = air_region;
                patrol_loc = base_loc +
                RandomHelper.RandomDirection() * RandomHelper.Random(60e3, 100e3);
            }
            else
            {
                patrol_loc = base_loc +
                RandomHelper.RandomDirection() * RandomHelper.Random(110e3, 160e3);
            }

            Instruction n = new Instruction(region, patrol_loc, Instruction.ACTION.PATROL);
            if (n != null)
                player_elem.AddNavPoint(n);

            int ntargets = (int)RandomHelper.Random(2.0, 5.1);

            while (ntargets > 0)
            {
                int t = CreateRandomTarget(region, patrol_loc);
                ntargets -= t;
                if (t < 1) break;
            }

            if (airborne && !airbase)
            {
                OrbitalInsertion(player_elem);
            }

            Instruction obj = new Instruction(n);
            obj.SetTargetDesc("inbound enemy units");
            player_elem.AddObjective(obj);

            if (carrier_elem != null && !airborne)
            {
                obj = new Instruction(Instruction.ACTION.DEFEND, carrier_elem.Name());
                if (obj != null)
                {
                    obj.SetTargetDesc("the " + carrier_elem.Name() + " battle group");
                    player_elem.AddObjective(obj);
                }
            }
        }
        protected virtual void CreateTargetsSweep()
        {
            if (squadron == null || player_elem == null) return;

            double traverse = Math.PI;
            double a = RandomHelper.Random(-Math.PI / 2, Math.PI / 2);
            Point base_loc = player_elem.Location();
            Point sweep_loc = base_loc;
            string region = player_elem.Region();
            Instruction n = null;

            if (carrier_elem != null)
                base_loc = carrier_elem.Location();

            if (airborne)
            {
                PlanetaryInsertion(player_elem);
                region = air_region;
                sweep_loc = RandomHelper.RandomPoint() + new Point(0, 0, 10.0e3);   // keep it airborne!
            }

            sweep_loc += new Point(Math.Sin(a), -Math.Cos(a), 0) * 100.0e3;

            n = new Instruction(region, sweep_loc, Instruction.ACTION.VECTOR);
            if (n != null)
            {
                n.SetSpeed(750);
                player_elem.AddNavPoint(n);
            }

            int index = 0;
            int ntargets = 6;

            while (traverse > 0)
            {
                double a1 = RandomHelper.Random(Math.PI / 4, Math.PI / 2);
                traverse -= a1;
                a += a1;

                sweep_loc += new Point(Math.Sin(a), -Math.Cos(a), 0) * 80.0e3;

                n = new Instruction(region, sweep_loc, Instruction.ACTION.SWEEP);
                if (n != null)
                {
                    n.SetSpeed(750);
                    n.SetFormation(Instruction.FORMATION.SPREAD);
                    player_elem.AddNavPoint(n);
                }

                if (ntargets != 0 && RandomHelper.RandomChance())
                {
                    ntargets -= CreateRandomTarget(region, sweep_loc);
                }

                index++;
            }

            if (ntargets > 0)
                CreateRandomTarget(region, sweep_loc);

            if (airborne && !airbase)
            {
                OrbitalInsertion(player_elem);
                region = player_elem.Region();
            }

            sweep_loc = base_loc;
            sweep_loc.Y += 30.0e3;

            n = new Instruction(region, sweep_loc, Instruction.ACTION.VECTOR);
            if (n != null)
            {
                n.SetSpeed(750);
                player_elem.AddNavPoint(n);
            }

            Instruction obj = new Instruction(region, sweep_loc, Instruction.ACTION.SWEEP);
            if (obj != null)
            {
                obj.SetTargetDesc("enemy patrols");
                player_elem.AddObjective(obj);
            }

            if (carrier_elem != null && !airborne)
            {
                obj = new Instruction(Instruction.ACTION.DEFEND, carrier_elem.Name());
                if (obj != null)
                {
                    obj.SetTargetDesc("the " + carrier_elem.Name() + " battle group");
                    player_elem.AddObjective(obj);
                }
            }
        }
        protected virtual void CreateTargetsIntercept()
        {
            if (squadron == null || player_elem == null) return;

            CombatUnit carrier = FindCarrier(squadron);
            CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.ATTACK_SQUADRON);
            CombatGroup s2 = FindSquadron(enemy, CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON);

            if (s == null || s2 == null) return;

            int ninbound = 2 + ((RandomHelper.RandomIndex() < 5) ? 1 : 0);
            bool second = ninbound > 2;
            string attacker = "";

            while (ninbound-- != 0)
            {
                MissionElement elem = CreateFighterPackage(s, 4, Mission.TYPE.ASSAULT);
                if (elem != null)
                {
                    elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                    elem.Loadouts().Clear();
                    elem.Loadouts().Add(new MissionLoad(-1, "Hvy Ship Strike"));

                    if (carrier != null)
                    {
                        Instruction obj = new Instruction(Instruction.ACTION.ASSAULT, carrier.Name());
                        if (obj != null)
                        {
                            elem.AddObjective(obj);
                            elem.SetLocation(carrier.Location() + RandomHelper.RandomPoint() * 6);
                        }
                    }
                    else
                    {
                        elem.SetLocation(squadron.Location() + RandomHelper.RandomPoint() * 5);
                    }

                    mission.AddElement(elem);

                    attacker = elem.Name();

                    if (prime_target == null)
                    {
                        prime_target = elem;
                        Instruction obj = new Instruction(Instruction.ACTION.INTERCEPT, attacker);
                        if (obj != null)
                        {
                            obj.SetTargetDesc("inbound strike package '" + elem.Name() + "'");
                            player_elem.AddObjective(obj);
                        }
                    }

                    MissionElement e2 = CreateFighterPackage(s2, 2, Mission.TYPE.ESCORT);
                    if (e2 != null)
                    {
                        e2.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                        e2.SetLocation(elem.Location() + RandomHelper.RandomPoint() * 0.25);

                        Instruction obj = new Instruction(Instruction.ACTION.ESCORT, elem.Name());
                        if (obj != null)
                            e2.AddObjective(obj);
                        mission.AddElement(e2);
                    }
                }
            }

            if (second)
            {
                // second friendly fighter package
                CombatGroup s3 = FindSquadron(ownside, CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON);

                if (s3 != null)
                {
                    MissionElement elem = CreateFighterPackage(s3, 2, Mission.TYPE.INTERCEPT);
                    if (elem != null)
                    {
                        Player p = Player.GetCurrentPlayer();
                        elem.SetAlert(p != null ? p.FlyingStart() == 0 : true);

                        Instruction obj = new Instruction(Instruction.ACTION.INTERCEPT, attacker);
                        if (obj != null)
                            elem.AddObjective(obj);
                        mission.AddElement(elem);
                    }
                }
            }

            if (carrier != null && !airborne)
            {
                Instruction obj = new Instruction(Instruction.ACTION.DEFEND, carrier.Name());
                if (obj != null)
                {
                    obj.SetTargetDesc("the " + carrier.Name() + " battle group");
                    player_elem.AddObjective(obj);
                }
            }
        }
        protected virtual void CreateTargetsFreightEscort()
        {
            if (squadron == null || player_elem == null) return;

            if (ward == null)
            {
                CreateTargetsPatrol();
                return;
            }

            CombatUnit carrier = FindCarrier(squadron);
            CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.ATTACK_SQUADRON);
            CombatGroup s2 = FindSquadron(enemy, CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON);

            if (s == null) s = s2;

            if (s == null || s2 == null) return;

            MissionElement elem = CreateFighterPackage(s, 2, Mission.TYPE.ASSAULT);
            if (elem != null)
            {
                elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);

                elem.SetLocation(ward.Location() + RandomHelper.RandomPoint() * 5);

                Instruction obj = new Instruction(Instruction.ACTION.ASSAULT, ward.Name());
                if (obj != null)
                    elem.AddObjective(obj);
                mission.AddElement(elem);

                MissionElement e2 = CreateFighterPackage(s2, 2, Mission.TYPE.ESCORT);
                if (e2 != null)
                {
                    e2.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                    e2.SetLocation(elem.Location() + RandomHelper.RandomPoint() * 0.25);

                    Instruction obj2 = new Instruction(Instruction.ACTION.ESCORT, elem.Name());
                    if (obj2 != null)
                        e2.AddObjective(obj2);
                    mission.AddElement(e2);
                }
            }

            Instruction obj3 = new Instruction(mission.GetRegion(), new Point(0, 0, 0), Instruction.ACTION.PATROL);

            if (obj3 != null)
            {
                obj3.SetTargetDesc("enemy patrols");
                player_elem.AddObjective(obj3);
            }
        }
        protected virtual void CreateTargetsShuttleEscort()
        {
            CreateTargetsFreightEscort();
        }

        protected virtual void CreateTargetsStrikeEscort()
        {
            if (squadron == null || player_elem == null) return;

            if (ward != null)
            {
                Point offset = new Point(2.0e3, 2.0e3, 1.0e3);

                foreach (Instruction npt in ward.NavList())
                {
                    Instruction n = new Instruction(npt.RegionName(), npt.Location() + offset, Instruction.ACTION.ESCORT);
                    if (n != null)
                    {
                        n.SetSpeed(npt.Speed());
                        player_elem.AddNavPoint(n);
                    }
                }
            }
        }

        protected virtual void CreateTargetsStrike()
        {
            if (squadron == null || player_elem == null) return;

            if (request != null && request.GetObjective() != null)
                strike_target = request.GetObjective();

            if (strike_target != null && strike_group != null)
            {
                CreateElements(strike_target);

                foreach (MissionElement elem in mission.GetElements())
                {
                    if (elem.GetCombatGroup() == strike_target)
                    {
                        prime_target = elem;
                        Instruction obj = new Instruction(Instruction.ACTION.STRIKE, elem.Name());
                        if (obj != null)
                        {
                            obj.SetTargetDesc("preplanned target '" + elem.Name() + "'");
                            player_elem.AddObjective(obj);
                        }

                        // create flight plan:
                        RLoc rloc = new RLoc();
                        Point loc = new Point(0, 0, 15e3);
                        Instruction n = null;

                        PlanetaryInsertion(player_elem);

                        // target approach and strike:
                        Point delta = prime_target.Location() - loc;

                        if (delta.Length >= 100e3)
                        {
                            Point mid = loc + delta * 0.5;
                            mid.Z = 10.0e3;

                            rloc.SetReferenceLoc(null);
                            rloc.SetBaseLocation(mid);
                            rloc.SetDistance(20e3);
                            rloc.SetDistanceVar(5e3);
                            rloc.SetAzimuth(90 * ConstantsF.DEGREES);
                            rloc.SetAzimuthVar(25 * ConstantsF.DEGREES);

                            n = new Instruction(prime_target.Region(), new Point(), Instruction.ACTION.VECTOR);
                            if (n != null)
                            {
                                n.SetSpeed(750);
                                n.SetRLoc(rloc);
                                player_elem.AddNavPoint(n);
                            }

                            loc = mid;
                        }

                        delta = loc - prime_target.Location();
                        delta.Normalize();
                        delta *= 25.0e3;

                        loc = prime_target.Location() + delta;
                        loc.Z = 8.0e3;

                        n = new Instruction(prime_target.Region(), loc, Instruction.ACTION.STRIKE);
                        if (n != null)
                        {
                            n.SetSpeed(500);
                            player_elem.AddNavPoint(n);
                        }

                        // exeunt:
                        rloc.SetReferenceLoc(null);
                        rloc.SetBaseLocation(new Point(0, 0, 30.0e3));
                        rloc.SetDistance(50e3);
                        rloc.SetDistanceVar(5e3);
                        rloc.SetAzimuth(-90 * ConstantsF.DEGREES);
                        rloc.SetAzimuthVar(25 * ConstantsF.DEGREES);

                        n = new Instruction(prime_target.Region(), new Point(), Instruction.ACTION.VECTOR);
                        if (n != null)
                        {
                            n.SetSpeed(750);
                            n.SetRLoc(rloc);
                            player_elem.AddNavPoint(n);
                        }

                        if (carrier_elem != null)
                        {
                            rloc.SetReferenceLoc(null);
                            rloc.SetBaseLocation(carrier_elem.Location());
                            rloc.SetDistance(60e3);
                            rloc.SetDistanceVar(10e3);
                            rloc.SetAzimuth(180 * ConstantsF.DEGREES);
                            rloc.SetAzimuthVar(30 * ConstantsF.DEGREES);
                            n = new Instruction(carrier_elem.Region(), new Point(), Instruction.ACTION.RTB);
                            if (n != null)
                            {
                                n.SetSpeed(750);
                                n.SetRLoc(rloc);
                                player_elem.AddNavPoint(n);
                            }
                        }

                        break;
                    }
                }
            }
        }
        protected virtual void CreateTargetsAssault()
        {
            if (squadron == null || player_elem == null) return;

            CombatGroup assigned = null;

            if (request != null)
                assigned = request.GetObjective();

            if (assigned != null)
            {
                if (assigned.Type() > CombatGroup.GROUP_TYPE.WING && assigned.Type() < CombatGroup.GROUP_TYPE.FLEET)
                {
                    mission.AddElement(CreateFighterPackage(assigned, 2, Mission.TYPE.CARGO));
                }
                else
                {
                    CreateElements(assigned);
                }

                // select the prime target element - choose the lowest ranking
                // unit of a DESRON, CBG, or CVBG:

                foreach (MissionElement elem in mission.GetElements())
                {
                    if (elem.GetCombatGroup() == assigned)
                    {
                        if (prime_target == null || assigned.Type() <= CombatGroup.GROUP_TYPE.CARRIER_GROUP)
                        {
                            prime_target = elem;
                        }
                    }
                }

                if (prime_target != null)
                {
                    MissionElement elem = prime_target;

                    Instruction obj = new Instruction(Instruction.ACTION.ASSAULT, elem.Name());
                    if (obj != null)
                    {
                        obj.SetTargetDesc("preplanned target '" + elem.Name() + "'");
                        player_elem.AddObjective(obj);
                    }

                    // create flight plan:
                    RLoc rloc = new RLoc();
                    Point dummy = new Point(0, 0, 0);
                    Instruction instr = null;
                    Point loc = player_elem.Location();
                    Point tgt = elem.Location();
                    Point mid;

                    CombatGroup tgt_group = elem.GetCombatGroup();
                    if (tgt_group != null && tgt_group.GetFirstUnit() != null && tgt_group.IsMovable())
                    {
                        tgt = tgt_group.GetFirstUnit().Location();
                    }

                    if (carrier_elem != null)
                        loc = carrier_elem.Location();

                    mid = loc + (elem.Location() - loc) * 0.5;

                    rloc.SetReferenceLoc(null);
                    rloc.SetBaseLocation(mid);
                    rloc.SetDistance(40e3);
                    rloc.SetDistanceVar(5e3);
                    rloc.SetAzimuth(90 * ConstantsF.DEGREES);
                    rloc.SetAzimuthVar(45 * ConstantsF.DEGREES);

                    instr = new Instruction(elem.Region(), dummy, Instruction.ACTION.VECTOR);
                    if (instr != null)
                    {
                        instr.SetSpeed(750);
                        instr.SetRLoc(rloc);

                        player_elem.AddNavPoint(instr);

                        if (RandomHelper.RandomChance())
                        {
                            CreateRandomTarget(elem.Region(), rloc.Location());
                        }
                    }

                    rloc.SetReferenceLoc(null);
                    rloc.SetBaseLocation(tgt);
                    rloc.SetDistance(60e3);
                    rloc.SetDistanceVar(5e3);
                    rloc.SetAzimuth(120 * ConstantsF.DEGREES);
                    rloc.SetAzimuthVar(15 * ConstantsF.DEGREES);

                    instr = new Instruction(elem.Region(), dummy, Instruction.ACTION.ASSAULT);
                    if (instr != null)
                    {
                        instr.SetSpeed(750);
                        instr.SetRLoc(rloc);
                        instr.SetTarget(elem.Name());

                        player_elem.AddNavPoint(instr);
                    }

                    if (carrier_elem != null)
                    {
                        rloc.SetReferenceLoc(null);
                        rloc.SetBaseLocation(loc);
                        rloc.SetDistance(30e3);
                        rloc.SetDistanceVar(0);
                        rloc.SetAzimuth(180 * ConstantsF.DEGREES);
                        rloc.SetAzimuthVar(60 * ConstantsF.DEGREES);

                        instr = new Instruction(carrier_elem.Region(), dummy, Instruction.ACTION.RTB);
                        if (instr != null)
                        {
                            instr.SetSpeed(500);
                            instr.SetRLoc(rloc);

                            player_elem.AddNavPoint(instr);
                        }
                    }
                }
            }
        }
        protected virtual int CreateRandomTarget(string rgn, Point base_loc)
        {
            if (mission == null) return 0;

            int ntargets = 0;
            int ttype = RandomHelper.RandomIndex();
            bool oca = (mission.Type() == Mission.TYPE.SWEEP);

            if (ttype < 8)
            {
                CombatGroup s = null;

                if (ttype < 4)
                    s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON);
                else
                    s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON);

                if (s != null)
                {
                    MissionElement elem = CreateFighterPackage(s, 2, Mission.TYPE.SWEEP);
                    if (elem != null)
                    {
                        elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                        elem.SetRegion(rgn);
                        elem.SetLocation(base_loc + RandomHelper.RandomPoint() * 1.5);
                        mission.AddElement(elem);
                        ntargets++;
                    }
                }
            }
            else if (ttype < 12)
            {
                if (oca)
                {
                    CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.LCA_SQUADRON);

                    if (s != null)
                    {
                        MissionElement elem = CreateFighterPackage(s, 1, Mission.TYPE.CARGO);
                        if (elem != null)
                        {
                            elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                            elem.SetRegion(rgn);
                            elem.SetLocation(base_loc + RandomHelper.RandomPoint() * 2);
                            mission.AddElement(elem);
                            ntargets++;

                            CombatGroup s2 = FindSquadron(enemy, CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON);

                            if (s2 != null)
                            {
                                MissionElement e2 = CreateFighterPackage(s2, 2, Mission.TYPE.ESCORT);
                                if (e2 != null)
                                {
                                    e2.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                                    e2.SetRegion(rgn);
                                    e2.SetLocation(elem.Location() + RandomHelper.RandomPoint() * 0.5);

                                    Instruction obj = new Instruction(Instruction.ACTION.ESCORT, elem.Name());
                                    if (obj != null)
                                        e2.AddObjective(obj);
                                    mission.AddElement(e2);
                                    ntargets++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.ATTACK_SQUADRON);

                    if (s != null)
                    {
                        MissionElement elem = CreateFighterPackage(s, 2, Mission.TYPE.ASSAULT);
                        if (elem != null)
                        {
                            elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                            elem.SetRegion(rgn);
                            elem.SetLocation(base_loc + RandomHelper.RandomPoint() * 1.3);
                            mission.AddElement(elem);
                            ntargets++;
                        }
                    }
                }
            }
            else if (ttype < 15)
            {
                if (oca)
                {
                    CombatGroup s = null;

                    if (airborne)
                        s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.LCA_SQUADRON);
                    else
                        s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.FREIGHT);

                    if (s != null)
                    {
                        MissionElement elem = CreateFighterPackage(s, 1, Mission.TYPE.CARGO);
                        if (elem != null)
                        {
                            elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                            elem.SetRegion(rgn);
                            elem.SetLocation(base_loc + RandomHelper.RandomPoint() * 2);
                            mission.AddElement(elem);
                            ntargets++;

                            CombatGroup s2 = FindSquadron(enemy, CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON);

                            if (s2 != null)
                            {
                                MissionElement e2 = CreateFighterPackage(s2, 2, Mission.TYPE.ESCORT);
                                if (e2 != null)
                                {
                                    e2.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                                    e2.SetRegion(rgn);
                                    e2.SetLocation(elem.Location() + RandomHelper.RandomPoint() * 0.5);

                                    Instruction obj = new Instruction(Instruction.ACTION.ESCORT, elem.Name());
                                    if (obj != null)
                                        e2.AddObjective(obj);
                                    mission.AddElement(e2);
                                    ntargets++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.ATTACK_SQUADRON);

                    if (s != null)
                    {
                        MissionElement elem = CreateFighterPackage(s, 2, Mission.TYPE.ASSAULT);
                        if (elem != null)
                        {
                            elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                            elem.SetRegion(rgn);
                            elem.SetLocation(base_loc + RandomHelper.RandomPoint() * 1.1);
                            mission.AddElement(elem);
                            ntargets++;

                            CombatGroup s2 = FindSquadron(enemy, CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON);

                            if (s2 != null)
                            {
                                MissionElement e2 = CreateFighterPackage(s2, 2, Mission.TYPE.ESCORT);
                                if (e2 != null)
                                {
                                    e2.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                                    e2.SetRegion(rgn);
                                    e2.SetLocation(elem.Location() + RandomHelper.RandomPoint() * 0.5);

                                    Instruction obj = new Instruction(Instruction.ACTION.ESCORT, elem.Name());
                                    if (obj != null)
                                        e2.AddObjective(obj);
                                    mission.AddElement(e2);
                                    ntargets++;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.LCA_SQUADRON);

                if (s != null)
                {
                    MissionElement elem = CreateFighterPackage(s, 2, Mission.TYPE.CARGO);
                    if (elem != null)
                    {
                        elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                        elem.SetRegion(rgn);
                        elem.SetLocation(base_loc + RandomHelper.RandomPoint() * 2);
                        mission.AddElement(elem);
                        ntargets++;
                    }
                }
            }

            return ntargets;
        }

        protected virtual bool IsGroundObjective(CombatGroup obj)
        {
            bool ground = false;

            if (obj != null)
            {
                CombatGroup pgroup = campaign.GetPlayerGroup();

                if (pgroup != null)
                {
                    CombatZone zone = pgroup.GetAssignedZone();

                    if (zone != null)
                    {
                        StarSystem system = campaign.GetSystem(zone.System());

                        if (system != null)
                        {
                            OrbitalRegion region = system.FindRegion(obj.GetRegion());

                            if (region != null && region.Type() == Orbital.OrbitalType.TERRAIN)
                            {
                                ground = true;
                            }
                        }
                    }
                }
            }

            return ground;
        }

        protected virtual void PlanetaryInsertion(MissionElement elem)
        {
            if (mission == null || elem == null) return;
            if (mission.GetStarSystem() == null) return;

            MissionElement carrier = mission.FindElement(elem.Commander());
            StarSystem system = mission.GetStarSystem();
            OrbitalRegion rgn1 = system.FindRegion(elem.Region());
            OrbitalRegion rgn2 = system.FindRegion(air_region);
            Point npt_loc = elem.Location();
            Instruction n = null;
            Player p = Player.GetCurrentPlayer();

            int flying_start = p != null ? p.FlyingStart() : 0;

            if (carrier != null && flying_start == 0)
            {
                npt_loc = carrier.Location() + new Point(1e3, -5e3, 0);
            }

            if (rgn1 != null && rgn2 != null)
            {
                double delta_t = mission.Start() - campaign.GetTime();
                Point r1 = rgn1.PredictLocation(delta_t);
                Point r2 = rgn2.PredictLocation(delta_t);

                Point delta = r2 - r1;

                delta.Y *= -1;
                delta.Normalize();
                delta *= 10e3;

                npt_loc += delta;

                n = new Instruction(elem.Region(), npt_loc, Instruction.ACTION.VECTOR);
                if (n != null)
                {
                    n.SetSpeed(750);
                    elem.AddNavPoint(n);
                }
            }

            n = new Instruction(air_region, new Point(0, 0, 15e3), Instruction.ACTION.VECTOR);
            if (n != null)
            {
                n.SetSpeed(750);
                elem.AddNavPoint(n);
            }
        }
        protected virtual void OrbitalInsertion(MissionElement elem)
        {
            Instruction n = new Instruction(air_region, new Point(0, 0, 30.0e3), Instruction.ACTION.VECTOR);
            if (n != null)
            {
                n.SetSpeed(750);
                elem.AddNavPoint(n);
            }
        }

        protected virtual MissionElement CreateSingleElement(CombatGroup g, CombatUnit u)
        {
            if (g == null || g.IsReserve()) return null;
            if (u == null || u.LiveCount() < 1) return null;

            // make sure this unit is actually in the right star system:
            Galaxy galaxy = Galaxy.GetInstance();
            if (galaxy != null)
            {
                if (galaxy.FindSystemByRegion(u.GetRegion()) !=
                        galaxy.FindSystemByRegion(squadron.GetRegion()))
                {
                    return null;
                }
            }

            // make sure this unit isn't already in the mission:
            foreach (MissionElement elem2 in mission.GetElements())
            {

                if (elem2.GetCombatUnit() == u)
                    return null;
            }

            MissionElement elem = new MissionElement();
            if (elem == null)
            {
                Exit();
                return null;
            }

            if (u.Name().Length != 0)
                elem.SetName(u.Name());
            else
                elem.SetName(u.DesignName());

            elem.SetElementID(pkg_id++);

            elem.SetDesign(u.GetDesign());
            elem.SetCount(u.LiveCount());
            elem.SetIFF(u.GetIFF());
            elem.SetIntelLevel(g.IntelLevel());
            elem.SetRegion(u.GetRegion());
            elem.SetHeading(u.GetHeading());

            int unit_index = g.GetUnits().IndexOf(u);
            Point base_loc = u.Location();
            bool exact = u.IsStatic(); // exact unit-level placement

            if (base_loc.Length < 1)
            {
                base_loc = g.Location();
                exact = false;
            }

            if (unit_index < 0 || unit_index > 0 && !exact)
            {
                Point loc = RandomHelper.RandomDirection();

                if (!u.IsStatic())
                {
                    while (Math.Abs(loc.Y) > Math.Abs(loc.X))
                        loc = RandomHelper.RandomDirection();

                    loc *= 10e3 + 9e3 * unit_index;
                }
                else
                {
                    loc *= 2e3 + 2e3 * unit_index;
                }

                elem.SetLocation(base_loc + loc);
            }
            else
            {
                elem.SetLocation(base_loc);
            }

            if (g.Type() == CombatGroup.GROUP_TYPE.CARRIER_GROUP)
            {
                if (u.Type() == CLASSIFICATION.CARRIER)
                {
                    elem.SetMissionRole(Mission.TYPE.FLIGHT_OPS);

                    if (squadron != null && elem.GetCombatGroup() == squadron.FindCarrier())
                        carrier_elem = elem;

                    else if (carrier_elem == null && u.GetIFF() == squadron.GetIFF())
                        carrier_elem = elem;
                }
                else
                {
                    elem.SetMissionRole(Mission.TYPE.ESCORT);
                }
            }
            else if (u.Type() == SimElements.CLASSIFICATION.STATION ||
                    u.Type() == SimElements.CLASSIFICATION.STARBASE)
            {
                elem.SetMissionRole(Mission.TYPE.FLIGHT_OPS);

                if (squadron != null && elem.GetCombatGroup() == squadron.FindCarrier())
                {
                    carrier_elem = elem;

                    if (u.Type() == CLASSIFICATION.STARBASE)
                        airbase = true;
                }
            }
            else if (u.Type() == CLASSIFICATION.FARCASTER)
            {
                elem.SetMissionRole(Mission.TYPE.OTHER);

                // link farcaster to other terminus:
                string name = u.Name();
                int dash = -1;

                for (int i = 0; i < (int)name.Length; i++)
                    if (name[i] == '-')
                        dash = i;

                string src = name.Substring(0, dash);
                string dst = name.Substring(dash + 1, name.Length - (dash + 1));

                Instruction obj = new Instruction(Instruction.ACTION.VECTOR, dst + "-" + src);
                elem.AddObjective(obj);
            }
            else if ((u.Type() & CLASSIFICATION.STARSHIPS) != 0)
            {
                elem.SetMissionRole(Mission.TYPE.FLEET);
            }

            elem.SetCombatGroup(g);
            elem.SetCombatUnit(u);

            return elem;
        }

        protected virtual MissionElement CreateFighterPackage(CombatGroup squadron, int count, Mission.TYPE role)
        {
            if (squadron == null) return null;

            CombatUnit fighter = squadron.GetUnits()[0];
            CombatUnit carrier = FindCarrier(squadron);

            if (fighter == null)
                return null;

            int avail = fighter.LiveCount();
            int actual = count;

            if (avail < actual)
                actual = avail;

            if (avail < 1)
            {
                ErrLogger.PrintLine("CMF - Insufficient fighters in squadron '{0}' - {1} required, {2} available",
                squadron.Name(), count, avail);
                return null;
            }

            MissionElement elem = new MissionElement();
            if (elem == null)
            {
                Exit();
                return null;
            }

            elem.SetName(Callsign.GetCallsign(fighter.GetIFF()));
            elem.SetElementID(pkg_id++);

            if (carrier != null)
            {
                elem.SetCommander(carrier.Name());
                elem.SetHeading(carrier.GetHeading());
            }
            else
            {
                elem.SetHeading(fighter.GetHeading());
            }

            elem.SetDesign(fighter.GetDesign());
            elem.SetCount(actual);
            elem.SetIFF(fighter.GetIFF());
            elem.SetIntelLevel(squadron.IntelLevel());
            elem.SetRegion(fighter.GetRegion());
            elem.SetSquadron(squadron.Name());
            elem.SetMissionRole(role);

            switch (role)
            {
                case Mission.TYPE.ASSAULT:
                    if (request.GetObjective() != null &&
                            request.GetObjective().Type() == CombatGroup.GROUP_TYPE.MINEFIELD)
                        elem.Loadouts().Add(new MissionLoad(-1, "Rockets"));
                    else
                        elem.Loadouts().Add(new MissionLoad(-1, "Ship Strike"));
                    break;

                case Mission.TYPE.STRIKE:
                    elem.Loadouts().Add(new MissionLoad(-1, "Ground Strike"));
                    break;

                default:
                    elem.Loadouts().Add(new MissionLoad(-1, "ACM Medium Range"));
                    break;
            }

            if (carrier != null)
            {
                Point offset = RandomHelper.RandomPoint() * 0.3;
                offset.Y = Math.Abs(offset.Y);
                offset.Z += 2e3;
                elem.SetLocation(carrier.Location() + offset);
            }
            else
            {
                elem.SetLocation(fighter.Location() + RandomHelper.RandomPoint());
            }

            elem.SetCombatGroup(squadron);
            elem.SetCombatUnit(fighter);

            return elem;
        }
        protected virtual CombatGroup FindSquadron(int iff, CombatGroup.GROUP_TYPE type)
        {
            if (squadron == null) return null;

            CombatGroup result = null;
            Campaign campaign = Campaign.GetCampaign();

            if (campaign != null)
            {
                foreach (Combatant combatant in campaign.GetCombatants())
                {
                    if (result != null) break;
                    if (combatant.GetIFF() == iff)
                    {
                        result = FindCombatGroup(combatant.GetForce(), type);

                        if (result != null && result.CountUnits() < 1)
                        {
                            result = null;
                        }
                    }
                }
            }

            return result;
        }
        protected virtual CombatUnit FindCarrier(CombatGroup g)
        {
            CombatGroup carrier = g.FindCarrier();

            if (carrier != null && carrier.GetUnits().Count != 0)
            {
                MissionElement carrier_elem = mission.FindElement(carrier.Name());

                if (carrier_elem != null)
                    return carrier.GetUnits()[0];
            }

            return null;
        }


        protected virtual void DefineMissionObjectives()
        {
            if (mission == null || player_elem == null) return;

            if (prime_target != null) mission.SetTarget(prime_target);
            if (ward != null) mission.SetWard(ward);

            string objectives = "";

            for (int i = 0; i < player_elem.Objectives().Count; i++)
            {
                Instruction obj = player_elem.Objectives()[i];
                objectives += "* ";
                objectives += obj.GetDescription();
                objectives += ".\n";
            }

            mission.SetObjective(objectives);
        }
        protected virtual MissionInfo DescribeMission()
        {
            if (mission == null || player_elem == null) return null;

            string name;
            string player_info = null;

            if (mission_info != null && mission_info.name.Length != 0)
                name = string.Format("MSN-%03d %s", mission.Identity(), mission_info.name);
            else if (ward != null)
                name = string.Format("MSN-%03d %s %s", mission.Identity(), Game.GetText(mission.TypeName()), ward.Name());
            else if (prime_target != null)
                name = string.Format("MSN-%03d %s %s %s", mission.Identity(), Game.GetText(mission.TypeName()),
                            Ship.ClassName(prime_target.GetDesign().type),
                            prime_target.Name());
            else
                name = string.Format("MSN-%03d %s", mission.Identity(), Game.GetText(mission.TypeName()));

            if (player_elem != null)
            {
                player_info = string.Format("%d x %s %s '%s'", player_elem.Count(), player_elem.GetDesign().abrv,
                                                                player_elem.GetDesign().name,
                                                                player_elem.Name());
            }

            MissionInfo info = new MissionInfo();

            info.id = mission.Identity();
            info.mission = mission;
            info.name = name;
            info.type = mission.Type();
            info.player_info = player_info;
            info.description = mission.Objective();
            info.start = mission.Start();

            if (mission.GetStarSystem() != null)
                info.system = mission.GetStarSystem().Name();
            info.region = mission.GetRegion();

            mission.SetName(name);

            return info;
        }
        protected virtual void Exit()
        {
            Starshatter stars = Starshatter.GetInstance();
            if (stars != null)
                stars.SetGameMode(Starshatter.MODE.MENU_MODE);
        }

        private static CombatGroup FindCombatGroup(CombatGroup g, CombatGroup.GROUP_TYPE type)
        {
            if (g.IntelLevel() <= Intel.INTEL_TYPE.RESERVE)
                return null;

            if (g.GetUnits().Count > 0)
            {
                for (int i = 0; i < g.GetUnits().Count; i++)
                {
                    CombatUnit u = g.GetUnits()[i];
                    if (g.Type() == type && u.LiveCount() > 0)
                        return g;
                }
            }

            CombatGroup result = null;

            foreach (CombatGroup subgroup in g.GetComponents())
            {
                if (result != null) break;
                result = FindCombatGroup(subgroup, type);
            }
            return result;
        }

        protected Campaign campaign;
        protected CampaignMissionRequest request;
        protected MissionInfo mission_info;

        protected CombatGroup squadron;
        protected CombatGroup strike_group;
        protected CombatGroup strike_target;
        protected Mission mission;
        protected MissionElement player_elem;
        protected MissionElement carrier_elem;
        protected MissionElement ward;
        protected MissionElement prime_target;
        protected MissionElement escort;
        protected string air_region;
        protected string orb_region;
        protected bool airborne;
        protected bool airbase;
        protected int ownside;
        protected int enemy;
        protected Mission.TYPE mission_type;
        private static int pkg_id = 1000;
    }
}
