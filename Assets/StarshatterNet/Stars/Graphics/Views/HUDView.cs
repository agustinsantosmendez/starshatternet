﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Hoop.h/Hoop.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    ILS Hoop (HUD display) class
*/
using System;
using System.IO;
using System.Linq;
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Audio;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.AI;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using POINT = UnityEngine.Vector2;

namespace StarshatterNet.Stars.Views
{
#warning HUDView class is still in development and is not recommended for production.
    public class HUDView : View, ISimObserver
    {
        public const int MAX_CONTACT = 50;
        public enum TXT
        {
            TXT_CAUTION_TXT = 0,
            TXT_LAST_CAUTION = 23,
            TXT_CAM_ANGLE,
            TXT_CAM_MODE,
            TXT_PAUSED,
            TXT_GEAR_DOWN,

            TXT_HUD_MODE,
            TXT_PRIMARY_WEP,
            TXT_SECONDARY_WEP,
            TXT_DECOY,
            TXT_SHIELD,
            TXT_AUTO,
            TXT_SHOOT,
            TXT_NAV_INDEX,
            TXT_NAV_ACTION,
            TXT_NAV_FORMATION,
            TXT_NAV_SPEED,
            TXT_NAV_ETR,
            TXT_NAV_HOLD,

            TXT_SPEED,
            TXT_RANGE,
            TXT_CLOSING_SPEED,
            TXT_THREAT_WARN,
            TXT_COMPASS,
            TXT_HEADING,
            TXT_PITCH,
            TXT_ALTITUDE,
            TXT_GFORCE,
            TXT_MISSILE_T1,
            TXT_MISSILE_T2,
            TXT_ICON_SHIP_TYPE,
            TXT_ICON_TARGET_TYPE,
            TXT_TARGET_NAME,
            TXT_TARGET_DESIGN,
            TXT_TARGET_SHIELD,
            TXT_TARGET_HULL,
            TXT_TARGET_SUB,
            TXT_TARGET_ETA,

            TXT_MSG_1,
            TXT_MSG_2,
            TXT_MSG_3,
            TXT_MSG_4,
            TXT_MSG_5,
            TXT_MSG_6,

            TXT_NAV_PT,
            TXT_SELF,
            TXT_SELF_NAME,
            TXT_CONTACT_NAME,
            TXT_CONTACT_INFO = TXT_CONTACT_NAME + MAX_CONTACT,
            TXT_LAST = TXT_CONTACT_INFO + MAX_CONTACT,

            TXT_LAST_ACTIVE = TXT_NAV_HOLD,
            TXT_INSTR_PAGE = TXT_CAUTION_TXT + 6,
        }

        public HUDView(Window w, string name = "HUDView") : base(w, name)
        {
            projector = null; camview = null;
            sim = null; ship = null; target = null; mode = HUDModes.HUD_MODE_TAC;
            tactical = false; overlay = 0; cockpit_hud_texture = null;
            threat = 0; active_region = null; transition = false; docking = false;
            az_ring = null; az_pointer = null; el_ring = null; el_pointer = null; compass_scale = 1;
            show_warn = false; show_inst = false; inst_page = 0;

            hud_view = this;

            sim = Sim.GetSim();

            if (sim != null)
                sim.ShowGrid(false);

            width = window.Width();
            height = window.Height();
            xcenter = (width / 2.0) - 0.5;
            ycenter = (height / 2.0) + 0.5;

            PrepareBitmap("HUDleftA", out hud_left_air);
            PrepareBitmap("HUDrightA", out hud_right_air);
            PrepareBitmap("HUDleft", out hud_left_fighter);
            PrepareBitmap("HUDright", out hud_right_fighter);
            PrepareBitmap("HUDleft1", out hud_left_starship);
            PrepareBitmap("HUDright1", out hud_right_starship);
            PrepareBitmap("INSTR_left", out instr_left);
            PrepareBitmap("INSTR_right", out instr_right);
            PrepareBitmap("CAUTION_left", out warn_left);
            PrepareBitmap("CAUTION_right", out warn_right);
            PrepareBitmap("hud_icon", out icon_ship);
            PrepareBitmap("hud_icon", out icon_target);

            PrepareBitmap("lead", out lead);
            PrepareBitmap("cross", out cross);
            PrepareBitmap("cross1", out cross1);
            PrepareBitmap("cross2", out cross2);
            PrepareBitmap("cross3", out cross3);
            PrepareBitmap("cross4", out cross4);
            PrepareBitmap("fpm", out fpm);
            PrepareBitmap("hpm", out hpm);
            PrepareBitmap("chase_l", out chase_left);
            PrepareBitmap("chase_r", out chase_right);
            PrepareBitmap("chase_t", out chase_top);
            PrepareBitmap("chase_b", out chase_bottom);
            PrepareBitmap("ladder1", out pitch_ladder_pos);
            PrepareBitmap("ladder2", out pitch_ladder_neg);

            hud_left_air.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            hud_right_air.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            hud_left_fighter.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            hud_right_fighter.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            hud_left_starship.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            hud_right_starship.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            instr_left.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            instr_right.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            warn_left.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            warn_right.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            icon_ship.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            icon_target.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            fpm.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            hpm.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            lead.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            cross.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            cross1.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            cross2.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            cross3.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            cross4.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            chase_left.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            chase_right.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            chase_top.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            chase_bottom.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            pitch_ladder_pos.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            pitch_ladder_neg.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);

            hud_left_air_sprite = new SpriteGraphic(hud_left_air, window.Canvas);
            hud_right_air_sprite = new SpriteGraphic(hud_right_air, window.Canvas);
            hud_left_fighter_sprite = new SpriteGraphic(hud_left_fighter, window.Canvas);
            hud_right_fighter_sprite = new SpriteGraphic(hud_right_fighter, window.Canvas);
            hud_left_sprite = hud_left_fighter_sprite;
            hud_right_sprite = hud_right_fighter_sprite;
            hud_left_starship_sprite = new SpriteGraphic(hud_left_starship, window.Canvas);
            hud_right_starship_sprite = new SpriteGraphic(hud_right_starship, window.Canvas);
            instr_left_sprite = new SpriteGraphic(instr_left, window.Canvas);
            instr_right_sprite = new SpriteGraphic(instr_right, window.Canvas);
            warn_left_sprite = new SpriteGraphic(warn_left, window.Canvas);
            warn_right_sprite = new SpriteGraphic(warn_right, window.Canvas);
            icon_ship_sprite = new SpriteGraphic(icon_ship, window.Canvas);
            icon_target_sprite = new SpriteGraphic(icon_target, window.Canvas);
            fpm_sprite = new SpriteGraphic(fpm, window.Canvas);
            hpm_sprite = new SpriteGraphic(hpm, window.Canvas);
            lead_sprite = new SpriteGraphic(lead, window.Canvas);
            aim_sprite = new SpriteGraphic(cross, window.Canvas);
            tgt1_sprite = new SpriteGraphic(cross1, window.Canvas);
            tgt2_sprite = new SpriteGraphic(cross2, window.Canvas);
            tgt3_sprite = new SpriteGraphic(cross3, window.Canvas);
            tgt4_sprite = new SpriteGraphic(cross4, window.Canvas);
            chase_left_sprite = new SpriteGraphic(chase_left, window.Canvas);
            chase_right_sprite = new SpriteGraphic(chase_right, window.Canvas);
            chase_top_sprite = new SpriteGraphic(chase_top, window.Canvas);
            chase_bottom_sprite = new SpriteGraphic(chase_bottom, window.Canvas);

            hud_sprite[0] = hud_left_sprite;
            hud_sprite[1] = hud_right_sprite;
            hud_sprite[2] = instr_left_sprite;
            hud_sprite[3] = instr_right_sprite;
            hud_sprite[4] = warn_left_sprite;
            hud_sprite[5] = warn_right_sprite;
            hud_sprite[6] = icon_ship_sprite;
            hud_sprite[7] = icon_target_sprite;
            hud_sprite[8] = fpm_sprite;
            hud_sprite[9] = hpm_sprite;
            hud_sprite[10] = lead_sprite;
            hud_sprite[11] = aim_sprite;
            hud_sprite[12] = tgt1_sprite;
            hud_sprite[13] = tgt2_sprite;
            hud_sprite[14] = tgt3_sprite;
            hud_sprite[15] = tgt4_sprite;
            hud_sprite[16] = chase_left_sprite;

            float[] pitch_ladder_UV = { 0.125f, 0.0625f, 0.875f, 0.0625f, 0.875f, 0, 0.125f, 0 };
            float[] UV = new float[8];
            const float x1 = 0.125f;
            const float x2 = 0.875f;
            const float y1 = 0.0625f;

            for (int i = 0; i < 15; i++)
            {
                pitch_ladder[i] = new SpriteGraphic(pitch_ladder_pos, window.Canvas);

                //CopyMemory(UV, pitch_ladder_UV, sizeof(UV));
                Array.Copy(pitch_ladder_UV, UV, UV.Length);
                UV[1] = UV[3] = (pitch_ladder_UV[1] * (i));
                UV[5] = UV[7] = (pitch_ladder_UV[1] * (i + 1));

                pitch_ladder[i].Reshape(192, 16);
                //pitch_ladder[i].SetTexCoords(UV);
                pitch_ladder[i].SetTexCoords(x1, y1 * i, x2 - x1, y1);
                pitch_ladder[i].SetBlendMode(2);
                pitch_ladder[i].Hide();
            }

            // zero mark at i=15
            {
                int i = 15;
                pitch_ladder[i] = new SpriteGraphic(pitch_ladder_pos, window.Canvas);

                UV[0] = UV[6] = 0;
                UV[2] = UV[4] = 1;
                UV[1] = UV[3] = (pitch_ladder_UV[1] * (i + 1));
                UV[5] = UV[7] = (pitch_ladder_UV[1] * (i));

                pitch_ladder[i].Reshape(256, 16);
                //pitch_ladder[i].SetTexCoords(UV);
                pitch_ladder[i].SetTexCoords(0, y1 * i, 1, -y1);
                pitch_ladder[i].SetBlendMode(2);
                pitch_ladder[i].Hide();
            }

            for (int i = 16; i < 31; i++)
            {
                pitch_ladder[i] = new SpriteGraphic(pitch_ladder_neg, window.Canvas);

                //CopyMemory(UV, pitch_ladder_UV, sizeof(UV));
                Array.Copy(pitch_ladder_UV, UV, UV.Length);
                UV[1] = UV[3] = (pitch_ladder_UV[1] * (30 - i));
                UV[5] = UV[7] = (pitch_ladder_UV[1] * (30 - i + 1));

                pitch_ladder[i].Reshape(192, 16);
                //pitch_ladder[i].SetTexCoords(UV);
                pitch_ladder[i].SetTexCoords(x1, y1 * (30 - i), x2 - x1, y1);
                pitch_ladder[i].SetBlendMode(2);
                pitch_ladder[i].Hide();
            }

            for (int i = 0; i < 3; i++)
                mfd[i] = new MFD(window, (MFD.Modes)i);

            mfd[0].SetRect(new Rect(8, height - 136, 128, 128));
            mfd[1].SetRect(new Rect(width - 136, height - 136, 128, 128));
            mfd[2].SetRect(new Rect(8, 8, 128, 128));

            hud_left_air_sprite.MoveTo(new Point(width / 2 - 128, height / 2, 1));
            hud_right_air_sprite.MoveTo(new Point(width / 2 + 128, height / 2, 1));
            hud_left_air_sprite.SetBlendMode(2);
            hud_left_air_sprite.SetFilter(0);
            hud_right_air_sprite.SetBlendMode(2);
            hud_right_air_sprite.SetFilter(0);

            hud_left_fighter_sprite.MoveTo(new Point(width / 2 - 128, height / 2, 1));
            hud_right_fighter_sprite.MoveTo(new Point(width / 2 + 128, height / 2, 1));
            hud_left_fighter_sprite.SetBlendMode(2);
            hud_left_fighter_sprite.SetFilter(0);
            hud_right_fighter_sprite.SetBlendMode(2);
            hud_right_fighter_sprite.SetFilter(0);

            hud_left_starship_sprite.MoveTo(new Point(width / 2 - 128, height / 2, 1));
            hud_right_starship_sprite.MoveTo(new Point(width / 2 + 128, height / 2, 1));
            hud_left_starship_sprite.SetBlendMode(2);
            hud_left_starship_sprite.SetFilter(0);
            hud_right_starship_sprite.SetBlendMode(2);
            hud_right_starship_sprite.SetFilter(0);

            instr_left_sprite.MoveTo(new Point(width / 2 - 128, height - 128, 1));
            instr_right_sprite.MoveTo(new Point(width / 2 + 128, height - 128, 1));
            instr_left_sprite.SetBlendMode(2);
            instr_left_sprite.SetFilter(0);
            instr_right_sprite.SetBlendMode(2);
            instr_right_sprite.SetFilter(0);

            warn_left_sprite.MoveTo(new Point(width / 2 - 128, height - 128, 1));
            warn_right_sprite.MoveTo(new Point(width / 2 + 128, height - 128, 1));
            warn_left_sprite.SetBlendMode(2);
            warn_left_sprite.SetFilter(0);
            warn_right_sprite.SetBlendMode(2);
            warn_right_sprite.SetFilter(0);

            icon_ship_sprite.MoveTo(new Point(184, height - 72, 1));
            icon_target_sprite.MoveTo(new Point(width - 184, height - 72, 1));
            icon_ship_sprite.SetBlendMode(2);
            icon_ship_sprite.SetFilter(0);
            icon_target_sprite.SetBlendMode(2);
            icon_target_sprite.SetFilter(0);

            fpm_sprite.MoveTo(new Point(width / 2, height / 2, 1));
            hpm_sprite.MoveTo(new Point(width / 2, height / 2, 1));
            lead_sprite.MoveTo(new Point(width / 2, height / 2, 1));
            aim_sprite.MoveTo(new Point(width / 2, height / 2, 1));
            tgt1_sprite.MoveTo(new Point(width / 2, height / 2, 1));
            tgt2_sprite.MoveTo(new Point(width / 2, height / 2, 1));
            tgt3_sprite.MoveTo(new Point(width / 2, height / 2, 1));
            tgt4_sprite.MoveTo(new Point(width / 2, height / 2, 1));

            fpm_sprite.SetBlendMode(2);
            hpm_sprite.SetBlendMode(2);
            lead_sprite.SetBlendMode(2);
            aim_sprite.SetBlendMode(2);
            tgt1_sprite.SetBlendMode(2);
            tgt2_sprite.SetBlendMode(2);
            tgt3_sprite.SetBlendMode(2);
            tgt4_sprite.SetBlendMode(2);
            chase_left_sprite.SetBlendMode(2);

            fpm_sprite.SetFilter(0);
            hpm_sprite.SetFilter(0);
            lead_sprite.SetFilter(0);
            aim_sprite.SetFilter(0);
            tgt1_sprite.SetFilter(0);
            tgt2_sprite.SetFilter(0);
            tgt3_sprite.SetFilter(0);
            tgt4_sprite.SetFilter(0);
            chase_left_sprite.SetFilter(0);

            lead_sprite.Hide();
            aim_sprite.Hide();
            tgt1_sprite.Hide();
            tgt2_sprite.Hide();
            tgt3_sprite.Hide();
            tgt4_sprite.Hide();
            chase_left_sprite.Hide();
            chase_right_sprite.Hide();
            chase_top_sprite.Hide();
            chase_bottom_sprite.Hide();

            aw = chase_left.Width() / 2;
            ah = chase_left.Height() / 2;

            mfd[0].SetMode(MFD.Modes.MFD_MODE_SHIP);
            mfd[1].SetMode(MFD.Modes.MFD_MODE_FOV);
            mfd[2].SetMode(MFD.Modes.MFD_MODE_GAME);

            hud_font = FontMgr.Find("HUD");
            big_font = FontMgr.Find("GUI");

            for (int i = 0; i < (int)TXT.TXT_LAST; i++)
            {
                hud_text[i] = new HUDText();
                hud_text[i].font = hud_font;
            }

            hud_text[(int)TXT.TXT_THREAT_WARN].font = big_font;
            hud_text[(int)TXT.TXT_SHOOT].font = big_font;
            hud_text[(int)TXT.TXT_AUTO].font = big_font;

            SetHUDColorSet(def_color_set);
            MFD.SetColor(standard_hud_colors[color]);

            //DataLoader* loader = DataLoader::GetLoader();
            //loader.SetDataPath("HUD/");
            string path = "HUD/";

            az_ring = new Solid();
            az_pointer = new Solid();
            el_ring = new Solid();
            el_pointer = new Solid();

            az_ring.Load(path + "CompassRing", compass_scale);
            az_pointer.Load(path + "CompassPointer", compass_scale);
            el_ring.Load(path + "PitchRing.mag", compass_scale);
            el_pointer.Load(path + "CompassPointer", compass_scale);

            missile_lock_sound = Sound.CreateStream("Sounds/MissileLock.wav", Sound.FlagEnum.LOOP | Sound.FlagEnum.LOCKED);

            //loader.SetDataPath(0);

            for (int i = 0; i < MAX_MSG; i++)
                msg_time[i] = 0;
        }

        public static void Initialize()
        {
            if (prefabHUDView != null) return;
            prefabHUDView = Resources.Load("Prefabs/HUD Canvas") as GameObject;
            HUDText.Initialize();
            ErrLogger.PrintLine("HUDView.Initialize is not yet implemented");
        }

        public enum HUDModes { HUD_MODE_OFF, HUD_MODE_TAC, HUD_MODE_NAV, HUD_MODE_ILS };

        // Operations:
        public virtual void ExecFrame()
        {
            // update the position of HUD elements that are
            // part of the 3D scene (like fpm and lcos sprites)
            HideCompass();

            if (ship != null && !transition && !docking && mode != HUDModes.HUD_MODE_OFF)
            {
                Player p = Player.GetCurrentPlayer();
                gunsight = p.Gunsight();

                if (ship.IsStarship())
                {
                    if (tactical)
                    {
                        hud_left_sprite.Hide();
                        hud_right_sprite.Hide();
                    }

                    else if (hud_left_sprite.Frame() != hud_left_starship)
                    {
                        //hud_left_starship_sprite.SetAnimation(hud_left_starship);
                        //hud_right_starship_sprite.SetAnimation(hud_right_starship);
                        hud_left_sprite = hud_left_starship_sprite;
                        hud_right_sprite = hud_right_starship_sprite;

                        hud_left_starship_sprite.MoveTo(new Point(width / 2 - 128, height / 2, 1));
                        hud_right_starship_sprite.MoveTo(new Point(width / 2 + 128, height / 2, 1));
                    }
                }

                else if (!ship.IsStarship())
                {
                    if (ship.IsAirborne() && hud_left_sprite.Frame() != hud_left_air)
                    {
                        //hud_left_air_sprite.SetAnimation(hud_left_air);
                        //hud_right_air_sprite.SetAnimation(hud_right_air);
                        hud_left_sprite = hud_left_air_sprite;
                        hud_right_sprite = hud_right_air_sprite;
                    }

                    else if (!ship.IsAirborne() && hud_left_sprite.Frame() != hud_left_fighter)
                    {
                        //hud_left_fighter_sprite.SetAnimation(hud_left_fighter);
                        //hud_right_fighter_sprite.SetAnimation(hud_right_fighter);
                        hud_left_sprite = hud_left_fighter_sprite;
                        hud_right_sprite = hud_right_fighter_sprite;
                    }
                }

                if (!tactical)
                {
                    if (Game.MaxTexSize() > 128)
                    {
                        hud_left_sprite.Show();
                        hud_right_sprite.Show();
                    }

                    if (!arcade)
                        DrawFPM();

                    if (ship.IsStarship() && ship.GetFLCSMode() == (int)FLCS_MODE.FLCS_HELM)
                        DrawHPM();
                    else if (!arcade)
                        DrawPitchLadder();
                }

                else
                {
                    if (ship.IsStarship() && ship.GetFLCSMode() == (int)FLCS_MODE.FLCS_HELM)
                        DrawCompass();
                }

                if (mode == HUDModes.HUD_MODE_TAC)
                {
                    DrawSight();
                    DrawDesignators();
                }

                if (width > 640 || (!show_inst && !show_warn))
                {
                    icon_ship_sprite.Show();
                    icon_target_sprite.Show();
                }
                else
                {
                    icon_ship_sprite.Hide();
                    icon_target_sprite.Hide();
                }
            }

            // if the hud is off or prohibited,
            // hide all of the sprites:

            else
            {
                hud_left_sprite.Hide();
                hud_right_sprite.Hide();
                instr_left_sprite.Hide();
                instr_right_sprite.Hide();
                warn_left_sprite.Hide();
                warn_right_sprite.Hide();
                icon_ship_sprite.Hide();
                icon_target_sprite.Hide();
                fpm_sprite.Hide();
                hpm_sprite.Hide();
                lead_sprite.Hide();
                aim_sprite.Hide();
                tgt1_sprite.Hide();
                tgt2_sprite.Hide();
                tgt3_sprite.Hide();
                tgt4_sprite.Hide();
                chase_left_sprite.Hide();
                chase_right_sprite.Hide();
                chase_top_sprite.Hide();
                chase_bottom_sprite.Hide();

                for (int i = 0; i < 3; i++)
                    mfd[i].Hide();

                for (int i = 0; i < 31; i++)
                    pitch_ladder[i].Hide();

                DrawILS();
            }
        }
        public virtual void UseCameraView(CameraView v)
        {
            if (v != null && camview == null)
            {
                camview = v;
                for (int i = 0; i < 3; i++)
                    mfd[i].UseCameraView(camview);

                projector = camview.GetProjector();

            }
        }

        public virtual Ship GetShip() { return ship; }
        public virtual SimObject GetTarget() { return target; }
        public virtual void SetShip(Ship s)
        {
            if (ship != s)
            {
                double new_scale = 1;

                ship_status = (ShipSystem.STATUS)(-1);
                ship = s;

                if (ship != null)
                {
                    if (ship.Life() == 0 || ship.IsDying() || ship.IsDead())
                    {
                        ship = null;
                    }
                    else
                    {
                        Observe(ship);
                        new_scale = 1.1 * ship.Radius() / 64;

                        if (ship.Design().hud_icon.Width() != 0)
                        {
                            TransferBitmap(ship.Design().hud_icon, icon_ship);
                            ColorizeBitmap(icon_ship_sprite, txt_color);
                        }
                    }
                }

                if (az_ring != null)
                {
                    az_ring.Rescale(1 / compass_scale);
                    az_ring.Rescale(new_scale);
                }

                if (az_pointer != null)
                {
                    az_pointer.Rescale(1 / compass_scale);
                    az_pointer.Rescale(new_scale);
                }

                if (el_ring != null)
                {
                    el_ring.Rescale(1 / compass_scale);
                    el_ring.Rescale(new_scale);
                }

                if (el_pointer != null)
                {
                    el_pointer.Rescale(1 / compass_scale);
                    el_pointer.Rescale(new_scale);
                }

                compass_scale = new_scale;
                inst_page = 0;

                if (ship != null && ship.GetElement() != null && ship.GetElement().NumInstructions() > 0)
                    if (!show_inst)
                        CycleHUDInst();
            }

            else if (ship != null && ship.Design().hud_icon.Width() != 0)
            {
                bool update = false;
                ShipSystem.STATUS s2 = ShipSystem.STATUS.NOMINAL;
                int integrity = (int)(ship.Integrity() / ship.Design().integrity * 100);

                if (integrity < 30) s2 = ShipSystem.STATUS.CRITICAL;
                else if (integrity < 60) s2 = ShipSystem.STATUS.DEGRADED;

                if (s2 != ship_status)
                {
                    ship_status = s2;
                    update = true;
                }

                if (update)
                {
                    SetStatusColor((ShipSystem.STATUS)ship_status);
                    ColorizeBitmap(icon_ship_sprite, status_color);
                }
            }

            if (ship != null && ship.Cockpit() != null)
            {
                Solid cockpit = (Solid)ship.Cockpit();

                bool change = false;

                if (cockpit.Hidden())
                {
                    if (cockpit_hud_texture != null)
                        change = true;

                    cockpit_hud_texture = null;
                }
                else
                {
                    if (cockpit_hud_texture == null)
                        change = true;

                    Model cockpit_model = cockpit.GetModel();
                    Material hud_material = null;

                    if (cockpit_model != null)
                    {
                        hud_material = (Material)cockpit_model.FindMaterial("HUD");
                        if (hud_material != null)
                        {
                            cockpit_hud_texture = hud_material.tex_emissive;
                        }
                    }
                }

                if (change)
                {
                    SetHUDColorSet(color);
                }
            }
        }
        public virtual void SetTarget(SimObject t)
        {
            bool update = false;

            if (target != t)
            {
                tgt_status = (ShipSystem.STATUS)(-1);
                target = t;
                if (target != null) Observe(target);
                update = true;
            }

            if (target != null && target.Type() == (int)SimObject.TYPES.SIM_SHIP)
            {
                ShipSystem.STATUS s = ShipSystem.STATUS.NOMINAL;
                Ship tship = (Ship)target;
                int integrity = (int)(tship.Integrity() / tship.Design().integrity * 100);

                if (integrity < 30) s = ShipSystem.STATUS.CRITICAL;
                else if (integrity < 60) s = ShipSystem.STATUS.DEGRADED;

                if (s != tgt_status)
                {
                    tgt_status = s;
                    update = true;
                }
            }

            if (update)
            {
                if (target != null && target.Type() == (int)SimObject.TYPES.SIM_SHIP)
                {
                    Ship tship = (Ship)target;
                    TransferBitmap(tship.Design().hud_icon, icon_target);
                }
                else
                {
                    PrepareBitmap("hud_icon", out icon_target);
                }

                SetStatusColor((ShipSystem.STATUS)tgt_status);
                ColorizeBitmap(icon_target_sprite, status_color);
            }
        }
        public virtual MFD GetMFD(int n)
        {
            if (n >= 0 && n < 3)
                return mfd[n];

            return null;
        }


        public virtual void HideAll()
        {
            for (int i = 0; i < 3; i++)
                mfd[i].Hide();

            hud_left_sprite.Hide();
            hud_right_sprite.Hide();
            instr_left_sprite.Hide();
            instr_right_sprite.Hide();
            warn_left_sprite.Hide();
            warn_right_sprite.Hide();
            icon_ship_sprite.Hide();
            icon_target_sprite.Hide();
            fpm_sprite.Hide();
            hpm_sprite.Hide();
            lead_sprite.Hide();
            aim_sprite.Hide();
            tgt1_sprite.Hide();
            tgt2_sprite.Hide();
            tgt3_sprite.Hide();
            tgt4_sprite.Hide();
            chase_left_sprite.Hide();
            chase_right_sprite.Hide();
            chase_top_sprite.Hide();
            chase_bottom_sprite.Hide();

            if (sim != null)
                sim.ShowGrid(false);

            for (int i = 0; i < 31; i++)
                pitch_ladder[i].Hide();

            if (missile_lock_sound != null)
                missile_lock_sound.Stop();

            HideCompass();
            DrawILS();
            Mouse.Show(false);
        }
        public virtual void DrawBars()
        {
            fpm_sprite.Hide();
            hpm_sprite.Hide();
            lead_sprite.Hide();
            aim_sprite.Hide();
            tgt1_sprite.Hide();
            tgt2_sprite.Hide();
            tgt3_sprite.Hide();
            tgt4_sprite.Hide();
            chase_left_sprite.Hide();
            chase_right_sprite.Hide();
            chase_top_sprite.Hide();
            chase_bottom_sprite.Hide();

            for (int i = 0; i < 31; i++)
                pitch_ladder[i].Hide();

            const int bar_width = 256;
            const int bar_height = 192;
            const int box_width = 120;

            int cx = width / 2;
            int cy = height / 2;
            int l = cx - bar_width / 2;
            int r = cx + bar_width / 2;
            int t = cy - bar_height / 2;
            int b = cy + bar_height / 2;
            TextFormat align = TextFormat.DT_LEFT;

            if (Game.Paused())
                DrawHUDText(TXT.TXT_PAUSED, Game.GetText("HUDView.PAUSED"), new Rect(cx - 128, cy - 60, 256, 12), TextFormat.DT_CENTER);

            if (ship != null)
            {
                DrawContactMarkers();

                string txt;
                double speed = ship.Velocity().Length;

                if (Point.Dot(ship.Velocity(), ship.Heading()) < 0)
                    speed = -speed;

                FormatUtil.FormatNumber(out txt, speed);

                if (tactical)
                {
                    l = box_width + 16;
                    r = width - box_width - 16;
                }

                Rect speed_rect = new Rect(l - box_width - 8, cy - 5, box_width, 12);

                align = (tactical) ? TextFormat.DT_LEFT : TextFormat.DT_RIGHT;
                DrawHUDText(TXT.TXT_SPEED, txt, speed_rect, align);

                // upper left hud quadrant (airborne fighters)
                if (ship.IsAirborne())
                {
                    double alt_msl = ship.AltitudeMSL();
                    double alt_agl = ship.AltitudeAGL();

                    if (alt_agl <= 1000)
                        txt = string.Format("R {0:D4}", (int)alt_agl);
                    else
                        FormatUtil.FormatNumber(out txt, alt_msl);

                    speed_rect.y -= 20;

                    if (arcade)
                    {
                        string arcade_txt;
                        arcade_txt = string.Format("{0} {1}", Game.GetText("HUDView.altitude"), txt);
                        align = (tactical) ? TextFormat.DT_LEFT : TextFormat.DT_RIGHT;
                        DrawHUDText(TXT.TXT_ALTITUDE, arcade_txt, speed_rect, align);
                    }
                    else
                    {
                        align = (tactical) ? TextFormat.DT_LEFT : TextFormat.DT_RIGHT;
                        DrawHUDText(TXT.TXT_ALTITUDE, txt, speed_rect, align);
                    }

                    if (!arcade)
                    {
                        txt = string.Format("{0:0.0} G", ship.GForce());
                        speed_rect.y -= 20;

                        align = (tactical) ? TextFormat.DT_LEFT : TextFormat.DT_RIGHT;
                        DrawHUDText(TXT.TXT_GFORCE, txt, speed_rect, align);

                        speed_rect.y += 40;
                    }
                }

                // upper left hud quadrant (starships)
                else if (ship.IsStarship() && ship.GetFLCSMode() == (int)FLCS_MODE.FLCS_HELM && !arcade)
                {
                    txt = string.Format("{0}: {1:0.0}", Game.GetText("HUDView.Pitch"), ship.GetHelmPitch() / ConstantsF.DEGREES);
                    speed_rect.y -= 50;

                    align = (tactical) ? TextFormat.DT_LEFT : TextFormat.DT_RIGHT;
                    DrawHUDText(TXT.TXT_PITCH, txt, speed_rect, align);

                    speed_rect.y -= 10;
                    int heading_degrees = (int)(ship.GetHelmHeading() / ConstantsF.DEGREES);
                    if (heading_degrees < 0) heading_degrees += 360;
                    txt = string.Format("{0}: {1:D3}", Game.GetText("HUDView.Heading"), heading_degrees);
                    DrawHUDText(TXT.TXT_HEADING, txt, speed_rect, align);

                    speed_rect.y += 60;
                }

                // per user request, all ships should show compass heading
                if (!tactical && !arcade)
                {
                    Rect heading_rect = new Rect(l, t + 5, bar_width, 12);
                    int heading_degrees = (int)(ship.CompassHeading() / ConstantsF.DEGREES);
                    if (heading_degrees < 0) heading_degrees += 360;
                    txt = string.Format("{0}", heading_degrees);
                    DrawHUDText(TXT.TXT_COMPASS, txt, heading_rect, TextFormat.DT_CENTER);
                }

                switch (mode)
                {
                    case HUDModes.HUD_MODE_TAC: txt = Game.GetText("HUDView.mode.tactical"); break;
                    case HUDModes.HUD_MODE_NAV: txt = Game.GetText("HUDView.mode.navigation"); break;
                    case HUDModes.HUD_MODE_ILS: txt = Game.GetText("HUDView.mode.landing"); break;
                }

                if (tactical)
                {
                    speed_rect.y += 76;
                    align = TextFormat.DT_LEFT;
                }
                else
                {
                    speed_rect.y = cy + 76;
                    align = TextFormat.DT_RIGHT;
                }

                DrawHUDText(TXT.TXT_HUD_MODE, txt, speed_rect, align);

                // landing gear:
                if (ship.IsGearDown())
                {
                    string gear_down = Game.GetText("HUDView.gear-down");

                    Rect gear_rect = new Rect(l, b + 20, box_width, 12);
                    DrawHUDText(TXT.TXT_GEAR_DOWN, gear_down, gear_rect, TextFormat.DT_CENTER, HUD_CASE.HUD_UPPER_CASE, true);
                }

                // sensor/missile lock warnings and quantum drive countdown:
                QuantumDrive quantum = ship.GetQuantumDrive();

                if (threat != 0 || (quantum != null && quantum.JumpTime() > 0))
                {
                    string threat_warn = Game.GetText("HUDView.threat-warn");
                    bool show_msg = true;

                    if (quantum != null && quantum.JumpTime() > 0)
                    {
                        string buf;
                        buf = string.Format("{0}: {1}", Game.GetText("HUDView.quantum-jump"), (int)quantum.JumpTime());
                        threat_warn = buf;
                    }

                    else if (threat > 1)
                    {
                        threat_warn = Game.GetText("HUDView.missile-warn");
                        show_msg = ((Game.RealTime() / 500) & 1) != 0;
                    }

                    if (show_msg)
                    {
                        Rect lock_rect = new Rect(l, t - 25, box_width, 12);
                        DrawHUDText(TXT.TXT_THREAT_WARN, threat_warn, lock_rect, TextFormat.DT_CENTER, HUD_CASE.HUD_MIXED_CASE, true);
                    }
                }

                if (ship.CanTimeSkip())
                {
                    Rect auto_rect = new Rect(l, t - 40, box_width, 12);
                    DrawHUDText(TXT.TXT_AUTO, Game.GetText("HUDView.AUTO"), auto_rect, TextFormat.DT_CENTER, HUD_CASE.HUD_MIXED_CASE, true);
                }

                if (mode == HUDModes.HUD_MODE_NAV)
                {
                    Instruction next = ship.GetNextNavPoint();

                    if (next != null)
                    {
                        double distance = ship.RangeToNavPoint(next);
                        FormatUtil.FormatNumber(out txt, distance);

                        Rect range_rect = new Rect(r - 20, cy - 5, box_width, 12);
                        DrawHUDText(TXT.TXT_RANGE, txt, range_rect, TextFormat.DT_RIGHT);
                        range_rect.Inflate(2, 2);
                    }
                }

                // lower left hud quadrant
                else if (mode == HUDModes.HUD_MODE_TAC)
                {
                    speed_rect.x = l - box_width - 8;
                    speed_rect.y = cy - 5 + 20;
                    speed_rect.w = box_width;
                    align = (tactical) ? TextFormat.DT_LEFT : TextFormat.DT_RIGHT;

                    if (!arcade && ship.GetPrimary() != null && !ship.IsNetObserver())
                        DrawHUDText(TXT.TXT_PRIMARY_WEP, ship.GetPrimary().Abbreviation(), speed_rect, align);

                    WeaponGroup missile = ship.GetSecondaryGroup();

                    if (missile != null && missile.Ammo() > 0 && !ship.IsNetObserver())
                    {
                        if (!arcade)
                        {
                            speed_rect.y = cy - 5 + 30;
                            txt = string.Format("{0} {1}", missile.Name(), missile.Ammo());
                            DrawHUDText(TXT.TXT_SECONDARY_WEP, txt, speed_rect, align);
                        }

                        // missile range indicator
                        if (missile.GetSelected().Locked())
                        {
                            Rect shoot_rect = new Rect(l, b + 5, box_width, 12);
                            DrawHUDText(TXT.TXT_SHOOT, Game.GetText("HUDView.SHOOT"), shoot_rect, TextFormat.DT_CENTER, HUD_CASE.HUD_MIXED_CASE, true);
                        }
                    }

                    if (!arcade && !ship.IsNetObserver())
                    {
                        if (ship.GetShield() != null)
                        {
                            speed_rect.y = cy - 5 + 40;
                            txt = string.Format("{0} - {1:D3} +", Game.GetText("HUDView.SHIELD"), ship.ShieldStrength());
                            DrawHUDText(TXT.TXT_SHIELD, txt, speed_rect, align);
                        }
                        else if (ship.GetDecoy() != null)
                        {
                            speed_rect.y = cy - 5 + 40;
                            txt = string.Format("{0} {1}", Game.GetText("HUDView.DECOY"), ship.GetDecoy().Ammo());
                            DrawHUDText(TXT.TXT_DECOY, txt, speed_rect, align);
                        }


                        Rect eta_rect = speed_rect;
                        eta_rect.y += 10;

                        align = TextFormat.DT_RIGHT;

                        if (tactical)
                        {
                            eta_rect.x = 8;
                            align = TextFormat.DT_LEFT;
                        }

                        for (int i = 0; i < 2; i++)
                        {
                            int eta = ship.GetMissileEta(i);
                            if (eta > 0)
                            {
                                int minutes = (eta / 60) % 60;
                                int seconds = (eta) % 60;

                                string eta_buf;
                                eta_buf = string.Format("T {0}:{1:D2}", minutes, seconds);
                                DrawHUDText(TXT.TXT_MISSILE_T1 + i, eta_buf, eta_rect, align);
                                eta_rect.y += 10;
                            }
                        }
                    }

                    NetGame netgame = NetGame.GetInstance();
                    if (netgame != null && !netgame.IsActive())
                    {
                        Rect shoot_rect = new Rect(l, b + 5, box_width, 12);
                        DrawHUDText(TXT.TXT_SHOOT, Game.GetText("HUDView.NET-GAME-OVER"), shoot_rect, TextFormat.DT_CENTER, HUD_CASE.HUD_MIXED_CASE, true);
                    }

                    else if (ship.IsNetObserver())
                    {
                        Rect shoot_rect = new Rect(l, b + 5, box_width, 12);
                        DrawHUDText(TXT.TXT_SHOOT, Game.GetText("HUDView.OBSERVER"), shoot_rect, TextFormat.DT_CENTER, HUD_CASE.HUD_MIXED_CASE, true);
                    }

                    DrawTarget();
                }

                else if (mode == HUDModes.HUD_MODE_ILS)
                {
                    DrawTarget();
                }

                DrawNavInfo();
            }
        }
        public virtual void DrawNav()
        {
            if (sim == null)
                return;

            active_region = sim.GetActiveRegion();

            if (ship != null)
            {
                int nav_index = 1;
                Instruction next = ship.GetNextNavPoint();

                if (mode == HUDModes.HUD_MODE_NAV)
                {
                    if (next != null && next.Action() == Instruction.ACTION.LAUNCH)
                        DrawNavPoint(next, 0, true);

                    foreach (Instruction navpt in ship.GetFlightPlan())
                    {
                        DrawNavPoint(navpt, nav_index++, (navpt == next));
                    }
                }
                else if (next != null)
                {
                    DrawNavPoint(next, 0, true);
                }
            }
        }
        public virtual void DrawILS()
        {
            if (ship != null)
            {
                bool hoops_drawn = false;
                bool same_sector = false;

                InboundSlot inbound = ship.GetInbound();
                if (inbound != null)
                {
                    FlightDeck fd = inbound.GetDeck();

                    if (fd != null && fd.IsRecoveryDeck() && fd.GetCarrier() != null)
                    {
                        if (fd.GetCarrier().GetRegion() == ship.GetRegion())
                            same_sector = true;

                        if (same_sector && mode == HUDModes.HUD_MODE_ILS && !transition && !docking)
                        {
                            Point dst = fd.MountLocation();
                            projector.Transform(dst);

                            if (dst.Z > 1.0)
                            {
                                projector.Project(dst);

                                int x = (int)dst.X;
                                int y = (int)dst.Y;

                                if (x > 4 && x < width - 4 &&
                                        y > 4 && y < height - 4)
                                {

                                    window.DrawLine(x - 6, y - 6, x + 6, y + 6, hud_color);
                                    window.DrawLine(x + 6, y - 6, x - 6, y + 6, hud_color);
                                }
                            }
                        }

                        // draw the hoops for this flight deck:
                        Scene scene = camview.GetScene();
                        for (int h = 0; h < fd.NumHoops(); h++)
                        {
                            Hoop hoop = fd.GetHoops()[h];
                            if (hoop != null && scene != null)
                            {
                                if (same_sector && mode == HUDModes.HUD_MODE_ILS && !transition && !docking)
                                {
                                    scene.AddGraphic(hoop);
                                    hoop.Show();

                                    hoops_drawn = true;
                                }
                                else
                                {
                                    hoop.Hide();
                                    scene.DelGraphic(hoop);
                                }
                            }
                        }
                    }
                }

                if (!hoops_drawn)
                {
                    foreach (Ship carrier in ship.GetRegion().Carriers())
                    {
                        bool ours = (carrier.GetIFF() == ship.GetIFF()) ||
                       (carrier.GetIFF() == 0);

                        for (int i = 0; i < carrier.NumFlightDecks(); i++)
                        {
                            FlightDeck fd = carrier.GetFlightDeck(i);

                            if (fd != null && fd.IsRecoveryDeck())
                            {
                                if (mode == HUDModes.HUD_MODE_ILS && ours && !transition && !docking)
                                {
                                    Point dst = fd.MountLocation();
                                    projector.Transform(dst);

                                    if (dst.Z > 1.0)
                                    {
                                        projector.Project(dst);

                                        int x = (int)dst.X;
                                        int y = (int)dst.Y;

                                        if (x > 4 && x < width - 4 &&
                                                y > 4 && y < height - 4)
                                        {

                                            window.DrawLine(x - 6, y - 6, x + 6, y + 6, hud_color);
                                            window.DrawLine(x + 6, y - 6, x - 6, y + 6, hud_color);
                                        }
                                    }
                                }

                                // draw the hoops for this flight deck:
                                Scene scene = camview.GetScene();
                                for (int h = 0; h < fd.NumHoops(); h++)
                                {
                                    Hoop hoop = fd.GetHoops()[h];
                                    if (hoop != null && scene != null)
                                    {
                                        if (mode == HUDModes.HUD_MODE_ILS && ours && !transition && !docking)
                                        {
                                            hoop.Show();
                                            if (hoop.GetScene() == null)
                                                scene.AddGraphic(hoop);
                                        }
                                        else
                                        {
                                            hoop.Hide();
                                            if (hoop.GetScene() != null)
                                                scene.DelGraphic(hoop);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public virtual void DrawObjective()
        {
            if (ship != null && ship.GetDirector() != null && ship.GetDirector().Type() >= SteerAI.SteerType.SEEKER)
            {
                SteerAI steer = (SteerAI)ship.GetDirector();
                Point obj = steer.GetObjective();
                projector.Transform(obj);

                if (obj.Z > 1.0)
                {
                    projector.Project(obj);

                    int x = (int)obj.X;
                    int y = (int)obj.Y;

                    if (x > 4 && x < width - 4 &&
                            y > 4 && y < height - 4)
                    {

                        Color c = Color.cyan;
                        window.DrawRect(x - 6, y - 6, x + 6, y + 6, c);
                        window.DrawLine(x - 6, y - 6, x + 6, y + 6, c);
                        window.DrawLine(x + 6, y - 6, x - 6, y + 6, c);
                    }
                }

                if (steer.GetOther() != null)
                {
                    obj = steer.GetOther().Location();
                    projector.Transform(obj);

                    if (obj.Z > 1.0)
                    {
                        projector.Project(obj);

                        int x = (int)obj.X;
                        int y = (int)obj.Y;

                        if (x > 4 && x < width - 4 &&
                                y > 4 && y < height - 4)
                        {

                            Color c = ColorExtensions.Orange;
                            window.DrawRect(x - 6, y - 6, x + 6, y + 6, c);
                            window.DrawLine(x - 6, y - 6, x + 6, y + 6, c);
                            window.DrawLine(x + 6, y - 6, x - 6, y + 6, c);
                        }
                    }
                }
            }
            /***/
        }
        public virtual void DrawNavInfo()
        {
            const int bar_width = 256;
            const int bar_height = 192;
            const int box_width = 120;

            if (arcade)
            {
                if (ship.IsAutoNavEngaged())
                {
                    Rect info_rect = new Rect(width / 2 - box_width, height / 2 + bar_height, box_width * 2, 12);

                    if (big_font != null)
                        hud_text[(int)TXT.TXT_NAV_INDEX].font = big_font;

                    DrawHUDText(TXT.TXT_NAV_INDEX, Game.GetText("HUDView.Auto-Nav"), info_rect, TextFormat.DT_CENTER);
                }

                return;
            }

            hud_text[(int)TXT.TXT_NAV_INDEX].font = hud_font;

            Instruction navpt = ship.GetNextNavPoint();

            if (navpt != null)
            {
                int cx = width / 2;
                int cy = height / 2;
                int l = cx - bar_width / 2;
                int r = cx + bar_width / 2;
                int t = cy - bar_height / 2;
                int b = cy + bar_height / 2;

                int index = ship.GetNavIndex(navpt);
                double distance = ship.RangeToNavPoint(navpt);
                double speed = ship.Velocity().Length;
                int etr = 0;
                string txt;

                if (speed > 10)
                    etr = (int)(distance / speed);

                Rect info_rect = new Rect(r - 20, cy + 32, box_width, 12);

                if (tactical)
                    info_rect.x = width - info_rect.w - 8;

                if (ship.IsAutoNavEngaged())
                    txt = string.Format("{0} {1}", Game.GetText("HUDView.Auto-Nav"), index);
                else
                    txt = string.Format("{0} {1}", Game.GetText("HUDView.Nav"), index);
                DrawHUDText(TXT.TXT_NAV_INDEX, txt, info_rect, TextFormat.DT_RIGHT);

                info_rect.y += 10;
                if (navpt.Action() != 0)
                    DrawHUDText(TXT.TXT_NAV_ACTION, Instruction.ActionName(navpt.Action()), info_rect, TextFormat.DT_RIGHT);

                info_rect.y += 10;
                FormatUtil.FormatNumber(out txt, navpt.Speed());
                DrawHUDText(TXT.TXT_NAV_SPEED, txt, info_rect, TextFormat.DT_RIGHT);

                if (etr > 3600)
                {
                    info_rect.y += 10;
                    txt = string.Format("{0} XX:XX", Game.GetText("HUDView.time-enroute"));
                    DrawHUDText(TXT.TXT_NAV_ETR, txt, info_rect, TextFormat.DT_RIGHT);
                }
                else if (etr > 0)
                {
                    info_rect.y += 10;

                    int minutes = (etr / 60) % 60;
                    int seconds = (etr) % 60;
                    txt = string.Format("{0} {1:D2}:{2:D2}", Game.GetText("HUDView.time-enroute"), minutes, seconds);
                    DrawHUDText(TXT.TXT_NAV_ETR, txt, info_rect, TextFormat.DT_RIGHT);
                }

                if (navpt.HoldTime() > 0)
                {
                    info_rect.y += 10;

                    int hold = (int)navpt.HoldTime();
                    int minutes = (hold / 60) % 60;
                    int seconds = (hold) % 60;
                    txt = string.Format("{0} {1:D2}:{2:D2}", Game.GetText("HUDView.HOLD"), minutes, seconds);
                    DrawHUDText(TXT.TXT_NAV_HOLD, txt, info_rect, TextFormat.DT_RIGHT);
                }
            }
        }
        public virtual void DrawNavPoint(Instruction navpt, int index, bool next)
        {
            if (index >= 15 || navpt.Region() == null) return;

            // transform from starsystem to world coordinates:
            Point npt = navpt.Region().Location() + navpt.Location();

            if (active_region != null)
                npt -= active_region.Location();

            npt = npt.OtherHand();

            // transform from world to camera:
            projector.Transform(npt);

            // clip:
            if (npt.Z > 1.0)
            {

                // project:
                projector.Project(npt);

                int x = (int)npt.X;
                int y = (int)npt.Y;

                // clip:
                if (x > 4 && x < width - 4 &&
                        y > 4 && y < height - 4)
                {

                    Color c = Color.white;
                    if (navpt.Status() > Instruction.STATUS.ACTIVE && navpt.HoldTime() <= 0)
                        c = ColorExtensions.DarkGray;

                    // draw:
                    if (next)
                        window.DrawEllipse(x - 6, y - 6, x + 5, y + 5, c);

                    window.DrawLine(x - 6, y - 6, x + 6, y + 6, c);
                    window.DrawLine(x + 6, y - 6, x - 6, y + 6, c);

                    if (index > 0)
                    {
                        string npt_buf;
                        Rect npt_rect = new Rect(x + 10, y - 4, 200, 12);

                        if (navpt.Status() == Instruction.STATUS.COMPLETE && navpt.HoldTime() > 0)
                        {
                            string hold_time;
                            FormatUtil.FormatTime(out hold_time, navpt.HoldTime());
                            npt_buf = string.Format("{0} {1}", index, hold_time);
                        }
                        else
                        {
                            npt_buf = string.Format("{0}", index);
                        }

                        DrawHUDText(TXT.TXT_NAV_PT + index, npt_buf, npt_rect, TextFormat.DT_LEFT);
                    }
                }
            }

            if (next && mode == HUDModes.HUD_MODE_NAV && navpt.Region() == ship.GetRegion())
            {

                // Translate into camera relative:
                Point tloc = navpt.Location().OtherHand();
                projector.Transform(tloc);

                bool behind = tloc.Z < 0;

                if (behind)
                    tloc.Z = -tloc.Z;

                // Project into screen coordinates:
                projector.Project(tloc);

                // DRAW THE OFFSCREEN CHASE INDICATOR:
                if (behind ||
                        tloc.X <= 0 || tloc.X >= width - 1 ||
                        tloc.Y <= 0 || tloc.Y >= height - 1)
                {

                    // Left side:
                    if (tloc.X <= 0 || (behind && tloc.X < width / 2))
                    {
                        if (tloc.Y < ah) tloc.Y = ah;
                        else if (tloc.Y >= height - ah) tloc.Y = height - 1 - ah;

                        chase_left_sprite.Show();
                        //chase_left_sprite.SetAnimation(chase_left);
                        chase_left_sprite.MoveTo(new Point(aw, tloc.Y, 1));
                    }

                    // Right side:
                    else if (tloc.X >= width - 1 || behind)
                    {
                        if (tloc.Y < ah) tloc.Y = ah;
                        else if (tloc.Y >= height - ah) tloc.Y = height - 1 - ah;

                        chase_right_sprite.Show();
                        //chase_right_sprite.SetAnimation(chase_right);
                        chase_right_sprite.MoveTo(new Point(width - 1 - aw, tloc.Y, 1));
                    }
                    else
                    {
                        if (tloc.X < aw) tloc.X = aw;
                        else if (tloc.X >= width - aw) tloc.X = width - 1 - aw;

                        // Top edge:
                        if (tloc.Y <= 0)
                        {
                            chase_top_sprite.Show();
                            //chase_top_sprite.SetAnimation(chase_top);
                            chase_top_sprite.MoveTo(new Point(tloc.X, ah, 1));
                        }

                        // Bottom edge:
                        else if (tloc.Y >= height - 1)
                        {
                            chase_bottom_sprite.Show();
                            //chase_bottom_sprite.SetAnimation(chase_bottom);
                            chase_bottom_sprite.MoveTo(new Point(tloc.X, height - 1 - ah, 1));
                        }
                    }
                }
            }
        }
        public virtual void DrawContactMarkers()
        {
            threat = 0;

            for (int i = 0; i < MAX_CONTACT; i++)
            {
                HideHUDText((int)TXT.TXT_CONTACT_NAME + i);
                HideHUDText((int)TXT.TXT_CONTACT_INFO + i);
            }


            if (ship == null)
                return;

            int index = 0;

            // draw own sensor contacts:
            foreach (Contact c2 in ship.ContactList())
            {

                // draw track ladder:
                if (c2.TrackLength() > 0 && c2.GetShip() != ship)
                {
                    DrawTrack(c2);
                }

                DrawContact(c2, index++);
            }

            Color c = ship.MarkerColor();

            // draw own ship track ladder:
            if (CameraDirector.GetCameraMode() == CameraDirector.CAM_MODE.MODE_ORBIT && ship.TrackLength() > 0)
            {
                int ctl = ship.TrackLength();

                Point t1 = ship.Location();
                Point t2 = ship.TrackPoint(0);

                if (t1 != t2)
                    DrawTrackSegment(t1, t2, c);

                for (int i = 0; i < ctl - 1; i++)
                {
                    t1 = ship.TrackPoint(i);
                    t2 = ship.TrackPoint(i + 1);

                    if (t1 != t2)
                        DrawTrackSegment(t1, t2, c * ((ctl - i) / (float)ctl));
                }
            }

            // draw own ship marker:
            Point mark_pt = ship.Location();
            projector.Transform(mark_pt);

            // clip:
            if (CameraDirector.GetCameraMode() == CameraDirector.CAM_MODE.MODE_ORBIT && mark_pt.Z > 1.0)
            {
                projector.Project(mark_pt);

                int x = (int)mark_pt.X;
                int y = (int)mark_pt.Y;

                if (x > 4 && x < width - 4 &&
                        y > 4 && y < height - 4)
                {

                    DrawDiamond(x, y, 5, c);

                    if (tactical)
                    {
                        Rect self_rect = new Rect(x + 8, y - 4, 200, 12);
                        DrawHUDText(TXT.TXT_SELF, ship.Name(), self_rect, TextFormat.DT_LEFT, HUD_CASE.HUD_MIXED_CASE);

                        if (NetGame.GetInstance() != null)
                        {
                            Player p = Player.GetCurrentPlayer();
                            if (p != null)
                            {
                                Rect net_name_rect = new Rect(x + 8, y + 6, 120, 12);
                                DrawHUDText(TXT.TXT_SELF_NAME, p.Name(), net_name_rect, TextFormat.DT_LEFT, HUD_CASE.HUD_MIXED_CASE);
                            }
                        }
                    }
                }
            }

            // draw life bars on targeted ship:
            if (target != null && target.Type() == (int)SimObject.TYPES.SIM_SHIP && target.Rep() != null)
            {
                Ship tgt_ship = (Ship)target;
                if (tgt_ship == null)
                {
                    ErrLogger.PrintLine("   Null Pointer in HUDView.DrawContactMarkers(). Please investigate.");
                    return;
                }
                Graphic g = tgt_ship.Rep();
                UnityEngine.Rect r = g.ScreenRect();

                Point mark_pt2 = new Point();

                if (tgt_ship != null)
                    mark_pt2 = tgt_ship.Location();

                projector.Transform(mark_pt2);

                // clip:
                if (mark_pt2.Z > 1.0)
                {
                    projector.Project(mark_pt2);

                    int x = (int)mark_pt2.X;
                    int y = (int)r.y;

                    if (y >= 2000)
                        y = (int)mark_pt2.Y;

                    if (x > 4 && x < width - 4 &&
                            y > 4 && y < height - 4)
                    {

                        const int BAR_LENGTH = 40;

                        // life bars:
                        int sx = x - BAR_LENGTH / 2;
                        int sy = y - 8;

                        double hull_strength = tgt_ship.Integrity() / tgt_ship.Design().integrity;

                        int hw = (int)(BAR_LENGTH * hull_strength);
                        int sw = (int)(BAR_LENGTH * (tgt_ship.ShieldStrength() / 100.0));

                        ShipSystem.STATUS s = ShipSystem.STATUS.NOMINAL;

                        if (hull_strength < 0.30) s = ShipSystem.STATUS.CRITICAL;
                        else if (hull_strength < 0.60) s = ShipSystem.STATUS.DEGRADED;

                        Color hc = GetStatusColor(s);
                        Color sc = hud_color;

                        window.FillRect(sx, sy, sx + hw, sy + 1, hc);
                        window.FillRect(sx, sy + 3, sx + sw, sy + 4, sc);
                    }
                }
            }
        }
        public virtual void DrawContact(Contact contact, int index)
        {
            if (index >= MAX_CONTACT) return;

            Color c = MarkerColor(contact);
            int c_iff = contact.GetIFF(ship);
            Ship c_ship = contact.GetShip();
            Shot c_shot = contact.GetShot();
            Point mark_pt = contact.Location();
            double distance = 0;

            if (c_ship == null && c_shot == null || c_ship == ship)
                return;

            if (c_ship != null && c_ship.GetFlightPhase() < OP_MODE.ACTIVE)
                return;

            if (c_ship != null)
            {
                mark_pt = c_ship.Location();

                if (c_ship.IsGroundUnit())
                    mark_pt += new Point(0, 150, 0);
            }
            else
            {
                mark_pt = c_shot.Location();
            }

            projector.Transform(mark_pt);

            // clip:
            if (mark_pt.Z > 1.0)
            {
                distance = mark_pt.Length;

                projector.Project(mark_pt);

                int x = (int)mark_pt.X;
                int y = (int)mark_pt.Y;

                if (x > 4 && x < width - 4 &&
                        y > 4 && y < height - 4)
                {

                    DrawDiamond(x, y, 3, c);

                    if (contact.Threat(ship))
                    {
                        if (c_ship != null)
                        {
                            window.DrawEllipse(x - 6, y - 6, x + 6, y + 6, c);
                        }
                        else
                        {
                            DrawDiamond(x, y, 7, c);
                        }
                    }

                    bool name_crowded = false;

                    if (x < width - 8)
                    {
                        string code = Game.GetText("HUDView.symbol.fighter");

                        if (c_ship != null)
                        {
                            if (c_ship.Class() > CLASSIFICATION.LCA)
                                code = Game.GetText("HUDView.symbol.starship");
                        }

                        else if (c_shot != null)
                        {
                            code = Game.GetText("HUDView.symbol.torpedo");
                        }

                        Sensor sensor = ship.GetSensor();
                        double limit = 75e3;

                        if (sensor != null)
                            limit = sensor.GetBeamRange();

                        double range = contact.Range(ship, limit);

                        string contact_buf;
                        Rect contact_rect = new Rect(x + 8, y - 4, 120, 12);

                        if (range == 0)
                        {
                            contact_buf = string.Format("{0} *", code);
                        }
                        else
                        {
                            bool mega = false;

                            if (range > 999e3)
                            {
                                range /= 1e6;
                                mega = true;
                            }
                            else if (range < 1e3)
                                range = 1;
                            else
                                range /= 1000;

                            if (arcade)
                            {
                                if (c_ship != null)
                                    contact_buf = c_ship.Name();
                                else if (!mega)
                                    contact_buf = string.Format("{0} {1}", code, (int)range);
                                else
                                    contact_buf = string.Format("{0} {1:0.0} M", code, range);
                            }
                            else
                            {
                                char closing = '+';
                                Point delta_v = new Point();

                                if (c_ship != null)
                                    delta_v = ship.Velocity() - c_ship.Velocity();
                                else if (c_shot != null)
                                    delta_v = ship.Velocity() - c_shot.Velocity();

                                if (Point.Dot(delta_v, ship.Velocity()) < 0)    // losing ground
                                    closing = '-';

                                if (!mega)
                                    contact_buf = string.Format("{0} {1}{2}", code, (int)range, closing);
                                else
                                    contact_buf = string.Format("{0} {1:0.0} M", code, range);
                            }
                        }

                        if (!IsNameCrowded(x, y))
                        {
                            DrawHUDText(TXT.TXT_CONTACT_INFO + index, contact_buf, contact_rect, TextFormat.DT_LEFT, HUD_CASE.HUD_MIXED_CASE);

                            if (c_shot != null || (c_ship != null && (c_ship.IsDropship() || c_ship.IsStatic())))
                                name_crowded = distance > 50e3;
                        }
                        else
                        {
                            name_crowded = true;
                        }
                    }

                    bool name_drawn = false;
                    if (NetGame.GetInstance() != null && c_ship != null)
                    {
                        NetPlayer netp = NetGame.GetInstance().FindPlayerByObjID(c_ship.GetObjID());
                        if (netp != null && netp.Name().Equals("Server A.I. Ship"))
                        {
                            Rect contact_rect = new Rect(x + 8, y + 6, 120, 12);
                            DrawHUDText(TXT.TXT_CONTACT_NAME + index, netp.Name(), contact_rect, TextFormat.DT_LEFT, HUD_CASE.HUD_MIXED_CASE);
                            name_drawn = true;
                        }
                    }

                    if (!name_drawn && !name_crowded && c_ship != null && c_iff < 10 && !arcade)
                    {
                        Rect contact_rect = new Rect(x + 8, y + 6, 120, 12);
                        DrawHUDText(TXT.TXT_CONTACT_NAME + index, c_ship.Name(), contact_rect, TextFormat.DT_LEFT, HUD_CASE.HUD_MIXED_CASE);
                    }
                }
            }

            if (contact.Threat(ship) && !ship.IsStarship())
            {
                if (threat < 1 && c_ship != null && !c_ship.IsStarship())
                    threat = 1;

                if (c_shot != null)
                    threat = 2;
            }
        }
        public virtual void DrawTrack(Contact contact)
        {
            Ship c_ship = contact.GetShip();

            if (c_ship != null && c_ship.GetFlightPhase() < OP_MODE.ACTIVE)
                return;

            int ctl = contact.TrackLength();
            Color c = MarkerColor(contact);

            Point t1 = contact.Location();
            Point t2 = contact.TrackPoint(0);

            if (t1 != t2)
                DrawTrackSegment(t1, t2, c);

            for (int i = 0; i < ctl - 1; i++)
            {
                t1 = contact.TrackPoint(i);
                t2 = contact.TrackPoint(i + 1);

                if (t1 != t2)
                    DrawTrackSegment(t1, t2, c * ((float)(ctl - i) / (float)ctl));
            }
        }
        public virtual void DrawTrackSegment(Point t1, Point t2, Color c)
        {
            int x1, y1, x2, y2;

            projector.Transform(t1);
            projector.Transform(t2);

            const double CLIP_Z = 0.1;

            if (t1.Z < CLIP_Z && t2.Z < CLIP_Z)
                return;

            if (t1.Z < CLIP_Z && t2.Z >= CLIP_Z)
            {
                double dx = t2.X - t1.X;
                double dy = t2.Y - t1.Y;
                double s = (CLIP_Z - t1.Z) / (t2.Z - t1.Z);

                t1.X += dx * s;
                t1.Y += dy * s;
                t1.Z = CLIP_Z;
            }

            else if (t2.Z < CLIP_Z && t1.Z >= CLIP_Z)
            {
                double dx = t1.X - t2.X;
                double dy = t1.Y - t2.Y;
                double s = (CLIP_Z - t2.Z) / (t1.Z - t2.Z);

                t2.X += dx * s;
                t2.Y += dy * s;
                t2.Z = CLIP_Z;
            }

            if (t1.Z >= CLIP_Z && t2.Z >= CLIP_Z)
            {
                projector.Project(t1, false);
                projector.Project(t2, false);

                x1 = (int)t1.X;
                y1 = (int)t1.Y;
                x2 = (int)t2.X;
                y2 = (int)t2.Y;

                if (window.ClipLine(ref x1, ref y1, ref x2, ref y2))
                    window.DrawLine(x1, y1, x2, y2, c);
            }
        }
        public virtual void DrawRect(SimObject targ)
        {
            Graphic g = targ.Rep();
            Rect r = new Rect((int)g.ScreenRect().x, (int)g.ScreenRect().y, (int)g.ScreenRect().width, (int)g.ScreenRect().height);
            Color c;

            if (targ.Type() == (int)SimObject.TYPES.SIM_SHIP)
                c = ((Ship)targ).MarkerColor();
            else
                c = ((Shot)targ).MarkerColor();

            if (r.w > 0 && r.h > 0)
            {
                if (r.w < 8)
                {
                    r.x -= (8 - r.w) / 2;
                    r.w = 8;
                }

                if (r.h < 8)
                {
                    r.y -= (8 - r.h) / 2;
                    r.h = 8;
                }
            }

            else
            {
                Point mark_pt = targ.Location();
                projector.Transform(mark_pt);

                // clip:
                if (mark_pt.Z < 1.0)
                    return;

                projector.Project(mark_pt);

                int x = (int)mark_pt.X;
                int y = (int)mark_pt.Y;

                if (x < 4 || x > width - 4 || y < 4 || y > height - 4)
                    return;

                r.x = x - 4;
                r.y = y - 4;
                r.w = 8;
                r.h = 8;
            }

            // horizontal
            window.DrawLine(r.x, r.y, r.x + 8, r.y, c);
            window.DrawLine(r.x + r.w - 8, r.y, r.x + r.w, r.y, c);
            window.DrawLine(r.x, r.y + r.h, r.x + 8, r.y + r.h, c);
            window.DrawLine(r.x + r.w - 8, r.y + r.h, r.x + r.w, r.y + r.h, c);
            // vertical
            window.DrawLine(r.x, r.y, r.x, r.y + 8, c);
            window.DrawLine(r.x, r.y + r.h - 8, r.x, r.y + r.h, c);
            window.DrawLine(r.x + r.w, r.y, r.x + r.w, r.y + 8, c);
            window.DrawLine(r.x + r.w, r.y + r.h - 8, r.x + r.w, r.y + r.h, c);
        }

        public virtual void DrawTarget()
        {
            const int bar_width = 256;
            const int bar_height = 192;
            const int box_width = 120;

            SimObject old_target = target;

            if (mode == HUDModes.HUD_MODE_ILS)
            {
                Ship controller = ship.GetController();
                if (controller != null && target == null)
                    target = controller;
            }

            if (target != null && target.Rep() != null)
            {
                Sensor sensor = ship.GetSensor();
                Contact contact = null;

                if (sensor != null && target.Type() == (int)SimObject.TYPES.SIM_SHIP)
                {
                    contact = sensor.FindContact((Ship)target);
                }

                int cx = width / 2;
                int cy = height / 2;
                int l = cx - bar_width / 2;
                int r = cx + bar_width / 2;
                int t = cy - bar_height / 2;
                int b = cy + bar_height / 2;
                Point delta = target.Location() - ship.Location();
                double distance = delta.Length;
                Point delta_v = ship.Velocity() - target.Velocity();
                double speed = delta_v.Length;
                string txt;

                if (mode == HUDModes.HUD_MODE_ILS && ship.GetInbound() != null && ship.GetInbound().GetDeck() != null)
                {
                    delta = ship.GetInbound().GetDeck().EndPoint() - ship.Location();
                    distance = delta.Length;
                }

                if (Point.Dot(delta, ship.Velocity()) > 0)
                {       // in front
                    if (Point.Dot(delta_v, ship.Velocity()) < 0)    // losing ground
                        speed = -speed;
                }
                else
                {                                    // behind
                    if (Point.Dot(delta_v, ship.Velocity()) > 0)    // passing
                        speed = -speed;
                }

                Rect range_rect = new Rect(r - 20, cy - 5, box_width, 12);

                if (tactical)
                    range_rect.x = width - range_rect.w - 8;

                if (contact != null)
                {
                    Sensor sensor2 = ship.GetSensor();
                    double limit = 75e3;

                    if (sensor2 != null)
                        limit = sensor2.GetBeamRange();

                    distance = contact.Range(ship, limit);

                    if (!contact.ActLock() && !contact.PasLock())
                    {
                        txt = Game.GetText("HUDView.No-Range");
                        speed = 0;
                    }
                    else
                    {
                        FormatUtil.FormatNumber(out txt, distance);
                    }
                }

                else
                {
                    FormatUtil.FormatNumber(out txt, distance);
                }

                DrawHUDText(TXT.TXT_RANGE, txt, range_rect, TextFormat.DT_RIGHT);

                if (arcade)
                {
                    target = old_target;
                    return;
                }

                range_rect.y += 18;
                FormatUtil.FormatNumber(out txt, speed);
                DrawHUDText(TXT.TXT_CLOSING_SPEED, txt, range_rect, TextFormat.DT_RIGHT);

                // target info:
                if (!tactical)
                {
                    range_rect.y = cy - 76;
                }

                else
                {
                    range_rect.x = width - 2 * box_width - 8;
                    range_rect.y = cy - 76;
                    range_rect.w = 2 * box_width;
                }

                DrawHUDText(TXT.TXT_TARGET_NAME, target.Name(), range_rect, TextFormat.DT_RIGHT);

                if (target.Type() == (int)SimObject.TYPES.SIM_SHIP)
                {
                    Ship tgt_ship = (Ship)target;

                    range_rect.y += 10;
                    DrawHUDText(TXT.TXT_TARGET_DESIGN, tgt_ship.Design().display_name, range_rect, TextFormat.DT_RIGHT);

                    if (mode != HUDModes.HUD_MODE_ILS)
                    {
                        if (tgt_ship.IsStarship())
                        {
                            range_rect.y += 10;
                            txt = string.Format("{0} {1:D3}", Game.GetText("HUDView.symbol.shield"), (int)tgt_ship.ShieldStrength());
                            DrawHUDText(TXT.TXT_TARGET_SHIELD, txt, range_rect, TextFormat.DT_RIGHT);
                        }

                        range_rect.y += 10;
                        txt = string.Format("{0} {1:D3}", Game.GetText("HUDView.symbol.hull"), (int)(tgt_ship.Integrity() / tgt_ship.Design().integrity * 100));
                        DrawHUDText(TXT.TXT_TARGET_HULL, txt, range_rect, TextFormat.DT_RIGHT);

                        ShipSystem sys = ship.GetSubTarget();
                        if (sys != null)
                        {
                            Color stat = hud_color;
                            ulong blink = Game.RealTime();

                            ulong blink_delta = Game.RealTime() - blink;
                            txt = string.Format("{0} {1:D3}", sys.Abbreviation(), (int)sys.Availability());

                            switch (sys.Status())
                            {
                                case ShipSystem.STATUS.DEGRADED: stat = new Color32(255, 255, 0, 255); break;
                                case ShipSystem.STATUS.CRITICAL:
                                case ShipSystem.STATUS.DESTROYED: stat = new Color32(255, 0, 0, 255); break;
                                case ShipSystem.STATUS.MAINT:
                                    if (blink_delta < 250)
                                        stat = new Color32(8, 8, 8, 255);
                                    break;
                            }

                            if (blink_delta > 500)
                                blink = Game.RealTime();

                            range_rect.y += 10;
                            DrawHUDText(TXT.TXT_TARGET_SUB, txt, range_rect, TextFormat.DT_RIGHT);
                        }
                    }
                }

                else if (target.Type() == (int)SimObject.TYPES.SIM_DRONE)
                {
                    Drone tgt_drone = (Drone)target;

                    range_rect.y += 10;
                    DrawHUDText(TXT.TXT_TARGET_DESIGN, tgt_drone.DesignName(), range_rect, TextFormat.DT_RIGHT);

                    range_rect.y += 10;
                    int eta = tgt_drone.GetEta();

                    if (eta > 0)
                    {
                        int minutes = (eta / 60) % 60;
                        int seconds = (eta) % 60;

                        string eta_buf;
                        eta_buf = string.Format("T {0}:{1:D2}", minutes, seconds);
                        DrawHUDText(TXT.TXT_TARGET_ETA, eta_buf, range_rect, TextFormat.DT_RIGHT);
                    }
                }
            }

            target = old_target;
        }
        public virtual void DrawSight()
        {
            if (target != null && target.Rep() != null)
            {
                Point delta = target.Location() - ship.Location();
                double distance = delta.Length;

                // draw LCOS on target:
                if (!tactical)
                    DrawLCOS(target, distance);
            }
        }
        public virtual void DrawLCOS(SimObject targ, double dist)
        {
            lead_sprite.Hide();
            aim_sprite.Hide();
            chase_left_sprite.Hide();
            chase_right_sprite.Hide();
            chase_top_sprite.Hide();
            chase_bottom_sprite.Hide();

            double xtarg = xcenter;
            double ytarg = ycenter;

            Weapon prim = ship.GetPrimary();
            if (prim == null) return;

            Point tloc = targ.Location();
            // Translate into camera relative:
            projector.Transform(tloc);

            bool behind = tloc.Z < 0;

            if (behind)
                tloc.Z = -tloc.Z;

            // Project into screen coordinates:
            projector.Project(tloc);

            // DRAW THE OFFSCREEN CHASE INDICATOR:
            if (behind ||
                    tloc.X <= 0 || tloc.X >= width - 1 ||
                    tloc.Y <= 0 || tloc.Y >= height - 1)
            {

                // Left side:
                if (tloc.X <= 0 || (behind && tloc.X < width / 2))
                {
                    if (tloc.Y < ah) tloc.Y = ah;
                    else if (tloc.Y >= height - ah) tloc.Y = height - 1 - ah;

                    chase_left_sprite.Show();
                    //chase_left_sprite.SetAnimation(chase_left);
                    chase_left_sprite.MoveTo(new Point(aw, tloc.Y, 1));
                }

                // Right side:
                else if (tloc.X >= width - 1 || behind)
                {
                    if (tloc.Y < ah) tloc.Y = ah;
                    else if (tloc.Y >= height - ah) tloc.Y = height - 1 - ah;

                    chase_right_sprite.Show();
                    //chase_right_sprite.SetAnimation(chase_right);
                    chase_right_sprite.MoveTo(new Point(width - 1 - aw, tloc.Y, 1));
                }
                else
                {
                    if (tloc.X < aw) tloc.X = aw;
                    else if (tloc.X >= width - aw) tloc.X = width - 1 - aw;

                    // Top edge:
                    if (tloc.Y <= 0)
                    {
                        chase_top_sprite.Show();
                        //chase_top_sprite.SetAnimation(chase_top);
                        chase_top_sprite.MoveTo(new Point(tloc.X, ah, 1));
                    }

                    // Bottom edge:
                    else if (tloc.Y >= height - 1)
                    {
                        chase_bottom_sprite.Show();
                        //chase_bottom_sprite.SetAnimation(chase_bottom);
                        chase_bottom_sprite.MoveTo(new Point(tloc.X, height - 1 - ah, 1));
                    }
                }
            }

            // DRAW THE LCOS:
            else
            {
                if (!ship.IsStarship())
                {
                    Point aim_vec = ship.Heading();
                    aim_vec.Normalize();

                    // shot speed is relative to ship speed:
                    Point shot_vel = ship.Velocity() + aim_vec * prim.Design().speed;
                    double shot_speed = shot_vel.Length;

                    // time for shot to reach target
                    double time = dist / shot_speed;

                    // LCOS (Lead Computing Optical Sight)
                    if (gunsight == 0)
                    {
                        // where the shot will be when it is the same distance
                        // away from the ship as the target:
                        Point impact = ship.Location() + (shot_vel * time);

                        // where the target will be when the shot reaches it:
                        Point targ_vel = targ.Velocity();
                        Point dest = targ.Location() + (targ_vel * time);
                        Point delta = impact - dest;

                        // draw the gun sight here in 3d world coordinates:
                        Point sight = targ.Location() + delta;

                        // Project into screen coordinates:
                        projector.Transform(sight);
                        projector.Project(sight);

                        xtarg = sight.X;
                        ytarg = sight.Y;

                        aim_sprite.Show();
                        aim_sprite.MoveTo(new Point(xtarg, ytarg, 1));
                    }

                    // Wing Commander style lead indicator
                    else
                    {
                        // where the target will be when the shot reaches it:
                        Point targ_vel = targ.Velocity() - ship.Velocity();
                        Point dest = targ.Location() + (targ_vel * time);

                        // Translate into camera relative:
                        projector.Transform(dest);
                        projector.Project(dest);

                        xtarg = dest.X;
                        ytarg = dest.Y;

                        lead_sprite.Show();
                        lead_sprite.MoveTo(new Point(xtarg, ytarg, 1));
                    }
                }
            }
        }
        public virtual void DrawDesignators()
        {
            double xtarg = xcenter;
            double ytarg = ycenter;
            SimObject t1 = null;
            SimObject t2 = null;
            SimObject t3 = null;
            SpriteGraphic sprite = null;

            tgt1_sprite.Hide();
            tgt2_sprite.Hide();
            tgt3_sprite.Hide();
            tgt4_sprite.Hide();

            // fighters just show primary target:
            if (ship.IsDropship())
            {
                SimObject t = ship.GetTarget();
                ShipSystem s = ship.GetSubTarget();

                if (t != null)
                {
                    Point tloc = t.Location();
                    if (s != null)
                    {
                        tloc = s.MountLocation();
                    }
                    else if (t.Type() == (int)SimObject.TYPES.SIM_SHIP)
                    {
                        Ship tgt_ship = (Ship)t;

                        if (tgt_ship.IsGroundUnit())
                            tloc += new Point(0, 150, 0);
                    }

                    projector.Transform(tloc);

                    if (tloc.Z > 0)
                    {
                        projector.Project(tloc);

                        xtarg = tloc.X;
                        ytarg = tloc.Y;

                        if (xtarg > 0 && xtarg < width - 1 && ytarg > 0 && ytarg < height - 1)
                        {
                            double range = new Point(t.Location() - ship.Location()).Length;

                            // use out-of-range crosshair if out of range:
                            if (ship.GetPrimaryDesign() == null || ship.GetPrimaryDesign().max_range < range)
                            {
                                tgt4_sprite.Show();
                                tgt4_sprite.MoveTo(new Point(xtarg, ytarg, 1));
                            }

                            // else, use in-range primary crosshair:
                            else
                            {
                                tgt1_sprite.Show();
                                tgt1_sprite.MoveTo(new Point(xtarg, ytarg, 1));
                            }
                        }
                    }
                }
            }

            // starships show up to three targets:
            else
            {
                //ListIter<WeaponGroup> w = ship.Weapons();
                //while (!t3 && ++w)
                foreach (WeaponGroup w in ship.Weapons())
                {
                    if (t3 != null) break;
                    SimObject t = w.GetTarget();
                    ShipSystem s = w.GetSubTarget();

                    if (w.Contains(ship.GetPrimary()))
                    {
                        if (t == null) t = ship.GetTarget();
                        t1 = t;
                        sprite = tgt1_sprite;
                    }

                    else if (t != null && w.Contains(ship.GetSecondary()))
                    {
                        t2 = t;
                        sprite = tgt2_sprite;

                        if (t2 == t1)
                            continue;   // don't overlap target designators
                    }

                    else if (t != null)
                    {
                        t3 = t;
                        sprite = tgt3_sprite;

                        if (t3 == t1 || t3 == t2)
                            continue;   // don't overlap target designators
                    }

                    if (t != null)
                    {
                        Point tloc = t.Location();

                        if (s != null)
                            tloc = s.MountLocation();

                        projector.Transform(tloc);

                        if (tloc.Z > 0)
                        {
                            projector.Project(tloc);

                            xtarg = tloc.X;
                            ytarg = tloc.Y;

                            if (xtarg > 0 && xtarg < width - 1 && ytarg > 0 && ytarg < height - 1)
                            {
                                double range = new Point(t.Location() - ship.Location()).Length;

                                // flip to out-of-range crosshair
                                if (sprite == tgt1_sprite)
                                {
                                    if (ship.GetPrimaryDesign() == null || ship.GetPrimaryDesign().max_range < range)
                                    {
                                        sprite = tgt4_sprite;
                                    }
                                }

                                sprite.Show();
                                sprite.MoveTo(new Point(xtarg, ytarg, 1));
                            }
                        }
                    }
                }
            }
        }
        public virtual void DrawFPM()
        {
            fpm_sprite.Hide();

            if (ship.Velocity().Length > 50)
            {
                double xtarg = xcenter;
                double ytarg = ycenter;

                Point svel = ship.Velocity();
                svel.Normalize();

                Point tloc = ship.Location() + svel * 1e8;
                // Translate into camera relative:
                projector.Transform(tloc);

                bool behind = tloc.Z < 0;

                if (behind)
                    return;

                // Project into screen coordinates:
                projector.Project(tloc);

                xtarg = tloc.X;
                ytarg = tloc.Y;

                fpm_sprite.Show();
                fpm_sprite.MoveTo(new Point(xtarg, ytarg, 1));
            }
        }
        public virtual void DrawHPM()
        {
            hpm_sprite.Hide();

            if (ship == null)
                return;

            double xtarg = xcenter;
            double ytarg = ycenter;

            double az = ship.GetHelmHeading() - Math.PI;
            double el = ship.GetHelmPitch();

            Point hvec = new Point(Math.Sin(az), Math.Sin(el), Math.Cos(az));
            hvec.Normalize();

            Point tloc = ship.Location() + hvec * 1e8;
            // Translate into camera relative:
            projector.Transform(tloc);

            bool behind = tloc.Z < 0;

            if (behind)
                return;

            // Project into screen coordinates:
            projector.Project(tloc);

            xtarg = tloc.X;
            ytarg = tloc.Y;

            hpm_sprite.Show();
            hpm_sprite.MoveTo(new Point(xtarg, ytarg, 1));
        }
        public virtual void DrawCompass()
        {
            if (ship == null || ship.Rep() == null)
                return;

            Solid solid = (Solid)ship.Rep();
            Point loc = solid.Location();

            az_ring.MoveTo(loc);
            az_pointer.MoveTo(loc);
            el_ring.MoveTo(loc);
            el_pointer.MoveTo(loc);

            double helm_heading = ship.GetHelmHeading();
            double helm_pitch = ship.GetHelmPitch();
            double curr_heading = ship.CompassHeading();
            double curr_pitch = ship.CompassPitch();

            bool show_az = Math.Abs(helm_heading - curr_heading) > 5 * ConstantsF.DEGREES;
            bool show_el = Math.Abs(helm_pitch - curr_pitch) > 5 * ConstantsF.DEGREES;

            Scene scene = camview.GetScene();

            if (show_az || show_el)
            {
                scene.AddGraphic(az_ring);
                az_ring.Show();

                if (show_el || Math.Abs(helm_pitch) > 5 * ConstantsF.DEGREES)
                {
                    scene.AddGraphic(el_ring);
                    Matrix33D ring_orient = new Matrix33D();
                    ring_orient.Yaw(helm_heading + Math.PI);
                    el_ring.SetOrientation(ring_orient);
                    el_ring.Show();

                    scene.AddGraphic(el_pointer);
                    Matrix33D pointer_orient = new Matrix33D();
                    pointer_orient.Yaw(helm_heading + Math.PI);
                    pointer_orient.Pitch(-helm_pitch);
                    pointer_orient.Roll(Math.PI / 2);
                    el_pointer.SetOrientation(pointer_orient);
                    el_pointer.Show();
                }
                else
                {
                    scene.AddGraphic(az_pointer);
                    Matrix33D pointer_orient = new Matrix33D();
                    pointer_orient.Yaw(helm_heading + Math.PI);

                    az_pointer.SetOrientation(pointer_orient);
                    az_pointer.Show();
                }
            }
        }
        public virtual void HideCompass()
        {
            az_ring.Hide();
            az_pointer.Hide();
            el_ring.Hide();
            el_pointer.Hide();

            Scene scene = az_ring.GetScene();
            if (scene != null)
            {
                scene.DelGraphic(az_ring);
                scene.DelGraphic(az_pointer);
                scene.DelGraphic(el_ring);
                scene.DelGraphic(el_pointer);
            }
        }


        public virtual void DrawPitchLadder()
        {
            for (int i = 0; i < 31; i++)
                pitch_ladder[i].Hide();

            if (ship.IsAirborne() && Game.MaxTexSize() > 128)
            {
                double xtarg = xcenter;
                double ytarg = ycenter;

                Point uvec = new Point(0, 1, 0);
                Point svel = ship.Velocity();

                if (svel.Length == 0)
                    svel = ship.Heading();

                if (svel.X == 0 && svel.Z == 0)
                    return;

                svel.Y = 0;
                svel.Normalize();

                Point gloc = ship.Location();
                gloc.Y = 0;

                const double baseline = 1e9;
                const double clip_angle = 20 * ConstantsF.DEGREES;

                Point tloc = gloc + svel * baseline;

                // Translate into camera relative:
                projector.Transform(tloc);

                // Project into screen coordinates:
                projector.Project(tloc);

                xtarg = tloc.X;
                ytarg = tloc.Y;

                // compute roll angle:
                double roll_angle = 0;
                double pitch_angle = 0;

                Point heading = ship.Heading();
                heading.Normalize();

                if (heading.X != 0 || heading.Z != 0)
                {
                    Point gheading = heading;
                    gheading.Y = 0;
                    gheading.Normalize();

                    double dot = Point.Dot(gheading, heading);

                    if (heading.Y < 0) dot = -dot;

                    pitch_angle = Math.Acos(dot);

                    if (pitch_angle > Math.PI / 2)
                        pitch_angle -= Math.PI;

                    double s0 = Math.Sin(pitch_angle);
                    double c0 = Math.Cos(pitch_angle);
                    double s1 = Math.Sin(pitch_angle + 10 * ConstantsF.DEGREES);
                    double c1 = Math.Cos(pitch_angle + 10 * ConstantsF.DEGREES);

                    tloc = gloc + (svel * baseline * c0) + (uvec * baseline * s0);
                    projector.Transform(tloc);

                    double x0 = tloc.X;
                    double y0 = tloc.Y;

                    tloc = gloc + (svel * baseline * c1) + (uvec * baseline * s1);
                    projector.Transform(tloc);

                    double x1 = tloc.X;
                    double y1 = tloc.Y;

                    double dx = x1 - x0;
                    double dy = y1 - y0;

                    roll_angle = Math.Atan2(-dy, dx) + Math.PI / 2;
                }

                const double alias_limit = 0.1 * ConstantsF.DEGREES;

                if (Math.Abs(roll_angle) <= alias_limit)
                {
                    if (roll_angle > 0)
                        roll_angle = alias_limit;
                    else
                        roll_angle = -alias_limit;
                }

                else if (Math.Abs(roll_angle - Math.PI) <= alias_limit)
                {
                    roll_angle = Math.PI - alias_limit;
                }

                if (Math.Abs(pitch_angle) <= clip_angle)
                {
                    pitch_ladder[15].Show();
                    pitch_ladder[15].MoveTo(new Point(xtarg, ytarg, 1));
                    pitch_ladder[15].SetAngle(roll_angle);
                }

                for (int i = 1; i <= 15; i++)
                {
                    double angle = i * 5 * ConstantsF.DEGREES;

                    if (i > 12)
                        angle = (60 + (i - 12) * 10) * ConstantsF.DEGREES;

                    double s = Math.Sin(angle);
                    double c = Math.Cos(angle);

                    if (Math.Abs(pitch_angle - angle) <= clip_angle)
                    {
                        // positive angle:
                        tloc = gloc + (svel * baseline * c) + (uvec * baseline * s);
                        projector.Transform(tloc);

                        if (tloc.Z > 0)
                        {
                            projector.Project(tloc);
                            pitch_ladder[15 - i].Show();
                            pitch_ladder[15 - i].MoveTo(new Point(tloc.X, tloc.Y, 1));
                            pitch_ladder[15 - i].SetAngle(roll_angle);
                        }
                    }

                    if (Math.Abs(pitch_angle + angle) <= clip_angle)
                    {
                        // negative angle:
                        tloc = gloc + (svel * baseline * c) + (uvec * -baseline * s);
                        projector.Transform(tloc);

                        if (tloc.Z > 0)
                        {
                            projector.Project(tloc);
                            pitch_ladder[15 + i].Show();
                            pitch_ladder[15 + i].MoveTo(new Point(tloc.X, tloc.Y, 1));
                            pitch_ladder[15 + i].SetAngle(roll_angle);
                        }
                    }
                }
            }
        }
        public virtual void DrawStarSystem()
        {
            if (sim != null && sim.GetStarSystem() != null)
            {
                StarSystem sys = sim.GetStarSystem();

                foreach (OrbitalBody body in sys.Bodies())
                {
                    DrawOrbitalBody(body);
                }
            }
        }


        public virtual void DrawMFDs()
        {
            for (int i = 0; i < 3; i++)
            {
                mfd[i].Show();
                mfd[i].SetShip(ship);
                mfd[i].SetCockpitHUDTexture(cockpit_hud_texture);
                mfd[i].Draw();
            }
        }

        public virtual void DrawWarningPanel()
        {
            int box_width = 75;
            int box_height = 17;
            int row_height = 28;
            int box_left = width / 2 - box_width * 2;

            if (cockpit_hud_texture != null)
            {
                box_left = 275;
                box_height = 18;
                row_height = 18;
            }

            if (ship != null)
            {
                if (Game.MaxTexSize() > 128)
                {
                    warn_left_sprite.Show();
                    warn_right_sprite.Show();
                }

                int x = box_left;
                int y = cockpit_hud_texture != null ? 410 : height - 97;
                int c = cockpit_hud_texture != null ? 3 : 4;

                ulong blink = Game.RealTime();

                for (int index = 0; index < 12; index++)
                {
                    ShipSystem.STATUS stat = (ShipSystem.STATUS)(-1);
                    string abrv = Game.GetText("HUDView.UNKNOWN");

                    switch (index)
                    {
                        case 0: stat = GetReactorStatus(ship); abrv = Game.GetText("HUDView.REACTOR"); break;
                        case 1: stat = GetDriveStatus(ship); abrv = Game.GetText("HUDView.DRIVE"); break;
                        case 2: stat = GetQuantumStatus(ship); abrv = Game.GetText("HUDView.QUANTUM"); break;
                        case 3:
                            stat = GetShieldStatus(ship); abrv = Game.GetText("HUDView.SHIELD");
                            if (ship.GetShield() == null && ship.GetDecoy() != null)
                                abrv = Game.GetText("HUDView.DECOY");
                            break;

                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            stat = GetWeaponStatus(ship, index - 4);
                            if (stat >= 0)
                            {
                                WeaponGroup g = ship.Weapons()[index - 4];
                                abrv = g.Name();
                            }
                            break;

                        case 8: stat = GetSensorStatus(ship); abrv = Game.GetText("HUDView.SENSOR"); break;
                        case 9: stat = GetComputerStatus(ship); abrv = Game.GetText("HUDView.COMPUTER"); break;
                        case 10: stat = GetThrusterStatus(ship); abrv = Game.GetText("HUDView.THRUSTER"); break;
                        case 11: stat = GetFlightDeckStatus(ship); abrv = Game.GetText("HUDView.FLTDECK"); break;
                    }

                    Rect warn_rect = new Rect(x, y, box_width, box_height);

                    if (cockpit_hud_texture != null)
                        cockpit_hud_texture.DrawRect(warn_rect, ColorExtensions.DarkGray);

                    if (stat >= 0)
                    {
                        SetStatusColor((ShipSystem.STATUS)stat);
                        Color tc = status_color;

                        if (stat != ShipSystem.STATUS.NOMINAL)
                        {
                            if (Game.RealTime() - blink < 250)
                            {
                                tc = cockpit_hud_texture != null ? txt_color : (Color)(new Color32(8, 8, 8, 255));
                            }
                        }

                        if (cockpit_hud_texture != null)
                        {
                            if (tc != txt_color)
                            {
                                Rect r2 = warn_rect;
                                r2.Inset(1, 1, 1, 1);
                                cockpit_hud_texture.FillRect(r2, tc);
                                tc = Color.black;
                            }

                            warn_rect.y += 4;

                            hud_font.SetColor(tc);
                            hud_font.DrawText(abrv, -1,
                            warn_rect,
                            TextFormat.DT_CENTER | TextFormat.DT_SINGLELINE,
                            cockpit_hud_texture);

                            warn_rect.y -= 4;
                        }
                        else
                        {
                            DrawHUDText(TXT.TXT_CAUTION_TXT + index,
                            abrv,
                            warn_rect,
                            TextFormat.DT_CENTER);

                            hud_text[(int)TXT.TXT_CAUTION_TXT + index].color = tc;
                        }
                    }

                    x += box_width;

                    if (--c <= 0)
                    {
                        c = cockpit_hud_texture != null ? 3 : 4;
                        x = box_left;
                        y += row_height;
                    }
                }

                if (Game.RealTime() - blink > 500)
                    blink = Game.RealTime();

                // reset for next time
                SetStatusColor(ShipSystem.STATUS.NOMINAL);
            }
        }

        public virtual void DrawInstructions()
        {
            if (ship == null) return;

            if (Game.MaxTexSize() > 128)
            {
                instr_left_sprite.Show();
                instr_right_sprite.Show();
            }

            int ninst = 0;
            int nobj = 0;
            Element elem = ship.GetElement();

            if (elem != null)
            {
                ninst = elem.NumInstructions();
                nobj = elem.NumObjectives();
            }

            Rect r = new Rect(width / 2 - 143, height - 105, 290, 17);

            if (ninst != 0)
            {
                int npages = ninst / 6 + ((ninst % 6) != 0 ? 1 : 0);

                if (inst_page >= npages)
                    inst_page = npages - 1;
                else if (inst_page < 0)
                    inst_page = 0;

                int first = inst_page * 6;
                int last = first + 6;
                if (last > ninst) last = ninst;

                TXT n = TXT.TXT_CAUTION_TXT;

                for (int i = first; i < last; i++)
                {
                    hud_text[(int)n].color = standard_txt_colors[color];
                    DrawHUDText(n++, FormatInstruction(elem.GetInstruction(i)), r, TextFormat.DT_LEFT, HUD_CASE.HUD_MIXED_CASE);
                    r.y += 14;
                }

                string page;
                page = string.Format("{0} / {1}", inst_page + 1, npages);
                r = new Rect(width / 2 + 40, height - 16, 110, 16);
                DrawHUDText(TXT.TXT_INSTR_PAGE, page, r, TextFormat.DT_CENTER, HUD_CASE.HUD_MIXED_CASE);
            }

            else if (nobj != 0)
            {
                TXT n = TXT.TXT_CAUTION_TXT;

                for (int i = 0; i < nobj; i++)
                {
                    string desc;
                    desc = string.Format("* {0}", elem.GetObjective(i).GetShortDescription());
                    hud_text[(int)n].color = standard_txt_colors[color];
                    DrawHUDText(n++, desc, r, TextFormat.DT_LEFT, HUD_CASE.HUD_MIXED_CASE);
                    r.y += 14;
                }
            }

            else
            {
                hud_text[(int)TXT.TXT_CAUTION_TXT].color = standard_txt_colors[color];
                DrawHUDText(TXT.TXT_CAUTION_TXT, Game.GetText("HUDView.No-Instructions"), r, TextFormat.DT_LEFT, HUD_CASE.HUD_MIXED_CASE);
            }
        }
        public virtual void DrawMessages()
        {
            bool message_queue_empty = true;

            // age messages:
            for (int i = 0; i < MAX_MSG; i++)
            {
                if (msg_time[i] > 0)
                {
                    msg_time[i] -= Game.GUITime();

                    if (msg_time[i] <= 0)
                    {
                        msg_time[i] = 0;
                        msg_text[i] = "";
                    }

                    message_queue_empty = false;
                }
            }

            if (!message_queue_empty)
            {
                // advance message pipeline:
                for (int i = 0; i < MAX_MSG; i++)
                {
                    if (msg_time[0] == 0)
                    {
                        for (int j = 0; j < MAX_MSG - 1; j++)
                        {
                            msg_time[j] = msg_time[j + 1];
                            msg_text[j] = msg_text[j + 1];
                        }

                        msg_time[MAX_MSG - 1] = 0;
                        msg_text[MAX_MSG - 1] = "";
                    }
                }

                // draw messages:
                for (int i = 0; i < MAX_MSG; i++)
                {
                    TXT index = TXT.TXT_MSG_1 + i;

                    if (msg_time[i] > 0)
                    {
                        Rect msg_rect = new Rect(10, 95 + i * 10, width - 20, 12);
                        DrawHUDText(index, msg_text[i], msg_rect, TextFormat.DT_LEFT, HUD_CASE.HUD_MIXED_CASE);
                        if (msg_time[i] > 1)
                            hud_text[(int)index].color = txt_color;
                        else
                            hud_text[(int)index].color = txt_color.dim(0.5 + 0.5 * msg_time[i]);
                    }
                }
            }
        }


        public virtual void MouseFrame()
        {
            MouseController ctrl = MouseController.GetInstance();
            if (ctrl != null && ctrl.Active())
                return;

            if (mouse_index >= TXT.TXT_CAUTION_TXT && mouse_index <= TXT.TXT_LAST_CAUTION)
            {
                if (show_inst)
                {
                    if (mouse_index == TXT.TXT_INSTR_PAGE)
                    {
                        if (Mouse.X() > width / 2 + 125)
                            CycleInstructions(1);
                        else if (Mouse.X() < width / 2 + 65)
                            CycleInstructions(-1);
                    }
                    else
                        show_inst = false;
                }
                else
                {
                    CycleHUDWarn();
                }
                return;
            }

            Starshatter stars = Starshatter.GetInstance();
            if (mouse_index == TXT.TXT_PAUSED)
                stars.Pause(!Game.Paused());

            if (mouse_index == TXT.TXT_GEAR_DOWN)
                ship.ToggleGear();

            if (mouse_index == TXT.TXT_HUD_MODE)
            {
                CycleHUDMode();

                if (mode == HUDModes.HUD_MODE_OFF)
                    CycleHUDMode();
            }

            if (mouse_index == TXT.TXT_PRIMARY_WEP)
            {
                HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_WEP_MODE);
                ship.CyclePrimary();
            }

            if (mouse_index == TXT.TXT_SECONDARY_WEP)
            {
                HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_WEP_MODE);
                ship.CycleSecondary();
            }

            if (mouse_index == TXT.TXT_DECOY)
                ship.FireDecoy();

            if (mouse_index == TXT.TXT_SHIELD)
            {
                Shield shield = ship.GetShield();

                if (shield != null)
                {
                    double level = shield.GetPowerLevel();

                    Rect r = hud_text[(int)TXT.TXT_SHIELD].rect;
                    if (Mouse.X() < r.x + r.w * 0.75)
                        shield.SetPowerLevel(level - 10);
                    else
                        shield.SetPowerLevel(level + 10);

                    HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_SHIELD_LEVEL);
                }
            }

            if (mouse_index == TXT.TXT_AUTO)
                ship.TimeSkip();

            if (mouse_index >= TXT.TXT_NAV_INDEX && mouse_index <= TXT.TXT_NAV_ETR)
            {
                ship.SetAutoNav(!ship.IsAutoNavEngaged());
                SetHUDMode(HUDModes.HUD_MODE_TAC);
            }
        }
        public virtual HUDModes GetHUDMode() { return mode; }
        public virtual bool GetTacticalMode() { return tactical; }
        public virtual void SetTacticalMode(bool mode = true)
        {
            if (tactical != mode)
            {
                tactical = mode;

                if (tactical)
                {
                    hud_left_sprite.Hide();
                    hud_right_sprite.Hide();

                    for (int i = 0; i < 31; i++)
                        pitch_ladder[i].Hide();
                }
                else if (Game.MaxTexSize() > 128)
                {
                    hud_left_sprite.Show();
                    hud_right_sprite.Show();
                }
            }
        }
        public virtual int GetOverlayMode() { return overlay; }
        public virtual void SetOverlayMode(int mode = 1)
        {
            if (overlay != mode)
            {
                overlay = mode;
            }
        }


        public virtual void SetHUDMode(HUDModes m)
        {
            if (mode != m)
            {
                mode = m;

                if (mode > HUDModes.HUD_MODE_ILS || mode < HUDModes.HUD_MODE_OFF)
                    mode = HUDModes.HUD_MODE_OFF;

                if (ship != null && !ship.IsDropship() && mode == HUDModes.HUD_MODE_ILS)
                    mode = HUDModes.HUD_MODE_OFF;

                RestoreHUD();
            }
        }

        public virtual void CycleHUDMode()
        {
            mode++;

            if (arcade && mode != HUDModes.HUD_MODE_TAC)
                mode = HUDModes.HUD_MODE_OFF;

            else if (mode > HUDModes.HUD_MODE_ILS || mode < HUDModes.HUD_MODE_OFF)
                mode = HUDModes.HUD_MODE_OFF;

            else if (!ship.IsDropship() && mode == HUDModes.HUD_MODE_ILS)
                mode = HUDModes.HUD_MODE_OFF;

            RestoreHUD();
            HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_HUD_MODE);
        }
        public virtual Color CycleHUDColor()
        {
            HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_HUD_MODE);
            SetHUDColorSet(color + 1);
            return hud_color;
        }

        public virtual void SetHUDColorSet(int c)
        {
            color = c;
            if (color > NUM_HUD_COLORS - 1) color = 0;
            hud_color = standard_hud_colors[color];
            txt_color = standard_txt_colors[color];

            ColorizeBitmap(fpm_sprite, hud_color, true);
            ColorizeBitmap(hpm_sprite, hud_color, true);
            ColorizeBitmap(lead_sprite, txt_color * 1.25f, true);
            ColorizeBitmap(aim_sprite, hud_color, true);
            ColorizeBitmap(tgt1_sprite, hud_color, true);
            ColorizeBitmap(tgt2_sprite, hud_color, true);
            ColorizeBitmap(tgt3_sprite, hud_color, true);
            ColorizeBitmap(tgt4_sprite, hud_color, true);

            if (Game.MaxTexSize() > 128)
            {
                ColorizeBitmap(hud_left_air_sprite, hud_color);
                ColorizeBitmap(hud_right_air_sprite, hud_color);
                ColorizeBitmap(hud_left_fighter_sprite, hud_color);
                ColorizeBitmap(hud_right_fighter_sprite, hud_color);
                ColorizeBitmap(hud_left_starship_sprite, hud_color);
                ColorizeBitmap(hud_right_starship_sprite, hud_color);

                ColorizeBitmap(instr_left_sprite, hud_color);
                ColorizeBitmap(instr_right_sprite, hud_color);
                ColorizeBitmap(warn_left_sprite, hud_color);
                ColorizeBitmap(warn_right_sprite, hud_color);

                //ColorizeBitmap(pitch_ladder_pos, pitch_ladder_pos_shade, hud_color);
                //ColorizeBitmap(pitch_ladder_neg, pitch_ladder_neg_shade, hud_color);
                for (int i = 0; i < pitch_ladder.Length; i++)
                    ColorizeBitmap(pitch_ladder[i], hud_color);
            }

            ColorizeBitmap(icon_ship_sprite, txt_color);
            ColorizeBitmap(icon_target_sprite, txt_color);

            ColorizeBitmap(chase_left_sprite, hud_color, true);
            ColorizeBitmap(chase_right_sprite, hud_color, true);
            ColorizeBitmap(chase_top_sprite, hud_color, true);
            ColorizeBitmap(chase_bottom_sprite, hud_color, true);

            MFD.SetColor(hud_color);
            Hoop.SetColor(hud_color);

            for (int i = 0; i < 3; i++)
                mfd[i].SetText3DColor(txt_color);

            FontItem font = FontMgr.Find("HUD");
            if (font != null)
                font.SetColor(txt_color);

            for (TXT i = 0; i < TXT.TXT_LAST; i++)
                hud_text[(int)i].color = txt_color;
        }
        public virtual int GetHUDColorSet() { return color; }
        public virtual Color GetHUDColor() { return hud_color; }
        public virtual Color GetTextColor() { return txt_color; }
        public virtual Color Ambient()
        {
            if (sim == null || ship == null || mode == HUDModes.HUD_MODE_OFF)
                return Color.black;

            SimRegion rgn = sim.GetActiveRegion();

            if (rgn == null || !rgn.IsAirSpace())
                return Color.black;

            Color c = sim.GetStarSystem().Ambient();

            if (c.Red() > 32 || c.Green() > 32 || c.Blue() > 32)
                return Color.black;

            // if we get this far, the night-vision aid is on
            return night_vision_colors[color];
        }
        public virtual void ShowHUDWarn()
        {
            if (!show_warn)
            {
                show_warn = true;

                if (ship != null && ship.HullStrength() <= 40)
                {
                    // TOO OBNOXIOUS!!
                    HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_RED_ALERT);
                }
            }
        }

        public virtual void ShowHUDInst()
        {
            show_inst = true;
        }

        public virtual void HideHUDWarn()
        {
            show_warn = false;

            if (ship != null)
            {
                ship.ClearCaution();
                HUDSounds.StopSound(HUDSounds.SOUNDS.SND_RED_ALERT);
            }
        }

        public virtual void HideHUDInst()
        {
            show_inst = false;
        }

        public virtual void CycleHUDWarn()
        {
            if (!show_warn)
            {
                show_warn = true;

                if (ship != null && ship.HullStrength() <= 40)
                {
                    // TOO OBNOXIOUS!!
                    HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_RED_ALERT);
                }
            }
        }

        public virtual void CycleHUDInst()
        {
            show_inst = !show_inst;
            HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_HUD_WIDGET);
        }

        public virtual void CycleMFDMode(MFD.Modes mfd_index)
        {
            if (mfd_index < 0 || mfd_index > (MFD.Modes)2) return;

            MFD.Modes m = mfd[(int)mfd_index].GetMode();
            m++;

            if (mfd_index == (MFD.Modes)2)
            {
                if (m > MFD.Modes.MFD_MODE_SHIP)
                    m = MFD.Modes.MFD_MODE_OFF;
            }
            else
            {
                if (m > MFD.Modes.MFD_MODE_3D)
                    m = MFD.Modes.MFD_MODE_OFF;

                if (m == MFD.Modes.MFD_MODE_GAME)
                    m++;

                if (mfd_index != 0 && m == MFD.Modes.MFD_MODE_SHIP)
                    m++;
            }

            mfd[(int)mfd_index].SetMode(m);
            HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_MFD_MODE);
        }
        public virtual void CycleInstructions(int direction)
        {
            if (direction > 0)
                inst_page++;
            else
                inst_page--;
        }

        public virtual void RestoreHUD()
        {
            if (mode == HUDModes.HUD_MODE_OFF)
            {
                HideAll();
            }
            else
            {
                for (int i = 0; i < 3; i++)
                    mfd[i].Show();

                if (width > 640 || (!show_inst && !show_warn))
                {
                    icon_ship_sprite.Show();
                    icon_target_sprite.Show();
                }
                else
                {
                    icon_ship_sprite.Hide();
                    icon_target_sprite.Hide();
                }

                if (!tactical && Game.MaxTexSize() > 128)
                {
                    hud_left_sprite.Show();
                    hud_right_sprite.Show();
                }

                fpm_sprite.Show();

                if (ship != null && ship.IsStarship())
                    hpm_sprite.Show();

                if (gunsight == 0)
                    aim_sprite.Show();
                else
                    lead_sprite.Show();
            }
        }

        public virtual void TargetOff() { target = null; }
        public static Color MarkerColor(Contact contact)
        {
            Color c = new Color32(80, 80, 80, 255);

            if (contact != null)
            {
                Sim sim = Sim.GetSim();
                Ship ship = sim.GetPlayerShip();

                int c_iff = contact.GetIFF(ship);

                c = Ship.IFFColor(c_iff) * (float)contact.Age();

                if (contact.GetShot() != null && contact.Threat(ship))
                {
                    if (((Game.RealTime() / 500) & 1) != 0)
                        c = c * 2;
                    else
                        c = c * 0.5f;
                    throw new NotImplementedException();
                }
            }

            return c;
        }

        public static bool IsNameCrowded(int x, int y)
        {
            for (int i = 0; i < MAX_CONTACT; i++)
            {
                HUDText test = hud_text[(int)TXT.TXT_CONTACT_NAME + i];

                if (!test.hidden)
                {
                    Rect r = test.rect;

                    int dx = r.x - x;
                    int dy = r.y - y;
                    int d = dx * dx + dy * dy;

                    if (d <= 400)
                        return true;
                }

                test = hud_text[(int)TXT.TXT_CONTACT_INFO + i];

                if (!test.hidden)
                {
                    Rect r = test.rect;

                    int dx = r.x - x;
                    int dy = r.y - y;
                    int d = dx * dx + dy * dy;

                    if (d <= 400)
                        return true;
                }
            }

            return false;
        }
        public static bool IsMouseLatched()
        {
            bool result = mouse_in;

            if (!result)
            {
                HUDView hud = HUDView.GetInstance();

                for (int i = 0; i < 3; i++)
                    result = result || hud.mfd[i].IsMouseLatched();
            }

            return result;
        }

        public static HUDView GetInstance() { return hud_view; }
        public static void Message(string fmt, params object[] args)
        {
            if (fmt != null)
            {
                string msg = string.Format(fmt, args) + "\n";

                ErrLogger.PrintLine("{0}", msg);

                if (hud_view != null)
                {
                    int index = -1;

                    for (int i = 0; i < MAX_MSG; i++)
                    {
                        if (hud_view.msg_time[i] <= 0)
                        {
                            index = i;
                            break;
                        }
                    }

                    // no space; advance pipeline:
                    if (index < 0)
                    {
                        for (int i = 0; i < MAX_MSG - 1; i++)
                        {
                            hud_view.msg_text[i] = hud_view.msg_text[i + 1];
                            hud_view.msg_time[i] = hud_view.msg_time[i + 1];
                        }

                        index = MAX_MSG - 1;
                    }

                    hud_view.msg_text[index] = msg;
                    hud_view.msg_time[index] = 10;
                }
            }
        }

        public static void ClearMessages()
        {
            if (hud_view != null)
            {
                for (int i = 0; i < MAX_MSG - 1; i++)
                {
                    hud_view.msg_text[i] = "";
                    hud_view.msg_time[i] = 0;
                }
            }
        }

        public static void PrepareBitmap(string name, out Bitmap img)
        {
            img = null;
            string filename = "HUD/" + name;
            img = new Bitmap(filename);
            if (img == null || img.Texture == null)
            {
                ErrLogger.PrintLine("Texture {0} not found.", filename);
                return;
            }
#if TODO
            shades = null;
            var data = img.Texture.GetRawTextureData<Color32>();
            if (shades?.Length != data.Length)
            {
                shades = new Color32[data.Length];
            }
            data.CopyTo(shades);
            shades = null;
#endif
        }
        public static void TransferBitmap(Bitmap src, Bitmap img) { throw new NotImplementedException(); }
        //public static void ColorizeBitmap(Bitmap img, Color32[] shades, Color color, bool force_alpha = false)
        //{
        //}
        public static void ColorizeBitmap(SpriteGraphic sprite, Color color, bool force_alpha = false)
        {
            sprite.Colorize(color, force_alpha);
        }
        public static void ColorizeBitmap(GameObject img, Color color, bool force_alpha = false)
        {
            if (img != null)
            {
                var image = img.GetComponent<UnityEngine.UI.Image>();
                if (image != null) image.color = color;
            }
        }

        public static int GetGunsight() { return gunsight; }
        public static void SetGunsight(int s) { gunsight = s; }
        public static bool IsArcade() { return arcade; }
        public static void SetArcade(bool a) { arcade = a; }
        public static int DefaultColorSet() { return def_color_set; }
        public static void SetDefaultColorSet(int c) { def_color_set = c; }
        public static Color GetStatusColor(ShipSystem.STATUS status)
        {
            Color32 sc;

            switch (status)
            {
                default:
                case ShipSystem.STATUS.NOMINAL: sc = new Color32(32, 192, 32, 255); break;
                case ShipSystem.STATUS.DEGRADED: sc = new Color32(255, 255, 0, 255); break;
                case ShipSystem.STATUS.CRITICAL: sc = new Color32(255, 0, 0, 255); break;
                case ShipSystem.STATUS.DESTROYED: sc = new Color32(0, 0, 0, 255); break;
            }

            return sc;
        }
        public static bool ShowFPS() { return show_fps; }
        public static void ShowFPS(bool f) { show_fps = f; }

        public virtual bool Update(SimObject obj)
        {
            if (obj == ship)
            {
                if (target != null)
                    SetTarget(null);

                ship = null;

                for (int i = 0; i < 3; i++)
                    mfd[i].SetShip(ship);
            }

            if (obj == target)
            {
                target = null;
                PrepareBitmap("hud_icon", out icon_target);
                ColorizeBitmap(icon_target_sprite, txt_color);
            }

            return simObserver.Update(obj);
        }
        public virtual string GetObserverName()
        {
            return "HUDView";
        }


        protected string FormatInstruction(string instr)
        {
            int position = instr.IndexOf("$");
            if (position == -1)
                return instr;

            KeyMap keymap = Starshatter.GetInstance().GetKeyMap();

            string result = "";
            int lastpos = 0;
            while (position < instr.Length)
            {
                // Search restart from the character after the previous found substring
                result += instr.Substring(lastpos, position - lastpos);
                int posEnd = position + 1;
                while (posEnd < instr.Length && (Char.IsUpper(instr[posEnd]) || Char.IsDigit(instr[posEnd]) || (instr[posEnd] == '_'))) posEnd++;
                string action = instr.Substring(position + 1, posEnd - position - 1);
                int act = KeyMap.GetKeyAction(action);
                int key = keymap.FindMapIndex(act);
                string s2 = keymap.DescribeKey(key);
                if (string.IsNullOrEmpty(s2)) s2 = action;

                result += s2;
                position = posEnd;
                lastpos = posEnd;
                position = instr.IndexOf("$", position + 1);
                if (position == -1)
                {
                    result += instr.Substring(lastpos);
                    break;
                }
            }

            return result;
        }
        protected void SetStatusColor(ShipSystem.STATUS status)
        {
            {
                switch (status)
                {
                    default:
                    case ShipSystem.STATUS.NOMINAL: status_color = txt_color; break;
                    case ShipSystem.STATUS.DEGRADED: status_color = new Color32(255, 255, 0, 255); break;
                    case ShipSystem.STATUS.CRITICAL: status_color = new Color32(255, 0, 0, 255); break;
                    case ShipSystem.STATUS.DESTROYED: status_color = new Color32(0, 0, 0, 255); break;
                }
            }
        }

        protected enum HUD_CASE { HUD_MIXED_CASE, HUD_UPPER_CASE };

        protected void DrawDiamond(int x, int y, int r, Color c)
        {
            POINT[] diamond = new POINT[4];

            diamond[0].x = x;
            diamond[0].y = y - r;

            diamond[1].x = x + r;
            diamond[1].y = y;

            diamond[2].x = x;
            diamond[2].y = y + r;

            diamond[3].x = x - r;
            diamond[3].y = y;

            window.DrawPoly(4, diamond, c);
        }
        protected void DrawHUDText(TXT index, string txt, Rect rect, TextFormat align, HUD_CASE upcase = HUD_CASE.HUD_UPPER_CASE, bool box = false)
        {
            if (index < 0 || index >= TXT.TXT_LAST)
                return;

            HUDText ht = hud_text[(int)index];
            Color hc = ht.color;
            string txt_buf = txt;

            if (upcase == HUD_CASE.HUD_UPPER_CASE)
                txt_buf = txt.ToUpperInvariant();
            if (box)
            {
                ht.DrawText(txt_buf, rect, TextFormat.DT_LEFT | TextFormat.DT_SINGLELINE | TextFormat.DT_CALCRECT);

                if (align.HasFlag(TextFormat.DT_CENTER))
                {
                    int cx = width / 2;
                    rect.x = cx - rect.w / 2;
                }
            }
            if (cockpit_hud_texture == null && rect.Contains((int)Mouse.X(), (int)Mouse.Y()))
            {
                mouse_in = true;

                if (index <= TXT.TXT_LAST_ACTIVE)
                    hc = Color.white;

                if (Mouse.LButton() != 0 && mouse_latch == 0)
                {
                    mouse_latch = 2;
                    mouse_index = index;
                }
            }

            if (cockpit_hud_texture != null &&
                    index >= TXT.TXT_HUD_MODE &&
                    index <= TXT.TXT_TARGET_ETA &&
                    ht.font != big_font)
            {

                SpriteGraphic s = hud_sprite[0];

                int cx = (int)s.Location().X;
                int cy = (int)s.Location().Y;
                int w2 = s.Width() / 2;
                int h2 = s.Height() / 2;

                Rect txt_rect = rect;
                txt_rect.x -= (cx - w2);
                txt_rect.y -= (cy - h2);

                if (index == TXT.TXT_ICON_SHIP_TYPE)
                    txt_rect = new Rect(0, 500, 128, 12);

                else if (index == TXT.TXT_ICON_TARGET_TYPE)
                    txt_rect = new Rect(128, 500, 128, 12);

                ht.SetColor(hc);
                ht.DrawText(txt_buf, txt_rect, align | TextFormat.DT_SINGLELINE, cockpit_hud_texture);
                ht.Hidden(false);
            }
            else
            {
                ht.SetColor(hc);
                ht.DrawText(txt_buf, rect, align | TextFormat.DT_SINGLELINE);
                ht.rect = rect;
                ht.Hidden(false);

                if (box)
                {
                    rect.Inflate(3, 2);
                    rect.h--;
                    //TODO window.DrawRect(rect, hud_color);
                }
            }
        }
        protected void HideHUDText(int index)
        {
            if (index >= (int)TXT.TXT_LAST)
                return;

            hud_text[index].Hidden(true);
        }

        protected void DrawOrbitalBody(OrbitalBody body)
        {
            if (body != null)
            {
                Point p = body.Rep().Location();

                projector.Transform(p);

                if (p.Z > 100)
                {
                    float r = (float)body.Radius();
                    r = projector.ProjectRadius(p, r);
                    projector.Project(p, false);

                    window.DrawEllipse((int)(p.X - r),
                    (int)(p.Y - r),
                    (int)(p.X + r),
                    (int)(p.Y + r),
                    ColorExtensions.Cyan);
                }

                foreach (OrbitalBody bodyiter in body.Satellites())
                {
                    DrawOrbitalBody(bodyiter);
                }
            }
        }
        public void Observe(SimObject obj)
        {
            throw new NotImplementedException();
        }

        public void Ignore(SimObject obj)
        {
            throw new NotImplementedException();
        }
        static ShipSystem.STATUS GetReactorStatus(Ship ship)
        {
            if (ship == null || ship.Reactors().Count < 1)
                return (ShipSystem.STATUS)(-1);

            ShipSystem.STATUS status = ShipSystem.STATUS.NOMINAL;
            bool maint = false;

            foreach (PowerSource s in ship.Reactors())
            {
                if (s.Status() < status)
                    status = s.Status();
            }

            if (maint && status == ShipSystem.STATUS.NOMINAL)
                status = ShipSystem.STATUS.MAINT;

            return status;
        }
        static ShipSystem.STATUS GetDriveStatus(Ship ship)
        {
            if (ship == null || ship.Drives().Count < 1)
                return (ShipSystem.STATUS)(-1);

            ShipSystem.STATUS status = ShipSystem.STATUS.NOMINAL;
            bool maint = false;

            foreach (Drive s in ship.Drives())
            {
                if (s.Status() < status)
                    status = s.Status();

                else if (s.Status() == ShipSystem.STATUS.MAINT)
                    maint = true;
            }

            if (maint && status == ShipSystem.STATUS.NOMINAL)
                status = ShipSystem.STATUS.MAINT;

            return status;
        }

        static ShipSystem.STATUS GetQuantumStatus(Ship ship)
        {
            if (ship == null || ship.GetQuantumDrive() == null)
                return (ShipSystem.STATUS)(-1);

            QuantumDrive s = ship.GetQuantumDrive();
            return s.Status();
        }

        static ShipSystem.STATUS GetThrusterStatus(Ship ship)
        {
            if (ship == null || ship.GetThruster() == null)
                return (ShipSystem.STATUS)(-1);

            Thruster s = ship.GetThruster();
            return s.Status();
        }

        static ShipSystem.STATUS GetShieldStatus(Ship ship)
        {
            if (ship == null)
                return (ShipSystem.STATUS)(-1);

            Shield s = ship.GetShield();
            Weapon d = ship.GetDecoy();

            if (s == null && d == null)
                return (ShipSystem.STATUS)(-1);

            ShipSystem.STATUS status = ShipSystem.STATUS.NOMINAL;
            bool maint = false;

            if (s != null)
            {
                if (s.Status() < status)
                    status = s.Status();

                else if (s.Status() == ShipSystem.STATUS.MAINT)
                    maint = true;
            }

            if (d != null)
            {
                if (d.Status() < status)
                    status = d.Status();

                else if (d.Status() == ShipSystem.STATUS.MAINT)
                    maint = true;
            }

            if (maint && status == ShipSystem.STATUS.NOMINAL)
                status = ShipSystem.STATUS.MAINT;

            return status;
        }

        static ShipSystem.STATUS GetWeaponStatus(Ship ship, int index)
        {
            if (ship == null || ship.Weapons().Count <= index)
                return (ShipSystem.STATUS)(-1);

            WeaponGroup group = ship.Weapons()[index];

            ShipSystem.STATUS status = ShipSystem.STATUS.NOMINAL;
            bool maint = false;

            foreach (Weapon s in group.GetWeapons())
            {
                if (s.Status() < status)
                    status = s.Status();

                else if (s.Status() == ShipSystem.STATUS.MAINT)
                    maint = true;
            }

            if (maint && status == ShipSystem.STATUS.NOMINAL)
                status = ShipSystem.STATUS.MAINT;

            return status;
        }

        static ShipSystem.STATUS GetSensorStatus(Ship ship)
        {
            if (ship == null || ship.GetSensor() == null)
                return (ShipSystem.STATUS)(-1);

            Sensor s = ship.GetSensor();
            Weapon p = ship.GetProbeLauncher();

            ShipSystem.STATUS status = s.Status();
            bool maint = s.Status() == ShipSystem.STATUS.MAINT;

            if (p != null)
            {
                if (p.Status() < status)
                    status = p.Status();

                else if (p.Status() == ShipSystem.STATUS.MAINT)
                    maint = true;
            }

            if (maint && status == ShipSystem.STATUS.NOMINAL)
                status = ShipSystem.STATUS.MAINT;

            return status;
        }

        static ShipSystem.STATUS GetComputerStatus(Ship ship)
        {
            if (ship == null || ship.Computers().Count < 1)
                return (ShipSystem.STATUS)(-1);

            ShipSystem.STATUS status = ShipSystem.STATUS.NOMINAL;
            bool maint = false;

            foreach (Computer s in ship.Computers())
            {
                if (s.Status() < status)
                    status = s.Status();

                else if (s.Status() == ShipSystem.STATUS.MAINT)
                    maint = true;
            }

            if (ship.GetNavSystem() != null)
            {
                NavSystem s = ship.GetNavSystem();

                if (s.Status() < status)
                    status = s.Status();

                else if (s.Status() == ShipSystem.STATUS.MAINT)
                    maint = true;
            }

            if (maint && status == ShipSystem.STATUS.NOMINAL)
                status = ShipSystem.STATUS.MAINT;

            return status;
        }

        static ShipSystem.STATUS GetFlightDeckStatus(Ship ship)
        {
            if (ship == null || ship.FlightDecks().Count < 1)
                return (ShipSystem.STATUS)(-1);

            ShipSystem.STATUS status = ShipSystem.STATUS.NOMINAL;
            bool maint = false;

            foreach (FlightDeck s in ship.FlightDecks())
            {
                if (s.Status() < status)
                    status = s.Status();

                else if (s.Status() == ShipSystem.STATUS.MAINT)
                    maint = true;
            }

            if (maint && status == ShipSystem.STATUS.NOMINAL)
                status = ShipSystem.STATUS.MAINT;

            return status;
        }
        public void Refresh()
        {
            sim = Sim.GetSim();
            mouse_in = false;

            if (sim == null || camview == null || projector == null)
            {
                return;
            }

            if (Mouse.LButton() == 0)
            {
                mouse_latch = 0;
                mouse_index = (TXT)(-1);
            }

            TXT mouse_index_old = mouse_index;

            SetShip(sim.GetPlayerShip());

            if (mode == HUDModes.HUD_MODE_OFF)
            {
                if (cockpit_hud_texture != null)
                {
                    cockpit_hud_texture.FillRect(0, 0, 512, 256, Color.black);
                }

                sim.ShowGrid(false);
                return;
            }

            if (cockpit_hud_texture != null && cockpit_hud_texture.Width() == 512)
            {
                Bitmap hud_bmp = null;

                if (hud_sprite[0] != null)
                {
                    hud_bmp = hud_sprite[0].Frame();
                    int bmp_w = hud_bmp.Width();
                    int bmp_h = hud_bmp.Height();

                    cockpit_hud_texture.BitBlt(0, 0, hud_bmp, 0, 0, bmp_w, bmp_h);
                }

                if (hud_sprite[1] != null)
                {
                    hud_bmp = hud_sprite[1].Frame();
                    int bmp_w = hud_bmp.Width();
                    int bmp_h = hud_bmp.Height();

                    cockpit_hud_texture.BitBlt(256, 0, hud_bmp, 0, 0, bmp_w, bmp_h);
                }

                if (hud_sprite[6] != null)
                {
                    if (hud_sprite[6].Hidden())
                    {
                        cockpit_hud_texture.FillRect(0, 384, 128, 512, Color.black);
                    }
                    else
                    {
                        hud_bmp = hud_sprite[6].Frame();
                        int bmp_w = hud_bmp.Width();
                        int bmp_h = hud_bmp.Height();

                        cockpit_hud_texture.BitBlt(0, 384, hud_bmp, 0, 0, bmp_w, bmp_h);
                    }
                }

                if (hud_sprite[7] != null)
                {
                    if (hud_sprite[7].Hidden())
                    {
                        cockpit_hud_texture.FillRect(128, 384, 256, 512, Color.black);
                    }
                    else
                    {
                        hud_bmp = hud_sprite[7].Frame();
                        int bmp_w = hud_bmp.Width();
                        int bmp_h = hud_bmp.Height();

                        cockpit_hud_texture.BitBlt(128, 384, hud_bmp, 0, 0, bmp_w, bmp_h);
                    }
                }

                for (int i = 8; i < 32; i++)
                {
                    if (hud_sprite[i] != null && !hud_sprite[i].Hidden())
                    {
                        SpriteGraphic s = hud_sprite[i];

                        int cx = (int)s.Location().X;
                        int cy = (int)s.Location().Y;
                        int w2 = s.Width() / 2;
                        int h2 = s.Height() / 2;

                        window.DrawBitmap(cx - w2, cy - h2, cx + w2, cy + h2, s.Frame(), Video.BLEND_TYPE.BLEND_ALPHA);
                    }
                }
            }
            else
            {
                for (int i = 0; i < 32; i++)
                {
                    if (hud_sprite[i] != null && !hud_sprite[i].Hidden())
                    {
                        SpriteGraphic s = hud_sprite[i];

                        int cx = (int)s.Location().X;
                        int cy = (int)s.Location().Y;
                        int w2 = s.Width() / 2;
                        int h2 = s.Height() / 2;

                        window.DrawBitmap(cx - w2, cy - h2, cx + w2, cy + h2, s.Frame(), Video.BLEND_TYPE.BLEND_ALPHA);
                    }
                }

                Video video = Video.GetInstance();

                for (int i = 0; i < 31; i++)
                {
                    SpriteGraphic s = pitch_ladder[i];

                    if (s != null && !s.Hidden())
                    {
                        s.Render2D(video);
                    }
                }
            }

            //DrawStarSystem();
            DrawMessages();

            if (ship != null)
            {
                // no hud in transition:
                if (ship.InTransition())
                {
                    transition = true;
                    HideAll();
                    return;
                }

                else if (transition)
                {
                    transition = false;
                    RestoreHUD();
                }

                CameraDirector cam_dir = CameraDirector.GetInstance();

                // everything is off during docking, except the final message:
                if (cam_dir != null && cam_dir.GetMode() == CameraDirector.CAM_MODE.MODE_DOCKING)
                {
                    docking = true;
                    HideAll();

                    if (ship.GetFlightPhase() == OP_MODE.DOCKING)
                    {
                        Rect dock_rect = new Rect(width / 2 - 100, height / 6, 200, 20);

                        if (ship.IsAirborne())
                            DrawHUDText(TXT.TXT_AUTO, Game.GetText("HUDView.SUCCESSFUL-LANDING"), dock_rect, TextFormat.DT_CENTER);
                        else
                            DrawHUDText(TXT.TXT_AUTO, Game.GetText("HUDView.DOCKING-COMPLETE"), dock_rect, TextFormat.DT_CENTER);
                    }
                    return;
                }
                else if (docking)
                {
                    docking = false;
                    RestoreHUD();
                }

                // go to NAV mode during autopilot:
                if (ship.GetNavSystem() != null && ship.GetNavSystem().AutoNavEngaged() && !arcade)
                    mode = HUDModes.HUD_MODE_NAV;

                SetTarget(ship.GetTarget());

                // internal view of HUD reticule
                if (CameraDirector.GetCameraMode() <= CameraDirector.CAM_MODE.MODE_CHASE)
                    SetTacticalMode(false);

                // external view
                else
                    SetTacticalMode(cockpit_hud_texture == null);

                sim.ShowGrid(tactical &&
                !ship.IsAirborne() &&
                CameraDirector.GetCameraMode() != CameraDirector.CAM_MODE.MODE_VIRTUAL);

                // draw HUD bars:
                DrawBars();

                if (missile_lock_sound != null)
                {
                    if (threat > 1)
                    {
                        long max_vol = AudioConfig.WrnVolume();
                        long volume = -1500;

                        if (volume > max_vol)
                            volume = max_vol;

                        missile_lock_sound.SetVolume(volume);
                        missile_lock_sound.Play();
                    }
                    else
                    {
                        missile_lock_sound.Stop();
                    }
                }

                DrawNav();
                DrawILS();

                // FOR DEBUG PURPOSES ONLY:
                // DrawObjective();

                if (overlay == 0)
                {
                    Rect fov_rect = new Rect(0, 10, width, 10);
                    int fov_degrees = 180 - 2 * (int)(projector.XAngle() * 180 / Math.PI);

                    if (fov_degrees > 90)
                        DrawHUDText(TXT.TXT_CAM_ANGLE, Game.GetText("HUDView.Wide-Angle"), fov_rect, TextFormat.DT_CENTER);

                    fov_rect.y = 20;
                    DrawHUDText(TXT.TXT_CAM_MODE, CameraDirector.GetModeName(), fov_rect, TextFormat.DT_CENTER);
                }

                DrawMFDs();

                instr_left_sprite.Hide();
                instr_right_sprite.Hide();
                warn_left_sprite.Hide();
                warn_right_sprite.Hide();

                if (cockpit_hud_texture != null)
                    cockpit_hud_texture.FillRect(256, 384, 512, 512, Color.black);

                if (show_inst)
                {
                    DrawInstructions();
                }

                else if (!arcade)
                {
                    if (ship.MasterCaution() && !show_warn)
                        ShowHUDWarn();

                    if (show_warn)
                        DrawWarningPanel();
                }

                if (width > 640 || (!show_inst && !show_warn))
                {
                    Rect icon_rect = new Rect(120, height - 24, 128, 16);

                    if (ship != null)
                        DrawHUDText(TXT.TXT_ICON_SHIP_TYPE, ship.DesignName(), icon_rect, TextFormat.DT_CENTER);

                    icon_rect.x = width - 248;

                    if (target != null && target.Type() == (int)SimObject.TYPES.SIM_SHIP)
                    {
                        Ship tship = (Ship)target;
                        DrawHUDText(TXT.TXT_ICON_TARGET_TYPE, tship.DesignName(), icon_rect, TextFormat.DT_CENTER);
                    }
                }
            }
            else
            {
                if (target != null)
                {
                    SetTarget(null);
                }
            }

            // latch mouse down to prevent dragging into a control:
            if (Mouse.LButton() == 1)
                mouse_latch = 1;

            if ((int)mouse_index > -1 && mouse_index_old != mouse_index)
                MouseFrame();
        }

        protected SimObserver simObserver = new SimObserver();

        protected Projector projector;
        protected CameraView camview;

        protected Graphic hudNode;

        protected int width, height, aw, ah;
        protected double xcenter, ycenter;

        protected Sim sim;
        protected Ship ship;
        protected SimObject target;

        protected SimRegion active_region;

        protected Bitmap cockpit_hud_texture;

        protected Color hud_color;
        protected Color txt_color;
        protected Color status_color;

        protected bool show_warn;
        protected bool show_inst;
        protected int inst_page;
        protected int threat;

        protected HUDModes mode;
        protected int color;
        protected bool tactical;
        protected int overlay;
        protected bool transition;
        protected bool docking;

        protected MFD[] mfd = new MFD[3];

        protected SpriteGraphic[] pitch_ladder = new SpriteGraphic[31];
        protected SpriteGraphic[] hud_sprite = new SpriteGraphic[32];

        protected Solid az_ring;
        protected Solid az_pointer;
        protected Solid el_ring;
        protected Solid el_pointer;
        protected double compass_scale;

        protected const int MAX_MSG = 6;
        protected string[] msg_text = new string[MAX_MSG];
        protected double[] msg_time = new double[MAX_MSG];

        protected static HUDView hud_view = null;
        protected static bool arcade = false;
        protected static bool show_fps = false;
        protected static int gunsight = 1;
        protected static int def_color_set = 1;

        protected static bool mouse_in = false;
        protected static int mouse_latch = 0;
        protected static TXT mouse_index = (TXT)(-1);

        protected static ShipSystem.STATUS ship_status = ShipSystem.STATUS.NOMINAL;
        protected static ShipSystem.STATUS tgt_status = ShipSystem.STATUS.NOMINAL;

#if TODO
        protected static GameObject hud_air;
        protected static GameObject hud_fighter;
        protected static GameObject hud_starship;
        protected static GameObject instr;
        protected static GameObject warn;
        protected static GameObject lead;
        protected static GameObject cross;
        protected static GameObject cross1;
        protected static GameObject cross2;
        protected static GameObject cross3;
        protected static GameObject cross4;
        protected static GameObject fpm;
        protected static GameObject hpm;
        protected static GameObject pitch_ladder_pos;
        protected static GameObject pitch_ladder_neg;
        protected static GameObject chase_left;
        protected static GameObject chase_right;
        protected static GameObject chase_top;
        protected static GameObject chase_bottom;
        protected static GameObject icon_ship;
        protected static GameObject icon_target;
#endif

        public const int NUM_HUD_COLORS = 4;

        public static Color32[] standard_hud_colors = new Color32[NUM_HUD_COLORS]{
                        new  Color32(130,190,140,255),  // green
                        new Color32(130,200,220,255),  // cyan
                        new Color32(250,170, 80,255),  // orange
                        // Color32(220,220,100,255),  // yellow
                        new Color32( 16, 16, 16,255)   // dark gray
                };

        public static Color32[] standard_txt_colors = new Color32[NUM_HUD_COLORS]{
                        new  Color32(150,200,170,255),  // green w/ green gray
                        new  Color32(220,220,180,255),  // cyan  w/ light yellow
                        new  Color32(220,220, 80,255),  // orange w/ yellow
                        // Color32(180,200,220,255),  // yellow w/ white
                        new  Color32( 32, 32, 32,255)   // dark gray
                };

        protected static Color32[] night_vision_colors = new Color32[NUM_HUD_COLORS]{
                        new  Color32( 20, 80, 20,255),  // green
                        new  Color32( 30, 80, 80,255),  // cyan
                        new  Color32( 80, 80, 20,255),  // yellow
                        // Color32(180,200,220,255),  // not used
                        new  Color32(  0,  0,  0,255)   // no night vision
                };

        protected static HUDText[] hud_text = new HUDText[(int)TXT.TXT_LAST];
        protected static GameObject prefabHUDView = null;

        protected static FontItem hud_font = null;
        protected static FontItem big_font = null;
        private static int numCanvas = 0;


        static Bitmap hud_left_air;
        static Bitmap hud_right_air;
        static Bitmap hud_left_fighter;
        static Bitmap hud_right_fighter;
        static Bitmap hud_left_starship;
        static Bitmap hud_right_starship;
        static Bitmap instr_left;
        static Bitmap instr_right;
        static Bitmap warn_left;
        static Bitmap warn_right;
        static Bitmap lead;
        static Bitmap cross;
        static Bitmap cross1;
        static Bitmap cross2;
        static Bitmap cross3;
        static Bitmap cross4;
        static Bitmap fpm;
        static Bitmap hpm;
        static Bitmap pitch_ladder_pos;
        static Bitmap pitch_ladder_neg;
        static Bitmap chase_left;
        static Bitmap chase_right;
        static Bitmap chase_top;
        static Bitmap chase_bottom;
        static Bitmap icon_ship;
        static Bitmap icon_target;


        static SpriteGraphic hud_left_air_sprite = null;
        static SpriteGraphic hud_right_air_sprite = null;
        static SpriteGraphic hud_left_fighter_sprite = null;
        static SpriteGraphic hud_right_fighter_sprite = null;
        static SpriteGraphic hud_left_starship_sprite = null;
        static SpriteGraphic hud_right_starship_sprite = null;
        static SpriteGraphic hud_left_sprite = null;
        static SpriteGraphic hud_right_sprite = null;
        static SpriteGraphic fpm_sprite = null;
        static SpriteGraphic hpm_sprite = null;
        static SpriteGraphic lead_sprite = null;
        static SpriteGraphic aim_sprite = null;
        static SpriteGraphic tgt1_sprite = null;
        static SpriteGraphic tgt2_sprite = null;
        static SpriteGraphic tgt3_sprite = null;
        static SpriteGraphic tgt4_sprite = null;
        static SpriteGraphic chase_left_sprite = null;
        static SpriteGraphic chase_right_sprite = null;
        static SpriteGraphic chase_top_sprite = null;
        static SpriteGraphic chase_bottom_sprite = null;
        static SpriteGraphic instr_left_sprite = null;
        static SpriteGraphic instr_right_sprite = null;
        static SpriteGraphic warn_left_sprite = null;
        static SpriteGraphic warn_right_sprite = null;
        static SpriteGraphic icon_ship_sprite = null;
        static SpriteGraphic icon_target_sprite = null;

        static Sound missile_lock_sound;

    }
    public class HUDText
    {
        public FontItem font;
        public Color color;
        public Rect rect;
        public bool hidden;
        public GameObject parent;
        protected GameObject txtGameObj;

        public GameObject text;
        protected static GameObject textPrefab = null;
        private static ulong textCnt = 0;
        public static void Initialize()
        {
            if (textPrefab != null) return; //TODO REVIEW 
            textPrefab = Resources.Load("Prefabs/HUDText") as GameObject;
        }
        public void SetColor(Color c)
        {
            color = c;
            if (txtGameObj != null)
            {
                var textCompoment = txtGameObj.GetComponent<UnityEngine.UI.Text>();
                textCompoment.color = this.color;
            }
        }
        public void DrawText(string txt, Rect text_rect, TextFormat format, Bitmap texture = null)
        {
            this.rect = text_rect;

            if (textPrefab == null) return;
            txtGameObj = GameObject.Instantiate(textPrefab, parent.transform);
            txtGameObj.name = string.Format("HUDText{0:X8}", textCnt++);
            var rectTransform = txtGameObj.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector3(-rect.x + rect.w / 2, rect.y - rect.h / 2, 1);
            rectTransform.sizeDelta = new Vector2(rect.w, rect.h);

            var textCompoment = txtGameObj.GetComponent<UnityEngine.UI.Text>();
            textCompoment.font = this.font.font;
            textCompoment.color = this.color;
            textCompoment.text = txt;

            if (format.HasFlag(TextFormat.DT_LEFT))
                textCompoment.alignment = TextAnchor.MiddleLeft;
            else if (format.HasFlag(TextFormat.DT_RIGHT))
                textCompoment.alignment = TextAnchor.MiddleRight;

            txtGameObj.Show();
        }
        public void Hidden(bool hidden)
        {
            this.hidden = hidden;
            if (txtGameObj != null)
            {
                txtGameObj.SetActive(!hidden);
            }
        }
        internal void Destroy()
        {
            if (txtGameObj != null)
                UnityEngine.Object.Destroy(txtGameObj);
            txtGameObj = null;
        }
    }


    /// <summary>
    /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-drawtext
    /// </summary>
    [Flags]
    public enum TextFormat
    {
        DT_BOTTOM = 1,      // Justifies the text to the bottom of the rectangle. This value is used only with the DT_SINGLELINE value.
        DT_VCENTER = 2,     // Centers text vertically. This value is used only with the DT_SINGLELINE value.
        DT_TOP = 4,         // Justifies the text to the top of the rectangle.
        DT_SINGLELINE = 8,  // Displays text on a single line only. Carriage returns and line feeds do not break the line.
        DT_LEFT = 16,       // Aligns text to the left.
        DT_CENTER = 32,     // Centers text horizontally in the rectangle.
        DT_RIGHT = 64,      // Aligns text to the right.
        DT_CALCRECT = 128,
        DT_WORDBREAK
    }

}
