﻿using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Gen.Misc
{
    public static class GameObjectExtensions
    {
        public static void Hide(this GameObject gameObj)
        {
            gameObj.SetActive(false); // false to hide, true to show
        }

        public static void Show(this GameObject gameObj)
        {
            gameObj.SetActive(true); // false to hide, true to show
        }

        public static void MoveTo(this GameObject gameObj, Point p)
        {
            var rectTransform = gameObj.GetComponent<RectTransform>();
            rectTransform.localPosition = new Vector3((float)p.X, (float)p.Y, (float)p.Z);
            ErrLogger.PrintLine("MoveTo is not yet implemented");
        }
        public static void MoveTo(this GameObject gameObj, Stars.Rect rect)
        {
        }
        public static void Reshape(this GameObject gameObj, float w, float h)
        {
            ErrLogger.PrintLine("Reshape is not yet implemented");
        }
        public static void SetAnimation(this GameObject gameObj, GameObject other)
        {
            gameObj = other;
        }
        public static void AddWindow(this GameObject gameObj, GameObject win)
        {
        }
        public static void AddWindow(this GameObject gameObj, ActiveWindow win)
        {
        }
        public static void DelWindow(this GameObject gameObj, ActiveWindow win)
        {
        }
        public static void DelWindow(this GameObject gameObj, GameObject win)
        {
        }
        public static void AddView(this GameObject gameObj, IView win)
        {
        }
        public static void DelView(this GameObject gameObj, IView win)
        {
        }

        public static float Width(this GameObject gameObj)
        {
            return gameObj.GetComponent<RectTransform>().rect.width;
        }
        public static float Height(this GameObject gameObj)
        {
            return gameObj.GetComponent<RectTransform>().rect.height;
        }
        public static Stars.Rect GetRect(this GameObject gameObj)
        {
            ErrLogger.PrintLine("GetRect is not yet implemented");
            return new Stars.Rect(); //TODO
        }

        public static void DrawLine(this GameObject gameObj, int x1, int y1, int x2, int y2, Color c, Video.BLEND_TYPE blend = 0)
        { }

        public static void DrawLine(this GameObject gameObj, double x1, double y1, double x2, double y2, Color c, Video.BLEND_TYPE blend = 0)
        { }

        public static void Print(this GameObject gameObj, int x1, int y1, string txt)
        { }
        public static void SetFont(this GameObject gameObj, Font font)
        { }
        public static void SetFont(this GameObject gameObj, FontItem font)
        { }

        public static void Print(this GameObject gameObj, double x1, double y1, string txt)
        { }
        public static void FillRect(this GameObject gameObj, double x1, double y1, double x2, double y2, Color c)
        { }
        public static void DrawRect(this GameObject gameObj, double x1, double y1, double x2, double y2, Color c)
        { }
        public static void DrawEllipse(this GameObject gameObj, double x1, double y1, double x2, double y2, Color c,
                                                                Video.BLEND_TYPE blend = 0)
        { }
        public static void FillEllipse(this GameObject gameObj, double x1, double y1, double x2, double y2, Color c,
                                                                Video.BLEND_TYPE blend = 0)
        { }
        public static void DrawBitmap(this GameObject gameObj, double x1, double y1, double x2, double y2,
                                                               Bitmap bmp, Video.BLEND_TYPE blend)
        { }

    }
}
