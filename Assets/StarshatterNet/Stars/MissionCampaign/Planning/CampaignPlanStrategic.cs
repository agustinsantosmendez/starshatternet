﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CampaignPlan.h/CampaignPlan.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CampaignPlanStrategic prioritizes targets and defensible
    allied forces as the first step in force tasking.  This
    algorithm computes which enemy resources are most important
    to attack, based on the AI value of each combat group, and
    strategic weighting factors that help shape the strategy
    to the objectives for the current campaign.
*/
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Stars.MissionCampaign.Planning
{
#warning CampaignPlanStrategic class is still in development and is not recommended for production.
    public class CampaignPlanStrategic : CampaignPlan
    {
        public CampaignPlanStrategic(Campaign c) : base(c) { }
        // ~CampaignPlanStrategic() { }

        // operations:
        public override void ExecFrame()
        {
            if (campaign != null && campaign.IsActive())
            {
                if (Campaign.Stardate() - exec_time < 300)
                    return;

                foreach (CombatZone zone in campaign.GetZones())
                    zone.Clear();

                foreach (Combatant c in campaign.GetCombatants())
                {
                    CombatGroup force = c.GetForce();

                    force.CalcValue();

                    PlaceGroup(force);
                    ScoreCombatant(c);
                    ScoreNeeds(c);

                    force.ClearUnlockedZones();
                    AssignZones(c);
                    ResolveZoneMovement(force);
                }

                exec_time = Campaign.Stardate();
            }
        }


        protected void PlaceGroup(CombatGroup g)
        {
            if (g == null)
                return;

            string rgn = g.GetRegion();
            CombatZone zone = campaign.GetZone(rgn);

            // if we couldn't find anything suitable,
            // just pick a zone at random:
            if (zone == null && g.IsMovable())
            {
                int nzones = campaign.GetZones().Count;
                int n = RandomHelper.RandomIndex() % nzones;
                zone = campaign.GetZones()[n];

                string assigned_rgn;
                if (campaign.GetZone(rgn) == null)
                {
                    assigned_rgn = zone.GetRegions()[0];
                    g.AssignRegion(assigned_rgn);
                }
            }

            if (zone != null && !zone.HasGroup(g))
                zone.AddGroup(g);

            foreach (CombatGroup iter in g.GetComponents())
                PlaceGroup(iter);
        }

        protected void ScoreCombatant(Combatant c)
        {
            // prep lists:
            c.GetDefendList().Clear();
            c.GetTargetList().Clear();

            ScoreDefensible(c);

            foreach (Combatant iter in campaign.GetCombatants())
            {
                if (iter.GetIFF() > 0 && iter.GetIFF() != c.GetIFF())
                    ScoreTargets(c, iter);
            }

            // sort lists:
            c.GetDefendList().Sort();
            c.GetTargetList().Sort();
        }

        protected void ScoreDefensible(Combatant c)
        {
            if (c.GetForce() != null)
                ScoreDefend(c, c.GetForce());
        }

        protected void ScoreDefend(Combatant c, CombatGroup g)
        {
            if (g == null || g.IsReserve())
                return;

            if (g.IsDefensible())
            {
                g.SetPlanValue(g.Value());
                c.GetDefendList().Add(g);

                CombatZone zone = campaign.GetZone(g.GetRegion());
                ZoneForce force = null;

                if (zone != null)
                    force = zone.FindForce(c.GetIFF());

                if (force != null)
                    force.GetDefendList().Add(g);
            }

            foreach (CombatGroup iter in g.GetComponents())
            {
                ScoreDefend(c, iter);
            }
        }

        protected void ScoreTargets(Combatant c, Combatant t)
        {
            if (t.GetForce() != null)
                ScoreTarget(c, t.GetForce());
        }

        protected void ScoreTarget(Combatant c, CombatGroup g)
        {
            if (g == null || g.IntelLevel() <= Intel.INTEL_TYPE.SECRET)
                return;

            if (g.IsTargetable())
            {
                g.SetPlanValue(g.Value() * c.GetTargetStratFactor(g.Type()));
                c.GetTargetList().Add(g);

                CombatZone zone = campaign.GetZone(g.GetRegion());
                ZoneForce force = null;

                if (zone != null)
                    force = zone.FindForce(c.GetIFF());

                if (force != null)
                    force.GetTargetList().Add(g);
            }

            foreach (CombatGroup iter in g.GetComponents())
            {
                ScoreTarget(c, iter);
            }
        }

        protected void ScoreNeeds(Combatant c)
        {
            foreach (CombatZone zone in campaign.GetZones())
            {
                ZoneForce force = zone.FindForce(c.GetIFF());

                // clear needs:
                force.SetNeed(CombatGroup.GROUP_TYPE.CARRIER_GROUP, 0);
                force.SetNeed(CombatGroup.GROUP_TYPE.BATTLE_GROUP, 0);
                force.SetNeed(CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON, 0);
                force.SetNeed(CombatGroup.GROUP_TYPE.ATTACK_SQUADRON, 0);
                force.SetNeed(CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON, 0);
                force.SetNeed(CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON, 0);

                // what defensive assets are needed in this zone?
                foreach (CombatGroup def in force.GetDefendList())
                {
                    CombatGroup.GROUP_TYPE[] defender_type = CombatGroup.PreferredDefender(def.Type());
                    force.AddNeed(defender_type[0], def.Value());
                }

                // what offensive assets are needed in this zone?
                foreach (CombatGroup tgt in force.GetTargetList())
                {
                    CombatGroup.GROUP_TYPE[] attacker_type = CombatGroup.PreferredAttacker(tgt.Type());
                    force.AddNeed(attacker_type[0], tgt.Value());
                }
            }
        }

        // zone alocation:
        protected void BuildGroupList(CombatGroup g, List<CombatGroup> groups)
        {
            if (g == null || g.IsReserve())
                return;

            if (g.IsAssignable())
                groups.Add(g);

            foreach (CombatGroup iter in g.GetComponents())
                BuildGroupList(iter, groups);
        }
        protected void AssignZones(Combatant c)
        {
            // find the list of assignable groups, in priority order:
            List<CombatGroup> groups = new List<CombatGroup>();
            BuildGroupList(c.GetForce(), groups);
            groups.Sort();

            // for each group, assign a zone:
            // first pass: fighter and attack squadrons assigned to star bases
            foreach (CombatGroup g in groups)
            {
                CombatGroup.GROUP_TYPE gtype = g.Type();

                if (gtype == CombatGroup.GROUP_TYPE.ATTACK_SQUADRON ||
                        gtype == CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON ||
                        gtype == CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON)
                {
                    CombatGroup parent = g.GetParent();

                    if (parent != null && parent.Type() == CombatGroup.GROUP_TYPE.WING)
                        parent = parent.GetParent();

                    if (parent == null || parent.Type() == CombatGroup.GROUP_TYPE.CARRIER_GROUP)
                        continue;

                    // these groups are attached to fixed resources,
                    // so they must be assigned to the parent's zone:
                    CombatZone parent_zone = campaign.GetZone(parent.GetRegion());

                    if (parent_zone != null)
                    {
                        ZoneForce parent_force = parent_zone.FindForce(g.GetIFF());

                        if (parent_force != null)
                        {
                            g.SetAssignedZone(parent_zone);
                            parent_force.AddNeed(g.Type(), -(g.Value()));
                        }
                    }
                }
            }

            // second pass: carrier groups
            foreach (CombatGroup g in groups)
            {
                CombatGroup.GROUP_TYPE gtype = g.Type();

                if (gtype == CombatGroup.GROUP_TYPE.CARRIER_GROUP)
                {
                    int current_zone_need = 0;
                    int highest_zone_need = 0;
                    CombatZone highest_zone = null;
                    ZoneForce highest_force = null;
                    CombatZone current_zone = null;
                    ZoneForce current_force = null;

                    List<CombatZone> possible_zones = new List<CombatZone>();

                    if (g.IsZoneLocked())
                    {
                        current_zone = g.GetAssignedZone();
                        current_force = current_zone.FindForce(g.GetIFF());
                    }

                    else
                    {
                        foreach (CombatZone zone in campaign.GetZones())
                        {
                            ZoneForce force = zone.FindForce(g.GetIFF());
                            int need = force.GetNeed(CombatGroup.GROUP_TYPE.CARRIER_GROUP) +
                            force.GetNeed(CombatGroup.GROUP_TYPE.ATTACK_SQUADRON) +
                            force.GetNeed(CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON) +
                            force.GetNeed(CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON);

                            if (g.IsSystemLocked() && zone.System() != g.GetAssignedSystem())
                                continue;

                            possible_zones.Add(zone);

                            if (zone.HasRegion(g.GetRegion()))
                            {
                                current_zone_need = need;
                                current_zone = zone;
                                current_force = force;
                            }

                            if (need > highest_zone_need)
                            {
                                highest_zone_need = need;
                                highest_zone = zone;
                                highest_force = force;
                            }
                        }
                    }

                    CombatZone assigned_zone = current_zone;
                    ZoneForce assigned_force = current_force;

                    if (highest_zone_need > current_zone_need)
                    {
                        assigned_zone = highest_zone;
                        assigned_force = highest_force;
                    }

                    // if we couldn't find anything suitable,
                    // just pick a zone at random:
                    if (assigned_zone == null)
                    {
                        if (possible_zones.IsEmpty())
                            possible_zones.AddRange(campaign.GetZones());

                        int nzones = possible_zones.Count;
                        int n = RandomHelper.RandomIndex() % nzones;

                        assigned_zone = possible_zones[n];
                        assigned_force = assigned_zone.FindForce(g.GetIFF());
                    }

                    if (assigned_force != null && assigned_zone != null)
                    {
                        string assigned_rgn;
                        if (campaign.GetZone(g.GetRegion()) == null)
                        {
                            assigned_rgn = assigned_zone.GetRegions()[0];
                            g.AssignRegion(assigned_rgn);
                        }

                        g.SetAssignedZone(assigned_zone);
                        assigned_force.AddNeed(g.Type(), -(g.Value()));

                        // also assign the carrier's wing and squadrons to the same zone:
                        foreach (CombatGroup squadron in g.GetComponents())
                        {
                            squadron.SetAssignedZone(assigned_zone);
                            assigned_force.AddNeed(squadron.Type(), -(squadron.Value()));

                            if (squadron.Type() == CombatGroup.GROUP_TYPE.WING)
                            {
                                foreach (CombatGroup s in squadron.GetComponents())
                                {
                                    s.SetAssignedZone(assigned_zone);
                                    assigned_force.AddNeed(s.Type(), -(s.Value()));
                                }
                            }
                        }
                    }
                }
            }

            // third pass: everything else
            foreach (CombatGroup g in groups)
            {
                CombatGroup.GROUP_TYPE gtype = g.Type();

                if (gtype == CombatGroup.GROUP_TYPE.BATTLE_GROUP || gtype == CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON)
                {
                    int current_zone_need = 0;
                    int highest_zone_need = 0;
                    CombatZone highest_zone = null;
                    ZoneForce highest_force = null;
                    CombatZone current_zone = null;
                    ZoneForce current_force = null;

                    List<CombatZone> possible_zones = new List<CombatZone>();

                    if (g.IsZoneLocked())
                    {
                        current_zone = g.GetAssignedZone();
                        current_force = current_zone.FindForce(g.GetIFF());
                    }

                    else
                    {
                        foreach (CombatZone zone in campaign.GetZones())
                        {
                            ZoneForce force = zone.FindForce(g.GetIFF());
                            int need = force.GetNeed(g.Type());

                            if (g.IsSystemLocked() && zone.System() != g.GetAssignedSystem())
                                continue;

                            possible_zones.Add(zone);

                            // battle groups can do double-duty:
                            if (gtype == CombatGroup.GROUP_TYPE.BATTLE_GROUP)
                                need += force.GetNeed(CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON);

                            if (zone.HasRegion(g.GetRegion()))
                            {
                                current_zone_need = need;
                                current_zone = zone;
                                current_force = force;
                            }

                            if (need > highest_zone_need)
                            {
                                highest_zone_need = need;
                                highest_zone = zone;
                                highest_force = force;
                            }
                        }
                    }

                    if (highest_zone_need > current_zone_need)
                    {
                        g.SetAssignedZone(highest_zone);

                        if (highest_force != null)
                            highest_force.AddNeed(g.Type(), -(g.Value()));
                    }
                    else
                    {
                        if (current_zone == null)
                        {
                            if (possible_zones.IsEmpty())
                                possible_zones.AddRange(campaign.GetZones());

                            int nzones = possible_zones.Count;
                            int n = RandomHelper.RandomIndex() % nzones;

                            current_zone = possible_zones[n];
                            current_force = current_zone.FindForce(g.GetIFF());
                        }

                        g.SetAssignedZone(current_zone);

                        if (current_force != null)
                            current_force.AddNeed(g.Type(), -(g.Value()));

                        string assigned_rgn;
                        if (campaign.GetZone(g.GetRegion()) == null)
                        {
                            assigned_rgn = current_zone.GetRegions()[0];
                            g.AssignRegion(assigned_rgn);
                        }
                    }
                }
            }
        }
        protected void ResolveZoneMovement(CombatGroup g)
        {
            CombatZone zone = g.GetAssignedZone();
            bool move = false;

            if (zone != null && !zone.HasRegion(g.GetRegion()))
            {
                move = true;
                CombatZone old_zone = g.GetCurrentZone();
                if (old_zone != null)
                    old_zone.RemoveGroup(g);
                zone.AddGroup(g);
            }

            foreach (CombatGroup comp in g.GetComponents())
                ResolveZoneMovement(comp);

            // assign region last, to allow components to
            // resolve their zones:
            if (zone != null && move)
                g.AssignRegion(zone.GetRegions()[0]);
        }
    }
}
