﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:              nGenEx.lib
    ORIGINAL FILES:         FormatUtil.h/FormatUtil.cpp
    ORIGINAL AUTHOR:        John DiCamillo
    .NET AUTHOR:            Agustin Santos


    OVERVIEW
    ========
    Text formatting utilities
*/

using System;
using System.Globalization;

namespace StarshatterNet.Gen.Misc
{
    public static class FormatUtil
    {
        public static void FormatNumber(out string txt, double n)
        {
            double a = Math.Abs(n);

            if (a < 1e3)
                txt = String.Format(CultureInfo.InvariantCulture, "{0}", (int)(n));

            else if (a < 1e6)
                txt = String.Format(CultureInfo.InvariantCulture, "{0:#.#}  K", (n / 1e3));

            else if (a < 1e9)
                txt = String.Format(CultureInfo.InvariantCulture, "{0:#.#}  M", (n / 1e6));

            else if (a < 1e12)
                txt = String.Format(CultureInfo.InvariantCulture, "{0:#.#}  G", (n / 1e9));

            else if (a < 1e15)
                txt = String.Format(CultureInfo.InvariantCulture, "{0:#.#}  T", (n / 1e12));

            else
                txt = String.Format(CultureInfo.InvariantCulture, "{0:#.#e0}  G", n);
        }


        public static void FormatNumberExp(ref string txt, double n)
        {
            double a = Math.Abs(n);

            if (a < 100e3)
                txt = String.Format(CultureInfo.InvariantCulture, "{0}", (int)(n));

            else
                txt = String.Format(CultureInfo.InvariantCulture, "{0:#.#e0}", n);
        }

        private const int MINUTE = 60;
        private const int HOUR = 60 * MINUTE;
        private const int DAY = 24 * HOUR;

        public static void FormatTime(out string txt, double seconds)
        {
            int t = (int)seconds;

            int h = (t / HOUR);
            int m = ((t - h * HOUR) / MINUTE);
            int s = (t - h * HOUR - m * MINUTE);

            if (h > 0)
                txt = String.Format(CultureInfo.InvariantCulture, "{0:00}:{1:00}:{2:00}", h, m, s);
            else
                txt = String.Format(CultureInfo.InvariantCulture, "{0:00}:{1:00} ", m, s);
        }

        public static void FormatTimeOfDay(ref string txt, double seconds)
        {
            int t = (int)seconds;

            if (t >= DAY)
            {
                int d = t / DAY;
                t -= d * DAY;
            }

            int h = (t / HOUR);
            int m = ((t - h * HOUR) / MINUTE);
            int s = (t - h * HOUR - m * MINUTE);

            txt = String.Format(CultureInfo.InvariantCulture, "{0:00}:{1:00}:{2:00}", h, m, s);
        }

        public static void FormatDayTime(out string txt, double seconds, bool short_format = false)
        {
            int t = (int)seconds;
            int d = 1, h = 0, m = 0, s = 0;

            if (t >= DAY)
            {
                d = t / DAY;
                t -= d * DAY;
                d++;
            }

            if (t >= HOUR)
            {
                h = t / HOUR;
                t -= h * HOUR;
            }

            if (t >= MINUTE)
            {
                m = t / MINUTE;
                t -= m * MINUTE;
            }

            s = t;

            if (short_format)
                txt = String.Format(CultureInfo.InvariantCulture, "{0:00}/{1:00}:{2:00}:{3:00}", d, h, m, s);
            else
                txt = String.Format(CultureInfo.InvariantCulture, "Day {0:00} {1:00}:{2:00}:{3:00}", d, h, m, s);
        }

        public static void FormatDay(out string txt, double seconds)
        {
            int t = (int)seconds;
            int d = 1;

            if (t >= DAY)
            {
                d = t / DAY;
                t -= d * DAY;
                d++;
            }

            txt = String.Format(CultureInfo.InvariantCulture, "Day {0:00}", d);
        }

        public static string FormatTimeString(DateTime utc)
        {
            return utc.ToString("G", CultureInfo.InvariantCulture);
        }

        public static string FormatTimeString()
        {
            return FormatTimeString(DateTime.Now);
        }

        public static string SafeQuotes(string msg)
        {

            if (!string.IsNullOrEmpty(msg))
            {
                string safe_str = msg.Replace('"', '\'');
                return safe_str;
            }

            return msg;
        }
        public static string FormatTextReplace(string msg, string tgt, string val)
        {
            if (msg == null || tgt == null || val == null)
                return "";

            string result = msg.Replace(tgt, val);
            return result;
        }

#if TODO
        public static void FormatPoint(ref string txt, Point p);

        public const char* SafeString(const char* s);


        // scan msg and replace all C-style \x escape sequences
        // with their single-character values, leave orig unmodified
        public Text FormatTextEscape(const char* msg);
#endif
    }
}
