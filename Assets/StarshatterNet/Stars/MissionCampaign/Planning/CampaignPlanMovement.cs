﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CampaignPlanMovement.h/CampaignPlanMovement.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CampaignPlanMovement simulates random patrol movements
    of starship groups between missions.  This agitation
    keeps the ships from bunching up in the middle of a
    sector.
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign.Planning
{
#warning CampaignPlanMovement class is still in development and is not recommended for production.
    public class CampaignPlanMovement : CampaignPlan
    {
        public CampaignPlanMovement(Campaign c) : base(c) { }
        // ~CampaignPlanMovement() { }

        // operations:
        public override void ExecFrame()
        {
            if (campaign != null && campaign.IsActive())
            {
                if (Campaign.Stardate() - exec_time < 7200)
                    return;

                campaign.GetAllCombatUnits(-1, all_units);

                foreach (CombatUnit u in all_units)
                {
                    if (u.IsStarship() && !u.IsStatic())
                        MoveUnit(u);
                }

                all_units.Clear();

                exec_time = Campaign.Stardate();
            }
        }


        protected void MoveUnit(CombatUnit u)
        {
            if (u != null)
            {
                // starship repair:
                double damage = u.GetSustainedDamage();

                if (damage > 0 && u.GetDesign() != null)
                {
                    int percent = (int)(100 * damage / u.GetDesign().integrity);

                    if (percent > 50)
                    {
                        u.SetSustainedDamage(0.90 * damage);
                    }
                }

                Point loc = u.Location();
                Point dir = loc;
                double dist = dir.Normalize();

                const double MAX_RAD = 320e3;
                const double MIN_DIST = 150e3;

                if (dist < MAX_RAD)
                {
                    double scale = 1 - dist / MAX_RAD;

                    loc += dir * (RandomHelper.Random(30e3, 90e3) * scale) + RandomHelper.RandomDirection() * 10e3;

                    if (Math.Abs(loc.Z) > 20e3)
                        loc.Z *= 0.1;

                    u.MoveTo(loc);

                    CombatGroup g = u.GetCombatGroup();
                    if (g != null && g.Type() > CombatGroup.GROUP_TYPE.FLEET && g.GetFirstUnit() == u)
                    {
                        g.MoveTo(loc);

                        if (g.IntelLevel() > Intel.INTEL_TYPE.KNOWN)
                            g.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                    }
                }

                else if (dist > 1.25 * MAX_RAD)
                {
                    double scale = 1 - dist / MAX_RAD;

                    loc += dir * (RandomHelper.Random(80e3, 120e3) * scale) + RandomHelper.RandomDirection() * 3e3;

                    if (Math.Abs(loc.Z) > 20e3)
                        loc.Z *= 0.1;

                    u.MoveTo(loc);

                    CombatGroup g = u.GetCombatGroup();
                    if (g != null && g.Type() > CombatGroup.GROUP_TYPE.FLEET && g.GetFirstUnit() == u)
                    {
                        g.MoveTo(loc);

                        if (g.IntelLevel() > Intel.INTEL_TYPE.KNOWN)
                            g.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                    }
                }

                else
                {
                    loc += RandomHelper.RandomDirection() * 30e3;

                    if (Math.Abs(loc.Z) > 20e3)
                        loc.Z *= 0.1;

                    u.MoveTo(loc);

                    CombatGroup g = u.GetCombatGroup();
                    if (g != null && g.Type() > CombatGroup.GROUP_TYPE.FLEET && g.GetFirstUnit() == u)
                    {
                        g.MoveTo(loc);

                        if (g.IntelLevel() > Intel.INTEL_TYPE.KNOWN)
                            g.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                    }
                }

                CombatUnit closest_unit = null;
                double closest_dist = 1e6;

                foreach (CombatUnit unit in all_units)
                {
                    if (unit.GetCombatGroup() != u.GetCombatGroup() && unit.GetRegion() == u.GetRegion() && !unit.IsDropship())
                    {
                        Point delta = loc - unit.Location();
                        dist = delta.Normalize();

                        if (dist < closest_dist)
                        {
                            closest_unit = unit;
                            closest_dist = dist;
                        }
                    }
                }

                if (closest_unit != null && closest_dist < MIN_DIST)
                {
                    Point delta = loc - closest_unit.Location();
                    dist = delta.Normalize();

                    loc += delta * 1.1 * (MIN_DIST - closest_dist);

                    if (Math.Abs(loc.Z) > 20e3)
                        loc.Z *= 0.1;

                    u.MoveTo(loc);

                    CombatGroup g = u.GetCombatGroup();
                    if (g != null && g.Type() > CombatGroup.GROUP_TYPE.FLEET && g.GetFirstUnit() == u)
                    {
                        g.MoveTo(loc);

                        if (g.IntelLevel() > Intel.INTEL_TYPE.KNOWN)
                            g.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                    }
                }
            }
        }

        protected List<CombatUnit> all_units;

    }
}
