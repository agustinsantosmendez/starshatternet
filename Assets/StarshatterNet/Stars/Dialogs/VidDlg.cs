﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      VidDlg.h/VidDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using System;
using System.IO;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class VidDlg : FormWindow
    {
        public VidDlg(Screen s, Camera c, FormDef def, BaseScreen mgr, GameObject p) :
            base(s, 0, 0, s.Width(), s.Height(), c, p, "VidDlg")
        {
            manager = mgr;
            selected_mode = 0; selected_detail = 0; orig_gamma = 128;
            selected_card = 0; selected_tex_size = 0; selected_render = 0; selected_texture = 0;
            mode = null; tex_size = null; detail = null; texture = null; gamma = null; shadows = null; spec_maps = null;
            bump_maps = null; lens_flare = null; corona = null; nebula = null; dust = null;
            apply = null; cancel = null; vid_btn = null; aud_btn = null; ctl_btn = null; opt_btn = null; mod_btn = null;
            closed = true;
            stars = Starshatter.GetInstance();

            Init(def);
            orig_gamma = Game.GammaLevel();
        }
        public override void RegisterControls()
        {
            if (apply != null)
                return;

            tex_size = (ComboBox)FindControl(204);
            REGISTER_CLIENT(ETD.EID_SELECT, tex_size, OnTexSize);

            detail = (ComboBox)FindControl(205);
            REGISTER_CLIENT(ETD.EID_SELECT, detail, OnDetail);

            texture = (ComboBox)FindControl(206);
            REGISTER_CLIENT(ETD.EID_SELECT, texture, OnTexture);

            gamma = (Slider)FindControl(215);

            if (gamma != null)
            {
                gamma.SetRangeMin(32);
                gamma.SetRangeMax(224);
                gamma.SetStepSize(16);

                REGISTER_CLIENT(ETD.EID_CLICK, gamma, OnGamma);
            }

            lens_flare = (ComboBox)FindControl(211);
            corona = (ComboBox)FindControl(212);
            nebula = (ComboBox)FindControl(213);
            dust = (ComboBox)FindControl(214);
            shadows = (ComboBox)FindControl(222);
            spec_maps = (ComboBox)FindControl(223);
            bump_maps = (ComboBox)FindControl(224);

            apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, apply, OnApply);

            cancel = (Button)FindControl(2);
            REGISTER_CLIENT(ETD.EID_CLICK, cancel, OnCancel);

            vid_btn = (Button)FindControl(901);
            REGISTER_CLIENT(ETD.EID_CLICK, vid_btn, OnVideo);

            aud_btn = (Button)FindControl(902);
            REGISTER_CLIENT(ETD.EID_CLICK, aud_btn, OnAudio);

            ctl_btn = (Button)FindControl(903);
            REGISTER_CLIENT(ETD.EID_CLICK, ctl_btn, OnControls);

            opt_btn = (Button)FindControl(904);
            REGISTER_CLIENT(ETD.EID_CLICK, opt_btn, OnOptions);

            mod_btn = (Button)FindControl(905);
            if (mod_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, mod_btn, OnMod);
        }
        public override void Show()
        {
            base.Show();
#if TODO
            if (closed)
            {
                bool fullscreen = true;

                if (stars != null)
                {
                    selected_render = 9;
                    selected_card = 0;

                    int n = stars.MaxTexSize();

                    for (int i = 0; i < 7; i++)
                    {
                        if (n <= Math.Pow(2.0f, i + 6))
                        {
                            selected_tex_size = i;
                            break;
                        }
                    }

                    Video video = Game.GetVideo();

                    if (video)
                    {
                        if (shadows != null)
                            shadows.SetSelection(video.IsShadowEnabled());

                        if (spec_maps != null)
                            spec_maps.SetSelection(video.IsSpecMapEnabled());

                        if (bump_maps != null)
                            bump_maps.SetSelection(video.IsBumpMapEnabled());

                        fullscreen = video.IsFullScreen();
                    }

                    if (lens_flare != null)
                        lens_flare.SetSelection(stars.LensFlare());

                    if (corona != null)
                        corona.SetSelection(stars.Corona());

                    if (nebula != null)
                        nebula.SetSelection(stars.Nebula());

                    if (dust != null)
                        dust.SetSelection(stars.Dust());
                }

                selected_detail = Terrain.DetailLevel() - 2;
                selected_texture = true;

                if (mode != null)
                {
                    BuildModeList();
                    mode.SetSelection(selected_mode);
                    mode.SetEnabled(fullscreen);
                }

                if (tex_size != null)
                    tex_size.SetSelection(selected_tex_size);

                if (detail != null)
                    detail.SetSelection(selected_detail);

                if (texture != null)
                    texture.SetSelection(selected_texture);


                if (gamma != null)
                {
                    orig_gamma = Game.GammaLevel();
                    gamma.SetValue(orig_gamma);
                }
            }

            if (vid_btn != null) vid_btn.SetButtonState(1);
            if (aud_btn != null) aud_btn.SetButtonState(0);
            if (ctl_btn != null) ctl_btn.SetButtonState(0);
            if (opt_btn != null) opt_btn.SetButtonState(0);
            if (mod_btn != null) mod_btn.SetButtonState(0);

            closed = false;
#endif
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnApply(null, null);
            }
        }

        // Operations:
        public virtual void OnTexSize(ActiveWindow obj, AWEvent evnt)
        {
            selected_tex_size = tex_size.GetSelectedIndex();
        }
        public virtual void OnMode(ActiveWindow obj, AWEvent evnt)
        {
            selected_mode = mode.GetSelectedIndex();
        }
        public virtual void OnDetail(ActiveWindow obj, AWEvent evnt)
        {
            selected_detail = detail.GetSelectedIndex();
        }
        public virtual void OnTexture(ActiveWindow obj, AWEvent evnt)
        {
            selected_texture = texture.GetSelectedIndex();
        }
        public virtual void OnGamma(ActiveWindow obj, AWEvent evnt)
        {
            int g = gamma.GetValue();

            if (g >= 0 && g <= 255)
            {
                Game.SetGammaLevel(g);
            }
        }

        public virtual void Apply()
        {
            if (closed) return;
#if TODO
            int w = 800;
            int h = 600;
            int d = 32;
            int a = 1;
            int g = 128;
            int t = 2048;
            float bias = 0;

            string mode_desc = mode.GetSelectedItem();

            if (mode_desc == "800 x 600")
            {
                w = 800;
                h = 600;
            }
            else if (mode_desc == "1024 x 768")
            {
                w = 1024;
                h = 768;
            }
            else if (mode_desc == "1152 x 864")
            {
                w = 1152;
                h = 864;
            }
            else if (mode_desc == "1280 x 800")
            {
                w = 1280;
                h = 800;
            }
            else if (mode_desc == "1280 x 960")
            {
                w = 1280;
                h = 960;
            }
            else if (mode_desc == "1280 x 1024")
            {
                w = 1280;
                h = 1024;
            }
            else if (mode_desc == "1440 x 900")
            {
                w = 1440;
                h = 900;
            }
            else if (mode_desc == "1600 x 900")
            {
                w = 1600;
                h = 900;
            }
            else if (mode_desc == "1600 x 1200")
            {
                w = 1600;
                h = 1200;
            }

            if (mode_desc == "x 16")
                d = 16;
            else if (mode_desc == "x 32")
                d = 32;

            if (selected_tex_size != 0)
                t = (int)Math.Pow(2.0f, selected_tex_size + 6);

            bool video_change = false;

            Video video = Game.GetVideo();
            if (video)
            {
                const VideoSettings vs = video.GetVideoSettings();

                if (vs != null)
                    bias = vs.depth_bias;

                if (video.IsFullScreen())
                {
                    if (video.Width() != w)
                        video_change = true;

                    if (video.Height() != h)
                        video_change = true;

                    if (video.Depth() != d)
                        video_change = true;
                }
                else if (vs != null)
                {
                    w = vs.fullscreen_mode.width;
                    h = vs.fullscreen_mode.height;

                    if (vs.fullscreen_mode.format == VideoMode.Format.FMT_R5G6B5 ||
                            vs.fullscreen_mode.format == VideoMode.Format.FMT_R5G5B5)
                        d = 16;
                    else
                        d = 32;
                }

                if (Game.MaxTexSize() != t)
                    video_change = true;
            }

            File f = 0;

            if (video_change)
                fopen_s(f, "video2.cfg", "w");
            else
                fopen_s(f, "video.cfg", "w");

            if (gamma != null)
            {
                g = gamma.GetValue();
            }

            if (f != null)
            {
                fprintf(f, "VIDEO\n\n");
                fprintf(f, "width:     %4d\n", w);
                fprintf(f, "height:    %4d\n", h);
                fprintf(f, "depth:     %4d\n", d);
                fprintf(f, "\n");
                fprintf(f, "max_tex:   %d\n", (int)Math.Pow(2.0f, 6 + selected_tex_size));
                fprintf(f, "primary3D: %s\n", (a > 0) ? "true" : "false");
                fprintf(f, "gamma:     %4d\n", g);
                fprintf(f, "\n");
                fprintf(f, "terrain_detail_level:   %d\n", selected_detail + 2);
                fprintf(f, "terrain_texture_enable: %s\n", selected_texture ? "true" : "false");
                fprintf(f, "\n");
                fprintf(f, "shadows:   %s\n", shadows.GetSelectedIndex() ? "true" : "false");
                fprintf(f, "spec_maps: %s\n", spec_maps.GetSelectedIndex() ? "true" : "false");
                fprintf(f, "bump_maps: %s\n", bump_maps.GetSelectedIndex() ? "true" : "false");
                fprintf(f, "bias:      %f\n", bias);
                fprintf(f, "\n");
                fprintf(f, "flare:     %s\n", lens_flare.GetSelectedIndex() ? "true" : "false");
                fprintf(f, "corona:    %s\n", corona.GetSelectedIndex() ? "true" : "false");
                fprintf(f, "nebula:    %s\n", nebula.GetSelectedIndex() ? "true" : "false");
                fprintf(f, "dust:      %d\n", dust.GetSelectedIndex());

                if (CameraDirector.GetRangeLimit() != 300e3)
                    fprintf(f, "   cam_range_max: %f,\n", CameraDirector.GetRangeLimit());

                fclose(f);
            }

            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                if (video_change)
                    stars.RequestChangeVideo();
                else
                    stars.LoadVideoConfig("video.cfg");
            }

            closed = true;
#endif
        }
        public virtual void Cancel()
        {
            Game.SetGammaLevel(orig_gamma);
            closed = true;
        }

        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            manager.ApplyOptions();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            manager.CancelOptions();
        }

        public virtual void OnAudio(ActiveWindow obj, AWEvent evnt) { manager.ShowAudDlg(); }
        public virtual void OnVideo(ActiveWindow obj, AWEvent evnt) { manager.ShowVidDlg(); }
        public virtual void OnOptions(ActiveWindow obj, AWEvent evnt) { manager.ShowOptDlg(); }
        public virtual void OnControls(ActiveWindow obj, AWEvent evnt) { manager.ShowCtlDlg(); }
        public virtual void OnMod(ActiveWindow obj, AWEvent evnt) { manager.ShowModDlg(); }


        protected virtual void BuildModeList()
        {
            string mode_desc;
            Starshatter stars = Starshatter.GetInstance();

            mode.ClearItems();
            selected_mode = 0;

            if (Game.DisplayModeSupported(800, 600, 16)) mode.AddItem("800 x 600 x 16");
            if (Game.DisplayModeSupported(800, 600, 32)) mode.AddItem("800 x 600 x 32");

            if (Game.DisplayModeSupported(1024, 768, 16)) mode.AddItem("1024 x 768 x 16");
            if (Game.DisplayModeSupported(1024, 768, 32)) mode.AddItem("1024 x 768 x 32");

            if (Game.DisplayModeSupported(1152, 864, 16)) mode.AddItem("1152 x 864 x 16");
            if (Game.DisplayModeSupported(1152, 864, 32)) mode.AddItem("1152 x 864 x 32");

            if (Game.DisplayModeSupported(1280, 800, 16)) mode.AddItem("1280 x 800 x 16");
            if (Game.DisplayModeSupported(1280, 800, 32)) mode.AddItem("1280 x 800 x 32");

            if (Game.DisplayModeSupported(1280, 960, 16)) mode.AddItem("1280 x 960 x 16");
            if (Game.DisplayModeSupported(1280, 960, 32)) mode.AddItem("1280 x 960 x 32");

            if (Game.DisplayModeSupported(1280, 1024, 16)) mode.AddItem("1280 x 1024 x 16");
            if (Game.DisplayModeSupported(1280, 1024, 32)) mode.AddItem("1280 x 1024 x 32");

            if (Game.DisplayModeSupported(1440, 900, 16)) mode.AddItem("1440 x 900 x 16");
            if (Game.DisplayModeSupported(1440, 900, 32)) mode.AddItem("1440 x 900 x 32");

            if (Game.DisplayModeSupported(1600, 900, 16)) mode.AddItem("1600 x 900 x 16");
            if (Game.DisplayModeSupported(1600, 900, 32)) mode.AddItem("1600 x 900 x 32");

            if (Game.DisplayModeSupported(1600, 1200, 16)) mode.AddItem("1600 x 1200 x 16");
            if (Game.DisplayModeSupported(1600, 1200, 32)) mode.AddItem("1600 x 1200 x 32");

            if (Game.DisplayModeSupported(1680, 1050, 16)) mode.AddItem("1680 x 1050 x 16");
            if (Game.DisplayModeSupported(1680, 1050, 32)) mode.AddItem("1680 x 1050 x 32");
#if TODO
            Video video = Game.GetVideo();

            if (stars != null && video != null)
            {
                switch (video.Width())
                {
                    case 800: strcpy_s(mode_desc, "800 x 600 x "); break;
                    default:
                    case 1024: strcpy_s(mode_desc, "1024 x 768 x "); break;
                    case 1152: strcpy_s(mode_desc, "1152 x 864 x "); break;
                    case 1280:
                        if (video.Height() < 900)
                            strcpy_s(mode_desc, "1280 x 800 x ");
                        if (video.Height() < 1000)
                            strcpy_s(mode_desc, "1280 x 960 x ");
                        else
                            strcpy_s(mode_desc, "1280 x 1024 x ");
                        break;
                    case 1440: strcpy_s(mode_desc, "1440 x 900 x "); break;
                    case 1600:
                        if (video.Height() < 1000)
                            strcpy_s(mode_desc, "1600 x 900 x ");
                        else
                            strcpy_s(mode_desc, "1600 x 1200 x ");
                        break;
                }

                switch (video.Depth())
                {
                    default:
                    case 8: strcat_s(mode_desc, "8"); break;
                    case 16: strcat_s(mode_desc, "16"); break;
                    case 32: strcat_s(mode_desc, "32"); break;
                }

                for (int i = 0; i < mode.GetCount(); i++)
                {
                    if (!strcmp(mode.GetItem(i), mode_desc))
                        selected_mode = i;
                }
            }
#endif
            throw new NotImplementedException();
        }

        protected BaseScreen manager;
        protected Starshatter stars;

        protected ComboBox mode;
        protected ComboBox tex_size;
        protected ComboBox detail;
        protected ComboBox texture;

        protected ComboBox shadows;
        protected ComboBox bump_maps;
        protected ComboBox spec_maps;

        protected ComboBox lens_flare;
        protected ComboBox corona;
        protected ComboBox nebula;
        protected ComboBox dust;

        protected Slider gamma;

        protected Button aud_btn;
        protected Button vid_btn;
        protected Button opt_btn;
        protected Button ctl_btn;
        protected Button mod_btn;

        protected Button apply;
        protected Button cancel;

        protected int selected_render;
        protected int selected_card;
        protected int selected_tex_size;
        protected int selected_mode;
        protected int selected_detail;
        protected int selected_texture;
        protected int orig_gamma;

        protected bool closed;
    }
}