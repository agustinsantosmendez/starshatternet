﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Campaign.h/Campaign.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Campaign defines a strategic military scenario.  This class
    owns (or generates) the Mission list that defines the action
    in the campaign.
*/

namespace StarshatterNet.Stars.MissionCampaign
{
#warning MissionInfo class is still in development and is not recommended for production.
    public class MissionInfo
    {
        private const int TIME_NEVER = (int)1e9;

        public MissionInfo()
        {
            mission = null; start = 0; type = 0; id = 0; min_rank = 0; max_rank = 0;
            action_id = 0; action_status = 0; exec_once = 0;
            start_before = TIME_NEVER; start_after = 0;
        }

        //~MissionInfo();
        public bool Equals(MissionInfo other)
        {
            return (this.id == other.id);
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            MissionInfo other = (MissionInfo)obj;
            return (this.id == other.id);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return this.id.GetHashCode();
        }

        public static bool operator ==(MissionInfo mi1, MissionInfo mi2)
        {
            if (object.ReferenceEquals(mi1, mi2))
            {
                return true;     // if both the same object, or both null
            }
            // If one is null, but not both, return false.
            if (((object)mi1 == null) || ((object)mi2 == null))
            {
                return false;
            }

            return mi1.id == mi2.id;
        }

        public static bool operator !=(MissionInfo mi1, MissionInfo mi2)
        {
            return !(mi1 == mi2);
        }
        public static bool operator <(MissionInfo mi1, MissionInfo mi2) { return mi1.id < mi2.id; }

        public static bool operator >(MissionInfo mi1, MissionInfo mi2) { return mi1.id > mi2.id; }

        public static bool operator <=(MissionInfo mi1, MissionInfo mi2) { return mi1.id <= mi2.id; }

        public static bool operator >=(MissionInfo mi1, MissionInfo mi2) { return mi1.id >= mi2.id; }

        public bool IsAvailable()
        {
            Campaign campaign = Campaign.GetCampaign();
            Player player = Player.GetCurrentPlayer();
            CombatGroup player_group = campaign.GetPlayerGroup();

            if (campaign.GetTime() < start_after)
                return false;

            if (campaign.GetTime() > start_before)
                return false;

            if (region.Length != 0 && player_group.GetRegion() != region)
                return false;

            if (min_rank != 0 && player.Rank() < min_rank)
                return false;

            if (max_rank != 0 && player.Rank() > max_rank)
                return false;

            if (exec_once < 0)
                return false;

            if (exec_once > 0)
                exec_once = -1;

            return true;
        }

        public int id;
        public string name;
        public string player_info;
        public string description;
        public string system;
        public string region;
        public string script;
        public int start;
        public Mission.TYPE type;

        public int min_rank;
        public int max_rank;
        public int action_id;
        public int action_status;
        public int exec_once;
        public int start_before;
        public int start_after;

        public Mission mission;

    }
}
