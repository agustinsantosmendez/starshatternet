﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      WeaponDesign.h/WeaponDesign.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Weapon (gun or missile launcher) Design parameters class
*/
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Audio;
using StarshatterNet.Stars.Graphics.General;
using System;
using System.Collections.Generic;
using UnityEngine;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Serialization;

namespace StarshatterNet.Stars.SimElements
{
    public class WeaponDesign : IComparable
    {
        public const string DefaultFilename = "WeaponDesign.json";
        public const int MAX_STORES = 8;
        public enum DAMAGE
        {
            DMG_NORMAL = 0,
            DMG_EMP = 1,
            DMG_POWER = 2
        }

        public enum WeaponType
        {
            primary,
            missile,
            beam,
            drone
        }

        public WeaponDesign()
        {
            type = 0;
            secret = false;
            drone = false;
            primary = false;
            beam = false;
            flak = false;
            guided = 0;
            self_aiming = false;
            syncro = false;
            value = 0;
            decoy_type = 0;
            probe = false;
            target_type = 0;

            visible_stores = false;
            nstores = 0;
            nbarrels = 0;

            recharge_rate = 0.0f;
            refire_delay = 0.0f;
            salvo_delay = 0.0f;
            charge = 0.0f;
            min_charge = 0.0f;
            carry_mass = 0.0f;
            carry_resist = 0.0f;

            speed = 0.0f;
            life = 0.0f;
            mass = 0.0f;
            drag = 0.0f;
            thrust = 0.0f;
            roll_rate = 0.0f;
            pitch_rate = 0.0f;
            yaw_rate = 0.0f;
            roll_drag = 0.0f;
            pitch_drag = 0.0f;
            yaw_drag = 0.0f;

            min_range = 0.0f;
            max_range = 0.0f;
            max_track = 0.0f;

            graphic_type = 0;
            width = 0;
            length = 0;

            scale = 1.0f;
            explosion_scale = 0.0f;
            light = 0.0f;
            light_color = Color.white;
            flash_scale = 0.0f;
            flare_scale = 0.0f;

            spread_az = 0.0f;
            spread_el = 0.0f;

            beauty_img = null;
            turret_model = null;
            turret_base_model = null;
            animation = null;
            anim_length = 0;
            shot_img = null;
            shot_model = null;
            trail_img = null;
            flash_img = null;
            flare_img = null;
            sound_resource = null;

            ammo = -1;
            ripple_count = 0;
            capacity = 100.0f;
            damage = 1.0f;
            damage_type = 0;
            penetration = 1.0f;
            firing_cone = 0.0f;
            aim_az_max = 1.5f;
            aim_az_min = -1.5f;
            aim_az_rest = 0.0f;
            aim_el_max = 1.5f;
            aim_el_min = -1.5f;
            aim_el_rest = 0.0f;
            slew_rate = (float)(60 * ConstantsF.DEGREES);
            turret_axis = 0;
            lethal_radius = 500.0f;
            integrity = 100.0f;

            eject = new Vector3F(0.0f, -100.0f, 0.0f);

            det_range = 0.0f;
            det_count = 0;
            det_spread = (float)(Math.PI / 8);

            trail_length = 0;
            trail_width = 0;
            trail_dim = 0;

            for (int i = 0; i < MAX_STORES; i++)
            {
                muzzle_pts[i] = new Vector3F(0, 0, 0);
                attachments[i] = new Vector3F(0, 0, 0);
            }
        }

        public static void Initialize(string filename = DefaultFilename)
        {
            ErrLogger.PrintLine("Loading Weapon Designs '{0}'", filename);
            LoadDesign("Weapons/", filename);
        }
        private static void LoadDesign(string path, string filename, bool mod = false)
        {
            Serialization.WeaponDesignSerializer.JSONReadWeaponDesign(path, filename, mod, catalog, mod_catalog);
        }
        public static void Close()
        {
            catalog.Clear();
            mod_catalog.Clear();
        }
        public static WeaponDesign Get(WeaponType type)
        {
            WeaponDesign result = catalog.Find(wd => wd.type == type);

            if (result == null)
                result = mod_catalog.Find(wd => wd.type == type);

            return result;
        }

        public static WeaponDesign Find(string name)
        {
            WeaponDesign result = catalog.Find(wd => wd.name == name);

            if (result == null)
                result = mod_catalog.Find(wd => wd.name == name);

            if (result != null) return result;

            ErrLogger.PrintLine("WeaponDesign: no catalog entry for design '{0}', checking mods...", name);
            return FindModDesign(name);
        }

        public static WeaponDesign FindModDesign(string name)
        {
            string fname = name;
            fname += ".json";

            LoadDesign("Mods/Weapons/", fname, true);

            for (int i = 0; i < mod_catalog.Count; i++)
            {
                WeaponDesign d = mod_catalog[i];

                if (d.name == name)
                {
                    ErrLogger.PrintLine("WeaponDesign: found mod weapon '{0}'", d.name);
                    return d;
                }
            }

            ErrLogger.PrintLine("WeaponDesign: no mod found for design '{0}'", name);
            return null;
        }



        public static void ClearModCatalog()
        {
            mod_catalog.Clear();
        }

        public static int GetDesignList(List<string> designs)
        {
            foreach (var wd in catalog)
                designs.Add(wd.name);
            foreach (var wd in mod_catalog)
                designs.Add(wd.name);

            return designs.Count;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            WeaponDesign other = obj as WeaponDesign;
            if (other != null)
                return this.name.CompareTo(other.name);
            else
                throw new ArgumentException("Object is not a WeaponDesign");
        }


        // identification:
        public WeaponType type = 0;             // unique id
        public string name;
        public string group;
        public string description;      // background info for tactical reference
        public bool secret;           // don't display in the tactical reference

        public bool drone;            // visible to sensors?
        public bool primary;          // laser or missile?
        public bool beam;             // if laser, beam or bolt?
        public bool self_aiming;      // turret or fixed?
        public bool syncro;           // fire all barrels?
        public bool flak;             // splash damage
        public int guided;           // (0) straight, (1) pure pursuit, (2) lead pursuit
        public int value;            // AI importance of system
        public int decoy_type;       // Ship Classifcation of decoy signature
        public string decoy;
        public bool probe;            // is sensor probe?
        public CLASSIFICATION target_type;      // bitmask of acceptable target classes

        // for turrets:
        public Vector3F[] muzzle_pts = new Vector3F[MAX_STORES];    // default turret muzzle points
        public int nbarrels;                  // number of barrels on the turret

        // for missile hard points:
        public bool visible_stores;            // are external stores visible?
        public Vector3F[] attachments = new Vector3F[MAX_STORES];   // attachment points on the rail
        public int nstores;                   // number of stores on this hard point
        public Vector3F eject = new Vector3F(0.0f, -100.0f, 0.0f);                     // eject velocity from rail in 3D

        // auto-aiming arc
        public float firing_cone;      // maximum deflection in any orientation
        public float aim_az_max = 1.5f;       // maximum deflection in azimuth
        public float aim_az_min = -1.5f;       // minimum deflection in azimuth
        public float aim_az_rest = 0.0f;      // azimuth of turret at rest
        public float aim_el_max = 1.5f;       // maximum deflection in elevation
        public float aim_el_min = -1.5f;       // minimum deflection in elevation
        public float aim_el_rest = 0.0f;      // elevation of turret at rest
        public float slew_rate = MathHelper.ToRadians(60);        // max rate of turret slew in rad/sec
        public int turret_axis = 0;     // 0=az  1=el  2=not supported

        // functional parameters:
        public float capacity = 100.0f;         // full charge (joules)
        public float recharge_rate;    // watts
        public float refire_delay;     // seconds - mechanical limit
        public float salvo_delay;      // seconds - ai refire time
        public int ammo = -1;
        public int ripple_count;     // number of rounds per salvo

        // carrying costs per shot:
        public float charge;           // energy cost of full charge
        public float min_charge;       // minimum energy needed to fire
        public float carry_mass;
        public float carry_resist;

        // shot parameters:
        public SimElements.WeaponDesign.DAMAGE damage_type;      // 0: normal, 1: EMP, 2: power drain
        public float damage = 1.0f;           // if beam, damage per second;
                                              // else,    damage per shot.
        public float penetration = 1.0f;      // ability to pierce shields, 1 is default
        public float speed;
        public float life;
        public float mass;
        public float drag;
        public float thrust;
        public float roll_rate;
        public float pitch_rate;
        public float yaw_rate;
        public float roll_drag;
        public float pitch_drag;
        public float yaw_drag;
        public float integrity = 100.0f;        // hit points for drones = 100
        public float lethal_radius = 500.0f;    // detonation range for missiles

        public float det_range = 0.0f;        // detonation range for cluster weapons
        public string det_child;        // type of submunition
        public int det_count = 0;        // number of submunitions
        public float det_spread = (float)(Math.PI / 8);       // spread of submunition deployment

        // HUD parameters:
        public float min_range;
        public float max_range;
        public float max_track;

        // shot representation:
        public Graphic.TYPE graphic_type;     // sprite or blob?
        public float width;            // blob width
        public float length;           // blob length
        public float scale = 1.0f;            // sprite scale
        public float explosion_scale;  // scale factor for damage to this drone
        public float light;            // light emitted by shot
        public Color light_color = Color.white;      // color of light emitted by shot
        public float flash_scale;      // size of muzzle flash sprite
        public float flare_scale;      // size of drive flare sprite

        public float spread_az;        // spread range in radians
        public float spread_el;        // spread range in radians

        public string[] anim_frames = new string[16];
        public int anim_length;
        public string beauty;
        public string bitmap;
        public string model;
        public string turret;
        public string turret_base;
        public string trail;
        public string flash;
        public string flare;
        public string sound;

        public Bitmap beauty_img;
        public Bitmap[] animation;
        public Bitmap shot_img;
        public Bitmap trail_img;
        public Bitmap flash_img;
        public Bitmap flare_img;
        public Model shot_model;
        public Model turret_model;
        public Model turret_base_model;
        public Sound sound_resource;

        public int trail_length = 0;
        public float trail_width = 0;
        public int trail_dim = 0;

        private static List<WeaponDesign> catalog = new List<WeaponDesign>();
        private static List<WeaponDesign> mod_catalog = new List<WeaponDesign>();
        private static bool degrees;
    }

    public class WeaponsCollection
    {
        public List<WeaponDesign> Weapons;
    }
}
