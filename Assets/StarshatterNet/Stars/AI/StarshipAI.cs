﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      StarshipAI.h/StarshipAI.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Starship (low-level) Artifical Intelligence class
*/
using System;
using DigitalRune.Mathematics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.AI
{
    public class StarshipAI : ShipAI
    {
        public StarshipAI(SimObject s) : base(s)
        {
            sub_select_time = 0; subtarget = null; tgt_point_defense = false;
            ai_type = SteerType.STARSHIP;

            // signifies this ship is a dead hulk:
            if (ship != null && ship.Design().auto_roll < 0)
            {
                Point torque = new Point(RandomHelper.Rand() - 16000, RandomHelper.Rand() - 16000, RandomHelper.Rand() - 16000);
                torque.Normalize();
                torque *= (float)ship.Mass() / 10;

                ship.SetFLCSMode(0);
                if (ship.GetFLCS() != null)
                    ship.GetFLCS().PowerOff();

                ship.ApplyTorque(torque);
                ship.SetVelocity(RandomHelper.RandomDirection() * (float)RandomHelper.Random(20, 50));

                for (int i = 0; i < 64; i++)
                {
                    Weapon w = ship.GetWeaponByIndex(i + 1);
                    if (w != null)
                        w.DrainPower(0);
                    else
                        break;
                }
            }

            else
            {
                tactical = new StarshipTacticalAI(this);
            }

            sub_select_time = (uint)(Game.GameTime() +  RandomHelper.Random(0, 2000));
            point_defense_time = sub_select_time;
        }

        // ~StarshipAI();

        // convert the goal point from world to local coords:
        public override void FindObjective()
        {
            distance = 0;

            RadioMessage.ACTION order = (RadioMessage.ACTION)ship.GetRadioOrders().Action();

            if (order == RadioMessage.ACTION.QUANTUM_TO ||
                    order == RadioMessage.ACTION.FARCAST_TO)
            {

                FindObjectiveQuantum();
                objective = Transform(obj_w);
                return;
            }

            bool hold = order == RadioMessage.ACTION.WEP_HOLD ||
            order == RadioMessage.ACTION.FORM_UP;

            bool form = hold ||
            (order == 0 && target == null) ||
            (farcaster != null);

            // if not the element leader, stay in formation:
            if (form && element_index > 1)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.formation"));

                if (navpt != null && navpt.Action() == Instruction.ACTION.LAUNCH)
                {
                    FindObjectiveNavPoint();
                }
                else
                {
                    navpt = null;
                    FindObjectiveFormation();
                }

                // transform into camera coords:
                objective = Transform(obj_w);
                return;
            }

            // under orders?
            bool directed = false;
            double threat_level = 0;
            double support_level = 1;
            Ship ward = ship.GetWard();

            if (tactical != null)
            {
                directed = (tactical.RulesOfEngagement() == TacticalAI.ROE.DIRECTED);
                threat_level = tactical.ThreatLevel();
                support_level = tactical.SupportLevel();
            }

            // threat processing:
            if (hold || !directed && threat_level >= 2 * support_level)
            {

                // seek support:
                if (support != null)
                {
                    double d_support = (support.Location() - ship.Location()).Length;
                    if (d_support > 35e3)
                    {
                        ship.SetDirectorInfo(localeManager.GetText("ai.regroup"));
                        FindObjectiveTarget(support);
                        objective = Transform(obj_w);
                        return;
                    }
                }

                // run away:
                else if (threat != null && threat != target)
                {
                    ship.SetDirectorInfo(localeManager.GetText("ai.retreat"));
                    obj_w = ship.Location() + (ship.Location() - threat.Location()) * 100;
                    objective = Transform(obj_w);
                    return;
                }
            }

            // weapons hold:
            if (hold)
            {
                if (navpt != null)
                {
                    ship.SetDirectorInfo(localeManager.GetText("ai.seek-navpt"));
                    FindObjectiveNavPoint();
                }

                else if (patrol != 0)
                {
                    ship.SetDirectorInfo(localeManager.GetText("ai.patrol"));
                    FindObjectivePatrol();
                }

                else
                {
                    ship.SetDirectorInfo(localeManager.GetText("ai.holding"));
                    objective = new Point();
                }
            }

            // normal processing:
            else if (target != null)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.seek-target"));
                FindObjectiveTarget(target);
            }

            else if (patrol != 0)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.patrol"));
                FindObjectivePatrol();
            }

            else if (ward != null)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.seek-ward"));
                FindObjectiveFormation();
            }

            else if (navpt != null)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.seek-navpt"));
                FindObjectiveNavPoint();
            }

            else if (rumor != null)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.search"));
                FindObjectiveTarget(rumor);
            }

            else
            {
                objective = new Point();
            }

            // transform into camera coords:
            objective = Transform(obj_w);
        }


        // accumulate behaviors:
        protected override void Navigator()
        {
            // signifies this ship is a dead hulk:
            if (ship != null && ship.Design().auto_roll < 0)
            {
                ship.SetDirectorInfo(localeManager.GetText("ai.dead"));
                return;
            }

            accumulator.Clear();
            magnitude = 0;

            hold = false;
            if ((ship.GetElement() != null && ship.GetElement().GetHoldTime() > 0) ||
                    (navpt != null && navpt.Status() == Instruction.STATUS.COMPLETE && navpt.HoldTime() > 0))
                hold = true;

            ship.SetFLCSMode(FLCS_MODE.FLCS_HELM);

            if (ship.GetDirectorInfo() == null)
            {
                if (target != null)
                    ship.SetDirectorInfo(localeManager.GetText("ai.seek-target"));
                else if (ship.GetWard() != null)
                    ship.SetDirectorInfo(localeManager.GetText("ai.seek-ward"));
                else
                    ship.SetDirectorInfo(localeManager.GetText("ai.patrol"));
            }

            if (farcaster != null && distance < 25e3)
            {
                accumulator = SeekTarget();
            }
            else
            {
                accumulator = AvoidCollision();

                if (other == null && !hold)
                    accumulator = SeekTarget();
            }

            HelmControl();
            ThrottleControl();
            FireControl();
            AdjustDefenses();
        }
        protected override Steer SeekTarget()
        {
            if (navpt != null)
            {
                SimRegion self_rgn = ship.GetRegion();
                SimRegion nav_rgn = navpt.Region();
                QuantumDrive qdrive = ship.GetQuantumDrive();

                if (self_rgn != null && nav_rgn == null)
                {
                    nav_rgn = self_rgn;
                    navpt.SetRegion(nav_rgn);
                }

                bool use_farcaster = self_rgn != nav_rgn &&
                (navpt.Farcast() != null ||
                qdrive == null ||
                !qdrive.IsPowerOn() ||
                qdrive.Status() < ShipSystem.STATUS.DEGRADED
                );

                if (use_farcaster)
                {
                    if (farcaster == null)
                    {
                        foreach (Ship s in self_rgn.Ships())
                        {
                            if (s.GetFarcaster() != null)
                            {
                                Ship dest = s.GetFarcaster().GetDest();
                                if (dest != null && dest.GetRegion() == nav_rgn)
                                {
                                    farcaster = s.GetFarcaster();
                                    if (farcaster != null) break;
                                }
                            }
                        }
                    }

                    if (farcaster != null)
                    {
                        if (farcaster.GetShip().GetRegion() != self_rgn)
                            farcaster = farcaster.GetDest().GetFarcaster();

                        obj_w = farcaster.EndPoint();
                        distance = (obj_w - ship.Location()).Length;

                        if (distance < 1000)
                            farcaster = null;
                    }
                }
                else if (self_rgn != nav_rgn)
                {
                    QuantumDrive q = ship.GetQuantumDrive();

                    if (q != null)
                    {
                        if (q.ActiveState() == QuantumDrive.ACTIVE_STATES.ACTIVE_READY)
                        {
                            q.SetDestination(navpt.Region(), navpt.Location());
                            q.Engage();
                        }
                    }
                }
            }

            return base.SeekTarget();
        }


        protected override Steer AvoidCollision()
        {
            if (ship == null || ship.Velocity().Length < 25)
                return new Steer();

            return base.AvoidCollision();
        }

        // steering functions:
        protected override Steer Seek(Point point)
        {
            // the point is in relative world coordinates
            //   x: distance east(-)  / west(+)
            //   y: altitude down(-)  / up(+)
            //   z: distance north(-) / south(+)

            Steer result = new Steer();

            result.yaw = Math.Atan2(point.X, point.Z) + Math.PI;

            double adjacent = Math.Sqrt(point.X * point.X + point.Z * point.Z);
            if (Math.Abs(point.Y) > ship.Radius() && adjacent > ship.Radius())
                result.pitch = Math.Atan(point.Y / adjacent);

            if (double.IsInfinity(result.yaw))
                result.yaw = 0;

            if (double.IsInfinity(result.pitch))
                result.pitch = 0;

            return result;
        }

        protected override Steer Flee(Point point)
        {
            Steer result = Seek(point);
            result.yaw += Math.PI;
            return result;
        }

        protected override Steer Avoid(Point point, float radius)
        {
            Steer result = Seek(point);

            if (Point.Dot(point, ship.BeamLine()) > 0)
                result.yaw -= Math.PI / 2;
            else
                result.yaw += Math.PI / 2;

            return result;
        }


        protected override Point Transform(Point point)
        {
            return point - self.Location();
        }


        // fire on target if appropriate:
        protected override void FireControl()
        {
            // identify unknown contacts:
            if (identify)
            {
                if (Math.Abs(ship.GetHelmHeading() - ship.CompassHeading()) < MathHelper.ToRadians(10))
                {
                    Contact contact = ship.FindContact(target);

                    if (contact != null && !contact.ActLock())
                    {
                        if (ship.GetProbe() == null)
                        {
                            ship.LaunchProbe();
                        }
                    }
                }

                return;
            }

            // investigate last known location of enemy ship:
            if (rumor != null && target == null && ship.GetProbeLauncher() != null && ship.GetProbe() == null)
            {
                // is rumor in basket?
                Point rmr = Transform(rumor.Location());
                rmr.Normalize();

                double dx = Math.Abs(rmr.X);
                double dy = Math.Abs(rmr.Y);

                if (dx < MathHelper.ToRadians(10) && dy < MathHelper.ToRadians(10) && rmr.Z > 0)
                {
                    ship.LaunchProbe();
                }
            }

            // Corvettes and Frigates are anti-air platforms.  They need to
            // target missile threats even when the threat is aimed at another
            // friendly ship.  Forward facing weapons must be on auto fire,
            // while lateral and aft facing weapons are set to point defense.

            if (ship.Class() == CLASSIFICATION.CORVETTE || ship.Class() == CLASSIFICATION.FRIGATE)
            {
                foreach (WeaponGroup group in ship.Weapons())
                {
                    foreach (Weapon weapon in group.GetWeapons())
                    {
                        double az = weapon.GetAzimuth();
                        if (Math.Abs(az) < MathHelper.ToRadians(45))
                        {
                            weapon.SetFiringOrders(Weapon.Orders.AUTO);
                            weapon.SetTarget(target, null);
                        }

                        else
                        {
                            weapon.SetFiringOrders(Weapon.Orders.POINT_DEFENSE);
                        }
                    }
                }
            }

            // All other starships are free to engage ship targets.  Weapon
            // fire control is managed by the type of weapon.

            else
            {
                ShipSystem subtgt = SelectSubtarget();

                foreach (WeaponGroup weapon in ship.Weapons())
                {

                    if ((weapon.GetDesign().target_type & CLASSIFICATION.DROPSHIPS) != 0)
                    {   // anti-air weapon?
                        weapon.SetFiringOrders(Weapon.Orders.POINT_DEFENSE);
                    }
                    else if (weapon.IsDrone())
                    {                               // torpedoes
                        weapon.SetFiringOrders(Weapon.Orders.MANUAL);
                        weapon.SetTarget(target, null);

                        if (target != null && target.GetRegion() == ship.GetRegion())
                        {
                            Point delta = target.Location() - ship.Location();
                            double range = delta.Length;

                            if (range < weapon.GetDesign().max_range * 0.9 &&
                                    !AssessTargetPointDefense())
                                weapon.SetFiringOrders(Weapon.Orders.AUTO);

                            else if (range < weapon.GetDesign().max_range * 0.5)
                                weapon.SetFiringOrders(Weapon.Orders.AUTO);
                        }
                    }
                    else
                    {                                                      // anti-ship weapon
                        weapon.SetFiringOrders(Weapon.Orders.AUTO);
                        weapon.SetTarget(target, subtgt);
                        weapon.SetSweep(subtgt != null ? Weapon.Sweep.SWEEP_NONE : Weapon.Sweep.SWEEP_TIGHT);
                    }
                }
            }
        }


        protected override void HelmControl()
        {
            // signifies this ship is a dead hulk:
            if (ship != null && ship.Design().auto_roll < 0)
            {
                return;
            }

            double trans_x = 0;
            double trans_y = 0;
            double trans_z = 0;

            bool station_keeping = distance < 0;

            if (station_keeping)
            {
                accumulator.brake = 1;
                accumulator.stop = 1;

                ship.SetHelmPitch(0);
            }

            else
            {
                Element elem = ship.GetElement();

                Ship ward = ship.GetWard();
                Ship s_threat = null;
                if (threat != null && threat.Class() >= ship.Class())
                    s_threat = threat;

                if (other != null || target != null || ward != null || s_threat != null || navpt != null || patrol != 0 || farcaster != null || element_index > 1)
                {
                    ship.SetHelmHeading(accumulator.yaw);

                    if ((Mission.TYPE)elem.Type() == Mission.TYPE.FLIGHT_OPS)
                    {
                        ship.SetHelmPitch(0);

                        if (ship.NumInbound() > 0)
                        {
                            ship.SetHelmHeading(ship.CompassHeading());
                        }
                    }

                    else if (accumulator.pitch > MathHelper.ToRadians(60))
                    {
                        ship.SetHelmPitch(MathHelper.ToRadians(60));
                    }

                    else if (accumulator.pitch < MathHelper.ToRadians(-60))
                    {
                        ship.SetHelmPitch(MathHelper.ToRadians(-60));
                    }

                    else
                    {
                        ship.SetHelmPitch(accumulator.pitch);
                    }

                }
                else
                {
                    ship.SetHelmPitch(0);
                }
            }

            ship.SetTransX(trans_x);
            ship.SetTransY(trans_y);
            ship.SetTransZ(trans_z);

            ship.ExecFLCSFrame();
        }

        protected override void ThrottleControl()
        {
            // signifies this ship is a dead hulk:
            if (ship != null && ship.Design().auto_roll < 0)
            {
                return;
            }

            // station keeping:

            if (distance < 0)
            {
                old_throttle = 0;
                throttle = 0;

                ship.SetThrottle(0);

                if (ship.GetFLCS() != null)
                    ship.GetFLCS().FullStop();

                return;
            }

            // normal throttle processing:

            double ship_speed = Point.Dot(ship.Velocity(), ship.Heading());
            double brakes = 0;
            Ship ward = ship.GetWard();
            Ship s_threat = null;

            if (threat != null && threat.Class() >= ship.Class())
                s_threat = threat;

            if (target != null || s_threat != null)
            {  // target pursuit, or retreat
                throttle = 100;

                if (target != null && distance < 50e3)
                {
                    double closing_speed = ship_speed;

                    if (target != null)
                    {
                        Point delta = target.Location() - ship.Location();
                        delta.Normalize();

                        closing_speed = Point.Dot(ship.Velocity(), delta);
                    }

                    if (closing_speed > 300)
                    {
                        throttle = 30;
                        brakes = 0.25;
                    }
                }

                throttle *= (1 - accumulator.brake);

                if (throttle < 1 && ship.GetFLCS() != null)
                    ship.GetFLCS().FullStop();
            }

            else if (ward != null)
            {           // escort, match speed of ward
                double speed = ward.Velocity().Length;
                throttle = old_throttle;

                if (speed == 0)
                {
                    double d = (ship.Location() - ward.Location()).Length;

                    if (d > 30e3)
                        speed = (d - 30e3) / 100;
                }

                if (speed > 0)
                {
                    if (ship_speed > speed)
                    {
                        throttle = old_throttle - 1;
                        brakes = 0.2;
                    }
                    else if (ship_speed < speed - 10)
                    {
                        throttle = old_throttle + 1;
                    }
                }
                else
                {
                    throttle = 0;
                    brakes = 0.5;
                }
            }

            else if (patrol != 0 || farcaster != null)
            {  // seek patrol point
                throttle = 100;

                if (distance < 10 * ship_speed)
                {
                    if (ship.Velocity().Length > 200)
                        throttle = 5;
                    else
                        throttle = 50;
                }
            }

            else if (navpt != null)
            {          // lead only, get speed from navpt
                double speed = navpt.Speed();
                throttle = old_throttle;

                if (hold)
                {
                    throttle = 0;
                    brakes = 1;
                }

                else
                {
                    if (speed <= 0)
                        speed = 300;

                    if (ship_speed > speed)
                    {
                        if (throttle > 0 && old_throttle > 1)
                            throttle = old_throttle - 1;

                        brakes = 0.25;
                    }
                    else if (ship_speed < speed - 10)
                    {
                        throttle = old_throttle + 1;
                    }
                }
            }

            else if (element_index > 1)
            { // wingman
                Ship lead = ship.GetElement().GetShip(1);
                double lv = lead.Velocity().Length;
                double sv = ship_speed;
                double dv = lv - sv;
                double dt = 0;

                if (dv > 0) dt = dv * 1e-2 * seconds;
                else if (dv < 0) dt = dv * 1e-2 * seconds;

                throttle = old_throttle + dt;
            }

            else
            {
                throttle = 0;
            }

            old_throttle = throttle;
            ship.SetThrottle(throttle);

            if (ship_speed > 1 && brakes > 0)
                ship.SetTransY(-brakes * ship.Design().trans_y);

            else if (throttle > 10 && (ship.GetEMCON() < 2 || ship.GetFuelLevel() < 10))
                ship.SetTransY(ship.Design().trans_y);
        }


        protected ShipSystem SelectSubtarget()
        {
            if (Game.GameTime() - sub_select_time < 2345)
                return subtarget;

            subtarget = null;

            if (target == null || (SimObject.TYPES)target.Type() != SimObject.TYPES.SIM_SHIP || GetAILevel() < 1)
                return subtarget;

            Ship tgt_ship = (Ship)target;

            if (!tgt_ship.IsStarship())
                return subtarget;

            Weapon subtgt = null;
            double dist = 50e3;
            Point svec = ship.Location() - tgt_ship.Location();

            sub_select_time = (uint)Game.GameTime();

            // first pass: turrets
            foreach (WeaponGroup g in tgt_ship.Weapons())
            {
                if (g.GetDesign() != null && g.GetDesign().turret_model != null)
                {
                    foreach (Weapon w in g.GetWeapons())
                    {

                        if (w.Availability() < 35)
                            continue;

                        if (Point.Dot(w.GetAimVector(), svec) < 0)
                            continue;

                        if (w.GetTurret() != null)
                        {
                            Point tloc = w.GetTurret().Location();
                            Point delta = tloc - ship.Location();
                            double dlen = delta.Length;

                            if (dlen < dist)
                            {
                                subtgt = w;
                                dist = dlen;
                            }
                        }
                    }
                }
            }

            // second pass: major weapons
            if (subtgt == null)
            {
                foreach (WeaponGroup g in tgt_ship.Weapons())
                {

                    if (g.GetDesign() != null && g.GetDesign().turret_model == null)
                    {
                        foreach (Weapon w in g.GetWeapons())
                        {
                            if (w.Availability() < 35)
                                continue;

                            if (Point.Dot(w.GetAimVector(), svec) < 0)
                                continue;

                            Point tloc = w.MountLocation();
                            Point delta = tloc - ship.Location();
                            double dlen = delta.Length;

                            if (dlen < dist)
                            {
                                subtgt = w;
                                dist = dlen;
                            }
                        }
                    }
                }
            }

            subtarget = subtgt;
            return subtarget;
        }
        protected bool AssessTargetPointDefense()
        {
            if (Game.GameTime() - point_defense_time < 3500)
                return tgt_point_defense;

            tgt_point_defense = false;

            if (target == null || (SimObject.TYPES)target.Type() != SimObject.TYPES.SIM_SHIP || GetAILevel() < 2)
                return tgt_point_defense;

            Ship tgt_ship = (Ship)target;

            if (!tgt_ship.IsStarship())
                return tgt_point_defense;

            //Weapon subtgt = null;
            Point svec = ship.Location() - tgt_ship.Location();

            point_defense_time = (uint)Game.GameTime();

            // first pass: turrets
            foreach (WeaponGroup g in tgt_ship.Weapons())
            {
                if (g.CanTarget((CLASSIFICATION)1))
                {
                    foreach (Weapon w in g.GetWeapons())
                    {
                        if (w.Availability() > 35 && Point.Dot(w.GetAimVector(), svec) > 0)
                            tgt_point_defense = true;
                        if (tgt_point_defense) return tgt_point_defense;
                    }
                }
            }

            return tgt_point_defense;
        }

        protected uint sub_select_time;
        protected uint point_defense_time;
        //TODO protected ShipSystem subtarget;
        protected bool tgt_point_defense;
    }
}