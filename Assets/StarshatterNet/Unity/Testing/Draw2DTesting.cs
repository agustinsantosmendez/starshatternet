﻿using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Graphics.General;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

public class Draw2DTesting : MonoBehaviour
{
    private Scene scene;

    private Camera cam;

    // Use this for initialization
    void Start()
    {
        scene = new Scene();

        // This code is similar to TacRefDlg.cpp, lines 133-160
        scene.SetAmbient(new Color(60f / 255f, 60f / 255f, 60f / 255f));

        CameraNode camera = new CameraNode();
        scene.AddCamera(camera.gameObj);
        cam = camera.gameObj.GetComponent<Camera>();

        Graphic2D window = new Graphic2D();
        window.MoveTo(0, 0, 15);
        window.Width = 0.03f;
        window.DrawLine(-5, -5, 5, 5, Color.white);
        window.DrawRect(-5, -5, 5, 5, Color.white);
        // window.DrawEllipse(new Vector2(0, 0), 5, 3, Color.white);
        window.DrawEllipse(-5, -5, 5, 3, Color.white);
        window.DrawText("Hello World!", new StarshatterNet.Stars.Rect(-5, -5, 5, 3), Color.white);
        scene.AddGraphic(window);

        Video video = Video.GetVideo();
        Screen screen = new Screen(video);
        Window win = new Window(screen, 0, 0, 10, 100);
        win.DrawLine(-5, -5, 5, 5, Color.white);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
