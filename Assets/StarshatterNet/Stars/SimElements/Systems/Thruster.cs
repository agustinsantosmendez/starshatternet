﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Thruster.h/Thruster.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Conventional Thruster (system) class
*/
using Assets.StarshatterNet.Stars.Graphics.General;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Audio;
using StarshatterNet.Stars.Graphics.General;
using System;
using System.Collections.Generic;
using DWORD = System.UInt32;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Config;
using StarshatterNet.Stars.Simulator;

namespace StarshatterNet.Stars.SimElements
{
    public class ThrusterPort
    {

        public ThrusterPort(int t, Point l, DWORD f, float s)
        {
            type = t; loc = l; flare = null; trail = null; fire = f; burn = 0; scale = s;
        }
        // ~ThrusterPort();

        public int type;
        public DWORD fire;
        public float burn;
        public float scale;
        public Point loc;
        public Graphic flare;
        public Graphic trail;
    }

    public class Thruster : ShipSystem
    {
        public enum Constants
        {
            LEFT, RIGHT, FORE, AFT, TOP, BOTTOM,
            YAW_L, YAW_R, PITCH_D, PITCH_U, ROLL_L, ROLL_R
        };
        private static int sys_value = 2;

        public Thruster(int dtype, double max_thrust, float flare_scale = 0)
            : base(CATEGORY.DRIVE, dtype, "Thruster", sys_value, max_thrust, max_thrust, max_thrust)
        {
            ship = null; thrust = 1.0f; scale = flare_scale;
            avail_x = 1.0f; avail_y = 1.0f; avail_z = 1.0f;
            name = Game.GetText("sys.thruster");
            abrv = Game.GetText("sys.thruster.abrv");

            power_flags = POWER_FLAGS.POWER_WATTS;

            // ZeroMemory(burn, sizeof(burn));

            emcon_power[0] = 50;
            emcon_power[1] = 50;
            emcon_power[2] = 100;
        }
        public Thruster(Thruster t)
            : base(t)
        {
            thrust = 1.0f; scale = t.scale;
            avail_x = 1.0f; avail_y = 1.0f; avail_z = 1.0f;
            power_flags = POWER_FLAGS.POWER_WATTS;
            Mount(t);

            //ZeroMemory(burn, sizeof(burn));

            if (subtype != (int)Drive.SUBTYPE.STEALTH)
            {
                for (int i = 0; i < t.ports.Count; i++)
                {
                    ThrusterPort p = t.ports[i];
                    CreatePort(p.type, p.loc, p.fire, p.scale);
                }
            }
        }
        // ~Thruster();

        static bool initialized = false;
        public static void Initialize()
        {
            if (initialized) return;

            Sound.FlagEnum SOUND_FLAGS = Sound.FlagEnum.LOCALIZED |
                                        Sound.FlagEnum.LOC_3D |
                                        Sound.FlagEnum.LOOP |
                                        Sound.FlagEnum.LOCKED;
            thruster_resource = Sound.CreateStream("Sounds/thruster.wav", SOUND_FLAGS);

            if (thruster_resource != null)
                thruster_resource.SetMaxDistance(15.0e3f);

            initialized = true;
        }

        public static void Close()
        {
            //delete thruster_resource;
            thruster_resource = null;

            if (thruster_sound != null)
            {
                thruster_sound.Stop();
                thruster_sound.Release();
            }
        }


        public override void ExecFrame(double seconds)
        {
            base.ExecFrame(seconds);

            if (ship != null)
            {
                double rr = 0, pr = 0, yr = 0;
                double rd = 0, pd = 0, yd = 0;

                double agility = 1;
                double stability = 1;

                FlightComp flcs = ship.GetFLCS();

                if (flcs != null)
                {
                    if (!flcs.IsPowerOn() || (int)flcs.Status() < (int)Component.STATUS.DEGRADED)
                    {
                        agility = 0.3;
                        stability = 0.0;
                    }
                }

                // check for thruster damage here:
                if (components.Count >= 3)
                {
                    Component.STATUS stat = components[0].Status();
                    if (stat == Component.STATUS.NOMINAL)
                        avail_x = 1.0f;
                    else if (stat == Component.STATUS.DEGRADED)
                        avail_x = 0.5f;
                    else
                        avail_x = 0.0f;

                    stat = components[1].Status();
                    if (stat == Component.STATUS.NOMINAL)
                        avail_z = 1.0f;
                    else if (stat == Component.STATUS.DEGRADED)
                        avail_z = 0.5f;
                    else
                        avail_z = 0.0f;

                    stat = components[2].Status();
                    if (stat == Component.STATUS.NOMINAL)
                        avail_y = 1.0f;
                    else if (stat == Component.STATUS.DEGRADED)
                        avail_y = 0.5f;
                    else
                        avail_y = 0.0f;
                }

                // thrust limited by power distribution:
                thrust = energy / capacity;
                energy = 0.0f;

                if (thrust < 0)
                    thrust = 0.0f;

                agility *= thrust;
                stability *= thrust;

                rr = roll_rate * agility * avail_y;
                pr = pitch_rate * agility * avail_y;
                yr = yaw_rate * agility * avail_x;

                rd = roll_drag * stability * avail_y;
                pd = pitch_drag * stability * avail_y;
                yd = yaw_drag * stability * avail_x;

                ship.SetAngularRates(rr, pr, yr);
                ship.SetAngularDrag(rd, pd, yd);
            }
        }

        public virtual void ExecTrans(double x, double y, double z)
        {
            if (ship == null || (ship.IsAirborne() && ship.Class() != CLASSIFICATION.LCA))
            {
                if (thruster_sound != null && thruster_sound.IsPlaying())
                    thruster_sound.Stop();

                for (int i = 0; i < ports.Count; i++)
                {
                    ThrusterPort  p = ports[i];
                    if (p.flare != null) p.flare.Hide();
                    if (p.trail != null) p.trail.Hide();
                }

                return;
            }

            bool sound_on = false;
            bool show_flare = true;

            if (ship.Rep() != null && ship.Rep().Hidden())
                show_flare = false;

            if (ship.Class() == CLASSIFICATION.LCA &&
                    ship.IsAirborne() &&
                    ship.Velocity().Length < 250 &&
                    ship.AltitudeAGL() > ship.Radius() / 2)
            {

                sound_on = true;
                IncBurn(Constants.BOTTOM, Constants.TOP);
            }

            else if (!ship.IsAirborne())
            {
                double tx_limit = ship.Design().trans_x;
                double ty_limit = ship.Design().trans_y;
                double tz_limit = ship.Design().trans_z;

                if (x < -0.15 * tx_limit) IncBurn(Constants.RIGHT, Constants.LEFT);
                else if (x > 0.15 * tx_limit) IncBurn(Constants.LEFT, Constants.RIGHT);
                else DecBurn(Constants.LEFT, Constants.RIGHT);

                if (y < -0.15 * ty_limit) IncBurn(Constants.FORE, Constants.AFT);
                else if (y > 0.15 * ty_limit) IncBurn(Constants.AFT, Constants.FORE);
                else DecBurn(Constants.FORE, Constants.AFT);

                if (z < -0.15 * tz_limit) IncBurn(Constants.TOP, Constants.BOTTOM);
                else if (z > 0.15 * tz_limit) IncBurn(Constants.BOTTOM, Constants.TOP);
                else DecBurn(Constants.TOP, Constants.BOTTOM);

                double r, p, y2;
                ship.GetAngularThrust(out r, out p, out y2);

                // Roll seems to have the opposite sign from
                // the pitch and yaw thrust factors.  Not sure why.

                if (r > 0) IncBurn(Constants.ROLL_L, Constants.ROLL_R);
                else if (r < 0) IncBurn(Constants.ROLL_R, Constants.ROLL_L);
                else DecBurn(Constants.ROLL_R, Constants.ROLL_L);

                if (y2 < 0) IncBurn(Constants.YAW_L, Constants.YAW_R);
                else if (y2 > 0) IncBurn(Constants.YAW_R, Constants.YAW_L);
                else DecBurn(Constants.YAW_R, Constants.YAW_L);

                if (p < 0) IncBurn(Constants.PITCH_D, Constants.PITCH_U);
                else if (p > 0) IncBurn(Constants.PITCH_U, Constants.PITCH_D);
                else DecBurn(Constants.PITCH_U, Constants.PITCH_D);
            }

            else
            {
                for (int i = 0; i < 12; i++)
                {
                    burn[i] -= 0.1f;
                    if (burn[i] < 0)
                        burn[i] = 0.0f;
                }
            }

            for (int i = 0; i < ports.Count; i++)
            {
                ThrusterPort p = ports[i];

                if (p.fire != 0)
                {
                    p.burn = 0;

                    int flag = 1;

                    for (int n = 0; n < 12; n++)
                    {
                        if ((p.fire & flag) != 0 && burn[n] > p.burn)
                            p.burn = burn[n];

                        flag <<= 1;
                    }
                }

                else
                {
                    p.burn = burn[p.type];
                }

                if (p.burn > 0 && thrust > 0)
                {
                    sound_on = true;

                    if (show_flare)
                    {
                        SpriteGraphic flare_rep = (SpriteGraphic)p.flare;
                        if (flare_rep != null)
                        {
                            flare_rep.Show();
                            flare_rep.SetShade(1);
                        }

                        if (p.trail != null)
                        {
                            Bolt  t = (Bolt )p.trail;
                            t.Show();
                            t.SetShade(1);
                        }
                    }
                }
                else
                {
                    if (p.flare != null) p.flare.Hide();
                    if (p.trail != null) p.trail.Hide();
                }
            }

            // thruster sound:
            if (ship !=null && ship == Sim.GetSim().GetPlayerShip() && ports.Count > 0)
            {
                CameraDirector  cam_dir = CameraDirector.GetInstance();

                // no sound when paused!
                if (!Game.Paused() && cam_dir != null && cam_dir.GetCamera() != null)
                {
                    if ( thruster_sound == null)
                    {
                        if (thruster_resource != null)
                            thruster_sound = thruster_resource.Duplicate();
                    }

                    if (thruster_sound != null)
                    {
                        if (sound_on)
                        {
                            Point cam_loc = cam_dir.GetCamera().Pos();
                            double dist = (ship.Location() - cam_loc).Length;

                            long max_vol = AudioConfig.EfxVolume();
                            long volume = -2000;

                            if (volume > max_vol)
                                volume = max_vol;

                            if (dist < thruster_sound.GetMaxDistance())
                            {
                                thruster_sound.SetLocation(ship.Location());
                                thruster_sound.SetVolume(volume);
                                thruster_sound.Play();
                            }
                            else if (thruster_sound.IsPlaying())
                            {
                                thruster_sound.Stop();
                            }
                        }
                        else if (thruster_sound.IsPlaying())
                        {
                            thruster_sound.Stop();
                        }
                    }
                }
            }

            ship.SetTransX(x * thrust);
            ship.SetTransY(y * thrust);
            ship.SetTransZ(z * thrust);
        }
        public virtual void SetShip(Ship s)
        {
            const double ROLL_SPEED = Math.PI * 0.0400;
            const double PITCH_SPEED = Math.PI * 0.0250;
            const double YAW_SPEED = Math.PI * 0.0250;

            ship = s;

            if (ship != null)
            {
                ShipDesign design = (ShipDesign)ship.Design();

                trans_x = design.trans_x;
                trans_y = design.trans_y;
                trans_z = design.trans_z;

                roll_drag = design.roll_drag;
                pitch_drag = design.pitch_drag;
                yaw_drag = design.yaw_drag;

                roll_rate = (float)(design.roll_rate * Math.PI / 180);
                pitch_rate = (float)(design.pitch_rate * Math.PI / 180);
                yaw_rate = (float)(design.yaw_rate * Math.PI / 180);

                double agility = design.agility;

                if (roll_rate == 0) roll_rate = (float)(agility * ROLL_SPEED);
                if (pitch_rate == 0) pitch_rate = (float)(agility * PITCH_SPEED);
                if (yaw_rate == 0) yaw_rate = (float)(agility * YAW_SPEED);
            }
        }

        public virtual double TransXLimit()
        {
            return trans_x * avail_x;
        }

        public virtual double TransYLimit()
        {
            return trans_y * avail_y;
        }
        public virtual double TransZLimit()
        {
            return trans_z * avail_z;
        }

        public virtual void AddPort(int type, Point loc, DWORD fire, float flare_scale = 0)
        {
            if (flare_scale == 0) flare_scale = scale;
            ThrusterPort port = new ThrusterPort(type, loc, fire, flare_scale);
            ports.Add(port);
        }

        public virtual void CreatePort(int type, Point loc, DWORD fire, float flare_scale)
        {
            Bitmap flare_bmp = Drive.drive_flare_bitmap[subtype];
            Bitmap trail_bmp = Drive.drive_trail_bitmap[subtype];

            if (subtype != (int)Drive.SUBTYPE.STEALTH)
            {
                SpriteGraphic flare_rep = new SpriteGraphic(flare_bmp, null);
                flare_rep.Scale(flare_scale * 0.667f);
                flare_rep.SetShade(0);

                Bolt trail_rep = new Bolt(flare_scale * 30, flare_scale * 8, trail_bmp, true);

                ThrusterPort port = new ThrusterPort(type, loc, fire, flare_scale);
                port.flare = flare_rep;
                port.trail = trail_rep;
                ports.Add(port);
            }
        }

        public int NumThrusters()
        {
            return ports.Count;
        }

        public Graphic Flare(int engine)
        {
            if (engine >= 0 && engine < ports.Count)
                return ports[engine].flare;

            return null;
        }

        public Graphic Trail(int engine)
        {
            if (engine >= 0 && engine < ports.Count)
                return ports[engine].trail;

            return null;
        }

        public override void Orient(Physical rep)
        {
            base.Orient(rep);

            bool hide_all = false;
            if (ship == null || (ship.IsAirborne() && ship.Class() != CLASSIFICATION.LCA))
            {
                hide_all = true;
            }

            if (ship.Rep() != null && ship.Rep().Hidden())
            {
                hide_all = true;
            }

            if (thrust <= 0)
            {
                hide_all = true;
            }

            Matrix33D orientation = rep.Cam().Orientation();
            Point ship_loc = rep.Location();

            for (int i = 0; i < ports.Count; i++)
            {
                ThrusterPort p = ports[i];

                Point projector = orientation * p.loc + ship_loc; // TODO matrix * vector multiplication order

                if (p.flare != null)
                    p.flare.MoveTo(projector);

                if (p.trail != null)
                {
                    float intensity = p.burn;

                    if (intensity > 0.5 && !hide_all)
                    {
                        Bolt t = (Bolt)p.trail;
                        float len = -50 * p.scale * intensity;

                        t.Show();

                        switch ((Constants)p.type)
                        {
                            case Constants.LEFT: t.SetEndPoints(projector, projector + rep.Cam().vrt() * len); break;
                            case Constants.RIGHT: t.SetEndPoints(projector, projector - rep.Cam().vrt() * len); break;
                            case Constants.AFT: t.SetEndPoints(projector, projector + rep.Cam().vpn() * len); break;
                            case Constants.FORE: t.SetEndPoints(projector, projector - rep.Cam().vpn() * len); break;
                            case Constants.BOTTOM: t.SetEndPoints(projector, projector + rep.Cam().vup() * len); break;
                            case Constants.TOP: t.SetEndPoints(projector, projector - rep.Cam().vup() * len); break;
                            default: t.Hide(); break;
                        }
                    }
                    else
                    {
                        p.trail.Hide();
                        if (p.flare != null)
                            p.flare.Hide();
                    }
                }
            }
        }

        public override double GetRequest(double seconds)
        {
            if (!power_on)
                return 0;

            for (int i = 0; i < 12; i++)
                if (burn[i] != 0)
                    return power_level * sink_rate * seconds;

            return 0;
        }



        protected void IncBurn(Constants inc, Constants dec)
        {
            burn[(int)inc] += 0.1f;
            if (burn[(int)inc] > 1)
                burn[(int)inc] = 1.0f;

            burn[(int)dec] -= 0.1f;
            if (burn[(int)dec] < 0)
                burn[(int)dec] = 0.0f;
        }

        protected void DecBurn(Constants a, Constants b)
        {
            burn[(int)a] -= 0.1f;
            if (burn[(int)a] < 0)
                burn[(int)a] = 0.0f;

            burn[(int)b] -= 0.1f;
            if (burn[(int)b] < 0)
                burn[(int)b] = 0.0f;
        }


        //protected Ship ship;
        protected float thrust;
        protected float scale;
        protected float[] burn = new float[12];

        protected float avail_x, avail_y, avail_z;
        protected float trans_x, trans_y, trans_z;
        protected float roll_rate, pitch_rate, yaw_rate;
        protected float roll_drag, pitch_drag, yaw_drag;

        protected List<ThrusterPort> ports = new List<ThrusterPort>();
        private static Sound thruster_resource = null;
        private static Sound thruster_sound = null;
    }
}