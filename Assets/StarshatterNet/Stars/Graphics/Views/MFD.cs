﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MFD.h/MFD.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Class for all Multi Function Displays
 */
using System;
using DigitalRune.Mathematics;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.SimElements;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.Views
{
#warning MFD class is still in development and is not recommended for production.
    public class MFD
    {
        public enum Modes
        {
            MFD_MODE_OFF, MFD_MODE_GAME, MFD_MODE_SHIP,
            MFD_MODE_FOV,  /*MFD_MODE_FWD,  MFD_MODE_MAP,*/
            MFD_MODE_HSD, MFD_MODE_3D
        };

        public MFD(Window w, Modes n)
        {
            window = w; rect = new Rect(0, 0, 0, 0); index = n; mode = Modes.MFD_MODE_OFF; sprite = null;
            ship = null; hidden = true; camview = null; lines = 0; mouse_latch = false; mouse_in = false;
            cockpit_hud_texture = null;

            sensor_fov_sprite = new SpriteGraphic(sensor_fov, window.Canvas);
            sensor_fwd_sprite = new SpriteGraphic(sensor_fwd, window.Canvas);
            sensor_hsd_sprite = new SpriteGraphic(sensor_hsd, window.Canvas);
            sensor_3d_sprite = new SpriteGraphic(sensor_3d, window.Canvas);

            sensor_fov_sprite.SetBlendMode(2);
            sensor_fov_sprite.SetFilter(0);
            sensor_fov_sprite.Hide();
            sensor_fwd_sprite.SetBlendMode(2);
            sensor_fwd_sprite.SetFilter(0);
            sensor_fwd_sprite.Hide();
            sensor_fwd_sprite.SetBlendMode(2);
            sensor_hsd_sprite.SetFilter(0);
            sensor_hsd_sprite.Hide();
            sensor_3d_sprite.SetBlendMode(2);
            sensor_3d_sprite.SetFilter(0);
            sensor_3d_sprite.Hide();

            sprite = sensor_fov_sprite;

            FontItem font = FontMgr.Find("HUD");

            for (int i = 0; i < TXT_LAST; i++)
            {
                mfd_text[i] = new HUDText();
                mfd_text[i].font = font;
                mfd_text[i].color = Color.white;
                mfd_text[i].hidden = true;
            }
        }
        // ~MFD();

        //int operator ==(const MFD& that) const { return this == &that; }

        public static void Initialize()
        {
            if (initialized) return;

            HUDView.PrepareBitmap("sensor_fov", out sensor_fov);
            HUDView.PrepareBitmap("sensor_fwd", out sensor_fwd);
            HUDView.PrepareBitmap("sensor_hsd", out sensor_hsd);
            HUDView.PrepareBitmap("sensor_3d", out sensor_3d);

            sensor_fov.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            sensor_fwd.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            sensor_hsd.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            sensor_3d.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);

            initialized = true;
        }
        public static void Close()
        {
            sensor_fov.ClearImage();
            sensor_fwd.ClearImage();
            sensor_hsd.ClearImage();
            sensor_3d.ClearImage();

            //delete[] sensor_fov_shade;
            //delete[] sensor_fwd_shade;
            //delete[] sensor_hsd_shade;
            //delete[] sensor_3d_shade;
        }
        public static void SetColor(Color c)
        {
            HUDView hud = HUDView.GetInstance();

            if (hud != null)
            {
                hud_color = hud.GetHUDColor();
                txt_color = hud.GetTextColor();
            }
            else
            {
                hud_color = c;
                txt_color = c;
            }

            HUDView.ColorizeBitmap(sensor_fov_sprite, c);
            HUDView.ColorizeBitmap(sensor_fwd_sprite, c);
            HUDView.ColorizeBitmap(sensor_hsd_sprite, c);
            HUDView.ColorizeBitmap(sensor_3d_sprite, c);
        }

        // Operations:
        public virtual void Draw()
        {
            mouse_in = false;

            if (Mouse.LButton() == 0)
                mouse_latch = false;

            if (rect.Contains((int)Mouse.X(), (int)Mouse.Y()))
                mouse_in = true;

            // click to turn on MFD when off:
            if (mode < Modes.MFD_MODE_FOV && Mouse.LButton() != 0 && !mouse_latch)
            {
                mouse_latch = true;
                if (mouse_in)
                {
                    HUDView hud = HUDView.GetInstance();
                    if (hud != null)
                        hud.CycleMFDMode(index);
                }
            }

            for (int i = 0; i < TXT_LAST; i++)
                HideMFDText(i);

            if (hidden || mode < Modes.MFD_MODE_FOV)
            {
                if (cockpit_hud_texture != null)
                {
                    int x1 = (int)index * 128;
                    int y1 = 256;
                    int x2 = x1 + 128;
                    int y2 = y1 + 128;

                    cockpit_hud_texture.FillRect(x1, y1, x2, y2, Color.black);
                }

                if (hidden)
                    return;
            }

            if (sprite != null && !sprite.Hidden())
            {
                if (cockpit_hud_texture != null)
                {
                    int x1 = (int)index * 128;
                    int y1 = 256;
                    int w = sprite.Width();
                    int h = sprite.Height();

                    cockpit_hud_texture.BitBlt(x1, y1, sprite.Frame(), 0, 0, w, h);
                }
                else
                {
                    int cx = rect.x + rect.w / 2;
                    int cy = rect.y + rect.h / 2;
                    int w2 = sprite.Width() / 2;
                    int h2 = sprite.Height() / 2;

                    window.DrawBitmap(cx - w2, cy - h2, cx + w2, cy + h2, sprite.Frame(), Video.BLEND_TYPE.BLEND_ALPHA);
                }
            }
            switch (mode)
            {
                default:
                case Modes.MFD_MODE_OFF: break;
                case Modes.MFD_MODE_GAME: DrawGameMFD(); break;
                case Modes.MFD_MODE_SHIP: DrawStatusMFD(); break;

                // sensor sub-modes:
                case Modes.MFD_MODE_FOV: DrawSensorMFD(); break;
                case Modes.MFD_MODE_HSD: DrawHSD(); break;
                case Modes.MFD_MODE_3D: Draw3D(); break;
            }
        }
        public virtual void DrawGameMFD()
        {
            if (lines < 10) lines++;

            string txt;
            Rect txt_rect = new Rect(rect.x, rect.y, rect.w, 12);

            int t = 0;

            if (!HUDView.IsArcade() && HUDView.ShowFPS())
            {
                txt = string.Format("FPS: %6.2f", Game.FrameRate());
                DrawMFDText(t++, txt, txt_rect, TextFormat.DT_LEFT);
                txt_rect.y += 10;

                if (lines <= 1) return;

                Starshatter game = Starshatter.GetInstance();
                txt = string.Format("Polys: {0}", 0);//TODO game.GetPolyStats().npolys);
                DrawMFDText(t++, txt, txt_rect, TextFormat.DT_LEFT);
                txt_rect.y += 10;
            }

            if (ship != null)
            {
                DrawMFDText(t++, ship.Name(), txt_rect, TextFormat.DT_LEFT);
                txt_rect.y += 10;
            }

            if (lines <= 2) return;

            int hours = (int)(Game.GameTime() / 3600000);
            int minutes = (int)(Game.GameTime() / 60000) % 60;
            int seconds = (int)(Game.GameTime() / 1000) % 60;

            if (ship != null)
            {
                uint clock = ship.MissionClock();

                hours = (int)(clock / 3600000);
                minutes = (int)(clock / 60000) % 60;
                seconds = (int)(clock / 1000) % 60;
            }

            if (Game.TimeCompression() > 1)
                txt = string.Format("%02d:%02d:%02d x%d", hours, minutes, seconds, Game.TimeCompression()); //-V576
            else
                txt = string.Format("%02d:%02d:%02d", hours, minutes, seconds);

            DrawMFDText(t++, txt, txt_rect, TextFormat.DT_LEFT);
            txt_rect.y += 10;

            if (HUDView.IsArcade() || lines <= 3) return;

            DrawMFDText(t++, ship.GetRegion().Name(), txt_rect, TextFormat.DT_LEFT);
            txt_rect.y += 10;

            if (lines <= 4) return;

            if (ship != null)
            {
                switch (ship.GetFlightPhase())
                {
                    case OP_MODE.DOCKED: DrawMFDText(t++, Game.GetText("MFD.phase.DOCKED"), txt_rect, TextFormat.DT_LEFT); break;
                    case OP_MODE.ALERT: DrawMFDText(t++, Game.GetText("MFD.phase.ALERT"), txt_rect, TextFormat.DT_LEFT); break;
                    case OP_MODE.LOCKED: DrawMFDText(t++, Game.GetText("MFD.phase.LOCKED"), txt_rect, TextFormat.DT_LEFT); break;
                    case OP_MODE.LAUNCH: DrawMFDText(t++, Game.GetText("MFD.phase.LAUNCH"), txt_rect, TextFormat.DT_LEFT); break;
                    case OP_MODE.TAKEOFF: DrawMFDText(t++, Game.GetText("MFD.phase.TAKEOFF"), txt_rect, TextFormat.DT_LEFT); break;
                    case OP_MODE.ACTIVE: DrawMFDText(t++, Game.GetText("MFD.phase.ACTIVE"), txt_rect, TextFormat.DT_LEFT); break;
                    case OP_MODE.APPROACH: DrawMFDText(t++, Game.GetText("MFD.phase.APPROACH"), txt_rect, TextFormat.DT_LEFT); break;
                    case OP_MODE.RECOVERY: DrawMFDText(t++, Game.GetText("MFD.phase.RECOVERY"), txt_rect, TextFormat.DT_LEFT); break;
                    case OP_MODE.DOCKING: DrawMFDText(t++, Game.GetText("MFD.phase.DOCKING"), txt_rect, TextFormat.DT_LEFT); break;
                }
            }
        }
        public virtual void DrawStatusMFD()
        {
            if (lines < 10) lines++;

            Rect status_rect = new Rect(rect.x, rect.y, rect.w, 12);
            int row = 0;
            string txt;

            if (ship != null)
            {
                if (status_rect.y > 320 && !ship.IsStarship())
                    status_rect.y += 32;

                Drive drive = ship.GetDrive();
                if (drive != null)
                {
                    DrawMFDText(row++, Game.GetText("MFD.status.THRUST"), status_rect, TextFormat.DT_LEFT);
                    DrawGauge(status_rect.x + 70, status_rect.y, (int)ship.Throttle());
                    status_rect.y += 10;
                }

                if (lines <= 1) return;

                if (ship.Reactors().Count > 0)
                {
                    PowerSource reactor = ship.Reactors()[0];
                    if (reactor != null)
                    {
                        DrawMFDText(row++, Game.GetText("MFD.status.FUEL"), status_rect, TextFormat.DT_LEFT);
                        DrawGauge(status_rect.x + 70, status_rect.y, reactor.Charge());
                        status_rect.y += 10;
                    }
                }

                if (lines <= 2) return;

                QuantumDrive quantum_drive = ship.GetQuantumDrive();
                if (quantum_drive != null)
                {
                    DrawMFDText(row++, Game.GetText("MFD.status.QUANTUM"), status_rect, TextFormat.DT_LEFT);
                    DrawGauge(status_rect.x + 70, status_rect.y, (int)quantum_drive.Charge());
                    status_rect.y += 10;
                }

                if (lines <= 3) return;

                double hull = ship.Integrity() / ship.Design().integrity * 100;
                ShipSystem.STATUS hull_status = ShipSystem.STATUS.CRITICAL;

                if (hull > 66)
                    hull_status = ShipSystem.STATUS.NOMINAL;
                else if (hull > 33)
                    hull_status = ShipSystem.STATUS.DEGRADED;

                DrawMFDText(row++, Game.GetText("MFD.status.HULL"), status_rect, TextFormat.DT_LEFT);
                DrawGauge(status_rect.x + 70, status_rect.y, (int)hull);
                status_rect.y += 10;

                if (lines <= 4) return;

                Shield shield = ship.GetShield();
                if (shield != null)
                {
                    DrawMFDText(row++, Game.GetText("MFD.status.SHIELD"), status_rect, TextFormat.DT_LEFT);
                    DrawGauge(status_rect.x + 70, status_rect.y, ship.ShieldStrength());
                    status_rect.y += 10;
                }

                if (lines <= 5) return;

                Weapon primary = ship.GetPrimary();
                if (primary != null)
                {
                    DrawMFDText(row++, Game.GetText("MFD.status.GUNS"), status_rect, TextFormat.DT_LEFT);
                    DrawGauge(status_rect.x + 70, status_rect.y, primary.Charge());
                    status_rect.y += 10;
                }

                if (lines <= 6) return;

                if (HUDView.IsArcade())
                {
                    for (int i = 0; i < ship.Weapons().Count && i < 4; i++)
                    {
                        WeaponGroup w = ship.Weapons()[i];

                        if (w.IsMissile())
                        {
                            string ammo;

                            if (ship.GetSecondaryGroup() == w)
                                ammo = string.Format("{0} *", w.Ammo());
                            else
                                ammo = string.Format("{0}", w.Ammo());

                            DrawMFDText(row++, w.GetDesign().name, status_rect, TextFormat.DT_LEFT);
                            status_rect.x += 70;
                            DrawMFDText(row++, ammo, status_rect, TextFormat.DT_LEFT);
                            status_rect.x -= 70;
                            status_rect.y += 10;
                        }
                    }

                    if (ship.GetDecoy() != null)
                    {
                        string ammo;
                        ammo = string.Format("{0}", ship.GetDecoy().Ammo());
                        DrawMFDText(row++, Game.GetText("MFD.status.DECOY"), status_rect, TextFormat.DT_LEFT);
                        status_rect.x += 70;
                        DrawMFDText(row++, ammo, status_rect, TextFormat.DT_LEFT);
                        status_rect.x -= 70;
                        status_rect.y += 10;
                    }

                    if (NetGame.GetInstance() != null)
                    {
                        string lives;
                        lives = string.Format("{0}", ship.RespawnCount() + 1);
                        DrawMFDText(row++, Game.GetText("MFD.status.LIVES"), status_rect, TextFormat.DT_LEFT);
                        status_rect.x += 70;
                        DrawMFDText(row++, lives, status_rect, TextFormat.DT_LEFT);
                        status_rect.x -= 70;
                        status_rect.y += 10;
                    }

                    return;
                }

                Sensor sensor = ship.GetSensor();
                if (sensor != null)
                {
                    if (ship.GetFlightPhase() != OP_MODE.ACTIVE)
                    {
                        DrawMFDText(row++, Game.GetText("MFD.status.SENSOR"), status_rect, TextFormat.DT_LEFT);
                        status_rect.x += 70;
                        DrawMFDText(row++, Game.GetText("MFD.status.OFFLINE"), status_rect, TextFormat.DT_LEFT);
                        status_rect.x -= 70;
                        status_rect.y += 10;
                    }

                    else
                    {
                        DrawMFDText(row++, Game.GetText("MFD.status.EMCON"), status_rect, TextFormat.DT_LEFT);
                        status_rect.x += 70;

                        txt = string.Format("{0} {1}", Game.GetText("MFD.status.MODE"), ship.GetEMCON());

                        if (!sensor.IsPowerOn() || sensor.GetEnergy() == 0)
                        {
                            if (!Game.Paused() && ((Game.RealTime() / 1000) & 2) != 0)
                                txt = string.Format(Game.GetText("MFD.status.SENSOR-OFF"));
                        }

                        DrawMFDText(row++, txt, status_rect, TextFormat.DT_LEFT);
                        status_rect.x -= 70;
                        status_rect.y += 10;
                    }
                }

                if (lines <= 7) return;

                DrawMFDText(row++, Game.GetText("MFD.status.SYSTEMS"), status_rect, TextFormat.DT_LEFT);
                status_rect.x += 70;
                DrawMFDText(row++, ship.GetDirectorInfo(), status_rect, TextFormat.DT_LEFT);

                if (NetGame.GetInstance() != null)
                {
                    string lives;
                    lives = string.Format("{0}", ship.RespawnCount() + 1);
                    status_rect.x -= 70;
                    status_rect.y += 10;
                    DrawMFDText(row++, Game.GetText("MFD.status.LIVES"), status_rect, TextFormat.DT_LEFT);
                    status_rect.x += 70;
                    DrawMFDText(row++, lives, status_rect, TextFormat.DT_LEFT);
                }
            }
        }
        public virtual void DrawSensorMFD()
        {
            int scan_r = rect.w;
            int scan_x = cockpit_hud_texture != null ? ((int)index * 128) : rect.x;
            int scan_y = cockpit_hud_texture != null ? 256 : rect.y;
            int r = scan_r / 2;

            double xctr = (scan_r / 2.0) - 0.5;
            double yctr = (scan_r / 2.0) + 0.5;

            Sensor sensor = ship.GetSensor();
            if (sensor == null)
            {
                DrawMFDText(0, Game.GetText("MFD.inactive"), rect, TextFormat.DT_CENTER);
                return;
            }

            int w = sprite.Width();
            int h = sprite.Height();

            if (w < sprite.Frame().Width())
                w += 2;

            if (h < sprite.Frame().Height())
                h += 16;

            sprite.Reshape(w, h);
            sprite.Show();

            if (h < sprite.Frame().Height())
                return;

            double sweep_scale = r / (Math.PI / 2);

            if (sensor.GetBeamLimit() > 90 * ConstantsF.DEGREES)
                sweep_scale = (double)r / (90 * ConstantsF.DEGREES);

            int az = (int)(sensor.GetBeamLimit() * sweep_scale);
            int el = az;
            int xc = (int)(scan_x + xctr);
            int yc = (int)(scan_y + yctr);

            if (mode == Modes.MFD_MODE_FOV)
            {
                if (sensor.GetMode() < Sensor.Mode.GM)
                {
                    if (cockpit_hud_texture != null)
                        cockpit_hud_texture.DrawEllipse(xc - az, yc - el, xc + az, yc + el, hud_color);
                    else
                        window.DrawEllipse(xc - az, yc - el, xc + az, yc + el, hud_color);
                }
            }
            else
            {
                string az_txt;
                az_txt = string.Format("{0}", (int)(sensor.GetBeamLimit() / ConstantsF.DEGREES));

                Rect az_rect = new Rect(scan_x + 2, scan_y + scan_r - 12, 32, 12);
                DrawMFDText(1, az_txt, az_rect, TextFormat.DT_LEFT);

                az_rect.x = scan_x + (scan_r / 2) - (az_rect.w / 2);
                DrawMFDText(2, "0", az_rect, TextFormat.DT_CENTER);

                az_rect.x = scan_x + scan_r - az_rect.w - 2;
                DrawMFDText(3, az_txt, az_rect, TextFormat.DT_RIGHT);
            }

            // draw next nav point:
            Instruction navpt = ship.GetNextNavPoint();
            if (navpt != null && navpt.Region() == ship.GetRegion())
            {
                CameraNode cam = ship.Cam();

                // translate:
                Point pt = navpt.Location().OtherHand() - ship.Location();

                // rotate:
                double tx = Point.Dot(pt, cam.vrt());
                double ty = Point.Dot(pt, cam.vup());
                double tz = Point.Dot(pt, cam.vpn());

                if (tz > 1.0)
                {
                    // convert to spherical coords:
                    double rng = pt.Length;
                    double az2 = Math.Asin(Math.Abs(tx) / rng);
                    double el2 = Math.Asin(Math.Abs(ty) / rng);

                    if (tx < 0) az2 = -az2;
                    if (ty < 0) el2 = -el2;

                    if (Math.Abs(az2) < 90 * ConstantsF.DEGREES)
                    {
                        az2 *= sweep_scale;
                        el2 *= sweep_scale;

                        int x = (int)(r + az2);
                        int y = (int)(r - el2);

                        // clip again:
                        if (x > 0 && x < scan_r &&
                                y > 0 && y < scan_r)
                        {

                            // draw:
                            int xc2 = scan_x + x;
                            int yc2 = scan_y + y;

                            if (cockpit_hud_texture != null)
                            {
                                cockpit_hud_texture.DrawLine(xc2 - 2, yc2 - 2, xc2 + 2, yc2 + 2, Color.white);
                                cockpit_hud_texture.DrawLine(xc2 - 2, yc2 + 2, xc2 + 2, yc2 - 2, Color.white);
                            }
                            else
                            {
                                window.DrawLine(xc2 - 2, yc2 - 2, xc2 + 2, yc2 + 2, Color.white);
                                window.DrawLine(xc2 - 2, yc2 + 2, xc2 + 2, yc2 - 2, Color.white);
                            }
                        }
                    }
                }
            }

            int num_contacts = ship.NumContacts();

            foreach (Contact contact in ship.ContactList())
            {
                Ship c_ship = contact.GetShip();
                double az2, el2, rng;
                bool aft = false;

                if (c_ship == ship) continue;

                contact.GetBearing(ship, out az2, out el2, out rng);

                // clip (is in-front):
                if (Math.Abs(az2) < 90 * ConstantsF.DEGREES)
                {
                    az2 *= sweep_scale;
                    el2 *= sweep_scale;
                }

                // rear anulus:
                else
                {
                    double len = Math.Sqrt(az2 * az2 + el2 * el2);

                    if (len > 1e-6)
                    {
                        az2 = r * az2 / len;
                        el2 = r * el2 / len;
                    }
                    else
                    {
                        az2 = -r;
                        el2 = 0;
                    }

                    aft = true;
                }

                int x = (int)(r + az2);
                int y = (int)(r - el2);

                // clip again:
                if (x < 0 || x > scan_r) continue;
                if (y < 0 || y > scan_r) continue;

                // draw:
                Color mark = HUDView.MarkerColor(contact);

                if (aft)
                    mark = mark * 0.75f;

                int xc2 = scan_x + x;
                int yc2 = scan_y + y;
                int size = 1;

                if (c_ship != null && c_ship == ship.GetTarget())
                    size = 2;

                if (cockpit_hud_texture != null)
                    cockpit_hud_texture.FillRect(xc2 - size, yc2 - size, xc2 + size, yc2 + size, mark);
                else
                    window.FillRect(xc2 - size, yc2 - size, xc2 + size, yc2 + size, mark);

                if (contact.Threat(ship))
                {
                    if (c_ship != null)
                    {
                        if (cockpit_hud_texture != null)
                            cockpit_hud_texture.DrawEllipse(xc2 - 4, yc2 - 4, xc2 + 3, yc2 + 3, mark);
                        else
                            window.DrawEllipse(xc2 - 4, yc2 - 4, xc2 + 3, yc2 + 3, mark);
                    }
                    else
                    {
                        if (cockpit_hud_texture != null)
                        {
                            cockpit_hud_texture.DrawLine(xc, yc - 5, xc + 5, yc, mark);
                            cockpit_hud_texture.DrawLine(xc + 5, yc, xc, yc + 5, mark);
                            cockpit_hud_texture.DrawLine(xc, yc + 5, xc - 5, yc, mark);
                            cockpit_hud_texture.DrawLine(xc - 5, yc, xc, yc - 5, mark);
                        }
                        else
                        {
                            window.DrawLine(xc, yc - 5, xc + 5, yc, mark);
                            window.DrawLine(xc + 5, yc, xc, yc + 5, mark);
                            window.DrawLine(xc, yc + 5, xc - 5, yc, mark);
                            window.DrawLine(xc - 5, yc, xc, yc - 5, mark);
                        }
                    }
                }
            }

            DrawSensorLabels(Game.GetText("MFD.mode.field-of-view"));
        }

        // HORIZONTAL SITUATION DISPLAY
        public virtual void DrawHSD()
        {
            int scan_r = rect.w;
            int scan_x = cockpit_hud_texture != null ? ((int)index * 128) : rect.x;
            int scan_y = cockpit_hud_texture != null ? 256 : rect.y;
            int r = scan_r / 2 - 4;

            double xctr = (scan_r / 2.0) - 0.5;
            double yctr = (scan_r / 2.0) + 0.5;

            int xc = (int)xctr + scan_x;
            int yc = (int)yctr + scan_y;

            Sensor sensor = ship.GetSensor();
            if (sensor == null)
            {
                DrawMFDText(0, Game.GetText("MFD.inactive"), rect, TextFormat.DT_CENTER);
                return;
            }

            int w = sprite.Width();
            int h = sprite.Height();

            if (w < sprite.Frame().Width())
                w += 2;

            if (h < sprite.Frame().Height())
                h += 16;

            sprite.Reshape(w, h);
            sprite.Show();

            if (h < sprite.Frame().Height())
                return;

            if (sensor.GetMode() < Sensor.Mode.PST)
            {
                double s = Math.Sin(sensor.GetBeamLimit());
                double c = Math.Cos(sensor.GetBeamLimit());

                int x0 = (int)(0.1 * r * s);
                int y0 = (int)(0.1 * r * c);
                int x1 = (int)(1.0 * r * s);
                int y1 = (int)(1.0 * r * c);

                if (cockpit_hud_texture != null)
                {
                    cockpit_hud_texture.DrawLine(xc - x0, yc - y0, xc - x1, yc - y1, hud_color);
                    cockpit_hud_texture.DrawLine(xc + x0, yc - y0, xc + x1, yc - y1, hud_color);
                }
                else
                {
                    window.DrawLine(xc - x0, yc - y0, xc - x1, yc - y1, hud_color);
                    window.DrawLine(xc + x0, yc - y0, xc + x1, yc - y1, hud_color);
                }
            }

            double rscale = (double)r / (sensor.GetBeamRange());

            CameraNode hsd_cam = ship.Cam();
            Point look = ship.Location() + ship.Heading() * 1000;
            look.Y = ship.Location().Y;

            hsd_cam.LookAt(look);

            // draw tick marks on range rings:
            for (int dir = 0; dir < 4; dir++)
            {
                Point tick = new Point();

                switch (dir)
                {
                    case 0: tick = new Point(0, 0, 1000); break;
                    case 1: tick = new Point(1000, 0, 0); break;
                    case 2: tick = new Point(0, 0, -1000); break;
                    case 3: tick = new Point(-1000, 0, 0); break;
                }

                double tx = Point.Dot(tick, hsd_cam.vrt());
                double tz = Point.Dot(tick, hsd_cam.vpn());
                double az = Math.Asin(Math.Abs(tx) / 1000);

                if (tx < 0) az = -az;

                if (tz < 0)
                    if (az < 0) az = -Math.PI - az;
                    else az = Math.PI - az;

                for (double range = 0.3; range < 1; range += 0.3)
                {
                    int x0 = (int)(Math.Sin(az) * r * range);
                    int y0 = (int)(Math.Cos(az) * r * range);
                    int x1 = (int)(Math.Sin(az) * r * (range + 0.1));
                    int y1 = (int)(Math.Cos(az) * r * (range + 0.1));

                    if (cockpit_hud_texture != null)
                    {
                        cockpit_hud_texture.DrawLine(xc + x0, yc - y0, xc + x1, yc - y1, hud_color);
                    }
                    else
                    {
                        window.DrawLine(xc + x0, yc - y0, xc + x1, yc - y1, hud_color);
                    }
                }
            }

            // draw next nav point:
            Instruction navpt = ship.GetNextNavPoint();
            if (navpt != null && navpt.Region() == ship.GetRegion())
            {
                CameraNode cam = hsd_cam;

                // translate:
                Point pt = navpt.Location().OtherHand() - ship.Location();

                // rotate:
                double tx = Point.Dot(pt, cam.vrt());
                double ty = Point.Dot(pt, cam.vup());
                double tz = Point.Dot(pt, cam.vpn());

                // convert to spherical coords:
                double rng = pt.Length;
                double az = Math.Asin(Math.Abs(tx) / rng);

                if (rng > sensor.GetBeamRange())
                    rng = sensor.GetBeamRange();

                if (tx < 0)
                    az = -az;

                if (tz < 0)
                    if (az < 0)
                        az = -Math.PI - az;
                    else
                        az = Math.PI - az;

                // draw:
                int x = (int)(xc + Math.Sin(az) * rng * rscale);
                int y = (int)(yc - Math.Cos(az) * rng * rscale);

                if (cockpit_hud_texture != null)
                {
                    cockpit_hud_texture.DrawLine(x - 2, y - 2, x + 2, y + 2, Color.white);
                    cockpit_hud_texture.DrawLine(x - 2, y + 2, x + 2, y - 2, Color.white);
                }
                else
                {
                    window.DrawLine(x - 2, y - 2, x + 2, y + 2, Color.white);
                    window.DrawLine(x - 2, y + 2, x + 2, y - 2, Color.white);
                }
            }

            // draw contact markers:
            double limit = sensor.GetBeamRange();

            foreach (Contact contact in ship.ContactList())
            {
                Ship c_ship = contact.GetShip();
                if (c_ship == ship) continue;

                // translate:
                Point targ_pt = contact.Location() - hsd_cam.Pos();

                // rotate:
                double tx = Point.Dot(targ_pt, hsd_cam.vrt());
                double rg = contact.Range(ship, limit);
                double true_range = targ_pt.Length;
                double az = Math.Asin(Math.Abs(tx) / true_range);

                // clip:
                if (rg > limit || rg <= 0)
                    continue;

                if (tx < 0)
                    az = -az;

                if (!contact.InFront(ship))
                    if (az < 0)
                        az = -Math.PI - az;
                    else
                        az = Math.PI - az;

                // draw:
                int x = (int)(xc + Math.Sin(az) * rg * rscale);
                int y = (int)(yc - Math.Cos(az) * rg * rscale);
                int size = 2;

                // clip again:
                if (x < scan_x || y < scan_y)
                    continue;

                if (c_ship != null && c_ship == ship.GetTarget())
                    size = 3;

                Color mark = HUDView.MarkerColor(contact);
                if (cockpit_hud_texture != null)
                {
                    cockpit_hud_texture.FillRect(x - size, y - size, x + size, y + size, mark);
                }
                else
                {
                    window.FillRect(x - size, y - size, x + size, y + size, mark);
                }

                if (contact.Threat(ship))
                {
                    if (c_ship != null)
                    {
                        if (cockpit_hud_texture != null)
                        {
                            cockpit_hud_texture.DrawEllipse(x - 4, y - 4, x + 3, y + 3, mark);
                        }
                        else
                        {
                            window.DrawEllipse(x - 4, y - 4, x + 3, y + 3, mark);
                        }
                    }
                    else
                    {
                        if (cockpit_hud_texture != null)
                        {
                            cockpit_hud_texture.DrawLine(x, y - 5, x + 5, y, mark);
                            cockpit_hud_texture.DrawLine(x + 5, y, x, y + 5, mark);
                            cockpit_hud_texture.DrawLine(x, y + 5, x - 5, y, mark);
                            cockpit_hud_texture.DrawLine(x - 5, y, x, y - 5, mark);
                        }
                        else
                        {
                            window.DrawLine(x, y - 5, x + 5, y, mark);
                            window.DrawLine(x + 5, y, x, y + 5, mark);
                            window.DrawLine(x, y + 5, x - 5, y, mark);
                            window.DrawLine(x - 5, y, x, y - 5, mark);
                        }
                    }
                }
            }

            DrawSensorLabels(Game.GetText("MFD.mode.horizontal"));
        }

        // ELITE-STYLE 3D RADAR
        public virtual void Draw3D()
        {
            int scan_r = rect.w;
            int scan_x = cockpit_hud_texture != null ? ((int)index * 128) : rect.x;
            int scan_y = cockpit_hud_texture != null ? 256 : rect.y;
            int r = scan_r / 2 - 4;

            double xctr = (scan_r / 2.0) - 0.5;
            double yctr = (scan_r / 2.0) + 0.5;

            int xc = (int)xctr + scan_x;
            int yc = (int)yctr + scan_y;

            Sensor sensor = ship.GetSensor();
            if (sensor == null)
            {
                DrawMFDText(0, Game.GetText("MFD.inactive"), rect, TextFormat.DT_CENTER);
                return;
            }

            int w = sprite.Width();
            int h = sprite.Height();

            if (w < sprite.Frame().Width())
                w += 2;

            if (h < sprite.Frame().Height())
                h += 16;

            sprite.Reshape(w, h);
            sprite.Show();

            if (h < sprite.Frame().Height())
                return;

            double rscale = (double)r / (sensor.GetBeamRange());

            CameraNode hsd_cam = ship.Cam();

            if (ship.IsStarship())
            {
                Point look = ship.Location() + ship.Heading() * 1000;
                look.Y = ship.Location().Y;

                hsd_cam.LookAt(look);
            }


            // draw next nav point:
            Instruction navpt = ship.GetNextNavPoint();
            if (navpt != null && navpt.Region() == ship.GetRegion())
            {
                CameraNode cam = hsd_cam;

                // translate:
                Point pt = navpt.Location().OtherHand() - ship.Location();

                // rotate:
                double tx = Point.Dot(pt, cam.vrt());
                double ty = Point.Dot(pt, cam.vup());
                double tz = Point.Dot(pt, cam.vpn());

                // convert to cylindrical coords:
                double rng = pt.Length;
                double az = Math.Asin(Math.Abs(tx) / rng);

                if (rng > sensor.GetBeamRange())
                    rng = sensor.GetBeamRange();

                if (tx < 0)
                    az = -az;

                if (tz < 0)
                {
                    if (az < 0)
                        az = -Math.PI - az;
                    else
                        az = Math.PI - az;
                }

                // accentuate vertical:
                if (ty > 10)
                    ty = Math.Log10(ty - 9) * r / 8;

                else if (ty < -10)
                    ty = -Math.Log10(9 - ty) * r / 8;

                else
                    ty = 0;

                // draw:
                int x = (int)(Math.Sin(az) * rng * rscale);
                int y = (int)(Math.Cos(az) * rng * rscale / 2);
                int z = (int)(ty);

                int x0 = xc + x;
                int y0 = yc - y - z;

                if (cockpit_hud_texture != null)
                {
                    cockpit_hud_texture.DrawLine(x0 - 2, y0 - 2, x0 + 2, y0 + 2, Color.white);
                    cockpit_hud_texture.DrawLine(x0 - 2, y0 + 2, x0 + 2, y0 - 2, Color.white);
                }
                else
                {
                    window.DrawLine(x0 - 2, y0 - 2, x0 + 2, y0 + 2, Color.white);
                    window.DrawLine(x0 - 2, y0 + 2, x0 + 2, y0 - 2, Color.white);
                }

                if (cockpit_hud_texture != null)
                {
                    if (z > 0)
                        cockpit_hud_texture.DrawLine(x0, y0 + 1, x0, y0 + z, Color.white);
                    else if (z < 0)
                        cockpit_hud_texture.DrawLine(x0, y0 + z, x0, y0 - 1, Color.white);
                }
                else
                {
                    if (z > 0)
                        window.DrawLine(x0, y0 + 1, x0, y0 + z, Color.white);
                    else if (z < 0)
                        window.DrawLine(x0, y0 + z, x0, y0 - 1, Color.white);
                }
            }


            // draw contact markers:
            double limit = sensor.GetBeamRange();

            foreach (Contact contact in ship.ContactList())
            {
                Ship c_ship = contact.GetShip();
                if (c_ship == ship) continue;

                // translate:
                Point targ_pt = contact.Location() - hsd_cam.Pos();

                // rotate:
                double tx = Point.Dot(targ_pt, hsd_cam.vrt());
                double ty = Point.Dot(targ_pt, hsd_cam.vup());
                double rg = contact.Range(ship, limit);
                double true_range = targ_pt.Length;
                double az = Math.Asin(Math.Abs(tx) / true_range);

                // clip:
                if (rg > limit || rg <= 0)
                    continue;

                if (tx < 0)
                    az = -az;

                if (!contact.InFront(ship))
                    if (az < 0)
                        az = -Math.PI - az;
                    else
                        az = Math.PI - az;

                // accentuate vertical:
                ty *= 4;

                // draw:
                int x = (int)(Math.Sin(az) * rg * rscale);
                int y = (int)(Math.Cos(az) * rg * rscale / 2);
                int z = (int)(ty * rscale / 2);
                int size = 1;

                int x0 = xc + x;
                int y0 = yc - y - z;

                if (c_ship != null && c_ship == ship.GetTarget())
                    size = 2;

                Color mark = HUDView.MarkerColor(contact);

                if (cockpit_hud_texture != null)
                {
                    cockpit_hud_texture.FillRect(x0 - size, y0 - size, x0 + size, y0 + size, mark);

                    if (contact.Threat(ship))
                    {
                        if (c_ship != null)
                        {
                            cockpit_hud_texture.DrawEllipse(x0 - 4, y0 - 4, x0 + 3, y0 + 3, mark);
                        }
                        else
                        {
                            cockpit_hud_texture.DrawLine(x0, y0 - 5, x0 + 5, y0, mark);
                            cockpit_hud_texture.DrawLine(x0 + 5, y0, x0, y0 + 5, mark);
                            cockpit_hud_texture.DrawLine(x0, y0 + 5, x0 - 5, y0, mark);
                            cockpit_hud_texture.DrawLine(x0 - 5, y0, x0, y0 - 5, mark);
                        }
                    }

                    if (z > 0)
                        cockpit_hud_texture.FillRect(x0 - 1, y0 + size, x0, y0 + z, mark);
                    else if (z < 0)
                        cockpit_hud_texture.FillRect(x0 - 1, y0 + z, x0, y0 - size, mark);
                }
                else
                {
                    window.FillRect(x0 - size, y0 - size, x0 + size, y0 + size, mark);

                    if (contact.Threat(ship))
                    {
                        if (c_ship != null)
                        {
                            window.DrawEllipse(x0 - 4, y0 - 4, x0 + 3, y0 + 3, mark);
                        }
                        else
                        {
                            window.DrawLine(x0, y0 - 5, x0 + 5, y0, mark);
                            window.DrawLine(x0 + 5, y0, x0, y0 + 5, mark);
                            window.DrawLine(x0, y0 + 5, x0 - 5, y0, mark);
                            window.DrawLine(x0 - 5, y0, x0, y0 - 5, mark);
                        }
                    }

                    if (z > 0)
                        window.FillRect(x0 - 1, y0 + size, x0, y0 + z, mark);
                    else if (z < 0)
                        window.FillRect(x0 - 1, y0 + z, x0, y0 - size, mark);
                }
            }

            DrawSensorLabels(Game.GetText("MFD.mode.3D"));
        }
        public virtual void DrawSensorLabels(string mfd_mode)
        {
            Sensor sensor = ship.GetSensor();
            string mode_buf = "       ";
            int scan_r = rect.w;
            int scan_x = rect.x;
            int scan_y = rect.y;

            switch (sensor.GetMode())
            {
                case Sensor.Mode.PAS: mode_buf = ContentBundle.Instance.GetText("MFD.mode.passive"); break;
                case Sensor.Mode.STD: mode_buf = ContentBundle.Instance.GetText("MFD.mode.standard"); break;
                case Sensor.Mode.ACM: mode_buf = ContentBundle.Instance.GetText("MFD.mode.auto-combat"); break;
                case Sensor.Mode.GM: mode_buf = ContentBundle.Instance.GetText("MFD.mode.ground"); break;
                case Sensor.Mode.PST: mode_buf = ContentBundle.Instance.GetText("MFD.mode.passive"); break;
                case Sensor.Mode.CST: mode_buf = ContentBundle.Instance.GetText("MFD.mode.combined"); break;
                default: break;
            }
            //Rect mode_rect = new Rect(scan_x + 2, scan_y + 2, 40, 12);
            Rect mode_rect = new Rect(scan_x + 2, scan_y + 2, 40, 16);
            DrawMFDText(0, mode_buf, mode_rect, TextFormat.DT_LEFT);

            string range_txt;
            double beam_range = 123456;//  sensor.GetBeamRange() + 1;
            if (beam_range >= 1e6)
                range_txt = string.Format("-{0}M+", (int)(beam_range / 1e6));
            else
                range_txt = string.Format("-{0:D3}+", (int)(beam_range / 1e3));

            //Rect range_rect = new Rect(scan_x + 2, scan_y + scan_r - 12, 40, 12);
            Rect range_rect = new Rect(scan_r / 2 + 16, -scan_r / 2 + 12, 50, 16);
            DrawMFDText(1, range_txt, range_rect, TextFormat.DT_LEFT);

            //Rect disp_rect = new Rect(scan_x + scan_r - 41, scan_y + 2, 40, 12);
            Rect disp_rect = new Rect(-scan_r / 2 + 41, scan_r / 2 + 2, 50, 16);
            DrawMFDText(2, mfd_mode, disp_rect, TextFormat.DT_RIGHT);


            //Rect probe_rect = new Rect(scan_x + scan_r - 41, scan_y + scan_r - 12, 40, 12);
            Rect probe_rect = new Rect(-scan_r / 2 + 41, -scan_r / 2 + 12, 50, 16);
            if (ship.GetProbeLauncher() != null)
            {
                string probes = string.Format("{0} {1:00}", ContentBundle.Instance.GetText("MFD.probe"), ship.GetProbeLauncher().Ammo());
                DrawMFDText(3, probes, probe_rect, TextFormat.DT_RIGHT);
            }
            else
            {
                HideMFDText(3);
            }
            if (Mouse.LButton() != 0 && !mouse_latch)
            {
                mouse_latch = true;

                if (mode_rect.Contains((int)Mouse.X(), (int)Mouse.Y()))
                {
                    if (sensor.GetMode() < Sensor.Mode.PST)
                    {
                        Sensor.Mode sensor_mode = sensor.GetMode() + 1;
                        if (sensor_mode > Sensor.Mode.GM)
                            sensor_mode = Sensor.Mode.PAS;

                        sensor.SetMode(sensor_mode);
                    }
                }

                else if (range_rect.Contains((int)Mouse.X(), (int)Mouse.Y()))
                {
                    if (Mouse.X() > range_rect.x + range_rect.w / 2)
                        sensor.IncreaseRange();
                    else
                        sensor.DecreaseRange();
                }

                else if (disp_rect.Contains((int)Mouse.X(), (int)Mouse.Y()))
                {
                    HUDView hud = HUDView.GetInstance();
                    if (hud != null)
                        hud.CycleMFDMode(index);
                }

                else if (probe_rect.Contains((int)Mouse.X(), (int)Mouse.Y()))
                {
                    ship.LaunchProbe();
                }
            }
        }

        // GROUND MAP
        public virtual void DrawMap()
        {
            DrawMFDText(0, ContentBundle.Instance.GetText("MFD.mode.ground"), new Rect(rect.x, rect.y, rect.w, 12), TextFormat.DT_CENTER);
        }
        public virtual void DrawGauge(int x, int y, float percent)
        {
            if (cockpit_hud_texture != null)
            {
                x += (int)this.index * 128 - this.rect.x;
                y += 256 - this.rect.y;
                cockpit_hud_texture.DrawRect(x, y, x + 53, y + 8, ColorExtensions.DarkGray);
            }
            else
            {
                window.DrawRect(x, y, x + 53, y + 8, ColorExtensions.DarkGray);
            }

            if (percent < 3) return;
            if (percent > 100) percent = 100;

            percent /= 2;

            if (cockpit_hud_texture != null)
                cockpit_hud_texture.FillRect(x + 2, y + 2, (int)(x + 2 + percent), y + 7, ColorExtensions.Gray);
            else
                window.FillRect(x + 2, y + 2, (int)(x + 2 + percent), y + 7, ColorExtensions.Gray);
        }
        public virtual void SetStatusColor(ShipSystem.STATUS status)
        {
            Color status_color;

            switch (status)
            {
                default:
                case ShipSystem.STATUS.NOMINAL: status_color = txt_color; break;
                case ShipSystem.STATUS.DEGRADED: status_color = new Color32(255, 255, 0, 255); break;
                case ShipSystem.STATUS.CRITICAL: status_color = new Color32(255, 0, 0, 255); break;
                case ShipSystem.STATUS.DESTROYED: status_color = new Color32(0, 0, 0, 255); break;
            }
        }

        public virtual void SetWindow(Window w) { window = w; }
        public virtual Window GetWindow() { return window; }
        public virtual void SetRect(Rect r)
        {
            rect = r;

            if (sprite != null)
                sprite.MoveTo(new Point(rect.x + sprite.Width() / 2,
                                        rect.y + sprite.Height() / 2,
                                        1));
        }
        public virtual Rect GetRect() { return rect; }
        public virtual void SetMode(Modes m)
        {
            if (m < Modes.MFD_MODE_OFF || m > Modes.MFD_MODE_3D)
                mode = Modes.MFD_MODE_OFF;
            else
                mode = m;

            sprite.Hide();

            for (int i = 0; i < TXT_LAST; i++)
                HideMFDText(i);

            sprite.Hide();
            switch (mode)
            {
                case Modes.MFD_MODE_GAME:
                case Modes.MFD_MODE_SHIP:
                    lines = 0;
                    break;

                case Modes.MFD_MODE_FOV:
                    //sprite.SetAnimation(sensor_fov);
                    sprite = sensor_fov_sprite;
                    sprite.Show();
                    sprite.Reshape(sensor_fov.Width() - 8, 16);
                    break;
                case Modes.MFD_MODE_HSD:
                    //sprite.SetAnimation(sensor_hsd);
                    sprite = sensor_hsd_sprite;
                    sprite.Show();
                    sprite.Reshape(sensor_hsd.Width() - 8, 16);
                    break;
                case Modes.MFD_MODE_3D:
                    //sprite.SetAnimation(sensor_3d);
                    sprite = sensor_3d_sprite;
                    sprite.Show();
                    sprite.Reshape(sensor_3d.Width() - 8, 16);
                    break;
            }
        }
        public virtual MFD.Modes GetMode() { return mode; }

        public virtual void SetShip(Ship s) { ship = s; }
        public virtual Ship GetShip() { return ship; }

        public virtual void Show()
        {
            switch (mode)
            {
                case Modes.MFD_MODE_FOV:
                case Modes.MFD_MODE_HSD:
                case Modes.MFD_MODE_3D:
                    if (sprite != null)
                        sprite.Show();
                    break;
            }

            hidden = false;
        }
        public virtual void Hide()
        {
            if (sprite != null)
                sprite.Hide();

            for (int i = 0; i < TXT_LAST; i++)
                HideMFDText(i);

            hidden = true;
        }

        public virtual void UseCameraView(CameraView v)
        {
            if (v != null && camview == null)
            {
                camview = v;
            }
        }
        public void DrawMFDText(int index, string txt, Rect txt_rect, TextFormat align, ShipSystem.STATUS status = ShipSystem.STATUS.NOMINAL)
        {
            if (index >= MFD.TXT_LAST)
            {
                ErrLogger.PrintLine("MFD DrawMFDText() invalid mfd_text index {0} '{1}'", index, txt);
            }
            else
            {
                HUDText mt = mfd_text[index];
                Color mc = mt.color;

                switch (status)
                {
                    default:
                    case ShipSystem.STATUS.NOMINAL: mc = txt_color; break;
                    case ShipSystem.STATUS.DEGRADED: mc = new Color32(255, 255, 0, 255); break;
                    case ShipSystem.STATUS.CRITICAL: mc = new Color32(255, 0, 0, 255); break;
                    case ShipSystem.STATUS.DESTROYED: mc = new Color32(0, 0, 0, 255); break;
                }

                txt = txt.ToUpper();

                if (cockpit_hud_texture != null)
                {
                    Rect hud_rect = txt_rect;

                    hud_rect.x = txt_rect.x + (int)index * 128 - this.rect.x;
                    hud_rect.y = txt_rect.y + 256 - this.rect.y;

                    mt.SetColor(mc);
                    mt.DrawText(txt, hud_rect, align | TextFormat.DT_SINGLELINE, cockpit_hud_texture);
                    mt.rect = rect;
                    mt.Hidden(false);
                }
                else
                {
                    if (txt_rect.Contains((int)Mouse.X(), (int)Mouse.Y()))
                        mc = Color.white;

                    mt.SetColor(mc);
                    mt.DrawText(txt, txt_rect, align | TextFormat.DT_SINGLELINE);
                    mt.rect = rect;
                    mt.Hidden(false);
                }

            }
            ErrLogger.PrintLine("MFD DrawMFDText() is not yet implemented, text = {0}", txt);
        }
        public void HideMFDText(int index)
        {
            {
                if (index >= TXT_LAST)
                    ErrLogger.PrintLine("MFD HideMFDText() invalid mfd_text index {0}", index);
                else
                    mfd_text[index].Hidden(true);
            }
        }
        public void ShowMFDText(int index)
        {
            {
                if (index >= TXT_LAST)
                    ErrLogger.PrintLine("MFD ShowMFDText() invalid mfd_text index {0}", index);
                else
                    mfd_text[index].Hidden(false);
            }
        }
        public void SetText3DColor(Color c)
        {
            for (int i = 0; i < TXT_LAST; i++)
                mfd_text[i].SetColor(c);
        }
        public void SetCockpitHUDTexture(Bitmap bmp)
        {
            cockpit_hud_texture = bmp;
        }

        public bool IsMouseLatched()
        {
            return mouse_in;
        }

        public Modes CycleMFDMode()
        {
            int m = (int)mode;
            m++;
            SetMode((Modes)m);
            return GetMode();

        }
        public void CycleMFDColor()
        {
            colorindex++;
            if (colorindex >= HUDView.NUM_HUD_COLORS) colorindex = 0;
            Color hud_color = HUDView.standard_hud_colors[colorindex];
            SetColor(hud_color);
            txt_color = HUDView.standard_txt_colors[colorindex];
            SetText3DColor(txt_color);
        }

        protected const int TXT_LAST = 20;
        protected int colorindex = 0;
        protected Window window;
        protected Rect rect = new Rect(0, 0, 0, 0);
        protected Modes index;
        protected MFD.Modes mode = Modes.MFD_MODE_OFF;
        protected int lines = 0;
        protected SpriteGraphic sprite = null;
        protected bool hidden = true;
        protected Ship ship = null;
        protected HUDText[] mfd_text = new HUDText[TXT_LAST];
        protected CameraView camview = null;
        protected Bitmap cockpit_hud_texture = null;

        protected bool mouse_latch = false;
        protected bool mouse_in = false;

        protected static Bitmap sensor_fov;
        protected static Bitmap sensor_fwd;
        protected static Bitmap sensor_hsd;
        protected static Bitmap sensor_3d;
        protected static SpriteGraphic sensor_fov_sprite = null;
        protected static SpriteGraphic sensor_fwd_sprite = null;
        protected static SpriteGraphic sensor_hsd_sprite = null;
        protected static SpriteGraphic sensor_3d_sprite = null;

        protected static Color hud_color = Color.black;
        protected static Color txt_color = Color.black;

        protected static GameObject rightParent;
        protected static GameObject leftParent;

        protected static GameObject MFDCanvas;

        protected static GameObject prefabMFD;
        protected static GameObject prefabRect;
        protected static FontItem font;
        protected static bool initialized = false;
        private static int numCanvas = 0;
    }
}
