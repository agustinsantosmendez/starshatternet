﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      ListBox.h/ListBox.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    ListBox ActiveWindow class
*/
using System;
using System.Collections.Generic;
using System.Linq;
using StarshatterNet.Stars.Views;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.Int32;

namespace StarshatterNet.Gen.Forms
{
    public class ListBox : ScrollWindow
    {
        public enum SORT
        {
            LIST_SORT_NUMERIC_DESCENDING = -2,
            LIST_SORT_ALPHA_DESCENDING,
            LIST_SORT_NONE,
            LIST_SORT_ALPHA_ASCENDING,
            LIST_SORT_NUMERIC_ASCENDING,
            LIST_SORT_NEVER
        };

        public enum ALIGN
        {
            LIST_ALIGN_LEFT = TextFormat.DT_LEFT,
            LIST_ALIGN_CENTER = TextFormat.DT_CENTER,
            LIST_ALIGN_RIGHT = TextFormat.DT_RIGHT
        };

        public enum STYLE
        {
            LIST_ITEM_STYLE_PLAIN,
            LIST_ITEM_STYLE_BOX,
            LIST_ITEM_STYLE_FILLED_BOX
        }
        public ListBox(ActiveWindow p, int ax, int ay, int aw, int ah, DWORD aid) : base(ax, ay, aw, ah, aid, p)
        {
            show_headings = false;
            multiselect = false;
            list_index = 0;
            selcount = 0;

            selected_color = new Color32(255, 255, 128, 255);

            sort_column = 0;
            item_style = STYLE.LIST_ITEM_STYLE_PLAIN;
            seln_style = STYLE.LIST_ITEM_STYLE_PLAIN;

            desc = "ListBox " + id;
        }


        // Operations:
        public override void DrawContent(Rect ctrl_rect) { throw new NotImplementedException(); }

        // Event Target Interface:
        public override int OnMouseMove(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonDown(int x, int y) { throw new NotImplementedException(); }
        public override int OnLButtonUp(int x, int y) { throw new NotImplementedException(); }
        public override int OnMouseWheel(int wheel) { throw new NotImplementedException(); }
        public override int OnClick()
        {
            bool fire_select = (scrolling == SCROLL.SCROLL_NONE);

            if (scrolling == SCROLL.SCROLL_THUMB)
                scrolling = SCROLL.SCROLL_NONE;

            if (fire_select)
                return base.OnSelect();
            else
                return base.OnClick();

        }
        public override int OnKeyDown(int vk, int flags) { throw new NotImplementedException(); }

        private void OnClickEvent(int itemPos)
        {
            Debug.Log("OnClickEvent Item: " + items[itemPos]);
            list_index = itemPos;
            if (!multiselect)
                ClearSelection();
            SetSelected(list_index);
            EnsureVisible(list_index);
            Button.PlaySound(Button.SOUNDS.SND_LIST_SELECT);

            OnClick();
        }

        // pseudo-events:
        public override int OnDragStart(int x, int y) { throw new NotImplementedException(); }
        public override int OnDragDrop(int x, int y, ActiveWindow source) { throw new NotImplementedException(); }

        // Property accessors:
        public int NumItems()
        {
            return items.Count;
        }
        public int NumColumns()
        {
            return columns.Count;
        }

        public string GetItemText(int index)
        {
            if (index >= 0 && index < items.Count)
                return items[index].text;

            return "";
        }
        public void SetItemText(int index, string text)
        {
            if (index >= 0 && index < items.Count)
            {
                items[index].text = text;
            }
        }
        public IComparable GetItemData(int index)
        {
            if (index >= 0 && index < items.Count)
                return items[index].data;

            return 0;
        }
        public void SetItemData(int index, DWORD data)
        {
            if (index >= 0 && index < items.Count)
            {
                items[index].data = data;
            }
        }
        public Texture2D GetItemImage(int index)
        {
            if (index >= 0 && index < items.Count)
                return items[index].image;

            return null;
        }
        public void SetItemImage(int index, Texture2D img)
        {
            if (index >= 0 && index < items.Count)
            {
                items[index].image = img;
            }
        }
        public Color GetItemColor(int index)
        {
            if (index >= 0 && index < items.Count)
                return items[index].color;

            return Color.white;
        }
        public void SetItemColor(int index, Color c)
        {
            if (index >= 0 && index < items.Count)
            {
                items[index].color = c;
            }
        }

        public string GetItemText(int index, int column)
        {
            if (column == 0)
            {
                return GetItemText(index);
            }

            if (index >= 0 && index < items.Count)
            {
                ListBoxItem item = items[index];

                column--;
                if (column >= 0 && column < item.subitems.Count)
                    return item.subitems[column].text;
            }

            return "";
        }
        public void SetItemText(int index, int column, string text)
        {
            if (column == 0)
            {
                SetItemText(index, text);
                return;
            }

            if (index >= 0 && index < items.Count)
            {
                ListBoxItem item = items[index];

                column--;
                if (column >= 0 && column < columns.Count - 1)
                {
                    while (column >= item.subitems.Count)
                    {
                        ListBoxCell cell = new ListBoxCell();
                        if (cell != null)
                            item.subitems.Add(cell);
                    }

                    item.subitems[column].text = text;
                }
            }
        }
        public IComparable GetItemData(int index, int column)
        {
            if (column == 0)
            {
                return GetItemData(index);
            }

            if (index >= 0 && index < items.Count)
            {
                ListBoxItem item = items[index];

                column--;
                if (column >= 0 && column < item.subitems.Count)
                    return item.subitems[column].data;
            }

            return 0;
        }
        public void SetItemData(int index, int column, DWORD data)
        {
            if (column == 0)
            {
                SetItemData(index, data);
                return;
            }

            if (index >= 0 && index < items.Count)
            {
                ListBoxItem item = items[index];

                column--;
                if (column >= 0 && column < columns.Count - 1)
                {
                    while (column >= item.subitems.Count)
                    {
                        ListBoxCell cell = new ListBoxCell();
                        if (cell != null)
                            item.subitems.Add(cell);
                    }

                    item.subitems[column].data = data;
                }
            }
        }
        public Texture2D GetItemImage(int index, int column)
        {
            if (column == 0)
            {
                return GetItemImage(index);
            }

            if (index >= 0 && index < items.Count)
            {
                ListBoxItem item = items[index];

                column--;
                if (column >= 0 && column < item.subitems.Count)
                    return item.subitems[column].image;
            }

            return null;
        }
        public void SetItemImage(int index, int column, Texture2D img)
        {
            if (column == 0)
            {
                SetItemImage(index, img);
                return;
            }

            if (index >= 0 && index < items.Count)
            {
                ListBoxItem item = items[index];

                column--;
                if (column >= 0 && column < columns.Count - 1)
                {
                    while (column >= item.subitems.Count)
                    {
                        ListBoxCell cell = new ListBoxCell();
                        if (cell != null)
                            item.subitems.Add(cell);
                    }

                    item.subitems[column].image = img;
                }
            }
        }

        public int AddItem(string text)
        {
            ListBoxItem item = new ListBoxItem();

            if (item != null)
            {
                item.text = text;
                item.color = fore_color;
                item.listbox = this;

                items.Add(item);

                line_count = items.Count;
                list_index = items.Count - 1;
            }

            RebuildListObjets();

            return list_index + 1;
        }

        public int AddImage(Texture2D img)
        {
            ListBoxItem item = new ListBoxItem();

            if (item != null)
            {
                item.image = img;
                item.color = fore_color;
                item.listbox = this;

                items.Add(item);

                line_count = items.Count;
                list_index = items.Count - 1;
            }

            return list_index + 1;
        }
        public int AddItemWithData(string text, IComparable data)
        {
            ListBoxItem item = new ListBoxItem();

            if (item != null)
            {
                item.text = text;
                item.data = data;
                item.color = fore_color;
                item.listbox = this;

                items.Add(item);

                line_count = items.Count;
                list_index = items.Count - 1;
            }

            return list_index + 1;
        }
        public void InsertItem(int index, string text)
        {
            if (index >= 0 && index < items.Count)
            {
                ListBoxItem item = new ListBoxItem();

                if (item != null)
                {
                    item.text = text;
                    item.color = fore_color;
                    item.listbox = this;

                    list_index = index;
                    items.Insert(list_index, item);
                    line_count = items.Count;
                }
            }
        }
        public void InsertItemWithData(int index, string text, int data)
        {
            if (index >= 0 && index < items.Count)
            {
                ListBoxItem item = new ListBoxItem();

                if (item != null)
                {
                    item.text = text;
                    item.data = data;
                    item.color = fore_color;
                    item.listbox = this;

                    list_index = index;
                    items.Insert(list_index, item);
                    line_count = items.Count;
                }
            }
        }
        public void ClearItems()
        {
            items.Clear();
            selcount = 0;
            top_index = 0;
            list_index = 0;
            line_count = 0;
        }
        public void RemoveItem(int index)
        {
            if (index >= 0 && index < items.Count)
            {
                if (items[index].selected)
                    selcount--;
                items.RemoveAt(index);
                line_count = items.Count;
            }
        }
        public void RemoveSelectedItems()
        {
            if (selcount != 0)
            {
                for (int i = items.Count - 1; i >= 0; i--)
                {
                    ListBoxItem item = items[i];
                    if (item.selected)
                    {
                        items.RemoveAt(i);
                    }
                }

                line_count = items.Count;
                selcount = 0;
            }
        }

        public void AddColumn(string title, int width, ALIGN align = ALIGN.LIST_ALIGN_LEFT, SORT sort = SORT.LIST_SORT_NONE)
        {
            ListBoxColumn column = new ListBoxColumn();

            if (column != null)
            {
                column.title = title;
                column.width = width;
                column.align = align;
                column.sort = sort;

                //column.columnObj = new GameObject();
                //RectTransform rectTransform = column.columnObj.GetComponent<RectTransform>();
                //rectTransform.SetParent(content.transform);

                //var vlg = column.columnObj.AddComponent<VerticalLayoutGroup>();
                //vlg.childForceExpandHeight = false;
                //vlg.childForceExpandWidth = true;
                //vlg.childControlHeight = false;
                //vlg.childControlWidth = false;
                //vlg.childAlignment = TextAnchor.UpperLeft;

                columns.Add(column);
            }
        }

        public string GetColumnTitle(int index)
        {
            if (index >= 0 && index < columns.Count)
                return columns[index].title;

            return "";
        }
        public void SetColumnTitle(int index, string title)
        {
            if (index >= 0 && index < columns.Count)
            {
                columns[index].title = title;
            }
        }
        public int GetColumnWidth(int index)
        {
            if (index >= 0 && index < columns.Count)
                return columns[index].width;

            return 0;
        }
        public void SetColumnWidth(int index, int width)
        {
            if (index >= 0 && index < columns.Count)
            {
                columns[index].width = width;
            }
        }
        public ALIGN GetColumnAlign(int index)
        {
            if (index >= 0 && index < columns.Count)
                return columns[index].align;

            return 0;
        }
        public void SetColumnAlign(int index, ALIGN align)
        {
            if (index >= 0 && index < columns.Count)
            {
                columns[index].align = align;
            }
        }
        public SORT GetColumnSort(int index)
        {
            if (index >= 0 && index < columns.Count)
                return columns[index].sort;

            return 0;
        }

        public void SetColumnSort(int index, SORT sort)
        {
            if (index >= 0 && index < columns.Count)
            {
                columns[index].sort = sort;
            }
        }
        public Color GetColumnColor(int index)
        {
            if (index >= 0 && index < columns.Count)
                return columns[index].color;

            return Color.white;
        }
        public void SetColumnColor(int index, Color c)
        {
            if (index >= 0 && index < columns.Count)
            {
                columns[index].color = c;
                columns[index].use_color = true;
            }
        }

        public Color GetItemColor(int index, int column)
        {
            Color c = Color.white;

            if (index >= 0 && index < items.Count)
                c = items[index].color;

            if (column >= 0 && column < columns.Count)
            {
                if (columns[column].use_color)
                    c = columns[column].color;
            }

            return c;
        }

        public bool GetMultiSelect()
        {
            return multiselect;
        }
        public void SetMultiSelect(bool nNewValue)
        {
            if (multiselect != nNewValue)
            {
                multiselect = nNewValue;
                ClearSelection();
            }
        }
        public bool GetShowHeadings()
        {
            return show_headings;
        }
        public void SetShowHeadings(bool nNewValue)
        {
            if (show_headings != nNewValue)
            {
                show_headings = nNewValue;
            }
        }
        public Color GetSelectedColor()
        {
            return selected_color;
        }
        public void SetSelectedColor(Color c)
        {
            if (selected_color != c)
            {
                selected_color = c;
            }
        }

        public STYLE GetItemStyle()
        {
            return item_style;
        }
        public void SetItemStyle(STYLE style)
        {
            if (style >= STYLE.LIST_ITEM_STYLE_PLAIN && style <= STYLE.LIST_ITEM_STYLE_FILLED_BOX)
            {
                item_style = style;
            }
        }
        public STYLE GetSelectedStyle()
        {
            return seln_style;
        }
        public void SetSelectedStyle(STYLE style)
        {
            if (style >= STYLE.LIST_ITEM_STYLE_PLAIN && style <= STYLE.LIST_ITEM_STYLE_FILLED_BOX)
            {
                seln_style = style;
            }
        }

        public bool IsSelected(int index)
        {
            if (index >= 0 && index < items.Count)
                return items[index].selected;

            return false;
        }
        public void SetSelected(int index, bool bNewValue = true)
        {
            if (index >= 0 && index < items.Count)
            {
                if (!multiselect)
                    ClearSelection();

                if (items[index].selected != bNewValue)
                {
                    items[index].selected = bNewValue;

                    if (bNewValue)
                    {
                        list_index = index;
                        selcount++;
                    }
                    else
                    {
                        selcount--;
                    }
                }
            }
        }
        public void ClearSelection()
        {
            foreach (ListBoxItem item in items)
                item.selected = false;

            selcount = 0;
        }

        public int GetSortColumn()
        {
            return sort_column;
        }
        public void SetSortColumn(int col_index)
        {
            if (col_index >= 0 && col_index <= columns.Count)
                sort_column = col_index;
        }
        public SORT GetSortCriteria()
        {
            return GetColumnSort(sort_column);
        }
        public void SetSortCriteria(SORT sort)
        {
            SetColumnSort(sort_column, sort);
        }
        public void SortItems()
        {
            if (sort_column >= 0 && sort_column <= columns.Count)
                items.Sort();
        }
        public void SizeColumns()
        {
            ListBoxColumn c = columns.First();

            if (c.percent < 0.001)
            {
                double total = 0;

                foreach (ListBoxColumn iter in columns)
                {
                    c = iter;
                    total += c.width;
                }

                foreach (ListBoxColumn iter in columns)
                {
                    c = iter;
                    c.percent = c.width / total;
                }
            }

            int usable_width = rect.w;
            int used = 0;

            if (IsScrollVisible())
            {
                usable_width -= (int)MISC.SCROLL_WIDTH + 2;
            }
            else
            {
                usable_width -= 3;
            }

            for (int i = 0; i < columns.Count; i++)
            {
                c = columns[i];

                if (i < columns.Count - 1)
                    c.width = (int)(c.percent * usable_width);
                else
                    c.width = usable_width - used;

                used += c.width;
            }
        }

        // read-only:
        public virtual int GetListIndex()
        {
            return list_index;
        }
        public override int GetLineCount()
        {
            line_count = items.Count;
            return line_count;
        }
        public virtual int GetSelCount()
        {
            return selcount;
        }
        public virtual int GetSelection()
        {
            for (int i = 0; i < items.Count; i++)
                if (items[i].selected)
                    return i;

            return -1;
        }
        public virtual string GetSelectedItem()
        {
            int n = GetSelection();
            return GetItemText(n);
        }

        protected int IndexFromPoint(int x, int y)
        {
            int sel_index = -1;

            if (show_headings)
                sel_index = top_index + (y - (line_height + (int)MISC.HEADING_EXTRA)) / (line_height + leading);

            else
                sel_index = top_index + y / (line_height + leading);

            return sel_index;
        }

        protected internal override void BuildGamebjects()
        {
            base.BuildGamebjects();

            foreach (var column in columns)
            {
                column.columnObj = new GameObject("Column: " + column.title);
                RectTransform rectTransform = column.columnObj.AddComponent<RectTransform>();
                rectTransform.SetParent(content.transform);

                var vlg = column.columnObj.AddComponent<VerticalLayoutGroup>();
                vlg.childForceExpandHeight = false;
                vlg.childForceExpandWidth = true;
                vlg.childControlHeight = false;
                vlg.childControlWidth = true;
                vlg.childAlignment = TextAnchor.UpperLeft;
            }

            RebuildListObjets();
        }
        public void RebuildListObjets()
        {
            if (content == null) return;
            DefaultControls.Resources uiResources = new DefaultControls.Resources();

            for (int i = 0; i < columns.Count; i++)
            {
                ListBoxColumn column = columns[i];
                foreach (Transform child in column.columnObj.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
                for (int pos = 0; pos < items.Count; pos++)
                {
                    ListBoxItem item = null;
                    ListBoxCell cell = null;
                    string txt = null;
                    if (i == 0)
                    {
                        item = items[pos];
                        txt = item.text;
                    }
                    else
                    {
                        if (items[pos].subitems.Count == 0)
                            for (int j = 0; j < columns.Count - 1; j++)
                            {
                                ListBoxCell lbc = new ListBoxCell();
                                items[pos].subitems.Add(lbc);
                            }
                        cell = items[pos].subitems[i - 1];
                        txt = cell.text;
                    }
                    GameObject obj2 = DefaultControls.CreateButton(uiResources);

                    var le = obj2.AddComponent<LayoutElement>();
                    le.preferredHeight = line_height;
                    le.preferredWidth = GetRect().w - (int)MISC.SCROLL_WIDTH - LeftPadding; // column.width;
                    RectTransform rectTransform = obj2.GetComponent<RectTransform>();
                    rectTransform.SetParent(column.columnObj.transform);

                    Image buttonImg = obj2.GetComponentInChildren<Image>();
                    Color c = buttonImg.color;
                    c.a = 0;
                    buttonImg.color = c;

                    Text buttonText = obj2.GetComponentInChildren<Text>();
                    buttonText.font = GetFont().font;
                    buttonText.fontSize = GetFontSize();
                    buttonText.text = txt;

                    buttonText.color = Color.white;
                    buttonText.alignment = TextAnchor.MiddleLeft;

                    obj2.name = desc + " " + txt;

                    UnityEngine.UI.Button button = obj2.GetComponent<UnityEngine.UI.Button>();
                    int copy = pos; // to avoid C# Closures. It is a captured variable 
                    button.onClick.AddListener(() => { OnClickEvent(copy); });

                }
            }
        }
        // properties:
        protected List<ListBoxItem> items = new List<ListBoxItem>();
        protected List<ListBoxColumn> columns = new List<ListBoxColumn>();

        protected bool show_headings;
        protected bool multiselect;
        protected int list_index;
        protected int selcount;

        protected Color selected_color;

        protected int sort_column;
        protected STYLE item_style;
        protected STYLE seln_style;

        private static int old_cursor;
    }

    public class ListBoxCell
    {
        public ListBoxCell() { data = 0; image = null; }

        public string text;
        public DWORD data;
        public Texture2D image;
    }

    public class ListBoxItem
    {
        public ListBoxItem()
        {
            data = 0; image = null; selected = false; listbox = null; color = Color.white;
        }
        //  public ~ListBoxItem() { subitems.destroy(); }
        public static int Compare(ListBoxItem l, ListBoxItem r)
        {
            if (l.listbox != null && l.listbox == r.listbox)
            {
                int sort_column = l.listbox.GetSortColumn() - 1;
                ListBox.SORT sort_criteria = (ListBox.SORT)l.listbox.GetSortCriteria();

                if (sort_column == -1)
                {
                    switch (sort_criteria)
                    {
                        case ListBox.SORT.LIST_SORT_NUMERIC_DESCENDING:
                            return l.data.CompareTo(r.data);

                        case ListBox.SORT.LIST_SORT_ALPHA_DESCENDING:
                            return String.Compare(l.text, r.text, StringComparison.OrdinalIgnoreCase);

                        case ListBox.SORT.LIST_SORT_ALPHA_ASCENDING:
                            return String.Compare(l.text, r.text, StringComparison.OrdinalIgnoreCase);

                        case ListBox.SORT.LIST_SORT_NUMERIC_ASCENDING:
                            return l.data.CompareTo(r.data);
                    }
                }
                else if (sort_column >= 0 &&
                        sort_column <= l.subitems.Count &&
                        sort_column <= r.subitems.Count)
                {
                    switch (sort_criteria)
                    {
                        case ListBox.SORT.LIST_SORT_NUMERIC_DESCENDING:
                            return l.subitems[sort_column].data.CompareTo(r.subitems[sort_column].data);

                        case ListBox.SORT.LIST_SORT_ALPHA_DESCENDING:
                            return String.Compare(l.subitems[sort_column].text, r.subitems[sort_column].text, StringComparison.OrdinalIgnoreCase);

                        case ListBox.SORT.LIST_SORT_ALPHA_ASCENDING:
                            return String.Compare(l.subitems[sort_column].text, r.subitems[sort_column].text, StringComparison.OrdinalIgnoreCase);

                        case ListBox.SORT.LIST_SORT_NUMERIC_ASCENDING:
                            return l.subitems[sort_column].data.CompareTo(r.subitems[sort_column].data);
                    }
                }
            }

            return 0;
        }
        public override bool Equals(object obj)
        {
            var other = obj as ListBoxItem;
            if (object.ReferenceEquals(other, null))
            {
                return false;
            }
            return Compare(this, other) == 0;
        }

        public override int GetHashCode()
        {
            var hashCode = 993618486;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(text);
            hashCode = hashCode * -1521134295 + data.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Texture2D>.Default.GetHashCode(image);
            hashCode = hashCode * -1521134295 + selected.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Color>.Default.GetHashCode(color);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<ListBoxCell>>.Default.GetHashCode(subitems);
            hashCode = hashCode * -1521134295 + EqualityComparer<ListBox>.Default.GetHashCode(listbox);
            return hashCode;
        }

        public static bool operator <(ListBoxItem left, ListBoxItem right)
        {
            return (Compare(left, right) < 0);
        }
        public static bool operator >(ListBoxItem left, ListBoxItem right)
        {
            return (Compare(left, right) > 0);
        }
        public static bool operator ==(ListBoxItem left, ListBoxItem right)
        {
            if (object.ReferenceEquals(left, null))
            {
                return object.ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }
        public static bool operator !=(ListBoxItem left, ListBoxItem right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return text + ":" + data;
        }

        public string text;
        public IComparable data;
        public Texture2D image;
        public bool selected;
        public Color color;
        public List<ListBoxCell> subitems = new List<ListBoxCell>();

        public ListBox listbox;
        public GameObject itemObj;
    }
    public class ListBoxColumn
    {
        public ListBoxColumn()
        {
            width = 0; align = 0; sort = 0; color = Color.white; use_color = false; percent = 0;
        }

        public string title;
        public int width;
        public ListBox.ALIGN align;
        public ListBox.SORT sort;
        public Color color;
        public bool use_color;

        public double percent;

        public GameObject columnObj;
    }
}
