﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      System.h/System.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Generic ship System class
*/
using System;
using StarshatterNet.Gen.Misc;
using System.Collections.Generic;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.Graphics.General;

namespace StarshatterNet.Stars.SimElements
{
    public class ShipSystem
    {
        public enum CATEGORY
        {
            MISC_SYSTEM = 0,
            DRIVE = 1,
            WEAPON,
            SHIELD,
            SENSOR,
            COMPUTER,
            POWER_SOURCE,
            FLIGHT_DECK,
            FARCASTER
        }
        public enum STATUS
        {
            DESTROYED,
            CRITICAL,
            DEGRADED,
            NOMINAL,
            MAINT
        }

        [Flags]
        public enum POWER_FLAGS
        {
            POWER_WATTS = 1,
            POWER_CRITICAL = 2
        }

        public ShipSystem(CATEGORY t, int s, string n, int maxv, double energy = 0,
                      double capacity = 100, double sink_rate = 1)
        {

            this.type = t;
            this.subtype = s;
            this.name = n;
            this.abrv = name;
            this.energy = (float)energy;
            this.capacity = (float)capacity;
            this.sink_rate = (float)sink_rate;

            if (maxv < 100)
                max_value = maxv;
            else
                max_value = (int)(maxv / 100.0);

            emcon_power[0] = 100;
            emcon_power[1] = 100;
            emcon_power[2] = 100;
        }

        public ShipSystem(ShipSystem s)
        {
            type = s.type;
            id = s.id;
            ship = null;
            subtype = s.subtype;
            status = s.status;
            availability = s.availability;
            safety = s.safety;
            stability = s.stability;
            crit_level = s.crit_level;
            net_avail = -1.0f;
            mount_rel = s.mount_rel;
            radius = s.radius;
            safety_overload = 0.0f;
            hull_factor = s.hull_factor;
            energy = s.energy;
            capacity = s.capacity;
            sink_rate = s.sink_rate;
            power_level = s.power_level;
            power_flags = s.power_flags;
            source_index = s.source_index;
            power_on = s.power_on;
            max_value = s.max_value;
            explosion_type = s.explosion_type;
            name = s.name;
            abrv = s.abrv;
            design = s.design;
            emcon = s.emcon;
            if (design != null)
            {
                foreach (var c in s.components)
                {
                    Component comp = new Component(c);
                    comp.SetSystem(this);
                    components.Add(comp);
                }
            }

            emcon_power[0] = s.emcon_power[0];
            emcon_power[1] = s.emcon_power[1];
            emcon_power[2] = s.emcon_power[2];
        }

        //public virtual ~System();

        //  public int operator ==(const System& s)  const { return this == &s; }

        public CATEGORY Type() { return type; }
        public int Subtype() { return subtype; }
        public string Name() { return name; }
        public string Abbreviation() { return abrv; }

        public void SetName(string n) { name = n; }
        public void SetAbbreviation(string a) { abrv = a; }
        public void SetDesign(SystemDesign d)
        {
            if (design != null)
            {
                design = null;
                components.Clear();
            }

            if (d != null)
            {
                design = d;

                foreach (var cd in design.components)
                {
                    Component comp = new Component(cd, this);
                    components.Add(comp);
                }
            }
        }


        public virtual int Value() { return (int)(max_value * availability * 100); }
        public int MaxValue() { return (int)(max_value * 100); }
        public STATUS Status() { return status; }
        public double Availability() { return availability * 100; }
        public double Safety() { return safety * 100; }
        public double Stability() { return stability * 100; }
        public virtual void CalcStatus()
        {
            if (components.Count > 0)
            {
                availability = 1.0f;
                safety = 1.0f;
                stability = 1.0f;

                foreach (var comp in components)
                {
                    if (comp.DamageEfficiency())
                        availability *= comp.Availability() / 100.0f;

                    if (comp.DamageSafety())
                        safety *= comp.Availability() / 100.0f;

                    if (comp.DamageStability())
                        stability *= comp.Availability() / 100.0f;

                    if (comp.IsJerried())
                    {
                        safety *= 0.95f;
                        stability *= 0.95f;
                    }
                }

                if (net_avail >= 0 && availability < net_avail)
                    availability = net_avail;
            }
        }

        public virtual void Repair()
        {
            if (status != STATUS.MAINT)
            {
                status = STATUS.MAINT;
                safety_overload = 0.0f;

                NetUtil.SendSysStatus(ship, this);
            }
        }


        public double NetAvail() { return net_avail; }
        public void SetNetAvail(double d) { net_avail = (float)d; }

        public List<Component> GetComponents() { return components; }

        public virtual void ApplyDamage(double damage)
        {
            if (!power_on)
                damage /= 10;

            if (components.Count > 0)
            {
                int index = (int)RandomHelper.Random(0, components.Count);

                if (damage > 50)
                {
                    damage /= 2;
                    components[index].ApplyDamage(damage);

                    index = (int)RandomHelper.Random(0, components.Count);
                }

                components[index].ApplyDamage(damage);

                if (safety < 0.5)
                    SetPowerLevel(50);

                else if (safety < 1.0)
                    SetPowerLevel(safety * 100);
            }
            else
            {
                availability -= (float)(damage / 100.0f);
                if (availability < 0.01) availability = 0.0f;
            }
        }

        public virtual void ExecFrame(double seconds)
        {
            if (seconds < 0.01)
                seconds = 0.01;

            STATUS s = STATUS.DESTROYED;

            if (availability > 0.99)
                s = STATUS.NOMINAL;
            else if (availability > crit_level)
                s = STATUS.DEGRADED;
            else
                s = STATUS.CRITICAL;

            bool repair = false;

            if (components.Count > 0)
            {
                foreach (var comp in components)
                {
                    if (comp.Status() > Component.STATUS.NOMINAL)
                    {
                        repair = true;
                        break;
                    }
                }
            }

            if (repair)
            {
                Repair();
            }

            else
            {
                if (status != s)
                {
                    status = s;
                    NetUtil.SendSysStatus(ship, this);
                }

                // collateral damage due to unsafe operation:
                if (power_on && power_level > safety)
                {
                    safety_overload += (float)seconds;

                    // inflict some damage now:
                    if (safety_overload > 60)
                    {
                        safety_overload -= (float)(RandomHelper.Random(0, 1000 * (power_level - safety)));
                        ApplyDamage(15);

                        NetUtil.SendSysStatus(ship, this);
                    }
                }

                else if (safety_overload > 0)
                {
                    safety_overload -= (float)seconds;
                }
            }
        }

        public virtual void ExecMaintFrame(double seconds)
        {
            if (components.Count > 0)
            {
                foreach (var comp in components)
                {
                    if (comp.Status() > Component.STATUS.NOMINAL)
                    {
                        comp.ExecMaintFrame(seconds);
                    }
                }
            }
        }

        public virtual void DoEMCON(int index)
        {
            int e = GetEMCONPower(index);

            if (power_level * 100 > e || emcon != index)
            {
                if (e == 0)
                {
                    PowerOff();
                }
                else
                {
                    if (emcon != index)
                        PowerOn();

                    SetPowerLevel(e);
                }
            }

            emcon = index;
        }

        // PHYSICAL LOCATION (for inflicting system damage):
        public virtual void Orient(Physical rep)
        {
#if TODO
            Matrix orientation = rep.Cam().Orientation();
            Point loc = rep.Location();
            mount_loc = (mount_rel * orientation) + loc;
#endif
            throw new NotImplementedException();
        }

        public virtual void Mount(Point loc, float radius, float hull_factor = 0.5f)
        {
            this.mount_rel = loc;
            this.radius = radius;
            this.hull_factor = hull_factor;
        }

        public virtual void Mount(ShipSystem system)
        {
            mount_rel = system.mount_rel;
            radius = system.radius;
            hull_factor = system.hull_factor;
        }


        public Point MountLocation() { return mount_loc; }
        public double Radius() { return radius; }
        public double HullProtection() { return hull_factor; }

        // POWER UTILIZATION:
        public bool IsPowerCritical() { return ((power_flags & POWER_FLAGS.POWER_CRITICAL) == POWER_FLAGS.POWER_CRITICAL) ? true : false; }
        public bool UsesWatts() { return ((power_flags & POWER_FLAGS.POWER_WATTS) == POWER_FLAGS.POWER_WATTS) ? true : false; }

        public virtual double GetRequest(double seconds)
        {
            if (!power_on || capacity == energy)
                return 0;

            else
                return power_level * sink_rate * seconds;
        }

        public virtual void Distribute(double delivered_energy, double seconds)
        {
            if (UsesWatts())
            {
                if (seconds < 0.01)
                    seconds = 0.01;

                // convert Joules to Watts:
                energy = (float)(delivered_energy / seconds);
            }
            else if (!starsGame.Paused())
            {
                energy += (float)delivered_energy;

                if (energy > capacity)
                    energy = capacity;

                else if (energy < 0)
                    energy = 0.0f;
            }
        }

        public int GetSourceIndex() { return source_index; }
        public void SetSourceIndex(int i) { source_index = i; }

        public virtual int Charge() { return (int)(100 * energy / capacity); }

        public bool IsPowerOn() { return power_on; }
        public virtual void PowerOn() { power_on = true; }
        public virtual void PowerOff() { power_on = false; }

        // percentage, but stored as 0-1
        public virtual double GetPowerLevel() { return power_level * 100; }
        public virtual void SetPowerLevel(double level)
        {
            if (level > 100)
                level = 100;
            else if (level < 0)
                level = 0;

            level /= 100;

            if (power_level != level)
            {
                // if the system is on emergency override power,
                // do not let the EMCON system use this method
                // to drop it back to normal power:
                if (power_level > 1 && level == 1)
                    return;

                power_level = (float)level;

                NetUtil.SendSysStatus(ship, this);
            }
        }

        public virtual void SetOverride(bool over)
        {
            bool changed = false;

            if (over && power_level != 1.2f)
            {
                power_level = 1.2f;
                changed = true;
            }

            else if (!over && power_level > 1)
            {
                power_level = 1.0f;
                changed = true;
            }

            if (changed)
                NetUtil.SendSysStatus(ship, this);
        }

        // for power drain damage:
        public virtual void DrainPower(double to_level)
        {
            energy = 0.0f;
        }

        public void SetCapacity(double c) { capacity = (float)c; }
        public double GetCapacity() { return capacity; }
        public double GetEnergy() { return energy; }
        public double GetSinkRate() { return sink_rate; }
        public void SetEMCONPower(int index, int power_level)
        {
            if (index >= 1 && index <= 3)
            {
                emcon_power[index - 1] = (byte)power_level;
            }
        }

        public int GetEMCONPower(int index)
        {
            if (index >= 1 && index <= 3)
            {
                return emcon_power[index - 1];
            }

            return 100;
        }


        public Explosion.ExplosionType GetExplosionType() { return explosion_type; }
        public void SetExplosionType(Explosion.ExplosionType t) { explosion_type = t; }

        public Ship GetShip() { return ship; }
        public void SetShip(Ship s) { ship = s; }
        public int GetID() { return id; }
        public void SetID(int n) { id = n; }


        // AI information:
        protected CATEGORY type;
        protected Ship ship;
        protected int id;
        protected int subtype;
        protected int max_value;

        // Displayable name:
        protected string name;
        protected string abrv;

        // System health status:
        protected STATUS status = STATUS.NOMINAL;
        protected float crit_level = 0.5f;
        protected float availability = 1.0f;
        protected float safety = 1.0f;
        protected float stability = 1.0f;
        protected float safety_overload;
        protected float net_avail = -1.0f;

        // Mounting:
        protected Point mount_loc;  // world space
        protected Point mount_rel;  // object space
        protected float radius;
        protected float hull_factor = 0.5f;

        // Power Sink:
        protected float energy;
        protected float capacity;
        protected float sink_rate;
        protected float power_level = 1.0f;
        protected int source_index;
        protected POWER_FLAGS power_flags;
        protected bool power_on = true;
        protected byte[] emcon_power = new byte[3];
        protected int emcon = 3;

        protected Explosion.ExplosionType explosion_type;

        // Subcomponents:
        protected SystemDesign design;
        protected List<Component> components = new List<Component>();

        protected readonly Starshatter starsGame;

    }
}
