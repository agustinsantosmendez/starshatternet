﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonSounds : MonoBehaviour
{

    public AudioSource AudioSource;
    public AudioClip ClickSound;
    public AudioClip EnterSound;

    public void OnPointerClick()
    {
        PlayClickSound();
    }
    public void OnPointerEnter()
    {
        PlayEnterSound();
    }


    private void PlayClickSound()
    {
        AudioSource.PlayOneShot(ClickSound);
    }

    private void PlayEnterSound()
    {
        AudioSource.PlayOneShot(EnterSound);
    }
}
