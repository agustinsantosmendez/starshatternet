﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CampaignSituationReport.h/CampaignSituationReport.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CampaignSituationReport generates the situation report
    portion of the briefing for a dynamically generated
    mission in a dynamic campaign.
*/
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.SimElements;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning CampaignSituationReport class is still in development and is not recommended for production.
    public class CampaignSituationReport
    {
        public CampaignSituationReport(Campaign c, Mission m)
        {
            campaign = c;
            mission = m;
        }
        //public virtual ~CampaignSituationReport();

        public virtual void GenerateSituationReport()
        {
            if (campaign == null || mission == null)
                return;

            sitrep = "";

            GlobalSituation();
            MissionSituation();

            mission.SetSituation(sitrep);
        }

        private static readonly string[] outlooks = { "good", "fluid", "poor", "bleak" };

        protected virtual void GlobalSituation()
        {
            if (campaign.GetTime() < 40 * 3600)
                sitrep = campaign.Name() + " is still in its early stages and the situation is ";
            else
                sitrep = "The overall outlook for " + campaign.Name() + " is ";

            int score = campaign.GetPlayerTeamScore();

            if (score > 1000)
                sitrep += outlooks[0];
            else if (score > -1000)
                sitrep += outlooks[1];
            else if (score > -2000)
                sitrep += outlooks[2];
            else
                sitrep += outlooks[3];

            sitrep += ".  ";

            string strat_dir = "";

            CombatGroup pg = campaign.GetPlayerGroup();

            if (pg != null)
                strat_dir = pg.GetStrategicDirection();

            if (strat_dir.Length > 0)
                sitrep += strat_dir;
            else
                sitrep += "Establishing and maintaining military control of the " +
                mission.GetStarSystem().Name() + " System remains a key priority.";
        }
        protected virtual void MissionSituation()
        {
            if (mission != null)
            {
                MissionElement player = mission.GetPlayer();
                MissionElement target = mission.GetTarget();
                MissionElement ward = mission.GetWard();
                MissionElement escort = FindEscort(player);
                string threat = GetThreatInfo();
                string sector = mission.GetRegion();

                sector += " sector.";

                switch (mission.Type())
                {
                    case Mission.TYPE.PATROL:
                    case Mission.TYPE.AIR_PATROL:
                        sitrep += "\n\nThis mission is a routine patrol of the ";
                        sitrep += sector;
                        break;

                    case Mission.TYPE.SWEEP:
                    case Mission.TYPE.AIR_SWEEP:
                        sitrep += "\n\nFor this mission, you will be performing a fighter sweep of the ";
                        sitrep += sector;
                        break;

                    case Mission.TYPE.INTERCEPT:
                    case Mission.TYPE.AIR_INTERCEPT:
                        sitrep += "\n\nWe have detected hostile elements inbound.  ";
                        sitrep += "Your mission is to intercept them before they are able to engage their targets.";
                        break;


                    case Mission.TYPE.STRIKE:
                        sitrep += "\n\nThe goal of this mission is to perform a strike on preplanned targets in the ";
                        sitrep += sector;

                        if (target != null)
                        {
                            sitrep += "  Your package has been assigned to strike the ";

                            if (target.GetCombatGroup() != null)
                                sitrep += target.GetCombatGroup().GetDescription();
                            else
                                sitrep += target.Name();

                            sitrep += ".";
                        }
                        break;

                    case Mission.TYPE.ASSAULT:
                        sitrep += "\n\nThis mission is to assault preplanned targets in the ";
                        sitrep += sector;

                        if (target != null)
                        {
                            sitrep += "  Your package has been assigned to strike the ";

                            if (target.GetCombatGroup() != null)
                                sitrep += target.GetCombatGroup().GetDescription();
                            else
                                sitrep += target.Name();

                            sitrep += ".";
                        }
                        break;

                    case Mission.TYPE.DEFEND:
                        if (ward != null)
                        {
                            sitrep += "\n\nFor this mission, you will need to defend ";
                            sitrep += ward.Name();
                            sitrep += " in the ";
                            sitrep += sector;
                        }
                        else
                        {
                            sitrep += "\n\nThis is a defensive patrol mission in the ";
                            sitrep += sector;
                        }
                        break;

                    case Mission.TYPE.ESCORT:
                        if (ward != null)
                        {
                            sitrep += "\n\nFor this mission, you will need to escort the ";
                            sitrep += ward.Name();
                            sitrep += " in the ";
                            sitrep += sector;
                        }
                        else
                        {
                            sitrep += "\n\nThis is an escort mission in the ";
                            sitrep += sector;
                        }
                        break;

                    case Mission.TYPE.ESCORT_FREIGHT:
                        if (ward != null)
                        {
                            sitrep += "\n\nFor this mission, you will need to escort the freighter ";
                            sitrep += ward.Name();
                            sitrep += ".";
                        }
                        else
                        {
                            sitrep += "\n\nThis is a freight escort mission in the ";
                            sitrep += sector;
                        }
                        break;

                    case Mission.TYPE.ESCORT_SHUTTLE:
                        if (ward != null)
                        {
                            sitrep += "\n\nFor this mission, you will need to escort the shuttle ";
                            sitrep += ward.Name();
                            sitrep += ".";
                        }
                        else
                        {
                            sitrep += "\n\nThis is a shuttle escort mission in the ";
                            sitrep += sector;
                        }
                        break;

                    case Mission.TYPE.ESCORT_STRIKE:
                        if (ward != null)
                        {
                            sitrep += "\n\nFor this mission, you will need to protect the ";
                            sitrep += ward.Name();
                            sitrep += " strike package from hostile interceptors.";
                        }
                        else
                        {
                            sitrep += "\n\nFor this mission, you will be responsible for strike escort duty.";
                        }
                        break;

                    case Mission.TYPE.INTEL:
                    case Mission.TYPE.SCOUT:
                    case Mission.TYPE.RECON:
                        sitrep += "\n\nThis is an intelligence gathering mission in the ";
                        sitrep += sector;
                        break;

                    case Mission.TYPE.BLOCKADE:
                        sitrep += "\n\nThis mission is part of the blockade operation in the ";
                        sitrep += sector;
                        break;

                    case Mission.TYPE.FLEET:
                        sitrep += "\n\nThis mission is a routine fleet patrol of the ";
                        sitrep += sector;
                        break;

                    case Mission.TYPE.BOMBARDMENT:
                        sitrep += "\n\nOur goal for this mission is to engage and destroy preplanned targets in the ";
                        sitrep += sector;
                        break;

                    case Mission.TYPE.FLIGHT_OPS:
                        sitrep += "\n\nFor this mission, the ";
                        sitrep += player.Name();
                        sitrep += " will be conducting combat flight operations in the ";
                        sitrep += sector;
                        break;

                    case Mission.TYPE.TRAINING:
                        sitrep += "\n\nThis will be a training mission.";
                        break;

                    case Mission.TYPE.TRANSPORT:
                    case Mission.TYPE.CARGO:
                    case Mission.TYPE.OTHER:
                    default:
                        break;
                }

                if (threat.Length > 0)
                {
                    sitrep += "  ";
                    sitrep += threat;
                    sitrep += "\n\n";
                }
            }
            else
            {
                sitrep += "\n\n";
            }

            string rank = "";
            string name = "";

            Player p = Player.GetCurrentPlayer();

            if (p != null)
            {
                if (p.Rank() > 6)
                    rank = ", Admiral";
                else
                    rank = ", " + Player.RankName(p.Rank());

                name = ", " + p.Name();
            }

            sitrep += "You have a mission to perform.  ";

            switch (RandomHelper.RandomIndex())
            {
                case 0: sitrep += "You'd better go get to it!"; break;
                case 1: sitrep += "And let's be careful out there!"; break;
                case 2: sitrep += "Good luck, sir!"; break;
                case 3: sitrep += "Let's keep up the good work out there."; break;
                case 4: sitrep += "Don't lose your focus."; break;
                case 5: sitrep += "Good luck out there."; break;
                case 6: sitrep += "What are you waiting for, cocktail hour?"; break;
                case 7: sitrep += "Godspeed" + rank + "!"; break;
                case 8: sitrep += "Good luck" + rank + "!"; break;
                case 9: sitrep += "Good luck" + name + "!"; break;
                case 10:
                    sitrep += "If everything is clear, get your team ready and get underway.";
                    break;
                case 11: sitrep += "Go get to it" + rank + "!"; break;
                case 12: sitrep += "The clock is ticking, so let's move it!"; break;
                case 13: sitrep += "Stay sharp out there!"; break;
                case 14: sitrep += "Go get 'em" + rank + "!"; break;
                case 15: sitrep += "Now get out of here and get to work!"; break;
            }
        }
        protected virtual MissionElement FindEscort(MissionElement player)
        {
            MissionElement escort = null;

            if (mission == null || player == null) return escort;

            foreach (MissionElement iter in mission.GetElements())
            {
                MissionElement elem = iter;
                // TODO this code does nothing  ????
            }

            return escort;
        }


        protected virtual string GetThreatInfo()
        {
            string threat_info;

            int enemy_fighters = 0;
            int enemy_starships = 0;
            int enemy_sites = 0;

            if (mission != null && mission.GetPlayer() != null)
            {
                MissionElement player = mission.GetPlayer();
                string rgn0 = player.Region();
                string rgn1 = null;
                int iff = player.GetIFF();

                foreach (Instruction nav in player.NavList())
                {
                    if (rgn0 != nav.RegionName())
                        rgn1 = nav.RegionName();
                }

                foreach (MissionElement elem in mission.GetElements())
                {
                    if (elem.GetIFF() > 0 && elem.GetIFF() != iff && elem.IntelLevel() > Intel.INTEL_TYPE.SECRET)
                    {
                        if (elem.IsGroundUnit())
                        {
                            if (elem.GetDesign() == null ||
                                    elem.GetDesign().type != CLASSIFICATION.SAM)
                                continue;

                            if (elem.Region() != rgn0 &&
                                    elem.Region() != rgn1)
                                continue;
                        }

                        Mission.TYPE mission_role = elem.MissionRole();

                        if (mission_role == Mission.TYPE.STRIKE ||
                                mission_role == Mission.TYPE.INTEL ||
                                mission_role == Mission.TYPE.CARGO ||
                                mission_role == Mission.TYPE.TRANSPORT)
                            continue;

                        if (elem.GetDesign().type >= CLASSIFICATION.MINE && elem.GetDesign().type <= CLASSIFICATION.DEFSAT)
                            enemy_sites += elem.Count();

                        else if (elem.IsDropship())
                            enemy_fighters += elem.Count();

                        else if (elem.IsStarship())
                            enemy_starships += elem.Count();

                        else if (elem.IsGroundUnit())
                            enemy_sites += elem.Count();
                    }
                }
            }

            if (enemy_starships > 0)
            {
                threat_info = "We have reports of several enemy starships in the vicinity.";

                if (enemy_fighters > 0)
                {
                    threat_info += "  Also be advised that enemy fighters may be operating nearby.";
                }

                else if (enemy_sites > 0)
                {
                    threat_info += "  And be on the lookout for mines and defense satellites.";
                }
            }

            else if (enemy_fighters > 0)
            {
                threat_info = "We have reports of several enemy fighters in your operating area.";
            }

            else if (enemy_sites > 0)
            {
                if (mission.Type() >= Mission.TYPE.AIR_PATROL && mission.Type() <= Mission.TYPE.STRIKE)
                    threat_info = "Remember to check air-to-ground sensors for SAM and AAA sites.";
                else
                    threat_info = "Be on the lookout for mines and defense satellites.";
            }
            else
            {
                threat_info = "We have no reliable information on threats in your operating area.";
            }

            return threat_info;
        }

        protected Campaign campaign;
        protected Mission mission;
        protected string sitrep;

    }
}
