﻿using System.Collections;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using UnityEngine;
using UnityEngine.Assertions;

public class Starfield : MonoBehaviour
{
    public int MaxStars = 100;
    public float StarSize = 0.1f;
    public float StarSizeRange = 0.5f;
    public float Radius = 900f;
    public bool Colorize = false;

    float xOffset;
    float yOffset;

    ParticleSystem Particles;
    ParticleSystem.Particle[] Stars;


    void Awake()
    {
        Stars = new ParticleSystem.Particle[MaxStars];
        Particles = GetComponent<ParticleSystem>();

        Assert.IsNotNull(Particles, "Particle system missing from object!");

        for (int i = 0; i < MaxStars; i++)
        {
            float randSize = Random.Range(StarSizeRange, StarSizeRange + 1f);          // Randomize star size within parameters

            DigitalRune.Mathematics.Algebra.Vector3F pos = RandomHelper.RandomVector(Radius);
            Stars[i].position = new Vector3(pos.X, pos.Y, pos.Z) + transform.position;
            //Stars[i].position = GetRandomInSphere(Radius) + transform.position;
            Stars[i].startSize = StarSize * randSize * Radius / 10;
            Stars[i].startColor = Colorize ? GetRandomColor(0.7f, 0.8f) : Color.white; // If coloration is desired, get a random color
        }
        Particles.SetParticles(Stars, Stars.Length);                                   // Write data to the particle system
        ParticleSystemRenderer renderer = GetComponent<ParticleSystemRenderer>();
        Debug.Log(renderer.material.name);
    }


    // GetRandomInSphere
    //----------------------------------------------------------
    // Get a random point in a sphere
    //
    Vector3 GetRandomInSphere(float radius)
    {
        Vector3 v = Random.onUnitSphere;

        if (radius > 0)
            v *= (float)radius;
        else
            v *= (float)Random.Range(radius / 3, radius);

        return v;
    }

    Color GetRandomColor(float min, float max)
    {
        Color color = new Color(Random.Range(min, max), Random.Range(min, max), Random.Range(min, max), 1f);

        return color;
    }
}
