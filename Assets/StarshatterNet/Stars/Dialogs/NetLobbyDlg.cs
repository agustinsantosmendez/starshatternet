﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetLobbyDlg.h/NetLobbyDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using UnityEngine;
using DWORD = System.Int32;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class NetLobbyDlg : FormWindow
    {
        public NetLobbyDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
         base(s, 0, 0, s.Width(), s.Height(), c, p, "NetLobbyDlg")
        {
            manager = mgr;
            net_lobby = null;

            selected_campaign = 0;
            selected_mission = 0;
            last_chat = 0;
            host_mode = false;

            Init(def);
        }
        public override void RegisterControls()
        {
            lst_campaigns = (ComboBox)FindControl(200);
            REGISTER_CLIENT(ETD.EID_SELECT, lst_campaigns, OnCampaignSelect);

            lst_missions = (ListBox)FindControl(201);
            REGISTER_CLIENT(ETD.EID_SELECT, lst_missions, OnMissionSelect);

            txt_desc = FindControl(202);
            lst_players = (ListBox)FindControl(210);
            lst_chat = (ListBox)FindControl(211);
            edt_chat = (EditBox)FindControl(212);

            if (edt_chat != null)
                edt_chat.SetText("");

            apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, apply, OnApply);

            cancel = (Button)FindControl(2);
            REGISTER_CLIENT(ETD.EID_CLICK, cancel, OnCancel);
        }
        public override void Show()
        {
            if (!IsShown())
            {
                // clear server data:
                if (lst_chat != null) lst_chat.ClearItems();
                if (lst_campaigns != null) lst_campaigns.ClearItems();
                if (lst_missions != null) lst_missions.ClearItems();
                if (txt_desc != null) txt_desc.SetText("");
                if (apply != null) apply.SetEnabled(false);

                if (lst_missions != null)
                {
                    lst_missions.SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_FILLED_BOX);
                    lst_missions.SetLeading(2);
                }

                selected_campaign = 0;
                selected_mission = 0;
                last_chat = 0;

                base.Show();

                net_lobby = NetLobby.GetInstance();

                if (net_lobby == null)
                {
                    Starshatter stars = Starshatter.GetInstance();
                    if (stars != null)
                        stars.StartLobby();

                    net_lobby = NetLobby.GetInstance();
                }

                if (net_lobby != null)
                {
                    if (net_lobby.IsServer())
                    {
                        host_mode = true;

                        NetUser user = net_lobby.GetLocalUser();

                        if (user == null)
                        {
                            Player player = Player.GetCurrentPlayer();
                            if (player != null)
                            {
                                user = new NetUser(player);
                                user.SetHost(true);
                            }
                            else
                            {
                                ErrLogger.PrintLine("NetLobbyDlg.Show() Host mode - no current player?");
                            }
                        }

                        net_lobby.SetLocalUser(user);
                    }

                    SelectMission();
                }
            }
        }
        public virtual void ExecFrame()
        {
            ExecLobbyFrame();

            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                if (edt_chat != null && edt_chat.GetText().Length > 0)
                {
                    SendChat(edt_chat.GetText());
                    edt_chat.SetText("");
                }
            }

            GetPlayers();
            GetChat();

            if (lst_campaigns != null)
            {
                if (lst_campaigns.NumItems() < 1)
                    GetMissions();
                else
                    GetSelectedMission();
            }
        }

        // Operations:
        public virtual void OnCampaignSelect(ActiveWindow obj, AWEvent evnt)
        {
            if (net_lobby != null)
            {
                List<NetCampaignInfo> campaigns = net_lobby.GetCampaigns();

                if (lst_campaigns != null && lst_missions != null && campaigns.Count != 0)
                {
                    int index = lst_campaigns.GetSelectedIndex();

                    if (index >= 0 && index < campaigns.Count)
                    {
                        NetCampaignInfo c = campaigns[index];

                        lst_missions.ClearItems();
                        txt_desc.SetText("");

                        foreach (MissionInfo m in c.missions)
                        {
                            lst_missions.AddItem(m.name);
                        }
                    }
                }
            }
        }
        public virtual void OnMissionSelect(ActiveWindow obj, AWEvent evnt)
        {
            if (net_lobby != null)
            {
                List<NetCampaignInfo> campaigns = net_lobby.GetCampaigns();

                if (lst_campaigns != null && lst_missions != null && txt_desc != null && campaigns.Count != 0)
                {
                    txt_desc.SetText("");

                    if (host_mode && apply != null)
                        apply.SetEnabled(false);

                    int c_index = lst_campaigns.GetSelectedIndex();
                    int m_index = lst_missions.GetSelection();

                    if (c_index >= 0 && c_index < campaigns.Count)
                    {
                        NetCampaignInfo c = campaigns[c_index];

                        if (m_index >= 0 && m_index < c.missions.Count)
                        {
                            MissionInfo m = c.missions[m_index];
                            txt_desc.SetText(m.description);

                            if (host_mode && apply != null)
                                apply.SetEnabled(true);
                        }
                    }
                }
            }
        }
        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            if (host_mode)
                SelectMission();

            manager.ShowNetUnitDlg();

            NetUnitDlg unit_dlg = manager.GetNetUnitDlg();
            if (unit_dlg != null)
                unit_dlg.SetHostMode(host_mode);
        }

        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            if (net_lobby != null)
            {
                net_lobby.SelectMission(0);
                net_lobby = null;

                Starshatter stars = Starshatter.GetInstance();
                if (stars != null)
                    stars.StopLobby();
            }

            manager.ShowNetClientDlg();
        }

        public virtual void ExecLobbyFrame()
        {
            if (net_lobby != null && net_lobby.GetLastError() != 0)
            {
                if (net_lobby.IsClient())
                {
                    Starshatter stars = Starshatter.GetInstance();
                    if (stars != null)
                        stars.StopLobby();

                    net_lobby = null;
                    manager.ShowNetClientDlg();
                }
            }
        }



        protected virtual void GetPlayers()
        {
            if (lst_players == null) return;

            if (net_lobby != null)
            {
                lst_players.ClearItems();

                NetUser nu = net_lobby.GetLocalUser();
                if (nu != null)
                {
                    string name = Player.RankAbrv(nu.Rank());
                    name += " ";
                    name += nu.Name();

                    int count = lst_players.AddItem(nu.IsHost() ? "*" : " ");
                    lst_players.SetItemText(count - 1, 1, name);
                    host_mode = true;
                }

                foreach (NetUser u in net_lobby.GetUsers())
                {
                    int count = lst_players.AddItem(u.IsHost() ? "*" : " ");

                    string name = Player.RankAbrv(u.Rank());
                    name += " ";
                    name += u.Name();

                    lst_players.SetItemText(count - 1, 1, name);

                    if (Player.GetCurrentPlayer().Name() == u.Name())
                        host_mode = u.IsHost();
                }
            }
        }
        protected virtual void GetChat()
        {
            if (lst_chat == null) return;

            if (net_lobby != null)
            {
                int last_item = lst_chat.NumItems() - 1;
                int count = 0;
                bool added = false;

                foreach (NetChatEntry c in net_lobby.GetChat())
                {
                    if (count++ > last_item)
                    {
                        int n = lst_chat.AddItem(c.GetUser());
                        lst_chat.SetItemText(n - 1, 1, c.GetMessage());
                        added = true;
                    }
                }

                if (added)
                    lst_chat.EnsureVisible(lst_chat.NumItems() + 1);
            }
        }
        protected virtual void GetMissions()
        {
            if (lst_campaigns == null || lst_missions == null)
                return;

            if (net_lobby != null)
            {
                lst_campaigns.ClearItems();

                List<NetCampaignInfo> campaigns = net_lobby.GetCampaigns();

                if (campaigns.Count != 0)
                {
                    foreach (NetCampaignInfo ci in campaigns)
                    {
                        lst_campaigns.AddItem(ci.name);
                    }

                    lst_campaigns.SetSelection(0);
                    NetCampaignInfo c = campaigns[0];

                    lst_missions.ClearItems();

                    foreach (MissionInfo m in c.missions)
                    {
                        lst_missions.AddItem(m.name);
                    }
                }
            }
        }
        protected virtual void GetSelectedMission()
        {
            if (lst_campaigns == null || lst_missions == null) return;

            if (net_lobby != null)
            {
                if (net_lobby.GetSelectedMissionID() != 0)
                {
                    int id = (int)net_lobby.GetSelectedMissionID();

                    selected_campaign = id >> NET_CAMPAIGN_SHIFT;
                    selected_mission = id & NET_MISSION_MASK;

                    List<NetCampaignInfo> campaigns = net_lobby.GetCampaigns();

                    for (int i = 0; i < campaigns.Count; i++)
                    {
                        NetCampaignInfo c = campaigns[i];

                        if (c.id == selected_campaign)
                        {
                            lst_campaigns.SetSelection(i);
                            OnCampaignSelect(null, null);

                            for (int j = 0; j < c.missions.Count; j++)
                            {
                                MissionInfo m = c.missions[j];

                                if (m.id == selected_mission)
                                {
                                    lst_missions.SetSelected(j);
                                    OnMissionSelect(null, null);
                                }
                            }
                        }
                    }
                }
                else if (selected_campaign != 0)
                {
                    selected_campaign = 0;
                    selected_mission = 0;
                }

                lst_campaigns.SetEnabled(selected_campaign == 0);
                lst_missions.SetEnabled(selected_mission == 0);

                if (!host_mode)
                    apply.SetEnabled(selected_mission != 0);
            }
        }

        protected virtual void SendChat(string msg)
        {
            if (string.IsNullOrEmpty(msg)) return;

            Player player = Player.GetCurrentPlayer();

            if (msg[0] >= '0' && msg[0] <= '9')
            {
                if (player != null)
                {
                    string macro = player.ChatMacro(msg[0] - '0');

                    if (!string.IsNullOrEmpty(macro))
                        msg = macro;
                }
            }

            if (net_lobby != null)
                net_lobby.AddChat(net_lobby.GetLocalUser(), msg);
        }
        protected virtual void SelectMission()
        {
            if (lst_campaigns == null || lst_missions == null) return;

            bool selected = false;

            if (net_lobby != null)
            {
                int c_index = lst_campaigns.GetSelectedIndex();
                int m_index = lst_missions.GetSelection();

                List<NetCampaignInfo> campaigns = net_lobby.GetCampaigns();

                if (c_index >= 0 && c_index < campaigns.Count && m_index >= 0)
                {
                    NetCampaignInfo c = campaigns[c_index];

                    if (m_index < c.missions.Count)
                    {
                        MissionInfo m = c.missions[m_index];

                        DWORD id = ((DWORD)c.id << NET_CAMPAIGN_SHIFT) +
                        (m.id & NET_MISSION_MASK);

                        net_lobby.SelectMission((uint)id);
                        selected = true;
                    }
                }

                if (!selected)
                    net_lobby.SelectMission(0);
            }
        }

        private const DWORD NET_CAMPAIGN_SHIFT = 12;
        private const DWORD NET_MISSION_MASK = 0xfff;
        private const DWORD NET_DISCONNECT_TIME = 30;

        protected MenuScreen manager;

        protected ComboBox lst_campaigns;
        protected ListBox lst_missions;
        protected ActiveWindow txt_desc;
        protected ListBox lst_players;
        protected ListBox lst_chat;
        protected EditBox edt_chat;

        protected Button apply;
        protected Button cancel;

        protected NetLobby net_lobby;

        protected int selected_campaign;
        protected int selected_mission;
        protected int last_chat;
        protected bool host_mode;
    }
}