﻿using System;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Graphics.General;
using UnityEngine;
using StarshatterNet.Stars;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using Screen = StarshatterNet.Gen.Graphics.Screen;
using Rect = StarshatterNet.Stars.Rect;
using StarshatterNet.Stars.Views;

public class MapViewTesting : MonoBehaviour
{
    private Window win;
    private View view;
    private MenuView menuView;
    private FontItem font;

    void Awake()
    {
        // Display
        Video video = Video.GetVideo();
        // Canvas
        Screen screen = new Screen(video);
        // Rect
        win = new Window(screen, 50, 50, 500, 400);
        rect = win.GetRect();
        view = new View(win, "MapViewExample");

        //menuView = new MenuView(this.gameObject, "MenuView");

        font = FontMgr.GetDefault();

        DrawGrid();
        DrawRect();
        FillRect();
        DrawBitmap();
        DrawEllipse();
        DrawText();
    }

    // Start is called before the first frame update
    void Start()
    {
        //stars.Run();
        //stars.Activate(true);
    }

    // Update is called once per frame
    void Update()
    {

        //if (Input.GetKeyDown(KeyCode.I))
        //    stars.ScreenCapture();
    }

    public virtual void DrawGrid()
    {
        int grid_step = rect.w / 8;
        int cx = rect.w / 2;
        int cy = rect.h / 2;

        Color c = new Color32(200, 255, 200, 255);

        view.Window.DrawLine(0, cy, rect.w, cy, c, Video.BLEND_TYPE.BLEND_ADDITIVE);
        view.Window.DrawLine(cx, 0, cx, rect.h, c, Video.BLEND_TYPE.BLEND_ADDITIVE);

        for (int i = 1; i < 4; i++)
        {
            view.Window.DrawLine(0, cy + (i * grid_step), rect.w, cy + (i * grid_step), c, Video.BLEND_TYPE.BLEND_ADDITIVE);
            view.Window.DrawLine(0, cy - (i * grid_step), rect.w, cy - (i * grid_step), c, Video.BLEND_TYPE.BLEND_ADDITIVE);
            view.Window.DrawLine(cx + (i * grid_step), 0, cx + (i * grid_step), rect.h, c, Video.BLEND_TYPE.BLEND_ADDITIVE);
            view.Window.DrawLine(cx - (i * grid_step), 0, cx - (i * grid_step), rect.h, c, Video.BLEND_TYPE.BLEND_ADDITIVE);
        }
    }

    public virtual void DrawRect()
    {
        int cx = rect.w / 2;
        int cy = rect.h / 2;

        Color c = new Color32(255, 180, 180, 255);
        view.Window.DrawRect(0, 0, cx, cy, c);

        Rect rect2 = new Rect(0, 0, rect.w, rect.h);
        c = new Color32(180, 180, 180, 255);
        view.Window.DrawRect(rect2, c);
    }

    public virtual void FillRect()
    {
        int x = rect.w / 3;
        int y = rect.h / 3;

        Color c = new Color32(255, 255, 180, 255);
        view.Window.FillRect(x, y, x + 100, y + 150, c);
    }
    public virtual void DrawBitmap()
    {
        int x = rect.w / 2;
        int y = rect.h / 3;
        Bitmap bitmap = new Bitmap("Campaigns/01/image");
        view.Window.DrawBitmap(x, y, x + bitmap.Width(), y + bitmap.Height(), bitmap);
        Color c = new Color32(90, 50, 255, 255);
        view.Window.FadeBitmap(x, 2 * y, x + bitmap.Width(), 2 * y + bitmap.Height(), bitmap, c);

    }
    public virtual void DrawEllipse()
    {
        int x = rect.w / 3;
        int y = 0;
        Color c = new Color32(90, 128, 0, 255);
        view.Window.DrawEllipse(x, y, x + 150, y + 80, c);

        c = new Color32(255, 32, 32, 255);

        view.Window.FillEllipse(2 * x, y, 2 * x + 150, y + 80, c);
    }
    public virtual void DrawText()
    {
        int x = 0;
        int y = 4 * rect.h / 5;
        Color c = new Color32(90, 255, 255, 255);
        font.color = c;
        view.Window.SetFont(font);
        view.Window.Print(x, y, "Some number {0}", 12345);
    }
    protected Rect rect;

}
