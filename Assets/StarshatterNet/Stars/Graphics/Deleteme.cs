﻿/*  Starshatter OpenSource Distribution
   Copyright (c) 1997-2004, Destroyer Studios LLC.
   All Rights Reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name "Destroyer Studios" nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

   SUBSYSTEM:          nGenEx
   ORIGINAL FILE:      N/A
   ORIGINAL AUTHOR:    John DiCamillo
   .NET AUTHOR:        Agustin Santos


   OVERVIEW
   ========
   Abstract Physical Object
*/
using System;
using System.IO;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using UnityEngine;
using ColorValue = UnityEngine.Color;
using DWORD = System.UInt32;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using Sprite = StarshatterNet.Gen.Graphics.SpriteGraphic;

namespace StarshatterNet.Stars
{
    public class Projector
    {
        internal void Transform(Point tloc)
        {
            throw new NotImplementedException();
        }

        internal void Project(Point tloc, bool v)
        {
            throw new NotImplementedException();
        }

        internal void Project(Point tloc)
        {
            throw new NotImplementedException();
        }

        internal double XAngle()   { return xangle; }
        internal double YAngle()   { return yangle; }
        double xangle, yangle;

        internal float ProjectRadius(Point p, float r)
        {
            throw new NotImplementedException();
        }
    }

    public class Particles { }


    public class Material
    {
        public enum BLEND_TYPE { MTL_SOLID = 1, MTL_TRANSLUCENT = 2, MTL_ADDITIVE = 4 };

        public Material()
        {
            throw new NotImplementedException();
        }
        //public ~Material();

        // int operator ==(const Material& m) const;

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public string name;
        public string shader;

        public ColorValue Ka;         // ambient color
        public ColorValue Kd;         // diffuse color
        public ColorValue Ks;         // specular color
        public ColorValue Ke;         // emissive color
        public float power;           // highlight sharpness (big=shiny)
        public float brilliance;      // diffuse power function
        public float bump;            // bump level (0=none)
        public BLEND_TYPE blend;      // alpha blend type
        public bool shadow;           // casts shadow
        public bool luminous;         // verts have their own lighting

        public Bitmap tex_diffuse;
        public Bitmap tex_specular;
        public Bitmap tex_bumpmap;
        public Bitmap tex_emissive;
        public Bitmap tex_alternate;
        public Bitmap tex_detail;

        public bool IsSolid() { return blend == BLEND_TYPE.MTL_SOLID; }
        public bool IsTranslucent() { return blend == BLEND_TYPE.MTL_TRANSLUCENT; }
        public bool IsGlowing() { return blend == BLEND_TYPE.MTL_ADDITIVE; }
        public string GetShader(int n)
        {
            throw new NotImplementedException();
        }

        //
        // Support for Magic GUI
        //

        public Color ambient_color;
        public Color diffuse_color;
        public Color specular_color;
        public Color emissive_color;

        public float ambient_value;
        public float diffuse_value;
        public float specular_value;
        public float emissive_value;

        public Bitmap thumbnail;        // preview image

        public void CreateThumbnail(int size = 128)
        {
            throw new NotImplementedException();
        }
        public DWORD GetThumbColor(int i, int j, int size)
        {
            throw new NotImplementedException();
        }
    }



    public class DriveSprite : Sprite
    {
        private Bitmap flare_bmp;
        private Bitmap glow_bmp;
        private Bitmap bitmap;

        public DriveSprite(Bitmap bitmap)
        {
            this.bitmap = bitmap;
        }

        public DriveSprite(Bitmap flare_bmp, Bitmap glow_bmp)
        {
            this.flare_bmp = flare_bmp;
            this.glow_bmp = glow_bmp;
        }

        internal void SetFront(Point vector3F)
        {
            throw new NotImplementedException();
        }

        internal void SetShade(double v)
        {
            throw new NotImplementedException();
        }
        internal void Scale(double v)
        {
            throw new NotImplementedException();
        }
    }
    public class Trail : Graphic { }

    public class AviFile { }
}
