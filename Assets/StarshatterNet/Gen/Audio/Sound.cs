﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Sound.h/Sound.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Abstract Sound Object
*/
using System;
using UnityEngine;
using StarshatterNet.Stars.Graphics.General;
using Vec3 = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Audio
{
    public class Sound
    {
        [Flags]
        public enum FlagEnum
        {
            AMBIENT = 0x0000,
            LOCALIZED = 0x0001,
            LOC_3D = 0x0002,
            MEMORY = 0x0000,
            STREAMED = 0x0004,
            ONCE = 0x0000,
            LOOP = 0x0008,
            FREE = 0x0000,
            LOCKED = 0x0010,
            DOPPLER = 0x0020,
            INTERFACE = 0x0040,
            OGGVORBIS = 0x4000,
            RESOURCE = 0x8000   // not playable, only used to store data
        }

        public enum StatusEnum
        {
            E_NOINTERFACE,
            UNINITIALIZED,
            INITIALIZING,
            READY,
            PLAYING,
            DONE
        }

        public static Sound CreateStream(string filename, FlagEnum flags = FlagEnum.OGGVORBIS | FlagEnum.RESOURCE)
        {
            Sound sound = null;

            if (string.IsNullOrEmpty(filename))
                return sound;

            sound = Create(flags, 0);
            if (sound != null)
            {
                sound.SetFilename(filename);
                var fname = System.IO.Path.ChangeExtension(filename, null);
                sound.AudioClip = Resources.Load(fname) as AudioClip; ;
            }
            return sound;
        }
        public static Sound Create(FlagEnum flags, int format)
        {
            var sound = new Sound();
            return sound;
        }

        public static void SetListener(CameraNode cameraNode, Vec3 vel)
        {
            throw new NotImplementedException();
        }

        public Sound()
        {
            status = StatusEnum.UNINITIALIZED; volume = 0; flags = 0; looped = 0;
            velocity = new Vec3(0, 0, 0); location = new Vec3(0, 0, 0); sound_check = null;
            filename = "Sound()";
        }

        public void Release()
        {
            throw new NotImplementedException();
        }

        public Sound Duplicate()
        {
            throw new NotImplementedException();
        }



        // transport operations:
        public virtual StatusEnum Play() // => Playing
        {
            if (audioClip != null)
                AudioSource.PlayOneShot(audioClip);

            return StatusEnum.E_NOINTERFACE;
        }
        public virtual StatusEnum Rewind() { return StatusEnum.E_NOINTERFACE; }  // => Ready
        public virtual StatusEnum Pause() { return StatusEnum.E_NOINTERFACE; }  // => Ready
        public virtual StatusEnum Stop() { return StatusEnum.E_NOINTERFACE; }  // => Done

        // accessors / mutators
        public bool IsReady() { return status == StatusEnum.READY; }
        public bool IsPlaying() { return status == StatusEnum.PLAYING; }
        public bool IsDone() { return status == StatusEnum.DONE; }
        public int LoopCount() { return looped; }

        public virtual FlagEnum GetFlags() { return flags; }
        public virtual void SetFlags(FlagEnum f) { flags = f; }
        public virtual StatusEnum GetStatus() { return status; }

        public virtual long GetVolume() { return volume; }

        public void Update()
        {
        }

        public virtual void SetVolume(long v) { volume = v; }
        public virtual long GetPan() { return 0; }
        public virtual void SetPan(long p) { }

        // These should be relative to the listener:
        // (only used for localized sounds)
        public virtual Vec3 GetLocation() { return location; }
        public virtual void SetLocation(Vec3 l) { location = l; }
        public virtual Vec3 GetVelocity() { return velocity; }
        public virtual void SetVelocity(Vec3 v) { velocity = v; }

        public virtual float GetMinDistance() { return 0; }
        public virtual void SetMinDistance(float f) { }
        public virtual float GetMaxDistance() { return 0; }
        public virtual void SetMaxDistance(float f) { }

        // TODO public virtual void SetSoundCheck(SoundCheck s) { sound_check = s; }
        //TODO public virtual void AddToSoundCard();

        public string GetFilename() { return filename; }
        public void SetFilename(string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                int n = s.Length;

                if (n >= 60)
                {
                    filename = "..." + s.Substring(n - 59);
                }
                else
                    filename = s;
            }
        }
        public AudioClip AudioClip { get => audioClip; set => audioClip = value; }
        public static AudioSource AudioSource { get => audioSource; set => audioSource = value; }

        // (only for streamed sounds)
        public virtual double GetTotalTime() { return 0; }
        public virtual double GetTimeRemaining() { return 0; }
        public virtual double GetTimeElapsed() { return 0; }

        protected FlagEnum flags;
        protected StatusEnum status;
        protected long volume;  // centibels, (0 .. -10000)
        protected int looped;
        protected Vec3 location;
        protected Vec3 velocity;
        protected SoundCheck sound_check;
        protected string filename;

        private AudioClip audioClip;
        private static AudioSource audioSource;
    }
    public class SoundCheck
    {
        public virtual void Update(Sound s) { }
    }
}
