﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars.exe
    ORIGINAL FILE:      StarSystem.h/StarSystem.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Various heavenly bodies
*/
using System;
using System.Collections.Generic;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using UnityEngine;

namespace StarshatterNet.Stars.StarSystems
{
    public class Orbital
    {
        public enum OrbitalType { NOTHING, STAR, PLANET, MOON, REGION, TERRAIN };

        public Orbital(StarSystem sys, string n, OrbitalType t, double m, float r, double o, Orbital p = null)
        {
            name = n; type = t; subtype = 0; radius = r; mass = m; orbit = o;
            phase = 0; period = 0; rotation = 0; retro = false;
            system = sys; primary = p; loc = new Vector3F(0, 0, 0); rep = null; velocity = 0;

            if (system != null && primary != null && orbit > 0)
            {
                velocity = Math.Sqrt(GRAV * primary.Mass() / orbit);
                period = 2 * Math.PI * orbit / velocity;
            }
            Update();
        }

        //public virtual ~Orbital();

        //public int operator ==(Orbital& o) { return type == o.type && name == o.name && system == o.system; }
        public static bool operator ==(Orbital left, Orbital right)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.type == right.type && left.name == right.name && left.system == right.system; ;
        }
        public static bool operator !=(Orbital left, Orbital right)
        {
            return !(left == right);
        }
        public override bool Equals(object obj)
        {
            return (obj is Orbital) && (this == (Orbital)obj);
        }

        public bool Equals(Orbital right)
        {
            // Return true if the fields match:
            return this.type == right.type && this.name == right.name && this.system == right.system; ;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.type.GetHashCode();
            hash = hash * 23 + this.name.GetHashCode();
            hash = hash * 23 + this.system.GetHashCode();
            return hash;
        }

        //public int operator <(Orbital& o) { return loc.length() < o.loc.length(); }
        //public int operator <=(Orbital& o) { return loc.length() <= o.loc.length(); }
        public static bool operator <(Orbital left, Orbital right)
        {
            return left.loc.Length < right.loc.Length;
        }
        public static bool operator >(Orbital left, Orbital right)
        {
            return left.loc.Length > right.loc.Length;
        }
        public static bool operator <=(Orbital left, Orbital right) { return (left.loc.Length <= right.loc.Length); }

        public static bool operator >=(Orbital left, Orbital right) { return (left.loc.Length >= right.loc.Length); }

        // operations:
        public virtual void Update()
        {
            if (system != null && primary != null && orbit > 0)
            {
                double grade = (retro) ? -1 : 1;

                // orbits are counter clockwise:
                phase = -2 * Math.PI * grade * StarshatterNet.Stars.StarSystems.StarSystem.Stardate() / period;

                loc = primary.Location() + new Vector3F((float)(orbit * Math.Cos(phase)),
                                                        (float)(orbit * Math.Sin(phase)),
                                                        0);
            }

            foreach (var region in regions)
                region.Update();

            if (rep != null)
                rep.MoveTo(PointExtensions.OtherHand(loc));
        }

        public Vector3D PredictLocation(double delta_t)
        {
            Vector3D predicted_loc = Location();

            if (system != null && primary != null && orbit > 0)
            {
                predicted_loc = primary.PredictLocation(delta_t);

                double grade = (retro) ? -1 : 1;
#if TODO

                // orbits are(?) counter clockwise:
                double predicted_phase = (double)(-2 * Math.PI * grade * (StarSystem.Stardate() + delta_t) / period);

                predicted_loc += new Vector3((float)(orbit * Math.Cos(predicted_phase)),
                (float)(orbit * Math.Sin(predicted_phase)),
                0);
#endif
                throw new NotImplementedException();
            }

            return predicted_loc;
        }

        // accessors:
        public string Name() { return name; }
        public OrbitalType Type() { return type; }
        public int SubType() { return subtype; }

        public string Description() { return description; }
        public double Mass() { return mass; }
        public double Radius() { return radius; }
        public double Rotation() { return rotation; }
        public double RotationPhase() { return theta; }
        public double Orbit() { return orbit; }
        public bool Retrograde() { return retro; }
        public double Phase() { return phase; }
        public double Period() { return period; }
        public Vector3D Location() { return loc; }
        public Graphic Rep() { return rep; }

        public Texture2D GetMapIcon() { return map_icon; }
        public void SetMapIcon(Texture2D img)
        {
#if TODO
             if (img.Width() >= 64 && img.Height() >= 64)
            {
                map_icon.CopyBitmap(img);
                map_icon.AutoMask();
                map_icon.MakeTexture();
            }
#endif
            throw new NotImplementedException();
        }

        public StarSystem StarSystem() { return system; }
        public Orbital Primary() { return primary; }
        public List<OrbitalRegion> Regions() { return regions; }

        protected string name;
        protected internal OrbitalType type;
        internal protected int subtype;

        protected string description;
        protected double mass;
        protected internal float radius;
        protected internal double rotation;
        protected double theta;
        protected double orbit;
        protected double phase;
        protected double period;
        protected double velocity;
        protected internal Vector3D loc;
        internal protected bool retro;
        public Graphic rep;
        protected Texture2D map_icon;

        protected StarSystem system;
        protected Orbital primary;

        internal protected List<OrbitalRegion> regions = new List<OrbitalRegion>();

        private const double GRAV = 6.673e-11;

    }
}
