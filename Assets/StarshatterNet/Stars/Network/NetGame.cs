﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetGame.h/NetGame.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Network Game Manager class
 */
using System;
using System.Collections.Generic;
using StarshatterNet.Config;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using DWORD = System.UInt32;

namespace StarshatterNet.Stars.Network
{
    public class NetGame
    {
        public enum MsgType { SHIP, SHOT };

        public NetGame() { }


        public virtual bool IsClient()
        {
            return false;
        }
        public virtual bool IsServer() { return false; }
        public virtual bool IsActive() { return active; }

        public virtual DWORD GetNetID() { return netid; }
        public virtual DWORD GetObjID() { throw new NotImplementedException(); }

        public virtual void ExecFrame() { throw new NotImplementedException(); }
        public virtual void Recv() { throw new NotImplementedException(); }
        public virtual void Send() { throw new NotImplementedException(); }

        public virtual void SendData(NetData data) { }

        public virtual NetPlayer FindPlayerByName(string name) { throw new NotImplementedException(); }
        public virtual NetPlayer FindPlayerByNetID(DWORD netid) { throw new NotImplementedException(); }
        public virtual NetPlayer FindPlayerByObjID(DWORD objid) { throw new NotImplementedException(); }
        public virtual Ship FindShipByObjID(DWORD objid) { throw new NotImplementedException(); }
        public virtual Shot FindShotByObjID(DWORD objid) { throw new NotImplementedException(); }

        public virtual NetPeer GetPeer(NetPlayer player) { throw new NotImplementedException(); }

        public virtual void Respawn(DWORD objid, Ship spawn) { throw new NotImplementedException(); }

        public static NetGame Create()
        {
            if (netgame == null)
            {
                if (NetServerConfig.GetInstance() != null)
                    netgame = new NetGameServer();

                else if (NetClientConfig.GetInstance() != null && NetClientConfig.GetInstance().GetSelectedServer() != null)
                    netgame = new NetGameClient();
            }

            return netgame;
        }
        public static NetGame GetInstance() { throw new NotImplementedException(); }
        public static bool IsNetGame() { throw new NotImplementedException(); }
        public static bool IsNetGameClient() { throw new NotImplementedException(); }
        public static bool IsNetGameServer() { throw new NotImplementedException(); }
        public static int NumPlayers() { throw new NotImplementedException(); }

        public static DWORD GetNextObjID(MsgType type = MsgType.SHIP) { throw new NotImplementedException(); }

#if TODO
        protected virtual void DoJoinRequest(NetMsg msg) { }
        protected virtual void DoJoinAnnounce(NetMsg msg) { }
        protected virtual void DoQuitRequest(NetMsg msg) { }
        protected virtual void DoQuitAnnounce(NetMsg msg) { }
        protected virtual void DoGameOver(NetMsg msg) { }
        protected virtual void DoDisconnect(NetMsg msg) { }

        protected virtual void DoObjLoc(NetMsg msg) { }
        protected virtual void DoObjDamage(NetMsg msg) { }
        protected virtual void DoObjKill(NetMsg msg) { }
        protected virtual void DoObjSpawn(NetMsg msg) { }
        protected virtual void DoObjHyper(NetMsg msg) { }
        protected virtual void DoObjTarget(NetMsg msg) { }
        protected virtual void DoObjEmcon(NetMsg msg) { }
        protected virtual void DoSysDamage(NetMsg msg) { }
        protected virtual void DoSysStatus(NetMsg msg) { }

        protected virtual void DoElemCreate(NetMsg msg) { }
        protected virtual void DoElemRequest(NetMsg msg) { }
        protected virtual void DoShipLaunch(NetMsg msg) { }
        protected virtual void DoNavData(NetMsg msg) { }
        protected virtual void DoNavDelete(NetMsg msg) { }

        protected virtual void DoWepTrigger(NetMsg msg) { }
        protected virtual void DoWepRelease(NetMsg msg) { }
        protected virtual void DoWepDestroy(NetMsg msg) { }

        protected virtual void DoCommMsg(NetMsg msg) { }
        protected virtual void DoChatMsg(NetMsg msg) { }
        protected virtual void DoSelfDestruct(NetMsg msg) { }
        protected List<NetPlayer> players;
        protected NetLink link;
#endif

        protected DWORD objid;
        protected DWORD netid;
        protected Ship local_player;
        protected string player_name;
        protected string player_pass;
        protected Ship target;
        protected Sim sim;
        protected bool active;

        protected DWORD last_send_time;

        private const int MAX_NET_FPS = 20;
        private const int MIN_NET_FRAME = 1000 / MAX_NET_FPS;

        private const DWORD SHIP_ID_START = 0x0010;
        private const DWORD SHOT_ID_START = 0x0400;

        private static NetGame  netgame = null;

        private static DWORD ship_id_key = SHIP_ID_START;
        private static DWORD shot_id_key = SHOT_ID_START;

        private static long start_time = 0;

    }
}