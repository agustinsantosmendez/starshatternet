﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      AwardShowDlg.h/AwardShowDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class AwardShowDlg : FormWindow
    {
        public AwardShowDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
            base(s, 0, 0, s.Width(), s.Height(), c, p, "AwardShowDlg")
        {
            manager = mgr;
            lbl_name = null; lbl_info = null; img_rank = null; btn_close = null; exit_latch = true;
            rank = -1; medal = -1;

            Init(def);
        }
        //public virtual ~AwardShowDlg();
        public override void RegisterControls()
        {
            lbl_name = FindControl(203);
            lbl_info = FindControl(201);
            img_rank = (ImageBox)FindControl(202);

            btn_close = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, btn_close,   OnClose);
        }
        public override void Show()
        {
            base.Show();
            ShowAward();
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                if (!exit_latch)
                    OnClose(null, null);
            }

            else if (Keyboard.KeyDown(KeyMap.VK_ESCAPE))
            {
                if (!exit_latch)
                    OnClose(null, null);
            }

            else
            {
                exit_latch = false;
            }
        }


        // Operations:
        public virtual void OnClose(ActiveWindow obj, AWEvent evnt)
        {
            manager.ShowPlayerDlg();
        }

        public virtual void ShowAward()
        {
            if (rank >= 0)
            {
                if (lbl_name != null)
                {
                    lbl_name.SetText("Rank of " + Player.RankName(rank));
                }

                if (lbl_info != null)
                {
                    lbl_info.SetText(Player.RankDescription(rank));
                }

                if (img_rank != null)
                {
                    img_rank.SetPicture(Player.RankInsignia(rank, 1).Texture);
                    img_rank.Show();
                }
            }

            else if (medal >= 0)
            {
                if (lbl_name != null)
                {
                    lbl_name.SetText(Player.MedalName(medal));
                }

                if (lbl_info != null)
                {
                    lbl_info.SetText(Player.MedalDescription(medal));
                }

                if (img_rank != null)
                {
                    img_rank.SetPicture( Player.MedalInsignia(medal, 1).Texture);
                    img_rank.Show();
                }
            }

            else
            {
                if (lbl_name != null) lbl_name.SetText("");
                if (lbl_info != null) lbl_info.SetText("");
                if (img_rank != null) img_rank.Hide();
            }
        }
        public virtual void SetRank(int r)
        {
            rank = r;
            medal = -1;
        }
        public virtual void SetMedal(int m)
        {
            rank = -1;
            medal = m;
        }


        protected MenuScreen manager;

        protected ActiveWindow lbl_name;
        protected ActiveWindow lbl_info;
        protected ImageBox img_rank;
        protected Button btn_close;

        protected bool exit_latch;

        protected int rank;
        protected int medal;

    }
}