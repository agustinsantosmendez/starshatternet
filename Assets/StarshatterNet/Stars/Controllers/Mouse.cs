﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    FILE:         Mouse.h
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mouse class
*/
using System;
using System.IO;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Controllers
{
    public static class Mouse
    {
        public enum CURSOR { ARROW, CROSS, WAIT, NOT, DRAG, USER1, USER2, USER3 };
        public enum HOTSPOT { HOTSPOT_CTR, HOTSPOT_NW };

        public static float X() { return x; }
        public static float Y() { return y; }
        public static int LButton() { return l; }
        public static int MButton() { return m; }
        public static int RButton() { return r; }
        public static float Wheel() { return w; }

        public static void Paint()
        {
            UnityEngine.Cursor.visible = show;
            if (show)
            {
                Vector2 hotSpot = Vector2.zero;
                CursorMode cursorMode = CursorMode.Auto;
                UnityEngine.Cursor.SetCursor(image[(int)cursor], hotSpot, cursorMode);
            }
        }

        public static void SetCursorPos(int x, int y) { throw new NotImplementedException(); }
        public static void Show(bool s = true)
        {
            show = s;
            Paint();
        }
        public static CURSOR SetCursor(CURSOR c)
        {
            CURSOR old = cursor;
            cursor = c;
            Paint();
            return old;
        }
        public static int LoadCursor(CURSOR c, string name, HOTSPOT hs = HOTSPOT.HOTSPOT_CTR)
        {
            int result = 0;

            //delete image[c];
            image[(int)c] = null;

            if (!string.IsNullOrEmpty(name))
            {
                name = Path.GetFileNameWithoutExtension(name); 
                image[(int)c] = Resources.Load("Cursors/" + name) as Texture2D;

                if (image[(int)c] != null)
                {
                    hotspot[(int)c] = hs;
                }
            }

            return result;
        }


        public static void Create(Screen screen)
        {
            // Nothing to do. Unity 3d takes care.
        }
#if TODO
        public static void Resize(Screen  screen) { throw new NotImplementedException(); }
#endif
        public static void Close() { throw new NotImplementedException(); }

        private static bool show = true;
        private static CURSOR cursor = CURSOR.ARROW;

        internal static float x = 320;
        internal static float y = 240;
        internal static int l = 0;
        internal static int m = 0;
        internal static int r = 0;
        internal static float w = 0;

        private static Texture2D[] image = new Texture2D[8];
        private static HOTSPOT[] hotspot = new HOTSPOT[8];


    }
}
