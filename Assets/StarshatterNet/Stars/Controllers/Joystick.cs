﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    FILE:         Joystick.h/Joystick.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Joystick Input class
*/
using System;

using StarshatterNet.Config;
using StarshatterNet.Gen.Misc;
using UnityEngine;
using DWORD = System.UInt32;
namespace StarshatterNet.Controllers
{
    public class Joystick : MotionController
    {
        public Joystick()
        {
            x = 0; y = 0; z = 0; p = 0; r = 0; w = 0; t = 0;
            if (joystick == null)
                joystick = this;

            select = 1;
            rudder = 0;
            throttle = 1;
            sensitivity = 25;
            dead_zone = 100;

            for (int i = 0; i < MotionController.MaxActions; i++)
                action[i] = false;

            for (int i = 0; i < KeyMap.KEY_MAP_SIZE; i++)
                map[i] = 0;

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    hat[i, j] = false;
                }
            }

            map_axis[0] = KeyMap.KEY_JOY_AXIS_X;
            map_axis[1] = KeyMap.KEY_JOY_AXIS_Y;
            map_axis[2] = KeyMap.KEY_JOY_AXIS_RZ;
            map_axis[3] = KeyMap.KEY_JOY_AXIS_S0;

            inv_axis[0] = false;
            inv_axis[1] = false;
            inv_axis[2] = false;
            inv_axis[3] = false;

#if TODO
            if (MachineInfo::GetDirectXVersion() < MachineInfo::DX_7)
            {
                Print("Joystick: DI7 not found, using multimedia library\n");
                pdi = 0;
                pdev = 0;
            }

            else if (!pdi)
            {
                HRESULT hr = DirectInputCreateEx(Game::GetHINST(),
                DIRECTINPUT_VERSION,
                IID_IDirectInput7,
                (void**)&pdi,
                NULL);
                if FAILED(hr) {
                    DirectInputError("Failed to initialize DI7", hr);
                    pdi = 0;
                    pdev = 0;
                }
                else
                {
                    Print("Joystick: initialized DI7 pdi = %08x\n", (DWORD)pdi);
                }
            }
#endif
        }
        //public virtual ~Joystick();

        // setup
        public override void MapKeys(KeyMapEntry[] mapping, int nkeys)
        {
            //ZeroMemory(map, sizeof(map));

            for (int i = 0; i < nkeys; i++)
            {
                KeyMapEntry k = mapping[i];

                if (k.act >= KeyMap.KEY_MAP_FIRST && k.act < KeyMap.KEY_MAP_LAST)
                {
                    if (k.act == KeyMap.KEY_JOY_SENSE)
                        sensitivity = k.key;

                    else if (k.act == KeyMap.KEY_JOY_DEAD_ZONE)
                        dead_zone = k.key;

                    else if (k.act == KeyMap.KEY_JOY_SWAP)
                        swapped = k.key != 0;

                    else if (k.act == KeyMap.KEY_JOY_RUDDER)
                        rudder = k.key;

                    else if (k.act == KeyMap.KEY_JOY_THROTTLE)
                        throttle = k.key;

                    else if (k.act == KeyMap.KEY_JOY_SELECT)
                        select = k.key;


                    else if (k.act == KeyMap.KEY_AXIS_YAW)
                        map_axis[0] = k.key;

                    else if (k.act == KeyMap.KEY_AXIS_PITCH)
                        map_axis[1] = k.key;

                    else if (k.act == KeyMap.KEY_AXIS_ROLL)
                        map_axis[2] = k.key;

                    else if (k.act == KeyMap.KEY_AXIS_THROTTLE)
                        map_axis[3] = k.key;


                    else if (k.act == KeyMap.KEY_AXIS_YAW_INVERT)
                        inv_axis[0] = k.key != 0 ? true : false;

                    else if (k.act == KeyMap.KEY_AXIS_PITCH_INVERT)
                        inv_axis[1] = k.key != 0 ? true : false;

                    else if (k.act == KeyMap.KEY_AXIS_ROLL_INVERT)
                        inv_axis[2] = k.key != 0 ? true : false;

                    else if (k.act == KeyMap.KEY_AXIS_THROTTLE_INVERT)
                        inv_axis[3] = k.key != 0 ? true : false;

                    else if (k.key >= KeyMap.KEY_JOY_1 && k.key <= KeyMap.KEY_JOY_32)
                        map[k.act] = k.key;

                    else if (k.alt >= KeyMap.KEY_JOY_1 && k.alt <= KeyMap.KEY_JOY_32)
                        map[k.act] = k.alt;

                    else if (k.joy >= KeyMap.KEY_JOY_1 && k.joy <= KeyMap.KEY_JOY_32)
                        map[k.act] = k.joy;

                    else if (k.key >= KeyMap.KEY_POV_0_UP && k.key <= KeyMap.KEY_POV_3_RIGHT)
                        map[k.act] = k.key;

                    else if (k.alt >= KeyMap.KEY_POV_0_UP && k.alt <= KeyMap.KEY_POV_3_RIGHT)
                        map[k.act] = k.alt;

                    else if (k.joy >= KeyMap.KEY_POV_0_UP && k.joy <= KeyMap.KEY_POV_3_RIGHT)
                        map[k.act] = k.joy;
                }
            }
        }

        // sample the physical device
        public override void Acquire()
        {
            //Nothing to do
        }

        // translations
        public override double X() { return x; }
        public override double Y() { return y; }
        public override double Z() { return z; }

        // rotations
        public override double Pitch() { return p; }
        public override double Roll() { return r; }
        public override double Yaw() { return w; }
        public override int Center() { return 0; }

        // throttle
        public override double Throttle() { return t; }
        public override void SetThrottle(double throttle) { t = throttle; }

        // actions
        public override bool Action(int n) { return action[n]; }
        public override bool ActionMap(int n) { return KeyDownMap(n); }

        public static bool KeyDown(int key)
        {
            if (joystick == null)
                return false;
            if (key >= KeyMap.KEY_JOY_1 && key <= KeyMap.KEY_JOY_32)
                return joystick.action[key - KeyMap.KEY_JOY_1];

            else if (key >= KeyMap.KEY_POV_0_UP && key <= KeyMap.KEY_POV_0_RIGHT)
                return joystick.hat[0, key - KeyMap.KEY_POV_0_UP];

            else if (key >= KeyMap.KEY_POV_1_UP && key <= KeyMap.KEY_POV_1_RIGHT)
                return joystick.hat[1, key - KeyMap.KEY_POV_1_UP];

            else if (key >= KeyMap.KEY_POV_2_UP && key <= KeyMap.KEY_POV_2_RIGHT)
                return joystick.hat[2, key - KeyMap.KEY_POV_2_UP];

            else if (key >= KeyMap.KEY_POV_3_UP && key <= KeyMap.KEY_POV_3_RIGHT)
                return joystick.hat[3, key - KeyMap.KEY_POV_3_UP];

            return false;
        }
        public static bool KeyDownMap(int key)
        {
            if (joystick == null)
                return false;

            if (key >= KeyMap.KEY_MAP_FIRST && key <= KeyMap.KEY_MAP_LAST && joystick.map[key] != 0)
                return KeyDown(joystick.map[key]);

            return false;
        }


        public static Joystick GetInstance()
        {
            return joystick;
        }

        public static void EnumerateDevices()
        {
            if (ndev < 1)
            {
                ErrLogger.PrintLine("Joystick: preparing to enumerate devices");
                joystickNames = Input.GetJoystickNames();

                ndev = joystickNames.Length;
            }
            ErrLogger.PrintLine("Joystick: detected {0} devices.", ndev);
        }
        public static int NumDevices()
        {
            return ndev;
        }

        public static string GetDeviceName(int i)
        {
            if (i >= 0 && i < ndev)
                return joystickNames[i];

            return null;
        }


        public static int ReadRawAxis(int axis) { throw new NotImplementedException(); }
        public static int GetAxisMap(int n) { throw new NotImplementedException(); }
        public static int GetAxisInv(int n) { throw new NotImplementedException(); }


        protected double ReadAxisDI(int axis) { throw new NotImplementedException(); }
        protected double ReadAxisMM(int axis) { throw new NotImplementedException(); }
        protected void ProcessAxes(double joy_x, double joy_y, double joy_r, double joy_t) { throw new NotImplementedException(); }
        protected void ProcessHat(int i, DWORD joy_pov) { throw new NotImplementedException(); }

        protected double x, y, z, p, r, w, t;
        protected bool[] action = new bool[MotionController.MaxActions];
        protected bool[,] hat = new bool[4, 4];
        protected int[] map = new int[KeyMap.KEY_MAP_SIZE];
        protected int[] map_axis = new int[4];
        protected bool[] inv_axis = new bool[4];

        private const int MAX_DEVICES = 8;
        protected static string[] joystickNames;

        private static int ndev = 0;
        private static int idev = -1;
        private static int strikes = 3;

        private static Joystick joystick = null;
    }
}
