﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    FILE:         MouseController.h/MouseController.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Abstract MotionController class (hides details of Joystick, Keyboard, etc.)
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Controllers
{
    public class MouseController : MotionController
    {
        public MouseController()
        {
            select = 0;
            sensitivity = 10;
            swapped = false;
            active = false;

            active_key = 20; // caps lock

            rbutton_latch = 0;

            for (int i = 0; i < MotionController.MaxActions; i++)
                action[i] = false;
        }
        // public virtual ~MouseController();

        // setup
        public override void MapKeys(KeyMapEntry[] mapping, int nkeys)
        {
            for (int i = 0; i < nkeys; i++)
            {
                KeyMapEntry k = mapping[i];

                if (k.act >= KeyMap.KEY_MAP_FIRST && k.act <= KeyMap.KEY_MAP_LAST)
                {

                    if (k.act == KeyMap.KEY_MOUSE_SENSE)
                        sensitivity = k.key;

                    else if (k.act == KeyMap.KEY_MOUSE_SWAP)
                        swapped = k.key != 0;

                    else if (k.act == KeyMap.KEY_MOUSE_INVERT)
                        inverted = k.key;

                    else if (k.act == KeyMap.KEY_MOUSE_SELECT)
                        select = k.key;

                    else if (k.act == KeyMap.KEY_MOUSE_ACTIVE)
                        active_key = k.key;
                }
            }
        }

        // sample the physical device
        public override void Acquire()
        {
#if TODO
            p = r = w = 0;
            action[0] = 0;
            action[1] = 0;
            action[2] = 0;
            action[3] = 0;

            if (active_key != 0 && Keyboard.KeyDown(active_key))
            {
                active_latch = 1;
            }
            else
            {
                if (active_latch != 0)
                {
                    active_latch = 0;
                    active = !active;
                }
            }

            if (select == 0 || !active)
                return;

            action[0] = Mouse.LButton();

            int roll_enable = 0;

            if (Mouse.RButton() != 0)
            {
                roll_enable = 1;

                if (rbutton_latch == 0)
                    rbutton_latch = Game.RealTime();
            }
            else
            {
                if (rbutton_latch != 0)
                {
                    rbutton_latch = Game.RealTime() - rbutton_latch;
                    if (rbutton_latch < 250)
                        action[1] = 1;
                }

                rbutton_latch = 0;
            }

            if (Mouse.MButton() != 0)
            {
                if (mbutton_latch == 0)
                    mbutton_latch = Game.RealTime();
            }
            else
            {
                if (mbutton_latch != 0)
                {
                    action[3] = 1;
                }

                mbutton_latch = 0;
            }

            double step = 0;
            int cx = Video.GetInstance().Width() / 2;
            int cy = Video.GetInstance().Height() / 2;

            dx += Mouse.X() - cx;
            dy += Mouse.Y() - cy;

            step = Math.Abs(dx) / cx;

            if (roll_enable || select == 1)
                step *= 3 * sensitivity;
            else
                step *= step * sensitivity / 4;

            if (roll_enable != 0)
            {
                if (dx > 0)
                    r = -step;
                else if (dx < 0)
                    r = step;
            }
            else
            {
                if (dx > 0)
                    w = step;
                else if (dx < 0)
                    w = -step;
            }

            step = Math.Abs(dy) / cy;

            if (select == 1)
                step *= 2 * sensitivity;
            else
                step *= step * sensitivity / 4;

            if (inverted != 0)
            {
                step *= -1;
            }

            if (dy > 0)
                p = step;
            else if (dy < 0)
                p = -step;

            if (select == 1)
            {
                // .SetCursorPos(cx, cy);

                double drain = cx * 4 * Game.FrameTime();

                if (dx > drain)
                {
                    dx -= drain;
                }
                else if (dx < -drain)
                {
                    dx += drain;
                }
                else
                {
                    dx = 0;
                }

                if (dy > drain)
                {
                    dy -= drain;
                }
                else if (dy < -drain)
                {
                    dy += drain;
                }
                else
                {
                    dy = 0;
                }
            }
            else
            {
                dx = 0;
                dy = 0;
            }

            if (Mouse.Wheel() > 0)
            {
                if (t < 0.25)
                    t += 0.025;
                else
                    t += 0.1;

                if (t > 1) t = 1;
            }

            else if (Mouse.Wheel() < 0)
            {
                if (t < 0.25)
                    t -= 0.025;
                else
                    t -= 0.1;

                if (t < 0) t = 0;
            }
#endif
            ErrLogger.PrintLine("WARNING: MouseController.Acquire is not yet implemented.");
        }

        // translations
        public override double X() { return 0; }
        public override double Y() { return 0; }
        public override double Z() { return 0; }

        // rotations
        public override double Pitch() { if (active) return p; return 0; }
        public override double Roll() { if (active) return r; return 0; }
        public override double Yaw() { if (active) return w; return 0; }
        public override int Center() { return 0; }

        // throttle
        public override double Throttle() { if (active) return t; return 0; }
        public override void SetThrottle(double throttle) { t = throttle; }

        // actions
        public override bool Action(int n) { return action[n]; }
        public override bool ActionMap(int n)
        {
            if (n >= KeyMap.KEY_ACTION_0 && n <= KeyMap.KEY_ACTION_3)
                return action[n - KeyMap.KEY_ACTION_0];

            return false;
        }

        // actively sampling?
        public virtual bool Active() { return active; }
        public virtual void SetActive(bool a) { active = a; }

        public static MouseController GetInstance()
        {
            return instance;
        }


        protected double p = 0, r = 0, w = 0, dx = 0, dy = 0, t = 0;
        protected bool[] action = new bool[MotionController.MaxActions];
        protected int[] map = new int[32];
        protected bool active;
        protected int active_key;

        protected static MouseController instance = new MouseController();
        protected static UInt32 rbutton_latch = 0;
        protected static UInt32 mbutton_latch = 0;
        protected static UInt32 active_latch = 0;
    }
}
