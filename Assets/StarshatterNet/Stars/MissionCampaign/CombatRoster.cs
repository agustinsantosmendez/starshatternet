﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CombatRoster.h/CombatRoster.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    The complete roster of all known persistent entities
    for all combatants in the game.
*/
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Stars.MissionCampaign
{
    public class CombatRoster
    {
        public CombatRoster()
        {
            LoadCombatRoster();
        }

        //~CombatRoster();

        public void LoadCombatRoster()
        {
            string[] files = {
                "Alliance",
                "Dantari",
                "Haiche",
                "Hegemony",
                "Pirates",
                "Silessia",
                "Solus",
                "Zolons"
            };

            //if (ReaderSupport.DoesContentDirectoryExist("Campaigns/"))
            //{
            //    string[] files = ReaderSupport.GetContentFiles("Campaigns/", "*.json");

            for (int i = 0; i < files.Length; i++)
            {
                //var combatGroups = ReaderSupport.Deserialize<CombatGroups>(files[i]);
                string file = "Campaigns/" + files[i];
                ErrLogger.PrintLine("Reading Order of Battle file: {0}", file);
                CombatGroup g = CombatGroup.LoadOrderOfBattle(file, -1, null);
                if (g != null)
                    forces.Add(g);
            }
            //}
        }

        public static void Initialize()
        {
            roster = new CombatRoster();
        }

        public static void Close()
        {
            //delete roster;
            roster = null;
        }

        public static CombatRoster GetInstance()
        {
            return roster;
        }

        public CombatGroup GetForce(string name)
        {
            foreach (CombatGroup f in forces)
            {
                if (f.Name() == name)
                    return f;
            }

            return null;
        }

        private List<CombatGroup> forces = new List<CombatGroup>();
        private static CombatRoster roster = null;
    }
}
