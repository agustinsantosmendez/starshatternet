﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      TacRefDlg.h/TacRefDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Briefing Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using DigitalRune.Mathematics;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.SimElements;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using Screen = StarshatterNet.Gen.Graphics.Screen;
namespace StarshatterNet.Stars.Dialogs
{
    public class TacRefDlg : FormWindow
    {
        public enum MODES { MODE_NONE, MODE_SHIPS, MODE_WEAPONS };

        public TacRefDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
             base(s, 0, 0, s.Width(), s.Height(), c, p, "TacRefDlg")
        {
            manager = mgr;
            beauty = null; camview = null; imgview = null;
            txt_caption = null; txt_stats = null; txt_description = null;
            lst_designs = null; btn_close = null; btn_ships = null; btn_weaps = null;
            mode = MODES.MODE_NONE; radius = 100; mouse_x = 0; mouse_y = 0;
            cam_zoom = 2.5; cam_az = -Math.PI / 6; cam_el = Math.PI / 7; captured = false;
            ship_index = 0; weap_index = 0;

            scene = new Scene();

            Init(def);
        }
        //public virtual ~TacRefDlg();

        public override void RegisterControls()
        {
            btn_close = (Button)FindControl(1);
            btn_ships = (Button)FindControl(101);
            btn_weaps = (Button)FindControl(102);
            lst_designs = (ListBox)FindControl(200);
            txt_caption = FindControl(301);
            //TODO beauty = FindControl(401);
            txt_stats = (RichTextBox)FindControl(402);
            txt_description = (RichTextBox)FindControl(403);

            if (btn_close != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_close, OnClose);
            }

            if (btn_ships != null)
            {
                btn_ships.SetButtonState(mode == MODES.MODE_SHIPS ? (short)1 : (short)0);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_ships, OnMode);
            }

            if (btn_weaps != null)
            {
                btn_weaps.SetButtonState(mode == MODES.MODE_WEAPONS ? (short)1 : (short)0);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_weaps, OnMode);
            }

            if (lst_designs != null)
            {
                REGISTER_CLIENT(ETD.EID_SELECT, lst_designs, OnSelect);
            }

            if (beauty != null)
            {
                REGISTER_CLIENT(ETD.EID_RBUTTON_DOWN, beauty, OnCamRButtonDown);
                REGISTER_CLIENT(ETD.EID_RBUTTON_UP, beauty, OnCamRButtonUp);
                REGISTER_CLIENT(ETD.EID_MOUSE_MOVE, beauty, OnCamMove);
                REGISTER_CLIENT(ETD.EID_MOUSE_WHEEL, beauty, OnCamZoom);

                scene.SetAmbient(new Color32(60, 60, 60, 255));
                Point light_pos = new Point(3e6, 5e6, 4e6);

                LightNode main_light = new LightNode(1.0f); //1.25f);
                main_light.MoveTo(light_pos);
                main_light.SetType(LightNode.TYPES.LIGHT_DIRECTIONAL);
                main_light.SetColor(Color.white);
                main_light.SetShadow(true);

                scene.AddLight(main_light);

                LightNode back_light = new LightNode(0.5f);
                back_light.MoveTo(light_pos * -1);
                back_light.SetType(LightNode.TYPES.LIGHT_DIRECTIONAL);
                back_light.SetColor(Color.white);
                back_light.SetShadow(false);

                scene.AddLight(back_light);

                camview = new CameraView(beauty, scene, "TacRefDlgView");
                camview.SetProjectionType(CameraView.PROJECTION_TYPE.PROJECTION_PERSPECTIVE);
                camview.SetFieldOfView(2);

                beauty.AddView(camview);

                imgview = new ImgView(beauty, null, "TacRefDlg Imgview");
                //TODO imgview.SetBlend(Video.BLEND_ALPHA);
            }
        }
        public virtual void ExecFrame()
        {
        }
        public override void Show()
        {
            update_scene = !shown;

            base.Show();

            if (update_scene)
            {
                AWEvent evnt = new AWEvent(btn_ships, ETD.EID_CLICK);
                OnMode(this, evnt);
            }
        }

        // Operations:
        public virtual void OnClose(ActiveWindow obj, AWEvent evnt)
        {
            manager.ShowMenuDlg();
        }
        public virtual void OnMode(ActiveWindow obj, AWEvent evnt)
        {
            if (evnt.window == btn_ships && mode != MODES.MODE_SHIPS)
            {
                mode = MODES.MODE_SHIPS;

                if (lst_designs != null)
                {
                    lst_designs.ClearItems();

                    List<string> designs = new List<string>();

                    for (int n = 0; n < 16; n++)
                    {
                        int type = 1 << n;
                        ShipDesign.GetDesignList(type, designs);

                        foreach (var val in designs)
                        {

                            ShipDesign dsn2 = ShipDesign.Get(val);

                            if (dsn2 != null)
                            {
                                string txt = string.Format("{0} {1}", dsn2.abrv, dsn2.DisplayName());
                                lst_designs.AddItemWithData(txt, dsn2);
                            }
                            else
                            {
                                lst_designs.AddItemWithData(val, 0);
                            }
                        }
                    }

                    lst_designs.SetSelected(ship_index);
                    lst_designs.RebuildListObjets();

#if TODO
                if (beauty != null)
                {
                    beauty.AddView(camview);
                    beauty.DelView(imgview);
                }
#endif
                    ShipDesign dsn = lst_designs.GetItemData(ship_index) as ShipDesign;

                    if (dsn != null)
                    {
                        SelectShip((ShipDesign)dsn);
                    }
                }

            }

            else if (evnt.window == btn_weaps && mode != MODES.MODE_WEAPONS)
            {
                mode = MODES.MODE_WEAPONS;

                WeaponDesign design = null;

                if (lst_designs != null)
                {
                    lst_designs.ClearItems();
                    List<string> designs = new List<string>();

                    WeaponDesign.GetDesignList(designs);

                    foreach (string val in designs)
                    {
                        if (val.Contains("Zolon") || val.Contains("Inverted"))
                            continue;

                        WeaponDesign dsn2 = WeaponDesign.Find(val);

                        if (dsn2 != null && !dsn2.secret)
                        {
                            lst_designs.AddItemWithData(val, dsn2);
                            if (design == null)
                                design = dsn2;
                        }
                    }

                    lst_designs.SetSelected(weap_index);
                    lst_designs.RebuildListObjets();

#if TODO
                if (beauty != null)
                {
                    beauty.DelView(camview);
                    beauty.AddView(imgview);
                }
#endif
                    WeaponDesign dsn = lst_designs.GetItemData(weap_index) as WeaponDesign;

                    if (dsn != null)
                    {
                        SelectWeapon(dsn);
                    }
                }

            }

            if (btn_ships != null)
                btn_ships.SetButtonState(mode == MODES.MODE_SHIPS ? (short)1 : (short)0);
            if (btn_weaps != null)
                btn_weaps.SetButtonState(mode == MODES.MODE_WEAPONS ? (short)1 : (short)0);
        }
        public virtual void OnSelect(ActiveWindow obj, AWEvent evnt)
        {
            if (lst_designs != null)
            {
                int seln = lst_designs.GetSelection();

                if (mode == MODES.MODE_SHIPS)
                {
                    ship_index = seln;
                    ShipDesign dsn = (ShipDesign)lst_designs.GetItemData(seln);

                    if (dsn != null)
                    {
                        SelectShip(dsn);
                    }
                }

                else if (mode == MODES.MODE_WEAPONS)
                {
                    weap_index = seln;
                    WeaponDesign dsn = (WeaponDesign)lst_designs.GetItemData(seln);

                    if (dsn != null)
                    {

                        SelectWeapon(dsn);
                    }
                }
            }
        }
        public virtual void OnCamRButtonDown(ActiveWindow obj, AWEvent evnt)
        {
            captured = SetCaptureBeauty();
            mouse_x = evnt.x;
            mouse_y = evnt.y;
        }
        public virtual void OnCamRButtonUp(ActiveWindow obj, AWEvent evnt)
        {
            if (captured)
                ReleaseCaptureBeauty();

            captured = false;
            mouse_x = 0;
            mouse_y = 0;
        }
        public virtual void OnCamMove(ActiveWindow obj, AWEvent evnt)
        {
            if (captured)
            {
                int mouse_dx = evnt.x - mouse_x;
                int mouse_dy = evnt.y - mouse_y;

                UpdateAzimuth(mouse_dx * 0.3 * ConstantsF.DEGREES);
                UpdateElevation(mouse_dy * 0.3 * ConstantsF.DEGREES);
                UpdateCamera();

                mouse_x = evnt.x;
                mouse_y = evnt.y;
            }
        }
        public virtual void OnCamZoom(ActiveWindow obj, AWEvent evnt)
        {
            int w = (int)Mouse.Wheel();

            if (w < 0)
            {
                while (w < 0)
                {
                    UpdateZoom(1.25);
                    w += 120;
                }
            }
            else
            {
                while (w > 0)
                {
                    UpdateZoom(0.75f);
                    w -= 120;
                }
            }

            UpdateCamera();
        }


        protected virtual void SelectShip(ShipDesign design)
        {
            if (beauty != null && camview != null)
            {
                scene.Graphics().Clear();

                if (design != null)
                {
                    radius = design.radius;

                    UpdateCamera();

                    int level = design.lod_levels - 1;
                    int n = design.models[level].Count;

                    for (int i = 0; i < n; i++)
                    {
                        Model model = design.models[level][i];

                        Solid s = new Solid();
                        s.UseModel(model);
                        s.CreateShadows(1);
                        s.MoveTo(design.offsets[level][i]);

                        scene.Graphics().Add(s);
                    }
                }
            }

            if (txt_caption != null)
            {
                txt_caption.SetText("");

                if (design != null)
                {
                    string txt = string.Format("{0} {1}", design.abrv, design.DisplayName());
                    txt_caption.SetText(txt);
                }
            }

            if (txt_stats != null)
            {
                txt_stats.SetText("");

                if (design != null)
                {
                    string desc = "";

                    string txt = string.Format("{0}\t\t\t{1}\n", Game.GetText("tacref.type"), Ship.ClassName(design.type));
                    desc += txt;

                    txt = string.Format("{0}\t\t\t{1}\n", Game.GetText("tacref.class"), design.DisplayName());
                    desc += txt;
                    desc += Game.GetText("tacref.length");
                    desc += "\t\t";

                    if (design.type < CLASSIFICATION.STATION)
                        FormatUtil.FormatNumber(out txt, design.radius / 2);
                    else
                        FormatUtil.FormatNumber(out txt, design.radius * 2);

                    txt += " m\n";
                    desc += txt;
                    desc += Game.GetText("tacref.mass");
                    desc += "\t\t\t";

                    FormatUtil.FormatNumber(out txt, design.mass);
                    txt += " T\n";
                    desc += txt;
                    desc += Game.GetText("tacref.hull");
                    desc += "\t\t\t";

                    FormatUtil.FormatNumber(out txt, design.integrity);
                    txt += "\n";
                    desc += txt;

                    if (design.weapons.Count > 0)
                    {
                        desc += Game.GetText("tacref.weapons");

                        WepGroup[] groups = new WepGroup[8];
                        for (int w = 0; w < design.weapons.Count; w++)
                        {
                            Weapon gun = design.weapons[w];
                            WepGroup group = FindWepGroup(groups, gun.Group());

                            if (group != null)
                                group.count++;
                        }

                        for (int g = 0; g < 8; g++)
                        {
                            WepGroup group = groups[g];
                            if (group != null && group.count > 0)
                            {
                                txt = string.Format("\t\t{0} ({1})\n\t\t", group.name, group.count);
                                desc += txt;

                                for (int w = 0; w < design.weapons.Count; w++)
                                {
                                    Weapon gun = design.weapons[w];

                                    if (group.name == gun.Group())
                                    {
                                        txt = string.Format("\t\t\t{0}\n\t\t", gun.Design().name);
                                        desc += txt;
                                    }
                                }
                            }
                        }

                        desc += "\n";
                    }

                    txt_stats.SetText(desc);
                }
            }

            if (txt_description != null)
            {
                if (design != null && !string.IsNullOrEmpty(design.description))
                {
                    txt_description.SetText(design.description);
                }
                else
                {
                    txt_description.SetText(Game.GetText("tacref.mass"));
                }
            }
        }
        protected virtual void SelectWeapon(WeaponDesign design)
        {
            if (beauty != null && imgview != null)
            {
                imgview.SetPicture(null);

                if (design != null)
                    imgview.SetPicture(design.beauty_img);
            }

            if (txt_caption != null)
            {
                txt_caption.SetText("");

                if (design != null)
                    txt_caption.SetText(design.name);
            }

            if (txt_stats != null)
            {
                txt_stats.SetText("");

                if (design != null)
                {
                    string desc;
                    string txt = "";

                    desc = Game.GetText("tacref.name");
                    desc += "\t";
                    desc += design.name;
                    desc += "\n";
                    desc += Game.GetText("tacref.type");
                    desc += "\t\t";

                    if (design.damage < 1)
                        desc += Game.GetText("tacref.wep.other");
                    else if (design.beam)
                        desc += Game.GetText("tacref.wep.beam");
                    else if (design.primary)
                        desc += Game.GetText("tacref.wep.bolt");
                    else if (design.drone)
                        desc += Game.GetText("tacref.wep.drone");
                    else if (design.guided != 0)
                        desc += Game.GetText("tacref.wep.guided");
                    else
                        desc += Game.GetText("tacref.wep.missile");

                    if (design.turret_model != null && design.damage >= 1)
                    {
                        desc += " ";
                        desc += Game.GetText("tacref.wep.turret");
                        desc += "\n";
                    }
                    else
                    {
                        desc += "\n";
                    }

                    desc += Game.GetText("tacref.targets");
                    desc += "\t";

                    if ((design.target_type & CLASSIFICATION.DROPSHIPS) != 0)
                    {
                        if ((design.target_type & CLASSIFICATION.STARSHIPS) != 0)
                        {
                            if ((design.target_type & CLASSIFICATION.GROUND_UNITS) != 0)
                            {
                                desc += Game.GetText("tacref.targets.fsg");
                            }
                            else
                            {
                                desc += Game.GetText("tacref.targets.fs");
                            }
                        }
                        else
                        {
                            if ((design.target_type & CLASSIFICATION.GROUND_UNITS) != 0)
                            {
                                desc += Game.GetText("tacref.targets.fg");
                            }
                            else
                            {
                                desc += Game.GetText("tacref.targets.f");
                            }
                        }
                    }

                    else if ((design.target_type & CLASSIFICATION.STARSHIPS) != 0)
                    {
                        if ((design.target_type & CLASSIFICATION.GROUND_UNITS) != 0)
                        {
                            desc += Game.GetText("tacref.targets.sg");
                        }
                        else
                        {
                            desc += Game.GetText("tacref.targets.s");
                        }
                    }

                    else if ((design.target_type & CLASSIFICATION.GROUND_UNITS) != 0)
                    {
                        desc += Game.GetText("tacref.targets.g");
                    }

                    desc += "\n";
                    desc += Game.GetText("tacref.speed");
                    desc += "\t";

                    FormatUtil.FormatNumber(out txt, design.speed);
                    desc += txt;
                    desc += "m/s\n";
                    desc += Game.GetText("tacref.range");
                    desc += "\t";

                    FormatUtil.FormatNumber(out txt, design.max_range);
                    desc += txt;
                    desc += "m\n";
                    desc += Game.GetText("tacref.damage");
                    desc += "\t";

                    if (design.damage > 0)
                    {
                        FormatUtil.FormatNumber(out txt, design.damage * design.charge);
                        desc += txt;
                        if (design.beam)
                            desc += "/s";
                    }
                    else
                    {
                        desc += Game.GetText("tacref.none");
                    }

                    desc += "\n";

                    if (!design.primary && design.damage > 0)
                    {
                        desc += Game.GetText("tacref.kill-radius");
                        desc += "\t";
                        FormatUtil.FormatNumber(out txt, design.lethal_radius);
                        desc += txt;
                        desc += " m\n";
                    }

                    txt_stats.SetText(desc);
                }
            }

            if (txt_description != null)
            {
                if (design != null && !string.IsNullOrEmpty(design.description))
                {
                    txt_description.SetText(design.description);
                }
                else
                {
                    txt_description.SetText(Game.GetText("tacref.no-info"));
                }
            }
        }

        protected virtual void UpdateZoom(double delta)
        {
            cam_zoom *= delta;

            if (cam_zoom < 1.2)
                cam_zoom = 1.2;

            else if (cam_zoom > 10)
                cam_zoom = 10;
        }
        protected virtual void UpdateAzimuth(double a)
        {
            cam_az += a;

            if (cam_az > Math.PI)
                cam_az = -2 * Math.PI + cam_az;

            else if (cam_az < -Math.PI)
                cam_az = 2 * Math.PI + cam_az;
        }
        protected virtual void UpdateElevation(double e)
        {
            cam_el += e;

            const double limit = (0.43 * Math.PI);

            if (cam_el > limit)
                cam_el = limit;
            else if (cam_el < -limit)
                cam_el = -limit;
        }
        protected virtual void UpdateCamera()
        {
            double x = cam_zoom * radius * Math.Sin(cam_az) * Math.Cos(cam_el);
            double y = cam_zoom * radius * Math.Cos(cam_az) * Math.Cos(cam_el);
            double z = cam_zoom * radius * Math.Sin(cam_el);
            cam.LookAt(new Point(0, 0, 0), new Point(x, z, y), new Point(0, 1, 0));
        }

        protected virtual bool SetCaptureBeauty() { throw new NotImplementedException(); }
        protected virtual bool ReleaseCaptureBeauty() { throw new NotImplementedException(); }
        public WepGroup FindWepGroup(WepGroup[] weapons, string name)
        {
            WepGroup group = null;
            int w = 0;
            WepGroup iter;
            int first = -1;
            while (group == null && w < 8)
            {
                if (weapons[w] == null)
                    weapons[w] = new WepGroup();

                iter = weapons[w];
                if (first < 0 && string.IsNullOrEmpty(iter.name))
                    first = w;

                if (iter != null && iter.name == name)
                    group = iter;
                w++;
            }

            if (group == null && first >= 0)
            {
                group = weapons[first];
                group.name = name;
            }

            return group;
        }

        protected MenuScreen manager;
        protected ActiveWindow beauty;
        protected ListBox lst_designs;
        protected ActiveWindow txt_caption;
        protected RichTextBox txt_stats;
        protected RichTextBox txt_description;
        protected Button btn_ships;
        protected Button btn_weaps;
        protected Button btn_close;

        protected ImgView imgview;
        protected CameraView camview;
        protected Scene scene;
        protected Camera cam;

        protected MODES mode;
        protected double radius;
        protected double cam_zoom;
        protected double cam_az;
        protected double cam_el;
        protected int mouse_x;
        protected int mouse_y;
        protected bool update_scene;
        protected bool captured;

        protected int ship_index;
        protected int weap_index;
    }
    public class WepGroup
    {
        public string name;
        public int count = 0;
    }
}