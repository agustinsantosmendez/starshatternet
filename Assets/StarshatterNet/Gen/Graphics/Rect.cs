﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    ORIGINAL FILE:      Geometry.h/Geometry.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Geometric classes: Rect, Vec3, Point, Matrix, Plane
*/
using System;


namespace StarshatterNet.Stars
{
    [Serializable]
    public class Rect
    {
        public Rect() { }
        public Rect(int ix, int iy, int iw, int ih)
        { x = ix; y = iy; w = iw; h = ih; }

        // public int operator ==(const Rect& r)  const { return x==r.x && y==r.y && w==r.w && h==r.h; }
        // public int operator !=(const Rect& r)  const { return x!=r.x || y!=r.y || w!=r.w || h!=r.h; }

        public void Inflate(int dx, int dy)
        {
            x -= dx;
            w += dx * 2;
            y -= dy;
            h += dy * 2;
        }
        public void Deflate(int dx, int dy)
        {
            x += dx;
            w -= dx * 2;
            y += dy;
            h -= dy * 2;
        }
        public void Inset(int left, int right, int top, int bottom)
        {
            x += left;
            y += top;
            w -= left + right;
            h -= top + bottom;
        }
        public bool Contains(int ax, int ay)
        {
            if (ax < x) return false;
            if (ax > x + w) return false;
            if (ay < y) return false;
            if (ay > y + h) return false;

            return true;
        }

        public override bool Equals(object obj)
        {
            var rect = obj as Rect;
            return rect != null &&
                   x == rect.x &&
                   y == rect.y &&
                   w == rect.w &&
                   h == rect.h;
        }

        public override int GetHashCode()
        {
            var hashCode = -121286848;
            hashCode = hashCode * -1521134295 + x.GetHashCode();
            hashCode = hashCode * -1521134295 + y.GetHashCode();
            hashCode = hashCode * -1521134295 + w.GetHashCode();
            hashCode = hashCode * -1521134295 + h.GetHashCode();
            return hashCode;
        }

        public int x = 0, y = 0, w = 0, h = 0;
    }
}
