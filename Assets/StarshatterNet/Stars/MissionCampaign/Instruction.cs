﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Instruction.h/Instruction.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Instruction (NavPoint / Order / Objective) class declaration
*/
using System;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning RadioMessage class is still in development and is not recommended for production.
    public class Instruction : SimObserver
    {
        [Obsolete("Check that. It has elements equal to RadioMessage.ACTION")]
        public enum ACTION
        {
            VECTOR,
            LAUNCH,
            DOCK,
            RTB,

            DEFEND,
            ESCORT,
            PATROL,
            SWEEP,
            INTERCEPT,
            STRIKE,     // ground attack
            ASSAULT,    // starship attack
            RECON,

            //RECALL,
            //DEPLOY,

            NUM_ACTIONS
        };

        public enum STATUS
        {
            PENDING,
            ACTIVE,
            SKIPPED,
            ABORTED,
            FAILED,
            COMPLETE,

            NUM_STATUS
        };

        public enum FORMATION
        {
            DIAMOND,
            SPREAD,
            BOX,
            TRAIL,

            NUM_FORMATIONS
        };

        public enum PRIORITY
        {
            PRIMARY = 1,
            SECONDARY,
            BONUS
        };

        public Instruction(ACTION act, string tgt)
        {
            region = null; action = act; formation = 0; tgt_name = tgt;
            status = STATUS.PENDING; speed = 0; target = null; emcon = 0; wep_free = false;
            priority = PRIORITY.PRIMARY; farcast = 0; hold_time = 0;
        }
        public Instruction(string rgn, Point loc, ACTION act = ACTION.VECTOR)
        {
            region = null; action = act; formation = 0;
            status = STATUS.PENDING; speed = 0; target = null; emcon = 0; wep_free = false;
            priority = PRIORITY.PRIMARY; farcast = 0; hold_time = 0;


            rgn_name = rgn;
            rloc.SetBaseLocation(loc);
            rloc.SetDistance(0);
        }

        public Instruction(SimRegion rgn, Point loc, ACTION act = ACTION.VECTOR)
        {
            region = rgn; action = act; formation = 0;
            status = STATUS.PENDING; speed = 0; target = null; emcon = 0; wep_free = false;
            priority = PRIORITY.PRIMARY; farcast = 0; hold_time = 0;


            rgn_name = region.Name();
            rloc.SetBaseLocation(loc);
            rloc.SetDistance(0);
        }
        public Instruction(Instruction instr)
        {
            region = instr.region; rgn_name = instr.rgn_name;
            rloc = instr.rloc; action = instr.action;
            formation = instr.formation; status = instr.status; speed = instr.speed;
            target = null; tgt_name = instr.tgt_name; tgt_desc = instr.tgt_desc;
            emcon = instr.emcon; wep_free = instr.wep_free;
            priority = instr.priority; farcast = instr.farcast;
            hold_time = instr.hold_time;


            SetTarget(instr.target);
        }
        // ~Instruction();

        //public Instruction& operator = (  Instruction& n);

        // accessors:
        public static string ActionName(ACTION a)
        {
            switch (a)
            {
                case ACTION.VECTOR: return "Vector";
                case ACTION.LAUNCH: return "Launch";
                case ACTION.DOCK: return "Dock";
                case ACTION.RTB: return "RTB";

                case ACTION.DEFEND: return "Defend";
                case ACTION.ESCORT: return "Escort";
                case ACTION.PATROL: return "Patrol";
                case ACTION.SWEEP: return "Sweep";
                case ACTION.INTERCEPT: return "Intercept";
                case ACTION.STRIKE: return "Strike";
                case ACTION.ASSAULT: return "Assault";
                case ACTION.RECON: return "Recon";

                default: return "Unknown";
            }
        }
        public static string StatusName(STATUS s)
        {
            switch (s)
            {
                case STATUS.PENDING: return "Pending";
                case STATUS.ACTIVE: return "Active";
                case STATUS.SKIPPED: return "Skipped";
                case STATUS.ABORTED: return "Aborted";
                case STATUS.FAILED: return "Failed";
                case STATUS.COMPLETE: return "Complete";

                default: return "Unknown";
            }
        }
        public static string FormationName(FORMATION f)
        {
            switch (f)
            {
                case FORMATION.DIAMOND: return "Diamond";
                case FORMATION.SPREAD: return "Spread";
                case FORMATION.BOX: return "Box";
                case FORMATION.TRAIL: return "Trail";

                default: return "Unknown";
            }
        }
        public static string PriorityName(PRIORITY p)
        {
            switch (p)
            {
                case PRIORITY.PRIMARY: return "Primary";
                case PRIORITY.SECONDARY: return "Secondary";
                case PRIORITY.BONUS: return "Bonus";

                default: return "Unknown";
            }
        }

        public string RegionName() { return rgn_name; }
        public SimRegion Region() { return region; }
        public Point Location()
        {
            return this.rloc.Location();
        }
        public RLoc GetRLoc() { return rloc; }
        public void SetRLoc(RLoc rl) { rloc = rl; }
        public RLoc RLoc { get { return rloc; } set { rloc = value; } }
        public Instruction.ACTION Action() { return action; }
        public STATUS Status() { return status; }
        public FORMATION Formation() { return formation; }
        public int Speed() { return speed; }
        public int EMCON() { return emcon; }
        public bool WeaponsFree() { return wep_free; }
        public PRIORITY Priority() { return priority; }
        public int Farcast() { return farcast; }
        public double HoldTime() { return hold_time; }

        public string TargetName() { return tgt_name; }
        public string TargetDesc() { return tgt_desc; }
        public SimObject GetTarget()
        {
            if (target == null && tgt_name.Length > 0)
            {
                Sim sim = Sim.GetSim();

                if (sim != null)
                {
                    Ship s = sim.FindShip(tgt_name, rgn_name);

                    if (s != null)
                    {
                        target = s;
                        Observe(target);
                    }
                }
            }

            return target;
        }

        public void Evaluate(Ship ship)
        {
            Sim sim = Sim.GetSim();

            switch (action)
            {
                case ACTION.VECTOR:
                    break;

                case ACTION.LAUNCH:
                    if (ship.GetFlightPhase() == OP_MODE.ACTIVE)
                        SetStatus(STATUS.COMPLETE);
                    break;

                case ACTION.DOCK:
                case ACTION.RTB:
                    if (sim.GetPlayerShip() == ship &&
                            (ship.GetFlightPhase() == OP_MODE.DOCKING ||
                                ship.GetFlightPhase() == OP_MODE.DOCKED))
                        SetStatus(STATUS.COMPLETE);
                    else if (ship.Integrity() < 1)
                        SetStatus(STATUS.FAILED);
                    break;

                case ACTION.DEFEND:
                case ACTION.ESCORT:
                    {
                        bool found = false;
                        bool safe = true;

                        foreach (Element e in sim.GetElements())
                        {
                            if (found) break;

                            if (e.IsFinished() || e.IsSquadron())
                                continue;

                            if (e.Name() == tgt_name ||
                                    (e.GetCommander() != null && e.GetCommander().Name() == tgt_name))
                            {

                                found = true;

                                for (int i = 0; i < e.NumShips(); i++)
                                {
                                    Ship s = e.GetShip(i + 1);

                                    if (s != null && s.Integrity() < 1)
                                        SetStatus(STATUS.FAILED);
                                }

                                if (status == STATUS.PENDING)
                                {
                                    // if the element had a flight plan, and all nav points
                                    // have been addressed, then the element is safe
                                    if (e.FlightPlanLength() > 0)
                                    {
                                        if (e.GetNextNavPoint() == null)
                                            SetStatus(STATUS.COMPLETE);
                                        else
                                            safe = false;
                                    }
                                }
                            }
                        }

                        if (status == STATUS.PENDING && safe &&
                                sim.GetPlayerShip() == ship &&
                                (ship.GetFlightPhase() == OP_MODE.DOCKING ||
                                    ship.GetFlightPhase() == OP_MODE.DOCKED))
                        {
                            SetStatus(STATUS.COMPLETE);
                        }
                    }
                    break;

                case ACTION.PATROL:
                case ACTION.SWEEP:
                    {
                        sim = Sim.GetSim();
                        bool alive = false;

                        foreach (Element e in sim.GetElements())
                        {
                            if (e.IsFinished() || e.IsSquadron())
                                continue;

                            if (e.GetIFF() != 0 && e.GetIFF() != ship.GetIFF())
                            {
                                for (int i = 0; i < e.NumShips(); i++)
                                {
                                    Ship s = e.GetShip(i + 1);

                                    if (s != null && s.Integrity() >= 1)
                                        alive = true;
                                }
                            }
                        }

                        if (status == STATUS.PENDING && !alive)
                        {
                            SetStatus(STATUS.COMPLETE);
                        }
                    }
                    break;

                case ACTION.INTERCEPT:
                case ACTION.STRIKE:
                case ACTION.ASSAULT:
                    {
                        sim = Sim.GetSim();
                        bool alive = false;

                        foreach (Element e in sim.GetElements())
                        {

                            if (e.IsFinished() || e.IsSquadron())
                                continue;

                            if (e.Name() == tgt_name)
                            {
                                for (int i = 0; i < e.NumShips(); i++)
                                {
                                    Ship s = e.GetShip(i + 1);

                                    if (s != null && s.Integrity() >= 1)
                                        alive = true;
                                }
                            }
                        }

                        if (status == STATUS.PENDING && !alive)
                        {
                            SetStatus(STATUS.COMPLETE);
                        }
                    }
                    break;

                case ACTION.RECON:
                    break;

                default:
                    break;
            }
        }
        public string GetShortDescription()
        {
            string desc;

            switch (action)
            {
                case ACTION.VECTOR:
                    if (farcast != 0)
                        desc = string.Format(Game.GetText("instr.short.farcast"), rgn_name);
                    else
                        desc = string.Format(Game.GetText("instr.short.vector"), rgn_name);
                    break;

                case ACTION.LAUNCH:
                    desc = string.Format(Game.GetText("instr.short.launch"), tgt_name);
                    break;

                case ACTION.DOCK:
                    desc = string.Format(Game.GetText("instr.short.dock"), tgt_name);
                    break;

                case ACTION.RTB:
                    desc = string.Format(Game.GetText("instr.short.return-to-base"));
                    break;

                case ACTION.DEFEND:
                    if (priority == PRIORITY.PRIMARY)
                    {
                        desc = string.Format(Game.GetText("instr.short.defend"), ActionName(action), tgt_desc);
                    }
                    else
                    {
                        desc = string.Format(Game.GetText("instr.short.protect"), tgt_desc);
                    }
                    break;

                case ACTION.ESCORT:
                    if (priority == PRIORITY.PRIMARY)
                    {
                        desc = string.Format(Game.GetText("instr.short.escort"), ActionName(action), tgt_desc);
                    }
                    else
                    {
                        desc = string.Format(Game.GetText("instr.short.protect"), tgt_desc);
                    }
                    break;

                case ACTION.PATROL:
                    desc = string.Format(Game.GetText("instr.short.patrol"),
                    tgt_desc,
                    rgn_name);
                    break;

                case ACTION.SWEEP:
                    desc = string.Format(Game.GetText("instr.short.sweep"),
                    tgt_desc,
                    rgn_name);
                    break;

                case ACTION.INTERCEPT:
                    desc = string.Format(Game.GetText("instr.short.intercept"), tgt_desc);
                    break;

                case ACTION.STRIKE:
                    desc = string.Format(Game.GetText("instr.short.strike"), tgt_desc);
                    break;

                case ACTION.ASSAULT:
                    desc = string.Format(Game.GetText("instr.short.assault"), tgt_desc);
                    break;

                case ACTION.RECON:
                    desc = string.Format(Game.GetText("instr.short.recon"), tgt_desc);
                    break;

                default:
                    desc = string.Format("{0}", ActionName(action));
                    break;
            }

            if (status != STATUS.PENDING)
            {
                desc += " - ";
                desc += Game.GetText(StatusName(status));
            }

            return desc;
        }
        public string GetDescription()
        {
            string desc;

            switch (action)
            {
                case ACTION.VECTOR:
                    if (farcast != 0)
                        desc = string.Format(Game.GetText("instr.long.farcast"), rgn_name);
                    else
                        desc = string.Format(Game.GetText("instr.long.vector"), rgn_name);
                    break;

                case ACTION.LAUNCH:
                    desc = string.Format(Game.GetText("instr.long.launch"), tgt_name);
                    break;

                case ACTION.DOCK:
                    desc = string.Format(Game.GetText("instr.long.dock"), tgt_name);
                    break;

                case ACTION.RTB:
                    desc = string.Format(Game.GetText("instr.long.return-to-base"));
                    break;

                case ACTION.DEFEND:
                    if (priority == PRIORITY.PRIMARY)
                    {
                        desc = string.Format(Game.GetText("instr.long.defend"), ActionName(action), tgt_desc);
                    }
                    else
                    {
                        desc = string.Format(Game.GetText("instr.long.protect"), tgt_desc);
                    }
                    break;

                case ACTION.ESCORT:
                    if (priority == PRIORITY.PRIMARY)
                    {
                        desc = string.Format(Game.GetText("instr.long.escort"), ActionName(action), tgt_desc);
                    }
                    else
                    {
                        desc = string.Format(Game.GetText("instr.long.protect"), tgt_desc);
                    }
                    break;

                case ACTION.PATROL:
                    desc = string.Format(Game.GetText("instr.long.patrol"),
                    tgt_desc,
                    rgn_name);
                    break;

                case ACTION.SWEEP:
                    desc = string.Format(Game.GetText("instr.long.sweep"),
                    tgt_desc,
                    rgn_name);
                    break;

                case ACTION.INTERCEPT:
                    desc = string.Format(Game.GetText("instr.long.intercept"), tgt_desc);
                    break;

                case ACTION.STRIKE:
                    desc = string.Format(Game.GetText("instr.long.strike"), tgt_desc);
                    break;

                case ACTION.ASSAULT:
                    desc = string.Format(Game.GetText("instr.long.assault"), tgt_desc);
                    break;

                case ACTION.RECON:
                    desc = string.Format(Game.GetText("instr.long.recon"), tgt_desc);
                    break;

                default:
                    desc = string.Format("{0}", ActionName(action));
                    break;
            }

            if (status != STATUS.PENDING)
            {
                desc += " - ";
                desc += Game.GetText(StatusName(status));
            }

            return desc;
        }

        // mutators:
        public void SetRegion(SimRegion r) { region = r; }
        public void SetLocation(Point l)
        {
            rloc.SetBaseLocation(l);
            rloc.SetReferenceLoc(null);
            rloc.SetDistance(0);
        }
        public void SetAction(Instruction.ACTION s) { action = s; }
        public void SetStatus(STATUS s)
        {
            status = s;
        }

        public void SetFormation(FORMATION s) { formation = s; }
        public void SetSpeed(int s) { speed = s; }
        public void SetEMCON(int e) { emcon = e; }
        public void SetWeaponsFree(bool f) { wep_free = f; }
        public void SetPriority(PRIORITY p) { priority = p; }
        public void SetFarcast(int f) { farcast = f; }
        public void SetHoldTime(double t) { hold_time = t; }

        public void SetTarget(string n)
        {
            if (!string.IsNullOrEmpty(n) && tgt_name != n)
            {
                tgt_name = n;
                tgt_desc = n;

                if (target != null)
                    target = null;
            }
        }
        public void SetTarget(SimObject s)
        {
            if (s != null && target != s)
            {
                tgt_name = s.Name();
                target = s;
                Observe(target);
            }
        }
        public void SetTargetDesc(string d)
        {
            if (!string.IsNullOrEmpty(d))
                tgt_desc = d;
        }
        public void ClearTarget()
        {
            if (target != null)
            {
                target = null;
                tgt_name = "";
                tgt_desc = "";
            }
        }

        public override bool Update(SimObject obj)
        {
            if (target == obj)
                target = null;

            return base.Update(obj);
        }
        public override string GetObserverName()
        {
            return "Instruction";
        }


        protected string rgn_name;
        protected SimRegion region;
        protected RLoc rloc = new RLoc();
        protected Instruction.ACTION action;
        protected FORMATION formation;
        protected STATUS status;
        protected int speed;

        protected string tgt_name;
        protected string tgt_desc;
        protected SimObject target;
        protected int emcon;
        protected bool wep_free;
        protected PRIORITY priority;
        protected int farcast;

        protected double hold_time;
    }
}