﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars.exe
    ORIGINAL FILE:      StarSystem.h/StarSystem.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Various heavenly bodies
*/
using System;
using DigitalRune.Mathematics.Algebra;
using UnityEngine;

namespace StarshatterNet.Stars.StarSystems
{
    public class Star
    {
        [Serializable]
        public enum SPECTRAL_CLASS
        {
            BLACK_HOLE,
            WHITE_DWARF,
            RED_GIANT,
            O,
            B,
            A,
            F,
            G,
            K,
            M
        }

        public Star(string n, Vector3F l, SPECTRAL_CLASS s)
        {
            this.name = n;
            this.loc = l;
            this.seq = s;
        }


        //public int operator == (const Star& s)     const { return name == s.name; }
        public static bool operator ==(Star left, Star right)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.name == right.name;
        }
        public static bool operator !=(Star left, Star right)
        {
            return !(left == right);
        }
        public override bool Equals(object obj)
        {
            return (obj is Star) && (this == (Star)obj);
        }

        public bool Equals(Star s)
        {
            // Return true if the fields match:
            return this.name == s.name;
        }

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        // accessors:
        public string Name() { return name; }
        public Vector3F Location() { return loc; }
        public SPECTRAL_CLASS Sequence() { return seq; }
        public Color GetColor()
        {
            return GetColor(this.seq);
        }

        public int GetSize()
        {
            return GetSize(this.seq);
        }

        public static Color GetColor(SPECTRAL_CLASS spectral_class)
        {
            switch (spectral_class)
            {
                case SPECTRAL_CLASS.O: return new Color32(128, 128, 255, 255);
                case SPECTRAL_CLASS.B: return new Color32(192, 192, 255, 255);
                case SPECTRAL_CLASS.A: return new Color32(220, 220, 255, 255);
                case SPECTRAL_CLASS.F: return new Color32(255, 255, 255, 255);
                case SPECTRAL_CLASS.G: return new Color32(255, 255, 128, 255);
                case SPECTRAL_CLASS.K: return new Color32(255, 192, 100, 255);
                case SPECTRAL_CLASS.M: return new Color32(255, 100, 100, 255);

                case SPECTRAL_CLASS.RED_GIANT: return new Color32(255, 80, 80, 255);
                case SPECTRAL_CLASS.WHITE_DWARF: return new Color32(255, 255, 255, 255);
                case SPECTRAL_CLASS.BLACK_HOLE: return new Color32(0, 0, 0, 255);
            }

            return Color.white;
        }
        public static int GetSize(SPECTRAL_CLASS spectral_class)
        {
            switch (spectral_class)
            {
                case SPECTRAL_CLASS.O: return 4;
                case SPECTRAL_CLASS.B: return 4;
                case SPECTRAL_CLASS.A: return 3;
                case SPECTRAL_CLASS.F: return 3;
                case SPECTRAL_CLASS.G: return 2;
                case SPECTRAL_CLASS.K: return 2;
                case SPECTRAL_CLASS.M: return 1;

                case SPECTRAL_CLASS.RED_GIANT: return 4;
                case SPECTRAL_CLASS.WHITE_DWARF: return 1;
                case SPECTRAL_CLASS.BLACK_HOLE: return 3;
            }

            return 3;
        }


        protected string name;
        protected Vector3F loc;
        protected SPECTRAL_CLASS seq;
    }
}
