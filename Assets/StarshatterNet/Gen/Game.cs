﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Game.h/Game.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos
*/
using System;
using System.IO;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Simulator;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars
{
#warning Game class is still in development and is not recommended for production.
    public class Game
    {
        public enum STATUS { OK, RUN, EXIT, PANIC, INIT_FAILED, TOO_MANY };

        public Game()
        {
            if (game == null)
            {
                game = this;
                video_settings = new VideoSettings();
            }
            status = STATUS.OK;
        }
        public virtual bool Init(Camera screen, GameObject p)
        {
            this.camera = screen;
            this.gameObj = p;

            status = STATUS.OK;

            ErrLogger.PrintLine("  Initializing Game");

            if (!InitApplication())
            {  // Initialize shared things
                ErrLogger.PrintLine("Could not initialize the application.");
                status = STATUS.INIT_FAILED;
            }

            if (status == STATUS.OK && video_settings == null)
            {
                ErrLogger.PrintLine("No video settings specified");
                status = STATUS.INIT_FAILED;
            }

            if (status == STATUS.OK)
            {
                // TODO
                // Print platform, OS, version, etc information 
                // MachineInfo.DescribeMachine();
                // for moee info https://docs.unity3d.com/ScriptReference/RuntimePlatform.html

                // Ouput the current Application into the Console
                // To set the CompayName, Product Name  or Version number in Unity, go to 
                // Edit >Project Settings>Player and open the Other Settings tab.
                ErrLogger.PrintLine("Application Company Name : {0}", Application.companyName);
                ErrLogger.PrintLine("Application Product Name : {0}", Application.productName);
                ErrLogger.PrintLine("Application Version : {0}", Application.version);
                ErrLogger.PrintLine("Application Unity Version : {0}", Application.unityVersion);
                ErrLogger.PrintLine("Application is running on : {0}", Application.platform);
            }

            if (status == STATUS.OK)
            {
                ErrLogger.PrintLine("  Initializing content...");
                InitContent();

                ErrLogger.PrintLine("  Initializing game...");
                if (!InitGame())
                {
                    ErrLogger.PrintLine("Could not initialize the game.");
                    status = STATUS.INIT_FAILED;
                }
            }

            return status == STATUS.OK;
        }
        public virtual void Run()
        {
            status = STATUS.RUN;
            ErrLogger.PrintLine("+====================================================================+");
            ErrLogger.PrintLine("|                              RUN                                   |");
            ErrLogger.PrintLine("+====================================================================+");
        }
        public virtual void Exit()
        {
            ErrLogger.PrintLine("\n\n*** Game::Exit()");
            status = STATUS.EXIT;
        }
        public virtual void Activate(bool f)
        {
            active = f;
        }
        public virtual void Pause(bool f)
        {
            paused = f;
            // TODO Pause sounds
        }

        protected void FlushKeys()
        {
            Keyboard.FlushKeys();
        }

        public STATUS Status() { return status; }


        private const string screenshotPrefix = "Screenshot-";
        public virtual void ScreenCapture(string name = null)
        {
            ulong shot_num = 0;

            if (server) return;

            if (string.IsNullOrEmpty(name) && (real_time - last_shot) < 1000)
                return;

            var path = Application.persistentDataPath;
            DirectoryInfo di = new DirectoryInfo(path);
            // try not to overwrite existing screen shots...
            foreach (var fi in di.GetFiles(screenshotPrefix + "*.png"))
            {
                var fn = Path.GetFileNameWithoutExtension(fi.Name).Replace(screenshotPrefix, "");
                ulong n;
                if (ulong.TryParse(fn, out n) && shot_num <= n)
                    shot_num = n + 1;
            }

            string filename;
            if (!string.IsNullOrEmpty(name))
                filename = path + "/" + name;
            else
                filename = path + "/" + string.Format(screenshotPrefix + "{ 0:D5}.png", shot_num);

            ErrLogger.PrintLine("Screenshot file {0}", filename);
            UnityEngine.ScreenCapture.CaptureScreenshot(filename, 3);
            last_shot = real_time;
        }

        public virtual void AVICapture(string fname = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The real time in seconds since the game started (Read Only).
        /// </summary>
        /// <returns></returns>
        public static ulong RealTime()
        {
            return real_time;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static double GameTime()
        {
            return game_time * 0.001;
        }
        public static double GUITime()
        {
            if (game != null)
                return game.gui_seconds;

            return 0;
        }

        internal static int GammaLevel()
        {
            return 0;// throw new NotImplementedException();
        }

        /// <summary>
        /// The scale at which time passes. This can be used for slow motion effects.
        /// When timeScale is 1.0 time passes as fast as realtime.When timeScale is 0.5 time passes 2x slower than realtime.
        /// </summary>
        public static double TimeCompression()
        {
            return UnityEngine.Time.timeScale;
        }

        /// <summary>
        /// The scale at which time passes. This can be used for slow motion effects.
        /// When timeScale is 1.0 time passes as fast as realtime.When timeScale is 0.5 time passes 2x slower than realtime.
        /// </summary>
        /// <param name="comp">The new scale</param>
        public static void SetTimeCompression(double comp)
        {
            Time.timeScale = (float)comp;
        }

        /// <summary>
        /// The completion time in seconds since the last frame (Read Only).
        /// This property provides the time between the current and previous frame.
        /// </summary>
        /// <returns></returns>
        public static double DeltaTime()
        {
            return Time.deltaTime;
        }
        public static double Frame()
        {
            return frame_number;
        }
        public static double FrameRate()
        {
            if (game != null)
                return game.fps;

            return 0;
        }
        public static double FrameTime()
        {
            if (game != null)
                return game.seconds;

            return 0;
        }
        public static void ResetGameTime()
        {
            game_time = 0;
        }
        public static void SkipGameTime(double seconds)
        {
            if (seconds > 0)
                game_time += (ulong)(seconds * 1000);
        }

        public static void SetGammaLevel(int g)
        {
            if (game != null)
            {
                game.gamma = g;
#if TODO
                if (game.video != null)
                    game.video.SetGammaLevel(g);
#endif
            }
        }

        public static Game GetInstance()
        {
            if (game == null) game = new Game();
            return game;
        }
        public static Video  GetVideo()
        {
            if (game != null)
                return game.video;

            return null;
        }
        public static Color GetScreenColor()
        {
            if (game != null)
                return game.screen_color;

            return Color.black;
        }
        public static void SetScreenColor(Color c)
        {
            if (game != null)
            {
                game.screen_color = c;
                if (game.screen != null)
                    ;
#if TODO
                    game.screen.SetBackgroundColor(c);
#endif
            }
        }

        public virtual int GetScreenWidth() { return 0; /* Not yet implemented */ }
        public virtual int GetScreenHeight() { return 0; /* Not yet implemented */ }

        public static bool Active() { return active; }
        public static bool Paused() { return paused; }
        public static bool Server() { return server; }
        public static bool ShowMouse() { return show_mouse; }

        public static void UseLocale(string locale)
        {
            throw new NotImplementedException();
        }

        public static string GetText(string key)
        {
            if (game != null && game.content != null && game.content.IsLoaded())
                return game.content.GetText(key);

            return key;
        }

        public virtual bool GameLoop()
        {
            if (active && !paused)
            {
                UpdateWorld();
                GameState();
            }

            real_time = (ulong)(Time.realtimeSinceStartup * 1000);
            frame_number++;

            return true;
        }
        protected virtual void UpdateWorld()
        {
            frameCount++;
            deltaTime += Time.unscaledDeltaTime;
            gui_seconds = Time.unscaledDeltaTime;
            if (deltaTime > 1.0 / updateRate)
            {
                fps = frameCount / deltaTime;
                frameCount = 0;
                deltaTime -= 1.0 / updateRate;
            }

            seconds = Time.deltaTime;
            game_time += (ulong)(Time.deltaTime * 1000);
            if (world != null)
                world.ExecFrame(seconds);
        }
        public virtual void GameState()
        {
        }
        public virtual void UpdateScreen()
        { }
        public virtual void CollectStats()
        { }

        public virtual bool InitApplication()
        {
            return true;
        }
        public virtual bool InitInstance(int n)
        {
            ErrLogger.PrintLine("  Instance initialized.");
            return true;
        }
        public virtual bool InitContent()
        {
            content = ContentBundle.Instance;
            return true;
        }
        public virtual bool InitGame()
        {
            if (server)
            {
                ErrLogger.PrintLine("  InitGame() - server mode.");
            }
            else
            {
                if (!InitVideo() || video == null  /*TODO || video.Status() != Video.VIDEO_OK*/)
                {
                    ErrLogger.PrintLine("PANIC: Could not create the Video Interface.");
                    return false;
                }
                screen = new Screen(video);
                ErrLogger.PrintLine("  Created screen object.\n");

                if (!screen.SetBackgroundColor(Color.black))
                    ErrLogger.PrintLine("  WARNING: could not set video background color to Black");
                screen.ClearAllFrames(true);

                //TODO video.SetGammaLevel(gamma);

                ErrLogger.PrintLine("  Established requested video parameters.\n");

            }

            return true;
        }
        public static int GetKey() { return 0; }
        public static int GetKeyPlus(ref int key, ref int shift) { return 0; }
        public virtual bool InitVideo()
        {
            if (server) return true;
            video = Video.GetVideo();

            return video != null;

        }


        protected Camera camera;
        protected GameObject gameObj;

        protected ContentBundle content;
        protected Universe world;
        protected Video video;
        protected VideoSettings video_settings;
        protected Screen screen;
        //protected SoundCard soundcard;
        protected int gamma;
        protected int max_tex_size;

        protected string app_name = "No Name";
        protected string title_text = "No Name";

        protected STATUS status;
        protected int exit_code;

        protected static Game game = null;

        protected static bool active = false;
        protected static bool paused = false;
        protected static bool server = false;
        protected static bool show_mouse = false;
        protected static ulong base_game_time = 0;
        protected static ulong real_time = 0;
        protected static ulong game_time = 0;
        protected static ulong time_comp = 1;
        protected static ulong frame_number = 0;

        protected double gui_seconds;

        protected double seconds;
        protected double deltaTime = 0.0;
        protected double fps = 0.0;
        protected ulong frameCount = 0;
        protected int frame_count;
        protected int frame_count0;
        protected int frame_time;
        protected int frame_time0;
        protected const double updateRate = 4.0;  // 4 updates per sec.

        protected Color screen_color;


        protected static double max_frame_length = MAX_FRAME_TIME_NORMAL;
        protected static double min_frame_length = MIN_FRAME_TIME_NORMAL;

        protected const double MAX_FRAME_TIME_NORMAL = 1.0 / 5.0;
        protected const double MIN_FRAME_TIME_NORMAL = 1.0 / 60.0;

        protected static ulong last_shot = 0;
        protected AviFile avi_file = null;

        internal static bool DisplayModeSupported(int v1, int v2, int v3)
        {
            throw new NotImplementedException();
        }

        public static int MaxTexSize()
        {
            if (game != null && game.video != null)
            {
                int max_vid_size = game.video.MaxTexSize();
                return max_vid_size < game.max_tex_size ?
                max_vid_size : game.max_tex_size;
            }
            else if (Video.GetInstance() != null)
            {
                return Video.GetInstance().MaxTexSize();
            }

            return 256;
        }
    }
}