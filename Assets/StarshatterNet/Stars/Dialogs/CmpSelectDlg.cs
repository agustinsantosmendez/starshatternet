﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmpSelectDlg.h/CmpSelectDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Select Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using DWORD = System.UInt32;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmpSelectDlg : FormWindow
    {
        public CmpSelectDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
             base(s, 0, 0, s.Width(), s.Height(), c, p, "CmpSelectDlg")
        {
            manager = mgr;
            lst_campaigns = null; btn_new = null; btn_saved = null; btn_delete = null;
            btn_accept = null; btn_cancel = null; description = null; stars = null; campaign = null;
            selected_mission = -1; show_saved = false; loading = false;
            loaded = false; //hproc = 0;

            stars = Starshatter.GetInstance();
            select_msg = Game.GetText("CmpSelectDlg.select_msg");
            Init(def);
        }
        //public virtual ~CmpSelectDlg();
        public override void RegisterControls()
        {
            btn_new = (Button)FindControl(100);
            btn_saved = (Button)FindControl(101);
            btn_delete = (Button)FindControl(102);
            btn_accept = (Button)FindControl(1);
            btn_cancel = (Button)FindControl(2);

            if (btn_new != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_new, OnNew);

            if (btn_saved != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_saved, OnSaved);

            if (btn_delete != null)
            {
                btn_delete.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_delete, OnDelete);
                REGISTER_CLIENT(ETD.EID_USER_1, btn_delete, OnConfirmDelete);
            }

            if (btn_accept != null)
            {
                btn_accept.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_accept, OnAccept);
            }

            if (btn_cancel != null)
            {
                btn_cancel.SetEnabled(true);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_cancel, OnCancel);
            }

            description = FindControl(200);

            lst_campaigns = (ListBox)FindControl(201);

            if (lst_campaigns != null)
                REGISTER_CLIENT(ETD.EID_SELECT, lst_campaigns, OnCampaignSelect);

            ShowNewCampaigns();
        }
        public override void Show()
        {
            base.Show();
            ShowNewCampaigns();
        }

        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                if (btn_accept != null && btn_accept.IsEnabled())
                    OnAccept(null, null);
            }

            //TODO AutoThreadSync a(sync);

            ShowCampaignInfo();
        }
        public virtual void ShowCampaignInfo()
        {
            if (loaded)
            {
                loaded = false;

                if (btn_cancel != null)
                    btn_cancel.SetEnabled(true);
                int verdanaSize = 18;
                FontItem fontitem = FontMgr.Find("Verdana");
                if (fontitem != null) verdanaSize = fontitem.size;

                int Limerick12 = 14;
                fontitem = FontMgr.Find("Limerick12");
                if (fontitem != null) Limerick12 = fontitem.size;

                if (description != null && btn_accept != null)
                {
                    if (campaign != null)
                    {
                        Campaign.SelectCampaign(campaign.Name());

                        if (load_index >= 0)
                        {
                            if (lst_campaigns != null)
                            {
                                images[load_index] = new Bitmap(campaign.GetImage(1));
                                lst_campaigns.SetItemImage(load_index, images[load_index].Texture);
                            }

                            description.SetText("<size=" + verdanaSize + "><color=#ffffffff><b>" +
                                                campaign.Name() +
                                                "</b></color></size>" +
                                                 "<size=" + Limerick12 + ">\n\n" +
                                                 "<color=#ffff80ff>" +
                                                Game.GetText("CmpSelectDlg.scenario") +
                                                 "</color>\n\t<color=#ffffffff>" +
                                                campaign.Description() +
                                                "</color></size>");
                        }
                        else
                        {
                            string time_buf;
                            string score_buf;

                            double t = campaign.GetLoadTime() - campaign.GetStartTime();
                            FormatUtil.FormatDayTime(out time_buf, t);

                            score_buf = string.Format("{0}", campaign.GetPlayerTeamScore());

                            string desc = "<size=" + verdanaSize + "><color=#ffffffff><b>" +
                                            campaign.Name() +
                                            "</b></color></size>" +
                                            "<size=" + Limerick12 + ">\n\n" +
                                            "<color=#ffff80ff>" +
                                            Game.GetText("CmpSelectDlg.scenario") +
                                            "</color>\n\t<color=#ffffffff>" +
                                            campaign.Description() +
                                            "</color>" +
                                            "\n\n<color=#ffff80ff>" +
                                            Game.GetText("CmpSelectDlg.campaign-time") +
                                            "</color><color=#ffffffff>\n\t" +
                                            time_buf +
                                            "</color>\n\n<color=#ffff80ff>" +
                                            Game.GetText("CmpSelectDlg.assignment") +
                                            "</color>\n\t<color=#ffffffff>";

                            if (campaign.GetPlayerGroup() != null)
                                desc += campaign.GetPlayerGroup().GetDescription();
                            else
                                desc += "n/a";

                            desc += "</color>\n\n<color=#ffff80ff>" +
                            Game.GetText("CmpSelectDlg.team-score") +
                            "</color>\n\t<color=#ffffffff>" +
                            score_buf +
                            "</color></size>";

                            description.SetText(desc);
                        }

                        btn_accept.SetEnabled(true);

                        if (btn_delete != null)
                            btn_delete.SetEnabled(show_saved);
                    }
                    else
                    {
                        description.SetText(select_msg);
                        btn_accept.SetEnabled(true);
                    }
                }
            }
        }
        public virtual bool CanClose() { throw new NotImplementedException(); }

        // Operations:
        public virtual void OnCampaignSelect(ActiveWindow obj, AWEvent evnt)
        {
            Debug.Log("CmpSelectDlg.OnCampaignSelect.");
            if (description != null && lst_campaigns != null)
            {
                // TODO AutoThreadSync a(sync);

                if (loading)
                {
                    description.SetText(Game.GetText("CmpSelectDlg.already-loading"));
                    Button.PlaySound(Button.SOUNDS.SND_REJECT);
                    return;
                }

                load_index = -1;
                load_file = "";

                Player player = Player.GetCurrentPlayer();
                if (player == null) return;

                // NEW CAMPAIGN:
                if (btn_new != null && btn_new.GetButtonState() != 0)
                {
                    List<Campaign> list = Campaign.GetAllCampaigns();

                    for (int i = 0; i < lst_campaigns.NumItems(); i++)
                    {
                        Campaign c = list[i];

                        // is campaign available?
                        // FULL GAME CRITERIA (based on player record):
                        if (c.GetCampaignId() <= 2 ||
                                player.HasCompletedCampaign(c.GetCampaignId() - 1))
                        {

                            if (lst_campaigns.IsSelected(i))
                            {
                                //images[i].CopyBitmap( c.GetImage(1));
                                images[i] = new Bitmap(c.GetImage(1));
                                lst_campaigns.SetItemImage(i, images[i].Texture);

                                // TODO AutoThreadSync a(sync);
                                load_index = i;
                            }
                            else
                            {
                                //images[i].CopyBitmap(c.GetImage(0));
                                images[i] = new Bitmap(c.GetImage(0));
                                lst_campaigns.SetItemImage(i, images[i].Texture);
                            }
                        }

                        // if not, then don't select
                        else
                        {
                            //images[i].CopyBitmap(*c.GetImage(2));
                            images[i] = new Bitmap(c.GetImage(2));
                            lst_campaigns.SetItemImage(i, images[i].Texture);

                            if (lst_campaigns.IsSelected(i))
                            {
                                description.SetText(select_msg);
                            }
                        }
                    }
                }

                // SAVED CAMPAIGN:StartLoadProc
                else
                {
                    int seln = lst_campaigns.GetSelection();

                    if (seln < 0)
                    {
                        description.SetText(select_msg);
                    }

                    else
                    {
                        load_index = -1;
                        load_file = lst_campaigns.GetItemText(seln);
                    }
                }

                if (btn_accept != null)
                    btn_accept.SetEnabled(false);
            }

            if (!loading && (load_index >= 0 || load_file.Length > 0))
            {
                if (btn_cancel != null)
                    btn_cancel.SetEnabled(false);

                StartLoadProc();
            }
        }
        public virtual void OnNew(ActiveWindow obj, AWEvent evnt)
        {
            ShowNewCampaigns();
        }

        public virtual void OnSaved(ActiveWindow obj, AWEvent evnt)
        {
            ShowSavedCampaigns();
        }

        public virtual void OnDelete(ActiveWindow obj, AWEvent evnt)
        {
            load_file = "";

            if (lst_campaigns != null)
            {
                int seln = lst_campaigns.GetSelection();

                if (seln < 0)
                {
                    description.SetText(select_msg);
                    btn_accept.SetEnabled(false);
                }

                else
                {
                    load_index = -1;
                    load_file = lst_campaigns.GetItemText(seln);
                }
            }

            if (load_file.Length > 0)
            {
                ConfirmDlg confirm = manager.GetConfirmDlg();
                if (confirm != null)
                {
                    string msg = string.Format(Game.GetText("CmpSelectDlg.are-you-sure"), load_file);
                    confirm.SetMessage(msg);
                    confirm.SetTitle(Game.GetText("CmpSelectDlg.confirm"));

                    manager.ShowConfirmDlg();
                }

                else
                {
                    OnConfirmDelete(this, evnt);
                }
            }

            ShowSavedCampaigns();
        }
        public virtual void OnConfirmDelete(ActiveWindow obj, AWEvent evnt)
        {
            if (load_file.Length > 0)
            {
                CampaignSaveGame.Delete(load_file);
            }

            ShowSavedCampaigns();
        }

        public virtual void OnAccept(ActiveWindow obj, AWEvent evnt)
        {
            // TODO AutoThreadSync a(sync);

            if (loading)
                return;

            // if this is to be a new campaign,
            // re-instaniate the campaign object
            if (btn_new.GetButtonState() != 0)
                Campaign.GetCampaign().Load();
            else
                Game.ResetGameTime();

            Mouse.Show(false);
            stars.SetGameMode(Starshatter.MODE.CLOD_MODE);
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            manager.ShowMenuDlg();
        }


        public virtual DWORD LoadProc() { throw new NotImplementedException(); }


        protected virtual void StartLoadProc()
        {
            ErrLogger.PrintLine("CmpSelectDlg.StartLoadProc. Load index: {0}", load_index);
            Campaign c = null;

            // NEW CAMPAIGN:
            if (load_index >= 0)
            {
                List<Campaign> list = Campaign.GetAllCampaigns();

                if (load_index < list.Count)
                {
                    c = list[load_index];
                    c.Load();
                }
            }
            else // SAVED CAMPAIGN:
            {
                CampaignSaveGame savegame = new CampaignSaveGame();
                savegame.Load(load_file);
                c = savegame.GetCampaign();
            }

            loading = false;
            loaded = true;
            campaign = c;

            if (lst_campaigns != null)
            {
                ShowCampaignInfo();
            }
        }
        protected virtual void StopLoadProc() { throw new NotImplementedException(); }
        protected virtual void ShowNewCampaigns()
        {
            //TODO AutoThreadSync a(sync);

            if (loading && description != null)
            {
                description.SetText(Game.GetText("CmpSelectDlg.already-loading"));
                Button.PlaySound(Button.SOUNDS.SND_REJECT);
                return;
            }

            if (btn_new != null)
                btn_new.SetButtonState(1);

            if (btn_saved != null)
                btn_saved.SetButtonState(0);

            if (btn_delete != null)
                btn_delete.SetEnabled(false);

            if (lst_campaigns != null)
            {
                images.Destroy();

                lst_campaigns.SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_PLAIN);
                lst_campaigns.SetLeading(0);
                lst_campaigns.ClearItems();
                lst_campaigns.SetLineHeight(100);

                Player player = Player.GetCurrentPlayer();
                if (player == null) return;

                foreach (Campaign c in Campaign.GetAllCampaigns())
                {
                    if (c.GetCampaignId() < (int)Campaign.CONSTANTS.SINGLE_MISSIONS)
                    {
                        Bitmap bmp = new Bitmap(c.GetImage(0));
                        images.Add(bmp);

                        int n = lst_campaigns.AddImage(bmp.Texture) - 1;
                        lst_campaigns.SetItemText(n, c.Name());

                        // if campaign is not available, show the grayed-out image

                        // FULL GAME CRITERIA (based on player record):
                        if (c.GetCampaignId() > 2 && c.GetCampaignId() < 10 &&
                                !player.HasCompletedCampaign(c.GetCampaignId() - 1))
                        {
                            images[n] = new Bitmap(c.GetImage(2));
                            lst_campaigns.SetItemImage(n, images[n].Texture);
                        }

                        // Two additional sequences of ten campaigns (10-19 and 20-29)
                        // for mod authors to use:
                        else if (c.GetCampaignId() >= 10 && c.GetCampaignId() < 30 &&
                                (c.GetCampaignId() % 10) != 0 &&
                                !player.HasCompletedCampaign(c.GetCampaignId() - 1))
                        {
                            images[n] = new Bitmap(c.GetImage(2));
                            lst_campaigns.SetItemImage(n, images[n].Texture);
                        }

                        // NOTE: Campaigns 10, 20, and 30-99 are always enabled if they exist!
                    }
                }
                lst_campaigns.RebuildListObjets();
            }

            if (description != null)
                description.SetText(select_msg);

            if (btn_accept != null)
                btn_accept.SetEnabled(false);

            show_saved = false;
        }
        protected virtual void ShowSavedCampaigns()
        {
            // TODO AutoThreadSync a(sync);

            if (loading && description != null)
            {
                description.SetText(Game.GetText("CmpSelectDlg.already-loading"));
                Button.PlaySound(Button.SOUNDS.SND_REJECT);
                return;
            }

            if (btn_new != null)
                btn_new.SetButtonState(0);

            if (btn_saved != null)
                btn_saved.SetButtonState(1);

            if (btn_delete != null)
                btn_delete.SetEnabled(false);

            if (lst_campaigns != null)
            {
                lst_campaigns.SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_FILLED_BOX);
                lst_campaigns.SetLeading(4);
                lst_campaigns.ClearItems();
                lst_campaigns.SetLineHeight(12);

                List<string> save_list;

                CampaignSaveGame.GetSaveGameList(out save_list);
                save_list.Sort();

                for (int i = 0; i < save_list.Count; i++)
                    lst_campaigns.AddItem(save_list[i]);

                save_list.Destroy();
            }

            if (description != null)
                description.SetText(select_msg);

            if (btn_accept != null)
                btn_accept.SetEnabled(false);

            show_saved = true;
        }


        protected MenuScreen manager;

        protected Button btn_new;
        protected Button btn_saved;
        protected Button btn_delete;
        protected Button btn_accept;
        protected Button btn_cancel;

        protected ListBox lst_campaigns;

        protected ActiveWindow description;

        protected Starshatter stars;
        protected Campaign campaign;
        protected int selected_mission;
        //protected HANDLE hproc;
        //protected ThreadSync sync;
        protected bool loading;
        protected bool loaded;
        protected string load_file;
        protected int load_index;
        protected bool show_saved;
        protected List<Bitmap> images = new List<Bitmap>();

        protected string select_msg;
    }
}