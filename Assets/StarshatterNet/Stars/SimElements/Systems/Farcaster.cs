﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Farcaster.h/Farcaster.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
 */
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
namespace StarshatterNet.Stars.SimElements
{
    public class Farcaster : ShipSystem, ISimObserver
    {
        public Farcaster(double cap, double rate)
            : base(CATEGORY.FARCASTER, 0, "Farcaster", 1, (float)cap, (float)cap, (float)rate)
        {
            name = Game.GetText("sys.farcaster");
            abrv = Game.GetText("sys.farcaster.abrv");
        }

        public Farcaster(Farcaster s) : base(s)
        {
            start_rel = s.start_rel;
            end_rel = s.end_rel;
            jumpship = null;
            cycle_time = s.cycle_time;
            active_state = QuantumDrive.ACTIVE_STATES.ACTIVE_READY;
            warp_fov = 1;
            no_dest = false;

            Mount(s);
            SetAbbreviation(s.Abbreviation());

            for (int i = 0; i < NUM_APPROACH_PTS; i++)
                approach_rel[i] = s.approach_rel[i];
        }

        // ~Farcaster();

        public const int NUM_APPROACH_PTS = 4;

        public override void ExecFrame(double seconds)
        {
            base.ExecFrame(seconds);

            if (ship != null && !no_dest)
            {
                if (dest == null)
                {
                    Element elem = ship.GetElement();

                    if (elem.NumObjectives() != 0)
                    {
                        Sim sim = Sim.GetSim();
                        Instruction obj = elem.GetObjective(0);

                        if (obj != null)
                            dest = sim.FindShip(obj.TargetName());
                    }

                    if (dest == null)
                        no_dest = true;
                }
                else
                {
                    if (dest.IsDying() || dest.IsDead())
                    {
                        dest = null;
                        no_dest = true;
                    }
                }
            }

            // if no destination, show red nav lights:
            if (no_dest)
                energy = 0.0f;

            if (active_state == QuantumDrive.ACTIVE_STATES.ACTIVE_READY && energy >= capacity &&
                ship != null && ship.GetRegion() != null && dest != null && dest.GetRegion() != null)
            {
                SimRegion rgn = ship.GetRegion();
                SimRegion dst = dest.GetRegion();

                jumpship = null;

                foreach (var s in rgn.Ships())
                {
                    if (s == ship || s.IsStatic() || s.WarpFactor() > 1)
                        continue;

                    Point delta = s.Location() - ship.Location();

                    // activate:
                    if (delta.Length < 1000)
                    {
                        active_state = QuantumDrive.ACTIVE_STATES.ACTIVE_PREWARP;
                        jumpship = s;
                        Observe(jumpship);
                        break;
                    }
                }
            }

            if (active_state == QuantumDrive.ACTIVE_STATES.ACTIVE_READY)
                return;

            if (ship != null)
            {
                bool warping = false;

                if (active_state == QuantumDrive.ACTIVE_STATES.ACTIVE_PREWARP)
                {
                    if (warp_fov < 5000)
                    {
                        warp_fov *= 1.5;
                    }
                    else
                    {
                        Jump();
                    }

                    warping = true;
                }

                else if (active_state == QuantumDrive.ACTIVE_STATES.ACTIVE_POSTWARP)
                {
                    if (warp_fov > 1)
                    {
                        warp_fov *= 0.75;
                    }
                    else
                    {
                        warp_fov = 1;
                        active_state = QuantumDrive.ACTIVE_STATES.ACTIVE_READY;
                    }

                    warping = true;
                }

                if (jumpship != null)
                {
                    if (warping)
                    {
                        jumpship.SetWarp(warp_fov);

                        SimRegion r = ship.GetRegion();

                        foreach (var neighbor in r.Ships())
                        {
                            if (neighbor.IsDropship())
                            {
                                Ship s = neighbor;
                                Point delta = s.Location() - ship.Location();

                                if (delta.Length < 5e3)
                                    s.SetWarp(warp_fov);
                            }
                        }
                    }
                    else
                    {
                        warp_fov = 1;
                        jumpship.SetWarp(warp_fov);
                    }
                }
            }
        }

        //public void SetShip(Ship s) { ship = s; }
        public void SetDest(Ship d) { dest = d; }

        public Point ApproachPoint(int i) { return approach_point[i]; }
        public Point StartPoint() { return start_point; }
        public Point EndPoint() { return end_point; }

        public virtual void SetApproachPoint(int i, Point loc)
        {
            if (i >= 0 && i < NUM_APPROACH_PTS)
                approach_rel[i] = loc;
        }

        public virtual void SetStartPoint(Point loc)
        {
            start_rel = loc;
        }

        public virtual void SetEndPoint(Point loc)
        {
            end_rel = loc;
        }

        public virtual void SetCycleTime(double time)
        {
            cycle_time = time;
        }

        public override void Orient(Physical rep)
        {
            base.Orient(rep);

            Matrix33D orientation = rep.Cam().Orientation();
            Point loc = rep.Location();

            start_point = (orientation * start_rel) + loc; // TODO matrix * vector multiplication order
            end_point = (orientation * end_rel) + loc; // TODO matrix * vector multiplication order

            for (int i = 0; i < NUM_APPROACH_PTS; i++)
                approach_point[i] = (orientation * approach_rel[i]) + loc; // TODO matrix * vector multiplication order
        }


        // SimObserver:
        public virtual bool Update(SimObject obj)
        {
            if (obj == jumpship)
            {
                jumpship.SetWarp(1);

                SimRegion r = ship.GetRegion();

                foreach (var neighbor in r.Ships())
                {
                    if (neighbor.IsDropship())
                    {
                        Ship s = neighbor;
                        Point delta = s.Location() - ship.Location();

                        if (delta.Length < 5e3)
                            s.SetWarp(1);
                    }
                }

                jumpship = null;
            }

            return simObserver.Update(obj);
        }

        public virtual string GetObserverName()
        {
            return Name();
        }

        // accessors:
        //public Ship GetShip() { return ship; }
        public Ship GetDest() { return dest; }

        public QuantumDrive.ACTIVE_STATES ActiveState() { return active_state; }
        public double WarpFactor() { return warp_fov; }


        protected virtual void Jump()
        {
            Sim sim = Sim.GetSim();
            SimRegion rgn = ship.GetRegion();
            SimRegion dst = dest.GetRegion();

            sim.CreateExplosion(jumpship.Location(), new Point(0, 0, 0), Explosion.ExplosionType.QUANTUM_FLASH, 1.0f, 0, rgn);
            sim.RequestHyperJump(jumpship, dst, dest.Location().OtherHand(), 0, ship, dest);

            energy = 0.0f;

            Farcaster f = dest.GetFarcaster();
            if (f != null) f.Arrive(jumpship);

            active_state = QuantumDrive.ACTIVE_STATES.ACTIVE_READY;
            warp_fov = 1;
            jumpship = null;
        }

        protected virtual void Arrive(Ship s)
        {
            energy = 0.0f;

            active_state = QuantumDrive.ACTIVE_STATES.ACTIVE_POSTWARP;
            warp_fov = 5000;
            jumpship = s;

            if (jumpship != null && jumpship.Velocity().Length < 500)
            {
                jumpship.SetVelocity(jumpship.Heading() * 500);
            }
        }


        public void Observe(SimObject obj)
        {
            simObserver.Observe(obj);
        }

        public void Ignore(SimObject obj)
        {
            simObserver.Ignore(obj);
        }

        //protected Ship ship;
        protected Ship dest = null;
        protected Ship jumpship = null;

        protected Point start_rel;
        protected Point end_rel;
        protected Point[] approach_rel = new Point[NUM_APPROACH_PTS];

        protected Point start_point;
        protected Point end_point;
        protected Point[] approach_point = new Point[NUM_APPROACH_PTS];

        protected double cycle_time = 10;
        protected QuantumDrive.ACTIVE_STATES active_state = QuantumDrive.ACTIVE_STATES.ACTIVE_READY;
        protected double warp_fov = 1;

        protected bool no_dest = false;
        protected ISimObserver simObserver = new SimObserver();
    }
}