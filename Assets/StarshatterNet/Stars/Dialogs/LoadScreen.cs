﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      LoadScreen.h/LoadScreen.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

*/
using System;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class LoadScreen
    {
        public LoadScreen()
        {
            screen = null;
            load_dlg = null; cmp_load_dlg = null; isShown = false;
        }
        //public virtual ~LoadScreen();

        public virtual void Setup(Screen s, Camera camera, GameObject go)
        {
            if (s == null)
                return;

            screen = s;

            //TODO DataLoader* loader = DataLoader.GetLoader();
            //TODO loader.UseFileSystem(true);

            // create windows
            FormDef load_def = new FormDef("LoadDlg", 0);
            load_def.Load("LoadDlg");
            load_dlg = new LoadDlg(screen, camera, load_def, go);

            FormDef cmp_load_def = new FormDef("CmpLoadDlg", 0);
            cmp_load_def.Load("CmpLoadDlg");
            cmp_load_dlg = new CmpLoadDlg(screen, camera, cmp_load_def, go);

            //TODO loader.UseFileSystem(Starshatter.UseFileSystem());
            ShowLoadDlg();
        }
        public virtual void TearDown()
        {
            //if (screen != null)
            //{
            //    if (load_dlg != null) screen.DelWindow(load_dlg);
            //    if (cmp_load_dlg != null) screen.DelWindow(cmp_load_dlg);
            //}

            //delete load_dlg;
            //delete cmp_load_dlg;

            load_dlg = null;
            cmp_load_dlg = null;
            screen = null;
        }
        public virtual bool CloseTopmost()
        {
            return false;
        }


        public virtual bool IsShown() { return isShown; }
        public virtual void Show()
        {
            if (!isShown)
            {
                ShowLoadDlg();
                isShown = true;
            }
        }
        public virtual void Hide()
        {
            if (isShown)
            {
                HideLoadDlg();
                isShown = false;
            }
        }

        public virtual void ShowLoadDlg()
        {
            if (load_dlg != null) load_dlg.Hide();
            if (cmp_load_dlg != null) cmp_load_dlg.Hide();

            Starshatter stars = Starshatter.GetInstance();

            // show campaign load dialog if available and loading campaign
            if (stars != null && cmp_load_dlg != null)
            {
                if (stars.GetGameMode() == Starshatter.MODE.CLOD_MODE ||
                        stars.GetGameMode() == Starshatter.MODE.CMPN_MODE)
                {
                    cmp_load_dlg.Show();
                    Mouse.Show(false);
                    return;
                }
            }

            // otherwise, show regular load dialog
            if (load_dlg != null)
            {
                load_dlg.Show();
                Mouse.Show(false);
            }
        }
        public virtual void HideLoadDlg()
        {
            if (load_dlg != null && load_dlg.IsShown())
                load_dlg.Hide();

            if (cmp_load_dlg != null && cmp_load_dlg.IsShown())
                cmp_load_dlg.Hide();
        }
        public virtual LoadDlg GetLoadDlg() { return load_dlg; }
        public virtual CmpLoadDlg GetCmpLoadDlg() { return cmp_load_dlg; }

        public virtual void ExecFrame()
        {
             Game.SetScreenColor(Color.black);

            if (load_dlg != null && load_dlg.IsShown())
                load_dlg.ExecFrame();

            if (cmp_load_dlg != null && cmp_load_dlg.IsShown())
                cmp_load_dlg.ExecFrame();
        }


        private Screen screen;
        private LoadDlg load_dlg;
        private CmpLoadDlg cmp_load_dlg;

        private bool isShown;

    }
}