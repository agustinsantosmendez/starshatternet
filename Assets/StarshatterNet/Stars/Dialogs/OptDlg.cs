﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      OptDlg.h/OptDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class OptDlg : FormWindow
    {
        public OptDlg(Screen s, Camera c, FormDef def, BaseScreen mgr, GameObject p) :
           base(s, 0, 0, s.Width(), s.Height(), c, p, "OptDlg")
        {
            manager = mgr;
            apply = null; cancel = null; vid_btn = null; aud_btn = null; ctl_btn = null; opt_btn = null; mod_btn = null;
            closed = true;
            Init(def);
        }
        public override void RegisterControls()
        {
            if (apply != null)
                return;

            flight_model = (ComboBox)FindControl(201);
            flying_start = (ComboBox)FindControl(211);
            landings = (ComboBox)FindControl(202);
            ai_difficulty = (ComboBox)FindControl(203);
            hud_mode = (ComboBox)FindControl(204);
            hud_color = (ComboBox)FindControl(205);
            ff_mode = (ComboBox)FindControl(206);
            grid_mode = (ComboBox)FindControl(207);
            gunsight = (ComboBox)FindControl(208);
            description = FindControl(500);
            apply = (Button)FindControl(1);
            cancel = (Button)FindControl(2);
            vid_btn = (Button)FindControl(901);
            aud_btn = (Button)FindControl(902);
            ctl_btn = (Button)FindControl(903);
            opt_btn = (Button)FindControl(904);
            mod_btn = (Button)FindControl(905);

            if (flight_model != null)
            {
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, flight_model, OnEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, flight_model, OnExit);
            }

            if (flying_start != null)
            {
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, flying_start, OnEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, flying_start, OnExit);
            }

            if (landings != null)
            {
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, landings, OnEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, landings, OnExit);
            }

            if (ai_difficulty != null)
            {
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, ai_difficulty, OnEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, ai_difficulty, OnExit);
            }

            if (hud_mode != null)
            {
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, hud_mode, OnEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, hud_mode, OnExit);
            }

            if (hud_color != null)
            {
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, hud_color, OnEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, hud_color, OnExit);
            }

            if (ff_mode != null)
            {
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, ff_mode, OnEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, ff_mode, OnExit);
            }

            if (grid_mode != null)
            {
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, grid_mode, OnEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, grid_mode, OnExit);
            }

            if (gunsight != null)
            {
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, gunsight, OnEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, gunsight, OnExit);
            }

            if (apply != null)
                REGISTER_CLIENT(ETD.EID_CLICK, apply, OnApply);

            if (cancel != null)
                REGISTER_CLIENT(ETD.EID_CLICK, cancel, OnCancel);

            if (vid_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, vid_btn, OnVideo);

            if (aud_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, aud_btn, OnAudio);

            if (ctl_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, ctl_btn, OnControls);

            if (opt_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, opt_btn, OnOptions);

            if (mod_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, mod_btn, OnMod);
        }
        public override void Show()
        {
            base.Show();

            if (closed)
            {
                Starshatter stars = Starshatter.GetInstance();

                if (stars != null)
                {
                    if (flight_model != null)
                        flight_model.SetSelection((int)Ship.GetFlightModel());

                    if (landings != null)
                        landings.SetSelection((int)Ship.GetLandingModel());

                    if (hud_mode != null)
                        hud_mode.SetSelection(HUDView.IsArcade() ? 1 : 0);

                    if (hud_color != null)
                        hud_color.SetSelection(HUDView.DefaultColorSet());

                    if (ff_mode != null)
                        ff_mode.SetSelection((int)(Ship.GetFriendlyFireLevel() * 4));
                }

                Player player = Player.GetCurrentPlayer();
                if (player != null)
                {
                    if (flying_start != null)
                        flying_start.SetSelection(player.FlyingStart());

                    if (ai_difficulty != null)
                        ai_difficulty.SetSelection(ai_difficulty.NumItems() - player.AILevel() - 1);

                    if (grid_mode != null)
                        grid_mode.SetSelection(player.GridMode());

                    if (gunsight != null)
                        gunsight.SetSelection(player.Gunsight());
                }
            }

            if (vid_btn != null) vid_btn.SetButtonState(0);
            if (aud_btn != null) aud_btn.SetButtonState(0);
            if (ctl_btn != null) ctl_btn.SetButtonState(0);
            if (opt_btn != null) opt_btn.SetButtonState(1);
            if (mod_btn != null) mod_btn.SetButtonState(0);

            closed = false;
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnApply(null, null);
            }
        }

        // Operations:
        public virtual void Apply()
        {
            if (closed) return;

            Player player = Player.GetCurrentPlayer();
            if (player != null)
            {
                if (flight_model != null)
                    player.SetFlightModel((SimElements.FLIGHT_MODEL)flight_model.GetSelectedIndex());

                if (flying_start != null)
                    player.SetFlyingStart(flying_start.GetSelectedIndex());

                if (landings != null)
                    player.SetLandingModel((SimElements.LANDING_MODEL)landings.GetSelectedIndex());

                if (ai_difficulty != null)
                    player.SetAILevel(ai_difficulty.NumItems() - ai_difficulty.GetSelectedIndex() - 1);

                if (hud_mode != null)
                    player.SetHUDMode(hud_mode.GetSelectedIndex());

                if (hud_color != null)
                    player.SetHUDColor(hud_color.GetSelectedIndex());

                if (ff_mode != null)
                    player.SetFriendlyFire(ff_mode.GetSelectedIndex());

                if (grid_mode != null)
                    player.SetGridMode(grid_mode.GetSelectedIndex());

                if (gunsight != null)
                    player.SetGunsight(gunsight.GetSelectedIndex());

                Player.Save();
            }

            if (flight_model != null)
                Ship.SetFlightModel((SimElements.FLIGHT_MODEL)flight_model.GetSelectedIndex());

            if (landings != null)
                Ship.SetLandingModel((SimElements.LANDING_MODEL)landings.GetSelectedIndex());

            if (hud_mode != null)
                HUDView.SetArcade(hud_mode.GetSelectedIndex() > 0);

            if (hud_color != null)
                HUDView.SetDefaultColorSet(hud_color.GetSelectedIndex());

            if (ff_mode != null)
                Ship.SetFriendlyFireLevel(ff_mode.GetSelectedIndex() / 4.0);

            HUDView hud = HUDView.GetInstance();
            if (hud != null) hud.SetHUDColorSet(hud_color.GetSelectedIndex());

            closed = true;
        }

        public virtual void Cancel()
        {
            closed = true;
        }


        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            manager.ApplyOptions();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            manager.CancelOptions();
        }

        public virtual void OnEnter(ActiveWindow obj, AWEvent evnt)
        {
            ActiveWindow src = evnt.window;

            if (src != null && description != null)
                description.SetText(src.GetAltText());
        }
        public virtual void OnExit(ActiveWindow obj, AWEvent evnt)
        {
            ComboBox cmb = (ComboBox)evnt.window;

            if (cmb == null || cmb.IsListShowing())
                return;

            if (description != null)
                description.SetText("");
        }

        public virtual void OnAudio(ActiveWindow obj, AWEvent evnt) { manager.ShowAudDlg(); }
        public virtual void OnVideo(ActiveWindow obj, AWEvent evnt) { manager.ShowVidDlg(); }
        public virtual void OnOptions(ActiveWindow obj, AWEvent evnt) { manager.ShowOptDlg(); }
        public virtual void OnControls(ActiveWindow obj, AWEvent evnt) { manager.ShowCtlDlg(); }
        public virtual void OnMod(ActiveWindow obj, AWEvent evnt) { manager.ShowModDlg(); }


        protected BaseScreen manager;

        protected ComboBox flight_model;
        protected ComboBox flying_start;
        protected ComboBox landings;
        protected ComboBox ai_difficulty;
        protected ComboBox hud_mode;
        protected ComboBox hud_color;
        protected ComboBox joy_mode;
        protected ComboBox ff_mode;
        protected ComboBox grid_mode;
        protected ComboBox gunsight;

        protected ActiveWindow description;

        protected Button aud_btn;
        protected Button vid_btn;
        protected Button opt_btn;
        protected Button ctl_btn;
        protected Button mod_btn;

        protected Button apply;
        protected Button cancel;

        protected bool closed;
    }
}