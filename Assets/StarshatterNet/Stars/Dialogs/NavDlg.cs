﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NavDlg.h/NavDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Navigation Active Window class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.StarSystems;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class NavDlg : FormWindow
    {
        public const int EID_MAP_CLICK = 1000;
        public NavDlg(Screen s, Camera c, FormDef def, BaseScreen mgr, GameObject p) :
              base(s, 0, 0, s.Width(), s.Height(), c, p, "NavDlg")
        {
            manager = mgr;
            loc_labels = null; dst_labels = null; loc_data = null; dst_data = null;
            seln_list = null; info_list = null; seln_mode = SELECT_REGION;
            nav_edit_mode = NAV_EDIT_MODE.NAV_EDIT_NONE; star_map = null; map_win = null;
            star_system = null; ship = null; mission = null; editor = false;

            Init(def);
        }
        //public virtual ~NavDlg();
        public override void RegisterControls()
        {
            int i;

            map_win = FindControl(100);

            if (map_win != null)
            {
                 star_map = new MapView(map_win);

                REGISTER_CLIENT(ETD.EID_LBUTTON_DOWN, map_win, OnMapDown);
                REGISTER_CLIENT(ETD.EID_MOUSE_MOVE, map_win, OnMapMove);
                REGISTER_CLIENT((ETD)EID_MAP_CLICK, map_win, OnMapClick);
             }

            for (i = 0; i < 3; i++)
            {
                view_btn[i] = (Button)FindControl(101 + i);
                REGISTER_CLIENT(ETD.EID_CLICK, view_btn[i], OnView);
            }

            close_btn = (Button)FindControl(2);

            if (close_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, close_btn, OnClose);

            zoom_in_btn = (Button)FindControl(110);
            zoom_out_btn = (Button)FindControl(111);

            for (i = 0; i < 6; i++)
            {
                filter_btn[i] = (Button)FindControl(401 + i);
                REGISTER_CLIENT(ETD.EID_CLICK, filter_btn[i], OnFilter);
            }

            commit_btn = (Button)FindControl(1);

            if (commit_btn != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, commit_btn, OnEngage);
            }

            loc_labels = FindControl(601);
            dst_labels = FindControl(602);
            loc_data = FindControl(701);
            dst_data = FindControl(702);

            seln_list = (ListBox)FindControl(801);

            if (seln_list != null)
            {
                seln_list.SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_FILLED_BOX);
                REGISTER_CLIENT(ETD.EID_SELECT, seln_list, OnSelect);
            }

            info_list = (ListBox)FindControl(802);

            if (star_map != null)
            {
                star_map.SetViewMode(VIEW_SYSTEM);
                view_btn[1].SetButtonState(1);

                star_map.SetSelectionMode(2);
            }

            UpdateSelection();
        }

        // Operations:
        public virtual void OnView(ActiveWindow obj, AWEvent evnt)
        {
            int use_filter_mode = -1;

            view_btn[VIEW_GALAXY].SetButtonState(0);
            view_btn[VIEW_SYSTEM].SetButtonState(0);
            view_btn[VIEW_REGION].SetButtonState(0);

            if (view_btn[0] == evnt.window)
            {
                star_map.SetViewMode(VIEW_GALAXY);
                view_btn[VIEW_GALAXY].SetButtonState(1);
                use_filter_mode = SELECT_SYSTEM;
            }

            else if (view_btn[VIEW_SYSTEM] == evnt.window)
            {
                star_map.SetViewMode(VIEW_SYSTEM);
                view_btn[VIEW_SYSTEM].SetButtonState(1);
                use_filter_mode = SELECT_REGION;
            }

            else if (view_btn[VIEW_REGION] == evnt.window)
            {
                star_map.SetViewMode(VIEW_REGION);
                view_btn[VIEW_REGION].SetButtonState(1);
                use_filter_mode = SELECT_STARSHIP;
            }

            if (use_filter_mode >= 0)
            {
                for (int i = 0; i < 6; i++)
                {
                    if (i == use_filter_mode)
                        filter_btn[i].SetButtonState(1);
                    else
                        filter_btn[i].SetButtonState(0);
                }

                UseFilter(use_filter_mode);
            }
        }
        public virtual void OnFilter(ActiveWindow obj, AWEvent evnt)
        {
            int filter_index = -1;
            for (int i = 0; i < 6; i++)
            {
                if (filter_btn[i] == evnt.window)
                {
                    filter_index = i;
                    filter_btn[i].SetButtonState(1);
                }
                else
                {
                    filter_btn[i].SetButtonState(0);
                }
            }

            if (filter_index >= 0)
                UseFilter(filter_index);
        }
        public virtual void OnSelect(ActiveWindow obj, AWEvent evnt)
        {
            int index = -1;

            for (int i = 0; i < seln_list.NumItems(); i++)
                if (seln_list.IsSelected(i))
                    index = i;

            if (index >= 0)
                SelectObject(index);
        }

        public virtual void OnCommit(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.ShowNavDlg();   // also hides
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.ShowNavDlg();   // also hides
        }
        public virtual void OnEngage(ActiveWindow obj, AWEvent evnt)
        {
            bool hide = false;

            if (ship != null)
            {
                NavSystem navsys = ship.GetNavSystem();
                if (navsys != null)
                {
                    if (navsys.AutoNavEngaged())
                    {
                        navsys.DisengageAutoNav();
                        commit_btn.SetText(Game.GetText("NavDlg.commit"));
                        commit_btn.SetBackColor(commit_color);
                    }
                    else
                    {
                        navsys.EngageAutoNav();
                        commit_btn.SetText(Game.GetText("NavDlg.cancel"));
                        commit_btn.SetBackColor(cancel_color);
                        hide = true;
                    }

                    Sim sim = Sim.GetSim();
                    if (sim != null)
                        ship.SetControls(sim.GetControls());
                }
            }

            if (manager != null && hide)
                manager.ShowNavDlg();   // also hides
        }

        public virtual void OnMapDown(ActiveWindow obj, AWEvent evnt) { }
        public virtual void OnMapMove(ActiveWindow obj, AWEvent evnt) { }
        static ulong click_time = 0;
        public virtual void OnMapClick(ActiveWindow obj, AWEvent evnt)
        {

            SetSystem(star_map.GetSystem());
            CoordinateSelection();

            // double-click:
            if (Game.RealTime() - click_time < 350)
            {
                MissionElement elem = star_map.GetSelectedElem();
                MsnElemDlg msn_elem_dlg = manager.GetMsnElemDlg();

                if (elem != null && msn_elem_dlg != null)
                {
                    msn_elem_dlg.SetMission(mission);
                    msn_elem_dlg.SetMissionElement(elem);
                    manager.ShowMsnElemDlg();
                }
            }

            click_time = Game.RealTime();
        }
        public virtual void OnClose(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.HideNavDlg();
        }


        public virtual void ExecFrame()
        {
            Sim sim = Sim.GetSim();

            if (loc_labels != null && ship != null)
            {
                string loc_buf;
                string x = null;
                string y = null;
                string z = null;
                string d = null;

                FormatUtil.FormatNumber(out x, -ship.Location().X);
                FormatUtil.FormatNumber(out y, ship.Location().Z);
                FormatUtil.FormatNumber(out z, ship.Location().Y);

                loc_buf = Game.GetText("NavDlg.loc-labels");
                loc_labels.SetText(loc_buf);

                if (sim.GetActiveRegion() != null)
                {
                    loc_buf = string.Format("\n{0}\n{1}\n{2}, {3}, {4}",
                                        star_system.Name(),
                                        sim.GetActiveRegion().Name(),
                                       x, y, z);

                }
                else
                {
                    loc_buf = string.Format("\n{0}\nPlanck Space?\n{1}, {2}, {3}",
                    star_system.Name(), x, y, z);
                }

                loc_data.SetText(loc_buf);

                if (ship != null)
                {
                    NavSystem navsys = ship.GetNavSystem();

                    if (ship.GetNextNavPoint() == null || navsys == null)
                    {
                        commit_btn.SetText(Game.GetText("NavDlg.commit"));
                        commit_btn.SetBackColor(commit_color);
                        commit_btn.SetEnabled(false);
                    }
                    else if (navsys != null)
                    {
                        commit_btn.SetEnabled(true);

                        if (navsys.AutoNavEngaged())
                        {
                            commit_btn.SetText(Game.GetText("NavDlg.cancel"));
                            commit_btn.SetBackColor(cancel_color);
                        }
                        else
                        {
                            commit_btn.SetText(Game.GetText("NavDlg.commit"));
                            commit_btn.SetBackColor(commit_color);
                        }
                    }
                }

                if (dst_labels != null)
                {
                    Instruction navpt = ship.GetNextNavPoint();

                    if (navpt != null && navpt.Region() != null)
                    {
                        FormatUtil.FormatNumber(out x, navpt.Location().X);
                        FormatUtil.FormatNumber(out y, navpt.Location().Y);
                        FormatUtil.FormatNumber(out z, navpt.Location().Z);

                        double distance = 0;
                        Point npt = navpt.Region().Location() + navpt.Location();
                        if (sim.GetActiveRegion() != null)
                            npt -= sim.GetActiveRegion().Location();

                        npt = npt.OtherHand();

                        // distance from self to navpt:
                        distance = new Point(npt - ship.Location()).Length;
                        FormatUtil.FormatNumber(out d, distance);

                        loc_buf = Game.GetText("NavDlg.dst-labels");
                        dst_labels.SetText(loc_buf);

                        loc_buf = string.Format("\n{0}\n{1}\n{2}, {3}, {4}\n{5}",
                                           star_system.Name(),
                                           navpt.Region().Name(),
                                           x, y, z, d);
                        dst_data.SetText(loc_buf);
                    }
                    else
                    {
                        dst_labels.SetText(Game.GetText("NavDlg.destination"));
                        dst_data.SetText(Game.GetText("NavDlg.not-avail"));
                    }
                }
            }

            UpdateSelection();
            UpdateLists();

            if (Keyboard.KeyDown(KeyMap.VK_ADD) ||
                    (zoom_in_btn != null && zoom_in_btn.GetButtonState() > 0))
            {
                star_map.ZoomIn();
            }
            else if (Keyboard.KeyDown(KeyMap.VK_SUBTRACT) ||
                    (zoom_out_btn != null && zoom_out_btn.GetButtonState() > 0))
            {
                star_map.ZoomOut();
            }

            else if (star_map != null && star_map.TargetRect().Contains((int)Mouse.X(), (int)Mouse.Y()))
            {

                if (Mouse.Wheel() > 0)
                {
                    star_map.ZoomIn();
                    star_map.ZoomIn();
                    star_map.ZoomIn();
                }

                else if (Mouse.Wheel() < 0)
                {
                    star_map.ZoomOut();
                    star_map.ZoomOut();
                    star_map.ZoomOut();
                }
            }

            if (nav_edit_mode == NAV_EDIT_MODE.NAV_EDIT_NONE)
                Mouse.SetCursor(Mouse.CURSOR.ARROW);
            else
                Mouse.SetCursor(Mouse.CURSOR.CROSS);
        }
        public StarSystem GetSystem() { return star_system; }
        public void SetSystem(StarSystem s)
        {
            if (star_system == s)
                return;

            star_system = s;

            if (star_map != null)
            {
                Campaign c = Campaign.GetCampaign();
                Sim sim = Sim.GetSim();

                if (sim != null && sim.GetSystemList().Count != 0)
                {
                    star_map.SetGalaxy(sim.GetSystemList());
                }
                else if (mission != null && mission.GetSystemList().Count != 0)
                {
                    star_map.SetGalaxy(mission.GetSystemList());
                }
                else if (c != null && c.GetSystemList().Count != 0)
                {
                    star_map.SetGalaxy(c.GetSystemList());
                }
                else
                {
                    Galaxy g = Galaxy.GetInstance();
                    if (g != null)
                        star_map.SetGalaxy(g.GetSystemList());
                }

                star_map.SetSystem(s);
            }

            // flush old object pointers:
            stars.Clear();
            planets.Clear();
            regions.Clear();
            contacts.Clear();

            if (star_system != null)
            {
                // insert objects from star system:
                foreach (OrbitalBody star in star_system.Bodies())
                {
                    switch (star.Type())
                    {
                        case Orbital.OrbitalType.STAR:
                            stars.Add(star);
                            break;
                        case Orbital.OrbitalType.PLANET:
                        case Orbital.OrbitalType.MOON:
                            planets.Add(star);
                            break;
                    }

                    foreach (OrbitalBody planet in star.Satellites())
                    {
                        planets.Add(planet);

                        foreach (OrbitalBody moon in planet.Satellites())
                        {
                            planets.Add(moon);
                        }
                    }
                }

                foreach (OrbitalRegion rgn in star_system.AllRegions())
                    regions.Add(rgn);
            }

            // sort region list by distance from the star:
            regions.Sort();
        }
        public Mission GetMission() { return mission; }
        public void SetMission(Mission m)
        {
            if (m == null && mission == m)
                return;

            if (mission == m && star_system == m.GetStarSystem())
                return;

            mission = m;

            if (mission != null)
            {
                SetSystem(mission.GetStarSystem());
            }

            if (star_map != null)
            {
                Campaign c = Campaign.GetCampaign();
                Sim sim = Sim.GetSim();

                star_map.SetMission(null);   // prevent building map menu twice

                if (sim != null && sim.GetSystemList().Count != 0)
                {
                    star_map.SetGalaxy(sim.GetSystemList());
                }
                else if (mission != null && mission.GetSystemList().Count != 0)
                {
                    star_map.SetGalaxy(mission.GetSystemList());
                }
                else if (c != null && c.GetSystemList().Count != 0)
                {
                    star_map.SetGalaxy(c.GetSystemList());
                }
                else
                {
                    Galaxy g = Galaxy.GetInstance();
                    if (g != null)
                        star_map.SetGalaxy(g.GetSystemList());
                }

                if (mission != null)
                {
                    star_map.SetMission(mission);
                    star_map.SetRegionByName(mission.GetRegion());

                    if (star_map.GetViewMode() == VIEW_REGION)
                    {
                        foreach (MissionElement elem in mission.GetElements())
                        {
                            MissionElement e = elem;

                            if (e.Player() != 0)
                                star_map.SetSelectedElem(e);
                        }
                    }
                }
            }

            bool updated = false;

            if (mission != null)
            {
                Orbital rgn = null;
                //rgn = mission.GetStarSystem().FindOrbital(mission.GetRegion());
                rgn = mission.GetStarSystem().FindRegion(mission.GetRegion());

                if (rgn != null)
                {
                    SelectRegion((OrbitalRegion)rgn);
                    updated = true;
                }
            }


            if (!updated)
                UpdateSelection();
        }
        public Ship GetShip() { return ship; }
        public void SetShip(Ship s)
        {
            if (ship == s)
                return;

            ship = s;

            if (ship != null)
                SetSystem(ship.GetRegion().System());

            if (star_map != null)
            {
                Sim sim = Sim.GetSim();

                if (sim != null && sim.GetSystemList().Count != 0)
                    star_map.SetGalaxy(sim.GetSystemList());

                star_map.SetShip(ship);

                if (ship != null)
                {
                    star_map.SetRegion(ship.GetRegion().GetOrbitalRegion());
                    UseViewMode(VIEW_REGION);
                    star_map.SetSelectedShip(ship);
                }
            }

            for (int i = 0; i < 6; i++)
                filter_btn[i].SetButtonState(0);

            filter_btn[SELECT_REGION].SetButtonState(1);
            UseFilter(SELECT_STARSHIP);
            UpdateSelection();
        }

        public bool GetEditorMode() { return editor; }
        public void SetEditorMode(bool b) { editor = b; }


        public void UseViewMode(int mode)
        {
            if (mode >= 0 && mode < 3)
            {
                int use_filter_mode = -1;

                view_btn[VIEW_GALAXY].SetButtonState(0);
                view_btn[VIEW_SYSTEM].SetButtonState(0);
                view_btn[VIEW_REGION].SetButtonState(0);

                if (mode == 0)
                {
                    star_map.SetViewMode(VIEW_GALAXY);
                    view_btn[VIEW_GALAXY].SetButtonState(1);
                    use_filter_mode = SELECT_SYSTEM;
                }

                else if (mode == 1)
                {
                    star_map.SetViewMode(VIEW_SYSTEM);
                    view_btn[VIEW_SYSTEM].SetButtonState(1);
                    use_filter_mode = SELECT_REGION;
                }

                else if (mode == 2)
                {
                    star_map.SetViewMode(VIEW_REGION);
                    view_btn[VIEW_REGION].SetButtonState(1);
                    use_filter_mode = SELECT_STARSHIP;
                }

                if (use_filter_mode >= 0)
                {
                    for (int i = 0; i < 6; i++)
                    {
                        filter_btn[i].SetButtonState(i == use_filter_mode ? (short)1 : (short)0);
                    }

                    UseFilter(use_filter_mode);
                }
            }
        }
        public void UseFilter(int filter_index)
        {
            seln_mode = filter_index;

            star_map.SetSelectionMode(seln_mode);
            UpdateSelection();
            UpdateLists();
        }
        public void SelectObject(int index)
        {
            string selected = seln_list.GetItemText(index);
            int selection = (int)seln_list.GetItemData(index);

            star_map.SetSelection(selection);
            SetSystem(star_map.GetSystem());
        }
        public void UpdateSelection()
        {
            if (info_list == null)
                return;

            if (star_map == null)
                return;

            info_list.ClearItems();

            string units_km = " " + Game.GetText("NavDlg.units.kilometers");
            string units_tonnes = " " + Game.GetText("NavDlg.units.tonnes");

            if (seln_mode <= SELECT_REGION)
            {
                Orbital s = star_map.GetSelection();

                if (s != null)
                {
                    string radius = null;
                    string mass = null;
                    string orbit = null;
                    string period = null;
                    string units = null;

                    double p = s.Period();

                    if (p < 60)
                    {
                        units = string.Format(" {0}", Game.GetText("NavDlg.units.seconds"));
                    }
                    else if (p < 3600)
                    {
                        p /= 60;
                        units = string.Format(" {0}", Game.GetText("NavDlg.units.minutes"));
                    }
                    else if (p < 24 * 3600)
                    {
                        p /= 3600;
                        units = string.Format(" {0}", Game.GetText("NavDlg.units.hours"));
                    }
                    else if (p < 365.25 * 24 * 3600)
                    {
                        p /= 24 * 3600;
                        units = string.Format(" {0}", Game.GetText("NavDlg.units.days"));
                    }
                    else
                    {
                        p /= 365.25 * 24 * 3600;
                        units = string.Format(" {0}", Game.GetText("NavDlg.units.years"));
                    }

                    FormatUtil.FormatNumber(out radius, s.Radius() / 1000);
                    FormatUtil.FormatNumber(out mass, s.Mass() / 1000);
                    FormatUtil.FormatNumber(out orbit, s.Orbit() / 1000);
                    FormatUtil.FormatNumber(out period, p);

                    radius += units_km;
                    mass += units_tonnes;
                    orbit += units_km;
                    period += units;

                    if (seln_mode >= SELECT_SYSTEM)
                    {
                        info_list.AddItem(Game.GetText("NavDlg.filter." + filter_name[seln_mode]));
                        info_list.AddItem(Game.GetText("NavDlg.radius"));
                        if (s.Mass() > 0)
                            info_list.AddItem(Game.GetText("NavDlg.mass"));
                        info_list.AddItem(Game.GetText("NavDlg.orbit"));
                        info_list.AddItem(Game.GetText("NavDlg.period"));

                        int row = 0;
                        info_list.SetItemText(row++, 1, s.Name());
                        info_list.SetItemText(row++, 1, radius);
                        if (s.Mass() > 0)
                            info_list.SetItemText(row++, 1, mass);
                        info_list.SetItemText(row++, 1, orbit);
                        info_list.SetItemText(row++, 1, period);
                    }
                }
            }

            else if (seln_mode == SELECT_STATION ||
                    seln_mode == SELECT_STARSHIP ||
                    seln_mode == SELECT_FIGHTER)
            {

                Ship sel_ship = star_map.GetSelectedShip();
                MissionElement sel_elem = star_map.GetSelectedElem();

                if (sel_ship != null)
                {
                    string order_desc = Game.GetText("NavDlg.none");
                    string shield = null;
                    string hull = null;
                    string range = null;

                    shield = string.Format("{0:D3}", sel_ship.ShieldStrength());
                    hull = string.Format("{0:D3}", sel_ship.HullStrength());
                    range = string.Format("{0}", Game.GetText("NavDlg.not-avail"));

                    if (ship != null)
                    {
                        FormatUtil.FormatNumberExp(ref range, new Point(sel_ship.Location() - ship.Location()).Length / 1000);
                        range += units_km;
                    }

                    info_list.AddItem(Game.GetText("NavDlg.name"));
                    info_list.AddItem(Game.GetText("NavDlg.class"));
                    info_list.AddItem(Game.GetText("NavDlg.sector"));
                    info_list.AddItem(Game.GetText("NavDlg.shield"));
                    info_list.AddItem(Game.GetText("NavDlg.hull"));
                    info_list.AddItem(Game.GetText("NavDlg.range"));
                    info_list.AddItem(Game.GetText("NavDlg.orders"));

                    int row = 0;
                    info_list.SetItemText(row++, 1, sel_ship.Name());
                    info_list.SetItemText(row++, 1, sel_ship.Abbreviation() + " " + sel_ship.Design().display_name);
                    info_list.SetItemText(row++, 1, sel_ship.GetRegion().Name());
                    info_list.SetItemText(row++, 1, shield);
                    info_list.SetItemText(row++, 1, hull);
                    info_list.SetItemText(row++, 1, range);
                    info_list.SetItemText(row++, 1, order_desc);
                }

                else if (sel_elem != null)
                {
                    string order_desc = Game.GetText("NavDlg.none");
                    string range = null;

                    MissionElement self = mission.GetElements()[0];
                    if (self != null)
                        FormatUtil.FormatNumberExp(ref range, new Point(sel_elem.Location() - self.Location()).Length / 1000);
                    else
                        range = "0";

                    range += units_km;

                    info_list.AddItem(Game.GetText("NavDlg.name"));
                    info_list.AddItem(Game.GetText("NavDlg.class"));
                    info_list.AddItem(Game.GetText("NavDlg.sector"));
                    info_list.AddItem(Game.GetText("NavDlg.range"));
                    info_list.AddItem(Game.GetText("NavDlg.orders"));

                    int row = 0;
                    info_list.SetItemText(row++, 1, sel_elem.Name());

                    if (sel_elem.GetDesign() != null)
                        info_list.SetItemText(row++, 1, sel_elem.Abbreviation() + " " + sel_elem.GetDesign().name);
                    else
                        info_list.SetItemText(row++, 1, Game.GetText("NavDlg.unknown"));

                    info_list.SetItemText(row++, 1, sel_elem.Region());
                    info_list.SetItemText(row++, 1, range);
                    info_list.SetItemText(row++, 1, order_desc);
                }
            }
        }
        public void UpdateLists()
        {
            if (seln_list == null)
                return;

            int seln_index = -1;
            int top_index = -1;

            if (seln_list.IsSelecting())
                seln_index = seln_list.GetListIndex();

            top_index = seln_list.GetTopIndex();

            seln_list.ClearItems();

            switch (seln_mode)
            {
                case SELECT_SYSTEM:
                    {
                        seln_list.SetColumnTitle(0, Game.GetText("NavDlg.filter." + filter_name[seln_mode]));
                        int i = 0;
                        foreach (StarSystem iter in star_map.GetGalaxy())
                            seln_list.AddItemWithData(iter.Name(), i++);
                    }
                    break;

                case SELECT_PLANET:
                    {
                        seln_list.SetColumnTitle(0, Game.GetText("NavDlg.filter." + filter_name[seln_mode]));
                        int i = 0;
                        foreach (Orbital iter in planets)
                        {
                            if (iter.Type() == Orbital.OrbitalType.MOON)
                                seln_list.AddItemWithData("- " + iter.Name(), i++);
                            else
                                seln_list.AddItemWithData(iter.Name(), i++);
                        }
                    }
                    break;

                case SELECT_REGION:
                    {
                        seln_list.SetColumnTitle(0, Game.GetText("NavDlg.filter." + filter_name[seln_mode]));
                        int i = 0;
                        foreach (OrbitalRegion iter in regions)
                        {
                            seln_list.AddItemWithData(iter.Name(), i++);
                        }
                    }
                    break;

                case SELECT_STATION:
                case SELECT_STARSHIP:
                case SELECT_FIGHTER:
                    {
                        seln_list.SetColumnTitle(0, Game.GetText("NavDlg.filter." + filter_name[seln_mode]));
                        int i = 0;

                        if (mission != null)
                        {
                            foreach (MissionElement e in mission.GetElements())
                            {
                                bool filter_ok =
                               (seln_mode == SELECT_STATION && e.IsStatic()) ||
                               (seln_mode == SELECT_STARSHIP && e.IsStarship() && !e.IsStatic()) ||
                               (seln_mode == SELECT_FIGHTER && e.IsDropship() && !e.IsSquadron());

                                if (filter_ok)
                                {
                                    bool visible = editor ||
                                    e.GetIFF() == 0 ||
                                    e.GetIFF() == mission.Team() ||
                                    e.IntelLevel() > Intel.INTEL_TYPE.KNOWN;

                                    if (visible)
                                        seln_list.AddItemWithData(e.Name(), e.Identity());
                                }
                            }
                        }

                        else if (ship != null)
                        {
                            Sim sim = Sim.GetSim();
                            foreach (SimRegion rgn in sim.GetRegions())
                            {

                                foreach (Ship s in rgn.Ships())
                                {
                                    bool filter_ok =
                                   (seln_mode == SELECT_STATION && s.IsStatic()) ||
                                   (seln_mode == SELECT_STARSHIP && s.IsStarship() && !s.IsStatic()) ||
                                   (seln_mode == SELECT_FIGHTER && s.IsDropship());


                                    if (filter_ok)
                                    {
                                        bool visible = s.GetIFF() == 0 ||
                                        s.GetIFF() == ship.GetIFF() ||
                                        s.GetElement() != null &&
                                        s.GetElement().IntelLevel() > Intel.INTEL_TYPE.KNOWN;

                                        if (visible)
                                            seln_list.AddItemWithData(s.Name(), s.Identity());
                                    }
                                }
                            }
                        }
                    }
                    break;

                default:
                    break;
            }

            if (top_index >= 0)
                seln_list.ScrollTo(top_index);

            if (seln_index >= 0)
                seln_list.SetSelected(seln_index);

            else
                CoordinateSelection();
        }
        public void CoordinateSelection()
        {
            if (seln_list == null || star_map == null)
                return;

            switch (seln_mode)
            {
                default:
                case SELECT_SYSTEM:
                case SELECT_PLANET:
                case SELECT_REGION:
                    {
                        seln_list.ClearSelection();

                        Orbital selected = star_map.GetSelection();

                        if (selected != null)
                        {
                            for (int i = 0; i < seln_list.NumItems(); i++)
                            {
                                if (seln_list.GetItemText(i) == selected.Name() ||
                                        seln_list.GetItemText(i) == "- " + selected.Name())
                                {
                                    seln_list.SetSelected(i, true);
                                }
                            }
                        }
                    }
                    break;

                case SELECT_STATION:
                case SELECT_STARSHIP:
                case SELECT_FIGHTER:
                    {
                        seln_list.ClearSelection();

                        Ship selected = star_map.GetSelectedShip();
                        MissionElement elem = star_map.GetSelectedElem();

                        if (selected != null)
                        {
                            for (int i = 0; i < seln_list.NumItems(); i++)
                            {
                                if (seln_list.GetItemText(i) == selected.Name())
                                {
                                    seln_list.SetSelected(i, true);
                                }
                            }
                        }

                        else if (elem != null)
                        {
                            for (int i = 0; i < seln_list.NumItems(); i++)
                            {
                                if (seln_list.GetItemText(i) == elem.Name())
                                {
                                    seln_list.SetSelected(i, true);
                                }
                            }
                        }
                    }
                    break;
            }
        }

        public void SelectStar(Orbital star)
        {
            UseViewMode(0);

            if (stars.Count > 0)
            {
                int sel = 0;

                foreach (Orbital iter in stars)
                {
                    if (iter == star)
                    {
                        int old_seln_mode = seln_mode;
                        UseFilter(SELECT_SYSTEM);
                        SelectObject(sel);
                        UseFilter(old_seln_mode);
                        return;
                    }

                    sel++;
                }
            }
        }
        public void SelectPlanet(Orbital planet)
        {
            UseViewMode(1);

            if (planets.Count > 0)
            {
                int sel = 0;

                foreach (Orbital iter in planets)
                {
                    if (iter == planet)
                    {
                        int old_seln_mode = seln_mode;
                        UseFilter(SELECT_PLANET);
                        SelectObject(sel);
                        UseFilter(old_seln_mode);
                        return;
                    }

                    sel++;
                }
            }
        }
        public void SelectRegion(OrbitalRegion rgn)
        {
            UseViewMode(2);

            if (regions.Count != 0)
            {
                int sel = 0;

                foreach (OrbitalRegion iter in regions)
                {
                    if (iter == rgn)
                    {
                        int old_seln_mode = seln_mode;
                        UseFilter(SELECT_REGION);
                        SelectObject(sel);
                        UseFilter(old_seln_mode);
                        return;
                    }

                    sel++;
                }
            }
        }

        public enum NAV_EDIT_MODE
        {
            NAV_EDIT_NONE = 0,
            NAV_EDIT_ADD = 1,
            NAV_EDIT_DEL = 2,
            NAV_EDIT_MOVE = 3
        };

        public void SetNavEditMode(NAV_EDIT_MODE mode)
        {
            if (nav_edit_mode != mode)
            {
                if (mode != NAV_EDIT_MODE.NAV_EDIT_NONE)
                {
                    int map_mode = star_map.GetSelectionMode();
                    if (map_mode != -1)
                        seln_mode = map_mode;

                    star_map.SetSelectionMode(-1);
                }
                else
                {
                    star_map.SetSelectionMode(seln_mode);
                }

                nav_edit_mode = mode;
            }
        }
        public NAV_EDIT_MODE GetNavEditMode()
        {
            return nav_edit_mode;
        }


        protected Button[] view_btn = new Button[3];
        protected Button[] filter_btn = new Button[6];
        protected Button commit_btn;
        protected Button zoom_in_btn;
        protected Button zoom_out_btn;
        protected Button close_btn;

        protected MapView star_map;
        protected ActiveWindow map_win;
        protected ActiveWindow loc_labels;
        protected ActiveWindow dst_labels;
        protected ActiveWindow loc_data;
        protected ActiveWindow dst_data;

        protected ListBox seln_list;
        protected ListBox info_list;

        protected BaseScreen manager;
        protected int seln_mode;
        protected NAV_EDIT_MODE nav_edit_mode;

        protected StarSystem star_system;
        protected List<Orbital> stars = new List<Orbital>();
        protected List<Orbital> planets = new List<Orbital>();
        protected List<OrbitalRegion> regions = new List<OrbitalRegion>();
        protected List<Ship> contacts = new List<Ship>();

        protected Ship ship;
        protected Mission mission;
        protected bool editor;

        private static string[] filter_name = {
                "SYSTEM",   "PLANET",
                "SECTOR",   "STATION",
                "STARSHIP", "FIGHTER"
            };

        private static string commit_name = "Commit";
        private static string cancel_name = "Cancel";

        private static Color32 commit_color = new Color32(53, 159, 67, 255);
        private static Color32 cancel_color = new Color32(160, 8, 8, 255);


        // Supported Selection Modes:

        private const int SELECT_NONE = -1;
        private const int SELECT_SYSTEM = 0;
        private const int SELECT_PLANET = 1;
        private const int SELECT_REGION = 2;
        private const int SELECT_STATION = 3;
        private const int SELECT_STARSHIP = 4;
        private const int SELECT_FIGHTER = 5;

        private const int VIEW_GALAXY = 0;
        private const int VIEW_SYSTEM = 1;
        private const int VIEW_REGION = 2;

    }
}