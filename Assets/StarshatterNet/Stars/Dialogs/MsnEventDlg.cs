﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetServerDlg.h/NetServerDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MsnEventDlg : FormWindow
    {
        public MsnEventDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
             base(s, 0, 0, s.Width(), s.Height(), c, p, "MsnEventDlg")
        {
            manager = mgr;
            btn_accept = null; btn_cancel = null; mission = null; evnt = null;

            Init(def);
        }
        //public virtual ~MsnEventDlg();
        public override void RegisterControls()
        {
            btn_accept = (Button)FindControl(1);
            if (btn_accept != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_accept, OnAccept);

            btn_cancel = (Button)FindControl(2);
            if (btn_accept != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_cancel, OnCancel);

            lbl_id = FindControl(201);
            edt_time = (EditBox)FindControl(202);
            edt_delay = (EditBox)FindControl(203);
            cmb_event = (ComboBox)FindControl(204);
            cmb_event_ship = (ComboBox)FindControl(205);
            cmb_event_source = (ComboBox)FindControl(206);
            cmb_event_target = (ComboBox)FindControl(207);
            edt_event_param = (EditBox)FindControl(208);
            edt_event_chance = (EditBox)FindControl(220);
            edt_event_sound = (EditBox)FindControl(209);
            edt_event_message = (EditBox)FindControl(210);

            cmb_trigger = (ComboBox)FindControl(221);
            cmb_trigger_ship = (ComboBox)FindControl(222);
            cmb_trigger_target = (ComboBox)FindControl(223);
            edt_trigger_param = (EditBox)FindControl(224);

            if (cmb_event != null)
                REGISTER_CLIENT(ETD.EID_SELECT, cmb_event, OnEventSelect);
        }
        public override void Show()
        {
            base.Show();

            if (evnt == null) return;

            FillShipList(cmb_event_ship, evnt.EventShip());
            FillShipList(cmb_event_source, evnt.EventSource());

            if (evnt.Event() == MissionEvent.EVENT_TYPE.JUMP)
                FillRgnList(cmb_event_target, evnt.EventTarget());
            else
                FillShipList(cmb_event_target, evnt.EventTarget());

            FillShipList(cmb_trigger_ship, evnt.TriggerShip());
            FillShipList(cmb_trigger_target, evnt.TriggerTarget());

            string buf;

            buf = string.Format("{0}", evnt.EventID());
            if (lbl_id != null) lbl_id.SetText(buf);

            if (edt_time != null)
            {
                buf = string.Format("{0:0.0}", evnt.Time());
                edt_time.SetText(buf);
            }

            if (edt_delay != null)
            {
                buf = string.Format("{0:0.0}", evnt.Delay());
                edt_delay.SetText(buf);
            }

            if (edt_event_chance != null)
            {
                buf = string.Format("{0}", evnt.EventChance());
                edt_event_chance.SetText(buf);
            }

            buf = string.Format("{0}", evnt.EventParam());
            if (edt_event_param != null) edt_event_param.SetText(buf);

            if (edt_trigger_param != null)
                edt_trigger_param.SetText(evnt.TriggerParamStr());

            if (edt_event_message != null)
                edt_event_message.SetText(evnt.EventMessage());

            if (edt_event_sound != null)
                edt_event_sound.SetText(evnt.EventSound());

            if (cmb_event != null)
            {
                cmb_event.ClearItems();

                for (int i = 0; i < (int)MissionEvent.EVENT_TYPE.NUM_EVENTS; i++)
                {
                    cmb_event.AddItem(MissionEvent.EventName(i));
                }

                cmb_event.SetSelection((int)evnt.Event());
            }

            if (cmb_trigger != null)
            {
                cmb_trigger.ClearItems();

                for (int i = 0; i < (int)MissionEvent.EVENT_TRIGGER.NUM_TRIGGERS; i++)
                {
                    cmb_trigger.AddItem(MissionEvent.TriggerName(i));
                }

                cmb_trigger.SetSelection((int)evnt.Trigger());
            }
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                if (btn_accept != null && btn_accept.IsEnabled())
                    OnAccept(null, null);
            }
        }


        // Operations:
        public virtual void SetMission(Mission msn)
        {
            mission = msn;
        }

        public virtual void SetMissionEvent(MissionEvent e)
        {
            evnt = e;
        }
        public virtual void OnEventSelect(ActiveWindow obj, AWEvent e)
        {
            if (cmb_event.GetSelectedIndex() == (int)MissionEvent.EVENT_TYPE.JUMP)
                FillRgnList(cmb_event_target, evnt.EventTarget());

            else
                FillShipList(cmb_event_target, evnt.EventTarget());
        }
        public virtual void OnAccept(ActiveWindow obj, AWEvent e)
        {
            if (mission != null && evnt != null)
            {
                string buf;
                int val;

                if (edt_time != null)
                {
                    buf = edt_time.GetText();

                    float t = 0;
                    float.TryParse(buf, out t);

                    evnt.time = t;
                }

                if (edt_delay != null)
                {
                    buf = edt_delay.GetText();

                    float t = 0;
                    float.TryParse(buf, out t);

                    evnt.delay = t;
                }

                if (edt_event_param != null)
                {
                    buf = edt_event_param.GetText();

                    if (int.TryParse(buf, out val))
                    {
                        evnt.event_param[0] = val;
                        evnt.event_nparams = 1;
                    }
                    else if (buf.StartsWith("("))
                    {
#if TODO
                        Parser parser = new Parser(new BlockReader(buf));
                        Term term = parser.ParseTerm();

                        if (term && term.isArray())
                        {
                            TermArray* val = term.isArray();
                            if (val)
                            {
                                int nelem = val.elements().Count;

                                if (nelem > 10)
                                    nelem = 10;

                                for (int i = 0; i < nelem; i++)
                                    evnt.event_param[i] = (int)(val.elements().at(i).isNumber().value());

                                evnt.event_nparams = nelem;
                            }
                        }
#endif 
                        throw new NotImplementedException();
                    }
                }

                if (edt_event_chance != null)
                {
                    buf = edt_event_chance.GetText();

                    if (!int.TryParse(buf, out val))
                        val = 0;

                    evnt.event_chance = val;
                }

                if (edt_event_message != null)
                    evnt.event_message = edt_event_message.GetText();

                if (edt_event_sound != null)
                    evnt.event_sound = edt_event_sound.GetText();

                if (edt_trigger_param != null)
                {
                    buf = edt_trigger_param.GetText();

                    //ZeroMemory(evnt.trigger_param, sizeof(evnt.trigger_param));

                    if (int.TryParse(buf, out val))
                    {
                        evnt.trigger_param[0] = val;
                        evnt.trigger_nparams = 1;
                    }

                    else if (buf.StartsWith("("))
                    {
#if TODO
                        Parser parser = new Parser(new BlockReader(buf));
                        Term* term = parser.ParseTerm();

                        if (term && term.isArray())
                        {
                            TermArray* val = term.isArray();
                            if (val)
                            {
                                int nelem = val.elements().Count;

                                if (nelem > 10)
                                    nelem = 10;

                                for (int i = 0; i < nelem; i++)
                                    evnt.trigger_param[i] = (int)(val.elements().at(i).isNumber().value());

                                evnt.trigger_nparams = nelem;
                            }
                        }
#endif
                        throw new NotImplementedException();
                    }

                }

                if (cmb_event != null)
                    evnt.evnt = (MissionEvent.EVENT_TYPE)cmb_event.GetSelectedIndex();

                if (cmb_event_ship != null)
                    evnt.event_ship = cmb_event_ship.GetSelectedItem();

                if (cmb_event_source != null)
                    evnt.event_source = cmb_event_source.GetSelectedItem();

                if (cmb_event_target != null)
                    evnt.event_target = cmb_event_target.GetSelectedItem();

                if (cmb_trigger != null)
                    evnt.trigger = (MissionEvent.EVENT_TRIGGER)cmb_trigger.GetSelectedIndex();

                if (cmb_trigger_ship != null)
                    evnt.trigger_ship = cmb_trigger_ship.GetSelectedItem();

                if (cmb_trigger_target != null)
                    evnt.trigger_target = cmb_trigger_target.GetSelectedItem();
            }

            manager.ShowMsnEditDlg();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            manager.ShowMsnEditDlg();
        }


        protected virtual void FillShipList(ComboBox cmb, string seln)
        {
            if (cmb == null) return;
            cmb.ClearItems();

            if (mission == null) return;

            int index = 1;
            int selected_index = 0;
            cmb.AddItem("");

            List<MissionElement> list = mission.GetElements();
            for (int i = 0; i < list.Count; i++)
            {
                MissionElement elem = list[i];

                if (elem.IsSquadron())
                    continue;

                if (elem.Count() == 1)
                {
                    cmb.AddItem(elem.Name());

                    if (elem.Name() == seln)
                        selected_index = index;

                    index++;
                }
                else
                {
                    string ship_name;

                    for (int n = 0; n < elem.Count(); n++)
                    {
                        ship_name = string.Format("{0} {1}", elem.Name(), n + 1);
                        cmb.AddItem(ship_name);

                        if (string.Equals(ship_name, seln, StringComparison.InvariantCultureIgnoreCase))
                            selected_index = index;

                        index++;
                    }
                }
            }

            cmb.SetSelection(selected_index);
        }
        protected virtual void FillRgnList(ComboBox cmb, string seln)
        {
            if (cmb == null) return;
            cmb.ClearItems();

            if (mission == null) return;

            int selected_index = 0;
            int i = 0;

            foreach (StarSystem sys in mission.GetSystemList())
            {
                foreach (OrbitalRegion region in sys.AllRegions())
                {

                    if (region.Name() == seln)
                        selected_index = i;

                    cmb.AddItem(region.Name());
                    i++;
                }
            }

            cmb.SetSelection(selected_index);
        }

        protected MenuScreen manager;

        protected ActiveWindow lbl_id;
        protected EditBox edt_time;
        protected EditBox edt_delay;

        protected ComboBox cmb_event;
        protected ComboBox cmb_event_ship;
        protected ComboBox cmb_event_source;
        protected ComboBox cmb_event_target;
        protected EditBox edt_event_param;
        protected EditBox edt_event_chance;
        protected EditBox edt_event_sound;
        protected EditBox edt_event_message;

        protected ComboBox cmb_trigger;
        protected ComboBox cmb_trigger_ship;
        protected ComboBox cmb_trigger_target;
        protected EditBox edt_trigger_param;

        protected Button btn_accept;
        protected Button btn_cancel;

        protected Mission mission;
        protected MissionEvent evnt;
    }
}