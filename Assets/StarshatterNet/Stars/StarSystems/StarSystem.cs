﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars.exe
    ORIGINAL FILE:      StarSystem.h/StarSystem.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Various heavenly bodies
*/
using System;
using System.Collections.Generic;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using UnityEngine;
using BYTE = System.Byte;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using WORD = System.UInt16;

namespace StarshatterNet.Stars.StarSystems
{
    public class StarSystem
    {
        public StarSystem(string name, Vector3F loc, int iff = 0, Star.SPECTRAL_CLASS s = (Star.SPECTRAL_CLASS)4)
        {
            this.name = name;
            affiliation = iff;
            sky_stars = 0;
            sky_dust = 0;
            this.loc = loc;
            seq = s;
            active_region = null;
            instantiated = false;
            ambient = new Color(0, 0, 0);
            sun_color = new Color(255, 255, 255);
            sun_scale = 1;
            point_stars = null;
            poly_stars = null;
            nebula = null;
            haze = null;
            center = new Orbital(this, "CG", Orbital.OrbitalType.NOTHING, 1.0e35f, 0.0f, 0.0f, null);
            radius = 0.0f;
        }

        //public virtual ~StarSystem();

        // public int operator ==( StarSystem& s)  { return name == s.name; }
        public static bool operator ==(StarSystem left, StarSystem right)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.name == right.name;
        }
        public static bool operator !=(StarSystem left, StarSystem right)
        {
            return !(left == right);
        }
        public override bool Equals(object obj)
        {
            return (obj is StarSystem) && (this == (StarSystem)obj);
        }

        public bool Equals(StarSystem s)
        {
            // Return true if the fields match:
            return this.name == s.name;
        }

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        // operations:
        public virtual void Load()
        {
            CalcStardate();
            active_region = null;
            filename = "Galaxy/" + name + "/" + name;
            ErrLogger.PrintLine("Loading StarSystem: {0}", filename);
            var entry = ReaderSupport.DeserializeAsset<Serialization.StarSystemEntry>(filename);
            if (entry == null)
            {
                ErrLogger.PrintLine("ERROR: invalid star system file '{0}'", name);
                return;
            }
            var ss = entry.STARSYSTEM;
            if (string.IsNullOrWhiteSpace(ss.name) || ss.name != name)
            {
                ErrLogger.PrintLine("ERROR: invalid star system file '{0}'", name);
                return;
            }
            if (ss.sky == null)
            {
                ErrLogger.PrintLine("WARNING: sky struct missing in '{0}'", name);
                return;
            }
            this.sky_poly_stars = ss.sky.poly_stars;
            this.sky_nebula = ss.sky.nebula;
            this.sky_haze = ss.sky.haze;

            this.sky_stars = ss.stars;
            this.ambient = new Color((float)(ss.ambient.x / 255f) * 2.5f, (float)(ss.ambient.y / 255f) * 2.5f, (float)(ss.ambient.z / 255f) * 2.5f);
            this.sky_dust = ss.dust;

            ParseStar(ss.star);

            if (ss.planets == null)
            {
                ErrLogger.PrintLine("WARNING: planet struct missing in '{0}'", name);
                return;
            }
            foreach (var p in ss.planets)
                ParsePlanet(p);
        }
        protected void CheckTextures()
        {
        }
        protected void ParseStar(Serialization.Star s)
        {
            if (s == null)
            {
                ErrLogger.PrintLine("WARNING: star struct missing in '{0}'", name);
                return;
            }
            OrbitalBody star = new OrbitalBody(this, s.name, Orbital.OrbitalType.STAR, s.mass, s.radius, s.orbit, this.center);
            star.map_name = s.map;
            star.tex_name = s.image;
            star.light = s.light;
            star.tscale = s.tscale;
            star.subtype = (int)Star.SPECTRAL_CLASS.G;
            star.retro = s.retro;
            star.rotation = s.rotation * 3600;
            star.color = s.color.ConvertToColor();
            star.back = s.back.ConvertToColor();

            bodies.Add(star);
            primary_star = star;
            primary_planet = null;
            primary_moon = null;

            if (s.orbit > this.radius)
                this.radius = s.orbit;

            if (s.region != null)
                ParseRegion(s.region, star);
        }

        protected void ParsePlanet(Serialization.Planet p)
        {
            if (p == null || string.IsNullOrWhiteSpace(p.name))
                return;

            OrbitalBody planet = new OrbitalBody(this, p.name, Orbital.OrbitalType.PLANET, p.mass, p.radius, p.orbit, primary_star);
            planet.map_name = p.map;
            planet.tex_name = p.image;
            planet.tex_high_res = p.high_res;
            planet.tex_ring = p.img_ring;
            planet.tex_glow = p.glow;
            planet.tex_glow_high_res = p.glow_high_res;
            planet.tex_gloss = p.gloss;
            planet.ring_min = p.minrad;
            planet.ring_max = p.maxrad;
            planet.tscale = p.tscale;
            planet.tilt = p.tilt;
            planet.retro = p.retro;
            planet.luminous = p.luminous;
            planet.rotation = p.rotation * 3600;
            planet.atmosphere = p.atmosphere.ConvertToColor();

            if (primary_star != null)
                primary_star.satellites.Add(planet);
            else
                bodies.Add(planet);

            primary_planet = planet;
            primary_moon = null;

            if (p.orbit > this.radius)
                this.radius = p.orbit;

            if (p.region != null)
                ParseRegion(p.region, planet);

            if (p.terrain != null)
                ParseTerrain(p.terrain, planet);

            foreach (var m in p.moons)
                ParseMoon(m);
        }
        protected void ParseMoon(Serialization.Moon m)
        {
            if (m == null || string.IsNullOrWhiteSpace(m.name))
                return;

            OrbitalBody moon = new OrbitalBody(this, m.name, Orbital.OrbitalType.MOON, m.mass, m.radius, m.orbit, primary_planet);
            moon.map_name = m.map;
            moon.tex_name = m.image;
            moon.tex_high_res = m.high_res;
            moon.tex_glow = m.glow;
            moon.tex_glow_high_res = m.glow_high_res;
            moon.tex_gloss = m.gloss;
            moon.tscale = m.tscale;
            moon.retro = m.retro;
            moon.rotation = m.rotation * 3600;
            moon.tilt = m.tilt;
            moon.atmosphere = m.atmosphere.ConvertToColor();

            if (m.region != null)
                ParseRegion(m.region, moon);

            if (primary_planet != null)
                primary_planet.satellites.Add(moon);
            else
            {
                ErrLogger.PrintLine("WARNING: no planet for moon {0} in '{1}', deleted.", m.name, filename);
                //elete moon;
                moon = null;
            }

            primary_moon = moon;
        }
        protected void ParseRegion(Serialization.Region r, Orbital primary)
        {
            if (r == null || string.IsNullOrWhiteSpace(r.name))
                return;

            OrbitalRegion region = new OrbitalRegion(this, r.name, 0, r.radius, r.orbit, primary);
            region.grid = r.grid;
            region.inclination = r.inclination;
            region.asteroids = r.asteroids;
            if (r.links != null && r.links.Count > 0)
                region.links.AddRange(r.links);
            if (primary != null)
                primary.regions.Add(region);
            else
                regions.Add(region);

            all_regions.Add(region);

            if (r.orbit > this.radius)
                this.radius = r.orbit;
        }
        protected void ParseTerrain(Serialization.Terrain t, Orbital primary)
        {
            if (t == null || string.IsNullOrWhiteSpace(t.name))
                return;

            TerrainRegion region = new TerrainRegion(this, t.name, t.radius, primary);
            region.grid = t.grid;
            region.inclination = t.inclination;
            region.patch_name = t.patch;
            region.patch_texture = t.patch_texture;
            region.noise_tex0 = t.detail_texture_0;
            region.noise_tex1 = t.detail_texture_1;
            region.apron_name = t.apron;
            region.apron_texture = t.apron_texture;
            region.water_texture = t.water_texture;
            region.haze_name = t.haze;
            region.clouds_high = t.clouds_high;
            region.shades_high = t.shades_high;
            region.clouds_low = t.clouds_low;
            region.shades_low = t.shades_low;
            region.scale = t.scale;
            region.mtnscale = t.mtnscale;
            region.fog_density = t.fog_density;
            region.fog_scale = t.fog_scale;
            region.haze_fade = t.haze_fade;
            region.clouds_alt_high = t.clouds_alt_high;
            region.clouds_alt_low = t.clouds_alt_low;

            region.env_texture_positive_x = t.env_texture_positive_x;
            region.env_texture_negative_x = t.env_texture_negative_x;
            region.env_texture_positive_y = t.env_texture_positive_y;
            region.env_texture_negative_y = t.env_texture_negative_y;
            region.env_texture_positive_z = t.env_texture_positive_z;
            region.env_texture_negative_z = t.env_texture_negative_z;

            if (t.layers == null)
            {
                ErrLogger.PrintLine("terrain layer struct missing in '{0}'", name);
                return;
            }
            foreach (var val in t.layers)
                ParseLayer(region, val);

            Weather weather = region.GetWeather();

            weather.SetPeriod(t.weather_period);
            for (int i = 0; i < Weather.NUM_STATES; i++)
                switch (i)
                {
                    case 0: weather.SetChance(i, t.weather_clear); break;
                    case 1: weather.SetChance(i, t.weather_high_clouds); break;
                    case 2: weather.SetChance(i, t.weather_moderate_clouds); break;
                    case 3: weather.SetChance(i, t.weather_overcast); break;
                    case 4: weather.SetChance(i, t.weather_fog); break;
                    case 5: weather.SetChance(i, t.weather_storm); break;
                }


            region.LoadSkyColors(t.sky_color);

            primary.regions.Add(region);
            all_regions.Add(region);

        }

        protected void ParseLayer(TerrainRegion rgn, Serialization.Layer l)
        {
            if (l == null)
            {
                ErrLogger.PrintLine("terrain layer struct missing in '{0}'", name);
                return;
            }
            if (rgn != null)
                rgn.AddLayer(l.height, l.tile, l.detail);
        }

        public virtual void Create()
        {
            if (Game.Server())
                return;

            if (!instantiated)
            {
                ErrLogger.PrintLine("Creating Star System {0}", name);

                //DataLoader loader = DataLoader.GetLoader();
                //loader.SetDataPath(datapath);

                // poly star shell:
                if (!string.IsNullOrWhiteSpace(sky_poly_stars))
                {
                    poly_stars = new Solid();
                    poly_stars.Load(sky_poly_stars, 120);
                    poly_stars.SetLuminous(true);
                    poly_stars.SetInfinite(true);

                    ErrLogger.PrintLine("Celestial Sphere '{0}' loaded ", sky_poly_stars);
                    ErrLogger.PrintLine("   radius: {0} ", poly_stars.Radius());
                }

                if (sky_stars != 0)
                {
                    ErrLogger.PrintLine("Point Stars: {0}", sky_stars);
                    point_stars = new Graphics.General.Stars(sky_stars);
                }

                //loader.SetDataPath(datapath);

                // nebula:
                if (!string.IsNullOrWhiteSpace(sky_nebula))
                {
                    nebula = new Solid();
                    nebula.Load(sky_nebula, 100);
                    nebula.SetLuminous(true);
                    nebula.SetInfinite(true);

                    ErrLogger.PrintLine("Nebular Sky '{0}' loaded", sky_nebula);
                    ErrLogger.PrintLine("   radius: {0}", nebula.Radius());
                }

                // atmospheric haze:
                if (!string.IsNullOrWhiteSpace(sky_haze))
                {
                    //loader.SetDataPath(datapath);
                    haze = new TerrainHaze();
                    haze.Load(sky_haze, 120);

                    haze.SetInfinite(true);

                    ErrLogger.PrintLine("Atmospheric Haze '{0}' loaded", sky_haze);
                    ErrLogger.PrintLine("   radius: {0}", haze.Radius());

                    haze.Hide();
                }

                //loader.SetDataPath(0);

                foreach (var star in bodies)
                {
                    CreateBody(star);

                    foreach (var planet in star.Satellites())
                    {
                        CreateBody(planet);

                        foreach (var moon in planet.Satellites())
                        {
                            CreateBody(moon);
                        }
                    }
                }
            }

            instantiated = true;
        }

        public virtual void Destroy()
        {
            ErrLogger.PrintLine("StarSystem Destroy() is not yet Implemented");
            throw new System.NotImplementedException();
        }


        public virtual void Activate(Scene scene)
        {
            if (!instantiated)
                Create();

            Starshatter stars = Starshatter.GetInstance();

            if (stars == null)
                return;

            if (point_stars != null)
            {
                scene.AddBackground(point_stars);
                point_stars.Hide();
            }

            if (poly_stars != null)
            {
                scene.AddBackground(poly_stars);
            }

            if (stars.Nebula() != 0 && nebula != null)
            {
                scene.AddBackground(nebula);
            }

            if (haze != null)
            {
                scene.AddBackground(haze);
                haze.Hide();
            }

            foreach (var star in bodies)
            {
                scene.AddGraphic(star.rep);
                scene.AddLight(star.light_rep);
                if (nebula != null && stars != null && stars.Nebula() != 0)
                {
                    star.back_light.SetColor(star.back);
                }
                else
                {
                    Color c = new Color(60, 60, 65);
                    star.back_light.SetColor(c);
                }
                scene.AddLight(star.back_light);

                if (nebula != null && stars != null && stars.Nebula() != 0)
                {
                    scene.SetAmbient(ambient);
                }
                else
                {
                    Color32 c = ambient;
                    byte n = (byte)((c.Red() + c.Green() + c.Blue()) / 3f);

                    c = new Color32(n, n, n, 255);
                    scene.SetAmbient(c);
                }

                foreach (var planet in star.Satellites())
                {
                    scene.AddGraphic(planet.rep);

                    foreach (var moon in planet.Satellites())
                    {
                        scene.AddGraphic(moon.rep);
                    }
                }
            }

            ExecFrame();
        }

        public virtual void Deactivate()
        {
            if (!instantiated)
                return;

            active_region = null;

            if (point_stars != null && point_stars.GetScene() != null)
                point_stars.GetScene().DelBackground(point_stars);

            if (poly_stars != null && poly_stars.GetScene() != null)
                poly_stars.GetScene().DelBackground(poly_stars);

            if (nebula != null && nebula.GetScene() != null)
                nebula.GetScene().DelBackground(nebula);

            if (haze != null && haze.GetScene() != null)
            {
                haze.GetScene().DelBackground(haze);
                haze.Hide();
            }

            foreach (var star in bodies)
            {
                if (star.rep != null && star.rep.GetScene() != null)
                    star.rep.GetScene().DelGraphic(star.rep);

                if (star.light_rep != null && star.light_rep.GetScene() != null)
                    star.light_rep.GetScene().DelLight(star.light_rep);

                if (star.back_light != null && star.back_light.GetScene() != null)
                    star.back_light.GetScene().DelLight(star.back_light);

                foreach (var planet in star.Satellites())
                {
                    if (planet.rep != null && planet.rep.GetScene() != null)
                        planet.rep.GetScene().DelGraphic(planet.rep);

                    foreach (var moon in planet.Satellites())
                    {
                        if (moon.rep != null && moon.rep.GetScene() != null)
                            moon.rep.GetScene().DelGraphic(moon.rep);
                    }
                }
            }
        }

        public virtual void ExecFrame()
        {
            CalcStardate();

            foreach (var star in bodies)
                star.Update();

            foreach (var region in regions)
                region.Update();

            // update the graphic reps, relative to the active region:
            if (instantiated && active_region != null)
            {
                Point active_loc = active_region.Location();
                active_loc = active_loc.OtherHand();

                Scene scene = null;
                TerrainRegion trgn = null;
                bool terrain = (active_region.Type() == Orbital.OrbitalType.TERRAIN);
                Matrix33D terrain_orientation = new Matrix33D();
                Matrix33D terrain_transformation;

                if (terrain)
                {
                    trgn = (TerrainRegion)active_region;
                    Color tmp = trgn.SkyColor();
                    Game.SetScreenColor(tmp);

                    tvpn = (active_region.Location() - active_region.Primary().Location());
                    tvpn.Normalize();
                    tvup = new Point(0, 0, -1);
                    tvrt = Point.Cross(tvpn, tvup);
                    tvrt.Normalize();

                    terrain_orientation.Rotate(0, Math.PI / 2, 0);
                    terrain_transformation = new Matrix33D(tvrt, tvup, tvpn);

                    if (point_stars != null)
                    {
                        point_stars.SetOrientation(terrain_transformation);

                        Color32 sky_color = trgn.SkyColor();
                        BYTE sky_red = (BYTE)sky_color.Red();
                        BYTE sky_green = (BYTE)sky_color.Green();
                        BYTE sky_blue = (BYTE)sky_color.Blue();

                        BYTE Max = max3(sky_red, sky_green, sky_blue);
                        BYTE Min = min3(sky_red, sky_green, sky_blue);

                        BYTE lum = (BYTE)(240.0 * (Max + Min) / 510.0);

                        if (lum > 50)
                        {
                            point_stars.Hide();
                        }
                        else
                        {
                            Graphics.General.Stars pstars = (Graphics.General.Stars)point_stars;

                            pstars.Illuminate(1.0 - lum / 50.0);
                            pstars.Show();
                        }

                        scene = point_stars.GetScene();
                    }

                    if (haze != null)
                    {
                        ((TerrainHaze)haze).UseTerrainRegion(trgn);
                        scene = haze.GetScene();
                    }

                    if (scene != null)
                    {
                        scene.SetAmbient(ambient);
                    }

                }
                else
                {
                    Game.SetScreenColor(Color.black);
                }

                double star_alt = 0;

                foreach (OrbitalBody star in bodies)
                {

                    if (active_region.Inclination() != 0)
                    {
                        double distance = (active_region.Location() - star.Location()).Length;
                        star_alt = Math.Sin(active_region.Inclination()) * distance;
                    }

                    if (terrain)
                    {
                        Point sloc = TerrainTransform(star.Location());

                        if (star.rep != null)
                        {
                            star.rep.MoveTo(sloc);

                            PlanetRep pr = (PlanetRep)star.rep;
                            pr.SetDaytime(true);
                        }

                        if (star.light_rep != null)
                        {
                            star.light_rep.MoveTo(sloc);
                            star.light_rep.SetActive(sloc.Y > -100);
                        }

                        if (star.back_light != null)
                        {
                            star.back_light.MoveTo(sloc * -1);
                            star.back_light.SetActive(sloc.Y > -100);
                        }

                        if (trgn != null && star.rep != null)
                        {
                            if (trgn.IsEclipsed())
                                star.rep.Hide();
                            else
                                star.rep.Show();
                        }
                    }

                    else
                    {
                        Point sloc = new Point(star.Location() - active_region.Location()).OtherHand();
                        sloc.Y = star_alt;

                        if (star.rep != null)
                        {
                            star.rep.MoveTo(sloc);

                            PlanetRep pr = (PlanetRep)star.rep;
                            pr.SetDaytime(false);
                        }

                        if (star.light_rep != null)
                        {
                            star.light_rep.MoveTo(sloc);
                            star.light_rep.SetActive(true);
                        }

                        if (star.back_light != null)
                        {
                            star.back_light.MoveTo(sloc * -1);
                            star.back_light.SetActive(true);
                        }
                    }

                    foreach (OrbitalBody planet in star.Satellites())
                    {
                        if (planet.rep != null)
                        {
                            PlanetRep pr = (PlanetRep)planet.rep;

                            if (terrain)
                            {
                                pr.MoveTo(TerrainTransform(planet.Location()));
                                pr.SetOrientation(terrain_orientation);

                                if (planet == active_region.Primary())
                                {
                                    pr.Hide();
                                }

                                else
                                {
                                    pr.Show();
                                    pr.SetDaytime(true);
                                }
                            }
                            else
                            {
                                pr.Show();
                                pr.TranslateBy(active_loc);
                                pr.SetDaytime(false);
                            }
                        }

                        foreach (OrbitalBody moon in planet.Satellites())
                        {
                            if (moon.rep != null)
                            {
                                PlanetRep pr = (PlanetRep)moon.rep;

                                if (terrain)
                                {
                                    pr.MoveTo(TerrainTransform(moon.Location()));
                                    pr.SetOrientation(terrain_orientation);

                                    if (moon == active_region.Primary())
                                    {
                                        pr.Hide();
                                    }

                                    else
                                    {
                                        pr.Show();
                                        pr.SetDaytime(true);
                                    }
                                }
                                else
                                {
                                    pr.Show();
                                    pr.TranslateBy(active_loc);
                                    pr.SetDaytime(false);
                                }
                            }
                        }
                    }
                }
            }
        }

        // accessors:
        public string Name() { return name; }
        public string Govt() { return govt; }
        public string Description() { return description; }
        public int Affiliation() { return affiliation; }
        public Star.SPECTRAL_CLASS Sequence() { return seq; }
        public Vector3F Location() { return loc; }
        public int NumStars() { return sky_stars; }
        public int NumDust() { return sky_dust; }
        public Color Ambient()
        {
            Color result = ambient;
            bool terrain = (active_region != null && active_region.Type() == Orbital.OrbitalType.TERRAIN);

            if (terrain)
            {
                TerrainRegion  trgn = (TerrainRegion )active_region;
                result = trgn.Ambient();

                if (trgn.IsEclipsed())
                    result = result * 0.3f;
            }

            return result;
        }

        public List<OrbitalBody> Bodies() { return bodies; }
        public List<OrbitalRegion> Regions() { return regions; }
        public List<OrbitalRegion> AllRegions() { return all_regions; }
        public OrbitalRegion ActiveRegion() { return active_region; }

        public Orbital FindOrbital(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return null;

            foreach (var star in bodies)
            {
                if (star.Name().Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    return star;

                foreach (var star_rgn in star.Regions())
                {
                    if (star_rgn.Name().Equals(name, StringComparison.InvariantCultureIgnoreCase))
                        return star_rgn;
                }

                foreach (var planet in star.Satellites())
                {
                    if (planet.Name().Equals(name, StringComparison.InvariantCultureIgnoreCase))
                        return planet;

                    foreach (var planet_rgn in planet.Regions())
                    {
                        if (planet_rgn.Name().Equals(name, StringComparison.InvariantCultureIgnoreCase))
                            return planet_rgn;
                    }

                    foreach (var moon in planet.Satellites())
                    {
                        if (moon.Name().Equals(name, StringComparison.InvariantCultureIgnoreCase))
                            return moon;

                        foreach (var moon_rgn in moon.Regions())
                        {
                            if (moon_rgn.Name().Equals(name, StringComparison.InvariantCultureIgnoreCase))
                                return moon_rgn;
                        }
                    }
                }
            }

            foreach (var region in regions)
            {
                if (region.Name().Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    return region;
            }

            return null;
        }

        public OrbitalRegion FindRegion(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return null;

            foreach (var region in all_regions)
            {
                if (region.Name().Equals(name))
                    return region;
            }

            return null;
        }

        public void SetActiveRegion(OrbitalRegion rgn)
        {
            Scene scene = null;

            if (active_region != rgn)
            {
                active_region = rgn;

                if (active_region != null)
                {
                    if (active_region.Type() != Orbital.OrbitalType.TERRAIN)
                    {
                        if (point_stars != null) point_stars.Hide();
                        if (poly_stars != null) poly_stars.Show();
                        if (nebula != null) nebula.Show();
                        if (haze != null) haze.Hide();
                    }
                    else
                    {
                        if (point_stars != null) point_stars.Show();
                        if (poly_stars != null) poly_stars.Hide();
                        if (nebula != null) nebula.Hide();
                        if (haze != null) haze.Show();
                    }

                    if (poly_stars != null)
                    {
                        scene = poly_stars.GetScene();
                        if (scene != null)
                            scene.SetAmbient(ambient);
                    }
                    else if (nebula != null)
                    {
                        scene = nebula.GetScene();
                        if (scene != null)
                            scene.SetAmbient(ambient);
                    }

                    foreach (OrbitalBody star in bodies)
                    {
                        if (star.rep != null)
                            star.rep.Show();
                    }

                    ExecFrame();
                }
                else
                {
                    if (point_stars != null) point_stars.Hide();
                    if (poly_stars != null) poly_stars.Hide();
                    if (nebula != null) nebula.Hide();
                    if (haze != null) haze.Hide();


                    foreach (OrbitalBody star in bodies)
                    {
                        if (star.rep != null)
                            star.rep.Hide();

                        if (star.light_rep != null)
                        {
                            scene = star.light_rep.GetScene();
                            if (scene != null)
                                scene.DelLight(star.light_rep);
                        }

                        if (star.back_light != null)
                        {
                            scene = star.back_light.GetScene();
                            if (scene != null)
                                scene.DelLight(star.back_light);
                        }
                    }
                }
            }
        }

        public static void SetBaseTime(double t, bool absolute = false)
        {
            if (absolute)
            {
                base_time = t;
                CalcStardate();
            }

            else if (t > 0)
            {
                if (t > epoch) t -= epoch;
                base_time = t;
                CalcStardate();
            }
        }

        public static double GetBaseTime()
        {
            return base_time;
        }
        public static double Stardate() { return stardate; }

        public static void CalcStardate()
        {
            if (base_time < 1)
            {
                base_time = (double)DateTime.Now.Ticks / (double)TimeSpan.TicksPerSecond;

                while (base_time < 0)
                    base_time += epoch;
            }


            double gtime = Game.GameTime() / 1000.0;
            double sdate = gtime + base_time + epoch;

            stardate = sdate;
        }

        public double Radius() { return radius; }

        public void SetSunlight(Color color, double brightness = 1)
        {
            sun_color = color;
            sun_scale = brightness;

            foreach (LightNode sun_light in sun_lights)
            {
                sun_light.SetColor(color);
                sun_light.SetIntensity((float)(1.1 * sun_scale));
            }

            foreach (LightNode back_light in back_lights)
            {
                back_light.SetIntensity((float)(0.5 * sun_scale));
            }
        }
        public void SetBacklight(Color color, double brightness = 1)
        {
            foreach (LightNode back_light in back_lights)
            {
                back_light.SetColor(color);
                back_light.SetIntensity((float)(0.5 * brightness));
            }
        }
        public void RestoreTrueSunColor()
        {
            foreach (OrbitalBody star in bodies)
            {
                if (star != null)
                {
                    if (star.light_rep != null)
                    {
                        star.light_rep.SetColor(star.LightColor());
                        star.light_rep.SetIntensity(1.1f);
                    }

                    if (star.back_light != null)
                    {
                        star.back_light.SetColor(star.back);
                        star.back_light.SetIntensity(0.5f);
                    }
                }
            }
        }

        public bool HasLinkTo(StarSystem s)
        {
            foreach (OrbitalRegion rgn in this.all_regions)
            {
                foreach (string t in rgn.Links())
                {
                    if (s.FindRegion(t) != null)
                        return true;
                }
            }

            return false;
        }
        public string GetDataPath() { return datapath; }

#if TODO
        protected void ParseStar(TermStruct* val);
        protected void ParsePlanet(TermStruct* val);
        protected void ParseMoon(TermStruct* val);
        protected void ParseRegion(TermStruct* val);
        protected void ParseTerrain(TermStruct* val);
        protected void ParseLayer(TerrainRegion* rgn, TermStruct* val);
#endif
        protected void CreateBody(OrbitalBody body)
        {
            //DataLoader* loader = DataLoader::GetLoader();
            //loader->SetDataPath(datapath);

            // stars:
            if (body.type == Orbital.OrbitalType.STAR)
            {
                Point starloc = body.loc;
                starloc = starloc.OtherHand();

                PlanetRep  rep = new   PlanetRep(body.tex_name,
                  null,
                  body.radius,
                  starloc,
                  body.tscale);

                rep.SetLuminous(true);
                rep.MoveTo(loc);
                body.rep = rep;

                sun_brightness = body.light;
                sun_color = body.color;

                LightNode sun_light = new  LightNode(1.1f);
                sun_light.SetColor(sun_color);
                sun_light.SetShadow(true);
                sun_light.MoveTo(body.loc);
                sun_light.SetType(LightNode.TYPES.LIGHT_DIRECTIONAL);

                sun_lights.Add(sun_light);
                body.light_rep = sun_light;

                if (body.back != Color.black)
                {
                    LightNode back_light = new LightNode(0.6f);
                    back_light.SetColor(body.back);
                    back_light.SetShadow(false);
                    back_light.MoveTo(body.loc * -1);
                    back_light.SetType(LightNode.TYPES.LIGHT_DIRECTIONAL);

                    back_lights.Add(back_light);
                    body.back_light = back_light;
                }
            }

            // planets and moons:
            else
            {
                Point planetloc = body.loc;
                planetloc = planetloc.OtherHand();

                double rmax = 0;
                double rmin = 0;

                if (body.ring_max > 0)
                {
                    rmax = body.ring_max * body.radius;
                    rmin = body.ring_min * body.radius;
                }

                string surface = body.tex_name;
                string glow = body.tex_glow;

                if (Game.MaxTexSize() >= 512)
                {
                    if (!string.IsNullOrEmpty(body.tex_high_res ))
                        surface = body.tex_high_res;

                    if (!string.IsNullOrEmpty(body.tex_glow_high_res ))
                        glow = body.tex_glow_high_res;
                }

                PlanetRep rep = new PlanetRep(surface,
                  glow,
                  body.radius,
                  planetloc,
                  body.tscale,
                  body.tex_ring,
                  rmin,
                  rmax,
                  body.atmosphere,
                  body.tex_gloss);

                rep.SetStarSystem(this);

                /***
            if (body.luminous) {
                rep->SetLuminous(1);
            }
            ***/

                if (body.tilt != 0)
                {
                    Matrix33F m = new Matrix33F();
                    m.Pitch(body.tilt);

                    rep.SetOrientation(m);
                }

                body.rep = rep;
            }
        }

        protected Vector3D TerrainTransform(Point loc)
        {
            Point result = new Point();

            Point tmp = loc - active_region.Location();

            result.X = Point.Dot(tmp, tvrt);
            result.Z = Point.Dot(tmp, tvup);
            result.Y = Point.Dot(tmp, tvpn);

            return result;
        }
        private static BYTE max3(BYTE a, BYTE b, BYTE c)
        {
            if (a > b)
                if (a > c) return a;
                else return c;
            else
                if (b > c) return b;
            else return c;
        }
        private static BYTE min3(BYTE a, BYTE b, BYTE c)
        {
            if (a < b)
                if (a < c) return a;
                else return c;
            else
                if (b < c) return b;
            else return c;
        }
        protected string filename;
        public string name;
        protected string govt;
        protected string description;
        protected string datapath;
        protected int affiliation;
        protected Star.SPECTRAL_CLASS seq;
        protected Vector3F loc;
        protected double radius;
        protected bool instantiated;

        public int sky_stars;
        public int sky_dust;
        public string sky_poly_stars;
        public string sky_nebula;
        public string sky_haze;
        protected double sky_uscale;
        protected double sky_vscale;
        public Color ambient;
        protected Color sun_color;
        protected double sun_brightness;
        protected double sun_scale;
        protected List<LightNode> sun_lights;
        protected List<LightNode> back_lights;

        protected Graphic point_stars;
        protected Solid poly_stars;
        protected Solid nebula;
        protected Solid haze;

        protected List<OrbitalBody> bodies = new List<OrbitalBody>();
        protected List<OrbitalRegion> regions = new List<OrbitalRegion>();
        protected List<OrbitalRegion> all_regions = new List<OrbitalRegion>();

        protected Orbital center;
        protected OrbitalRegion active_region;

        protected Point tvpn, tvup, tvrt;

        protected static double stardate;
        private static double base_time = 0;
        private static WORD oldcw = 0;
        private static WORD fpcw = 0;
        private const double epoch = 0.5e9;

        private static OrbitalBody primary_star = null;
        private static OrbitalBody primary_planet = null;
        private static OrbitalBody primary_moon = null;
    }

#if DELETEME
    public class StarSystemEntry
    {
        public StarSystemData name;
    }
    [Serializable]
    public class StarSystemData
    {
        public string name;
        public SkyInfo sky;
        public int stars;
        public int dust;
        public Color ambient;
        public StartInfo star;
        public List<OrbitalInfo> orbitals;
        public string comment;
    }

    [Serializable]
    public class SkyInfo
    {
        public string poly_stars;
        public string nebula;
        public string haze;
    }

    [Serializable]
    public class StartInfo
    {
        public string name;
        public string map;

        public string image;
        public double mass;
        public double orbit;
        public double radius;
        public double rotation;
        public double tscale;
        public double light;
        public bool retro;
        public Color color;
        public Color back_color;

    }

    [Serializable]
    public class OrbitalInfo
    {
        public string name;
        public string map;
        public string image;
        public string image_west;
        public string image_east;
        public string glow;
        public string gloss;
        public string high_res;
        public string high_res_west;
        public string high_res_east;
        public string glow_high_res;
        public double mass;
        public double orbit;
        public bool retro;
        public bool luminous;
        public double rotation;
        public double radius;
        public string ring;
        public double minrad;
        public double maxrad;
        public double tscale;
        public double tilt;
        public Color atmosphere;
        public List<OrbitalRegionInfo> region;
        public List<OrbitalInfo> orbital;
    }

    [Serializable]
    public class OrbitalRegionInfo
    {
        public string name;
        public List<string> link;
        public double orbit;
        public double size;
        public double radius;
        public double grid;
        public double inclination;
        public double asteroids;
    }

#endif
    [Serializable]
    public enum StarSystemType { System, Star }

    [Serializable]
    public class StarSystemIndex
    {
        public string name;
        public string type;
        public Vector3F loc;
        public string sys_class;
        public int iff;
        public string comment;
    }

    [Serializable]
    public class StarSystemList
    {
        public List<StarSystemIndex> Galaxy;
    }
}
namespace StarshatterNet.Stars.StarSystems.Serialization
{
    [Serializable]
    public class Color
    {
        public double x;
        public double y;
        public double z;

        public UnityEngine.Color ConvertToColor()
        {
            return new UnityEngine.Color((float)(x / 255f), (float)(y / 255f), (float)(z / 255f));
        }
    }

    [Serializable]
    public class StarSystemEntry
    {
        public StarSystem STARSYSTEM;
    }

    [Serializable]
    public class StarSystem
    {
        public string name;
        public Sky sky;
        public int stars;
        public int dust;
        public Color ambient;
        public Star star;
        public List<Planet> planets;
    }

    [Serializable]
    public class Sky
    {
        public string poly_stars;
        public string nebula;
        public string haze;
    }

    [Serializable]
    public class OrbitalBody
    {
        public string name;
        public string map;
        public string image;
        public string high_res;
        public string glow;
        public string glow_high_res;
        public string img_ring;
        public string gloss;
        public double mass;
        public double minrad;
        public double maxrad;
        public double tilt;
        public double tscale;
        public bool retro;
        public bool luminous;
        public float radius;
        public double orbit;
        public double rotation;
        public double light;
        public Color atmosphere;
        public Region region;
    }

    [Serializable]
    public class Star : OrbitalBody
    {
        public Color color;
        public Color back;
    }

    [Serializable]
    public class Planet : OrbitalBody
    {
        public Terrain terrain;
        public List<Moon> moons;
    }

    [Serializable]
    public class Moon : OrbitalBody
    {
    }

    [Serializable]
    public class Terrain
    {
        public string name;
        public float radius;
        public double grid;

        public string patch;
        public string patch_texture;
        public string detail_texture_0;
        public string detail_texture_1;
        public string apron;
        public string apron_texture;
        public string water_texture;
        public string env_texture_positive_x;
        public string env_texture_negative_x;
        public string env_texture_positive_y;
        public string env_texture_negative_y;
        public string env_texture_positive_z;
        public string env_texture_negative_z;

        public string clouds_high;
        public string clouds_low;
        public string shades_high;
        public string shades_low;
        public string haze;
        public string sky_color;
        public List<Layer> layers;

        public double inclination;
        public double scale;
        public double mtnscale;
        public double haze_fade;
        public double fog_density;
        public double fog_scale;

        public double clouds_alt_high;
        public double clouds_alt_low;

        public double weather_period;
        public double weather_clear;
        public double weather_high_clouds;
        public double weather_moderate_clouds;
        public double weather_overcast;
        public double weather_fog;
        public double weather_storm;
    }

    [Serializable]
    public class Layer
    {
        public string tile;
        public double height;
        public string detail;
    }

    [Serializable]
    public class Region
    {
        public string name;
        public float radius;
        public double grid;
        public double orbit;
        public double inclination;
        public int asteroids;
        public List<string> links;
    }
}