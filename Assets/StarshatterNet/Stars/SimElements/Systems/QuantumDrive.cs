﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NavLight.h/NavLight.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Navigation Lights System class
*/
using StarshatterNet.Stars.Simulator;
using System;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.SimElements
{
    public class QuantumDrive : ShipSystem
    {
        public enum SUBTYPE { QUANTUM, HYPER };
        private static int drive_value = 3;

        public QuantumDrive(SUBTYPE s, double cap, double rate)
            : base(CATEGORY.DRIVE, (int)s, "Quantum", drive_value, (float)cap, (float)cap, (float)rate)
        {
            dst_rgn = null; active_state = ACTIVE_STATES.ACTIVE_READY; warp_fov = 1; jump_time = 0; countdown = 5;
            name = Game.GetText("sys.quantum");
            abrv = Game.GetText("sys.quantum.abrv");

            emcon_power[0] = 0;
            emcon_power[1] = 0;
            emcon_power[2] = 100;
        }

        public QuantumDrive(QuantumDrive d)
            : base(d)
        {
            dst_rgn = null; active_state = ACTIVE_STATES.ACTIVE_READY; warp_fov = 1; jump_time = 0;
            countdown = d.countdown;

            Mount(d);
            SetAbbreviation(d.Abbreviation());

            energy = capacity;
        }


        public enum ACTIVE_STATES
        {
            ACTIVE_READY,
            ACTIVE_COUNTDOWN,
            ACTIVE_PREWARP,
            ACTIVE_POSTWARP
        };

        public void SetDestination(SimRegion rgn, Point loc)
        {
            dst_rgn = rgn;
            dst_loc = loc;
        }

        public bool Engage(bool immediate = false)
        {
            if (active_state == ACTIVE_STATES.ACTIVE_READY && ship != null &&
                    IsPowerOn() && Status() == STATUS.NOMINAL && energy == capacity)
            {

                active_state = ACTIVE_STATES.ACTIVE_COUNTDOWN;
                if (immediate)
                {
                    jump_time = 1;
                    return true;
                }

                jump_time = countdown;

                SimRegion rgn = ship.GetRegion();

                foreach (var s in rgn.Ships())
                {

                    if (s != ship)
                    {
                        double dist = (s.Location() - ship.Location()).Length;

                        if (dist < 25e3)
                            jump_time += 5;

                        else if (dist < 50e3)
                            jump_time += 2;

                        else if (dist < 100e3)
                            jump_time += 1;

                        else if (dist < 200e3)
                            jump_time += 0.5;
                    }
                }

                return true;
            }

            return false;
        }

        public ACTIVE_STATES ActiveState() { return active_state; }
        public double WarpFactor() { return warp_fov; }
        public double JumpTime() { return jump_time; }
        public override void PowerOff()
        {
            base.PowerOff();
            AbortJump();
        }

        public override void ExecFrame(double seconds)
        {
            base.ExecFrame(seconds);

            if (active_state == ACTIVE_STATES.ACTIVE_READY)
                return;

            if (ship != null)
            {
                bool warping = false;

                if (active_state == ACTIVE_STATES.ACTIVE_COUNTDOWN)
                {
                    if (jump_time > 0)
                    {
                        jump_time -= seconds;
                    }

                    else
                    {
                        jump_time = 0;
                        active_state = ACTIVE_STATES.ACTIVE_PREWARP;
                    }
                }

                else if (active_state == ACTIVE_STATES.ACTIVE_PREWARP)
                {
                    if (warp_fov < 5000)
                    {
                        warp_fov *= 1.5;
                    }
                    else
                    {
                        Jump();
                        energy = 0.0f;
                    }

                    warping = true;
                }

                else if (active_state == ACTIVE_STATES.ACTIVE_POSTWARP)
                {
                    if (warp_fov > 1)
                    {
                        warp_fov *= 0.75;
                    }
                    else
                    {
                        warp_fov = 1;
                        active_state = ACTIVE_STATES.ACTIVE_READY;
                    }

                    warping = true;
                }

                if (warping)
                {
                    ship.SetWarp(warp_fov);

                    SimRegion r = ship.GetRegion();

                    foreach (var neighbor in r.Ships())
                    {
                        if (neighbor.IsDropship())
                        {
                            Ship s = neighbor;
                            Point delta = s.Location() - ship.Location();

                            if (delta.Length < 5e3)
                                s.SetWarp(warp_fov);
                        }
                    }
                }
            }
        }

        public double GetCountdown() { return countdown; }
        public void SetCountdown(double d) { countdown = d; }


        protected void Jump()
        {
#if TODO
            Sim.Sim sim = Sim.GetSim();

            if (ship != null && sim != null)
            {
                double dist = 150e3 + RandomHelper.Random(0, 60e3);
                Point esc_vec = dst_rgn.GetOrbitalRegion().Location() -
                dst_rgn.GetOrbitalRegion().Primary().Location();

                esc_vec.Normalize();
                esc_vec *= dist;
                esc_vec += RandomDirection() * Random(15e3, 22e3);

                if ((SUBTYPE)subtype == SUBTYPE.HYPER)
                    sim.CreateExplosion(ship.Location(), new Point(0, 0, 0), Explosion.HYPER_FLASH, 1, 1, ship.GetRegion());
                else
                    sim.CreateExplosion(ship.Location(), new Point(0, 0, 0), Explosion.QUANTUM_FLASH, 1, 0, ship.GetRegion());

                sim.RequestHyperJump(ship, dst_rgn, esc_vec);

                ShipStats stats = ShipStats.Find(ship.Name());
                stats.AddEvent(SimEvent.QUANTUM_JUMP, dst_rgn.Name());
            }

            dst_rgn = null;
            dst_loc = new Point();

            active_state = ACTIVE_STATES.ACTIVE_POSTWARP;
#endif
            throw new NotImplementedException();
        }

        protected void AbortJump()
        {
            active_state = ACTIVE_STATES.ACTIVE_READY;
            jump_time = 0;
            energy = 0;
            warp_fov = 1;

            ship.SetWarp(warp_fov);

            SimRegion r = ship.GetRegion();

            foreach (var neighbor in r.Ships())
            {
                if (neighbor.IsDropship())
                {
                    Ship s = neighbor;
                    Point delta = s.Location() - ship.Location();

                    if (delta.Length < 5e3)
                        s.SetWarp(warp_fov);
                }
            }
        }

        protected ACTIVE_STATES active_state;

        protected double warp_fov;
        protected double jump_time;
        protected double countdown;

        protected SimRegion dst_rgn;
        protected Point dst_loc;
    }
}