﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      FlightDeck.h/FlightDeck.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Everything needed to launch and recover space craft

    See Also: Hangar
*/
using StarshatterNet.Config;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Audio;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.Simulator;
using System;
using System.Collections.Generic;
using UnityEngine;
using DWORD = System.UInt32;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars.SimElements
{
    public class InboundSlot : SimObserver
    {
        public InboundSlot() { }
        public InboundSlot(Ship s, FlightDeck d, int squad, int index)
        {
            ship = s; deck = d; squadron = squad; slot = index; cleared = false; final = false; approach = 0;
            if (ship != null)
                Observe(ship);

            if (deck != null && deck.GetCarrier() != null)
                Observe((SimObject)deck.GetCarrier());
        }

        //public int operator <(const InboundSlot& that)  ;
        //public int operator <=(const InboundSlot& that)  ;
        //public int operator ==(const InboundSlot& that)  ;

        // SimObserver:
        public override bool Update(SimObject obj)
        {
            if ((SimObject.TYPES)obj.Type() == SimObject.TYPES.SIM_SHIP)
            {
                Ship s = (Ship)obj;

                if (s == ship)
                {
                    ship = null;
                }

                // Actually, this can't happen.  The flight deck
                // owns the slot.  When the carrier is destroyed,
                // the flight decks and slots are destroyed before
                // the carrier updates the observers.

                // I'm leaving this block in, just in case.

                else if (deck != null && s == deck.GetCarrier())
                {
                    ship = null;
                    deck = null;
                }
            }

            return base.Update(obj);
        }

        public override string GetObserverName()
        {
            return "InboundSlot";
        }

        public Ship GetShip() { return ship; }
        public FlightDeck GetDeck() { return deck; }
        public int Squadron() { return squadron; }
        public int Index() { return slot; }
        public bool Cleared() { return cleared; }
        public bool Final() { return final; }
        public int Approach() { return approach; }
        public Point Offset() { return offset; }
        public double Distance()
        {
            if (ship != null && deck != null)
            {
                return (ship.Location() - deck.MountLocation()).Length;
            }

            return 1e9;
        }


        public void SetApproach(int a) { approach = a; }
        public void SetOffset(Point p) { offset = p; }
        public void SetFinal(bool f) { final = f; }
        public void Clear(bool clear = true)
        {
            if (ship != null)
                cleared = clear;
        }

        private Ship ship = null;
        private FlightDeck deck = null;
        private int squadron = 0;
        private int slot = 0;
        private bool cleared = false;
        private bool final = false;
        private int approach = 0;
        private Point offset;
    }

    public class FlightDeckSlot
    {
        public FlightDeckSlot() { }
        //int operator ==(const FlightDeckSlot& that) const { return this == &that; }

        public Ship ship;
        public Point spot_rel;
        public Point spot_loc;

        public FlightDeck.FLIGHT_SLOT_STATE state = 0;
        public int sequence = 0;
        public double time = 0;
        public double clearance;
        public DWORD filter = 0xf;
    }

    public class FlightDeck : ShipSystem, ISimObserver
    {
        public FlightDeck()
            : base(CATEGORY.FLIGHT_DECK, (int)FLIGHT_DECK_MODE.FLIGHT_DECK_LAUNCH, "Flight Deck", 1, 1)
        {
            name = Game.GetText("sys.flight-deck");
            abrv = Game.GetText("sys.flight-deck.abrv");
        }
        public FlightDeck(FlightDeck s) : base(s)
        {
            carrier = null; index = 0; start_rel = s.start_rel;
            end_rel = s.end_rel; cam_rel = s.cam_rel;
            cycle_time = s.cycle_time;
            num_slots = s.num_slots; slots = null;
            num_hoops = 0; hoops = null; azimuth = s.azimuth; light = null;
            num_catsounds = 0; num_approach_pts = s.num_approach_pts;
            Mount(s);

            slots = new FlightDeckSlot[num_slots];
            for (int i = 0; i < num_slots; i++)
            {
                slots[i].spot_rel = s.slots[i].spot_rel;
                slots[i].filter = s.slots[i].filter;
            }

            for (int i = 0; i < NUM_APPROACH_PTS; i++)
                approach_rel[i] = s.approach_rel[i];

            for (int i = 0; i < 2; i++)
                runway_rel[i] = s.runway_rel[i];

            if (s.light != null)
            {
                //TODO light = s.light.Clone();
                light = s.light;
            }
        }
        // ~FlightDeck();

        public enum FLIGHT_DECK_MODE { FLIGHT_DECK_LAUNCH, FLIGHT_DECK_RECOVERY };
        public enum FLIGHT_SLOT_STATE { CLEAR, READY, QUEUED, LOCKED, LAUNCH, DOCKING };
        public const int NUM_APPROACH_PTS = 8;

        private static bool initialized = false;
        private static Sound tire_sound = null;
        private static Sound catapult_sound = null;

        public static void Initialize()
        {
            if (initialized) return;
            string path = "Sounds/";
            const Sound.FlagEnum SOUND_FLAGS = Sound.FlagEnum.LOCALIZED | Sound.FlagEnum.LOC_3D;

            tire_sound = Sound.CreateStream(path + "Tires", SOUND_FLAGS);
            catapult_sound = Sound.CreateStream(path + "Catapult", SOUND_FLAGS);

            if (tire_sound != null)
                tire_sound.SetMaxDistance(2.5e3f);

            if (catapult_sound != null)
                catapult_sound.SetMaxDistance(0.5e3f);

            initialized = true;
        }

        public static void Close()
        {
            //delete tire_sound;
            // catapult_sound;

            tire_sound = null;
            catapult_sound = null;
        }


        public override void ExecFrame(double seconds)
        {
            base.ExecFrame(seconds);

            bool advance_queue = false;
            int max_vol = AudioConfig.EfxVolume();
            int volume = -1000;
            Sim sim = Sim.GetSim();

            if (volume > max_vol)
                volume = max_vol;

            // update ship status:
            for (int i = 0; i < num_slots; i++)
            {
                FlightDeckSlot slot = slots[i];
                Ship slot_ship = null;

                if (slot.ship == null)
                {
                    slot.state = FlightDeck.FLIGHT_SLOT_STATE.CLEAR;
                }
                else
                {
                    slot_ship = slot.ship;
                    slot_ship.SetThrottle(0);
                }

                switch (slot.state)
                {
                    case FlightDeck.FLIGHT_SLOT_STATE.CLEAR:
                        if (slot.time > 0)
                        {
                            slot.time -= seconds;
                        }
                        else if (IsRecoveryDeck())
                        {
                            GrantClearance();
                        }
                        break;

                    case FlightDeck.FLIGHT_SLOT_STATE.READY:
                        {
                            CameraNode c = carrier.Cam().Clone();
                            c.Yaw(azimuth);

                            if (slot_ship != null)
                            {
                                slot_ship.CloneCam(c);
                                slot_ship.MoveTo(slot.spot_loc);
                                slot_ship.TranslateBy(carrier.Cam().vup() * (float)slot.clearance);
                                slot_ship.SetVelocity(carrier.Velocity());
                            }
                            slot.time = 0;
                        }
                        break;

                    case FlightDeck.FLIGHT_SLOT_STATE.QUEUED:
                        if (slot.time > 0)
                        {
                            CameraNode c = carrier.Cam().Clone();
                            c.Yaw(azimuth);

                            slot.time -= seconds;
                            if (slot_ship != null)
                            {
                                slot_ship.CloneCam(c);
                                slot_ship.MoveTo(slot.spot_loc);
                                slot_ship.TranslateBy(carrier.Cam().vup() * (float)slot.clearance);
                                slot_ship.SetFlightPhase(OP_MODE.ALERT);
                            }
                        }

                        if (slot.sequence == 1 && slot.time <= 0)
                        {
                            bool clear_for_launch = true;
                            for (int j = 0; j < num_slots; j++)
                                if (slots[j].state == FlightDeck.FLIGHT_SLOT_STATE.LOCKED)
                                    clear_for_launch = false;

                            if (clear_for_launch)
                            {
                                slot.sequence = 0;
                                slot.state = FlightDeck.FLIGHT_SLOT_STATE.LOCKED;
                                slot.time = cycle_time;
                                if (slot_ship != null)
                                    slot_ship.SetFlightPhase(OP_MODE.LOCKED);
                                num_catsounds = 0;

                                advance_queue = true;
                            }
                        }
                        break;

                    case FlightDeck.FLIGHT_SLOT_STATE.LOCKED:
                        if (slot.time > 0)
                        {
                            slot.time -= seconds;

                            if (slot_ship != null)
                            {
                                double ct4 = cycle_time / 4;

                                double dx = start_rel.X - slot.spot_rel.X;
                                double dy = start_rel.Z - slot.spot_rel.Z;
                                double dyaw = Math.Atan2(dx, dy) - azimuth;

                                CameraNode c = carrier.Cam().Clone();
                                c.Yaw(azimuth);

                                // rotate:
                                if (slot.time > 3 * ct4)
                                {
                                    double step = 1 - (slot.time - 3 * ct4) / ct4;
                                    c.Yaw(dyaw * step);
                                    slot_ship.CloneCam(c);
                                    slot_ship.MoveTo(slot.spot_loc);
                                    slot_ship.TranslateBy(carrier.Cam().vup() * (float)slot.clearance);

                                    if (carrier.IsGroundUnit())
                                    {
                                        slot_ship.SetThrottle(25);
                                    }

                                    else if (num_catsounds < 1)
                                    {
                                        if (catapult_sound != null)
                                        {
                                            Sound sound = catapult_sound.Duplicate();
                                            sound.SetLocation(slot_ship.Location());
                                            sound.SetVolume(volume);
                                            sound.Play();
                                        }
                                        num_catsounds = 1;
                                    }
                                }

                                // translate:
                                else if (slot.time > 2 * ct4)
                                {
                                    double step = (slot.time - 2 * ct4) / ct4;

                                    Point loc = start_point +
                                    (slot.spot_loc - start_point) * (float)step;

                                    c.Yaw(dyaw);
                                    slot_ship.CloneCam(c);
                                    slot_ship.MoveTo(loc);
                                    slot_ship.TranslateBy(carrier.Cam().vup() * (float)slot.clearance);

                                    if (carrier.IsGroundUnit())
                                    {
                                        slot_ship.SetThrottle(25);
                                    }

                                    else if (num_catsounds < 2)
                                    {
                                        if (catapult_sound != null)
                                        {
                                            Sound sound = catapult_sound.Duplicate();
                                            sound.SetLocation(slot_ship.Location());
                                            sound.SetVolume(volume);
                                            sound.Play();
                                        }
                                        num_catsounds = 2;
                                    }
                                }

                                // rotate:
                                else if (slot.time > ct4)
                                {
                                    double step = (slot.time - ct4) / ct4;
                                    c.Yaw(dyaw * step);
                                    slot_ship.CloneCam(c);
                                    slot_ship.MoveTo(start_point);
                                    slot_ship.TranslateBy(carrier.Cam().vup() * (float)slot.clearance);

                                    if (carrier.IsGroundUnit())
                                    {
                                        slot_ship.SetThrottle(25);
                                    }

                                    else if (num_catsounds < 3)
                                    {
                                        if (catapult_sound != null)
                                        {
                                            Sound sound = catapult_sound.Duplicate();
                                            sound.SetLocation(slot_ship.Location());
                                            sound.SetVolume(volume);
                                            sound.Play();
                                        }
                                        num_catsounds = 3;
                                    }
                                }

                                // wait:
                                else
                                {
                                    slot_ship.SetThrottle(100);
                                    slot_ship.CloneCam(c);
                                    slot_ship.MoveTo(start_point);
                                    slot_ship.TranslateBy(carrier.Cam().vup() * (float)slot.clearance);
                                }

                                slot_ship.SetFlightPhase(OP_MODE.LOCKED);
                            }
                        }
                        else
                        {
                            slot.state = FlightDeck.FLIGHT_SLOT_STATE.LAUNCH;
                            slot.time = 0;

                        }
                        break;

                    case FlightDeck.FLIGHT_SLOT_STATE.LAUNCH:
                        LaunchShip(slot_ship);
                        break;

                    case FlightDeck.FLIGHT_SLOT_STATE.DOCKING:
                        if (slot_ship != null && !slot_ship.IsAirborne())
                        {
                            // do arresting gear stuff:
                            if (Ship.GetFlightModel() == FLIGHT_MODEL.FM_ARCADE)
                                slot_ship.ArcadeStop();

                            slot_ship.SetVelocity(carrier.Velocity());
                        }

                        if (slot.time > 0)
                        {
                            slot.time -= seconds;
                        }
                        else
                        {
                            if (slot_ship != null)
                            {
                                slot_ship.SetFlightPhase(OP_MODE.DOCKED);
                                slot_ship.Stow();

                                NetUtil.SendObjKill(slot_ship, carrier, NetObjKill.KillType.KILL_DOCK, index);
                            }

                            Clear(i);
                        }
                        break;

                    default:
                        break;
                }
            }

            if (advance_queue)
            {
                for (int i = 0; i < num_slots; i++)
                {
                    FlightDeckSlot slot = slots[i];
                    if (slot.state == FlightDeck.FLIGHT_SLOT_STATE.QUEUED && slot.sequence > 1)
                        slot.sequence--;
                }
            }
        }

        public void SetCarrier(Ship s) { ship = carrier = s; }
        public void SetIndex(int n) { index = n; }

        public virtual int SpaceLeft(CLASSIFICATION type)
        {
            int space_left = 0;

            for (int i = 0; i < num_slots; i++)
                if (slots[i].ship == null && (slots[i].filter & (uint)type) != 0)
                    space_left++;

            return space_left;
        }


        public virtual bool Spot(Ship s, ref int index)
        {
            if (s == null)
                return false;

            if (index < 0)
            {
                for (int i = 0; i < num_slots; i++)
                {
                    if (slots[i].ship == null && (slots[i].filter & (uint)s.Class()) != 0)
                    {
                        index = i;
                        break;
                    }
                }
            }

            if (index >= 0 && index < num_slots && slots[index].ship == null)
            {
                slots[index].state = FLIGHT_SLOT_STATE.READY;
                slots[index].ship = s;
                slots[index].clearance = 0;

                if (s.GetGear() != null)
                    slots[index].clearance = s.GetGear().GetClearance();

                if (IsRecoveryDeck() && !s.IsAirborne())
                {
                    s.SetVelocity(s.Velocity() * 0.01f);   // hit the brakes!
                }

                if (!IsRecoveryDeck())
                {
                    CameraNode work = carrier.Cam().Clone();
                    work.Yaw(azimuth);

                    s.CloneCam(work);
                    s.MoveTo(slots[index].spot_loc);

                    LandingGear g = s.GetGear();
                    if (g != null)
                    {
                        g.SetState(LandingGear.GEAR_STATE.GEAR_DOWN);
                        g.ExecFrame(0);
                        s.TranslateBy(carrier.Cam().vup() * (float)slots[index].clearance);
                    }

                    s.SetFlightPhase(OP_MODE.ALERT);
                }

                s.SetCarrier(carrier, this);
                Observe(s);

                return true;
            }

            return false;
        }

        public virtual bool Clear(int index)
        {
            if (index >= 0 && index < num_slots && slots[index].ship != null)
            {
                Ship s = slots[index].ship;

                slots[index].ship = null;
                slots[index].time = cycle_time;
                slots[index].state = FLIGHT_SLOT_STATE.CLEAR;

                foreach (var iter in recovery_queue)
                {
                    if (iter.GetShip() == s)
                    {
                        //delete iter.removeItem(); // ??? SHOULD DELETE HERE ???
                        break;
                    }
                }

                return true;
            }

            return false;
        }

        public virtual bool Launch(int index)
        {
            if ((FLIGHT_DECK_MODE)subtype == FLIGHT_DECK_MODE.FLIGHT_DECK_LAUNCH && index >= 0 && index < num_slots)
            {
                FlightDeckSlot slot = slots[index];

                if (slot.ship != null && slot.state == FLIGHT_SLOT_STATE.READY)
                {
                    int last = 0;
                    FlightDeckSlot last_slot = null;
                    FlightDeckSlot lock_slot = null;

                    for (int i = 0; i < num_slots; i++)
                    {
                        FlightDeckSlot s = slots[i];

                        if (s.state == FLIGHT_SLOT_STATE.QUEUED && s.sequence > last)
                        {
                            last = s.sequence;
                            last_slot = s;
                        }

                        else if (s.state == FLIGHT_SLOT_STATE.LOCKED)
                        {
                            lock_slot = s;
                        }
                    }

                    // queue the slot for launch:
                    slot.state = FLIGHT_SLOT_STATE.QUEUED;
                    slot.sequence = last + 1;
                    slot.time = 0;

                    if (last_slot != null)
                        slot.time = last_slot.time + cycle_time;

                    else if (lock_slot != null)
                        slot.time = lock_slot.time;

                    return true;
                }
            }

            return false;
        }

        public virtual bool LaunchShip(Ship slot_ship)
        {
#if TODO
            FlightDeckSlot slot = null;
            Sim.Sim sim = Sim.Sim.GetSim();
            if (slot_ship != null)
            {
                for (int i = 0; i < num_slots; i++)
                {
                    if (slots[i].ship == slot_ship)
                        slot = slots[i];
                }

                // only suggest a launch point if no flight plan has been filed...
                if (slot_ship.GetElement().FlightPlanLength() == 0)
                {
                    Point departure = end_point;
                    departure = departure.OtherHand();

                    Instruction launch_point = new Instruction(carrier.GetRegion(), departure, Instruction.LAUNCH);
                    launch_point.SetSpeed(350);

                    slot_ship.SetLaunchPoint(launch_point);
                }

                if (!slot_ship.IsAirborne())
                {
                    Point cat;
                    if (Math.Abs(azimuth) < MathHelper.ToRadians(5))
                    {
                        cat = carrier.Heading() * 300;
                    }
                    else
                    {
                        CameraNode c = carrier.Cam().Clone();
                        c.Yaw(azimuth);
                        cat = c.vpn() * 300;
                    }

                    slot_ship.SetVelocity(carrier.Velocity() + cat);
                    slot_ship.SetFlightPhase(OP_MODE.LAUNCH);
                }
                else
                {
                    slot_ship.SetFlightPhase(OP_MODE.TAKEOFF);
                }
 
                Director dir = slot_ship.GetDirector();
                if (dir != null && dir.Type() == ShipCtrl.DIR_TYPE)
                {
                    ShipCtrl ctrl = (ShipCtrl)dir;
                    ctrl.Launch();
                }

                ShipStats c = ShipStats.Find(carrier.Name());
                if (c) c.AddEvent(SimEvent.LAUNCH_SHIP, slot_ship.Name());

                ShipStats stats = ShipStats.Find(slot_ship.Name());
                if (stats)
                {
                    stats.SetRegion(carrier.GetRegion().Name());
                    stats.SetType(slot_ship.Design().name);

                    if (slot_ship.GetElement() != null)
                    {
                        Element elem = slot_ship.GetElement();
                        stats.SetRole(Mission.RoleName(elem.Type()));
                        stats.SetCombatGroup(elem.GetCombatGroup());
                        stats.SetCombatUnit(elem.GetCombatUnit());
                        stats.SetElementIndex(slot_ship.GetElementIndex());
                    }

                    stats.SetIFF(slot_ship.GetIFF());
                    stats.AddEvent(SimEvent.LAUNCH, carrier.Name());

                    if (slot_ship == sim.GetPlayerShip())
                        stats.SetPlayer(true);
                }

                sim.ProcessEventTrigger(MissionEvent.TRIGGER_LAUNCH, 0, slot_ship.Name());

                if (slot != null)
                {
                    slot.ship = null;
                    slot.state = FLIGHT_SLOT_STATE.CLEAR;
                    slot.sequence = 0;
                    slot.time = 0;
                }

                return true;
            }

            return false;
#endif
            throw new NotImplementedException();
        }

        public virtual bool Recover(Ship s)
        {
            if (s != null && (FLIGHT_DECK_MODE)subtype == FLIGHT_DECK_MODE.FLIGHT_DECK_RECOVERY)
            {
                if (OverThreshold(s))
                {
                    if (s.GetFlightPhase() < OP_MODE.RECOVERY)
                    {
                        s.SetFlightPhase(OP_MODE.RECOVERY);
                        s.SetCarrier(carrier, this); // let ship know which flight deck it's in (needed for dock cam)
                    }

                    // are we there yet?
                    if (s.GetFlightPhase() >= OP_MODE.ACTIVE && s.GetFlightPhase() < OP_MODE.DOCKING)
                    {
                        if (slots[0].ship == null)
                        { // only need to ask this on approach
                            double dock_distance = (s.Location() - MountLocation()).Length;

                            if (s.IsAirborne())
                            {
                                double altitude = s.Location().Y - MountLocation().Y;
                                if (dock_distance < Radius() * 3 && altitude < s.Radius())
                                    Dock(s);
                            }
                            else
                            {
                                if (dock_distance < s.Radius())
                                    Dock(s);
                            }
                        }
                    }

                    return true;
                }
                else
                {
                    if (s.GetFlightPhase() == OP_MODE.RECOVERY)
                        s.SetFlightPhase(OP_MODE.ACTIVE);
                }
            }

            return false;
        }

        public virtual bool Dock(Ship s)
        {
            if (s != null && (FLIGHT_DECK_MODE)subtype == FLIGHT_DECK_MODE.FLIGHT_DECK_RECOVERY && slots[0].time <= 0)
            {
                int index = 0;
                if (Spot(s, ref index))
                {
                    s.SetFlightPhase(OP_MODE.DOCKING);
                    s.SetCarrier(carrier, this);

                    // hard landings?
                    if (Ship.GetLandingModel() == 0)
                    {
                        double base_damage = s.Design().integrity;

                        // did player do something stupid?
                        if (s.GetGear() != null && !s.IsGearDown())
                        {
                            ErrLogger.PrintLine("FlightDeck.Dock({0}) Belly landing!", s.Name());
                            s.InflictDamage(0.5 * base_damage);
                        }

                        double docking_deflection = Math.Abs(carrier.Cam().vup().Y - s.Cam().vup().Y);

                        if (docking_deflection > 0.35)
                        {
                            ErrLogger.PrintLine("Landing upside down? y = {0:0.000}", docking_deflection);
                            s.InflictDamage(0.8 * base_damage);
                        }

                        // did incoming ship exceed safe landing parameters?
                        if (s.IsAirborne())
                        {
                            if (s.Velocity().Y < -20)
                            {
                                ErrLogger.PrintLine("FlightDeck.Dock({0}) Slammed it!", s.Name());
                                s.InflictDamage(0.1 * base_damage);
                            }
                        }

                        // did incoming ship exceed safe docking speed?
                        else
                        {
                            Point delta_v = s.Velocity() - carrier.Velocity();
                            double excess = (delta_v.Length - 100);

                            if (excess > 0)
                                s.InflictDamage(excess);
                        }
                    }

                    if (s.IsAirborne())
                    {
                        if (s == Sim.GetSim().GetPlayerShip() && tire_sound != null)
                        {
                            Sound sound = tire_sound.Duplicate();
                            sound.Play();
                        }
                    }

                    if (s.GetDrive() != null)
                        s.GetDrive().PowerOff();

                    slots[index].state = FLIGHT_SLOT_STATE.DOCKING;
                    slots[index].time = 5;

                    if (s.IsAirborne())
                        slots[index].time = 7.5;
                    return true;
                }
            }

            return false;
        }

        public virtual int Inbound(InboundSlot s)
        {
#if TODO
            if (s != null && s.GetShip() != null)
            {
                Ship inbound = s.GetShip();

                if (recovery_queue.Contains(s))
                {
                    InboundSlot orig = s;
                    s = recovery_queue.Find(s);
                    //delete orig;
                }
                else
                {
                    recovery_queue.Add(s);
                    Observe(s.GetShip());
                }

                inbound.SetInbound(s);

                // find the best initial approach point for this ship:
                double current_distance = 1e9;
                for (int i = 0; i < num_approach_pts; i++)
                {
                    double distance = (inbound.Location() - approach_point[i]).Length;
                    if (distance < current_distance)
                    {
                        current_distance = distance;
                        s.SetApproach(i);
                    }
                }

                Point offset = new Point(rand() - 16000, rand() - 16000, rand() - 16000);
                offset.Normalize();
                offset *= 200;

                s.SetOffset(offset);

                // *** DEBUG ***
                // PrintQueue();

                // if the new guy is first in line, and the deck is almost ready,
                // go ahead and clear him for approach now
                recovery_queue.Sort();
                if (recovery_queue[0] == s && !s.Cleared() && slots[0].time <= 3)
                    s.Clear();

                return recovery_queue.index(s) + 1;
            }

            return 0;
#endif
            throw new NotImplementedException();
        }

        public virtual void GrantClearance()
        {
#if TODO
            if (recovery_queue.Count > 0)
            {
                if (recovery_queue[0].Cleared() && recovery_queue[0].Distance() > 45e3)
                {
                    recovery_queue[0].Clear(false);
                }

                if (!recovery_queue[0].Cleared())
                {
                    recovery_queue.Sort();

                    if (recovery_queue[0].Distance() < 35e3)
                    {
                        recovery_queue[0].Clear();

                        Ship dst = recovery_queue[0].GetShip();

                        RadioMessage clearance = new RadioMessage(dst, carrier, RadioMessage.CALL_CLEARANCE);
                        clearance.SetInfo("for final approach to " + Name());
                        RadioTraffic.Transmit(clearance);
                    }
                }
            }
#endif
            throw new NotImplementedException();
        }

        public virtual void AddSlot(Point spot, DWORD filter = 0xf)
        {
            if (slots == null)
                slots = new FlightDeckSlot[10];

            slots[num_slots].spot_rel = spot;
            slots[num_slots].filter = filter;
            num_slots++;
        }


        public virtual bool IsLaunchDeck() { return (FLIGHT_DECK_MODE)subtype == FLIGHT_DECK_MODE.FLIGHT_DECK_LAUNCH; }
        public virtual void SetLaunchDeck() { subtype = (int)FLIGHT_DECK_MODE.FLIGHT_DECK_LAUNCH; }
        public virtual bool IsRecoveryDeck() { return (FLIGHT_DECK_MODE)subtype == FLIGHT_DECK_MODE.FLIGHT_DECK_RECOVERY; }
        public virtual void SetRecoveryDeck() { subtype = (int)FLIGHT_DECK_MODE.FLIGHT_DECK_RECOVERY; }

        public Point BoundingBox() { return box; }
        public Point ApproachPoint(int i) { return approach_point[i]; }
        public Point RunwayPoint(int i) { return runway_point[i]; }
        public Point StartPoint() { return start_point; }
        public Point EndPoint() { return end_point; }
        public Point CamLoc() { return cam_loc; }
        public double Azimuth() { return azimuth; }

        public virtual void SetBoundingBox(Point dimensions) { box = dimensions; }
        public virtual void SetApproachPoint(int i, Point loc)
        {
            if (i >= 0 && i < NUM_APPROACH_PTS)
            {
                approach_rel[i] = loc;

                if (i >= num_approach_pts)
                    num_approach_pts = i + 1;
            }
        }

        public virtual void SetRunwayPoint(int i, Point loc)
        {
            if (i >= 0 && i < 2)
                runway_rel[i] = loc;
        }

        public virtual void SetStartPoint(Point loc)
        {
            start_rel = loc;
        }

        public virtual void SetEndPoint(Point loc)
        {
            end_rel = loc;
        }

        public virtual void SetCamLoc(Point loc)
        {
            cam_rel = loc;
        }

        public virtual void SetCycleTime(double time)
        {
            cycle_time = time;
        }

        public virtual void SetAzimuth(double az) { azimuth = az; }
        public virtual void SetLight(double l)
        {
#if TODO
            LIGHT_DESTROY(light);
            light = new Light((float)l);
#endif
            throw new NotImplementedException();
        }


        public override void Orient(Physical rep)
        {
            base.Orient(rep);
#if TODO
            Matrix33F orientation = rep.Cam().Orientation();
            Point loc = rep.Location();

            start_point = (orientation * start_rel * ) + loc; // TODO matrix * vector multiplication order
            end_point = (orientation * end_rel) + loc; // TODO matrix * vector multiplication order
            cam_loc = (orientation * cam_rel) + loc; // TODO matrix * vector multiplication order

            for (int i = 0; i < num_approach_pts; i++)
                approach_point[i] = (orientation * approach_rel[i]) + loc; // TODO matrix * vector multiplication order

            for (int i = 0; i < num_slots; i++)
                slots[i].spot_loc = (orientation * slots[i].spot_rel) + loc; // TODO matrix * vector multiplication order

            if (IsRecoveryDeck())
            {
                if (carrier.IsAirborne())
                {
                    runway_point[0] = (orientation * runway_rel[0]) + loc; // TODO matrix * vector multiplication order
                    runway_point[1] = (orientation * runway_rel[1]) + loc; // TODO matrix * vector multiplication order
                }

                if (num_hoops < 1)
                {
                    num_hoops = 4;
                    hoops = new Hoop[num_hoops];
                }

                Point hoop_vec = start_point - end_point;
                double hoop_d = hoop_vec.Normalize() / num_hoops;

                orientation.Yaw(azimuth);

                for (int i = 0; i < num_hoops; i++)
                {
                    hoops[i].MoveTo(end_point + hoop_vec * (i + 1) * hoop_d);
                    hoops[i].SetOrientation(orientation);
                }
            }

            if (light != null)
                light.MoveTo(mount_loc);
#endif
            throw new NotImplementedException();
        }

        // SimObserver:
        public virtual bool Update(SimObject obj)
        {
            if ((SimObject.TYPES)obj.Type() == SimObject.TYPES.SIM_SHIP)
            {
                Ship s = (Ship)obj;

                foreach (InboundSlot recovery_slot in recovery_queue)
                {

                    if (recovery_slot.GetShip() == s || recovery_slot.GetShip() == null)
                    {
                        // delete iter.removeItem();
                    }
                }

                for (int i = 0; i < num_slots; i++)
                {
                    FlightDeckSlot slot = slots[i];

                    if (slot.ship == s)
                    {
                        slot.ship = null;
                        slot.state = 0;
                        slot.sequence = 0;
                        slot.time = 0;
                        break;
                    }
                }
            }

            return simObserver.Update(obj);
        }

        public virtual string GetObserverName()
        {
            return Name();
        }


        // accessors:
        public int NumSlots() { return num_slots; }
        public double TimeRemaining(int index)
        {
            if (index >= 0 && index < num_slots)
                return slots[index].time;

            return 0;
        }

        public FlightDeck.FLIGHT_SLOT_STATE State(int index)
        {
            if (index >= 0 && index < num_slots)
                return slots[index].state;

            return 0;
        }

        public int Sequence(int index)
        {
            if (index >= 0 && index < num_slots)
                return slots[index].sequence;

            return 0;
        }

        public Ship GetCarrier() { return carrier; }
        public int GetIndex() { return index; }
        public Ship GetShip(int index)
        {
            if (index >= 0 && index < num_slots)
                return slots[index].ship;

            return null;
        }

        public int NumHoops() { return num_hoops; }
        public Hoop[] GetHoops() { return hoops; }
        public LightNode GetLight() { return light; }

        public List<InboundSlot> GetRecoveryQueue() { return recovery_queue; }
        public void PrintQueue()
        {
            ErrLogger.PrintLine();
            ErrLogger.PrintLine("Recovery Queue for {0}", Name());
            if (recovery_queue.Count < 1)
            {
                ErrLogger.PrintLine("  (empty)");
                ErrLogger.PrintLine();
                return;
            }

            for (int i = 0; i < recovery_queue.Count; i++)
            {
                InboundSlot s = recovery_queue[i];

                if (s == null)
                {
                    ErrLogger.PrintLine("  {0:0.00}. null", i);
                }
                else if (s.GetShip() == null)
                {
                    ErrLogger.PrintLine("  {0:0.00}. ship is null ", i);
                }
                else
                {
                    double d = (s.GetShip().Location() - MountLocation()).Length;
                    ErrLogger.PrintLine("  {0:0.00}. {1} {2:-10} {3} km ", i, s.Cleared() ? '*' : ' ', s.GetShip().Name(), (int)(d / 1000));
                }
            }

            ErrLogger.PrintLine();
        }

        public bool OverThreshold(Ship s)
        {
            if (carrier.IsAirborne())
            {
                if (s.AltitudeAGL() > s.Radius() * 4)
                    return false;

                Point sloc = s.Location();

                // is ship between the markers?
                double distance = 1e9;

                Point d0 = runway_point[0] - sloc;
                Point d1 = runway_point[1] - sloc;
                double d = Point.Dot(d0, d1);
                bool between = d < 0;

                if (between)
                {
                    // distance from point to line:
                    Point src = runway_point[0];
                    Point dir = runway_point[1] - src;
                    Point w = Point.Cross((sloc - src), dir);

                    distance = w.Length / dir.Length;
                }

                return distance < Radius();
            }

            else
            {
                return (s.Location() - MountLocation()).Length < (s.Radius() + Radius());
            }
        }

        public bool ContainsPoint(Point p)
        {
            return (p - MountLocation()).Length < Radius();
        }


        public void Observe(SimObject obj)
        {
            throw new NotImplementedException();
        }

        public void Ignore(SimObject obj)
        {
            throw new NotImplementedException();
        }

        protected Ship carrier;
        protected int index;
        protected int num_slots;
        protected FlightDeckSlot[] slots;

        protected Point box;
        protected Point start_rel;
        protected Point end_rel;
        protected Point cam_rel;
        protected Point[] approach_rel = new Point[NUM_APPROACH_PTS];
        protected Point[] runway_rel = new Point[2];

        protected Point start_point;
        protected Point end_point;
        protected Point cam_loc;
        protected Point[] approach_point = new Point[NUM_APPROACH_PTS];
        protected Point[] runway_point = new Point[2];

        protected double azimuth;
        protected double cycle_time = 5;

        protected int num_approach_pts;
        protected int num_catsounds;
        protected int num_hoops;
        protected Hoop[] hoops;
        protected LightNode light;
        protected List<InboundSlot> recovery_queue;

        protected ISimObserver simObserver = new SimObserver();
    }
}