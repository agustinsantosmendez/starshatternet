﻿using StarshatterNet.Stars;
using UnityEngine;

public class GameTesting : MonoBehaviour
{
    private Game game;
    private int width, height;
    private float value = 10.0f;
    private float scrollBar = 1.0f;
    void Awake()
    {
        game = Game.GetInstance();
        game.Init(null, this.gameObject);

        width = Screen.width;
        height = Screen.height;

        Game.SetTimeCompression(scrollBar);
    }

    // Start is called before the first frame update
    void Start()
    {
        game.Run();
        game.Activate(true);
    }

    // Update is called once per frame
    void Update()
    {
        game.GameLoop();
        Game.SetTimeCompression(scrollBar);

        if (Input.GetKeyDown(KeyCode.I))
            game.ScreenCapture();
    }
    void OnGUI()
    {
        GUIStyle sliderDetails = new GUIStyle(GUI.skin.GetStyle("horizontalSlider"));
        GUIStyle sliderThumbDetails = new GUIStyle(GUI.skin.GetStyle("horizontalSliderThumb"));
        GUIStyle labelDetails = new GUIStyle(GUI.skin.GetStyle("label"));

        // Set the size of the fonts and the width/height of the slider.
        labelDetails.fontSize = 6 * (width / 200);
        sliderDetails.fixedHeight = height / 32;
        sliderDetails.fontSize = 3 * (width / 200);
        sliderThumbDetails.fixedHeight = height / 32;
        sliderThumbDetails.fixedWidth = width / 32;

        // Show the slider. Make the scale to be ten times bigger than the needed size.
        value = GUI.HorizontalSlider(new UnityEngine.Rect(width / 8, height / 4, width - (4 * width / 8), height - (2 * height / 4)),
            value, 0.0f, 200.0f, sliderDetails, sliderThumbDetails);

        // Show the value from the slider. Make sure that 0.5, 0.6... 1.9, 2.0 are shown.
        float v = ((float)Mathf.RoundToInt(value)) / 10.0f;
        GUI.Label(new UnityEngine.Rect(width / 8, height / 3.25f, width - (2 * width / 8), height - (2 * height / 4)),
            "timeScale: " + v.ToString("f1"), labelDetails);
        scrollBar = v;

        // Display the recorded time in a certain size.
        labelDetails.fontSize = 6 * (width / 200);
        GUI.Label(new UnityEngine.Rect(width / 8, height / 1.7f, width - (2 * width / 8), height - (2 * height / 4)),
            "Game time is: " + Game.GameTime().ToString("f4") + " seconds.", labelDetails);

        // Display the real time in a certain size.
        GUI.Label(new UnityEngine.Rect(width / 8, height / 1.5f, width - (2 * width / 8), height - (2 * height / 4)),
            "Real time is: " + (Game.RealTime()/1000f).ToString("f4") + " seconds.", labelDetails);

        // Display the FPD in a certain size.
        GUI.Label(new UnityEngine.Rect(width / 8, height / 1.3f, width - (2 * width / 8), height - (2 * height / 4)),
            "FPS is: " +  Game.FrameRate().ToString("f2") + " frames per seconds.", labelDetails);
    }

}
