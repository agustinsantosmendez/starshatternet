﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Grid.h/Grid.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Tactical Grid
*/
using System.Collections.Generic;
using StarshatterNet.Gen.Graphics;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.Graphics.General
{
    public class GridNode : Graphic2D
    {

        public GridNode(int size = 10, int step = 1)
        {
            this.size = size;
            this.step = step;
            this.radius = (float)(size * 1.414);
            this.name = string.Format("GridGroupObjet{0:X8}", this.Identity());
            gameObj = new GameObject(this.name);

            CreateGrid();
        }


        public override bool CollidesWith(Graphic o)
        { return false; }

        public float Width
        {
            get => lineWidth;
            set {
                lineWidth = value;
                foreach (var line in lines) { 
                    line.startWidth = lineWidth; line.endWidth = lineWidth;
                }
            }
        }

        // Add a grid with thick major grid lines and thin stroked minor grid lines.
        private void CreateGrid()
        {
            this.Width = lineWidth;

            int c = 0;
            Color line;
            for (int i = 0; i <= size; i += step)
            {
                Vector3 p1 = new Vector3(i, -size, 0);
                Vector3 p2 = new Vector3(i, size, 0);
                Vector3 p3 = new Vector3(-i, -size, 0);
                Vector3 p4 = new Vector3(-i, size, 0);

                if (c != 0)
                    line = DARK_LINE;
                else
                    line = MAIN_LINE;

                DrawLine(p1, p2, line);
                DrawLine(p3, p4, line);

                c++;
                if (c > 3) c = 0;
            }

            c = 0;

            for (int i = 0; i <= size; i += step)
            {
                Vector3 p1 = new Vector3(-size, i, 0);
                Vector3 p2 = new Vector3(size, i, 0);
                Vector3 p3 = new Vector3(-size, -i, 0);
                Vector3 p4 = new Vector3(size, -i, 0);

                if (c != 0)
                    line = DARK_LINE;
                else
                    line = MAIN_LINE;

                DrawLine(p1, p2, line);
                DrawLine(p3, p4, line);

                c++;
                if (c > 3) c = 0;
            }
        }

        protected int size;
        protected int step;
        private float lineWidth = 0.05f;
#if TODO
        // Original code, black over white
        private static readonly Color DARK_LINE = new Color(8 / 255f, 8 / 255f, 8 / 255f);
        private static readonly Color MAIN_LINE = new Color(16 / 255f, 16 / 255f, 16 / 255f);
#endif
        // white over black
        private static readonly Color DARK_LINE = new Color(128 / 255f, 128 / 255f, 128 / 255f); // Gray
        private static readonly Color MAIN_LINE = new Color(255 / 255f, 255 / 255f, 255 / 255f); // White

    }
}
