﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MsnWepDlg.h/MsnWepDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Briefing Dialog Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MsnWepDlg : FormWindow
    {
        public MsnWepDlg(Screen s, Camera c, FormDef def, PlanScreen mgr, GameObject p) :
               base(s, 0, 0, s.Width(), s.Height(), c, p, "MsnWepDlg")
        {
            msnDlg = new MsnDlg(mgr);
            elem = null; first_station = 0; beauty = null; player_desc = null;


            msnDlg.campaign = Campaign.GetCampaign();

            if (msnDlg.campaign != null)
                msnDlg.mission = msnDlg.campaign.GetMission();

            designs.Initialize();
            mounts.Initialize();
            Init(def);
            Show();
        }
        //public virtual ~MsnWepDlg();
        public override void RegisterControls()
        {
            lbl_element = FindControl(601);
            lbl_type = FindControl(602);
            lbl_weight = FindControl(603);
            loadout_list = (ListBox)FindControl(604);
            beauty = (ImageBox)FindControl(300);
            player_desc = FindControl(301);

            if (loadout_list != null)
                REGISTER_CLIENT(ETD.EID_SELECT, loadout_list, OnLoadout);

            for (int i = 0; i < 8; i++)
            {
                lbl_desc[i] = FindControl(500 + i * 10);
                lbl_station[i] = FindControl(401 + i);

                for (int n = 0; n < 8; n++)
                {
                    btn_load[i, n] = (Button)FindControl(500 + i * 10 + n + 1);

                    if (btn_load[i, n] != null)
                    {
                        Texture2D pic;
                        if (i == 0)
                        {
                            btn_load[i, n].GetPicture(out pic);
                            if (n == 0)
                                led_off.Texture = pic;
                            else if (n == 1)
                                led_on.Texture = pic;
                        }
                        btn_load[i, n].GetPicture(out pic);
                        led_off.Texture = pic;
                        btn_load[i, n].SetPictureLocation(4); // centered
                        REGISTER_CLIENT(ETD.EID_CLICK, btn_load[i, n], OnMount);
                    }
                }
            }

            msnDlg.RegisterMsnControls(this);

            if (msnDlg.commit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.commit, OnCommit);

            if (msnDlg.cancel != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.cancel, OnCancel);

            if (msnDlg.sit_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.sit_button, OnTabButton);

            if (msnDlg.pkg_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.pkg_button, OnTabButton);

            if (msnDlg.nav_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.nav_button, OnTabButton);

            if (msnDlg.wep_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.wep_button, OnTabButton);
        }
        public override void Show()
        {
            if (msnDlg == null) return;
            base.Show();
            msnDlg.ShowMsnDlg();

            if (msnDlg.mission != null)
            {
                for (int i = 0; i < msnDlg.mission.GetElements().Count; i++)
                {
                    MissionElement e = msnDlg.mission.GetElements()[i];
                    if (e.Player() != 0)
                    {
                        elem = e;
                        break;
                    }
                }
            }

            if (elem != null)
            {
                SetupControls();
            }
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnCommit(null, null);
            }
        }
        // Operations:
        public virtual void OnCommit(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnCommit(obj, evnt);
        }

        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnCancel(obj, evnt);
        }
        public virtual void OnTabButton(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnTabButton(obj, evnt);
        }
        public virtual void OnMount(ActiveWindow obj, AWEvent evnt)
        {
            int station = -1;
            int item = -1;

            for (int i = 0; i < 8 && item < 0; i++)
            {
                for (int n = 0; n < 8 && station < 0; n++)
                {
                    if (btn_load[i, n] == evnt.window)
                    {
                        station = n;
                        item = i;
                    }
                }
            }

            if (item >= 0 && station >= 0)
            {
                if (loads[station] == item)
                    item = -1;

                loads[station] = item;

                for (int n = 0; n < 8; n++)
                {
                    btn_load[n, station].SetPicture(n == item ? led_on.Texture : led_off.Texture);
                }

                if (elem != null)
                {
                    int nstations = elem.GetDesign().hard_points.Count;

                    if (elem.Loadouts().Count < 1)
                    {
                        MissionLoad l = new MissionLoad();
                        elem.Loadouts().Add(l);

                        for (int n = 0; n < nstations; n++)
                            l.SetStation(n, LoadToPointIndex(n));
                    }
                    else
                    {
                        foreach (MissionLoad l in elem.Loadouts())
                        {
                            // if the player customizes the loadout,
                            // tell the sim loader not to use a named
                            // loadout from the ship design:
                            l.SetName("");

                            for (int n = 0; n < nstations; n++)
                                l.SetStation(n, LoadToPointIndex(n));
                        }
                    }
                }
            }

            if (loadout_list != null)
                loadout_list.ClearSelection();

            if (lbl_weight != null && elem != null)
            {
                ShipDesign d = (ShipDesign)elem.GetDesign();
                int nstations = d.hard_points.Count;
                double mass = d.mass;

                for (int n = 0; n < nstations; n++)
                {
                    int item2 = loads[n + first_station];
                    mass += d.hard_points[n].GetCarryMass(item2);
                }

                string weight;
                weight = string.Format("{0} kg", (int)(mass * 1000));
                lbl_weight.SetText(weight);
            }
        }
        public virtual void OnLoadout(ActiveWindow obj, AWEvent evnt)
        {
            if (elem == null) return;

            ShipDesign design = (ShipDesign)elem.GetDesign();
            ShipLoad shipload = null;

            if (loadout_list != null && design != null)
            {
                int index = loadout_list.GetListIndex();
                string loadname = loadout_list.GetItemText(index);

                foreach (ShipLoad sl in design.loadouts)
                {
                    if (sl.name == loadname)
                    {
                        shipload = sl;
                    }
                }

                if (shipload == null) return;

                if (lbl_weight != null)
                {
                    string weight;
                    weight = string.Format("{0} kg", (int)((design.mass + shipload.mass) * 1000));
                    lbl_weight.SetText(weight);
                }

                if (elem.Loadouts().Count < 1)
                {
                    MissionLoad l = new MissionLoad(-1, shipload.name);
                    elem.Loadouts().Add(l);
                }
                else
                {
                    foreach (MissionLoad l in elem.Loadouts())
                    {
                        // if the player chooses a std loadout,
                        // tell the sim loader to use a named
                        // loadout from the ship design:
                        l.SetName(shipload.name);
                    }
                }

                int nstations = design.hard_points.Count;
                int[] loadout = shipload.load;

                for (int i = 0; i < 8; i++)
                    loads[i] = -1;

                for (int i = 0; i < nstations; i++)
                    loads[i + first_station] = PointIndexToLoad(i, loadout[i]);

                for (int i = 0; i < 8; i++)
                {
                    for (int n = 0; n < 8; n++)
                    {
                        btn_load[i, n].SetPicture(i == loads[n] ? led_on.Texture : led_off.Texture);
                    }
                }
            }
        }

        protected virtual void SetupControls()
        {
            ShipDesign design = (ShipDesign)elem.GetDesign();

            if (lbl_element != null)
                lbl_element.SetText(elem.Name());

            if (lbl_type != null)
                lbl_type.SetText(design.name);

            BuildLists();

            for (int i = 0; i < 8; i++)
            {
                if (lbl_desc[i] == null) continue;

                if (designs[i] != null)
                {
                    lbl_desc[i].Show();
                    lbl_desc[i].SetText(designs[i].group + " " + designs[i].name);

                    for (int n = 0; n < 8; n++)
                    {
                        if (mounts[i, n])
                        {
                            btn_load[i, n].Show();
                            btn_load[i, n].SetPicture((loads[n] == i) ? led_on.Texture : led_off.Texture);
                        }
                        else
                        {
                            btn_load[i, n].Hide();
                        }
                    }
                }
                else
                {
                    lbl_desc[i].Hide();

                    for (int n = 0; n < 8; n++)
                    {
                        btn_load[i, n].Hide();
                    }
                }
            }

            double loaded_mass = 0;
            string weight;

            if (loadout_list != null)
            {
                loadout_list.ClearItems();

                if (design != null)
                {
                    foreach (ShipLoad load in design.loadouts)
                    {
                        int item = loadout_list.AddItem(load.name) - 1;

                        weight = string.Format("{0} kg", (int)((design.mass + load.mass) * 1000));
                        loadout_list.SetItemText(item, 1, weight);
                        loadout_list.SetItemData(item, 1, (int)(load.mass * 1000));

                        if (elem.Loadouts().Count > 0 &&
                                elem.Loadouts()[0].GetName() == load.name)
                        {
                            loadout_list.SetSelected(item, true);
                            loaded_mass = design.mass + load.mass;
                        }
                    }
                }
            }

            if (lbl_weight != null && design != null)
            {
                if (loaded_mass < 1)
                    loaded_mass = design.mass;

                weight = string.Format("{0} kg", (int)(loaded_mass * 1000));
                lbl_weight.SetText(weight);
            }

            if (beauty != null && design != null)
            {
                beauty.SetPicture(design.beauty.Texture);
            }

            if (player_desc != null && design != null)
            {
                string txt;

                if (design.type <= CLASSIFICATION.ATTACK)
                    txt = string.Format("{0} {1}", design.abrv, design.display_name);
                else
                    txt = string.Format("{0} {1}", design.abrv, elem.Name());

                player_desc.SetText(txt);
            }
        }
        protected virtual void BuildLists()
        {
            designs.Initialize();
            mounts.Initialize();
            if (elem != null)
            {
                ShipDesign d = (ShipDesign)elem.GetDesign();
                int nstations = d.hard_points.Count;

                first_station = (8 - nstations) / 2;

                int index = 0;
                int station = first_station;

                for (int s = 0; s < 8; s++)
                    if (lbl_station[s] != null)
                        lbl_station[s].SetText("");

                foreach (HardPoint hp in d.hard_points)
                {
                    if (lbl_station[station] != null)
                        lbl_station[station].SetText(hp.GetAbbreviation());

                    for (int n = 0; n < HardPoint.MAX_DESIGNS; n++)
                    {
                        WeaponDesign wep_dsn = hp.GetWeaponDesign(n);

                        if (wep_dsn != null)
                        {
                            bool found = false;

                            for (int i = 0; i < 8 && !found; i++)
                            {
                                if (designs[i] == wep_dsn)
                                {
                                    found = true;
                                    mounts[i, station] = true;
                                }
                            }

                            if (!found)
                            {
                                mounts[index, station] = true;
                                designs[index++] = wep_dsn;
                            }
                        }
                    }

                    station++;
                }

                if (elem.Loadouts().Count != 0)
                {
                    MissionLoad msn_load = elem.Loadouts()[0];

                    for (int i = 0; i < 8; i++)
                        loads[i] = -1;

                    // map loadout:
                    int[] loadout = null;
                    if (msn_load.GetName().Length != 0)
                    {
                        foreach (ShipLoad sl in ((ShipDesign)elem.GetDesign()).loadouts)
                        {
                            if (sl.name.Equals(msn_load.GetName(), StringComparison.InvariantCultureIgnoreCase))
                                loadout = sl.load;
                        }
                    }
                    else
                    {
                        loadout = msn_load.GetStations();
                    }

                    for (int i = 0; i < nstations; i++)
                    {
                        loads[i + first_station] = loadout[i];
                    }
                }
            }
        }
        protected virtual int LoadToPointIndex(int n)
        {
            int nn = n + first_station;

            if (elem == null || nn < 0 || nn >= 8 || loads[nn] == -1)
                return -1;

            int index = -1;
            WeaponDesign wep_design = designs[loads[nn]];
            ShipDesign design = (ShipDesign)elem.GetDesign();
            HardPoint hard_point = design.hard_points[n];

            for (int i = 0; i < 8 && index < 0; i++)
            {
                if (hard_point.GetWeaponDesign(i) == wep_design)
                {
                    index = i;
                }
            }

            return index;
        }
        protected virtual int PointIndexToLoad(int n, int index)
        {
            int nn = n + first_station;

            if (elem == null || nn < 0 || nn >= 8)
                return -1;

            int result = -1;
            ShipDesign design = (ShipDesign)elem.GetDesign();
            HardPoint hard_point = design.hard_points[n];
            WeaponDesign wep_design = hard_point.GetWeaponDesign(index);

            for (int i = 0; i < 8 && result < 0; i++)
            {
                if (designs[i] == wep_design)
                {
                    result = i;
                }
            }

            return result;
        }

        protected ActiveWindow lbl_element;
        protected ActiveWindow lbl_type;
        protected ActiveWindow lbl_weight;
        protected ActiveWindow player_desc;
        protected ImageBox beauty;

        protected ActiveWindow[] lbl_station = new ActiveWindow[8];
        protected ActiveWindow[] lbl_desc = new ActiveWindow[8];
        protected Button[,] btn_load = new Button[8, 8];

        protected ListBox loadout_list;

        protected MissionElement elem;
        protected WeaponDesign[] designs = new WeaponDesign[8];
        protected bool[,] mounts = new bool[8, 8];
        protected int[] loads = new int[8];
        protected int first_station;

        protected Bitmap led_off = new Bitmap();
        protected Bitmap led_on = new Bitmap();

        protected MsnDlg msnDlg;
    }
}