﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CombatAction.h/CombatAction.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    A planned action (mission/story/strategy) in a dynamic campaign.
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using static StarshatterNet.Stars.MissionCampaign.CombatAction;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning CombatAction class is still in development and is not recommended for production.
    public class CombatAction
    {
        public enum TYPE
        {
            NO_ACTION,
            STRATEGIC_DIRECTIVE,
            ZONE_ASSIGNMENT,
            SYSTEM_ASSIGNMENT,
            MISSION_TEMPLATE,
            COMBAT_EVENT,
            INTEL_EVENT,
            CAMPAIGN_SITUATION,
            CAMPAIGN_ORDERS
        };

        public enum STATUS
        {
            PENDING,
            ACTIVE,
            SKIPPED,
            FAILED,
            COMPLETE
        };

        public CombatAction(int n, TYPE typ, int sub, int iff)
        {
            id = n; type = typ; subtype = sub; opp_type = -1; team = iff;
            status = STATUS.PENDING; count = 0; rval = -1; source = 0; time = 0;
            start_before = (int)1e9; start_after = 0;
            min_rank = 0; max_rank = 100;
            delay = 0; probability = 100; asset_type = 0; target_type = 0;
        }
        // ~CombatAction();

        //    int operator ==(const CombatAction& a)  const { return id == a.id; }

        public bool IsAvailable()
        {
            if (rval < 0)
            {
                this.rval = (int)RandomHelper.Random(0, 100);

                if (rval > probability)
                    this.status = STATUS.SKIPPED;
            }

            if (status != STATUS.PENDING)
                return false;

            if (min_rank > 0 || max_rank < 100)
            {
                Player player = Player.GetCurrentPlayer();

                if (player.Rank() < min_rank || player.Rank() > max_rank)
                    return false;
            }

            Campaign campaign = Campaign.GetCampaign();
            if (campaign != null)
            {
                if (campaign.GetTime() < start_after)
                {
                    return false;
                }

                if (campaign.GetTime() > start_before)
                {
                    this.status = STATUS.FAILED; // too late!
                    return false;
                }

                // check requirements against actions in current campaign:
                foreach (CombatActionReq r in this.requirements)
                {
                    bool ok = false;

                    if (r.action > 0)
                    {
                        foreach (CombatAction a in campaign.GetActions())
                        {
                            if (a.Identity() == r.action)
                            {
                                if (r.not)
                                {
                                    if (a.Status() == r.stat)
                                        return false;
                                }
                                else
                                {
                                    if (a.Status() != r.stat)
                                        return false;
                                }
                            }
                        }
                    }

                    // group-based requirement
                    else if (r.group_type > 0)
                    {
                        if (r.c1 != null)
                        {
                            CombatGroup group = r.c1.FindGroup(r.group_type, r.group_id);

                            if (group != null)
                            {
                                int test = 0;
                                int comp = 0;

                                if (r.intel != 0)
                                {
                                    test = (int)group.IntelLevel();
                                    comp = r.intel;
                                }

                                else
                                {
                                    test = group.CalcValue();
                                    comp = r.score;
                                }

                                switch (r.comp)
                                {
                                    case CombatActionReq.COMPARISON_OPERATOR.LT: ok = (test < comp); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.LE: ok = (test <= comp); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.GT: ok = (test > comp); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.GE: ok = (test >= comp); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.EQ: ok = (test == comp); break;
                                }
                            }

                            if (!ok)
                                return false;
                        }
                    }

                    // score-based requirement
                    else
                    {
                        int test = 0;

                        if (r.comp <= CombatActionReq.COMPARISON_OPERATOR.EQ)
                        {  // absolute
                            if (r.c1 != null)
                            {
                                test = r.c1.Score();

                                switch (r.comp)
                                {
                                    case CombatActionReq.COMPARISON_OPERATOR.LT: ok = (test < r.score); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.LE: ok = (test <= r.score); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.GT: ok = (test > r.score); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.GE: ok = (test >= r.score); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.EQ: ok = (test == r.score); break;
                                }
                            }
                        }

                        else
                        {                                 // relative
                            if (r.c1 != null && r.c2 != null)
                            {
                                test = r.c1.Score() - r.c2.Score();

                                switch (r.comp)
                                {
                                    case CombatActionReq.COMPARISON_OPERATOR.RLT: ok = (test < r.score); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.RLE: ok = (test <= r.score); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.RGT: ok = (test > r.score); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.RGE: ok = (test >= r.score); break;
                                    case CombatActionReq.COMPARISON_OPERATOR.REQ: ok = (test == r.score); break;
                                }
                            }
                        }

                        if (!ok)
                            return false;
                    }

                    if (delay > 0)
                    {
                        this.start_after = (int)campaign.GetTime() + delay;
                        this.delay = 0;
                        return IsAvailable();
                    }
                }
            }

            return true;
        }
        public void FireAction()
        {
            Campaign campaign = Campaign.GetCampaign();
            if (campaign != null)
                time = (int)campaign.GetTime();

            if (count >= 1)
                count--;

            if (count < 1)
                status = STATUS.COMPLETE;
        }
        public void FailAction()
        {
            Campaign campaign = Campaign.GetCampaign();
            if (campaign != null)
                time = (int)campaign.GetTime();

            count = 0;
            status = STATUS.FAILED;
        }
        public void AddRequirement(int action, STATUS stat, bool not = false)
        {
            requirements.Add(new CombatActionReq(action, stat, not));
        }

        public void AddRequirement(Combatant c1, Combatant c2, CombatActionReq.COMPARISON_OPERATOR comp, int score)
        {
            requirements.Add(new CombatActionReq(c1, c2, comp, score));
        }

        public void AddRequirement(Combatant c1, CombatGroup.GROUP_TYPE group_type, int group_id, CombatActionReq.COMPARISON_OPERATOR comp, int score, int intel = 0)
        {
            requirements.Add(new CombatActionReq(c1, group_type, group_id, comp, score, intel));
        }
        public static TYPE TypeFromName(string n)
        {
            TYPE type = 0;

            if (n.Equals("NO_ACTION", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.NO_ACTION;

            else if (n.Equals("MARKER", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.NO_ACTION;

            else if (n.Equals("STRATEGIC_DIRECTIVE", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.STRATEGIC_DIRECTIVE;

            else if (n.Equals("STRATEGIC", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.STRATEGIC_DIRECTIVE;

            else if (n.Equals("ZONE_ASSIGNMENT", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.ZONE_ASSIGNMENT;

            else if (n.Equals("ZONE", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.ZONE_ASSIGNMENT;

            else if (n.Equals("SYSTEM_ASSIGNMENT", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.SYSTEM_ASSIGNMENT;

            else if (n.Equals("SYSTEM", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.SYSTEM_ASSIGNMENT;

            else if (n.Equals("MISSION_TEMPLATE", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.MISSION_TEMPLATE;

            else if (n.Equals("MISSION", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.MISSION_TEMPLATE;

            else if (n.Equals("COMBAT_EVENT", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.COMBAT_EVENT;

            else if (n.Equals("EVENT", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.COMBAT_EVENT;

            else if (n.Equals("INTEL_EVENT", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.INTEL_EVENT;

            else if (n.Equals("INTEL", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.INTEL_EVENT;

            else if (n.Equals("CAMPAIGN_SITUATION", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.CAMPAIGN_SITUATION;

            else if (n.Equals("SITREP", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.CAMPAIGN_SITUATION;

            else if (n.Equals("CAMPAIGN_ORDERS", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.CAMPAIGN_ORDERS;

            else if (n.Equals("ORDERS", StringComparison.InvariantCultureIgnoreCase))
                type = TYPE.CAMPAIGN_ORDERS;

            return type;
        }
        public static STATUS StatusFromName(string n)
        {
            STATUS stat = 0;

            if (n.Equals("PENDING", StringComparison.InvariantCultureIgnoreCase))
                stat = STATUS.PENDING;

            else if (n.Equals("ACTIVE", StringComparison.InvariantCultureIgnoreCase))
                stat = STATUS.ACTIVE;

            else if (n.Equals("SKIPPED", StringComparison.InvariantCultureIgnoreCase))
                stat = STATUS.SKIPPED;

            else if (n.Equals("FAILED", StringComparison.InvariantCultureIgnoreCase))
                stat = STATUS.FAILED;

            else if (n.Equals("COMPLETE", StringComparison.InvariantCultureIgnoreCase))
                stat = STATUS.COMPLETE;

            return stat;
        }

        // accessors/mutators:
        public int Identity() { return id; }
        public TYPE Type() { return type; }
        public int Subtype() { return subtype; }
        public int OpposingType() { return opp_type; }
        public int GetIFF() { return team; }
        public CombatAction.STATUS Status() { return status; }
        public CombatEvent.EVENT_SOURCE Source() { return source; }
        public Point Location() { return loc; }
        public string System() { return system; }
        public string Region() { return region; }
        public string Filename() { return text_file; }
        public string ImageFile() { return image_file; }
        public string SceneFile() { return scene_file; }
        public int Count() { return count; }
        public int ExecTime() { return time; }
        public int StartBefore() { return start_before; }
        public int StartAfter() { return start_after; }
        public int MinRank() { return min_rank; }
        public int MaxRank() { return max_rank; }
        public int Delay() { return delay; }
        public int Probability() { return probability; }
        public int AssetType() { return asset_type; }
        public int AssetId() { return asset_id; }
        public List<string> AssetKills() { return asset_kills; }
        public int TargetType() { return target_type; }
        public int TargetId() { return target_id; }
        public int TargetIFF() { return target_iff; }
        public List<string> TargetKills() { return target_kills; }
        public string GetText() { return text; }

        public void SetType(TYPE t) { type = t; }
        public void SetSubtype(int s) { subtype = s; }
        public void SetOpposingType(int t) { opp_type = t; }
        public void SetIFF(int t) { team = t; }
        public void SetStatus(CombatAction.STATUS s) { status = s; }
        public void SetSource(CombatEvent.EVENT_SOURCE s) { source = s; }
        public void SetLocation(Point p) { loc = p; }
        public void SetSystem(string sys) { system = sys; }
        public void SetRegion(string rgn) { region = rgn; }
        public void SetFilename(string f) { text_file = f; }
        public void SetImageFile(string f) { image_file = f; }
        public void SetSceneFile(string f) { scene_file = f; }
        public void SetCount(int n) { count = (char)n; }
        public void SetExecTime(int t) { time = t; }
        public void SetStartBefore(int s) { start_before = s; }
        public void SetStartAfter(int s) { start_after = s; }
        public void SetMinRank(int n) { min_rank = (char)n; }
        public void SetMaxRank(int n) { max_rank = (char)n; }
        public void SetDelay(int d) { delay = d; }
        public void SetProbability(int n) { probability = n; }
        public void SetAssetType(int t) { asset_type = t; }
        public void SetAssetId(int n) { asset_id = n; }
        public void SetTargetType(int t) { target_type = t; }
        public void SetTargetId(int n) { target_id = n; }
        public void SetTargetIFF(int n) { target_iff = n; }
        public void SetText(string t) { text = t; }


        private int id;
        private TYPE type;
        private int subtype;
        private int opp_type;
        private int team;
        private CombatAction.STATUS status;
        private int min_rank;
        private int max_rank;
        private CombatEvent.EVENT_SOURCE source;
        private Point loc;
        private string system;
        private string region;
        private string text_file;
        private string image_file;
        private string scene_file;
        private int count;
        private int start_before;
        private int start_after;
        private int delay;
        private int probability;
        private int rval;
        private int time;

        private string text;
        private int asset_type;
        private int asset_id;
        private List<string> asset_kills = new List<string>();
        private int target_type;
        private int target_id;
        private int target_iff;
        private List<string> target_kills = new List<string>();

        private List<CombatActionReq> requirements = new List<CombatActionReq>();
    }

    public class CombatActionReq
    {
        public enum COMPARISON_OPERATOR
        {
            LT, LE, GT, GE, EQ,    // absolute score comparison
            RLT, RLE, RGT, RGE, REQ    // delta score comparison
        };

        public CombatActionReq(int a, STATUS s, bool n = false)
        {
            action = a; stat = s; not = n; c1 = null; c2 = null; comp = 0; score = 0; intel = 0;
        }

        public CombatActionReq(Combatant a1, Combatant a2, COMPARISON_OPERATOR comparison, int value)
        {
            action = 0; stat = 0; not = false; c1 = a1; c2 = a2; group_type = 0; group_id = 0;
            comp = comparison; score = value; intel = 0;
        }

        public CombatActionReq(Combatant a1, CombatGroup.GROUP_TYPE gtype, int gid, COMPARISON_OPERATOR comparison, int value, int intel_level = 0)
        {
            action = 0; stat = 0; not = false; c1 = a1; c2 = null; group_type = gtype; group_id = gid;
            comp = comparison; score = value; intel = intel_level;
        }

        public static COMPARISON_OPERATOR CompFromName(string sym)
        {
            switch (sym)
            {
                case "LT":
                    return COMPARISON_OPERATOR.LT;

                case "LE":
                    return COMPARISON_OPERATOR.LE;

                case "GT":
                    return COMPARISON_OPERATOR.GT;

                case "GE":
                    return COMPARISON_OPERATOR.GE;

                case "EQ":
                    return COMPARISON_OPERATOR.EQ;

                case "RLT":
                    return COMPARISON_OPERATOR.RLT;

                case "RLE":
                    return COMPARISON_OPERATOR.RLE;

                case "RGT":
                    return COMPARISON_OPERATOR.RGT;

                case "RGE":
                    return COMPARISON_OPERATOR.RGE;

                case "REQ":
                    return COMPARISON_OPERATOR.REQ;
            }
            throw new System.ArgumentException();
        }


        public int action;
        public STATUS stat;
        public bool not;

        public Combatant c1;
        public Combatant c2;
        public CombatActionReq.COMPARISON_OPERATOR comp;
        public int score;
        public int intel;
        public CombatGroup.GROUP_TYPE group_type;
        public int group_id;
    }
}