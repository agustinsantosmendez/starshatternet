﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MenuView.h/MenuView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    View class for displaying right-click context menus
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Views;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.Views
{
    public class MenuView : View
    {
        public MenuView(Window c, string name) : base(c, name)
        {
            mouse_down = false; right_down = false; shift_down = false; show_menu = false;
            action = 0; menu = null; menu_item = null; selected = null;
            text_color = Color.white; back_color = Color.black;

            right_start.X = 0;
            right_start.Y = 0;

            OnWindowMove();
        }
        //public virtual ~MenuView();

        // Operations:
        public override void Refresh()
        {
            if (show_menu)
                DrawMenu();
        }

        public override void OnWindowMove()
        {
            offset.X = window.X();
            offset.Y = window.Y();
            width = window.Width();
            height = window.Height();
        }

        static ulong rbutton_latch = 0;
        public virtual void DoMouseFrame()
        {

            action = 0;

            MouseController mouse_con = MouseController.GetInstance();
            if (Mouse.RButton() != 0)
            {
                if (!right_down && (mouse_con == null || !mouse_con.Active()))
                {
                    rbutton_latch = Game.RealTime();
                    right_down = true;
                    show_menu = false;
                }
            }
            else
            {
                if (right_down && (Game.RealTime() - rbutton_latch < 250))
                {
                    right_start.X = Mouse.X() - offset.X;
                    right_start.Y = Mouse.Y() - offset.Y;
                    show_menu = true;
                    Button.PlaySound(Button.SOUNDS.SND_MENU_OPEN);
                }

                right_down = false;
            }


            if (mouse_con == null || !mouse_con.Active())
            {
                if (Mouse.LButton() != 0)
                {
                    if (mouse_con == null)
                        shift_down = Keyboard.KeyDown(KeyMap.VK_SHIFT);

                    mouse_down = true;
                }

                else if (mouse_down)
                {
                    int mouse_x = (int)(Mouse.X() - offset.X);
                    int mouse_y = (int)(Mouse.Y() - offset.Y);
                    bool keep_menu = false;

                    if (show_menu)
                    {
                        keep_menu = ProcessMenuItem();
                        Mouse.Show(true);
                    }

                    mouse_down = false;

                    if (!keep_menu)
                    {
                        ClearMenuSelection(menu);
                        show_menu = false;
                    }
                }
            }
        }
        public virtual void DrawMenu()
        {
            menu_item = null;

            if (menu != null)
            {
                int mx = (int)(right_start.X - 2);
                int my = (int)(right_start.Y - 2);

                DrawMenu(mx, my, menu);
            }
        }

        public virtual void DrawMenu(int x, int y, Menu menu) { throw new NotImplementedException(); }
        public virtual bool ProcessMenuItem()
        {
            if (menu_item == null || !menu_item.GetEnabled())
                return false;

            if (menu_item.GetSubmenu() != null)
            {
                foreach (MenuItem item in menu_item.GetMenu().GetItems())
                    if (item == menu_item)
                        item.SetSelected(2);
                    else
                        item.SetSelected(0);

                Button.PlaySound(Button.SOUNDS.SND_MENU_OPEN);
                return true;   // keep menu showing
            }

            action = (int)menu_item.GetData();
            Button.PlaySound(Button.SOUNDS.SND_MENU_SELECT);
            return false;
        }
        public virtual void ClearMenuSelection(Menu menu)
        {
            if (menu != null)
            {
                foreach (MenuItem item in menu.GetItems())
                {
                    item.SetSelected(0);
                    if (item.GetSubmenu() != null)
                        ClearMenuSelection(item.GetSubmenu());
                }
            }
        }

        public virtual bool IsShown() { return show_menu; }
        public virtual int GetAction() { return action; }
        public virtual Menu GetMenu() { return menu; }
        public virtual void SetMenu(Menu m) { menu = m; }
        public virtual MenuItem GetMenuItem() { return menu_item; }

        public virtual Color GetBackColor() { return back_color; }
        public virtual void SetBackColor(Color c) { back_color = c; }
        public virtual Color GetTextColor() { return text_color; }
        public virtual void SetTextColor(Color c) { text_color = c; }


        protected int width, height;

        protected bool shift_down;
        protected bool mouse_down;
        protected bool right_down;
        protected bool show_menu;
        protected Point right_start;
        protected Point offset;

        protected int action;
        protected Menu menu;
        protected MenuItem menu_item;
        protected MenuItem selected;

        protected Color back_color;
        protected Color text_color;
    }
}