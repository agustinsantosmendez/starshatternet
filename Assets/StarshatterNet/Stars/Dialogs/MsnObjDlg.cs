﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MsnObjDlg.h/MsnObjDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Briefing Dialog Active Window class
*/
using System;
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MsnObjDlg : FormWindow
    {
        public MsnObjDlg(Screen s, Camera c, FormDef def, PlanScreen mgr, GameObject p) :
             base(s, 0, 0, s.Width(), s.Height(), c, p, "MsnObjDlg")
        {
            msnDlg = new MsnDlg(mgr);

            objectives = null; sitrep = null; beauty = null; camview = null; player_desc = null;
            ship = null;

            msnDlg.campaign = Campaign.GetCampaign();

            if (msnDlg.campaign != null)
                msnDlg.mission = msnDlg.campaign.GetMission();

            Init(def);
            Show();
        }
        //public virtual ~MsnObjDlg();
        public override void RegisterControls()
        {
            objectives = FindControl(400);
            sitrep = FindControl(401);
            //TODO beauty = FindControl(300);
            player_desc = FindControl(301);
            cmb_skin = (ComboBox)FindControl(302);

            msnDlg.RegisterMsnControls(this);

            if (msnDlg.commit !=null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.commit,  OnCommit);

            if (msnDlg.cancel != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.cancel,  OnCancel);

            if (msnDlg.sit_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.sit_button,  OnTabButton);

            if (msnDlg.pkg_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.pkg_button,  OnTabButton);

            if (msnDlg.nav_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.nav_button,  OnTabButton);

            if (msnDlg.wep_button != null)
                REGISTER_CLIENT(ETD.EID_CLICK, msnDlg.wep_button,  OnTabButton);

            if (cmb_skin != null)
            {
                REGISTER_CLIENT(ETD.EID_SELECT, cmb_skin,  OnSkin);
            }

            if (beauty != null)
            {
                scene.SetAmbient(new Color32(72, 75, 78,255));

                Point light_pos = new Point(3e6, 5e6, 4e6);

                LightNode main_light = new LightNode(1.2f);
                main_light.MoveTo(light_pos);
                main_light.SetType(LightNode.TYPES.LIGHT_DIRECTIONAL);
                main_light.SetColor(Color.white);
                main_light.SetShadow(true);

                scene.AddLight(main_light);

                LightNode back_light = new LightNode(0.35f);
                back_light.MoveTo(light_pos * -1);
                back_light.SetType(LightNode.TYPES.LIGHT_DIRECTIONAL);
                back_light.SetColor(Color.white);
                back_light.SetShadow(false);

                scene.AddLight(back_light);

                camview = new  CameraView(beauty, scene, "Views");
                camview.SetProjectionType(CameraView.PROJECTION_TYPE.PROJECTION_PERSPECTIVE);
                camview.SetFieldOfView(2);

                beauty.AddView(camview);
            }
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnCommit(null, null);
            }
        }
        public override void Show()
        {
            if (msnDlg == null) return;

            bool update_scene = !shown;

            base.Show();
            msnDlg.ShowMsnDlg();

            if (objectives != null)
            {
                if (msnDlg.mission != null)
                {
                    if (msnDlg.mission.IsOK())
                        objectives.SetText(msnDlg.mission.Objective());
                    else
                        objectives.SetText("");
                }
                else
                {
                    objectives.SetText(Game.GetText("MsnDlg.no-mission"));
                }
            }

            if (sitrep != null)
            {
                if (msnDlg.mission != null)
                {
                    if (msnDlg.mission.IsOK())
                        sitrep.SetText(msnDlg.mission.Situation());
                    else
                        sitrep.SetText(Game.GetText("MsnDlg.found-errors") +
                        msnDlg.mission.ErrorMessage());
                }
                else
                {
                    sitrep.SetText(Game.GetText("MsnDlg.no-mission"));
                }
            }

            if (cmb_skin != null)
            {
                cmb_skin.ClearItems();
                cmb_skin.Hide();
            }

            if (beauty != null)
            {
                if (msnDlg.mission != null && msnDlg.mission.IsOK())
                {
                    MissionElement elem = msnDlg.mission.GetPlayer();

                    if (elem != null)
                    {
                        ShipDesign design = elem.GetDesign();

                        if (design != null && camview != null && update_scene)
                        {
                            double az = -Math.PI / 6;
                            double el = Math.PI / 8;
                            double zoom = 1.8;

                            scene.Graphics().Clear();

                            if (elem.IsStarship())
                            {
                                az = -Math.PI / 8;
                                el = Math.PI / 12;
                                zoom = 1.7;
                            }

                            if (design.beauty_cam.Z > 0)
                            {
                                az = design.beauty_cam.X;
                                el = design.beauty_cam.Y;
                                zoom = design.beauty_cam.Z;
                            }

                            double r = design.radius;
                            double x = zoom * r * Math.Sin(az) * Math.Cos(el);
                            double y = zoom * r * Math.Cos(az) * Math.Cos(el);
                            double z = zoom * r * Math.Sin(el);

                            cam.LookAt(new Point(0, 0, r / 5), new Point(x, z, y), new Point(0, 1, 0));

                            int n = design.lod_levels;

                            if (n >= 1)
                            {
                                Model model = design.models[n - 1][0];

                                if (model != null)
                                {
                                    ship = new ShipSolid(null);
                                    ship.UseModel(model);
                                    ship.CreateShadows(1);
                                    ship.SetSkin(elem.GetSkin());

                                    Matrix33D o = new Matrix33D();
                                    o.Pitch(3 * ConstantsF.DEGREES);
                                    o.Roll(13 * ConstantsF.DEGREES);

                                    ship.SetOrientation(o);

                                    scene.Graphics().Add(ship);
                                }
                            }
                        }

                        if (cmb_skin != null && design != null && design.skins.Count != 0)
                        {
                            cmb_skin.Show();
                            cmb_skin.AddItem(Game.GetText("MsnDlg.default"));
                            cmb_skin.SetSelection(0);

                            foreach (Skin s in ((ShipDesign)design).skins)
                            {
                                cmb_skin.AddItem(s.Name());

                                if (elem != null && elem.GetSkin() != null && s.Name() != elem.GetSkin().Name())
                                {
                                    cmb_skin.SetSelection(cmb_skin.NumItems() - 1);
                                }
                            }
                        }
                    }
                }
            }

            if (player_desc != null)
            {
                player_desc.SetText("");

                if (msnDlg.mission != null && msnDlg.mission.IsOK())
                {
                    MissionElement elem = msnDlg.mission.GetPlayer();

                    if (elem != null)
                    {
                        ShipDesign design = elem.GetDesign();

                        if (design != null)
                        {
                            string txt;

                            if (design.type <= CLASSIFICATION.ATTACK)
                                txt = string.Format("{0} {1}", design.abrv, design.display_name);
                            else
                                txt = string.Format("{0} {1}", design.abrv, elem.Name());

                            player_desc.SetText(txt);
                        }
                    }
                }
            }
        }
        // Operations:
        public virtual void OnCommit(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnCommit(obj, evnt);
        }

        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnCancel(obj, evnt);
        }
        public virtual void OnTabButton(ActiveWindow obj, AWEvent evnt)
        {
            msnDlg.OnTabButton(obj, evnt);
        }
        public virtual void OnSkin(ActiveWindow obj, AWEvent evnt)
        {
            string skin_name = cmb_skin.GetSelectedItem();

            if (msnDlg.mission != null && msnDlg.mission.IsOK())
            {
                MissionElement elem = msnDlg.mission.GetPlayer();

                if (elem != null)
                {
                    ShipDesign design = elem.GetDesign();

                    if (design != null)
                    {
                        Skin skin = design.FindSkin(skin_name);

                        elem.SetSkin(skin);

                        if (ship != null)
                            ship.SetSkin(skin);
                    }
                }
            }
        }


        protected MsnDlg msnDlg;
        protected ActiveWindow objectives;
        protected ActiveWindow sitrep;
        protected ActiveWindow player_desc;
        protected ActiveWindow beauty;
        protected ComboBox cmb_skin;
        protected CameraView camview;
        protected Scene scene;
        protected Camera cam;
        protected ShipSolid ship;
    }
}