﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetClientDlg.h/NetClientDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class NetClientDlg : FormWindow
    {
        public NetClientDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
                base(s, 0, 0, s.Width(), s.Height(), c, p, "NetClientDlg")
        {
            throw new NotImplementedException();
        }
        //public virtual ~NetClientDlg();

        public override void RegisterControls() { throw new NotImplementedException(); }
        public override void Show() { throw new NotImplementedException(); }
        public virtual void ExecFrame() { throw new NotImplementedException(); }

        // Operations:
        public virtual void OnSelect(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnAdd(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnDel(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnServer(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnHost(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnJoin(AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnCancel(AWEvent evnt) { throw new NotImplementedException(); }

        public virtual void ShowServers() { throw new NotImplementedException(); }
        public virtual void UpdateServers() { throw new NotImplementedException(); }
        public virtual void PingServer(int n) { throw new NotImplementedException(); }
        public virtual bool PingComplete() { throw new NotImplementedException(); }
        public virtual void StopNetProc() { throw new NotImplementedException(); }


        protected MenuScreen manager;
        protected NetClientConfig config;

        protected Button btn_add;
        protected Button btn_del;
        protected ListBox lst_servers;
        protected ActiveWindow lbl_info;
        protected int server_index;
        protected int ping_index;
        //protected HANDLE hnet;
        protected NetServerInfo ping_info;

        protected Button btn_server;
        protected Button btn_host;
        protected Button btn_join;
        protected Button btn_cancel;
    }
}