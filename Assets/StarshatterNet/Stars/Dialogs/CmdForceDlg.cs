﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmdForceDlg.h/CmdForceDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Operational Command Dialog (Order of Battle Tab)
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmdForceDlg : CmdDlg
    {
        public CmdForceDlg(Screen s, Camera c, FormDef def, CmpnScreen mgr, GameObject p) :
            base(s, mgr, 0, 0, s.Width(), s.Height(), c, p, "CmdForceDlg")
        {
            cmb_forces = null; lst_combat = null; lst_desc = null;
            current_group = null; current_unit = null;
            btn_transfer = null;


            Init(def);
        }
        //public virtual ~CmdForceDlg();
        public override void RegisterControls()
        {
            cmb_forces = (ComboBox)FindControl(400);
            lst_combat = (ListBox)FindControl(401);
            lst_desc = (ListBox)FindControl(402);
            btn_transfer = (Button)FindControl(403);

            RegisterCmdControls(this);

            if (btn_save != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_save, OnSave);

            if (btn_exit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_exit, OnExit);

            for (int i = 0; i < 5; i++)
            {
                if (btn_mode[i] != null)
                    REGISTER_CLIENT(ETD.EID_CLICK, btn_mode[i], OnMode);
            }

            if (cmb_forces != null)
                REGISTER_CLIENT(ETD.EID_SELECT, cmb_forces, OnForces);

            if (lst_combat != null)
            {
                lst_combat.AddColumn("datatype", 0);
                REGISTER_CLIENT(ETD.EID_SELECT, lst_combat, OnCombat);
            }

            if (btn_transfer != null)
            {
                btn_transfer.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_transfer, OnTransfer);
            }
        }

        public override void Show()
        {
            mode = MODE.MODE_FORCES;

            base.Show();
            ShowCmdDlg();

            if (cmb_forces == null) return;
            cmb_forces.ClearItems();

            campaign = Campaign.GetCampaign();

            if (campaign != null)
            {
                List<Combatant> combatants = campaign.GetCombatants();

                if (combatants.Count > 0)
                {
                    for (int i = 0; i < combatants.Count; i++)
                    {
                        if (IsVisible(combatants[i]))
                        {
                            cmb_forces.AddItem(combatants[i].Name());
                        }
                    }

                    cmb_forces.SetSelection(0);
                    Combatant c = combatants[0];
                    ShowCombatant(c);
                }
            }
        }
        public override void ExecFrame()
        {
            base.ExecFrame();
        }

        // Operations:
        public override void OnMode(ActiveWindow obj, AWEvent evnt)
        {
            base.OnMode(obj, evnt);
        }
        public override void OnSave(ActiveWindow obj, AWEvent evnt)
        {
            base.OnSave(obj, evnt);
        }

        public override void OnExit(ActiveWindow obj, AWEvent evnt)
        {
            base.OnExit(obj, evnt);
        }
        public virtual void OnForces(ActiveWindow obj, AWEvent evnt)
        {
            string name = cmb_forces.GetSelectedItem();

            campaign = Campaign.GetCampaign();

            if (campaign != null)
            {
                foreach (Combatant c in campaign.GetCombatants())
                {
                    if (name == c.Name())
                    {
                        ShowCombatant(c);
                        break;
                    }
                }
            }
        }

        static int old_index = -1;
        public virtual void OnCombat(ActiveWindow obj, AWEvent evnt)
        {
#if TODO
            int top_index = 0;
            bool expand = false;
            DWORD data = 0;
            DWORD type = 0;

            current_group = null;
            current_unit = null;

            if (lst_combat != null)
            {
                top_index = lst_combat.GetTopIndex();

                int index = lst_combat.GetListIndex();
                data = lst_combat.GetItemData(index);
                type = lst_combat.GetItemData(index, 1);
                string item = lst_combat.GetItemText(index);
                int nplus = item.IndexOf(Font.PIPE_PLUS);
                int xplus = -1;
                int dx = 10000;

                if (nplus < 0)
                    nplus = item.IndexOf(Font.PIPE_MINUS);

                if (nplus >= 0 && nplus < 64)
                {
                    string pipe;
                    strncpy(pipe, item.data(), nplus + 1);
                    pipe[nplus + 1] = 0;

                    Rect rect = new Rect(0, 0, 1000, 20);
                    lst_combat.DrawText(pipe, 0, rect, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_CALCRECT);

                    xplus = rect.w;
                }

                if (xplus > 0)
                {
                    dx = Mouse.X() - (lst_combat.GetRect().x + xplus - 16);
                }

                // look for click on plus/minus in pipe
                if (dx >= 0 && dx < 16)
                {
                    if (data && type == 0)
                    {
                        CombatGroup grp = (CombatGroup)data;
                        grp.SetExpanded(!grp.IsExpanded());
                        expand = true;

                        current_group = grp;
                        current_unit = null;
                    }
                }

                old_index = index;
            }

            if (campaign && data)
            {
                if (!expand)
                {
                    lst_desc.ClearItems();

                    // combat group
                    if (type == 0)
                    {
                        CombatGroup grp = (CombatGroup)data;

                        current_group = grp;
                        current_unit = null;

                        // if has units, show location and assignment
                        if (grp.GetUnits().Count > 0)
                        {
                            string txt;
                            int n;

                            n = lst_desc.AddItem("Group:") - 1;
                            lst_desc.SetItemText(n, 1, grp.GetDescription());
                            n = lst_desc.AddItem("Sector:") - 1;
                            lst_desc.SetItemText(n, 1, grp.GetRegion());

                            lst_desc.AddItem(" ");
                            n = lst_desc.AddItem("Sorties:") - 1;

                            if (grp.Sorties() >= 0)
                                sprintf_s(txt, "%d", grp.Sorties());
                            else
                                strcpy_s(txt, "Unavail");

                            lst_desc.SetItemText(n, 1, txt);

                            n = lst_desc.AddItem("Kills:") - 1;

                            if (grp.Kills() >= 0)
                                sprintf_s(txt, "%d", grp.Kills());
                            else
                                strcpy_s(txt, "Unavail");

                            lst_desc.SetItemText(n, 1, txt);

                            n = lst_desc.AddItem("Eff Rating:") - 1;

                            if (grp.Points() >= 0)
                            {
                                if (grp.Sorties() > 0)
                                    sprintf_s(txt, "%.1f", (double)grp.Points() / grp.Sorties());
                                else
                                    sprintf_s(txt, "%.1f", (double)grp.Points());
                            }
                            else
                            {
                                strcpy_s(txt, "Unavail");
                            }

                            lst_desc.SetItemText(n, 1, txt);
                        }

                        // else (if high-level) show components
                        else
                        {
                            int n;

                            n = lst_desc.AddItem("Group:") - 1;
                            lst_desc.SetItemText(n, 1, grp.GetDescription());

                            ListIter<CombatGroup> c = grp.GetLiveComponents();
                            while (++c)
                            {
                                n = lst_desc.AddItem("-") - 1;
                                lst_desc.SetItemText(n, 1, c.GetDescription());
                            }
                        }
                    }
                    // combat unit
                    else
                    {
                        CombatUnit unit = (CombatUnit)data;
                        current_group = unit.GetCombatGroup();
                        current_unit = unit;

                        int n;
                        string txt;

                        n = lst_desc.AddItem("Unit:") - 1;
                        lst_desc.SetItemText(n, 1, unit.GetDescription());
                        n = lst_desc.AddItem("Sector:") - 1;
                        lst_desc.SetItemText(n, 1, unit.GetRegion());

                        ShipDesign design = unit.GetDesign();
                        if (design)
                        {
                            lst_desc.AddItem(" ");
                            n = lst_desc.AddItem("Type:") - 1;
                            lst_desc.SetItemText(n, 1, Ship.ClassName(design.type));
                            n = lst_desc.AddItem("Class:") - 1;
                            lst_desc.SetItemText(n, 1, design.DisplayName());

                            if (design.type < Ship.STATION)
                                FormatNumber(txt, design.radius / 2);
                            else
                                FormatNumber(txt, design.radius * 2);

                            strcat_s(txt, " m");

                            n = lst_desc.AddItem("Length:") - 1;
                            lst_desc.SetItemText(n, 1, txt);

                            FormatNumber(txt, design.mass);
                            strcat_s(txt, " T");

                            n = lst_desc.AddItem("Mass:") - 1;
                            lst_desc.SetItemText(n, 1, txt);

                            FormatNumber(txt, design.integrity);
                            n = lst_desc.AddItem("Hull:") - 1;
                            lst_desc.SetItemText(n, 1, txt);

                            if (design.weapons.Count)
                            {
                                lst_desc.AddItem(" ");
                                n = lst_desc.AddItem("Weapons:") - 1;

                                WepGroup[] groups = new WepGroup[8];
                                for (int w = 0; w < design.weapons.Count; w++)
                                {
                                    Weapon gun = design.weapons[w];
                                    WepGroup group = FindWepGroup(groups, gun.Group());

                                    if (group != null)
                                        group.count++;
                                }

                                for (int g = 0; g < 8; g++)
                                {
                                    WepGroup group = groups[g];
                                    if (group != null && group.count != 0)
                                    {
                                        sprintf_s(txt, "%s (%d)", group.name, group.count);
                                        if (g > 0) n = lst_desc.AddItem(" ") - 1;
                                        lst_desc.SetItemText(n, 1, txt);
                                    }
                                }
                            }
                        }
                    }
                }

                else
                {
                    List<Combatant> combatants = campaign.GetCombatants();
                    Combatant c = combatants[0];

                    if (cmb_forces != null)
                    {
                        string name = cmb_forces.GetSelectedItem();

                        for (int i = 0; i < combatants.Count; i++)
                        {
                            c = combatants[i];

                            if (name == c.Name())
                            {
                                break;
                            }
                        }
                    }

                    if (c != null)
                    {
                        ShowCombatant(c);

                        lst_combat.ScrollTo(top_index);
                        lst_combat.SetSelected(old_index);
                    }
                }
            }

            if (btn_transfer != null && campaign != null && current_group != null)
                btn_transfer.SetEnabled(campaign.IsActive() &&
                CanTransfer(current_group));
#endif
            ErrLogger.PrintLine("WARNING: CmdForceDlg.OnCombat is not yet implemented.");
        }

        public virtual void OnTransfer(ActiveWindow obj, AWEvent evnt)
        {
            if (campaign != null && current_group != null)
            {

                // check player rank/responsibility:
                Player player = Player.GetCurrentPlayer();
                CLASSIFICATION cmd_class = CLASSIFICATION.FIGHTER;

                switch (current_group.Type())
                {
                    case CombatGroup.GROUP_TYPE.WING:
                    case CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON:
                    case CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON:
                        cmd_class = CLASSIFICATION.FIGHTER;
                        break;

                    case CombatGroup.GROUP_TYPE.ATTACK_SQUADRON:
                        cmd_class = CLASSIFICATION.ATTACK;
                        break;

                    case CombatGroup.GROUP_TYPE.LCA_SQUADRON:
                        cmd_class = CLASSIFICATION.LCA;
                        break;

                    case CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON:
                        cmd_class = CLASSIFICATION.DESTROYER;
                        break;

                    case CombatGroup.GROUP_TYPE.BATTLE_GROUP:
                        cmd_class = CLASSIFICATION.CRUISER;
                        break;

                    case CombatGroup.GROUP_TYPE.CARRIER_GROUP:
                    case CombatGroup.GROUP_TYPE.FLEET:
                        cmd_class = CLASSIFICATION.CARRIER;
                        break;
                }

                string transfer_info;

                if (player.CanCommand((int)cmd_class))
                {
                    if (current_unit != null)
                    {
                        campaign.SetPlayerUnit(current_unit);

                        transfer_info = string.Format("Your transfer request has been approved, {0} {1}.  You are now assigned to the {2}.  Good luck.\n\nFleet Admiral A. Evars FORCOM\nCommanding",
                           Player.RankName(player.Rank()),
                           player.Name(),
                           current_unit.GetDescription());
                    }
                    else
                    {
                        campaign.SetPlayerGroup(current_group);

                        transfer_info = string.Format("Your transfer request has been approved, {0} {1}.  You are now assigned to the {2}.  Good luck.\n\nFleet Admiral A. Evars FORCOM\nCommanding",
                           Player.RankName(player.Rank()),
                           player.Name(),
                           current_group.GetDescription());
                    }

                    Button.PlaySound(Button.SOUNDS.SND_ACCEPT);

                    CmdMsgDlg msgdlg = manager.GetCmdMsgDlg();
                    msgdlg.Title().SetText("Transfer Approved");
                    msgdlg.Message().SetText(transfer_info);
                    msgdlg.Message().SetTextAlign(TextFormat.DT_LEFT);

                    manager.ShowCmdMsgDlg();
                }

                else
                {
                    Button.PlaySound(Button.SOUNDS.SND_REJECT);

                    transfer_info = string.Format("Your transfer request has been denied, {0} {1}.  The {2} requires a command rank of {3}.  Please return to your unit and your duties.\n\nFleet Admiral A. Evars FORCOM\nCommanding",
                        Player.RankName(player.Rank()),
                        player.Name(),
                        current_group.GetDescription(),
                        Player.RankName(Player.CommandRankRequired((int)cmd_class)));

                    CmdMsgDlg msgdlg = manager.GetCmdMsgDlg();
                    msgdlg.Title().SetText("Transfer Denied");
                    msgdlg.Message().SetText(transfer_info);
                    msgdlg.Message().SetTextAlign(TextFormat.DT_LEFT);

                    manager.ShowCmdMsgDlg();
                }
            }
        }


        protected void ShowCombatant(Combatant c)
        {
            if (lst_combat == null || c == null) return;

            lst_combat.ClearItems();
            // ZeroMemory(pipe_stack, 32);

            CombatGroup force = c.GetForce();

            if (force != null)
            {
                List<CombatGroup> groups = force.GetComponents();
                for (int i = 0; i < groups.Count; i++)
                {
                    CombatGroup g = groups[i];
                    if (g.Type() < CombatGroup.GROUP_TYPE.CIVILIAN && g.CountUnits() > 0)
                        AddCombatGroup(g);
                }
            }

            current_group = null;
            current_unit = null;

            if (lst_desc != null) lst_desc.ClearItems();
            if (btn_transfer != null) btn_transfer.SetEnabled(false);
        }
        protected void AddCombatGroup(CombatGroup grp, bool last_child = false)
        {
            if (lst_combat == null || grp == null || grp.IntelLevel() < Intel.INTEL_TYPE.KNOWN) return;
#if TODO
            string prefix = "    ";

            if (grp.GetParent() == null || grp.GetParent().Type() == CombatGroup.GROUP_TYPE.FORCE)
            {
                if (!grp.IsExpanded())
                {
                    prefix[0] = Font.PIPE_PLUS;
                    prefix[1] = '\0';
                }
                else
                {
                    prefix[0] = Font.PIPE_MINUS;
                    prefix[1] = '\0';
                }
            }
            else
            {
                prefix[0] = last_child ? Font.PIPE_LL : Font.PIPE_LT;
                prefix[1] = Font.PIPE_HORZ;
                prefix[2] = '\0';
                prefix[3] = '\0';

                if (grp.GetLiveComponents().Count > 0 || grp.GetUnits().Count > 0)
                {
                    if (grp.IsExpanded())
                        prefix[1] = Font.PIPE_MINUS;
                    else
                        prefix[1] = Font.PIPE_PLUS;
                }
            }

            int index = lst_combat.AddItemWithData(
            pipe_stack +
            prefix +
            grp.GetDescription(),
            (DWORD)grp);
            lst_combat.SetItemData(index - 1, 1, 0);
            blank_line = false;

            int stacklen = strlen(pipe_stack);

            if (!grp.GetParent() || grp.GetParent().Type() == CombatGroup.FORCE)
                ; // no stack after first entry
            else if (prefix[0] == Font.PIPE_LT)
                pipe_stack[stacklen++] = Font.PIPE_VERT;
            else
                pipe_stack[stacklen++] = Font.PIPE_NBSP;
            pipe_stack[stacklen] = 0;

            if (grp.IsExpanded() && grp.GetUnits().Count > 0)
            {
                prefix[0] = (grp.GetLiveComponents().Count > 0) ? Font.PIPE_VERT : Font.PIPE_NBSP;
                prefix[1] = Font.PIPE_NBSP;
                prefix[2] = 0;
                prefix[3] = 0;

                foreach (CombatUnit unit in grp.GetUnits())
                {
                    string info;
                    int damage = (int)(100 * unit.GetSustainedDamage() / unit.GetDesign().integrity);

                    if (damage < 1 || unit.DeadCount() >= unit.Count())
                    {
                        sprintf_s(info, "%s%s%s", pipe_stack, prefix, unit.GetDescription());
                    }
                    else
                    {
                        sprintf_s(info, "%s%s%s %d%% damage", pipe_stack, prefix, unit.GetDescription(), damage);
                    }

                    int index = lst_combat.AddItemWithData(info, (DWORD)unit);

                    lst_combat.SetItemData(index - 1, 1, 1);
                    lst_combat.SetItemColor(index - 1, lst_combat.GetForeColor() * 0.65);
                }

                // blank line after last unit in group:
                lst_combat.AddItem(pipe_stack + prefix);
                blank_line = true;
            }

            if (grp.IsExpanded() && grp.GetLiveComponents().Count > 0)
            {
                List<CombatGroup> groups = grp.GetLiveComponents();
                for (int i = 0; i < groups.Count; i++)
                {
                    AddCombatGroup(groups[i], i == groups.Count - 1);
                }

                // blank line after last group in group:
                if (!blank_line)
                {
                    lst_combat.AddItem(pipe_stack);
                    blank_line = true;
                }
            }

            if (stacklen > 1)
                pipe_stack[stacklen - 1] = 0;
            else
                pipe_stack[0] = 0;
#endif
        }
        protected bool CanTransfer(CombatGroup grp)
        {
            if (grp == null || campaign == null)
                return false;

            if (grp.Type() < CombatGroup.GROUP_TYPE.WING)
                return false;

            if (grp.Type() > CombatGroup.GROUP_TYPE.CARRIER_GROUP)
                return false;

            if (grp.Type() == CombatGroup.GROUP_TYPE.FLEET ||
                    grp.Type() == CombatGroup.GROUP_TYPE.LCA_SQUADRON)
                return false;

            CombatGroup player_group = campaign.GetPlayerGroup();
            if (player_group.GetIFF() != grp.GetIFF())
                return false;

            return true;
        }
        protected bool IsVisible(Combatant c)
        {
            int nvis = 0;

            if (c != null)
            {
                CombatGroup force = c.GetForce();

                if (force != null)
                {
                    List<CombatGroup> groups = force.GetComponents();
                    for (int i = 0; i < groups.Count; i++)
                    {
                        CombatGroup g = groups[i];

                        if (g.Type() < CombatGroup.GROUP_TYPE.CIVILIAN &&
                                g.CountUnits() > 0 &&
                                g.IntelLevel() >= Intel.INTEL_TYPE.KNOWN)

                            nvis++;
                    }
                }
            }

            return nvis > 0;
        }


        protected CmpnScreen manager;

        protected ComboBox cmb_forces;
        protected ListBox lst_combat;
        protected ListBox lst_desc;
        protected Button btn_transfer;

        protected CombatGroup current_group;
        protected CombatUnit current_unit;

        static string pipe_stack;
        static bool blank_line = false;

    }
}