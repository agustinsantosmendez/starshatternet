﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars.exe
    ORIGINAL FILE:      StarSystem.h/StarSystem.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Various heavenly bodies
*/
using System;
using System.Collections.Generic;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Stars.Graphics.General;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars.StarSystems
{
    public class OrbitalBody : Orbital
    {
        public OrbitalBody(StarSystem sys, string n, OrbitalType t, double m, float r, double o, Orbital prime = null)
            : base(sys, n, t, m, r, o, prime)
        {
            light = 0; light_rep = null; back_light = null;
            ring_min = 0; ring_max = 0; tilt = 0; luminous = false;

            Update();
        }

        //public virtual ~OrbitalBody();

        // operations:
        public override void Update()
        {
            base.Update();

            theta = 0;
 
            if (rotation > 0)
                theta = -2 * Math.PI *  StarSystems.StarSystem.Stardate() / rotation;

            foreach (var body in satellites)
                body.Update();

            if (rep != null && theta != 0)
            {
                Matrix33D m = new Matrix33D();
                m.Pitch(tilt);
                m.Roll(tilt / 2);
                m.Yaw(theta);
                rep.SetOrientation(m);
            }

            if (light_rep != null)
            {
                Point bodyloc = loc;
                bodyloc = bodyloc.OtherHand();
                light_rep.MoveTo(bodyloc);
            }
         }

        // accessors:
        public List<OrbitalBody> Satellites() { return satellites; }

        public double Tilt() { return tilt; }
        public double RingMin() { return ring_min; }
        public double RingMax() { return ring_max; }

        public double LightIntensity() { return light; }
        public Color LightColor() { return color; }
        public bool Luminous() { return luminous; }

        internal protected string map_name;
        internal protected string tex_name;
        internal protected string tex_high_res;
        internal protected string tex_ring;
        internal protected string tex_glow;
        internal protected string tex_glow_high_res;
        internal protected string tex_gloss;

        internal protected double tscale;
        internal protected double light;
        internal protected double ring_min;
        internal protected double ring_max;
        internal protected double tilt;
        public LightNode light_rep;
        public LightNode back_light;
        internal protected Color color;
        internal protected Color back;
        internal protected Color atmosphere;
        internal protected bool luminous;

        internal protected List<OrbitalBody> satellites = new List<OrbitalBody>();
    }
}
