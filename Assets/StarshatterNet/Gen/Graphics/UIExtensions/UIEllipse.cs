﻿
using UnityEngine;
using UnityEngine.UI;

namespace StarshatterNet.Gen.Graphics
{
    [AddComponentMenu("UI/Extensions/Primitives/UI Ellipse")]
    public class UIEllipse : UIPrimitiveBase
    {
        public enum Mode { FillInside = 0, FillOutside = 1, Edge = 2 };

        [SerializeField]
        private int detail = 64;

        [SerializeField]
        public Mode mode;

        [SerializeField]
        [Tooltip("Edge mode only")]
        private float edgeThickness = 1;

        private Vector2 uv = Vector2.zero;
        private Color32 color32;

        private float width = 1f, height = 1f;
        private float deltaWidth, deltaHeight;
        private float deltaRadians;

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            Rect r = GetPixelAdjustedRect();

            color32 = color;
            width = r.width * 0.5f;
            height = r.height * 0.5f;

            vh.Clear();

            Vector2 pivot = rectTransform.pivot;
            deltaWidth = r.width * (0.5f - pivot.x);
            deltaHeight = r.height * (0.5f - pivot.y);

            if (mode == Mode.FillInside)
            {
                deltaRadians = 360f / detail * Mathf.Deg2Rad;
                FillInside(vh);
            }
            else if (mode == Mode.FillOutside)
            {
                int quarterDetail = (detail + 3) / 4;
                deltaRadians = 360f / (quarterDetail * 4) * Mathf.Deg2Rad;

                vh.AddVert(new Vector3(width + deltaWidth, height + deltaHeight, 0f), color32, new Vector2(1, 1));
                vh.AddVert(new Vector3(-width + deltaWidth, height + deltaHeight, 0f), color32, new Vector2(0, 1));
                vh.AddVert(new Vector3(-width + deltaWidth, -height + deltaHeight, 0f), color32, new Vector2(0, 0));
                vh.AddVert(new Vector3(width + deltaWidth, -height + deltaHeight, 0f), color32, new Vector2(1, 0));

                int triangleIndex = 4;
                FillOutside(vh, new Vector3(width + deltaWidth, deltaHeight, 0f), new Vector2(1, 0.5f), 0, quarterDetail, ref triangleIndex);
                FillOutside(vh, new Vector3(deltaWidth, height + deltaHeight, 0f), new Vector2(0.5f, 1f), 1, quarterDetail, ref triangleIndex);
                FillOutside(vh, new Vector3(-width + deltaWidth, deltaHeight, 0f), new Vector2(0.0f, 0.5f), 2, quarterDetail, ref triangleIndex);
                FillOutside(vh, new Vector3(deltaWidth, -height + deltaHeight, 0f), new Vector2(0.5f, 0.0f), 3, quarterDetail, ref triangleIndex);
            }
            else
            {
                deltaRadians = 360f / detail * Mathf.Deg2Rad;
                GenerateEdges(vh);
            }
        }

        private void FillInside(VertexHelper vh)
        {
            uv = new Vector2(0.5f, 0.5f);
            vh.AddVert(new Vector3(deltaWidth, deltaHeight, 0f), color32, uv);
            uv = new Vector2(1f, 0.5f);
            vh.AddVert(new Vector3(width + deltaWidth, deltaHeight, 0f), color32, uv);

            int triangleIndex = 2;
            for (int i = 1; i < detail; i++, triangleIndex++)
            {
                float radians = i * deltaRadians;
                uv = new Vector2(0.5f * (Mathf.Cos(radians) + 1), 0.5f * (Mathf.Sin(radians) + 1));
                vh.AddVert(new Vector3(Mathf.Cos(radians) * width + deltaWidth, Mathf.Sin(radians) * height + deltaHeight, 0f), color32, uv);
                vh.AddTriangle(triangleIndex, triangleIndex - 1, 0);
            }

            vh.AddTriangle(1, triangleIndex - 1, 0);
        }

        private void FillOutside(VertexHelper vh, Vector3 initialPoint, Vector2 initialUV, int quarterIndex, int detail, ref int triangleIndex)
        {
            int startIndex = quarterIndex * detail;
            int endIndex = (quarterIndex + 1) * detail;

            vh.AddVert(initialPoint, color32, initialUV);
            triangleIndex++;

            for (int i = startIndex + 1; i <= endIndex; i++, triangleIndex++)
            {
                float radians = i * deltaRadians;
                uv = new Vector2(0.5f * (Mathf.Cos(radians) + 1), 0.5f * (Mathf.Sin(radians) + 1));
                vh.AddVert(new Vector3(Mathf.Cos(radians) * width + deltaWidth, Mathf.Sin(radians) * height + deltaHeight, 0f), color32, uv);
                vh.AddTriangle(quarterIndex, triangleIndex - 1, triangleIndex);
            }
        }

        private void GenerateEdges(VertexHelper vh)
        {
            float innerWidth = width - edgeThickness;
            float innerHeight = height - edgeThickness;

            uv = new Vector2(1f, 0.5f);
            vh.AddVert(new Vector3(width + deltaWidth, deltaHeight, 0f), color32, uv);
            uv = new Vector2(0.5f * (innerWidth / width + 1), 0.5f);
            vh.AddVert(new Vector3(innerWidth + deltaWidth, deltaHeight, 0f), color32, uv);

            int triangleIndex = 2;
            for (int i = 1; i < detail; i++, triangleIndex += 2)
            {
                float radians = i * deltaRadians;
                float cos = Mathf.Cos(radians);
                float sin = Mathf.Sin(radians);

                uv = new Vector2(0.5f * (cos + 1), 0.5f * (sin + 1));
                vh.AddVert(new Vector3(cos * width + deltaWidth, sin * height + deltaHeight, 0f), color32, uv);
                uv = new Vector2(0.5f * (cos * innerWidth / width + 1), 0.5f * (sin * innerHeight / height + 1));
                vh.AddVert(new Vector3(cos * innerWidth + deltaWidth, sin * innerHeight + deltaHeight, 0f), color32, uv);

                vh.AddTriangle(triangleIndex, triangleIndex - 2, triangleIndex - 1);
                vh.AddTriangle(triangleIndex, triangleIndex - 1, triangleIndex + 1);
            }

            vh.AddTriangle(0, triangleIndex - 2, triangleIndex - 1);
            vh.AddTriangle(0, triangleIndex - 1, 1);
        }
    }
}
