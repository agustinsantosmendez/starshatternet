﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      ModConfig.h/ModConfig.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mod file deployment configuration and manager
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Config
{
    public class ModConfig
    {
        protected ModConfig()
        {
            Load();
            FindMods();
            Deploy();
        }

        public static void Initialize()
        {
            mod_config = new ModConfig();
        }
        public static void Close()
        {
            mod_config = null;
        }

        public static ModConfig Instance
        {
            get
            {
                if (mod_config == null) Initialize();
                return mod_config;
            }
        }


        public void Load(string filename = ModFilename)
        {
            if (!File.Exists(filename))
            {
                ErrLogger.PrintLine("WARNING: invalid {0} file. Using defaults", filename);
                Save();
            }
            else
            {
                using (XmlReader reader = XmlReader.Create(filename))
                    this.Load(reader);
            }
        }

        /// <summary>
        /// Loads the mod configuration entries.
        /// </summary>
        /// <param name="reader">
        /// An XML reader for th XML document containing the mod configuration.
        /// </param>
        public void Load(XmlReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException();

            if (!reader.ReadToFollowing("ModConfig"))
            {
                ErrLogger.PrintLine("ModConfig entry is missing. No mods deployed.");
                return;
            }
            enabled.Clear();
            while (reader.ReadToDescendant("Mod"))
            {

                var attr = reader.GetAttribute("path");
                if (!string.IsNullOrWhiteSpace(attr))
                {
                    enabled.Add(attr);
                }
            }
        }

        public void Save(string filename = ModFilename)
        {
            XmlWriterSettings writerSettings = new XmlWriterSettings()
            {
#if DEBUG
                Indent = true,
                NewLineOnAttributes = true,
#endif
            };

            using (XmlWriter writer = XmlWriter.Create(filename, writerSettings))
                this.Save(writer);
        }

        /// <summary>
        /// Saves the mod config entries.
        /// </summary>
        /// <param name="writer"> The XML writer to which the mod configuration is written.</param>
        public void Save(XmlWriter writer)
        {
            if (writer == null)
                return;

            writer.WriteStartElement("ModConfig");
            {
                foreach (var mod in enabled)
                {
                    writer.WriteStartElement("Mod");
                    writer.WriteAttributeString("path", mod);
                    writer.WriteEndElement();
                }
            }
            writer.WriteEndElement();
        }
        public void FindMods()
        {
#warning ModConfig.FindMods is not yet implemented
            ErrLogger.PrintLine("ModConfig FindMods() is not yet Implemented");
        }

        public bool IsDeployed(string name)
        {
#warning ModConfig.IsDeployed is not yet implemented
            throw new NotImplementedException();
        }
        public void Deploy()
        {
#warning ModConfig.Deploy is not yet implemented
            ErrLogger.PrintLine("ModConfig Deploy() is not yet Implemented");
        }

        public void Undeploy()
        {
#warning ModConfig.Undeploy is not yet implemented
            ErrLogger.PrintLine("ModConfig Undeploy() is not yet Implemented");
        }

        public void Redeploy()
        {
            ErrLogger.PrintLine("ModConfig Redeploy() is not yet Implemented");
        }


        // these methods change the configuration only
        // you must Redeploy() to have them take effect:

        public void EnableMod(string name) { throw new NotImplementedException(); }
        public void DisableMod(string name) { throw new NotImplementedException(); }
        public void IncreaseModPriority(int mod_index) { throw new NotImplementedException(); }
        public void DecreaseModPriority(int mod_index) { throw new NotImplementedException(); }

        public List<string> EnabledMods() { return enabled; }
        public List<string> DisabledMods() { return disabled; }
        public List<ModInfo> GetModInfoList() { return mods; }

        public ModInfo GetModInfo(string filename) { throw new NotImplementedException(); }


        private List<string> enabled = new List<string>();
        private List<string> disabled = new List<string>();
        private List<ModInfo> mods = new List<ModInfo>();
        private static ModConfig mod_config = null;
        private const string ModFilename = "mod.cfg";

    }
}
