﻿using System;
 using UnityEngine;
using UnityEngine.UI;

namespace  StarshatterNet.Stars.Graphics.Views
{
     public static class PopupMenuCreator
    {
        // based on: https://github.com/jamesjlinden/unity-decompiled/blob/master/UnityEngine.UI/UI/DefaultControls.cs
        public static GameObject CreatePopupMenu(GameObject canvas, Vector3 position, StarshatterNet.Stars.Views.Menu menu)
        {
            /// --------- Main Menu creation ------
            GameObject uiElementRoot = CreateUIElementRoot("PopupMenu", s_ThickElementSize);
            PopupMenu popupItem = uiElementRoot.AddComponent<PopupMenu>();
            popupItem.RootPopupMenu = uiElementRoot;

            Image image1 = uiElementRoot.AddComponent<Image>();
            //image1.sprite = resources.standard;
            //image1.type = Image.Type.Sliced;
            image1.color = s_DefaultSelectableColor;

            ContentSizeFitter contentSizeFitter1 = uiElementRoot.AddComponent<ContentSizeFitter>();
            contentSizeFitter1.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            contentSizeFitter1.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

            VerticalLayoutGroup verticalLayoutGroup = uiElementRoot.AddComponent<VerticalLayoutGroup>();
            verticalLayoutGroup.padding = new RectOffset(2, 0, 2, 2);
            verticalLayoutGroup.spacing = 3;
            verticalLayoutGroup.childAlignment = TextAnchor.LowerLeft;
            verticalLayoutGroup.childControlWidth = true;
            verticalLayoutGroup.childControlHeight = true;
            verticalLayoutGroup.childForceExpandWidth = true;
            verticalLayoutGroup.childForceExpandHeight = true;

            RectTransform rectTransform = uiElementRoot.GetComponent<RectTransform>();
            popupItem.menutemplate = rectTransform;
            /// --------- End Main Menu creation ------

            /// --------- Item Template creation ------
            GameObject template = CreateUIObject("Template", uiElementRoot);
            PopupItem itemTemplate = template.AddComponent<PopupItem>();
            itemTemplate.DefaultSelectableColor = s_DefaultSelectableColor;
            itemTemplate.DefaultSelectedColor = s_DefaultSelectedColor;

            Image imageItem = template.AddComponent<Image>();
            //imageItem.sprite = resources.mask;
            //imageItem.type = Image.Type.Sliced;
            imageItem.color = s_DefaultSelectableColor;

            HorizontalLayoutGroup horizontalLayoutGroup = template.AddComponent<HorizontalLayoutGroup>();
            horizontalLayoutGroup.padding = new RectOffset(5, 5, 2, 2);
            horizontalLayoutGroup.spacing = 10;
            horizontalLayoutGroup.childAlignment = TextAnchor.LowerLeft;
            horizontalLayoutGroup.childControlWidth = true;
            horizontalLayoutGroup.childControlHeight = false;
            horizontalLayoutGroup.childForceExpandWidth = true;
            horizontalLayoutGroup.childForceExpandHeight = true;

            GameObject textObj = CreateUIObject("Text", template);
            RectTransform rectTransformtext = textObj.GetComponent<RectTransform>();
            rectTransformtext.sizeDelta = new Vector2(0, 16);

            Text text = textObj.AddComponent<Text>();
            text.color = Color.black;
            text.alignment = TextAnchor.MiddleLeft;
            text.font = FontMgr.GetDefault().font;

            LayoutElement layoutText = textObj.AddComponent<LayoutElement>();
            layoutText.flexibleWidth = 1000;
            layoutText.flexibleHeight = 1000;

            GameObject arrowObj = CreateUIObject("Arrow", template);
            RectTransform rectTransformArrow = arrowObj.GetComponent<RectTransform>();
            rectTransformArrow.localScale = new Vector3(1, 0.7f, 1);
            rectTransformArrow.sizeDelta = new Vector2(16, 16);
            Image imageArrow = arrowObj.AddComponent<Image>();
            imageArrow.sprite = Resources.Load<UnityEngine.Sprite>("Screens/RightArrow");
            imageArrow.color = s_DefaultSelectableColor;
            LayoutElement layoutArrow = arrowObj.AddComponent<LayoutElement>();
            layoutArrow.preferredWidth = 20;
            layoutArrow.preferredHeight = 14;
            layoutArrow.flexibleWidth = 0;
            layoutArrow.flexibleHeight = 0;

            Toggle toggle = template.GetComponent<Toggle>();
            //toggle.targetGraphic = (Graphic)imageArrow;
            toggle.graphic = (Graphic)imageArrow;

            template.SetActive(false);

            popupItem.itemtemplate = template.GetComponent<RectTransform>();

            ///---------- End Item Template creation

            popupItem.menu = menu;

            // Update the canvases to get the updated rect of the text.
            Canvas.ForceUpdateCanvases();
            rectTransform.localPosition = position - new Vector3(0, rectTransform.rect.height, 0);
            SetParentAndAlign(uiElementRoot, canvas);

            return uiElementRoot;
        }

        private static GameObject CreateUIElementRoot(string name, Vector2 size)
        {
            GameObject gameObject = new GameObject(name);
            RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
            rectTransform.sizeDelta = size;
            rectTransform.anchorMax = Vector2.right;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.pivot = Vector2.zero;
            return gameObject;
        }
        private static GameObject CreateUIObject(string name, GameObject parent)
        {
            GameObject child = new GameObject(name);
            child.AddComponent<RectTransform>();
            SetParentAndAlign(child, parent);
            return child;
        }
        public static void SetParentAndAlign(GameObject child, GameObject parent)
        {
            if ((UnityEngine.Object)parent == (UnityEngine.Object)null)
                return;
            child.transform.SetParent(parent.transform, false);
            SetLayerRecursively(child, parent.layer);
        }
        private static void SetLayerRecursively(GameObject go, int layer)
        {
            go.layer = layer;
            Transform transform = go.transform;
            for (int index = 0; index < transform.childCount; ++index)
                SetLayerRecursively(transform.GetChild(index).gameObject, layer);
        }

        private static Vector2 s_ThickElementSize = new Vector2(160f, 30f);
        public static Color s_DefaultSelectableColor = new Color(1f, 1f, 1f, 1f);
        public static Color s_DefaultSelectedColor = new Color(0.7f, 0.7f, 0.7f, 1f);
    }
}
