﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    FILE:         MotionController.h/MotionController.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Abstract MotionController class (hides details of Joystick, Keyboard, etc.)
*/
using StarshatterNet.Config;

namespace StarshatterNet.Controllers
{
    public class MotionController
    {
        public MotionController()
        {
            status = StatusValue.StatusOK; sensitivity = 1; dead_zone = 0;
            swapped = false; inverted = 0; rudder = 0; throttle = 0; select = 0;
        }

        // public virtual ~MotionController() { }

        public enum StatusValue { StatusOK, StatusErr, StatusBadParm };
        public const int MaxActions = 32;

        public StatusValue Status() { return status; }
        public int Sensitivity() { return sensitivity; }
        public int DeadZone() { return dead_zone; }
        public bool Swapped() { return swapped; }
        public int Inverted() { return inverted; }
        public int RudderEnabled() { return rudder; }
        public int ThrottleEnabled() { return throttle; }
        public int Selector() { return select; }


        // setup:
        public virtual void SetSensitivity(int sense, int dead)
        {
            if (sense > 0) sensitivity = sense;
            if (dead > 0) dead_zone = dead;
        }

        public virtual void SetSelector(int sel) { select = sel; }
        public virtual void SetRudderEnabled(int rud) { rudder = rud; }
        public virtual void SetThrottleEnabled(int t) { throttle = t; }

        public virtual void SwapYawRoll(bool swap) { swapped = swap; }
        public virtual bool GetSwapYawRoll() { return swapped; }
        public virtual void InvertPitch(int inv) { inverted = inv; }
        public virtual int GetInverted() { return inverted; }

        public virtual void MapKeys(KeyMapEntry[] mapping, int nkeys) { }

        // sample the physical device
        public virtual void Acquire() { }

        // translations
        public virtual double X() { return 0; }
        public virtual double Y() { return 0; }
        public virtual double Z() { return 0; }

        // rotations
        public virtual double Pitch() { return 0; }
        public virtual double Roll() { return 0; }
        public virtual double Yaw() { return 0; }
        public virtual int Center() { return 0; }

        // throttle
        public virtual double Throttle() { return 0; }
        public virtual void SetThrottle(double t) { }

        // actions
        public virtual bool Action(int n) { return false; }
        public virtual bool ActionMap(int n) { return false; }


        protected StatusValue status;
        protected int sensitivity;
        protected int dead_zone;
        protected bool swapped;
        protected int inverted;
        protected int rudder;
        protected int throttle;
        protected int select;
    }
}
