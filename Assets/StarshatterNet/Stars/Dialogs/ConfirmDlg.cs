﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      ConfirmDlg.h/ConfirmDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    General-purpose confirmation dialog class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class ConfirmDlg : FormWindow
    {
        public ConfirmDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
                base(s, 0, 0, s.Width(), s.Height(), c, p, "ConfirmDlg")
        {
            manager = mgr;
            parent_control = null; btn_apply = null; btn_cancel = null;

            Init(def);
        }
        //public virtual ~ConfirmDlg();
        public override void RegisterControls()
        {
            if (btn_apply != null)
                return;

            btn_apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, btn_apply, OnApply);

            btn_cancel = (Button)FindControl(2);
            REGISTER_CLIENT(ETD.EID_CLICK, btn_cancel, OnCancel);

            lbl_title = FindControl(100);
            lbl_message = FindControl(101);
        }
        public override void Show()
        {
            if (!IsShown())
            {
                Button.PlaySound(Button.SOUNDS.SND_CONFIRM);
            }

            base.Show();
            //TODO SetFocus();
        }
        public ActiveWindow GetParentControl()
        {
            return parent_control;
        }
        public void SetParentControl(ActiveWindow p)
        {
            parent_control = p;
        }

        public string GetTitle()
        {
            if (lbl_title != null)
                return lbl_title.GetText();

            return "";
        }
        public void SetTitle(string t)
        {
            if (lbl_title != null)
                lbl_title.SetText(t);
        }

        public string GetMessage()
        {
            if (lbl_message != null)
                return lbl_message.GetText();

            return "";
        }
        public void SetMessage(string m)
        {
            if (lbl_message != null)
                lbl_message.SetText(m);
        }

        // Operations:
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnApply(null, null);
            }

            if (Keyboard.KeyDown(KeyMap.VK_ESCAPE))
            {
                OnCancel(null, null);
            }
        }

        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            manager.HideConfirmDlg();

            if (parent_control != null)
                parent_control.ClientEvent(ETD.EID_USER_1);
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt) { manager.HideConfirmDlg(); }


        protected MenuScreen manager;

        protected ActiveWindow lbl_title;
        protected ActiveWindow lbl_message;

        protected Button btn_apply;
        protected Button btn_cancel;

        protected ActiveWindow parent_control;
    }
}