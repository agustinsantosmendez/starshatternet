﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Ship.h/Ship.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Starship (or space/ground station) class
*/
using System;
using System.IO;
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.SimElements;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using DWORD = System.UInt32;
using StarshatterNet.Audio;

namespace StarshatterNet.Stars.Serialization
{
    public static class ShipDesignSerializer
    {
        public static ShipDesignEntry JSONReadShipDesign(string path, string filename)
        {
            var shipdesign = ReaderSupport.DeserializeAsset<ShipDesignEntry>(Path.Combine(path, filename));

            return shipdesign;
        }
        public static ShipDesignEntry JSONReadShipDesignOverwrite(string path, string filename, SimElements.ShipDesign shipdesign)
        {
            string fullfilename = Path.Combine(path, filename);
            ShipDesignEntry sde = (ShipDesignEntry)shipdesign;
            ReaderSupport.DeserializeAssetOverwrite<ShipDesignEntry>(fullfilename, sde);
            ShipDesignEntry.Copy(sde, shipdesign, fullfilename);

            return sde;
        }

    }

    [Serializable]
    public class ShipDesignEntry
    {
        public Ship SHIP;

        public static explicit operator ShipDesignEntry(SimElements.ShipDesign sd)
        {
            ShipDesignEntry sde = new ShipDesignEntry();
            sde.SHIP = new Ship();

            sde.SHIP.@class = SimElements.ShipDesign.ship_design_class_name[(int)sd.type];
            sde.SHIP.name = sd.name;
            sde.SHIP.description = sd.description;
            sde.SHIP.display_name = sd.display_name;
            sde.SHIP.abrv = sd.abrv;

            sde.SHIP.pcs = sd.pcs.ToString();
            sde.SHIP.acs = sd.acs.ToString();
            sde.SHIP.detet = sd.detet.ToString();
            sde.SHIP.scale = sd.scale.ToString();
            sde.SHIP.explosion_scale = sd.explosion_scale.ToString();
            sde.SHIP.mass = sd.mass.ToString();
            sde.SHIP.vlimit = sd.vlimit.ToString();
            sde.SHIP.agility = sd.agility.ToString();
            sde.SHIP.air_factor = sd.air_factor.ToString();
            sde.SHIP.roll_rate = sd.roll_rate.ToString();
            sde.SHIP.pitch_rate = sd.pitch_rate.ToString();
            sde.SHIP.yaw_rate = sd.yaw_rate.ToString();
            sde.SHIP.integrity = sd.integrity.ToString();
            sde.SHIP.drag = sd.drag.ToString();
            sde.SHIP.arcade_drag = sd.arcade_drag.ToString();
            sde.SHIP.roll_drag = sd.roll_drag.ToString();
            sde.SHIP.pitch_drag = sd.pitch_drag.ToString();
            sde.SHIP.yaw_drag = sd.yaw_drag.ToString();
            sde.SHIP.trans_x = sd.trans_x.ToString();
            sde.SHIP.trans_y = sd.trans_y.ToString();
            sde.SHIP.trans_z = sd.trans_z.ToString();
            sde.SHIP.turn_bank = sd.turn_bank.ToString();
            sde.SHIP.cockpit_scale = sd.cockpit_scale.ToString();
            sde.SHIP.auto_roll = sd.auto_roll.ToString();

            sde.SHIP.CL = sd.CL.ToString();
            sde.SHIP.CD = sd.CD.ToString();
            sde.SHIP.stall = sd.stall.ToString();

            sde.SHIP.prep_time = sd.prep_time.ToString();
            sde.SHIP.avoid_time = sd.avoid_time.ToString();
            sde.SHIP.avoid_fighter = sd.avoid_fighter.ToString();
            sde.SHIP.avoid_strike = sd.avoid_strike.ToString();
            sde.SHIP.avoid_target = sd.avoid_target.ToString();
            sde.SHIP.commit_range = sd.commit_range.ToString();


            sde.SHIP.splash_radius = sd.splash_radius.ToString();
            sde.SHIP.scuttle = sd.scuttle.ToString();
            sde.SHIP.repair_speed = sd.repair_speed.ToString();
            sde.SHIP.repair_teams = sd.repair_teams.ToString();
            sde.SHIP.secret = sd.secret;
            sde.SHIP.repair_auto = sd.repair_auto;
            sde.SHIP.repair_screen = sd.repair_screen;
            sde.SHIP.wep_screen = sd.wep_screen;

            return sde;
        }
        public static void Copy(ShipDesignEntry sde, SimElements.ShipDesign sd, string file)
        {
            bool degrees = sde.SHIP.degrees;

            if (!string.IsNullOrEmpty(sde.SHIP.cockpit_model))
                sd.cockpit_name = sde.SHIP.cockpit_model;

            if (string.IsNullOrEmpty(sde.SHIP.detail_0))
                ErrLogger.PrintLine("WARNING: invalid or missing detail_0 in'{0}' ", file);
            else
                SimElements.ShipDesign.detail[0].Add(sde.SHIP.detail_0);

            if (!string.IsNullOrEmpty(sde.SHIP.detail_1))
                SimElements.ShipDesign.detail[1].Add(sde.SHIP.detail_1);

            if (!string.IsNullOrEmpty(sde.SHIP.detail_2))
                SimElements.ShipDesign.detail[2].Add(sde.SHIP.detail_2);

            if (!string.IsNullOrEmpty(sde.SHIP.detail_3))
                SimElements.ShipDesign.detail[3].Add(sde.SHIP.detail_3);

            sd.spin_rates.Add(sde.SHIP.spin);

            if (!PointExtensions.IsNullOrEmpty(sde.SHIP.offset_0))
                SimElements.ShipDesign.offset[0].Add(sde.SHIP.offset_0);

            if (!PointExtensions.IsNullOrEmpty(sde.SHIP.offset_1))
                SimElements.ShipDesign.offset[1].Add(sde.SHIP.offset_1);

            if (!PointExtensions.IsNullOrEmpty(sde.SHIP.offset_2))
                SimElements.ShipDesign.offset[3].Add(sde.SHIP.offset_2);

            if (!PointExtensions.IsNullOrEmpty(sde.SHIP.offset_3))
                SimElements.ShipDesign.offset[3].Add(sde.SHIP.offset_3);

            if (!PointExtensions.IsNullOrEmpty(sde.SHIP.beauty))
            {
                sd.beauty_cam = sde.SHIP.beauty;
                if (degrees)
                {
                    sd.beauty_cam.X *= (float)ConstantsF.DEGREES;
                    sd.beauty_cam.Y *= (float)ConstantsF.DEGREES;
                }
            }
            if (!string.IsNullOrEmpty(sde.SHIP.hud_icon))
                sd.hud_icon = new Bitmap(sde.SHIP.hud_icon);

            if (!string.IsNullOrEmpty(sde.SHIP.feature_0))
                if (!float.TryParse(sde.SHIP.feature_0, out sd.feature_size[0]))
                    ErrLogger.PrintLine("WARNING: invalid or missing feature_0 in '{0}' ", file);

            if (!string.IsNullOrEmpty(sde.SHIP.feature_1))
                if (!float.TryParse(sde.SHIP.feature_1, out sd.feature_size[1]))
                    ErrLogger.PrintLine("WARNING: invalid or missing feature_1 in '{0}' ", file);

            if (!string.IsNullOrEmpty(sde.SHIP.feature_2))
                if (!float.TryParse(sde.SHIP.feature_2, out sd.feature_size[2]))
                    ErrLogger.PrintLine("WARNING: invalid or missing feature_2 in '{0}' ", file);

            if (!string.IsNullOrEmpty(sde.SHIP.feature_3))
                if (!float.TryParse(sde.SHIP.feature_3, out sd.feature_size[3]))
                    ErrLogger.PrintLine("WARNING: invalid or missing feature_3 in '{0}' ", file);

            if (string.IsNullOrEmpty(sde.SHIP.@class))
                ErrLogger.PrintLine("WARNING: invalid or missing ship class in '{0}' ", file);
            else
            {
                sd.type = SimElements.ShipDesign.ClassForName(sde.SHIP.@class);
                if (sd.type <= SimElements.CLASSIFICATION.LCA)
                {
                    sd.repair_auto = false;
                    sd.repair_screen = false;
                    sd.wep_screen = false;
                }
            }
            if (string.IsNullOrEmpty(sde.SHIP.name))
                ErrLogger.PrintLine("WARNING: invalid or missing ship name in '{0}' ", file);
            else
                sd.name = sde.SHIP.name;

            if (!string.IsNullOrEmpty(sde.SHIP.description))
                sd.description = Game.GetText(sde.SHIP.description);
            sd.display_name = sde.SHIP.display_name;
            sd.abrv = sde.SHIP.abrv;

            if (!string.IsNullOrEmpty(sde.SHIP.pcs))
                float.TryParse(sde.SHIP.pcs, out sd.pcs);
            if (!string.IsNullOrEmpty(sde.SHIP.acs))
                float.TryParse(sde.SHIP.acs, out sd.acs);
            if (!string.IsNullOrEmpty(sde.SHIP.detet))
                float.TryParse(sde.SHIP.detet, out sd.detet);
            if (!string.IsNullOrEmpty(sde.SHIP.scale))
                float.TryParse(sde.SHIP.scale, out sd.scale);
            if (!string.IsNullOrEmpty(sde.SHIP.explosion_scale))
                float.TryParse(sde.SHIP.explosion_scale, out sd.explosion_scale);
            if (!string.IsNullOrEmpty(sde.SHIP.mass))
                float.TryParse(sde.SHIP.mass, out sd.mass);
            if (!string.IsNullOrEmpty(sde.SHIP.vlimit))
                float.TryParse(sde.SHIP.vlimit, out sd.vlimit);
            if (!string.IsNullOrEmpty(sde.SHIP.agility))
                float.TryParse(sde.SHIP.agility, out sd.agility);
            if (!string.IsNullOrEmpty(sde.SHIP.air_factor))
                float.TryParse(sde.SHIP.air_factor, out sd.air_factor);
            if (!string.IsNullOrEmpty(sde.SHIP.roll_rate))
                float.TryParse(sde.SHIP.roll_rate, out sd.roll_rate);
            if (!string.IsNullOrEmpty(sde.SHIP.pitch_rate))
                float.TryParse(sde.SHIP.pitch_rate, out sd.pitch_rate);
            if (!string.IsNullOrEmpty(sde.SHIP.yaw_rate))
                float.TryParse(sde.SHIP.yaw_rate, out sd.yaw_rate);
            if (!string.IsNullOrEmpty(sde.SHIP.integrity))
                float.TryParse(sde.SHIP.integrity, out sd.integrity);
            if (!string.IsNullOrEmpty(sde.SHIP.drag))
                float.TryParse(sde.SHIP.drag, out sd.drag);
            if (!string.IsNullOrEmpty(sde.SHIP.arcade_drag))
                float.TryParse(sde.SHIP.arcade_drag, out sd.arcade_drag);
            if (!string.IsNullOrEmpty(sde.SHIP.roll_drag))
                float.TryParse(sde.SHIP.roll_drag, out sd.roll_drag);
            if (!string.IsNullOrEmpty(sde.SHIP.pitch_drag))
                float.TryParse(sde.SHIP.pitch_drag, out sd.pitch_drag);
            if (!string.IsNullOrEmpty(sde.SHIP.yaw_drag))
                float.TryParse(sde.SHIP.yaw_drag, out sd.yaw_drag);
            if (!string.IsNullOrEmpty(sde.SHIP.trans_x))
                float.TryParse(sde.SHIP.trans_x, out sd.trans_x);
            if (!string.IsNullOrEmpty(sde.SHIP.trans_y))
                float.TryParse(sde.SHIP.trans_y, out sd.trans_y);
            if (!string.IsNullOrEmpty(sde.SHIP.trans_z))
                float.TryParse(sde.SHIP.trans_z, out sd.trans_z);
            if (!string.IsNullOrEmpty(sde.SHIP.turn_bank))
                float.TryParse(sde.SHIP.turn_bank, out sd.turn_bank);
            if (!string.IsNullOrEmpty(sde.SHIP.cockpit_scale))
                float.TryParse(sde.SHIP.cockpit_scale, out sd.cockpit_scale);
            if (!string.IsNullOrEmpty(sde.SHIP.auto_roll))
                int.TryParse(sde.SHIP.auto_roll, out sd.auto_roll);

            if (!string.IsNullOrEmpty(sde.SHIP.CL))
                float.TryParse(sde.SHIP.CL, out sd.CL);
            if (!string.IsNullOrEmpty(sde.SHIP.CD))
                float.TryParse(sde.SHIP.CD, out sd.CD);
            if (!string.IsNullOrEmpty(sde.SHIP.stall))
                float.TryParse(sde.SHIP.stall, out sd.stall);

            if (!string.IsNullOrEmpty(sde.SHIP.prep_time))
                float.TryParse(sde.SHIP.prep_time, out sd.prep_time);
            if (!string.IsNullOrEmpty(sde.SHIP.avoid_time))
                float.TryParse(sde.SHIP.avoid_time, out sd.avoid_time);
            if (!string.IsNullOrEmpty(sde.SHIP.avoid_fighter))
                float.TryParse(sde.SHIP.avoid_fighter, out sd.avoid_fighter);
            if (!string.IsNullOrEmpty(sde.SHIP.avoid_strike))
                float.TryParse(sde.SHIP.avoid_strike, out sd.avoid_strike);
            if (!string.IsNullOrEmpty(sde.SHIP.avoid_target))
                float.TryParse(sde.SHIP.avoid_target, out sd.avoid_target);
            if (!string.IsNullOrEmpty(sde.SHIP.commit_range))
                float.TryParse(sde.SHIP.commit_range, out sd.commit_range);

            if (!string.IsNullOrEmpty(sde.SHIP.splash_radius))
                float.TryParse(sde.SHIP.splash_radius, out sd.splash_radius);
            if (!string.IsNullOrEmpty(sde.SHIP.scuttle))
                float.TryParse(sde.SHIP.scuttle, out sd.scuttle);
            if (!string.IsNullOrEmpty(sde.SHIP.repair_speed))
                float.TryParse(sde.SHIP.repair_speed, out sd.repair_speed);
            if (!string.IsNullOrEmpty(sde.SHIP.repair_teams))
                int.TryParse(sde.SHIP.repair_teams, out sd.repair_teams);
            sd.secret = sde.SHIP.secret;
            sd.repair_auto = sde.SHIP.repair_auto;
            sd.repair_screen = sde.SHIP.repair_screen;
            sd.wep_screen = sde.SHIP.wep_screen;

            if (!string.IsNullOrEmpty(sde.SHIP.emcon_1))
                float.TryParse(sde.SHIP.emcon_1, out sd.e_factor[0]);
            if (!string.IsNullOrEmpty(sde.SHIP.emcon_2))
                float.TryParse(sde.SHIP.emcon_2, out sd.e_factor[1]);
            if (!string.IsNullOrEmpty(sde.SHIP.emcon_3))
                float.TryParse(sde.SHIP.emcon_3, out sd.e_factor[2]);

            if (!PointExtensions.IsNullOrEmpty(sde.SHIP.chase))
                sd.chase_vec = sde.SHIP.chase * (float)sd.scale;
            if (!PointExtensions.IsNullOrEmpty(sde.SHIP.bridge))
                sd.bridge_vec = sde.SHIP.bridge * (float)sd.scale;

            if (sde.SHIP.power != null)
                ParsePower(sde.SHIP.power, sd, file);
            if (sde.SHIP.drive != null)
                ParseDrive(sde.SHIP.drive, sd, file);
            if (sde.SHIP.quantum_drive != null)
                ParseQuantumDrive(sde.SHIP.quantum_drive, sd, file);
            if (sde.SHIP.farcaster != null)
                ParseFarcaster(sde.SHIP.farcaster, sd, file);
            if (sde.SHIP.thruster != null)
                ParseThruster(sde.SHIP.thruster, sd, file);
            if (sde.SHIP.navlight != null)
                ParseNavlight(sde.SHIP.navlight, sd, file);
            if (sde.SHIP.flightdeck != null)
                ParseFlightDeck(sde.SHIP.flightdeck, sd, file);
            if (sde.SHIP.gear != null)
                ParseLandingGear(sde.SHIP.gear, sd, file);
            if (sde.SHIP.weapon != null)
                ParseWeapon(sde.SHIP.weapon, sd, file);
            if (sde.SHIP.hardpoint != null)
                ParseHardPoint(sde.SHIP.hardpoint, sd, file);
            if (sde.SHIP.loadout != null && sde.SHIP.loadout.Length != 0)
                ParseLoadout(sde.SHIP.loadout, sd, file);
            if (sde.SHIP.decoy != null)
                ParseWeapon(sde.SHIP.decoy, sd, file);
            if (sde.SHIP.probe != null)
                ParseWeapon(sde.SHIP.probe, sd, file);
            if (sde.SHIP.sensor != null)
                ParseSensor(sde.SHIP.sensor, sd, file);
            if (sde.SHIP.nav != null)
                ParseNavsys(sde.SHIP.nav, sd, file);
            if (sde.SHIP.computer != null)
                ParseComputer(sde.SHIP.computer, sd, file);
            if (sde.SHIP.shield != null)
                ParseShield(sde.SHIP.shield, sd, file);
            if (sde.SHIP.death_spiral != null)
                ParseDeathSpiral(sde.SHIP.death_spiral, sd, file);
            if (sde.SHIP.map != null)
                ParseMap(sde.SHIP.map, sd, file);
            if (sde.SHIP.squadron != null)
                ParseSquadron(sde.SHIP.squadron, sd, file);
            if (sde.SHIP.skin != null)
                ParseSkin(sde.SHIP.skin, sd, file);
        }
        public static void ParsePower(Power val, ShipDesign sd, string filename)
        {
            PowerSource.SUBTYPE stype = 0;
            float output = 1000.0f;
            float fuel = 0.0f;
            Point loc = new Point(0.0f, 0.0f, 0.0f);
            float size = 0.0f;
            float hull = 0.5f;
            string design_name = null;
            string pname = null;
            string pabrv = null;
            Graphics.General.Explosion.ExplosionType etype = 0;
            int emcon_1 = -1;
            int emcon_2 = -1;
            int emcon_3 = -1;

            if (!string.IsNullOrEmpty(val.type))
            {
                string tname = val.type;
                if (tname.StartsWith("B")) stype = PowerSource.SUBTYPE.BATTERY;
                else if (tname.StartsWith("A")) stype = PowerSource.SUBTYPE.AUX;
                else if (tname.StartsWith("F")) stype = PowerSource.SUBTYPE.FUSION;
                else ErrLogger.PrintLine("WARNING: unknown power source type '{0}' in '{1}'", tname, filename);
            }
            if (!string.IsNullOrEmpty(val.name))
                pname = val.name;
            if (!string.IsNullOrEmpty(val.abrv))
                pabrv = val.abrv;
            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;
            if (!string.IsNullOrEmpty(val.max_output))
                output = float.Parse(val.max_output);
            if (!string.IsNullOrEmpty(val.fuel_range))
                fuel = float.Parse(val.fuel_range);
            if (!val.loc.IsNullOrEmpty())
            {
                loc = val.loc;
                loc *= (float)sd.scale;
            }
            if (!string.IsNullOrEmpty(val.size))
            {
                size = float.Parse(val.size);
                size *= (float)sd.scale;
            }
            if (!string.IsNullOrEmpty(val.hull_factor))
                hull = float.Parse(val.hull_factor);
            etype = (Graphics.General.Explosion.ExplosionType)val.explosion;
            emcon_1 = val.emcon_1;
            emcon_2 = val.emcon_2;
            emcon_3 = val.emcon_3;

            PowerSource source = new PowerSource((PowerSource.SUBTYPE)stype, output);
            if (!string.IsNullOrEmpty(pname)) source.SetName(pname);
            if (!string.IsNullOrEmpty(pabrv)) source.SetName(pabrv);
            source.SetFuelRange(fuel);
            source.Mount(loc, size, hull);
            source.SetExplosionType(etype);

            if (!string.IsNullOrEmpty(design_name))
            {
                SystemDesign sdtmp = SystemDesign.Find(design_name);
                if (sd != null)
                    source.SetDesign(sdtmp);
            }

            if (emcon_1 >= 0 && emcon_1 <= 100)
                source.SetEMCONPower(1, emcon_1);

            if (emcon_2 >= 0 && emcon_2 <= 100)
                source.SetEMCONPower(1, emcon_2);

            if (emcon_3 >= 0 && emcon_3 <= 100)
                source.SetEMCONPower(1, emcon_3);

            sd.reactors.Add(source);
        }
        public static void ParseDrive(Drive val, ShipDesign sd, string filename)
        {
            string dname = null;
            string dabrv = null;
            SimElements.Drive.SUBTYPE dtype = 0;
            Graphics.General.Explosion.ExplosionType etype = 0;
            float dthrust = 1.0f;
            float daug = 0.0f;
            float dscale = 1.0f;
            Point loc = new Point(0.0f, 0.0f, 0.0f);
            float size = 0.0f;
            float hull = 0.5f;
            string design_name = null;
            int emcon_1 = -1;
            int emcon_2 = -1;
            int emcon_3 = -1;
            bool trail = true;
            SimElements.Drive drive = null;

            if (!string.IsNullOrEmpty(val.type))
            {
                string tval = val.type;
                if (tval == "Plasma") dtype = SimElements.Drive.SUBTYPE.PLASMA;
                else if (tval == "Fusion") dtype = SimElements.Drive.SUBTYPE.FUSION;
                else if (tval == "Alien") dtype = SimElements.Drive.SUBTYPE.GREEN;
                else if (tval == "Green") dtype = SimElements.Drive.SUBTYPE.GREEN;
                else if (tval == "Red") dtype = SimElements.Drive.SUBTYPE.RED;
                else if (tval == "Blue") dtype = SimElements.Drive.SUBTYPE.BLUE;
                else if (tval == "Yellow") dtype = SimElements.Drive.SUBTYPE.YELLOW;
                else if (tval == "Stealth") dtype = SimElements.Drive.SUBTYPE.STEALTH;
                else ErrLogger.PrintLine("WARNING: unknown drive type '{0}' in '{1}'\n", tval, filename);
            }
            if (!string.IsNullOrEmpty(val.name))
                dname = val.name;
            if (!string.IsNullOrEmpty(val.abrv))
                dabrv = val.abrv;
            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;
            dthrust = val.thrust;
            daug = val.augmenter;
            dscale = val.scale;
            if (val.port != null && val.port.Length > 0)
            {
                foreach (var port in val.port)
                {
                    Point p = port * sd.scale;
                    float flare_scale = dscale;

                    if (drive == null)
                        drive = new SimElements.Drive(dtype, dthrust, daug, trail);
                    drive.AddPort(port, flare_scale);
                }
            }
            if (!val.loc.IsNullOrEmpty())
                loc = val.loc * (float)sd.scale;
            size = val.size * sd.scale;
            hull = val.hull_factor;
            etype = (Graphics.General.Explosion.ExplosionType)val.explosion;
            emcon_1 = val.emcon_1;
            emcon_2 = val.emcon_2;
            emcon_3 = val.emcon_3;
            trail = val.trail;

            if (drive == null)
                drive = new SimElements.Drive(dtype, dthrust, daug, trail);

            drive.SetSourceIndex(sd.reactors.Count - 1);
            drive.Mount(loc, size, hull);
            if (!string.IsNullOrEmpty(dname)) drive.SetName(dname);
            if (!string.IsNullOrEmpty(dabrv)) drive.SetAbbreviation(dabrv);
            drive.SetExplosionType(etype);

            if (!string.IsNullOrEmpty(design_name))
            {
                SystemDesign sdtmp = SystemDesign.Find(design_name);
                if (sdtmp != null)
                    drive.SetDesign(sdtmp);
            }

            if (emcon_1 >= 0 && emcon_1 <= 100)
                drive.SetEMCONPower(1, emcon_1);

            if (emcon_2 >= 0 && emcon_2 <= 100)
                drive.SetEMCONPower(1, emcon_2);

            if (emcon_3 >= 0 && emcon_3 <= 100)
                drive.SetEMCONPower(1, emcon_3);

            sd.main_drive = sd.drives.Count;
            sd.drives.Add(drive);
        }
        public static void ParseQuantumDrive(QuantumDrive val, ShipDesign sd, string filename)
        {
            double capacity = 250e3;
            double consumption = 1e3;
            Point loc = new Point(0.0f, 0.0f, 0.0f);
            float size = 0.0f;
            float hull = 0.5f;
            float countdown = 5.0f;
            string design_name = null;
            string type_name;
            string abrv = null;
            SimElements.QuantumDrive.SUBTYPE subtype = SimElements.QuantumDrive.SUBTYPE.QUANTUM;
            int emcon_1 = -1;
            int emcon_2 = -1;
            int emcon_3 = -1;

            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;
            if (!string.IsNullOrEmpty(val.abrv))
                abrv = val.abrv;
            if (!string.IsNullOrEmpty(val.type))
            {
                type_name = val.type;
                if (type_name.ToLower().Contains("hyper"))
                {
                    subtype = SimElements.QuantumDrive.SUBTYPE.HYPER;
                }
            }
            capacity = val.capacity;
            consumption = val.consumption;
            if (val.loc != null)
            {
                loc = val.loc;
                loc *= (float)sd.scale;
            }
            if (!string.IsNullOrEmpty(val.size))
            {
                size = float.Parse(val.size);
                size *= (float)sd.scale;
            }
            if (!string.IsNullOrEmpty(val.hull_factor))
                hull = float.Parse(val.hull_factor);
            if (!string.IsNullOrEmpty(val.jump_time))
                countdown = float.Parse(val.jump_time);
            if (!string.IsNullOrEmpty(val.countdown))
                countdown = float.Parse(val.countdown);
            emcon_1 = val.emcon_1;
            emcon_2 = val.emcon_2;
            emcon_3 = val.emcon_3;

            SimElements.QuantumDrive drive = new SimElements.QuantumDrive(subtype, capacity, consumption);
            drive.SetSourceIndex(sd.reactors.Count - 1);
            drive.Mount(loc, size, hull);
            drive.SetCountdown(countdown);

            if (!string.IsNullOrEmpty(design_name))
            {
                SystemDesign sdtmp = SystemDesign.Find(design_name);
                if (sdtmp != null)
                    drive.SetDesign(sdtmp);
            }

            if (!string.IsNullOrEmpty(abrv))
                drive.SetAbbreviation(abrv);

            if (emcon_1 >= 0 && emcon_1 <= 100)
                drive.SetEMCONPower(1, emcon_1);

            if (emcon_2 >= 0 && emcon_2 <= 100)
                drive.SetEMCONPower(1, emcon_2);

            if (emcon_3 >= 0 && emcon_3 <= 100)
                drive.SetEMCONPower(1, emcon_3);

            sd.quantum_drive = drive;
        }
        public static void ParseFarcaster(Farcaster val, ShipDesign sd, string filename)
        {
            string design_name = null;
            double capacity = 300e3;
            double consumption = 15e3;  // twenty second recharge
            int napproach = 0;
            Point[] approach = new Point[Farcaster.NUM_APPROACH_PTS];
            Point loc = new Point(0.0f, 0.0f, 0.0f);
            Point start = new Point(0.0f, 0.0f, 0.0f);
            Point end = new Point(0.0f, 0.0f, 0.0f);
            float size = 0.0f;
            float hull = 0.5f;
            int emcon_1 = -1;
            int emcon_2 = -1;
            int emcon_3 = -1;

            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;
            capacity = val.capacity;
            consumption = val.consumption;
            if (val.loc != null)
            {
                loc = val.loc;
                loc *= (float)sd.scale;
            }
            if (!string.IsNullOrEmpty(val.size))
            {
                size = float.Parse(val.size);
                size *= (float)sd.scale;
            }

            if (!string.IsNullOrEmpty(val.hull_factor))
                hull = float.Parse(val.hull_factor);
            if (val.start != null)
            {
                start = val.start;
                start *= (float)sd.scale;
            }
            if (val.end != null)
            {
                end = val.end;
                end *= (float)sd.scale;
            }
            if (val.approach != null && val.approach.Length > 0)
            {
                for (int i = 0; i < val.approach.Length; i++)
                    if (napproach < Farcaster.NUM_APPROACH_PTS)
                    {
                        approach[i] = val.approach[i];
                        approach[i] *= (float)sd.scale;
                    }
                    else
                        ErrLogger.PrintLine("WARNING: farcaster approach point ignored in '{0}' (max={1})",
                                        filename, Farcaster.NUM_APPROACH_PTS);
            }

            emcon_1 = val.emcon_1;
            emcon_2 = val.emcon_2;
            emcon_3 = val.emcon_3;

            SimElements.Farcaster caster = new SimElements.Farcaster(capacity, consumption);
            caster.SetSourceIndex(sd.reactors.Count - 1);
            caster.Mount(loc, size, hull);

            if (!string.IsNullOrEmpty(design_name))
            {
                SystemDesign sdtmp = SystemDesign.Find(design_name);
                if (sdtmp != null)
                    caster.SetDesign(sdtmp);
            }

            caster.SetStartPoint(start);
            caster.SetEndPoint(end);

            for (int i = 0; i < napproach; i++)
                caster.SetApproachPoint(i, approach[i]);

            if (emcon_1 >= 0 && emcon_1 <= 100)
                caster.SetEMCONPower(1, emcon_1);

            if (emcon_2 >= 0 && emcon_2 <= 100)
                caster.SetEMCONPower(1, emcon_2);

            if (emcon_3 >= 0 && emcon_3 <= 100)
                caster.SetEMCONPower(1, emcon_3);

            sd.farcaster = caster;
        }

        public static void ParseThruster(Thruster val, ShipDesign sd, string filename)
        {

            double thrust = 100;

            Point loc = new Point(0.0f, 0.0f, 0.0f);
            float size = 0.0f;
            float hull = 0.5f;
            string design_name = null;
            float tscale = 1.0f;
            int emcon_1 = -1;
            int emcon_2 = -1;
            int emcon_3 = -1;
            SimElements.Drive.SUBTYPE dtype = 0;

            SimElements.Thruster drive = null;

            if (!string.IsNullOrEmpty(val.type))
            {
                string tval = val.type;

                if (tval == "Plasma") dtype = SimElements.Drive.SUBTYPE.PLASMA;
                else if (tval == "Fusion") dtype = SimElements.Drive.SUBTYPE.FUSION;
                else if (tval == "Alien") dtype = SimElements.Drive.SUBTYPE.GREEN;
                else if (tval == "Green") dtype = SimElements.Drive.SUBTYPE.GREEN;
                else if (tval == "Red") dtype = SimElements.Drive.SUBTYPE.RED;
                else if (tval == "Blue") dtype = SimElements.Drive.SUBTYPE.BLUE;
                else if (tval == "Yellow") dtype = SimElements.Drive.SUBTYPE.YELLOW;
                else if (tval == "Stealth") dtype = SimElements.Drive.SUBTYPE.STEALTH;

                else ErrLogger.PrintLine("WARNING: unknown thruster type '{0}' in '{1}", tval, filename);
            }
            thrust = val.thrust;
            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;
            if (val.loc != null)
            {
                loc = val.loc;
                loc *= (float)sd.scale;
            }
            if (!string.IsNullOrEmpty(val.size))
            {
                size = float.Parse(val.size);
                size *= (float)sd.scale;
            }

            if (!string.IsNullOrEmpty(val.hull_factor))
                hull = float.Parse(val.hull_factor);
            if (!string.IsNullOrEmpty(val.scale))
                tscale = float.Parse(val.scale);

            if (drive == null)
                drive = new SimElements.Thruster((int)dtype, thrust, tscale);

            if (val.port_bottom != null)
            {
                Point port = val.port_bottom.loc;
                if (val.port_bottom != null)
                    port *= sd.scale;
                float port_scale = val.port_bottom.scale;
                uint fire = val.port_bottom.fire;
                if (port_scale <= 0)
                    port_scale = tscale;

                drive.AddPort((int)SimElements.Thruster.Constants.BOTTOM, port, fire, port_scale);
            }
            if (val.port_top != null)
            {
                Point port = val.port_top.loc;
                if (val.port_top != null)
                    port *= sd.scale;
                float port_scale = val.port_top.scale;
                uint fire = val.port_top.fire;
                if (port_scale <= 0)
                    port_scale = tscale;

                drive.AddPort((int)SimElements.Thruster.Constants.TOP, port, fire, port_scale);
            }
            if (val.port_left != null)
            {
                Point port = val.port_left.loc;
                if (val.port_left != null)
                    port *= sd.scale;
                float port_scale = val.port_left.scale;
                uint fire = val.port_left.fire;
                if (port_scale <= 0)
                    port_scale = tscale;

                drive.AddPort((int)SimElements.Thruster.Constants.LEFT, port, fire, port_scale);
            }
            if (val.port_left != null)
            {
                Point port = val.port_right.loc;
                if (val.port_right != null)
                    port *= sd.scale;
                float port_scale = val.port_right.scale;
                uint fire = val.port_right.fire;
                if (port_scale <= 0)
                    port_scale = tscale;

                drive.AddPort((int)SimElements.Thruster.Constants.RIGHT, port, fire, port_scale);
            }
            if (val.port_fore != null)
            {
                Point port = val.port_fore.loc;
                if (val.port_fore != null)
                    port *= sd.scale;
                float port_scale = val.port_fore.scale;
                uint fire = val.port_fore.fire;
                if (port_scale <= 0)
                    port_scale = tscale;

                drive.AddPort((int)SimElements.Thruster.Constants.FORE, port, fire, port_scale);
            }
            if (val.port_aft != null)
            {
                Point port = val.port_aft.loc;
                if (val.port_aft != null)
                    port *= sd.scale;
                float port_scale = val.port_aft.scale;
                uint fire = val.port_aft.fire;
                if (port_scale <= 0)
                    port_scale = tscale;

                drive.AddPort((int)SimElements.Thruster.Constants.AFT, port, fire, port_scale);
            }

            emcon_1 = val.emcon_1;
            emcon_2 = val.emcon_2;
            emcon_3 = val.emcon_3;


            if (drive == null)
                drive = new SimElements.Thruster((int)dtype, thrust, tscale);
            drive.SetSourceIndex(sd.reactors.Count - 1);
            drive.Mount(loc, size, hull);

            if (!string.IsNullOrEmpty(design_name))
            {
                SystemDesign sdtmp = SystemDesign.Find(design_name);
                if (sdtmp != null)
                    drive.SetDesign(sdtmp);
            }

            if (emcon_1 >= 0 && emcon_1 <= 100)
                drive.SetEMCONPower(1, emcon_1);

            if (emcon_2 >= 0 && emcon_2 <= 100)
                drive.SetEMCONPower(1, emcon_2);

            if (emcon_3 >= 0 && emcon_3 <= 100)
                drive.SetEMCONPower(1, emcon_3);

            sd.thruster = drive;
        }

        public static void ParseNavlight(Navlight val, ShipDesign sd, string filename)
        {
            string dname = null;
            string dabrv = null;
            string design_name = null;
            int nlights = 0;
            float dscale = 1.0f;
            float period = 10.0f;
            Point[] bloc = new Point[NavLight.MAX_LIGHTS];
            int[] btype = new int[NavLight.MAX_LIGHTS];
            uint[] pattern = new uint[NavLight.MAX_LIGHTS];

            if (!string.IsNullOrEmpty(val.name))
                dname = val.name;
            if (!string.IsNullOrEmpty(val.abrv))
                dabrv = val.abrv;
            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;
            dscale = val.scale;
            period = val.period;
            if (val.lights != null && val.lights.Length > 0)
            {
                foreach (var l in val.lights)
                {
                    Point loc = l.loc;
                    int t = l.type;
                    uint ptn = l.pattern;

                    if (t < 1 || t > 4)
                        t = 1;

                    if (nlights < NavLight.MAX_LIGHTS)
                    {
                        bloc[nlights] = loc * sd.scale;
                        btype[nlights] = t - 1;
                        pattern[nlights] = ptn;
                        nlights++;
                    }
                    else
                    {
                        ErrLogger.PrintLine("WARNING: Too many lights ship '{0}' in '{1]'", sd.name, filename);
                    }
                }
            }
            SimElements.NavLight nav = new SimElements.NavLight(period, dscale);
            if (!string.IsNullOrEmpty(dname)) nav.SetName(dname);
            if (!string.IsNullOrEmpty(dabrv)) nav.SetAbbreviation(dabrv);

            if (!string.IsNullOrEmpty(design_name))
            {
                SystemDesign sdtmp = SystemDesign.Find(design_name);
                if (sdtmp != null)
                    nav.SetDesign(sdtmp);
            }

            for (int i = 0; i < nlights; i++)
                nav.AddBeacon(bloc[i], pattern[i], btype[i]);

            sd.navlights.Add(nav);
        }

        public static void ParseFlightDeck(FlightDeck val, ShipDesign sd, string filename)
        {
            string dname = null;
            string dabrv = null;
            string design_name = null;
            float dscale = 1.0f;
            float az = 0.0f;
            int etype = 0;

            bool launch = false;
            bool recovery = false;
            int nslots = 0;
            int napproach = 0;
            int nrunway = 0;
            DWORD[] filters = new DWORD[10];
            Point[] spots = new Point[10];
            Point[] approach = new Point[FlightDeck.NUM_APPROACH_PTS];
            Point[] runway = new Point[2];
            Point loc = new Point(0, 0, 0);
            Point start = new Point(0, 0, 0);
            Point end = new Point(0, 0, 0);
            Point cam = new Point(0, 0, 0);
            Point box = new Point(0, 0, 0);
            float cycle_time = 0.0f;
            float size = 0.0f;
            float hull = 0.5f;

            float light = 0.0f;

            if (!string.IsNullOrEmpty(val.name))
                dname = val.name;
            if (!string.IsNullOrEmpty(val.abrv))
                dabrv = val.abrv;
            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;
            if (val.start != null)
            {
                start = val.start;
                start *= (float)sd.scale;
            }
            if (val.end != null)
            {
                end = val.end;
                end *= (float)sd.scale;
            }
            if (val.cam != null)
            {
                cam = val.cam;
                cam *= (float)sd.scale;
            }
            if (val.box != null)
            {
                box = val.box;
                box *= (float)sd.scale;
            }
            if (val.approachs != null && val.approachs.Length > 0)
            {
                foreach (var a in val.approachs)
                {
                    if (napproach < FlightDeck.NUM_APPROACH_PTS)
                    {
                        approach[napproach] = a;
                        approach[napproach++] *= (float)sd.scale;
                    }
                    else
                    {
                        ErrLogger.PrintLine("WARNING: flight deck approach point ignored in '{0}' (max={1})\n",
                                            filename, FlightDeck.NUM_APPROACH_PTS);
                    }
                }
            }
            if (val.runways != null && val.runways.Length > 0)
            {
                foreach (var r in val.runways)
                {
                    runway[nrunway] = r;
                    runway[nrunway++] *= (float)sd.scale;
                }
            }
            if (val.spots != null && val.spots.Length > 0)
            {
                foreach (var s in val.spots)
                {
                    spots[nslots] = s.loc;
                    spots[nslots] *= (float)sd.scale;
                    filters[nslots] = s.filter;
                    nslots++;

                    //TODO  if (pdef.term().isArray())
                    //{
                    //    GetDefVec(spots[nslots], pdef, filename);
                    //    spots[nslots] *= (float)scale;
                    //    filters[nslots++] = 0xf;
                    //}
                }
            }
            light = val.light;
            cycle_time = val.cycle_time;
            launch = val.launch;
            recovery = val.recovery;
            az = val.azimuth;
            if (ShipDesign.degrees) az *= (float)ConstantsF.DEGREES;
            if (val.loc != null)
            {
                loc = val.loc;
                loc *= (float)sd.scale;
            }
            size = val.size;
            size *= (float)sd.scale;
            hull = val.hull_factor;
            etype = val.explosion;

            SimElements.FlightDeck deck = new SimElements.FlightDeck();
            deck.Mount(loc, size, hull);
            if (!string.IsNullOrEmpty(dname)) deck.SetName(dname);
            if (!string.IsNullOrEmpty(dabrv)) deck.SetAbbreviation(dabrv);

            if (!string.IsNullOrEmpty(design_name))
            {
                SystemDesign sdtmp = SystemDesign.Find(design_name);
                if (sdtmp != null)
                    deck.SetDesign(sdtmp);
            }

            if (launch)
                deck.SetLaunchDeck();
            else if (recovery)
                deck.SetRecoveryDeck();

            deck.SetAzimuth(az);
            deck.SetBoundingBox(box);
            deck.SetStartPoint(start);
            deck.SetEndPoint(end);
            deck.SetCamLoc(cam);
            deck.SetExplosionType((Graphics.General.Explosion.ExplosionType)etype);

            if (light > 0)
                deck.SetLight(light);

            for (int i = 0; i < napproach; i++)
                deck.SetApproachPoint(i, approach[i]);

            for (int i = 0; i < nrunway; i++)
                deck.SetRunwayPoint(i, runway[i]);

            for (int i = 0; i < nslots; i++)
                deck.AddSlot(spots[i], filters[i]);

            if (cycle_time > 0)
                deck.SetCycleTime(cycle_time);

            sd.flight_decks.Add(deck);
        }
        public static void ParseLandingGear(LandingGear val, ShipDesign sd, string filename)
        {
            string dname = null;
            string dabrv = null;
            string design_name = null;
            int ngear = 0;
            Point[] start = new Point[LandingGear.MAX_GEAR];
            Point[] end = new Point[LandingGear.MAX_GEAR];
            Model[] model = new Model[LandingGear.MAX_GEAR];

            if (!string.IsNullOrEmpty(val.name))
                dname = val.name;
            if (!string.IsNullOrEmpty(val.abrv))
                dabrv = val.abrv;
            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;
            if (val.gears != null && val.gears.Length > 0)
            {
                foreach (var g in val.gears)
                {
                    string mod_name = g.model;
                    Point v1 = g.start;
                    Point v2 = g.end;


                    if (ngear < LandingGear.MAX_GEAR)
                    {
                        Model m = new Model();
                        if (!m.Load(mod_name, sd.scale))
                        {
                            ErrLogger.PrintLine("WARNING: Could not load landing gear model '{0}'", mod_name);
                            //delete m;
                            m = null;
                        }
                        else
                        {
                            model[ngear] = m;
                            start[ngear] = v1 * sd.scale;
                            end[ngear] = v2 * sd.scale;
                            ngear++;
                        }
                    }
                    else
                    {
                        ErrLogger.PrintLine("WARNING: Too many landing gear ship '{0}' in '{1}'", sd.name, filename);
                    }
                }
            }


            sd.gear = new SimElements.LandingGear();
            if (!string.IsNullOrEmpty(dname)) sd.gear.SetName(dname);
            if (!string.IsNullOrEmpty(dabrv)) sd.gear.SetAbbreviation(dabrv);

            if (!string.IsNullOrEmpty(design_name))
            {
                SystemDesign sdtmp = SystemDesign.Find(design_name);
                if (sdtmp != null)
                    sd.gear.SetDesign(sdtmp);
            }

            for (int i = 0; i < ngear; i++)
                sd.gear.AddGear(model[i], start[i], end[i]);
        }
        public static void ParseWeapon(Weapon val, ShipDesign sd, string filename)
        {
            string wtype = null;
            string wname = null;
            string wabrv = null;
            string design_name = null;
            string group_name = null;
            int nmuz = 0;
            Point[] muzzles = new Point[Weapon.MAX_BARRELS];
            Point loc = new Point(0.0f, 0.0f, 0.0f);
            float size = 0.0f;
            float hull = 0.5f;
            float az = 0.0f;
            float el = 0.0f;
            float az_max = 1e6f;
            float az_min = 1e6f;
            float el_max = 1e6f;
            float el_min = 1e6f;
            float az_rest = 1e6f;
            float el_rest = 1e6f;
            int etype = 0;
            int emcon_1 = -1;
            int emcon_2 = -1;
            int emcon_3 = -1;

            if (!string.IsNullOrEmpty(val.type))
                wtype = val.type;
            if (!string.IsNullOrEmpty(val.name))
                wname = val.name;
            if (!string.IsNullOrEmpty(val.abrv))
                wabrv = val.abrv;
            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;
            if (!string.IsNullOrEmpty(val.group))
                group_name = val.design;
            if (val.muzzles != null && val.muzzles.Length > 0)
            {
                foreach (var m in val.muzzles)
                {
                    if (nmuz < Weapon.MAX_BARRELS)
                    {
                        muzzles[nmuz] = m;
                        nmuz++;
                    }
                    else
                    {
                        ErrLogger.PrintLine("WARNING: too many muzzles (max={0}) for weapon in '{1}'", filename, Weapon.MAX_BARRELS);
                    }

                }
            }
            if (val.loc != null)
            {
                loc = val.loc;
                loc *= (float)sd.scale;
            }
            size = val.size;
            size *= (float)sd.scale;
            hull = val.hull_factor;
            az = val.azimuth;
            if (ShipDesign.degrees) az *= (float)ConstantsF.DEGREES;
            el = val.elevation;
            if (ShipDesign.degrees) el *= (float)ConstantsF.DEGREES;

            az_max = val.aim_az_max;
            if (ShipDesign.degrees) az_max *= (float)ConstantsF.DEGREES;
            az_min = 0.0f - az_max;

            el_max = val.aim_el_max;
            if (ShipDesign.degrees) el_max *= (float)ConstantsF.DEGREES;
            el_min = 0.0f - el_max;

            az_min = val.aim_az_min;
            if (ShipDesign.degrees) az_min *= (float)ConstantsF.DEGREES;

            el_min = val.aim_el_min;
            if (ShipDesign.degrees) el_min *= (float)ConstantsF.DEGREES;

            az_rest = val.aim_az_rest;
            if (ShipDesign.degrees) az_rest *= (float)ConstantsF.DEGREES;

            el_rest = val.aim_el_rest;
            if (ShipDesign.degrees) el_rest *= (float)ConstantsF.DEGREES;

            az_rest = val.rest_azimuth;
            if (ShipDesign.degrees) az_rest *= (float)ConstantsF.DEGREES;

            el_rest = val.rest_elevation;
            if (ShipDesign.degrees) el_rest *= (float)ConstantsF.DEGREES;

            etype = val.explosion;
            emcon_1 = val.emcon_1;
            emcon_2 = val.emcon_3;
            emcon_3 = val.emcon_3;


            WeaponDesign meta = WeaponDesign.Find(wtype);
            if (meta == null)
            {
                ErrLogger.PrintLine("WARNING: unusual weapon name '{0}' in '{1}'", wtype, filename);
            }
            else
            {
                // non-turret weapon muzzles are relative to ship scale:
                if (meta.turret_model == null)
                {
                    for (int i = 0; i < nmuz; i++)
                        muzzles[i] *= (float)sd.scale;
                }

                // turret weapon muzzles are relative to weapon scale:
                else
                {
                    for (int i = 0; i < nmuz; i++)
                        muzzles[i] *= (float)meta.scale;
                }

                SimElements.Weapon gun = new SimElements.Weapon(meta, nmuz, muzzles, az, el);
                gun.SetSourceIndex(sd.reactors.Count - 1);
                gun.Mount(loc, size, hull);

                if (az_max < 1e6) gun.SetAzimuthMax(az_max);
                if (az_min < 1e6) gun.SetAzimuthMin(az_min);
                if (az_rest < 1e6) gun.SetRestAzimuth(az_rest);

                if (el_max < 1e6) gun.SetElevationMax(el_max);
                if (el_min < 1e6) gun.SetElevationMin(el_min);
                if (el_rest < 1e6) gun.SetRestElevation(el_rest);

                if (emcon_1 >= 0 && emcon_1 <= 100)
                    gun.SetEMCONPower(1, emcon_1);

                if (emcon_2 >= 0 && emcon_2 <= 100)
                    gun.SetEMCONPower(1, emcon_2);

                if (emcon_3 >= 0 && emcon_3 <= 100)
                    gun.SetEMCONPower(1, emcon_3);

                if (!string.IsNullOrEmpty(wname)) gun.SetName(wname);
                if (!string.IsNullOrEmpty(wabrv)) gun.SetAbbreviation(wabrv);

                if (!string.IsNullOrEmpty(design_name))
                {
                    SystemDesign sdtmp = SystemDesign.Find(design_name);
                    if (sdtmp != null)
                        gun.SetDesign(sdtmp);
                }

                if (!string.IsNullOrEmpty(group_name))
                {
                    gun.SetGroup(group_name);
                }

                gun.SetExplosionType((Graphics.General.Explosion.ExplosionType)etype);

                if (meta.decoy_type != 0 && sd.decoy == null)
                    sd.decoy = gun;
                else if (meta.probe && sd.probe == null)
                    sd.probe = gun;
                else
                    sd.weapons.Add(gun);
            }
        }
        public static void ParseHardPoint(HardPoint[] hardpoints, ShipDesign sd, string filename)
        {
            if (hardpoints == null || hardpoints.Length == 0) return;
            foreach (var val in hardpoints)
            {
                if (val.types == null || val.types.Length == 0) continue;

                string[] wtypes = new string[8];
                string wname;
                string wabrv;
                string design;
                Vector3F muzzle = Vector3F.Zero;
                Point loc = new Point(0.0f, 0.0f, 0.0f);
                float size = 0.0f;
                float hull = 0.5f;
                float az = 0.0f;
                float el = 0.0f;
                //int ntypes = 0;
                int emcon_1 = -1;
                int emcon_2 = -1;
                int emcon_3 = -1;


                for (int ntypes = 0; ntypes < val.types.Length; ntypes++)
                {
                    wtypes[ntypes] = val.types[ntypes];
                }
                wname = val.name;
                wabrv = val.abrv;
                design = val.design;

                if (val.muzzle != null)
                {
                    muzzle = val.muzzle * (float)sd.scale;
                }
                if (val.loc != null)
                {
                    loc = val.loc * (float)sd.scale;
                }
                size = val.size * (float)sd.scale;
                hull = val.hull_factor;
                az = val.azimuth;
                if (ShipDesign.degrees) az *= (float)ConstantsF.DEGREES;
                el = val.elevation;
                if (ShipDesign.degrees) el *= (float)ConstantsF.DEGREES;
                emcon_1 = val.emcon_1;
                emcon_2 = val.emcon_2;
                emcon_3 = val.emcon_3;


                SimElements.HardPoint hp = new SimElements.HardPoint(muzzle, az, el);
                if (hp != null)
                {
                    for (int i = 0; i < val.types.Length; i++)
                    {
                        if (string.IsNullOrEmpty(wtypes[i])) continue;
                        WeaponDesign meta = WeaponDesign.Find(wtypes[i]);
                        if (meta == null)
                        {
                            ErrLogger.PrintLine("WARNING: unusual weapon name '{0}' in '{1}'\n", wtypes[i], filename);
                        }
                        else
                        {
                            hp.AddDesign(meta);
                        }
                    }

                    hp.Mount(loc, size, hull);
                    if (!string.IsNullOrEmpty(wname)) hp.SetName(wname);
                    if (!string.IsNullOrEmpty(wabrv)) hp.SetAbbreviation(wabrv);
                    if (!string.IsNullOrEmpty(design)) hp.SetDesign(design);

                    sd.hard_points.Add(hp);
                }
            }
        }
        public static void ParseLoadout(Loadout[] lodaouts, ShipDesign sd, string filename)
        {
            foreach (var val in lodaouts)
            {
                if (val.name == null && val.stations == null) continue;
                ShipLoad load = new ShipLoad();

                if (load == null) return;

                if (!string.IsNullOrEmpty(val.name))
                    load.name = val.name;
                if (val.stations != null && val.stations.Length > 0)
                    load.load = val.stations;

                sd.loadouts.Add(load);
            }
        }

        public static void ParseSensor(Sensor val, ShipDesign sd, string filename)
        {
            string design_name = null;
            Point loc = new Point(0.0f, 0.0f, 0.0f);
            float size = 0.0f;
            float hull = 0.5f;
            int nranges = 0;
            float[] ranges = new float[8];
            int emcon_1 = -1;
            int emcon_2 = -1;
            int emcon_3 = -1;

            //ZeroMemory(ranges, sizeof(ranges));
            if (val.ranges != null && val.ranges.Length > 0)
            {
                foreach (var r in val.ranges)
                {
                    ranges[nranges++] = r;
                }
            }
            if (val.loc != null)
            {
                loc = val.loc;
                loc *= (float)sd.scale;
            }
            size = val.size;
            size *= (float)sd.scale;
            hull = val.hull_factor;
            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;

            emcon_1 = val.emcon_1;
            emcon_2 = val.emcon_3;
            emcon_3 = val.emcon_3;

            if (sd.sensor == null)
            {
                sd.sensor = new SimElements.Sensor();

                if (!string.IsNullOrEmpty(design_name))
                {
                    SystemDesign sdtmp = SystemDesign.Find(design_name);
                    if (sdtmp != null)
                        sd.sensor.SetDesign(sdtmp);
                }

                for (int i = 0; i < nranges; i++)
                    sd.sensor.AddRange(ranges[i]);

                if (emcon_1 >= 0 && emcon_1 <= 100)
                    sd.sensor.SetEMCONPower(1, emcon_1);

                if (emcon_2 >= 0 && emcon_2 <= 100)
                    sd.sensor.SetEMCONPower(1, emcon_2);

                if (emcon_3 >= 0 && emcon_3 <= 100)
                    sd.sensor.SetEMCONPower(1, emcon_3);

                sd.sensor.Mount(loc, size, hull);
                sd.sensor.SetSourceIndex(sd.reactors.Count - 1);
            }
            else
            {
                ErrLogger.PrintLine("WARNING: additional sensor ignored in '{0}' ", filename);
            }
        }
        public static void ParseNavsys(NavSystem val, ShipDesign sd, string filename)
        {
            string design_name = null;
            Point loc = new Point(0.0f, 0.0f, 0.0f);
            float size = 0.0f;
            float hull = 0.5f;

            if (val.loc != null)
            {
                loc = val.loc;
                loc *= (float)sd.scale;
            }
            size = val.size;
            size *= (float)sd.scale;
            hull = val.hull_factor;
            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;

            if (sd.navsys == null)
            {
                sd.navsys = new SimElements.NavSystem();

                if (!string.IsNullOrEmpty(design_name))
                {
                    SystemDesign sdtmp = SystemDesign.Find(design_name);
                    if (sdtmp != null)
                        sd.navsys.SetDesign(sdtmp);
                }

                sd.navsys.Mount(loc, size, hull);
                sd.navsys.SetSourceIndex(sd.reactors.Count - 1);
            }
            else
            {
                ErrLogger.PrintLine("WARNING: additional nav system ignored in '{0}'", filename);
            }
        }
        public static void ParseComputer(Computer val, ShipDesign sd, string filename)
        {
            string comp_name = "Computer";
            string comp_abrv = "Comp";
            string design_name = null;
            int comp_type = 1;
            Point loc = new Point(0.0f, 0.0f, 0.0f);
            float size = 0.0f;
            float hull = 0.5f;

            if (!string.IsNullOrEmpty(val.name))
                comp_name = val.name;

            if (!string.IsNullOrEmpty(val.abrv))
                comp_abrv = val.abrv;

            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;
            comp_type = val.type;
            if (val.loc != null)
            {
                loc = val.loc;
                loc *= (float)sd.scale;
            }
            size = val.size;
            size *= (float)sd.scale;
            hull = val.hull_factor;

            SimElements.Computer comp = new SimElements.Computer(comp_type, comp_name);
            comp.Mount(loc, size, hull);
            comp.SetAbbreviation(comp_abrv);
            comp.SetSourceIndex(sd.reactors.Count - 1);

            if (!string.IsNullOrEmpty(design_name))
            {
                SystemDesign sdtmp = SystemDesign.Find(design_name);
                if (sdtmp != null)
                    comp.SetDesign(sdtmp);
            }

            sd.computers.Add(comp);
        }
        public static void ParseShield(Shield val, ShipDesign sd, string filename)
        {
            string dname = null;
            string dabrv = null;
            string design_name = null;
            string model_name = null;
            double factor = 0;
            double capacity = 0;
            double consumption = 0;
            double cutoff = 0;
            double curve = 0;
            double def_cost = 1;
            int shield_type = 0;
            Point loc = new Point(0.0f, 0.0f, 0.0f);
            float size = 0.0f;
            float hull = 0.5f;
            int etype = 0;
            bool shield_capacitor = false;
            bool shield_bubble = false;
            int emcon_1 = -1;
            int emcon_2 = -1;
            int emcon_3 = -1;

            shield_type = val.type;

            if (!string.IsNullOrEmpty(val.name))
                dname = val.name;
            if (!string.IsNullOrEmpty(val.abrv))
                dabrv = val.abrv;
            if (!string.IsNullOrEmpty(val.design))
                design_name = val.design;

            if (!string.IsNullOrEmpty(val.model))
                model_name = val.model;
            if (val.loc != null)
            {
                loc = val.loc;
                loc *= (float)sd.scale;
            }
            size = val.size;
            size *= (float)sd.scale;
            hull = val.hull_factor;
            factor = val.factor;
            cutoff = val.cutoff;
            curve = val.curve;
            shield_capacitor = val.capacitor;
            shield_bubble = val.bubble;
            capacity = val.capacity;
            consumption = val.consumption;
            def_cost = val.deflection_cost;
            etype = val.explosion;

            emcon_1 = val.emcon_1;
            emcon_2 = val.emcon_3;
            emcon_3 = val.emcon_3;

            if (!string.IsNullOrEmpty(val.bolt_hit_sound))
                sd.bolt_hit_sound = val.bolt_hit_sound;
            if (!string.IsNullOrEmpty(val.beam_hit_sound))
                sd.beam_hit_sound = val.beam_hit_sound;


            if (sd.shield == null)
            {
                if (shield_type != 0)
                {
                    sd.shield = new SimElements.Shield((SimElements.Shield.SUBTYPE)shield_type);
                    sd.shield.SetSourceIndex(sd.reactors.Count - 1);
                    sd.shield.Mount(loc, size, hull);
                    if (!string.IsNullOrEmpty(dname)) sd.shield.SetName(dname);
                    if (!string.IsNullOrEmpty(dabrv)) sd.shield.SetAbbreviation(dabrv);

                    if (!string.IsNullOrEmpty(design_name))
                    {
                        SystemDesign sdtmp = SystemDesign.Find(design_name);
                        if (sdtmp != null)
                            sd.shield.SetDesign(sdtmp);
                    }

                    sd.shield.SetExplosionType((Graphics.General.Explosion.ExplosionType)etype);
                    sd.shield.SetShieldCapacitor(shield_capacitor);
                    sd.shield.SetShieldBubble(shield_bubble);

                    if (factor > 0) sd.shield.SetShieldFactor(factor);
                    if (capacity > 0) sd.shield.SetCapacity(capacity);
                    if (cutoff > 0) sd.shield.SetShieldCutoff(cutoff);
                    if (consumption > 0) sd.shield.SetConsumption(consumption);
                    if (def_cost > 0) sd.shield.SetDeflectionCost(def_cost);
                    if (curve > 0) sd.shield.SetShieldCurve(curve);

                    if (emcon_1 >= 0 && emcon_1 <= 100)
                        sd.shield.SetEMCONPower(1, emcon_1);

                    if (emcon_2 >= 0 && emcon_2 <= 100)
                        sd.shield.SetEMCONPower(1, emcon_2);

                    if (emcon_3 >= 0 && emcon_3 <= 100)
                        sd.shield.SetEMCONPower(1, emcon_3);

                    if (!string.IsNullOrEmpty(model_name))
                    {
                        sd.shield_model = new Model();
                        if (!sd.shield_model.Load(model_name, sd.scale))
                        {
                            ErrLogger.PrintLine("ERROR: Could not load shield model '{0}'\n", model_name);
                            //delete shield_model;
                            sd.shield_model = null;
                            sd.valid = false;
                        }
                        else
                        {
                            sd.shield_model.SetDynamic(true);
                            sd.shield_model.SetLuminous(true);
                        }
                    }

                    Sound.FlagEnum SOUND_FLAGS = Sound.FlagEnum.LOCALIZED | Sound.FlagEnum.LOC_3D;

                    if (!string.IsNullOrEmpty(sd.bolt_hit_sound))
                    {
#if TODO
                        if (!loader.LoadSound(bolt_hit_sound, bolt_hit_sound_resource, SOUND_FLAGS, true))
                        {
                            loader.SetDataPath("Sounds/");
                            loader.LoadSound(sd.bolt_hit_sound, bolt_hit_sound_resource, SOUND_FLAGS);
                            loader.SetDataPath(path_name);
                        }
#endif
                    }

                    if (!string.IsNullOrEmpty(sd.beam_hit_sound))
                    {
#if TODO

                        if (!loader.LoadSound(beam_hit_sound, beam_hit_sound_resource, SOUND_FLAGS, true))
                        {
                            loader.SetDataPath("Sounds/");
                            loader.LoadSound(beam_hit_sound, beam_hit_sound_resource, SOUND_FLAGS);
                            loader.SetDataPath(path_name);
                        }
#endif
                    }
                }
                else
                {
                    ErrLogger.PrintLine("WARNING: invalid shield type in '{0}'\n", filename);
                }
            }
            else
            {
                ErrLogger.PrintLine("WARNING: additional shield ignored in '{0}'\n", filename);
            }
        }
        public static void ParseDeathSpiral(DeathSpiral val, ShipDesign sd, string filename)
        {
            int exp_index = -1;
            int debris_index = -1;
            int fire_index = -1;

            sd.death_spiral_time = val.time;
            if (val.explosion != null && val.explosion.Length > 0)
            {
                foreach (var explosion in val.explosion)
                    ParseExplosion(explosion, ++exp_index, sd, filename);
            }
            if (val.debris != null && val.debris.Length > 0)
            {
                foreach (var debris in val.debris)
                {
                    ParseDebris(debris, ++debris_index, sd, filename);
                }
            }
        }

        private static void ParseExplosion(Explosion val, int index, ShipDesign sd, string filename)
        {
            if (sd.explosion[index] == null) sd.explosion[index] = new ShipExplosion();

            ShipExplosion exp = sd.explosion[index];

            exp.time = val.time;
            exp.type = (Graphics.General.Explosion.ExplosionType)val.type;
            if (val.loc != null)
            {
                exp.loc = val.loc;
                exp.loc *= (float)sd.scale;
            }
            exp.final = val.final;
        }
        private static void ParseDebris(Debris val, int index, ShipDesign sd, string filename)
        {
            string model_name;
            int fire_index = 0;

            if (sd.debris[index] == null)
                sd.debris[index] = new ShipDebris();
            ShipDebris deb = sd.debris[index];

            if (!string.IsNullOrEmpty(val.model))
            {
                model_name = val.model;
                Model model = new Model();
                if (!model.Load(model_name, sd.scale))
                {
                    ErrLogger.PrintLine("Could not load debris model '{0}'", model_name);
                    //delete model;
                    return;
                }

                ShipDesign.PrepareModel(model);
                deb.model = model;
            }
            deb.mass = val.mass;
            deb.speed = val.speed;
            deb.drag = val.drag;
            deb.drag = val.drag;
            if (val.loc != null)
            {
                deb.loc = val.loc;
                deb.loc *= (float)sd.scale;
            }
            deb.count = val.count;
            deb.life = val.life;
            if (val.fire != null && val.fire.Length > 0)
                foreach (var f in val.fire)
                {
                    if (fire_index < 5)
                    {
                        deb.fire_loc[fire_index] = f;
                        deb.fire_loc[fire_index] *= (float)sd.scale;
                        fire_index++;
                    }
                }
            deb.fire_type = val.fire_type;
        }

        public static void ParseMap(Map val, ShipDesign sd, string filename)
        {
            if (!string.IsNullOrEmpty(val.sprite))
            {
                Bitmap sprite = new Bitmap(val.sprite, Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
                sd.map_sprites.Add(sprite);
            }
        }
        public static void ParseSquadron(Squadron val, ShipDesign sd, string filename)
        {
            string name = "";
            string design = "";
            int count = 4;
            int avail = 4;

            if (!string.IsNullOrEmpty(val.name))
                name = val.name;
            if (!string.IsNullOrEmpty(val.design))
                design = val.design;
            count = val.count;
            avail = val.avail;


            SimElements.ShipSquadron s = new SimElements.ShipSquadron();
            s.name = name;

            s.design = ShipDesign.Get(design);
            s.count = count;
            s.avail = avail;

            sd.squadrons.Add(s);
        }
        public static void ParseSkin(Skin val, ShipDesign sd, string filename)
        { }
    }

    [Serializable]
    public class Ship
    {
        public string @class;
        public string name;

        public string cockpit_model;
        public string detail_0; // "model",  "detail_0"
        public string detail_1;
        public string detail_2;
        public string detail_3;

        public Point spin; //TODO Review if spin should be a list o array

        public Point offset_0;
        public Point offset_1;
        public Point offset_2;
        public Point offset_3;

        public Point beauty;
        public string hud_icon;
        public string feature_0;
        public string feature_1;
        public string feature_2;
        public string feature_3;


        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string description;

        public string display_name;

        //[JsonProperty(Required = Required.Always)]
        public string abrv;

        public string pcs;
        public string acs;
        public string detet;
        public string scale;
        public string explosion_scale;
        public string mass;
        public string vlimit;
        public string agility;
        public string air_factor;
        public string roll_rate;
        public string pitch_rate;
        public string yaw_rate;
        public string integrity;
        public string drag;
        public string arcade_drag;
        public string roll_drag;
        public string pitch_drag;
        public string yaw_drag;
        public string trans_x;
        public string trans_y;
        public string trans_z;
        public string turn_bank;
        public string cockpit_scale;
        public string auto_roll;

        public string CL;
        public string CD;
        public string stall;

        public string prep_time;
        public string avoid_time;
        public string avoid_fighter;
        public string avoid_strike;
        public string avoid_target;
        public string commit_range;

        public string splash_radius;
        public string scuttle;
        public string repair_speed;
        public string repair_teams;
        public bool secret;
        public bool repair_auto;
        public bool repair_screen;
        public bool wep_screen;
        public bool degrees;

        public string emcon_1;
        public string emcon_2;
        public string emcon_3;

        public Point chase;
        public Point bridge;

        public Power power;
        public Drive drive;
        public QuantumDrive quantum_drive;
        public Farcaster farcaster;
        public Thruster thruster;
        public Navlight navlight;
        public FlightDeck flightdeck;
        public LandingGear gear;
        public Weapon weapon;
        public HardPoint[] hardpoint;
        public Loadout[] loadout;
        public Weapon decoy;
        public Weapon probe;
        public Sensor sensor;
        public NavSystem nav;
        public Computer computer;
        public Shield shield;
        public DeathSpiral death_spiral;
        public Map map;
        public Squadron squadron;
        public Skin skin;

        [Serializable]
        public enum ClassType : uint
        {
            Drone = 0x0001,
            Fighter = 0x0002,
            Attack = 0x0004,
            LCA = 0x0008,
            Courier = 0x0010,
            Cargo = 0x0020,
            Corvette = 0x0040,
            Freighter = 0x0080,

            Frigate = 0x0100,
            Destroyer = 0x0200,
            Cruiser = 0x0400,
            Battleship = 0x0800,
            Carrier = 0x1000,
            Dreadnaught = 0x2000,

            Station = 0x4000,
            Farcaster = 0x8000,

            Mine = 0x00010000,
            DEFSAT = 0x00020000,
            COMSAT = 0x00040000,
            SWACS = 0x00080000,

            Building = 0x00100000,
            Factory = 0x00200000,
            SAM = 0x00400000,
            EWR = 0x00800000,
            C3I = 0x01000000,
            Starbase = 0x02000000,

            Dropships = 0x0000000f,
            Starships = 0x0000fff0,
            Space_units = 0x000f0000,
            Ground_units = 0xfff00000
        }
    }

    [Serializable]
    public class Power
    {
        // always require a string value for Power Type
        //[JsonProperty(Required = Required.Always)]
        //[EnumDataType(typeof(PowerType))]
        public string type;

        //[Required]
        public string name;

        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string abrv;
        public string design;

        //[Required]
        public string max_output = "1000.0";
        public string fuel_range = "0.0";
        public Point loc;
        public string size = "0.0";
        public string hull_factor = "0.5";
        public int explosion = 0;

        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_1 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_2 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_3 = -1;
        public enum PowerType
        {
            Battery = 'B',
            Aux = 'A',
            Fusion = 'F'
        }
    }

    [Serializable]
    public class Drive
    {
        //[Required]
        //[EnumDataType(typeof(DriveType))]
        public string type;
        //[Required]
        public string name;
        public string abrv;
        public string design;
        public float thrust = 1.0f;
        public float augmenter = 0.0f;
        public float scale = 1.0f;
        public Point[] port;
        public Point loc;
        public float size = 0.0f;
        public float hull_factor = 0.5f;
        public int explosion;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_1 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_2 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_3 = -1;
        public bool trail = true;
    }

    [Serializable]
    public class QuantumDrive
    {
        public string design;
        public string abrv;
        //[EnumDataType(typeof(QuantumDriveType))]
        public string type;
        public double capacity = 250e3;
        public double consumption = 1e3;
        public Point loc;
        public string size;
        public string hull_factor;
        public string jump_time;
        public string countdown;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_1 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_2 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_3 = -1;
        public enum QuantumDriveType
        {
            Quantum,
            Hyper
        };

    }

    [Serializable]
    public class Farcaster
    {
        public const int NUM_APPROACH_PTS = 8;

        public string design;
        public double capacity = 300e3;
        public double consumption = 15e3;  // twenty second recharge
        public Point loc;
        public string size;
        public string hull_factor;
        public Point start;
        public Point end;
        //[MaxLength(NUM_APPROACH_PTS)]
        public Point[] approach;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_1 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_2 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_3 = -1;
    }

    [Serializable]
    public class Port
    {
        public Point loc;
        public uint fire = 0;
        public float scale = 0.0f;

    }

    [Serializable]
    public class Thruster
    {
        public string type;
        public double thrust = 100;
        public string design;
        public Point loc;
        public string size;
        public string hull_factor;
        public string scale;
        public Port port_bottom;
        public Port port_top;
        public Port port_left;
        public Port port_right;
        public Port port_fore;
        public Port port_aft;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_1 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_2 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_3 = -1;
    }

    [Serializable]
    public class Navlight
    {
        public const int MAX_LIGHTS = 8;
        public string name;
        public string abrv;
        public string design;
        public float scale = 1.0f;
        public float period = 10.0f;
        //[MaxLength(MAX_LIGHTS)]
        public Light[] lights;
    }

    [Serializable]
    public class Light
    {
        public int type;
        public Point loc;
        public uint pattern;
    }

    [Serializable]
    public class FlightDeck
    {
        public const int NUM_APPROACH_PTS = 8;
        public string name;
        public string abrv;
        public string design;
        public Point start;
        public Point end;
        public Point cam;
        public Point box;

        //[MaxLength(NUM_APPROACH_PTS)]
        public Point[] approachs;
        //[MaxLength(2)]
        public Point[] runways;
        //[MaxLength(10)]
        public Spot[] spots;

        public float light;

        public float cycle_time = 0.0f;
        public bool launch = false;
        public bool recovery = false;
        public float azimuth = 0.0f;
        public Point loc;

        public float size = 0.0f;
        public float hull_factor = 0.5f;
        public int explosion = 0;

    }
    [Serializable]
    public class Spot
    {
        public Point loc;
        public DWORD filter;
    }
    [Serializable]
    public class LandingGear
    {
        public const int MAX_GEAR = 4;
        public string name;
        public string abrv;
        public string design;
        //[MaxLength(MAX_GEAR)]
        public Gear[] gears;
    }

    [Serializable]
    public class Gear
    {
        public string model;
        public Point start;
        public Point end;
    }

    [Serializable]
    public class Weapon
    {
        public const int MAX_BARRELS = 8;
        public string type;
        public string name;
        public string abrv;
        public string design;
        public string group;
        //[MaxLength(MAX_BARRELS)]
        public Point[] muzzles;
        public Point loc;
        public float size = 0.0f;
        public float hull_factor = 0.5f;
        public float azimuth = 0.0f;
        public float elevation = 0.0f;
        public float aim_az_max = 1e6f;
        public float aim_az_min = 1e6f;
        public float aim_el_max = 1e6f;
        public float aim_el_min = 1e6f;
        public float aim_az_rest = 1e6f;
        public float aim_el_rest = 1e6f;
        public float rest_azimuth = 1e6f;
        public float rest_elevation = 1e6f;
        public int explosion = 0;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_1 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_2 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_3 = -1;
    }

    [Serializable]
    public class HardPoint
    {
        //[MaxLength(8)]
        public string[] types;
        public string name;
        public string abrv;
        public string design;
        public Vector3F muzzle;
        public Vector3F loc;
        public float size = 0.0f;
        public float hull_factor = 0.5f;
        public float azimuth = 0.0f;
        public float elevation = 0.0f;
        public int ntypes = 0;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_1 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_2 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_3 = -1;
    }

    [Serializable]
    public class Loadout
    {
        //[Required]
        public string name;
        //[Required]
        //[MaxLength(16)]
        public int[] stations;
    }

    [Serializable]
    public class Sensor
    {
        //[Required]
        public Point loc;
        public float size = 0.0f;
        public float hull_factor = 0.5f;
        //[MaxLength(8)]
        public float[] ranges;
        public string design;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_1 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_2 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_3 = -1;
    }

    [Serializable]
    public class NavSystem
    {
        public string design;
        public Point loc;
        public float size = 0.0f;
        public float hull_factor = 0.5f;
    }

    [Serializable]
    public class Computer
    {
        //[Required]
        public string name;
        public string abrv;
        public string design;
        public int type;
        public Point loc;
        public float size = 0.0f;
        public float hull_factor = 0.5f;
    }

    [Serializable]
    public class Shield
    {
        public int type;
        //[Required]
        public string name;
        public string abrv;
        public string design;
        public string model;
        public Point loc;
        public float size = 0.0f;
        public float hull_factor = 0.5f;
        public double factor = 0;
        public double cutoff = 0;
        public double curve = 0;
        public bool capacitor = false;
        public bool bubble = false;
        public double capacity = 0;
        public double consumption = 0;
        public double deflection_cost = 1;
        public int explosion = 0;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_1 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_2 = -1;
        //[Range(0, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int emcon_3 = -1;
        public string bolt_hit_sound;
        public string beam_hit_sound;
    }

    [Serializable]
    public class DeathSpiral
    {
        public float time;
        //[MaxLength(10)]
        public Explosion[] explosion;
        public Debris[] debris;
        public float debris_mass;
        public float debris_speed;
        public float debris_drag;
        public Vector3 debris_loc;
        public int debris_count;
        public int debris_life;
        public Vector3 debris_fire;
        public int debris_fire_type;
    }

    [Serializable]
    public class Explosion
    {
        public float time;
        public int type;
        public Point loc;
        public bool final = false;
    }

    [Serializable]
    public class Debris
    {
        public string model;
        public float mass;
        public float speed;
        public float drag;
        public Vector3F loc;
        public int count;
        public int life;
        //[MaxLength(5)]
        public Vector3F[] fire;
        public int fire_type;
    }

    [Serializable]
    public class Map
    {
        public string sprite;
    }

    [Serializable]
    public class Squadron
    {
        public string name;
        public string design;
        public int count;
        public int avail;
    }

    [Serializable]
    public class Skin
    {
        public string name;
        public SkinMtl material;
    }

    [Serializable]
    public class SkinMtl
    {
        public string name;
        public Color Ka;
        public Color Kd;
        public Color Ks;
        public Color Ke;
        public float power;
        public float bump;
        public bool luminous;
        public uint blend;
        public string tex_d;
        public string tex_s;
        public string tex_b;
        public string tex_e;

        public enum BlendTypes
        {
            translucent,
            additive,
            solid
        }
    }
}