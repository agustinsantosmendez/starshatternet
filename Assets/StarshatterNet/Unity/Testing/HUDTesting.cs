﻿using System;
using StarshatterNet.Config;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.StarSystems;
using StarshatterNet.Stars.Views;
using UnityEngine;
using UnityEngine.UI;
using Screen = StarshatterNet.Gen.Graphics.Screen;

public class HUDTesting : MonoBehaviour
{
    public Scene scene;

    private Camera cam;
    private HUDView hud;
    private Canvas canvas;

    // Use this for initialization
    void Start()
    {
        // Display
        Video video = Video.GetVideo();
        // Canvas
        Screen screen = new Screen(video);
        CreateCanvas();
        // Rect
        Window win = new Window(screen, canvas, 0, 0, screen.Width(), screen.Height());


        scene = new Scene();
        scene.SetAmbient(new Color32(60, 60, 60, 255));

        CameraNode camera = new CameraNode();
        scene.AddCamera(camera.gameObj);
        cam = camera.gameObj.GetComponent<Camera>();

        MFD.Initialize();
        hud = new HUDView(win);

    }

    // Update is called once per frame
    void Update()
    {

    }

    protected void CreateCanvas()
    {
        GameObject g = new GameObject();
        g.name = "HUDCanvas";
        canvas = g.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        CanvasScaler cs = g.AddComponent<CanvasScaler>();
        cs.scaleFactor = 1.0f;
        cs.dynamicPixelsPerUnit = 100f;
        GraphicRaycaster gr = g.AddComponent<GraphicRaycaster>();
    }
}