﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      QuitView.h/QuitView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Non-rendering view class that controls the fade level (fade-in/fade-out)
*/
using System;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars;
using UnityEngine;

namespace StarshatterNet.Gen.Views
{
    public class FadeView : View
    {
        public enum FadeState { StateStart, State2, StateIn, StateHold, StateOut, StateDone };
         
        public FadeView(Window c, string name, double fadein = 1, double fadeout = 1, double holdtime = 4) :
            base(c, name)
        {
            fade_in = fadein * 1000;
            fade_out = fadeout * 1000;
            hold_time = holdtime * 1000;
            step_time = 0;
            fast = 1;
            time = 0;

            state = FadeState.StateStart;
        }
        //public virtual ~FadeView();

        // Operations:
        public override void Refresh()
        {
            double msec = 0;

            if (state == FadeState.StateStart)
            {
                time = Game.RealTime();
            }
            else if (state != FadeState.StateDone)
            {
                double new_time = Game.RealTime();
                msec = new_time - time;
                time = new_time;
            }

            switch (state)
            {
                case FadeState.StateStart:
                    if (fade_in != 0)
                    {
                        //Print("  *  FadeView: %f, %f, %f\n", fade_in, fade_out, hold_time);
                        ColorExtensions.SetFade(0);
                        //Print("  1. FadeView SetFade to   0  (%6.1f)\n", time);
                    }

                    step_time = 0;
                    state = FadeState.State2;
                    break;

                case FadeState.State2:
                    if (fade_in != 0)
                    {
                        ColorExtensions.SetFade(0);
                        //Print("  1. FadeView SetFade to   0  (%6.1f)\n", time);
                    }

                    step_time = 0;
                    state = FadeState.StateIn;
                    break;

                case FadeState.StateIn:
                    if (step_time < fade_in)
                    {
                        double fade = step_time / fade_in;
                        ColorExtensions.SetFade(fade);
                        //Print("  2. FadeView SetFade to %3d  (%6.1f) %6.1f\n", (int) (fade * 100), time, step_time);
                        step_time += msec;
                    }
                    else
                    {
                        ColorExtensions.SetFade(1);
                        //Print("  2. FadeView SetFade to %3d  (%6.1f) %6.1f => HOLDING\n", 100, time, step_time);
                        step_time = 0;
                        state = FadeState.StateHold;
                    }
                    break;

                case FadeState.StateHold:
                    if (step_time < hold_time)
                    {
                        step_time += msec;
                        //Print("  3. FadeView holding at %3d  (%6.1f) %6.1f\n", 100, time, step_time);
                    }
                    else
                    {
                        //Print("  3. FadeView HOLD COMPLETE   (%6.1f) %6.1f\n", time, step_time);
                        step_time = 0;
                        state = FadeState.StateOut;
                    }
                    break;

                case FadeState.StateOut:
                    if (fade_out > 0)
                    {
                        if (step_time < fade_out)
                        {
                            double fade = 1 - step_time / fade_out;
                            ColorExtensions.SetFade(fade);
                            //Print("  4. FadeView SetFade to %3d  (%6.1f) %6.1f\n", (int) (fade*100), time, step_time);
                            step_time += msec;
                        }
                        else
                        {
                            ColorExtensions.SetFade(0);
                            //Print("  4. FadeView SetFade to %3d  (%6.1f) %6.1f\n", 0, time, step_time);
                            step_time = 0;
                            state = FadeState.StateDone;
                        }
                    }
                    else
                    {
                        ColorExtensions.SetFade(1);
                        //Print("  4. FadeView SetFade to %3d  (%6.1f) %6.1f\n", 0, time, step_time);
                        step_time = 0;
                        state = FadeState.StateDone;
                    }
                    break;

                default:
                case FadeState.StateDone:
                    //Print("  5. FadeView done  (%6.1f) %6.1f\n", time, step_time);
                    break;
            }
        }
        public virtual bool Done() { return state == FadeState.StateDone; }
        public virtual bool Holding() { return state == FadeState.StateHold; }

        // Control:
        public virtual void FastFade(int fade_fast) { fast = fade_fast; }
        public virtual void FadeIn(double fadein) { fade_in = fadein * 1000; }
        public virtual void FadeOut(double fadeout) { fade_out = fadeout * 1000; }
        public virtual void StopHold()
        {
            //ErrLogger.PrintLine("  FadeView.StopHold()");
            hold_time = 0;
        }



        protected double fade_in;
        protected double fade_out;
        protected double hold_time;
        protected double time;
        protected double step_time;

        protected int fast;
        protected FadeState state;
    }
}