﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmpLoadDlg.h/CmpLoadDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Campaign title card and load progress dialog
*/
using System;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using DWORD = System.UInt32;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmpLoadDlg : FormWindow
    {
        public CmpLoadDlg(Screen s, Camera c, FormDef def, GameObject p) :
                  base(  s, 0, 0, s.Width(), s.Height(), c, p, "CmpLoadDlg")
        {
            lbl_progress = null; lbl_activity = null; lbl_title = null; img_title = null; show_time = 0;

            Init(def);
        }
        //public virtual ~CmpLoadDlg();
        public override void RegisterControls()
        {
            img_title = (ImageBox)FindControl(100);
            lbl_title = FindControl(200);
            lbl_activity = FindControl(101);
            lbl_progress = (Slider)FindControl(102);
        }

        public override void Show()
        {
            base.Show();

            Campaign campaign = Campaign.GetCampaign();

            if (campaign != null)
            {
                Texture2D bmp = campaign.GetImage(3);
                if (img_title != null && bmp != null)
                {
                    Rect tgt_rect = new Rect();
                    tgt_rect.w = img_title.Width();
                    tgt_rect.h = img_title.Height();

                    //TODO img_title.SetTargetRect(tgt_rect);
                    img_title.SetPicture(bmp);
                }

                if (lbl_title != null)
                    lbl_title.SetText(campaign.Name());
            }

            show_time = Game.RealTime();
        }

        public virtual void ExecFrame()
        {
            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                if (lbl_activity != null) lbl_activity.SetText(stars.GetLoadActivity());
                if (lbl_progress != null) lbl_progress.SetValue(stars.GetLoadProgress());
            }
        }

        public override void MoveTo(Rect r)
        {
            base.MoveTo(r);

            Campaign campaign = Campaign.GetCampaign();

            if (campaign != null && img_title != null && campaign.GetImage(3))
            {
                Texture2D bmp = campaign.GetImage(3);

                Rect tgt_rect = new Rect();
                tgt_rect.w = img_title.Width();
                tgt_rect.h = img_title.Height();

                //TODO img_title.SetTargetRect(tgt_rect);
                img_title.SetPicture(bmp);
            }
        }


        public virtual bool IsDone()
        {
            if (Game.RealTime() - show_time < 5000)
                return false;

            return true;
        }



        protected ActiveWindow lbl_activity;
        protected Slider lbl_progress;
        protected ActiveWindow lbl_title;
        protected ImageBox img_title;
        protected ulong show_time;

    }
}