﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      ExitDlg.h/ExitDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Navigation Active Window class
*/
using System;
using StarshatterNet.Audio;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class ExitDlg : FormWindow
    {
        public ExitDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
                base(s, 0, 0, s.Width(), s.Height(), c, p, "ExitDlg")
        {
            manager = mgr; exit_latch = false;
            credits = null; apply = null; cancel = null;
            def_rect = def.GetRect();

            Init(def);
        }

        //public virtual ~ExitDlg();

        public override void RegisterControls()
        {
            if (apply != null)
                return;

            credits = (RichTextBox)FindControl(201);

            apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, apply, OnApply);

            cancel = (Button)FindControl(2);
            REGISTER_CLIENT(ETD.EID_CLICK, cancel, OnCancel);
        }

        public override void Show()
        {
            if (!IsShown() && def_rect != null)
            {
                Rect r = def_rect;

                if (r.w > screen.Width())
                {
                    int extra = r.w - screen.Width();
                    r.w -= extra;
                }

                if (r.h > screen.Height())
                {
                    int extra = r.h - screen.Height();
                    r.h -= extra;
                }

                r.x = (screen.Width() - r.w) / 2;
                r.y = (screen.Height() - r.h) / 2;

                MoveTo(r);

                exit_latch = true;
                Button.PlaySound(Button.SOUNDS.SND_CONFIRM);
                MusicDirector.SetMode(MusicDirector.MODES.CREDITS);

                var block = Resources.Load<TextAsset>("credits");
                if (block != null && !string.IsNullOrWhiteSpace(block.text) && credits != null)
                {
                    credits.SetText(block.text);
                }
            }

            base.Show();
        }

        // Operations:
        public virtual void ExecFrame()
        {
            if (credits != null && credits.GetLineCount() > 0)
            {
                credits.SmoothScroll(ScrollWindow.SCROLL.SCROLL_DOWN, Game.GUITime());

                if (credits.GetTopIndex() >= credits.GetLineCount() - 1)
                {
                    credits.ScrollTo(0);
                }
            }

            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                OnApply(null, null);
            }

            if (Keyboard.KeyDown(KeyMap.VK_ESCAPE))
            {
                if (!exit_latch)
                    OnCancel(null, null);
            }
            else
            {
                exit_latch = false;
            }
        }

        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                ErrLogger.PrintLine("Exit Confirmed.");
                stars.Exit();
            }
#if UNITY_EDITOR
            // Application.Quit() does not work in the editor so
            // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            manager.ShowMenuDlg();
            MusicDirector.SetMode(MusicDirector.MODES.MENU);
        }


        protected MenuScreen manager;

        protected RichTextBox credits;
        protected Button apply;
        protected Button cancel;
        protected Rect def_rect;

        protected bool exit_latch;
    }
}