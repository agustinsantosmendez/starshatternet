﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmdOrdersDlg.h/CmdOrdersDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Operational Command Dialog (Campaign Orders Tab)
*/
using System;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmdOrdersDlg : CmdDlg
    {
        public CmdOrdersDlg(Screen s, Camera c, FormDef def, CmpnScreen mgr, GameObject p) :
          base(s, mgr, 0, 0, s.Width(), s.Height(), c, p, "CmdOrdersDlg")
        {
            lbl_orders = null;

            Init(def);
        }
        //public virtual ~CmdOrdersDlg();
        public override void RegisterControls()
        {
            lbl_orders = FindControl(400);

            RegisterCmdControls(this);

            if (btn_save != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_save, OnSave);

            if (btn_exit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_exit, OnExit);

            for (int i = 0; i < 5; i++)
            {
                if (btn_mode[i] != null)
                    REGISTER_CLIENT(ETD.EID_CLICK, btn_mode[i], OnMode);
            }
        }
        public override void Show()
        {
            mode = MODE.MODE_ORDERS;

            base.Show();
            ShowCmdDlg();

            campaign = Campaign.GetCampaign();

            if (campaign != null && lbl_orders != null)
            {
                FontItem Limerick = FontMgr.Find("Limerick12");
                int limerickSize = 14;
                if (Limerick != null)
                    limerickSize = Limerick.size;

                FontItem Verdana = FontMgr.Find("Verdana");
                int verdanaSize = 12;
                if (Limerick != null)
                    verdanaSize = Verdana.size;

                string orders = "<size=" + limerickSize + "><color=#ffff80>";
                orders += Game.GetText("CmdOrdersDlg.situation");
                orders += "\n</color></size><size=" + verdanaSize + "><color=#ffffff>";
                if (campaign.Situation() != null)
                    orders += campaign.Situation();
                else
                    orders += campaign.Description();

                orders += "\n\n</color></size><size=" + limerickSize + "><color=#ffff80>";
                orders += Game.GetText("CmdOrdersDlg.orders");
                orders += "\n</color></size><size=" + verdanaSize + "><color=#ffffff>";
                orders += campaign.Orders();
                orders += "</color></size>";

                lbl_orders.SetText(orders);
            }
        }
        public override void ExecFrame()
        {
            base.ExecFrame();
        }

        // Operations:
        public override void OnMode(ActiveWindow obj, AWEvent evnt)
        {
            base.OnSave(obj, evnt);
        }
        public override void OnSave(ActiveWindow obj, AWEvent evnt)
        {
            base.OnExit(obj, evnt);
        }
        public override void OnExit(ActiveWindow obj, AWEvent evnt)
        {
            base.OnMode(obj, evnt);
        }

        protected ActiveWindow lbl_orders;
    }
}