﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      ModDlg.h/ModDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mod Config Dialog Active Window class
*/
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class ModDlg : FormWindow
    {
        public ModDlg(Screen s, Camera c, FormDef def, BaseScreen mgr, GameObject p) :
               base(s, 0, 0, s.Width(), s.Height(), c, p, "ModDlg")
        {
            manager = mgr;
            lst_disabled = null; lst_enabled = null; btn_enable = null; btn_disable = null;
            btn_increase = null; btn_decrease = null; btn_accept = null; btn_cancel = null;
            aud_btn = null; vid_btn = null; ctl_btn = null; opt_btn = null; mod_btn = null;
            config = null; changed = false;

            config = ModConfig.Instance;
            Init(def);
        }
        //public virtual ~ModDlg();

        public override void RegisterControls()
        {
            btn_accept = (Button)FindControl(1);
            btn_cancel = (Button)FindControl(2);
            btn_enable = (Button)FindControl(301);
            btn_disable = (Button)FindControl(302);
            btn_increase = (Button)FindControl(303);
            btn_decrease = (Button)FindControl(304);

            lst_disabled = (ListBox)FindControl(201);
            lst_enabled = (ListBox)FindControl(202);

            if (btn_accept != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_accept, OnAccept);

            if (btn_cancel != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_cancel, OnCancel);

            if (lst_enabled != null)
                REGISTER_CLIENT(ETD.EID_SELECT, lst_enabled, OnSelectEnabled);

            if (lst_disabled != null)
                REGISTER_CLIENT(ETD.EID_SELECT, lst_disabled, OnSelectDisabled);

            if (btn_enable != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_enable, OnEnable);
                btn_enable.SetEnabled(false);
            }

            if (btn_disable != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_disable, OnDisable);
                btn_disable.SetEnabled(false);
            }

            if (btn_increase != null)
            {
                string up_arrow = char.ConvertFromUtf32(0x2191); // Font.ARROW_UP;
                btn_increase.SetText(up_arrow);
                btn_increase.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_increase, OnIncrease);
            }

            if (btn_decrease != null)
            {
                string dn_arrow = char.ConvertFromUtf32(0x2193);  //Font.ARROW_DOWN;
                btn_decrease.SetText(dn_arrow);
                btn_decrease.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_decrease, OnDecrease);
            }

            vid_btn = (Button)FindControl(901);
            REGISTER_CLIENT(ETD.EID_CLICK, vid_btn, OnVideo);

            aud_btn = (Button)FindControl(902);
            REGISTER_CLIENT(ETD.EID_CLICK, aud_btn, OnAudio);

            ctl_btn = (Button)FindControl(903);
            REGISTER_CLIENT(ETD.EID_CLICK, ctl_btn, OnControls);

            opt_btn = (Button)FindControl(904);
            REGISTER_CLIENT(ETD.EID_CLICK, opt_btn, OnOptions);

            mod_btn = (Button)FindControl(905);
            if (mod_btn != null)
                REGISTER_CLIENT(ETD.EID_CLICK, mod_btn, OnMod);
        }
        public override void Show()
        {
            base.Show();
            UpdateLists();

            if (vid_btn != null) vid_btn.SetButtonState(0);
            if (aud_btn != null) aud_btn.SetButtonState(0);
            if (ctl_btn != null) ctl_btn.SetButtonState(0);
            if (opt_btn != null) opt_btn.SetButtonState(0);
            if (mod_btn != null) mod_btn.SetButtonState(1);
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                if (btn_accept != null && btn_accept.IsEnabled())
                    OnAccept(null, null);
            }
        }

        // Operations:

        public virtual void OnIncrease(ActiveWindow obj, AWEvent evnt)
        {
            int index = lst_enabled.GetSelection();
            config.IncreaseModPriority(index--);

            UpdateLists();
            lst_enabled.SetSelected(index);
            btn_disable.SetEnabled(true);
            btn_increase.SetEnabled(index > 0);
            btn_decrease.SetEnabled(index < lst_enabled.NumItems() - 1);
        }
        public virtual void OnDecrease(ActiveWindow obj, AWEvent evnt)
        {
            int index = lst_enabled.GetSelection();
            config.DecreaseModPriority(index++);

            UpdateLists();
            lst_enabled.SetSelected(index);
            btn_disable.SetEnabled(true);
            btn_increase.SetEnabled(index > 0);
            btn_decrease.SetEnabled(index < lst_enabled.NumItems() - 1);
        }

        public virtual void OnSelectEnabled(ActiveWindow obj, AWEvent evnt)
        {
            if (btn_enable != null && lst_disabled != null)
            {
                btn_enable.SetEnabled(lst_disabled.GetSelCount() == 1);
            }
        }
        public virtual void OnSelectDisabled(ActiveWindow obj, AWEvent evnt)
        {
            if (btn_enable != null && lst_disabled != null)
            {
                btn_enable.SetEnabled(lst_disabled.GetSelCount() == 1);
            }
        }

        public virtual void OnEnable(ActiveWindow obj, AWEvent evnt)
        {
            int index = lst_disabled.GetSelection();
            string mod_name = lst_disabled.GetItemText(index);

            config.EnableMod(mod_name);
            changed = true;

            UpdateLists();

            ModInfo mod_info = config.GetModInfo(mod_name);
            ModInfoDlg mod_info_dlg = manager.GetModInfoDlg();

            if (mod_info != null && mod_info_dlg != null)
            {
                mod_info_dlg.SetModInfo(this, mod_info);
                manager.ShowModInfoDlg();
            }
        }
        public virtual void OnDisable(ActiveWindow obj, AWEvent evnt)
        {
            int index = lst_enabled.GetSelection();
            string mod_name = lst_enabled.GetItemText(index);

            config.DisableMod(mod_name);
            changed = true;

            UpdateLists();
        }

        public virtual void OnAccept(ActiveWindow obj, AWEvent evnt)
        {
            manager.ApplyOptions();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            manager.CancelOptions();
        }

        public virtual void Apply()
        {
            if (changed)
            {
                config.Save();
                config.FindMods();
                config.Redeploy();
                changed = false;
            }
        }
        public virtual void Cancel()
        {
            if (changed)
            {
                config.Load();
                config.FindMods();
                config.Redeploy();
                changed = false;
            }
        }

        public virtual void OnAudio(ActiveWindow obj, AWEvent evnt) { manager.ShowAudDlg(); }
        public virtual void OnVideo(ActiveWindow obj, AWEvent evnt) { manager.ShowVidDlg(); }
        public virtual void OnOptions(ActiveWindow obj, AWEvent evnt) { manager.ShowOptDlg(); }
        public virtual void OnControls(ActiveWindow obj, AWEvent evnt) { manager.ShowCtlDlg(); }
        public virtual void OnMod(ActiveWindow obj, AWEvent evnt) { manager.ShowModDlg(); }


        protected void UpdateLists()
        {
            config = ModConfig.Instance;

            if (config != null && lst_disabled != null && lst_enabled != null)
            {
                lst_disabled.ClearItems();
                lst_enabled.ClearItems();

                foreach (var t in config.DisabledMods())
                {
                    lst_disabled.AddItem(t);
                }

                foreach (string t in config.EnabledMods())
                {
                    lst_enabled.AddItem(t);
                }
            }

            if (btn_disable != null)
                btn_disable.SetEnabled(false);

            if (btn_enable != null)
                btn_enable.SetEnabled(false);

            if (btn_increase != null)
                btn_increase.SetEnabled(false);

            if (btn_decrease != null)
                btn_decrease.SetEnabled(false);
        }

        protected BaseScreen manager;

        protected ListBox lst_disabled;
        protected ListBox lst_enabled;

        protected Button btn_accept;
        protected Button btn_cancel;
        protected Button btn_enable;
        protected Button btn_disable;
        protected Button btn_increase;
        protected Button btn_decrease;

        protected Button aud_btn;
        protected Button vid_btn;
        protected Button opt_btn;
        protected Button ctl_btn;
        protected Button mod_btn;

        protected ModConfig config;
        protected bool changed;
    }
}