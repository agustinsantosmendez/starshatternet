﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputeSize : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        var collider = GetComponentInChildren<Collider>();
        if (collider != null) Debug.Log(collider.bounds.size);
        var renderer = GetComponentInChildren<Renderer>();
        if (renderer != null) Debug.Log(renderer.bounds.size);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
