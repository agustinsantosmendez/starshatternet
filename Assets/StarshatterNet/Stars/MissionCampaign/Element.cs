﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Element.h/Element.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Package Element (e.g. Flight) class
*/
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;

namespace StarshatterNet.Stars
{
#warning Element class is still in development and is not recommended for production.
    public class Element : SimObserver
    {
        // CONSTRUCTORS:
        public Element(string call_sign, int a_iff, Mission.TYPE a_type = 0 /*PATROL*/)
        {
            id = id_key++; name = call_sign; type = a_type; iff = a_iff;
            player = 0; command_ai = 1; commander = null; assignment = null; carrier = null;
            combat_group = null; combat_unit = null; launch_time = 0; hold_time = 0;
            zone_lock = 0; respawns = 0; count = 0; rogue = false; playable = true; intel = 0;

            if (call_sign == null)
            {
                string buf = "Pkg " + id;
                name = buf;
            }

            SetLoadout(null);
        }


        // ~Element();

        //public int operator ==( Element& e)  { return id == e.id; }

        // GENERAL ACCESSORS:
        public int Identity() { return id; }
        public Mission.TYPE Type() { return type; }
        public string Name() { return name; }
        public void SetName(string s) { name = s; }
        public virtual int GetIFF() { return iff; }
        public int Player() { return player; }
        public void SetPlayer(int p) { player = p; }
        public ulong GetLaunchTime() { return launch_time; }
        public void SetLaunchTime(ulong t)
        {
            if (launch_time == 0 || t == 0)
                launch_time = t;
        }
        public Intel.INTEL_TYPE IntelLevel() { return intel; }
        public void SetIntelLevel(Intel.INTEL_TYPE i) { intel = i; }

        // ELEMENT COMPONENTS:
        public int NumShips() { return ships.Count; }
        public int AddShip(Ship ship, int index = -1)
        {
            if (ship != null && !ships.Contains(ship))
            {
                Observe(ship);

                if (index < 0)
                {
                    ships.Add(ship);
                    index = ships.Count;
                }
                else
                {
                    ships.Insert(index - 1, ship);
                }

                ship.SetElement(this);

                if (respawns < ship.RespawnCount())
                    respawns = ship.RespawnCount();
            }

            return index;
        }
        public void DelShip(Ship ship)
        {
            if (ship != null && ships.Contains(ship))
            {
                ships.Remove(ship);
                ship.SetElement(null);

                if (ships.IsEmpty())
                    respawns = ship.RespawnCount();
            }
        }
        public Ship GetShip(int index)
        {
            if (index >= 1 && index <= ships.Count)
                return ships[index - 1];

            return null;
        }
        public CLASSIFICATION GetShipClass()
        {
            if (ships.Count > 0)
                return ships[0].Class();

            return 0;
        }
        public int FindIndex(Ship s)
        {
            return ships.IndexOf(s) + 1;
        }
        public bool Contains(Ship s)
        {
            return ships.Contains(s);
        }
        public bool IsActive()
        {
            bool active = false;

            for (int i = 0; i < ships.Count && !active; i++)
            {
                Ship s = ships[i];
                if (s.Life() != 0 && s.MissionClock() != 0)
                    active = true;
            }

            return active;
        }
        public bool IsFinished()
        {
            bool finished = false;

            if (launch_time > 0 && respawns < 1)
            {
                finished = true;

                if (ships.Count > 0)
                {
                    for (int i = 0; i < ships.Count && finished; i++)
                    {
                        Ship s = ships[i];
                        if (s.RespawnCount() > 0 ||
                                s.MissionClock() == 0 ||
                                s.Life() != 0 && s.GetInbound() == null)
                            finished = false;
                    }
                }
            }

            return finished;
        }
        public bool IsNetObserver()
        {
            bool observer = !IsSquadron();

            for (int i = 0; i < ships.Count && observer; i++)
            {
                Ship s = ships[i];

                if (!s.IsNetObserver())
                    observer = false;
            }

            return observer;
        }
        public bool IsSquadron()
        {
            return count > 0;
        }
        public bool IsStatic()
        {
            if (IsSquadron() || IsFinished())
                return false;

            Ship s = ships[0];
            if (s != null && s.IsStatic())
                return true;

            return false;
        }
        public bool IsHostileTo(Ship s)
        {
            if (iff <= 0 || iff >= 100 || s == null || launch_time == 0 || IsFinished())
                return false;

            if (IsSquadron())
                return false;

            if (s.IsRogue())
                return true;

            int s_iff = s.GetIFF();

            if (s_iff <= 0 || s_iff >= 100 || s_iff == iff)
                return false;

            if (ships.Count > 0 && ships[0].GetRegion() != s.GetRegion())
                return false;

            return true;
        }

        public bool IsHostileTo(int iff_code)
        {
            if (iff <= 0 || iff >= 100 || launch_time == 0 || IsFinished())
                return false;

            if (IsSquadron())
                return false;

            if (iff_code <= 0 || iff_code >= 100 || iff_code == iff)
                return false;

            return true;
        }
        public bool IsObjectiveTargetOf(Ship s)
        {
            if (s == null || launch_time == 0 || IsFinished())
                return false;

            string e_name = Name();
            int e_len = Name().Length;

            Instruction orders = s.GetRadioOrders();
            if (orders != null && orders.Action() > Instruction.ACTION.SWEEP)
            {
                string o_name = orders.TargetName();
                int o_len = 0;

                if (!string.IsNullOrEmpty(o_name))
                    o_len = o_name.Length;

                if (e_len < o_len)
                    o_len = e_len;

                if (e_name.StartsWith(o_name))
                    return true;
            }

            Element elem = s.GetElement();
            if (elem != null)
            {
                for (int i = 0; i < elem.NumObjectives(); i++)
                {
                    Instruction obj = elem.GetObjective(i);

                    if (obj != null)
                    {
                        string o_name = obj.TargetName();
                        int o_len = 0;

                        if (!string.IsNullOrEmpty(o_name))
                            o_len = o_name.Length;

                        if (e_len < o_len)
                            o_len = e_len;

                        if (e_name.StartsWith(o_name))
                            return true;
                    }
                }
            }

            return false;
        }
        public bool IsRogue() { return rogue; }
        public bool IsPlayable() { return playable; }
        public int[] Loadout() { return load; }

        public void SetRogue(bool r) { rogue = r; }
        public void SetPlayable(bool p) { playable = p; }
        public void SetLoadout(int[] l)
        {
            if (l != null)
            {
                //CopyMemory(load, l, sizeof(load));
                l.CopyTo(load, 0);
            }
            else
            {
                for (int i = 0; i < 16; i++)
                    load[i] = -1;
            }
        }
        public virtual void SetIFF(int iff)
        {
            for (int i = 0; i < ships.Count; i++)
                ships[i].SetIFF(iff);
        }

        public virtual void ExecFrame(double seconds)
        {
            if (hold_time > 0)
            {
                hold_time -= seconds;
                return;
            }

            foreach (Instruction instr in flight_plan)
            {
                if (instr.Status() == Instruction.STATUS.COMPLETE && instr.HoldTime() > 0)
                    instr.SetHoldTime(instr.HoldTime() - seconds);
            }
        }
        public override bool Update(SimObject obj)
        {
            // false alarm, keep watching:
            if (obj.Life() != 0)
            {
                ErrLogger.PrintLine("Element ({0}) false update on ({1}) life = {2}", Name(), obj.Name(), obj.Life());
                return false;
            }

            Ship s = (Ship)obj;
            ships.Remove(s);

            if (ships.IsEmpty())
                respawns = s.RespawnCount();

            return base.Update(obj);
        }

        public override string GetObserverName()
        {
            return "Element " + Name();
        }

        // OBJECTIVES:
        public void ClearObjectives()
        {
            objectives.Destroy();
        }
        public void AddObjective(Instruction obj)
        {
            objectives.Add(obj);
        }
        public Instruction GetObjective(int index)
        {
            if (objectives.IsEmpty())
                return null;

            if (index < 0)
                index = 0;

            else if (index >= objectives.Count)
                index = index % objectives.Count;

            return objectives[index];
        }
        public Instruction GetTargetObjective()
        {
            for (int i = 0; i < objectives.Count; i++)
            {
                Instruction obj = objectives[i];

                if (obj.Status() <= Instruction.STATUS.ACTIVE)
                {
                    switch (obj.Action())
                    {
                        case Instruction.ACTION.INTERCEPT:
                        case Instruction.ACTION.STRIKE:
                        case Instruction.ACTION.ASSAULT:
                        case Instruction.ACTION.SWEEP:
                        case Instruction.ACTION.PATROL:
                        case Instruction.ACTION.RECON:
                        case Instruction.ACTION.ESCORT:
                        case Instruction.ACTION.DEFEND:
                            return obj;

                        default:
                            break;
                    }
                }
            }

            return null;
        }
        public int NumObjectives() { return objectives.Count; }

        public void ClearInstructions()
        {
            instructions.Clear();
        }
        public void AddInstruction(string instr)
        {
            instructions.Add(instr);
        }
        public string GetInstruction(int index)
        {
            if (instructions.IsEmpty())
                return "";

            if (index < 0)
                index = 0;

            if (index >= instructions.Count)
                index = index % instructions.Count;

            return instructions[index];
        }
        public int NumInstructions() { return instructions.Count; }

        // ORDERS AND NAVIGATION:
        public double GetHoldTime()
        {
            return hold_time;
        }
        public void SetHoldTime(double t)
        {
            if (t >= 0)
                hold_time = t;
        }
        public int GetZoneLock()
        {
            return zone_lock;
        }
        public void SetZoneLock(int z)
        {
            zone_lock = z;
        }
        public void AddNavPoint(Instruction pt, Instruction afterPoint = null, bool send = true)
        {
            if (pt != null && !flight_plan.Contains(pt))
            {
                int index = -1;

                if (afterPoint != null)
                {
                    index = flight_plan.IndexOf(afterPoint);

                    if (index > -1)
                        flight_plan.Insert(index + 1, pt);
                    else
                        flight_plan.Add(pt);
                }

                else
                {
                    flight_plan.Add(pt);
                }

                if (send)
                {
                    NetUtil.SendNavData(true, this, index, pt);
                }
            }
        }
        public void DelNavPoint(Instruction pt, bool send = true)
        {
            // XXX MEMORY LEAK
            // This is a small memory leak, but I'm not sure if it is
            // safe to delete the navpoint when removing it from the
            // flight plan.  Other ships in the element might have
            // pointers to the object...?

            if (pt != null)
            {
                int index = flight_plan.IndexOf(pt);
                flight_plan.Remove(pt);

                if (send)
                {
                    NetUtil.SendNavDelete(this, index);
                }
            }
        }

        public void ClearFlightPlan(bool send = true)
        {
            hold_time = 0;
            flight_plan.Destroy();
            objectives.Destroy();
            instructions.Destroy();

            if (send)
            {
                NetUtil.SendNavDelete(this, -1);
            }
        }
        public Instruction GetNextNavPoint()
        {
            if (hold_time <= 0 && flight_plan.Count > 0)
            {
                foreach (Instruction navpt in flight_plan)
                {
                    if (navpt.Status() == Instruction.STATUS.COMPLETE && navpt.HoldTime() > 0)
                        return navpt;

                    if (navpt.Status() <= Instruction.STATUS.ACTIVE)
                        return navpt;
                }
            }

            return null;
        }
        public int GetNavIndex(Instruction n)
        {
            int index = 0;

            if (flight_plan.Count > 0)
            {
                foreach (Instruction navpt in flight_plan)
                {
                    index++;
                    if (navpt == n)
                        return index;
                }
            }

            return 0;
        }
        public List<Instruction> GetFlightPlan()
        {
            return flight_plan;
        }
        public int FlightPlanLength()
        {
            return flight_plan.Count;
        }
        static RadioHandler rh;
        public virtual void HandleRadioMessage(RadioMessage msg)
        {
            if (msg == null) return;


            // if this is a message from within the element,
            // then all ships should report in.  Otherwise,
            // just the leader will acknowledge the message.
            bool full_report = ships.Contains(msg.Sender());
            bool reported = false;

            foreach (Ship s in ships)
            {
                if (rh.ProcessMessage(msg, s))
                {
                    if (full_report)
                    {
                        if (s != msg.Sender())
                            rh.AcknowledgeMessage(msg, s);
                    }

                    else if (!reported)
                    {
                        rh.AcknowledgeMessage(msg, s);
                        reported = true;
                    }
                }
            }
        }

        // CHAIN OF COMMAND:
        public Element GetCommander() { return commander; }
        public void SetCommander(Element e) { commander = e; }
        public Element GetAssignment() { return assignment; }
        public void SetAssignment(Element e) { assignment = e; }
        public void ResumeAssignment()
        {
            SetAssignment(null);

            if (objectives.IsEmpty())
                return;

            Instruction objective = null;

            for (int i = 0; i < objectives.Count && objective == null; i++)
            {
                Instruction instr = objectives[i];

                if (instr.Status() <= Instruction.STATUS.ACTIVE)
                {
                    switch (instr.Action())
                    {
                        case Instruction.ACTION.INTERCEPT:
                        case Instruction.ACTION.STRIKE:
                        case Instruction.ACTION.ASSAULT:
                            objective = instr;
                            break;
                    }
                }
            }

            if (objective != null)
            {
                Sim sim = Sim.GetSim();

                foreach (Element elem in sim.GetElements())
                {
                    SimObject tgt = objective.GetTarget();

                    if (tgt != null && tgt.Type() == (int)SimObject.TYPES.SIM_SHIP && elem.Contains((Ship)tgt))
                    {
                        SetAssignment(elem);
                        return;
                    }
                }
            }
        }

        public bool CanCommand(Element e)
        {
            while (e != null)
            {
                if (e.commander == this)
                    return true;
                e = e.commander;
            }

            return false;
        }
        public Ship GetCarrier() { return carrier; }
        public void SetCarrier(Ship c) { carrier = c; }
        public int GetCommandAILevel() { return command_ai; }
        public void SetCommandAILevel(int n) { command_ai = n; }
        public string GetSquadron() { return squadron; }
        public void SetSquadron(string s) { squadron = s; }

        // DYNAMIC CAMPAIGN:
        public CombatGroup GetCombatGroup() { return combat_group; }
        public void SetCombatGroup(CombatGroup g) { combat_group = g; }
        public CombatUnit GetCombatUnit() { return combat_unit; }
        public void SetCombatUnit(CombatUnit u) { combat_unit = u; }

        // SQUADRON STUFF:
        public int GetCount() { return count; }
        public void SetCount(int n) { count = n; }


        protected int id;
        protected int iff;
        protected Mission.TYPE type;
        protected int player;
        protected int command_ai;
        protected int respawns;
        protected Intel.INTEL_TYPE intel;
        protected string name;

        // squadron elements only:
        protected int count;

        protected List<Ship> ships;
        protected List<string> ship_names;
        protected List<string> instructions;
        protected List<Instruction> objectives;
        protected List<Instruction> flight_plan;

        protected Element commander;
        protected Element assignment;
        protected Ship carrier;
        protected string squadron;

        protected CombatGroup combat_group;
        protected CombatUnit combat_unit;
        protected ulong launch_time;
        protected double hold_time;

        protected bool rogue;
        protected bool playable;
        protected int zone_lock;
        protected int[] load = new int[16];

        private static int id_key = 1000;
    }
}