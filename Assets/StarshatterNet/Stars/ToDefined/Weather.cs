﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Weather.h/Weather.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Manages local weather conditions according to the system stardate
*/
using StarshatterNet.Gen.Misc;
using System;

namespace StarshatterNet.Stars
{
    public class Weather
    {
        public enum STATE
        {
            CLEAR,
            HIGH_CLOUDS,
            MODERATE_CLOUDS,
            OVERCAST,
            FOG,
            STORM,
        }

        public Weather()
        {
            state = STATE.CLEAR;
            period = 7 * 20 * 3600;
            ceiling = 0;
            visibility = 1;

            for (int i = 0; i < NUM_STATES; i++)
                chances[i] = 0;

            chances[0] = 1;
        }

        //public virtual ~Weather();



        public static readonly int NUM_STATES = Enum.GetNames(typeof(STATE)).Length;

        public virtual void Update()
        {
            NormalizeChances();

            double weather = (Math.Sin(StarSystems.StarSystem.Stardate() * 2 * Math.PI / period) + 1) / 2;

            state = active_states[0];

            for (int i = 1; i < NUM_STATES; i++)
            {
                if (weather > thresholds[i - 1] && weather <= thresholds[i])
                {
                    state = active_states[i];
                    break;
                }
            }

            switch (state)
            {
                default:
                case STATE.CLEAR:
                    ceiling = 0;
                    visibility = 1.0;
                    break;

                case STATE.HIGH_CLOUDS:
                    ceiling = 0;
                    visibility = 0.9;
                    break;

                case STATE.MODERATE_CLOUDS:
                    ceiling = 0;
                    visibility = 0.8;
                    break;

                case STATE.OVERCAST:
                    ceiling = 6000;
                    visibility = 0.7;
                    break;

                case STATE.FOG:
                    ceiling = 3500;
                    visibility = 0.6;
                    break;

                case STATE.STORM:
                    ceiling = 7500;
                    visibility = 0.5;
                    break;
            }
        }

        // accessors:
        public STATE State() { return state; }
        public string Description()
        {
            string description;
            switch (state)
            {
                default:
                case STATE.CLEAR:
                    description = localeManager.GetText("weather.clear");
                    break;

                case STATE.HIGH_CLOUDS:
                    description = localeManager.GetText("weather.high-clouds");
                    break;

                case STATE.MODERATE_CLOUDS:
                    description = localeManager.GetText("weather.partly-cloudy");
                    break;

                case STATE.OVERCAST:
                    description = localeManager.GetText("weather.overcast");
                    break;

                case STATE.FOG:
                    description = localeManager.GetText("weather.fog");
                    break;

                case STATE.STORM:
                    description = localeManager.GetText("weather.storm");
                    break;
            }

            return description;
        }
        public double Period() { return period; }
        public double Chance(STATE s) { return chances[(int)s]; }
        public double Ceiling() { return ceiling; }
        public double Visibility() { return visibility; }

        public void SetPeriod(double p) { period = p; }
        public void SetChance(int n, double c)
        {
            if (n >= 0 && n < NUM_STATES)
            {
                if (c > 1 && c <= 100)
                    chances[n] = c / 100;

                else if (c < 1)
                    chances[n] = c;
            }
        }

        protected void NormalizeChances()
        {
            double total = 0;

            for (int i = 1; i < NUM_STATES; i++)
                total += chances[i];

            if (total <= 1)
            {
                chances[0] = 1 - total;
            }

            else
            {
                chances[0] = 0;

                for (int i = 1; i < NUM_STATES; i++)
                    chances[i] /= total;
            }

            int index = 0;
            double level = 0;

            for (int i = 0; i < NUM_STATES; i++)
            {
                if (chances[i] > 0)
                {
                    level += chances[i];

                    active_states[index] = (STATE)i;
                    thresholds[index] = level;

                    index++;
                }
            }

            while (index < NUM_STATES)
                thresholds[index++] = 10;
        }

        protected STATE state;
        protected double period;
        protected double[] chances = new double[NUM_STATES];
        protected double ceiling;
        protected double visibility;

        protected STATE[] active_states = new STATE[NUM_STATES];
        protected double[] thresholds = new double[NUM_STATES];
        protected ContentBundle localeManager; //TODO = ServiceLocator.Current.GetInstance<ContentBundle>();

    }
}
