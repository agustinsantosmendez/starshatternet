﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MsnSelectDlg.h/MsnSelectDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Select Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MsnSelectDlg : FormWindow
    {
        public MsnSelectDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
             base(s, 0, 0, s.Width(), s.Height(), c, p, "MsnSelectDlg")
        {
            manager = mgr;
            cmb_campaigns = null; lst_campaigns = null; lst_missions = null;
            btn_accept = null; btn_cancel = null; btn_mod = null;
            btn_new = null; btn_edit = null; btn_del = null; editable = false;
            description = null; stars = null; campaign = null;
            selected_mission = -1; mission_id = 0;

            stars = Starshatter.GetInstance();
            campaign = Campaign.GetCampaign();
            edit_mission = null;

            Init(def);
        }
        //public virtual ~MsnSelectDlg();
        public override void RegisterControls()
        {
            btn_accept = (Button)FindControl(1);
            btn_cancel = (Button)FindControl(2);

            if (btn_accept != null)
            {
                btn_accept.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_accept, OnAccept);
            }

            if (btn_cancel != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_cancel, OnCancel);
            }

            //TODO btn_mod = (Button)FindControl(300);
            btn_new = (Button)FindControl(301);
            btn_edit = (Button)FindControl(302);
            btn_del = (Button)FindControl(303);

            if (btn_mod != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_mod, OnMod);

            if (btn_new != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_new, OnNew);

            if (btn_edit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_edit, OnEdit);

            if (btn_del != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_del, OnDel);
                REGISTER_CLIENT(ETD.EID_USER_1, btn_del, OnDelConfirm);
            }

            description = FindControl(200);

            cmb_campaigns = (ComboBox)FindControl(201);
            lst_campaigns = (ListBox)FindControl(203);
            lst_missions = (ListBox)FindControl(202);

            if (cmb_campaigns != null)
            {
                REGISTER_CLIENT(ETD.EID_SELECT, cmb_campaigns, OnCampaignSelect);
            }

            if (lst_campaigns != null)
            {
                REGISTER_CLIENT(ETD.EID_SELECT, lst_campaigns, OnCampaignSelect);

                lst_campaigns.SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_FILLED_BOX);
                lst_campaigns.SetLeading(4);
            }

            if (lst_missions != null)
            {
                REGISTER_CLIENT(ETD.EID_SELECT, lst_missions, OnMissionSelect);

                lst_missions.SetSelectedStyle(ListBox.STYLE.LIST_ITEM_STYLE_FILLED_BOX);
                lst_missions.SetLeading(4);
            }
        }
        public override void Show()
        {
            base.Show();
            campaign = Campaign.GetCampaign();

            if (cmb_campaigns != null)
            {
                int n = 0;
                cmb_campaigns.ClearItems();
                foreach (Campaign c in Campaign.GetAllCampaigns())
                {
                    if (c.GetCampaignId() >= (int)Campaign.CONSTANTS.SINGLE_MISSIONS)
                    {
                        cmb_campaigns.AddItem(c.Name());

                        if (campaign.GetCampaignId() < (int)Campaign.CONSTANTS.SINGLE_MISSIONS)
                        {
                            campaign = Campaign.SelectCampaign(c.Name());
                            cmb_campaigns.SetSelection(n);
                        }

                        else if (campaign.GetCampaignId() == c.GetCampaignId())
                        {
                            cmb_campaigns.SetSelection(n);
                        }

                        n++;
                    }
                }
            }

            else if (lst_campaigns != null)
            {
                int n = 0;
                lst_campaigns.ClearItems();
                foreach (Campaign c in Campaign.GetAllCampaigns())
                {

                    if (c.GetCampaignId() >= (int)Campaign.CONSTANTS.SINGLE_MISSIONS)
                    {
                        lst_campaigns.AddItem(c.Name());

                        if (campaign.GetCampaignId() < (int)Campaign.CONSTANTS.SINGLE_MISSIONS)
                        {
                            campaign = Campaign.SelectCampaign(c.Name());
                            lst_campaigns.SetSelected(n);
                        }

                        else if (campaign.GetCampaignId() == c.GetCampaignId())
                        {
                            lst_campaigns.SetSelected(n);
                        }

                        n++;
                    }
                }
                //lst_campaigns.RebuildListObjets();
            }
            if (campaign != null)
            {
                int id = campaign.GetCampaignId();
                editable = (id >= (int)Campaign.CONSTANTS.MULTIPLAYER_MISSIONS &&
                id <= (int)Campaign.CONSTANTS.CUSTOM_MISSIONS);

                if (btn_new != null) btn_new.SetEnabled(editable);
                if (btn_edit != null) btn_edit.SetEnabled(false);
                if (btn_del != null) btn_del.SetEnabled(false);
            }

            if (description != null)
                description.SetText(Game.GetText("MsnSelectDlg.choose"));

            if (lst_missions != null)
            {
                lst_missions.ClearItems();

                if (campaign != null)
                {
                    foreach (MissionInfo info in campaign.GetMissionList())
                    {
                        Mission m = info.mission;

                        lst_missions.AddItem(info.name);

                        if (m != null && m == edit_mission)
                        {
                            lst_missions.SetSelected(lst_missions.NumItems() - 1);
                        }
                    }

                    if (selected_mission >= 0 && lst_missions.GetSelCount() == 0)
                    {
                        lst_missions.SetSelected(selected_mission);
                    }
                }

                lst_missions.RebuildListObjets();

                OnMissionSelect(null, null);
                edit_mission = null;
            }
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                if (btn_accept != null && btn_accept.IsEnabled())
                    OnAccept(null, null);
            }
        }

        // Operations:
        public virtual void OnCampaignSelect(ActiveWindow obj, AWEvent evnt)
        {
            string selected_campaign = null;

            if (cmb_campaigns != null)
                selected_campaign = cmb_campaigns.GetSelectedItem();
            else if (lst_campaigns != null)
                selected_campaign = lst_campaigns.GetSelectedItem();

            Campaign c = Campaign.SelectCampaign(selected_campaign);

            if (c != null)
            {
                campaign = c;

                if (cmb_campaigns != null)
                {
                    cmb_campaigns.ClearItems();

                    foreach (MissionInfo iter in campaign.GetMissionList())
                    {
                        cmb_campaigns.AddItem(iter.name);
                    }
                }

                else if (lst_missions != null)
                {
                    lst_missions.ClearItems();

                    foreach (MissionInfo iter in campaign.GetMissionList())
                    {
                        lst_missions.AddItem(iter.name);
                    }

                    lst_missions.RebuildListObjets();
                    lst_missions.ScrollTo(0);
                }

                if (btn_accept != null)
                    btn_accept.SetEnabled(false);

                if (description != null)
                    description.SetText(Game.GetText("MsnSelectDlg.choose"));

                int id = c.GetCampaignId();
                editable = (id >= (int)Campaign.CONSTANTS.MULTIPLAYER_MISSIONS &&
                id <= (int)Campaign.CONSTANTS.CUSTOM_MISSIONS);

                if (btn_new != null) btn_new.SetEnabled(editable);
                if (btn_edit != null) btn_edit.SetEnabled(false);
                if (btn_del != null) btn_del.SetEnabled(false);
            }
        }
        public virtual void OnMissionSelect(ActiveWindow obj, AWEvent evnt)
        {
            selected_mission = -1;

            for (int i = 0; i < lst_missions.NumItems(); i++)
                if (lst_missions.IsSelected(i))
                    selected_mission = i;

            if (btn_accept != null && description != null && campaign != null)
            {
                List<MissionInfo> mission_info_list = campaign.GetMissionList();

                if (selected_mission >= 0 && selected_mission < mission_info_list.Count)
                {
                    MissionInfo info = mission_info_list[selected_mission];
                    mission_id = info.id;

                    string time_buf;
                    FormatUtil.FormatDayTime(out time_buf, info.start);

                    FontItem Limerick = FontMgr.Find("Limerick12");
                    int limerickSize = 14;
                    if (Limerick != null)
                        limerickSize = Limerick.size;

                    FontItem Verdana = FontMgr.Find("Verdana");
                    int verdanaSize = 12;
                    if (Limerick != null)
                        verdanaSize = Verdana.size;

                    string d = "<size=" + limerickSize + "><color=#ffffff>";
                    d += info.name;
                    d += "</color></size><size=" + verdanaSize + "><color=#ffff80>\n\n";
                    d += Game.GetText("MsnSelectDlg.mission-type");
                    d += "</color><color=#ffffff>\n\t";
                    d += Mission.RoleName(info.type);
                    d += "\n\n</color><color=#ffff80>";
                    d += Game.GetText("MsnSelectDlg.scenario");
                    d += "</color><color=#ffffff>\n\t";
                    d += info.description;
                    d += "\n\n</color><color=#ffff80>";
                    d += Game.GetText("MsnSelectDlg.location");
                    d += "</color><color=#ffffff>\n\t";
                    d += info.region;
                    d += " ";
                    d += Game.GetText("MsnSelectDlg.sector");
                    d += " / ";
                    d += info.system;
                    d += " ";
                    d += Game.GetText("MsnSelectDlg.system");
                    d += "\n\n</color><color=#ffff80>";
                    d += Game.GetText("MsnSelectDlg.start-time");
                    d += "</color><color=#ffffff>\n\t";
                    d += time_buf;
                    d += "</color></size>";

                    description.SetText(d);
                    btn_accept.SetEnabled(true);

                    if (btn_edit != null) btn_edit.SetEnabled(editable);
                    if (btn_del != null) btn_del.SetEnabled(editable);
                }

                else
                {
                    description.SetText(Game.GetText("MsnSelectDlg.choose"));
                    btn_accept.SetEnabled(false);

                    if (btn_edit != null) btn_edit.SetEnabled(false);
                    if (btn_del != null) btn_del.SetEnabled(false);
                }
            }
        }

        public virtual void OnMod(ActiveWindow obj, AWEvent evnt)
        {
        }
        public virtual void OnNew(ActiveWindow obj, AWEvent evnt)
        {
            string cname = null;

            if (cmb_campaigns != null)
                cname = cmb_campaigns.GetSelectedItem();
            else if (lst_campaigns != null)
                cname = lst_campaigns.GetSelectedItem();

            Campaign c = Campaign.SelectCampaign(cname);
            if (c == null) return;

            MissionInfo info = c.CreateNewMission();
            if (info == null || info.mission == null)
                return;

            mission_id = info.id;

            MsnEditDlg editor = manager.GetMsnEditDlg();

            if (editor != null)
            {
                edit_mission = info.mission;

                editor.SetMissionInfo(info);
                editor.SetMission(info.mission);
                manager.ShowMsnEditDlg();
            }

            MsnEditNavDlg navdlg = (MsnEditNavDlg)manager.GetNavDlg();

            if (navdlg != null)
            {
                navdlg.SetMission(info.mission);
                navdlg.SetMissionInfo(info);
            }
        }
        public virtual void OnEdit(ActiveWindow obj, AWEvent evnt)
        {
            string cname = null;

            if (cmb_campaigns != null)
                cname = cmb_campaigns.GetSelectedItem();
            else if (lst_campaigns != null)
                cname = lst_campaigns.GetSelectedItem();

            Campaign c = Campaign.SelectCampaign(cname);
            if (c == null) return;

            Mission m = c.GetMission(mission_id);
            if (m == null) return;

            MsnEditDlg editor = manager.GetMsnEditDlg();

            if (editor != null)
            {
                edit_mission = m;

                editor.SetMissionInfo(c.GetMissionInfo(mission_id));
                editor.SetMission(m);
                manager.ShowMsnEditDlg();
            }
        }
        public virtual void OnDel(ActiveWindow obj, AWEvent evnt)
        {
            string cname = null;

            if (cmb_campaigns != null)
                cname = cmb_campaigns.GetSelectedItem();
            else if (lst_campaigns != null)
                cname = lst_campaigns.GetSelectedItem();

            Campaign c = Campaign.SelectCampaign(cname);
            if (c == null) return;

            Mission m = c.GetMission(mission_id);
            if (m == null) return;

            ConfirmDlg confirm = manager.GetConfirmDlg();
            if (confirm != null)
            {
                string msg = string.Format(Game.GetText("MsnSelectDlg.are-you-sure"), m.Name());
                confirm.SetMessage(msg);
                confirm.SetTitle(Game.GetText("MsnSelectDlg.confirm-delete"));
                confirm.SetParentControl(btn_del);

                manager.ShowConfirmDlg();
            }

            else
            {
                OnDelConfirm(obj, evnt);
            }
        }
        public virtual void OnDelConfirm(ActiveWindow obj, AWEvent evnt)
        {
            string cname = null;

            if (cmb_campaigns != null)
                cname = cmb_campaigns.GetSelectedItem();
            else if (lst_campaigns != null)
                cname = lst_campaigns.GetSelectedItem();

            Campaign c = Campaign.SelectCampaign(cname);
            if (c == null) return;

            edit_mission = null;
            c.DeleteMission(mission_id);
            Show();
        }

        public virtual void OnAccept(ActiveWindow obj, AWEvent evnt)
        {
            if (selected_mission >= 0)
            {
                Mouse.Show(false);

                int id = campaign.GetMissionList()[selected_mission].id;
                campaign.SetMissionId(id);
                campaign.ReloadMission(id);

                stars.SetGameMode(Starshatter.MODE.PREP_MODE);
            }
        }

        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            manager.ShowMenuDlg();
        }


        protected MenuScreen manager;

        protected Button btn_mod;
        protected Button btn_new;
        protected Button btn_edit;
        protected Button btn_del;
        protected Button btn_accept;
        protected Button btn_cancel;

        protected ComboBox cmb_campaigns;
        protected ListBox lst_campaigns;
        protected ListBox lst_missions;

        protected ActiveWindow description;

        protected Starshatter stars;
        protected Campaign campaign;
        protected int selected_mission;
        protected int mission_id;
        protected bool editable;

        private static Mission edit_mission;

    }
}