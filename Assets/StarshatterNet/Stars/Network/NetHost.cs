﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetHost.h/NetHost.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

     OVERVIEW
     ========
     Network Host
 */
using System;
using System.Collections.Generic;
using System.Net;

namespace StarshatterNet.Stars.Network
{
    public class NetHost
    {
        public NetHost()
        {
            string hostname = Dns.GetHostName();
            Init(hostname);
        }

        public NetHost(string host_addr)
        {
            Init(host_addr);
        }

        public string Name()
        {
            return name;
        }

        public NetAddr Address()
        {
            if (addresses.Count > 0)
                return addresses[0];

            return new NetAddr(0);
        }

        public string[] Aliases() { return aliases; }
        public List<NetAddr> AddressList() { return addresses; }


        private void Init(string host_name)
        {
            // Resolves a host name or IP address to an IPHostEntry instance using the dns class
            IPHostEntry iphost = System.Net.Dns.GetHostEntry(host_name);
            // get all of aliases that are associated with a host.
            aliases = iphost.Aliases;
            // get all of the possible IP addresses for this host
            IPAddress[] addrs = iphost.AddressList;

            foreach (IPAddress ip in addrs)
                addresses.Add(new NetAddr(ip));
        }

        private string name;
        private string[] aliases;
        private List<NetAddr> addresses = new List<NetAddr>();
    }
}