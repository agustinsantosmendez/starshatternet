﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      AudioConfig.h/AudioConfig.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Audio Configuration class
*/
using System;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Config
{
    public class AudioConfig
    {
        protected AudioConfig() { }

        public static void Initialize()
        {
            if (audio_config == null) audio_config = new AudioConfig();

            if (audio_config != null)
                audio_config.Load();
        }
        public static void Close() { audio_config = null; }
        public static AudioConfig GetInstance()
        {
            return audio_config;
        }
        public static AudioConfig Instance
        {
            get
            {
                if (audio_config == null) audio_config = new AudioConfig();
                return audio_config;
            }
        }

        public void Load(string filename = AudioFilename)
        {
            if (!ReaderSupport.DoesDataFileExist(filename))
            {
                ErrLogger.PrintLine("WARNING: invalid {0} file. Using defaults", filename);
                Save();
            }
            else
            {
                var entry = ReaderSupport.DeserializeData<AudioConfigEntry>(filename);
                if (entry != null)
                {
                    this.menu_music = entry.menu_music;
                    this.game_music = entry.game_music;
                    this.efx_volume = entry.efx_volume;
                    this.gui_volume = entry.gui_volume;
                    this.wrn_volume = entry.wrn_volume;
                    this.vox_volume = entry.vox_volume;
                }
            }
        }

        /// <summary>
        /// Saves the video config entries.
        /// </summary>        
        public void Save(string filename = AudioFilename)
        {
            ReaderSupport.SerializeData<AudioConfigEntry>(this, filename);
        }


        public static int MenuMusic()
        {
            if (audio_config != null)
                return -50 * (100 - audio_config.menu_music);

            return 0;
        }
        public static int GameMusic()
        {
            int vol = 0;

            if (audio_config != null)
            {
                vol = -50 * (100 - audio_config.game_music);

                if (audio_config.training)
                    vol -= 2000;
            }

            return vol;
        }
        public static int EfxVolume()
        {
            int vol = 0;

            if (audio_config != null)
            {
                vol = -50 * (100 - audio_config.efx_volume);

                if (audio_config.training)
                    vol -= 2000;
            }

            return vol;
        }
        public static int GuiVolume()
        {
            if (audio_config != null)
                return -50 * (100 - audio_config.gui_volume);

            return 0;
        }
        public static int WrnVolume()
        {
            int vol = 0;

            if (audio_config != null)
            {
                vol = -50 * (100 - audio_config.wrn_volume);

                if (audio_config.training)
                    vol -= 2000;
            }

            return vol;
        }

        public static int VoxVolume()
        {
            int vol = 0;

            if (audio_config != null)
            {
                vol = -50 * (100 - audio_config.vox_volume);

                if (audio_config.training && vol < -750)
                    vol = -750;
            }

            return vol;
        }
        public static int Silence()
        {
            return -5000;
        }
        public static void SetTraining(bool t)
        {
            if (audio_config != null)
                audio_config.training = t;
        }

        public int GetMenuMusic() { return menu_music; }
        public int GetGameMusic() { return game_music; }
        public int GetEfxVolume() { return efx_volume; }
        public int GetGuiVolume() { return gui_volume; }
        public int GetWrnVolume() { return wrn_volume; }
        public int GetVoxVolume() { return vox_volume; }

        public void SetMenuMusic(int v)
        {
            if (v < 0) v = 0;
            else if (v > 100) v = 100;

            menu_music = v;
        }
        public void SetGameMusic(int v)
        {
            if (v < 0) v = 0;
            else if (v > 100) v = 100;

            game_music = v;
        }
        public void SetEfxVolume(int v)
        {
            if (v < 0) v = 0;
            else if (v > 100) v = 100;

            efx_volume = v;
        }
        public void SetGuiVolume(int v)
        {
            if (v < 0) v = 0;
            else if (v > 100) v = 100;

            gui_volume = v;
            //Button::SetVolume(-50 * (100 - gui_volume));
        }
        public void SetWrnVolume(int v)
        {
            if (v < 0) v = 0;
            else if (v > 100) v = 100;

            wrn_volume = v;
            //Button::SetVolume(-50 * (100 - wrn_volume));
        }
        public void SetVoxVolume(int v)
        {
            if (v < 0) v = 0;
            else if (v > 100) v = 100;

            vox_volume = v;
        }


        protected int menu_music = 90;
        protected int game_music = 90;

        protected int efx_volume = 90;
        protected int gui_volume = 90;
        protected int wrn_volume = 90;
        protected int vox_volume = 90;

        protected bool training = false;

        private static AudioConfig audio_config = new AudioConfig();
        private const string AudioFilename = "audio.cfg";

        public static implicit operator AudioConfig(AudioConfigEntry ace)
        {
            var rst = new AudioConfig()
            {
                menu_music = ace.menu_music,
                game_music = ace.game_music,
                efx_volume = ace.efx_volume,
                gui_volume = ace.gui_volume,
                wrn_volume = ace.wrn_volume,
                vox_volume = ace.vox_volume
            };

            return rst;
        }
    }
    [Serializable]
    public class AudioConfigEntry
    {
        public int menu_music;
        public int game_music;
        public int efx_volume;
        public int gui_volume;
        public int wrn_volume;
        public int vox_volume;

        public static implicit operator AudioConfigEntry(AudioConfig ac)
        {
            var rst = new AudioConfigEntry()
            {
                menu_music = ac.GetMenuMusic(),
                game_music = ac.GetGameMusic(),
                efx_volume = ac.GetEfxVolume(),
                gui_volume = ac.GetGuiVolume(),
                wrn_volume = ac.GetWrnVolume(),
                vox_volume = ac.GetVoxVolume()
            };
            return rst;
        }
    }
}
