﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CameraView.h/CameraView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    3D Projection Camera View class
*/
using System;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Stars;
using StarshatterNet.Gen.Graphics;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using DWORD = System.UInt32;
using System.Collections.Generic;
using DigitalRune.Mathematics;

namespace StarshatterNet.Gen.Views
{
    // example https://github.com/Noesis/Tutorials/blob/0d3422e11e06b40d6b464688348bda98436982e8/Samples/RenderTexture/Unity/Assets/NoesisGUI/Samples/RenderTexture/DraggablePanel.cs
    // https://blog.theknightsofunity.com/implementing-minimap-unity/
    public class CameraView : View
    {
        public enum PROJECTION_TYPE
        {
            PROJECTION_PERSPECTIVE = 1,
            PROJECTION_ORTHOGONAL = 2,
        }

        public CameraView(Window c, Scene s, string name) : base(c, name)
        {
            if (emergency_scene == null && s == null)
            {
                emergency_scene = new Scene();
                emergency_cam = emergency_scene.Camera;
            }

            scene = s ?? emergency_scene;

            camera = scene.Camera;

            //projector = new Stars.Projector(c, cam);
            lens_flare_enable = false; halo_bitmap = null; infinite = 0;
            projection_type = PROJECTION_TYPE.PROJECTION_PERSPECTIVE;
            if (camera != null) camera.orthographic = false;

            elem_bitmap[0] = null;
            elem_bitmap[1] = null;
            elem_bitmap[2] = null;

            if (rectTransform == null) return;
            width = (int)rectTransform.rect.width;
            height = (int)rectTransform.rect.height;
        }

        //public virtual ~CameraView();

        // Operations:
        public override void Refresh() { throw new NotImplementedException(); }
        public override void OnWindowMove() { }
        public virtual void UseCamera(Camera cam)
        {
            if (cam != null)
                camera = cam;
            else
                camera = emergency_cam;
#if TODO
            projector.UseCamera(camera);
#endif
            camera.orthographic = (projection_type == PROJECTION_TYPE.PROJECTION_ORTHOGONAL);
        }

        public virtual void UseScene(Scene s)
        {
            if (s != null)
                scene = s;
            else
                scene = emergency_scene;
        }
        public virtual void LensFlareElements(Bitmap halo, Bitmap e1 = null, Bitmap e2 = null, Bitmap e3 = null)
        {
            if (halo != null)
                halo_bitmap = halo;

            if (e1 != null)
                elem_bitmap[0] = e1;

            if (e2 != null)
                elem_bitmap[1] = e2;

            if (e3 != null)
                elem_bitmap[2] = e3;
        }
        public virtual void LensFlare(bool enable, double dim = 1)
        {
            lens_flare_enable = enable;
            lens_flare_dim = dim;
        }

        public virtual void SetDepthScale(float scale) { throw new NotImplementedException(); }

        // accessors:
        public Camera GetCamera() { return camera; }
        public Stars.Projector GetProjector() { return projector; }
        public Scene GetScene() { return scene; }
        public virtual void SetFieldOfView(float fov)
        {
            if (camera != null)
                camera.fieldOfView = MathHelper.ToDegrees(fov);
        }
        public virtual float GetFieldOfView()
        {
            if (camera != null)
                return MathHelper.ToRadians(camera.fieldOfView);
            else
                return 0;
        }
        public virtual void SetProjectionType(PROJECTION_TYPE pt)
        {
            if (camera != null)
                camera.orthographic = (pt == PROJECTION_TYPE.PROJECTION_ORTHOGONAL);
            projection_type = pt;
        }
        public virtual PROJECTION_TYPE GetProjectionType()
        {
            return projection_type;
        }

#if TODO
        public Point Pos() { return camera.Pos(); }
        public Point vrt() { return camera.vrt(); }
        public Point vup() { return camera.vup(); }
        public Point vpn() { return camera.vpn(); }
        public Matrix33D Orientation() { return camera.Orientation(); }
#endif
        public Point SceneOffset() { return camera_loc; }

        // projection and clipping geometry:
        public virtual void TranslateScene() { throw new NotImplementedException(); }
        public virtual void UnTranslateScene() { throw new NotImplementedException(); }
        public virtual void MarkVisibleObjects() { throw new NotImplementedException(); }
        public virtual void MarkVisibleLights(Graphic g, DWORD flags) { throw new NotImplementedException(); }

        public virtual void RenderScene() { throw new NotImplementedException(); }
        public virtual void RenderSceneObjects(bool distant = false) { throw new NotImplementedException(); }
        public virtual void RenderForeground() { throw new NotImplementedException(); }
        public virtual void RenderBackground() { throw new NotImplementedException(); }
        public virtual void RenderSprites() { throw new NotImplementedException(); }
        public virtual void RenderLensFlare() { throw new NotImplementedException(); }
        public virtual void Render(Graphic g, DWORD flags) { throw new NotImplementedException(); }

        public virtual void FindDepth(Graphic g) { throw new NotImplementedException(); }
        public virtual int SetInfinite(int i) { throw new NotImplementedException(); }


        protected Camera camera;
        protected Scene scene;
        //TODO protected Video video;

        protected virtual void WorldPlaneToView(Plane plane) { throw new NotImplementedException(); }

        protected Point camera_loc;
        protected Vector3D cvrt;
        protected Vector3D cvup;
        protected Vector3D cvpn;

        protected Stars.Projector projector;
        protected int infinite;
        protected int width;
        protected int height;
        protected PROJECTION_TYPE projection_type;

        // lens flare:
        protected bool lens_flare_enable;
        protected double lens_flare_dim;
        protected Bitmap halo_bitmap;
        protected Bitmap[] elem_bitmap = new Bitmap[3];

        protected List<Graphic> graphics = new List<Graphic>();

        private static Camera emergency_cam;
        private static Scene emergency_scene;

        protected UnityEngine.RenderTexture renderTexture;
    }
}