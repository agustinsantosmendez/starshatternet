﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      DetailSet.h/DetailSet.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Level of Detail Manger class
 */
using System;
using System.Collections.Generic;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars
{
    public class DetailSet
    {
        public const int MAX_DETAIL = 4;

        public DetailSet()
        {
            for (int i = 0; i < MAX_DETAIL; i++)
                rad[i] = 0;

            index = -1;
            levels = 0;
            rgn = null;
        }

        //public virtual ~DetailSet();

        public int DefineLevel(double r, Graphic g, Point offset, Point spin) { throw new NotImplementedException(); }
        public void AddToLevel(int level, Graphic g, Point offset, Point spin) { throw new NotImplementedException(); }
        public int NumLevels() { return levels; }
        public int NumModels(int level) { throw new NotImplementedException(); }

        public void ExecFrame(double seconds) { throw new NotImplementedException(); }
        public void SetLocation(SimRegion rgn, Point loc) { throw new NotImplementedException(); }
        public static void SetReference(SimRegion rgn, Point loc) { throw new NotImplementedException(); }

        public int GetDetailLevel() { throw new NotImplementedException(); }
        public Graphic GetRep(int level, int n = 0) { throw new NotImplementedException(); }
        public Point GetOffset(int level, int n = 0) { throw new NotImplementedException(); }
        public Point GetSpin(int level, int n = 0) { throw new NotImplementedException(); }
        public void Destroy() { throw new NotImplementedException(); }


        protected List<Graphic> rep = new List<Graphic>();
        protected List<Point> off = new List<Point>();
        protected double[] rad = new double[MAX_DETAIL];

        protected List<Point> spin;
        protected List<Point> rate;

        protected int index;
        protected int levels;
        protected SimRegion rgn;
        protected Point loc;

        protected static SimRegion ref_rgn;
        protected static Point ref_loc;
    }
}