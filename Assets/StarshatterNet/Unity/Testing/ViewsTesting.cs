﻿using System;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using UnityEngine;
using UnityEngine.UI;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using Screen = StarshatterNet.Gen.Graphics.Screen;
public class ViewsTesting : MonoBehaviour
{
    public double radius = 100;
    public float cam_zoom = 2.5f;
    public float cam_az = -(float)(Math.PI / 6f);
    public float cam_el = (float)(Math.PI / 7f);
    public RawImage rawImage;

    private CameraView view;
    private Scene scene;
    private Model model;
    private Solid solid;

    private int width, height;
    private Screen screen;

    void Awake()
    {        
        // Display
        Video video = Video.GetVideo();
        // Canvas
        screen = new Screen(video);
        // Rect
        Window win = new Window(screen, 0, 0, screen.Width(), screen.Height());

        width = screen.Width();
        height = screen.Height();

        scene = new Scene();

        scene.SetAmbient(new Color32(60, 60, 60, 255));
        //Point light_pos = new Point(3e6, 5e6, 4e6);
        Point light_pos = new Point(3e3, 5e3, 4e3);

        LightNode main_light = new LightNode(1.0f); //1.25f);
        main_light.MoveTo(light_pos);
        main_light.SetType(LightNode.TYPES.LIGHT_DIRECTIONAL);
        main_light.SetColor(Color.white);
        main_light.SetShadow(true);
        scene.AddLight(main_light);

        LightNode back_light = new LightNode(0.5f);
        back_light.MoveTo(light_pos * -1);
        back_light.SetType(LightNode.TYPES.LIGHT_DIRECTIONAL);
        back_light.SetColor(Color.white);
        back_light.SetShadow(false);
        scene.AddLight(back_light);

        solid = new Solid();
        solid.Load("Ships/Alliance_Fighters/Eagle_0");
        radius = solid.Radius();
        scene.AddGraphic(solid);
        UpdateCamera();

        view = new CameraView(win, scene, "Views");
        view.SetProjectionType(CameraView.PROJECTION_TYPE.PROJECTION_PERSPECTIVE);
        view.SetFieldOfView(2f);

        if (rawImage != null)
        {
            var rt = new RenderTexture((int)rawImage.rectTransform.rect.width, (int)rawImage.rectTransform.rect.height, 32);
            rt.name = "CameraView Texture";
            rt.Create();

            scene.Camera.targetTexture = rt;
            rawImage.texture = rt;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //stars.Run();
        //stars.Activate(true);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCamera();

        //if (Input.GetKeyDown(KeyCode.I))
        //    stars.ScreenCapture();
    }
    void OnGUI()
    {
        var groupWidth = 380;
        var groupHeight = 145;

        var screenWidth = screen.Width();
        var screenHeight = screen.Height();

        var groupX = 20; // (screenWidth - groupWidth) / 2;
        var groupY = 20; // (screenHeight - groupHeight) / 2;

        GUI.BeginGroup(new Rect(groupX, groupY, groupWidth, groupHeight));
        GUI.Box(new Rect(0, 0, groupWidth, groupHeight), "Camera Settings");

        GUI.Label(new Rect(10, 30, 100, 30), "Azimut");
        cam_az = GUI.HorizontalSlider(new Rect(120, 35, 200, 30), cam_az, -(float)Math.PI, (float)Math.PI);
        GUI.Label(new Rect(330, 30, 50, 30), "(" + cam_az.ToString("f2") + ")");

        GUI.Label(new Rect(10, 70, 100, 30), "Elevation");
        cam_el = GUI.HorizontalSlider(new Rect(120, 75, 200, 30), cam_el, -(float)(0.43 * Math.PI), (float)(0.43 * Math.PI));
        GUI.Label(new Rect(330, 70, 50, 30), "(" + cam_el.ToString("f2") + ")");

        GUI.Label(new Rect(10, 105, 100, 30), "Zoom");
        cam_zoom = GUI.HorizontalSlider(new Rect(120, 110, 200, 30), cam_zoom, 1, 10);
        GUI.Label(new Rect(330, 105, 50, 30), "(" + cam_zoom.ToString("f2") + ")");

        GUI.EndGroup();
    }


    protected virtual void UpdateCamera()
    {
        double x = cam_zoom * radius * Math.Sin(cam_az) * Math.Cos(cam_el);
        double y = cam_zoom * radius * Math.Cos(cam_az) * Math.Cos(cam_el);
        double z = cam_zoom * radius * Math.Sin(cam_el);
        scene.Camera.LookAt(new Point(0, 0, 0), new Point(x, z, y), new Point(0, 1, 0));
    }

}
