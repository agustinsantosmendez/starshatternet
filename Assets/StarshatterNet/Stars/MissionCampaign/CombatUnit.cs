﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CombatUnit.h/CombatUnit.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
     A ship, station, or ground unit in the dynamic campaign.
*/
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.SimElements;
using System;
using System.Collections.Generic;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning CombatUnit class is still in development and is not recommended for production.
    public class CombatUnit
    {
        public CombatUnit(string n, string reg, CLASSIFICATION t, string dname, int number, int i)
        {
            name = n; regnum = reg; type = t; design_name = dname; design = null;
            count = number; iff = i; leader = false; dead_count = 0; available = number;
            carrier = null; plan_value = 0; launch_time = -1e6; jump_time = 0;
            sustained_damage = 0; target = null; group = null; heading = 0;
        }

        public CombatUnit(CombatUnit u)
        {
            name = u.name;
            regnum = u.regnum;
            type = u.type;
            design_name = u.design_name;
            design = u.design; count = u.count; iff = u.iff;
            dead_count = u.dead_count; available = u.available;
            leader = u.leader; region = u.region; location = u.location;
            carrier = u.carrier; plan_value = 0; launch_time = u.launch_time;
            jump_time = u.jump_time; sustained_damage = u.sustained_damage;
            target = null; group = null; heading = u.heading;
        }

        // public int operator ==(CombatUnit& u) { return this == &u; }

        public string GetDescription()
        {
            if (design != null)
            {
                this.GetDesign();
            }

            string desc;

            if (design == null)
            {
                desc = ContentBundle.Instance.GetText("[unknown]");
            }

            else if (count > 1)
            {
                desc = string.Format("{0:X} {1} {2}", LiveCount(), design.abrv, design.DisplayName());
            }

            else
            {
                if (!string.IsNullOrEmpty(regnum))
                    desc = string.Format("{0}-{1} {2}", design.abrv, regnum, name);
                else
                    desc = string.Format("{0} {1}", design.abrv, name);

                if (dead_count > 0)
                {
                    desc += " ";
                    desc += ContentBundle.Instance.GetText("killed.in.action");
                }
            }

            return desc;
        }

        public int GetValue()
        {
            return GetSingleValue() * LiveCount();
        }

        public int GetSingleValue()
        {
            return Ship.Value(GetShipClass());
        }

        public bool CanDefend(CombatUnit unit)
        {
            if (unit == null || unit == this)
                return false;

            if (type > CLASSIFICATION.STATION)
                return false;

            double distance = (location - unit.location).Length;

            if (type > unit.type)
                return false;

            if (distance > MaxRange())
                return false;

            return true;
        }

        public bool CanLaunch()
        {
            bool result = false;

            switch (type)
            {
                case CLASSIFICATION.FIGHTER:
                case CLASSIFICATION.ATTACK:
                    result = (Campaign.Stardate() - launch_time) >= 300;
                    break;

                case CLASSIFICATION.CORVETTE:
                case CLASSIFICATION.FRIGATE:
                case CLASSIFICATION.DESTROYER:
                case CLASSIFICATION.CRUISER:
                case CLASSIFICATION.CARRIER:
                    result = true;
                    break;
            }

            return result;
        }
        public double PowerVersus(CombatUnit tgt)
        {
            if (tgt == null || tgt == this || available < 1)
                return 0;

            if (type > CLASSIFICATION.STATION)
                return 0;

            double effectiveness = 1;
            double distance = (location - tgt.location).Length;

            if (distance > MaxRange())
                return 0;

            if (distance > MaxEffectiveRange())
                effectiveness = 0.5;

            if (type == CLASSIFICATION.FIGHTER)
            {
                if (tgt.type == CLASSIFICATION.FIGHTER || tgt.type == CLASSIFICATION.ATTACK)
                    return (uint)CLASSIFICATION.FIGHTER * 2 * available * effectiveness;
                else
                    return 0;
            }
            else if (type == CLASSIFICATION.ATTACK)
            {
                if (tgt.type > CLASSIFICATION.ATTACK)
                    return (uint)CLASSIFICATION.ATTACK * 3 * available * effectiveness;
                else
                    return 0;
            }
            else if (type == CLASSIFICATION.CARRIER)
            {
                return 0;
            }
            else if (type == CLASSIFICATION.SWACS)
            {
                return 0;
            }
            else if (type == CLASSIFICATION.CRUISER)
            {
                if (tgt.type <= CLASSIFICATION.ATTACK)
                    return (uint)type * effectiveness;

                else
                    return 0;
            }
            else
            {
                if (tgt.type > CLASSIFICATION.ATTACK)
                    return (uint)type * effectiveness;
                else
                    return (uint)type * 0.1 * effectiveness;
            }
        }

        public int AssignMission()
        {
            int assign = count;

            if (count > 4)
                assign = 4;

            if (assign > 0)
            {
                available -= assign;
                launch_time = Campaign.Stardate();
                return assign;
            }

            return 0;
        }
        public void CompleteMission()
        {
            Disengage();

            if (count > 4)
                available += 4;

            else
                available += count;
        }

        public double MaxRange()
        {
            return 100e3;
        }

        public double MaxEffectiveRange()
        {
            return 50e3;
        }

        public double OptimumRange()
        {
            if (type == CLASSIFICATION.FIGHTER || type == CLASSIFICATION.ATTACK)
                return 15e3;

            return 30e3;
        }

        public void Engage(CombatUnit tgt)
        {
            if (tgt == null)
                Disengage();

            else if (!tgt.attackers.Contains(this))
                tgt.attackers.Add(this);

            target = tgt;
        }
        public void Disengage()
        {
            if (target != null)
                target.attackers.Remove(this);

            target = null;
        }

        // accessors and mutators:
        public string Name() { return name; }
        public string Registry() { return regnum; }
        public string DesignName() { return design_name; }
        public string Skin() { return skin; }
        public void SetSkin(string s) { skin = s; }
        public CLASSIFICATION Type() { return type; }
        public int Count() { return count; }
        public int LiveCount() { return count - dead_count; }
        public int DeadCount() { return dead_count; }
        public void SetDeadCount(int n) { dead_count = n; }

        public int Kill(int n)
        {
            int killed = n;

            if (killed > LiveCount())
                killed = LiveCount();

            dead_count += killed;

            int value_killed = killed * GetSingleValue();

            if (killed != 0)
            {
                // if unit could support children, kill them too:
                if (type == CLASSIFICATION.CARRIER ||
                        type == CLASSIFICATION.STATION ||
                        type == CLASSIFICATION.STARBASE)
                {

                    if (group != null)
                    {
                        foreach (CombatGroup g in group.GetComponents())
                        {
                            value_killed += KillGroup(g);
                        }
                    }
                }
            }

            return value_killed;
        }
        public int Available() { return available; }
        public int GetIFF() { return iff; }
        public bool IsLeader() { return leader; }
        public void SetLeader(bool l) { leader = l; }
        public Point Location() { return location; }
        public void MoveTo(Point loc)
        {
            if (carrier == null)
                location = loc;
            else
                location = carrier.location;
        }
        public string GetRegion() { return region; }
        public void SetRegion(string rgn) { region = rgn; }
        public CombatGroup GetCombatGroup() { return group; }
        public void SetCombatGroup(CombatGroup g) { group = g; }

        public Color MarkerColor()
        {
            return Ship.IFFColor(iff);
        }

        public bool IsGroundUnit()
        {
            return (design != null && (((uint)design.type & (uint)CLASSIFICATION.GROUND_UNITS) != 0)) ? true : false;
        }

        public bool IsStarship()
        {
            return (design != null && (((uint)design.type & (uint)CLASSIFICATION.STARSHIPS) != 0)) ? true : false;
        }

        public bool IsDropship()
        {
            return (design != null && (((uint)design.type & (uint)CLASSIFICATION.DROPSHIPS) != 0)) ? true : false;
        }

        public bool IsStatic()
        {
            return design != null && (design.type >= CLASSIFICATION.STATION);
        }

        public CombatUnit GetCarrier() { return carrier; }
        public void SetCarrier(CombatUnit c) { carrier = c; }

        public ShipDesign GetDesign()
        {
            if (design == null)
                design = ShipDesign.Get(design_name);

            return design;
        }

        public CLASSIFICATION GetShipClass()
        {
            if (design != null)
                return design.type;

            return type;
        }

        public List<CombatUnit> GetAttackers() { return attackers; }

        public double GetPlanValue() { return plan_value; }
        public void SetPlanValue(int v) { plan_value = v; }

        public double GetSustainedDamage() { return sustained_damage; }
        public void SetSustainedDamage(double d) { sustained_damage = d; }

        public double GetHeading() { return heading; }
        public void SetHeading(double d) { heading = d; }

        public double GetNextJumpTime() { return jump_time; }

        private static int KillGroup(CombatGroup group)
        {
            int value_killed = 0;

            if (group != null)
            {
                foreach (CombatUnit u in group.GetUnits())
                {
                    value_killed += u.Kill(u.LiveCount());
                }

                foreach (CombatGroup g in group.GetComponents())
                {
                    value_killed += KillGroup(g);
                }
            }

            return value_killed;
        }

        private string name;
        private string regnum;
        private string design_name;
        private string skin;
        private CLASSIFICATION type;
        private ShipDesign design;
        private int count;
        private int dead_count;
        private int available;
        private int iff;
        private bool leader;
        private string region;
        private Point location;
        private double plan_value; // scratch pad for plan modules
        private double launch_time;
        private double jump_time;
        private double sustained_damage;
        private double heading;

        private CombatUnit carrier;
        private List<CombatUnit> attackers;
        private CombatUnit target;
        private CombatGroup group;
    }

    [Serializable]
    public class CombatUnitInfo
    {
        public string name;
        public string regnum;
        public string region;
        public Point loc;
        public string type;
        public string design;
        public string skin;
        public int count;
        public int dead_count;
        public int damage;
        public double heading;

        // User-defined conversion from CombatUnitInfo to CombatUnit
        public static implicit operator CombatUnit(CombatUnitInfo cui)
        {
            throw new NotImplementedException();
        }

        //  User-defined conversion from CombatUnit to CombatUnitInfo
        public static implicit operator CombatUnitInfo(CombatUnit cu)
        {
            throw new NotImplementedException();
        }
    }

}