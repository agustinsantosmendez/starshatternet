﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      QuitView.h/QuitView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    View class for End Mission menu
*/
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Views;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Simulator;
using System;
using UnityEngine;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Dialogs;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars.Views
{
    public class QuitView : View
    {
        public QuitView(Window c, string name) : base(c, name)
        {
            mouse_latch = false;
            quit_view = this;
            sim = Sim.GetSim();

            width = window.Width();
            height = window.Height();
            xcenter = width / 2;
            ycenter = height / 2;

            mouse_con = MouseController.GetInstance();
        }
        //public virtual ~QuitView();

        // Operations:
        public override void Refresh()
        {
            if (show_menu && menu_bmp != null)
            {
                Rect clip_rect = new Rect();

                clip_rect.x = xcenter - w2;
                clip_rect.y = ycenter - h2;
                clip_rect.w = w2 * 2;
                clip_rect.h = h2 * 2;

                window.ClipBitmap(xcenter - w2,
                                    ycenter - h2,
                                    xcenter - w2 + menu_bmp.Width(),
                                    ycenter - h2 + menu_bmp.Height(),
                                    menu_bmp,
                                    Color.white,
                                    Video.BLEND_TYPE.BLEND_SOLID,
                                    clip_rect);
             }
        }

        public override void OnWindowMove()
        {
            width = window.Width();
            height = window.Height();
            xcenter = width / 2;
            ycenter = height / 2;
        }

        public virtual void ExecFrame()
        {
            sim = Sim.GetSim();

            if (show_menu)
            {
                ColorExtensions.SetFade(1, Color.black, 0);
                int action = 0;

                if (Mouse.LButton() != 0)
                {
                    mouse_latch = true;
                }
                else if (mouse_latch)
                {
                    mouse_latch = false;

                    if (Mouse.X() > xcenter - w2 && Mouse.X() < xcenter + w2)
                    {
                        int y0 = ycenter - h2;

                        for (int i = 0; i < 4; i++)
                            if (Mouse.Y() >= y0 + 75 + i * 30 && Mouse.Y() <= y0 + 105 + i * 30)
                                action = i + 1;
                    }
                }

                for (int i = 1; i <= 4; i++)
                {
                    if (Keyboard.KeyDown('0' + i))
                        action = i;
                }

                // was mission long enough to accept?
                if (action == 1 && !CanAccept())
                {
                    Button.PlaySound(Button.SOUNDS.SND_REJECT);
                    action = 3;
                }

                // exit and accept:
                if (action == 1)
                {
                    CloseMenu();
                    Game.SetTimeCompression(1);

                    Starshatter stars = Starshatter.GetInstance();
                    stars.SetGameMode(Starshatter.MODE.PLAN_MODE);
                }

                // quit and discard results:
                else if (action == 2)
                {
                    CloseMenu();
                    Game.SetTimeCompression(1);

                    Starshatter stars = Starshatter.GetInstance();
                    Campaign campaign = Campaign.GetCampaign();

                    // discard mission and events:
                    if (sim != null) sim.UnloadMission();
                    else ShipStats.Initialize();

                    if (campaign != null && campaign.GetCampaignId() < (int)Campaign.CONSTANTS.SINGLE_MISSIONS)
                    {
                        campaign.RollbackMission();
                        stars.SetGameMode(Starshatter.MODE.CMPN_MODE);
                    }

                    else
                    {
                        stars.SetGameMode(Starshatter.MODE.MENU_MODE);
                    }
                }

                // resume:
                else if (action == 3)
                {
                    CloseMenu();
                }

                // controls:
                else if (action == 4)
                {
                    GameScreen game_screen = GameScreen.GetInstance();

                    if (game_screen != null)
                        game_screen.ShowCtlDlg();
                    else
                        CloseMenu();
                }
            }
        }


        public virtual bool CanAccept()
        {
            sim = Sim.GetSim();

            if (sim == null || sim.IsNetGame())
                return true;

            Ship player_ship = sim.GetPlayerShip();

            if (player_ship.MissionClock() < 60000)
            {
                RadioView.Message(Game.GetText("QuitView.too-soon"));
                RadioView.Message(Game.GetText("QuitView.abort"));
                return false;
            }

            foreach (Contact c in player_ship.ContactList())
            {
                Ship cship = c.GetShip();
                int ciff = c.GetIFF(player_ship);

                if (c.Threat(player_ship))
                {
                    RadioView.Message(Game.GetText("QuitView.threats-present"));
                    RadioView.Message(Game.GetText("QuitView.abort"));
                    return false;
                }

                else if (cship != null && ciff > 0 && ciff != player_ship.GetIFF())
                {
                    Point delta = c.Location() - player_ship.Location();
                    double dist = delta.Length;

                    if (cship.IsDropship() && dist < 50e3)
                    {
                        RadioView.Message(Game.GetText("QuitView.threats-present"));
                        RadioView.Message(Game.GetText("QuitView.abort"));
                        return false;
                    }

                    else if (cship.IsStarship() && dist < 100e3)
                    {
                        RadioView.Message(Game.GetText("QuitView.threats-present"));
                        RadioView.Message(Game.GetText("QuitView.abort"));
                        return false;
                    }
                }
            }

            return true;
        }
        public virtual bool IsMenuShown()
        {
            return show_menu;
        }

        public virtual void ShowMenu()
        {
            if (!show_menu)
            {
                show_menu = true;

                for (int i = 0; i < 10; i++)
                {
                    if (Keyboard.KeyDown('1' + i))
                    {
                        // just need to clear the key down flag
                        // so we don't process old keystrokes
                        // as valid menu picks...
                    }
                }

                Button.PlaySound(Button.SOUNDS.SND_CONFIRM);
                Starshatter.GetInstance().Pause(true);

                if (mouse_con != null)
                {
                    mouse_active = mouse_con.Active();
                    mouse_con.SetActive(false);
                }
            }
        }
        public virtual void CloseMenu()
        {
            show_menu = false;
            Starshatter.GetInstance().Pause(false);

            if (mouse_con != null)
                mouse_con.SetActive(mouse_active);
        }

        public static void Initialize()
        {
            if (menu_bmp == null)
            {
                string path = "Screens/";
                menu_bmp = new Bitmap(path + "QuitWin ", Bitmap.BMP_TYPES.BMP_TRANSPARENT);
            }
        }
        public static void Close()
        {
            show_menu = false;
            Starshatter.GetInstance().Pause(false);

            if (mouse_con != null)
                mouse_con.SetActive(mouse_active);
        }


        public static QuitView GetInstance() { return quit_view; }


        protected int width, height;
        protected int xcenter, ycenter;
        protected bool mouse_latch;

        protected Sim sim;

        protected static QuitView quit_view;

        static bool show_menu = false;
        static Bitmap menu_bmp = null;
        static MouseController mouse_con = null;
        static bool mouse_active = false;
        const int w2 = 200;
        const int h2 = 128;

    }
}