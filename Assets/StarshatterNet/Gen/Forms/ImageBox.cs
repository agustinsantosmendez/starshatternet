﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      ImageBox.h/ImageBox.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    ImageBox class
*/
using StarshatterNet.Stars.Views;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.Int32;

namespace StarshatterNet.Gen.Forms
{
    public class ImageBox : ActiveWindow
    {
        public ImageBox(ActiveWindow p, int ax, int ay, int aw, int ah, DWORD aid) : base(p.GetScreen(), ax, ay, aw, ah, aid, 0, p)
        {
            picture_loc = 1;
            text_align = TextFormat.DT_CENTER;

            desc = "ImageBox " + id;

        }

        public void GetPicture(ref Texture2D img)
        {
            img = picture;
        }

        public void SetPicture(Texture2D img)
        {
            picture = img;
            //picture.AutoMask();
            //picture.MakeTexture();

            SetPicture();
        }
        protected void SetPicture()
        {
            if (obj == null) return;
            if (picture == null)
            {
                Color ic = image.color;
                ic.a = 0;
                image.color = ic;
            }
            else
            {
                //Set the Sprite of the Image Component on the new GameObject
                Insets c = this.GetMargins();
                image.type = Image.Type.Sliced;
                Vector4 border = new Vector4(c.left, c.bottom, c.right, c.top); //X=left, Y=bottom, Z=right, W=top.
                Sprite s = Sprite.Create(picture, new Rect(0, 0, picture.width, picture.height), new Vector2(.5f, .5f), 100f, 0, SpriteMeshType.FullRect, border);
                s.name = picture.name;
                image.sprite = s;
            }
        }

        protected internal override void BuildGamebjects()
        {
            base.BuildGamebjects();

            obj = new GameObject(); //Create the GameObject
            image = obj.AddComponent<Image>(); //Add the Image Component script
            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            rectTransform.SetParent(this.parentObj.transform); //Assign the newly created Image GameObject as a Child of the Parent Panel.
            obj.name = desc;
            obj.SetActive(true); //Activate the GameObject

            SetPicture();

            // update children windows:
            foreach (var child in children)
            {
                //child.parentObj = obj;
                child.BuildGamebjects();
            }
        }
        protected Image image;
        protected Texture2D picture;
        protected int picture_loc; // 0 to 8
    }

}
