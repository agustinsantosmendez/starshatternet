﻿using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

public class FormDefTesting : MonoBehaviour
{
    public Camera cameraObj;
    public DlgChoices ChoiceIndex = DlgChoices.TestingDlg;

    private Game game;
    private int width, height;
    public enum DlgChoices
    {
        TestingDlg,
        AudDlg,
        AwardDlg,
        CmdDlg,
        CmdForceDlg,
        CmdIntelDlg,
        CmdMissionsDlg,
        CmdMsgDlg,
        CmdOrdersDlg,
        CmdTheaterDlg,
        CmpCompleteDlg,
        CmpLoadDlg,
        CmpSceneDlg,
        CmpSelectDlg,
        ConfirmDlg,
        CtlDlg,
        DebriefDlg,
        DeckDlg,
        EngDlg,
        ExitDlg,
        FileDlg,
        FirstTimeDlg,
        FltDlg,
        HelmDetail,
        JoyDlg,
        KeyDlg,
        LoadDlg,
        MenuDlg,
        ModConfigDlg,
        ModDlg,
        ModInfoDlg,
        MsgDlg,
        MsnEditDlg,
        MsnEditNavDlg,
        MsnElemDlg,
        MsnEventDlg,
        MsnNavDlg,
        MsnObjDlg,
        MsnPkgDlg,
        MsnSelectDlg,
        MsnWepDlg,
        NavDlg,
        NetAddrDlg,
        NetClientDlg,
        NetLobbyDlg,
        NetPassDlg,
        NetServerDlg,
        NetUnitDlg,
        OptDlg,
        PlanDlg,
        PlayerDlg,
        TacRefDlg,
        VidDlg,
        WepDlg
    }
    void Awake()
    {
        game = Game.GetInstance();
        game.Init(cameraObj, this.gameObject);
        Button.Initialize();

        // Display
        Video video = Video.GetVideo();
        // Canvas
        Screen screen = new Screen(video);

        width = screen.Width(); //1920
        height = screen.Height(); //1080

        FormWindow form = new FormWindow(screen, 0, 0, width, height, cameraObj, this.gameObject, "Main", 0, 0);
        FormDef formdef = new FormDef();

        string dlg = ChoiceIndex.ToString();

        formdef.Load(dlg);
        form.Init(formdef);
        switch (ChoiceIndex)
        {
            //case DlgChoices.CmpSelectDlg:
            //    //case DlgChoices.TestingDlg:
            //    var lb = form.FindControl(201) as ListBox;
            //    if (lb != null)
            //        lb.testitems();
            //    break;
            case DlgChoices.ExitDlg:
                var rtb = form.FindControl(201) as RichTextBox;
                if (rtb != null)
                {
                    var block = Resources.Load<TextAsset>("credits");
                    if (block != null && !string.IsNullOrWhiteSpace(block.text))
                    {
                        rtb.SetText(block.text);
                    }
                }
                break;
        }


    }

    // Start is called before the first frame update
    void Start()
    {
        game.Run();
        game.Activate(true);
    }

    // Update is called once per frame
    void Update()
    {
        //game.GameLoop();

        if (Input.GetKeyDown(KeyCode.I))
            game.ScreenCapture();
    }
}
