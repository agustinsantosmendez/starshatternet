﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars.exe
    ORIGINAL FILE:      Debris.h/Debris.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Debris Sprite class
*/
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.StarSystems;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars
{
#warning Debris class is still in development and is not recommended for production.
    public class Debris : SimObject
    {
        public Debris(Model model, Point pos, Vector3D vel, double m) :
            base("Debris", SimObject.TYPES.SIM_DEBRIS)
        {
            MoveTo(pos);

            velocity = vel;
            mass = (float)m;
            integrity = mass * 10.0f;
            life = 300;

            Solid solid = new Solid();

            if (solid != null)
            {
                solid.UseModel(model);
                solid.MoveTo(pos);

                rep = solid;

                radius = solid.Radius();
            }

            Point torque = RandomHelper.RandomVector(Mass() / 2);

            if (Mass() < 10)
                torque *= (RandomHelper.Rand() / 3200);
            else if (Mass() > 10e3)
                torque *= 0.25f;
            else if (Mass() > 10e6)
                torque *= 0.005f;

            ApplyTorque(torque);
        }

        public void SetLife(int seconds) { life = seconds; }
        public virtual int HitBy(Shot shot, out Point impact)
        {
            impact = new Point();
            if (!shot.IsArmed()) return 0;

            const int HIT_NOTHING = 0;
            const int HIT_HULL = 1;

            Point hull_impact;
            int hit_type = HIT_NOTHING;
            //bool hit_hull = true;
            Point shot_loc = shot.Location();
            Point delta = shot_loc - Location();
            double dlen = delta.Length;
            double dscale = 1;
            float scale = 1.0f;
            Sim sim = Sim.GetSim();

            // MISSILE PROCESSING ------------------------------------------------

            if (shot.IsMissile())
            {
                if (dlen < Radius())
                {
                    hull_impact = impact = shot_loc;
                    sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.HULL_FLASH, 0.3f * scale, scale, region, this);
                    sim.CreateExplosion(impact, new Point(), Explosion.ExplosionType.SHOT_BLAST, 2.0f, scale, region);
                    hit_type = HIT_HULL;
                }
            }

            // ENERGY WEP PROCESSING ---------------------------------------------

            else
            {

                Solid solid = (Solid)rep;

                shot_loc = shot.Location();
                Point shot_vpn = shot_loc - shot.Origin();
                double shot_len = shot_vpn.Length;
                 shot_vpn.Normalize();
                if (shot_len == 0) shot_len = 1000;

                // impact:
                if (solid != null)
                {
                    if (solid.CheckRayIntersection(shot.Origin(), shot_vpn, shot_len, impact))
                    {
                        // trim beam shots to impact point:
                        if (shot.IsBeam())
                            shot.SetBeamPoints(shot.Origin(), impact);

                        hull_impact = impact;

                        if (shot.IsBeam())
                            sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.BEAM_FLASH, 0.30f * scale, scale, region, this);
                        else
                            sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.HULL_FLASH, 0.30f * scale, scale, region, this);

                        Point burst_vel = hull_impact - Location();
                        burst_vel.Normalize();
                        burst_vel *= (float)(Radius() * 0.5);
                        burst_vel += Velocity();

                        sim.CreateExplosion(hull_impact, burst_vel, Explosion.ExplosionType.HULL_BURST, 0.50f * scale, scale, region, this);

                        hit_type = HIT_HULL;
                        //hit_hull = true;
                    }
                }

                else
                {
                    if (dlen < Radius())
                    {
                        hull_impact = impact = shot_loc;

                        if (shot.IsBeam())
                            sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.BEAM_FLASH, 0.30f * scale, scale, region, this);
                        else
                            sim.CreateExplosion(impact, Velocity(), Explosion.ExplosionType.HULL_FLASH, 0.30f * scale, scale, region, this);

                        hit_type = HIT_HULL;
                    }
                }
            }

            // DAMAGE RESOLUTION -------------------------------------------------

            if (hit_type != HIT_NOTHING)
            {
                double effective_damage = shot.Damage() * dscale;

                if (shot.IsBeam())
                {
                    effective_damage *= SimulationTime.FrameTime();
                }
                else
                {
                    ApplyTorque(shot.Velocity() * (float)effective_damage * 1e-6f);
                }

                if (effective_damage > 0)
                    base.InflictDamage(effective_damage);
            }

            return hit_type;
        }

        public override void ExecFrame(double seconds)
        {
            if (GetRegion().Type() == SimRegion.TYPES.AIR_SPACE)
            {
                if (AltitudeAGL() < Radius())
                {
                    velocity = new Point();
                    arcade_velocity = new Point();

                    Terrain terrain = region.GetTerrain();

                    if (terrain != null)
                    {
                        Point loc = Location();
                        MoveTo(new Point(loc.X, terrain.Height(loc.X, loc.Z), loc.Z));
                    }
                }
                else
                {
                    if (mass > 100)
                    {
                        Orbital primary = GetRegion().GetOrbitalRegion().Primary();

                        const double GRAV = 6.673e-11;
                        double m0 = primary.Mass();
                        double r = primary.Radius();

                        SetDrag(0.001);
                        SetGravity(6 * GRAV * m0 / (r * r));  // accentuate gravity
                        SetBaseDensity(1.0f);
                    }

                    AeroFrame(seconds);
                }
            }
            else
            {
                base.ExecFrame(seconds);
            }
        }
        public virtual double AltitudeAGL()
        {
            Point loc = Location();
            double altitude_agl = loc.Y;

            Terrain terrain = region.GetTerrain();

            if (terrain != null)
                altitude_agl -= terrain.Height(loc.X, loc.Z);

            if (double.IsInfinity(altitude_agl))
                altitude_agl = 0;

            return altitude_agl;
        }
    }
}