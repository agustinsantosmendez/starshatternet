﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MapView.h/MapView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Star Map class
*/
using System;
using System.Collections.Generic;
using System.Linq;
using StarshatterNet.Controllers;
using StarshatterNet.Gen;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Dialogs;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;
using DWORD = System.Int32;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using POINT = DigitalRune.Mathematics.Algebra.Vector2D;

namespace StarshatterNet.Stars.Views
{
    public class MapView : View, IEventTarget, ISimObserver
    {

        public MapView(Window c, string name = "MapView") : base(c, name)
        {
            system = null; zoom = 1.1; offset_x = 0; offset_y = 0; ship = null; campaign = null;
            captured = false; dragging = false; adding_navpt = false;
            moving_navpt = false; moving_elem = false;
            view_mode = VIEW_SYSTEM; seln_mode = SELECT_REGION; ship_filter = (SimElements.CLASSIFICATION)0xffffffff;
            current_star = 0; current_planet = 0; current_region = 0;
            current_ship = null; current_elem = null; current_navpt = null; mission = null;
            scrolling = 0; scroll_x = 0; scroll_y = 0; click_x = 0; click_y = 0;
            active_menu = null; map_menu = null; map_system_menu = null; map_sector_menu = null;
            ship_menu = null; editor = false;
            nav_menu = null; action_menu = null; objective_menu = null; formation_menu = null; speed_menu = null;

            for (int i = 0; i < 3; i++)
            {
                view_zoom[i] = zoom;
                view_offset_x[i] = offset_x;
                view_offset_y[i] = offset_y;
            }

            menu_view = new MenuView(c, "MenuView");

            title_font = FontMgr.Find("Limerick12");
            font = FontMgr.Find("Verdana");

            active_window = (ActiveWindow)window;

            if (active_window != null)
                active_window.AddView(this);
        }
        // public virtual ~MapView();

        // Operations:
        public override void Refresh()
        {
            rect = windowObj.GetRect();

            if (system == null)
            {
                DrawGrid();
                DrawTabbedText(title_font, Game.GetText("MapView.item.no-system"));
                return;
            }

            if (font != null)
                font.SetColor(Color.white);

            if (scrolling != 0)
            {
                offset_x -= scroll_x;
                offset_y -= scroll_y;
                scrolling--;
            }

            rect.w -= 2;
            rect.h -= 2;

            switch (view_mode)
            {
                case VIEW_GALAXY: DrawGalaxy(); break;
                case VIEW_SYSTEM: DrawSystem(); break;
                case VIEW_REGION: DrawRegion(); break;
                default: DrawGrid(); break;
            }

            rect.w += 2;
            rect.h += 2;

            if (menu_view != null)
            {
                if (current_navpt != null)
                {
                    active_menu = nav_menu;
                }
                else if (current_ship != null)
                {
                    if (current_ship.GetIFF() == ship.GetIFF())
                        active_menu = ship_menu;
                    else
                        active_menu = map_menu;
                }
                else if (current_elem != null)
                {
                    if (editor || current_elem.GetIFF() == mission.Team())
                        active_menu = ship_menu;
                    else
                        active_menu = map_menu;
                }
                else
                {
                    active_menu = map_menu;
                }

                menu_view.SetBackColor(Color.gray);
                menu_view.SetTextColor(Color.white);
                menu_view.SetMenu(active_menu);
                menu_view.DoMouseFrame();

                if (menu_view.GetAction() != 0)
                {
                    ProcessMenuItem(menu_view.GetAction());
                }

                menu_view.Refresh();
            }

            DrawTitle();
        }

        public override void OnWindowMove()
        {
            if (menu_view != null)
                menu_view.OnWindowMove();
        }


        public override void OnShow()
        {
            EventDispatch dispatch = EventDispatch.GetInstance();
            if (dispatch != null)
                dispatch.Register(this);
        }

        public override void OnHide()
        {
            EventDispatch dispatch = EventDispatch.GetInstance();
            if (dispatch != null)
                dispatch.Unregister(this);

            if (captured)
            {
                ReleaseCapture();
                captured = false;
                Mouse.Show(true);
            }

            dragging = false;
        }


        public virtual void DrawTitle()
        {
            title_font.SetColor(active_window.GetForeColor());
            DrawTabbedText(title_font, title);
        }
        public virtual void DrawGalaxy()
        {
            title = Game.GetText("MapView.title.Galaxy");
            DrawGrid();

            double cx = rect.w / 2;
            double cy = rect.h / 2;

            c = (cx > cy) ? cx : cy;
            r = 10; // * zoom;

            Galaxy g = Galaxy.GetInstance();
            if (g != null)
                r = g.Radius(); // * zoom;

            double scale = c / r;
            double ox = 0;
            double oy = 0;

            // compute offset:
            foreach (StarSystem s in system_list)
            {
                if (system == s)
                {
                    if (Math.Abs(s.Location().X) > 10 || Math.Abs(s.Location().Y) > 10)
                    {
                        int sx = (int)s.Location().X;
                        int sy = (int)s.Location().Y;

                        sx -= sx % 10;
                        sy -= sy % 10;

                        ox = sx * -scale;
                        oy = sy * -scale;
                    }
                }
            }

            // draw the list of systems, and their connections:
            foreach (StarSystem s in system_list)
            {

                int sx = (int)(cx + ox + s.Location().X * scale);
                int sy = (int)(cy + oy + s.Location().Y * scale);

                if (sx < 4 || sx > rect.w - 4 || sy < 4 || sy > rect.h - 4)
                    continue;

                windowObj.DrawEllipse(sx - 7, sy - 7, sx + 7, sy + 7, Ship.IFFColor(s.Affiliation()));

                if (s == system)
                {
                    windowObj.DrawLine(0, sy, rect.w, sy, Color.gray, Video.BLEND_TYPE.BLEND_ADDITIVE);
                    windowObj.DrawLine(sx, 0, sx, rect.h, Color.gray, Video.BLEND_TYPE.BLEND_ADDITIVE);
                }

                foreach (StarSystem s2 in system_list)
                {
                    if (s != s2 && s.HasLinkTo(s2))
                    {
                        int ax = sx;
                        int ay = sy;

                        int bx = (int)(cx + ox + s2.Location().X * scale);
                        int by = (int)(cy + oy + s2.Location().Y * scale);

                        if (ax == bx)
                        {
                            if (ay < by)
                            {
                                ay += 8; by -= 8;
                            }
                            else
                            {
                                ay -= 8; by += 8;
                            }
                        }

                        else if (ay == by)
                        {
                            if (ax < bx)
                            {
                                ax += 8; bx -= 8;
                            }
                            else
                            {
                                ax -= 8; bx += 8;
                            }
                        }

                        else
                        {
                            Point d = new Point(bx, by, 0) - new Point(ax, ay, 0);
                            d.Normalize();

                            ax += (int)(8 * d.X);
                            ay += (int)(8 * d.Y);

                            bx -= (int)(8 * d.X);
                            by -= (int)(8 * d.Y);
                        }

                        windowObj.DrawLine(ax, ay, bx, by, new Color32(120, 120, 120, 255), Video.BLEND_TYPE.BLEND_ADDITIVE);
                    }
                }
            }

            // finally draw all the stars in the galaxy:
            if (g != null)
            {
                foreach (StarSystems.Star s in g.Stars())
                {
                    int sx = (int)(cx + ox + s.Location().X * scale);
                    int sy = (int)(cy + oy + s.Location().Y * scale);
                    int sr = s.GetSize();

                    if (sx < 4 || sx > rect.w - 4 || sy < 4 || sy > rect.h - 4)
                        continue;

                    windowObj.FillEllipse(sx - sr, sy - sr, sx + sr, sy + sr, s.GetColor());

                    if (s.Name() == "GSC")
                        font.SetColor(new Color32(100, 100, 100, 255));
                    else
                        font.SetColor(Color.white);

                    Rect name_rect = new Rect(sx - 60, sy + 8, 120, 20);
                    active_window.SetFont(font);
                    active_window.DrawText(s.Name(), 0, name_rect, TextFormat.DT_SINGLELINE | TextFormat.DT_CENTER);
                }
            }

            font.SetColor(Color.white);
        }

        public virtual void DrawSystem()
        {
            string caption = Game.GetText("MapView.title.Starsystem");
            caption += " ";
            caption += system.Name();

            if (current_ship != null)
            {
                caption += "\n";
                caption += Game.GetText("MapView.title.Ship");
                caption += " ";
                caption += current_ship.Name();
            }
            else if (current_elem != null)
            {
                caption += "\n";
                caption += Game.GetText("MapView.title.Ship");
                caption += " ";
                caption += current_elem.Name();
            }

            title = caption;

            foreach (OrbitalBody star in system.Bodies())
            {
                int p_orb = 1;

                foreach (OrbitalBody planet in star.Satellites())
                {
                    DrawOrbital(planet, p_orb++);

                    int m_orb = 1;

                    foreach (OrbitalBody moon in planet.Satellites())
                    {
                        DrawOrbital(moon, m_orb++);

                        foreach (OrbitalRegion regionm in moon.Regions())
                        {
                            DrawOrbital(regionm, 1);
                        }
                    }

                    foreach (OrbitalRegion regionp in planet.Regions())
                    {
                        DrawOrbital(regionp, 1);
                    }
                }

                foreach (OrbitalRegion regiosn in star.Regions())
                {
                    DrawOrbital(regiosn, 1);
                }

                DrawOrbital(star, 0);
            }

            string r_txt;
            FormatUtil.FormatNumber(out r_txt, system.Radius() * zoom);
            string resolution;
            resolution = string.Format("{0}: {1}", Game.GetText("MapView.info.Resolution"), r_txt);

            active_window.SetFont(font);
            active_window.DrawText(resolution, -1, new Rect(4, 4, rect.w - 8, 24), TextFormat.DT_SINGLELINE | TextFormat.DT_RIGHT);
        }

        public virtual void DrawRegion()
        {
            OrbitalRegion rgn = (OrbitalRegion)regions[current_region];

            string caption = Game.GetText("MapView.title.Sector");
            caption += " ";
            caption += rgn.Name();

            if (current_ship != null)
            {
                caption += "\n";
                caption += Game.GetText("MapView.title.Ship");
                caption += " ";
                caption += current_ship.Name();
            }
            else if (current_elem != null)
            {
                caption += "\n";
                caption += Game.GetText("MapView.title.Ship");
                caption += " ";
                caption += current_elem.Name();
            }

            title = caption;

            double cx = rect.w / 2;
            double cy = rect.h / 2;

            int size = (int)rgn.Radius();
            int step = (int)rgn.GridSpace();

            c = (cx < cy) ? cx : cy;
            r = rgn.Radius() * zoom;
            double scale = c / r;

            double ox = offset_x * scale;
            double oy = offset_y * scale;

            int left = (int)(-size * scale + ox + cx);
            int right = (int)(size * scale + ox + cx);
            int top = (int)(-size * scale + oy + cy);
            int bottom = (int)(size * scale + oy + cy);

            Color major = new Color(48, 48, 48);
            Color minor = new Color(24, 24, 24);

            int x, y;
            int tick = 0;

            for (x = 0; x <= size; x += step)
            {
                int lx = (int)(x * scale + ox + cx);
                if (tick == 0)
                    windowObj.DrawLine(lx, top, lx, bottom, major, Video.BLEND_TYPE.BLEND_ADDITIVE);
                else
                    windowObj.DrawLine(lx, top, lx, bottom, minor, Video.BLEND_TYPE.BLEND_ADDITIVE);

                lx = (int)(-x * scale + ox + cx);
                if (tick == 0)
                    windowObj.DrawLine(lx, top, lx, bottom, major, Video.BLEND_TYPE.BLEND_ADDITIVE);
                else
                    windowObj.DrawLine(lx, top, lx, bottom, minor, Video.BLEND_TYPE.BLEND_ADDITIVE);

                if (++tick > 3) tick = 0;
            }

            tick = 0;

            for (y = 0; y <= size; y += step)
            {
                int ly = (int)(y * scale + oy + cy);
                if (tick == 0)
                    windowObj.DrawLine(left, ly, right, ly, major, Video.BLEND_TYPE.BLEND_ADDITIVE);
                else
                    windowObj.DrawLine(left, ly, right, ly, minor, Video.BLEND_TYPE.BLEND_ADDITIVE);

                ly = (int)(-y * scale + oy + cy);
                if (tick == 0)
                    windowObj.DrawLine(left, ly, right, ly, major, Video.BLEND_TYPE.BLEND_ADDITIVE);
                else
                    windowObj.DrawLine(left, ly, right, ly, minor, Video.BLEND_TYPE.BLEND_ADDITIVE);

                if (++tick > 3) tick = 0;
            }

            int rep = 3;
            if (r > 70e3) rep = 2;
            if (r > 250e3) rep = 1;

            if (campaign != null && rgn != null)
            {
                // draw the combatants in this region:
                foreach (Combatant combatant in campaign.GetCombatants())
                {
                    DrawCombatGroup(combatant.GetForce(), rep);
                }
            }

            else if (mission != null && rgn != null)
            {
                // draw the elements in this region:
                foreach (MissionElement elem in mission.GetElements())
                    if (!elem.IsSquadron())
                        DrawElem(elem, (elem == current_elem), rep);
            }

            else if (ship != null)
            {
                // draw the ships in this region:
                Sim sim = Sim.GetSim();
                SimRegion simrgn = null;

                if (sim != null && rgn != null)
                    simrgn = sim.FindRegion(rgn.Name());

                // this algorithm uses the allied track list for the region,
                // even if this ship is in a different region.  a previous
                // version used the ship's own contact list, which only shows
                // ships in the player's region.  this way is less "realistic"
                // but more fun for managing large battles.

                if (simrgn != null)
                {
                    foreach (Contact contact in simrgn.TrackList(ship.GetIFF()))
                    {
                        Ship s = contact.GetShip();

                        if (s != null && ((s.Class() & ship_filter) != 0) && !IsClutter(s) && s != ship)
                            DrawShip(s, (s == current_ship), rep);
                    }

                    foreach (Ship s in simrgn.Ships())
                    {
                        if (s != null && (s.IsStatic()) && !IsClutter(s) &&
                                (s.GetIFF() == ship.GetIFF() || s.GetIFF() == 0))
                            DrawShip(s, (s == current_ship), rep);
                    }

                    // draw nav routes for allied ships not in the region:
                    foreach (SimRegion r in sim.GetRegions())
                    {
                        if (r != simrgn)
                        {
                            foreach (Ship s in r.Ships())
                            {
                                if (s != null && !s.IsStatic() && !IsClutter(s) &&
                                        (s.GetIFF() == ship.GetIFF() || s.GetIFF() == 0))
                                {
                                    DrawNavRoute(simrgn.GetOrbitalRegion(),
                                    s.GetFlightPlan(),
                                    s.MarkerColor(),
                                    s, null);
                                }
                            }
                        }
                    }
                }

                // draw our own ship:
                DrawShip(ship, (ship == current_ship), rep);
            }

            string r_txt ;
            FormatUtil.FormatNumber(out r_txt, r * 2);
            string resolution;
            resolution = string.Format("{0}: {1}", Game.GetText("MapView.info.Resolution"), r_txt);

            active_window.SetFont(font);
            active_window.DrawText(resolution, -1, new Rect(4, 4, rect.w - 8, 24), TextFormat.DT_SINGLELINE | TextFormat.DT_RIGHT);
        }


        public virtual void DrawGrid()
        {
            int grid_step = rect.w / 8;
            int cx = rect.w / 2;
            int cy = rect.h / 2;

            Color c = new Color(32, 32, 32);

            windowObj.DrawLine(0, cy, rect.w, cy, c, Video.BLEND_TYPE.BLEND_ADDITIVE);
            windowObj.DrawLine(cx, 0, cx, rect.h, c, Video.BLEND_TYPE.BLEND_ADDITIVE);

            for (int i = 1; i < 4; i++)
            {
                windowObj.DrawLine(0, cy + (i * grid_step), rect.w, cy + (i * grid_step), c, Video.BLEND_TYPE.BLEND_ADDITIVE);
                windowObj.DrawLine(0, cy - (i * grid_step), rect.w, cy - (i * grid_step), c, Video.BLEND_TYPE.BLEND_ADDITIVE);
                windowObj.DrawLine(cx + (i * grid_step), 0, cx + (i * grid_step), rect.h, c, Video.BLEND_TYPE.BLEND_ADDITIVE);
                windowObj.DrawLine(cx - (i * grid_step), 0, cx - (i * grid_step), rect.h, c, Video.BLEND_TYPE.BLEND_ADDITIVE);
            }
        }

        public virtual void DrawOrbital(Orbital body, int index)
        {
            Orbital.OrbitalType type = body.Type();

            if (type == Orbital.OrbitalType.NOTHING)
                return;

            int x1, y1, x2, y2;
            Rect label_rect = new Rect();
            int label_w = 64;
            int label_h = 18;

            double cx = rect.w / 2;
            double cy = rect.h / 2;

            c = (cx < cy) ? cx : cy;
            r = system.Radius() * zoom;

            if ((r > 300e9) && (type > Orbital.OrbitalType.PLANET))
                return;

            double xscale = cx / r;
            double yscale = cy / r * 0.75;

            double ox = offset_x * xscale;
            double oy = offset_y * yscale;

            double bo_x = body.Orbit() * xscale;
            double bo_y = body.Orbit() * yscale;
            double br = body.Radius() * yscale;
            double bx = body.Location().X * xscale;
            double by = body.Location().Y * yscale;

            double px = 0;
            double py = 0;

            if (body.Primary() != null)
            {
                double min_pr = GetMinRadius(body.Primary().Type());

                if (index != 0)
                {
                    if (min_pr < 4)
                        min_pr = 4;

                    min_pr *= (index + 1);
                }

                double min_x = min_pr * xscale / yscale;
                double min_y = min_pr;

                if (bo_x < min_x)
                    bo_x = min_x;

                if (bo_y < min_y)
                    bo_y = min_y;

                px = body.Primary().Location().X * xscale;
                py = body.Primary().Location().Y * yscale;
            }

            if (type == Orbital.OrbitalType.TERRAIN)
                bo_x = bo_y;

            int ipx = (int)(cx + px + ox);
            int ipy = (int)(cy + py + oy);
            int ibo_x = (int)bo_x;
            int ibo_y = (int)bo_y;

            x1 = ipx - ibo_x;
            y1 = ipy - ibo_y;
            x2 = ipx + ibo_x;
            y2 = ipy + ibo_y;

            if (type != Orbital.OrbitalType.TERRAIN)
            {
                double a = x2 - x1;
                double b = rect.w * 32;

                if (a < b)
                    windowObj.DrawEllipse(x1, y1, x2, y2, new Color32(64, 64, 64, 255), Video.BLEND_TYPE.BLEND_ADDITIVE);
            }

            // show body's location on possibly magnified orbit
            bx = px + bo_x * Math.Cos(body.Phase());
            by = py + bo_y * Math.Sin(body.Phase());

            double min_br = GetMinRadius(type);
            if (br < min_br) br = min_br;

            Color color = new Color();

            switch (type)
            {
                case Orbital.OrbitalType.STAR: color = new Color32(248, 248, 128, 255); break;
                case Orbital.OrbitalType.PLANET: color = new Color32(64, 64, 192, 255); break;
                case Orbital.OrbitalType.MOON: color = new Color32(32, 192, 96, 255); break;
                case Orbital.OrbitalType.REGION: color = new Color32(255, 255, 255, 255); break;
                case Orbital.OrbitalType.TERRAIN: color = new Color32(16, 128, 48, 255); break;
            }

            int icx = (int)(cx + bx + ox);
            int icy = (int)(cy + by + oy);
            int ibr = (int)br;

            x1 = icx - ibr;
            y1 = icy - ibr;
            x2 = icx + ibr;
            y2 = icy + ibr;

            if (type < Orbital.OrbitalType.REGION)
            {
                if (body.GetMapIcon().width > 64)
                {
                    Bitmap map_icon = new Bitmap(body.GetMapIcon());

                    if (type == Orbital.OrbitalType.STAR)
                        windowObj.DrawBitmap(x1, y1, x2, y2, map_icon, Video.BLEND_TYPE.BLEND_ADDITIVE);
                    else
                        windowObj.DrawBitmap(x1, y1, x2, y2, map_icon, Video.BLEND_TYPE.BLEND_ALPHA);
                }
                else
                {
                    windowObj.FillEllipse(x1, y1, x2, y2, color);
                }
            }
            else
            {
                windowObj.DrawRect(x1, y1, x2, y2, color);

                if (campaign != null)
                {
                    foreach (Combatant combatant in campaign.GetCombatants())
                    {
                        if (ibr >= 4 || combatant.GetIFF() == 1)
                            DrawCombatantSystem(combatant, body, icx, icy, ibr);
                    }
                }
            }

            if (type == Orbital.OrbitalType.STAR || bo_y > label_h)
            {
                label_rect.x = x1 - label_w + (int)br;
                label_rect.y = y1 - label_h;
                label_rect.w = label_w * 2;
                label_rect.h = label_h;

                active_window.SetFont(font);
                active_window.DrawText(body.Name(), -1, label_rect, TextFormat.DT_SINGLELINE | TextFormat.DT_CENTER);
            }
        }

        public virtual void DrawShip(Ship s, bool current = false, int rep = 3)
        {
            OrbitalRegion rgn = (OrbitalRegion)regions[current_region];
            if (rgn == null) return;

            int x1, y1, x2, y2;
            POINT shiploc = new POINT();
            Point sloc = s.Location().OtherHand();

            double cx = rect.w / 2;
            double cy = rect.h / 2;

            c = (cx < cy) ? cx : cy;
            r = rgn.Radius() * zoom;

            double scale = c / r;

            double ox = offset_x * scale;
            double oy = offset_y * scale;

            double rlx = 0;
            double rly = 0;

            int sprite_width = 10;

            windowObj.SetFont(font);

            // draw ship icon:
            if (rep != 0 && view_mode == VIEW_REGION && rgn == s.GetRegion().GetOrbitalRegion())
            {
                double sx = (sloc.X + rlx) * scale;
                double sy = (sloc.Y + rly) * scale;

                shiploc.X = (int)(cx + sx + ox);
                shiploc.Y = (int)(cy + sy + oy);

                bool ship_visible = shiploc.X >= 0 && shiploc.X < rect.w &&
                shiploc.Y >= 0 && shiploc.Y < rect.h;

                if (ship_visible)
                {
                    if (rep < 3)
                    {
                        windowObj.FillRect(shiploc.X - 2, shiploc.Y - 2, shiploc.X + 2, shiploc.Y + 2, s.MarkerColor());
                        sprite_width = 2;

                        if (s == ship || !IsCrowded(s))
                            windowObj.Print(shiploc.X - sprite_width, shiploc.Y + sprite_width + 2, s.Name());
                    }
                    else
                    {
                        Point heading = s.Heading().OtherHand();
                        heading.Z = 0;
                        heading.Normalize();

                        double theta = 0;

                        if (heading.Y > 0)
                            theta = Math.Acos(heading.X);
                        else
                            theta = -Math.Acos(heading.X);

                        const double THETA_SLICE = 4 / Math.PI;
                        const double THETA_OFFSET = Math.PI + THETA_SLICE / 2;

                        int sprite_index = (int)((theta + THETA_OFFSET) * THETA_SLICE);
                        int nsprites = s.Design().map_sprites.Count;

                        if (nsprites != 0)
                        {
                            if (sprite_index < 0 || sprite_index >= nsprites)
                                sprite_index = sprite_index % nsprites;

                            Bitmap map_sprite = s.Design().map_sprites[sprite_index];

                            Bitmap bmp = new Bitmap(map_sprite.Texture);
                            //bmp.CopyBitmap(map_sprite);
                            ColorizeBitmap(bmp, s.MarkerColor());
                            sprite_width = bmp.Width() / 2;
                            int h = bmp.Height() / 2;

                            windowObj.DrawBitmap(shiploc.X - sprite_width,
                            shiploc.Y - h,
                            shiploc.X + sprite_width,
                            shiploc.Y + h,
                             bmp,
                            Video.BLEND_TYPE.BLEND_ALPHA);
                        }

                        else
                        {
                            theta -= Math.PI / 2;

                            if (s.IsStatic())
                            {
                                windowObj.FillRect(shiploc.X - 6, shiploc.Y - 6, shiploc.X + 6, shiploc.Y + 6, s.MarkerColor());
                                windowObj.DrawRect(shiploc.X - 6, shiploc.Y - 6, shiploc.X + 6, shiploc.Y + 6, Color.white);
                            }
                            else if (s.IsStarship())
                            {
                                windowObj.FillRect(shiploc.X - 4, shiploc.Y - 4, shiploc.X + 4, shiploc.Y + 4, s.MarkerColor());
                                windowObj.DrawRect(shiploc.X - 4, shiploc.Y - 4, shiploc.X + 4, shiploc.Y + 4, Color.white);
                            }
                            else
                            {
                                windowObj.FillRect(shiploc.X - 3, shiploc.Y - 3, shiploc.X + 3, shiploc.Y + 3, s.MarkerColor());
                                windowObj.DrawRect(shiploc.X - 3, shiploc.Y - 3, shiploc.X + 3, shiploc.Y + 3, Color.white);
                            }
                        }

                        windowObj.Print(shiploc.X - sprite_width, shiploc.Y + sprite_width + 2, s.Name());
                    }
                }
            }

            // draw nav route:
            // draw current ship marker:
            if (current && s.GetRegion().Name() == regions[current_region].Name())
            {
                x1 = (int)(shiploc.X - sprite_width - 1);
                x2 = (int)(shiploc.X + sprite_width + 1);
                y1 = (int)(shiploc.Y - sprite_width - 1);
                y2 = (int)(shiploc.Y + sprite_width + 1);

                windowObj.DrawRect(x1, y1, x2, y2, Color.white);
            }

            // only see routes for your own team:
            if (s.GetIFF() == 0 || ship != null && s.GetIFF() == ship.GetIFF())
            {
                DrawNavRoute(rgn, s.GetFlightPlan(), s.MarkerColor(), s, null);
            }
        }


        public virtual void DrawElem(MissionElement s, bool current = false, int rep = 3)
        {
            if (mission == null) return;

            bool visible = editor ||
            s.GetIFF() == 0 ||
            s.GetIFF() == mission.Team() ||
            s.IntelLevel() > Intel.INTEL_TYPE.KNOWN;

            if (!visible) return;

            OrbitalRegion rgn = (OrbitalRegion)regions[current_region];

            if (rgn == null) return;

            int x1, y1, x2, y2;
            POINT shiploc = new POINT();

            double cx = rect.w / 2;
            double cy = rect.h / 2;

            c = (cx < cy) ? cx : cy;
            r = rgn.Radius() * zoom;

            double scale = c / r;

            double ox = offset_x * scale;
            double oy = offset_y * scale;

            double rlx = 0;
            double rly = 0;

            int sprite_width = 10;

            windowObj.SetFont(font);

            // draw ship icon:
            if (s.Region().Equals(rgn.Name(), StringComparison.InvariantCultureIgnoreCase))
            {
                double sx = (s.Location().X + rlx) * scale;
                double sy = (s.Location().Y + rly) * scale;

                shiploc.X = (int)(cx + sx + ox);
                shiploc.Y = (int)(cy + sy + oy);

                bool ship_visible = shiploc.X >= 0 && shiploc.X < rect.w &&
                shiploc.Y >= 0 && shiploc.Y < rect.h;

                if (ship_visible)
                {
                    if (rep < 3)
                    {
                        windowObj.FillRect(shiploc.X - 2, shiploc.Y - 2, shiploc.X + 2, shiploc.Y + 2, s.MarkerColor());
                        sprite_width = 2;

                        if (!IsCrowded(s))
                            windowObj.Print(shiploc.X - sprite_width, shiploc.Y + sprite_width + 2, s.Name());
                    }
                    else
                    {
                        double theta = s.Heading();

                        const double THETA_SLICE = 4 / Math.PI;
                        const double THETA_OFFSET = Math.PI / 2;

                        int sprite_index = (int)((theta + THETA_OFFSET) * THETA_SLICE);
                        int nsprites = 0;

                        if (s.GetDesign() != null)
                            nsprites = s.GetDesign().map_sprites.Count;

                        if (nsprites > 0)
                        {
                            if (sprite_index < 0 || sprite_index >= nsprites)
                                sprite_index = sprite_index % nsprites;

                            Bitmap map_sprite = s.GetDesign().map_sprites[sprite_index];

                            Bitmap bmp = new Bitmap(map_sprite.Texture);
                            //bmp.CopyBitmap(map_sprite);
                            ColorizeBitmap(bmp, s.MarkerColor());
                            sprite_width = bmp.Width() / 2;
                            int h = bmp.Height() / 2;

                            windowObj.DrawBitmap(shiploc.X - sprite_width,
                            shiploc.Y - h,
                            shiploc.X + sprite_width,
                            shiploc.Y + h,
                             bmp,
                            Video.BLEND_TYPE.BLEND_ALPHA);
                        }

                        else
                        {
                            theta -= Math.PI / 2;

                            if (s.IsStatic())
                            {
                                windowObj.FillRect(shiploc.X - 6, shiploc.Y - 6, shiploc.X + 6, shiploc.Y + 6, s.MarkerColor());
                                windowObj.DrawRect(shiploc.X - 6, shiploc.Y - 6, shiploc.X + 6, shiploc.Y + 6, Color.white);
                            }
                            else if (s.IsStarship())
                            {
                                windowObj.FillRect(shiploc.X - 4, shiploc.Y - 4, shiploc.X + 4, shiploc.Y + 4, s.MarkerColor());
                                windowObj.DrawRect(shiploc.X - 4, shiploc.Y - 4, shiploc.X + 4, shiploc.Y + 4, Color.white);
                            }
                            else
                            {
                                windowObj.FillRect(shiploc.X - 3, shiploc.Y - 3, shiploc.X + 3, shiploc.Y + 3, s.MarkerColor());
                                windowObj.DrawRect(shiploc.X - 3, shiploc.Y - 3, shiploc.X + 3, shiploc.Y + 3, Color.white);
                            }
                        }

                        string label;

                        if (s.Count() > 1)
                            label = string.Format("{0} x {1}", s.Name(), s.Count());
                        else
                            label = s.Name();

                        windowObj.Print(shiploc.X - sprite_width, shiploc.Y + sprite_width + 2, label);
                    }
                }
            }

            // draw nav route:
            // draw current ship marker:
            if (current && s.Region() == regions[current_region].Name())
            {
                x1 = (int)(shiploc.X - sprite_width - 1);
                x2 = (int)(shiploc.X + sprite_width + 1);
                y1 = (int)(shiploc.Y - sprite_width - 1);
                y2 = (int)(shiploc.Y + sprite_width + 1);

                windowObj.DrawRect(x1, y1, x2, y2, Color.white);
            }

            // only see routes for your own team:
            if (editor || s.GetIFF() == 0 || mission != null && s.GetIFF() == mission.Team())
            {
                DrawNavRoute(rgn, s.NavList(), s.MarkerColor(), null, s);
            }
        }

        public virtual void DrawNavRoute(OrbitalRegion rgn,
                                        List<Instruction> s_route,
                                        Color s_marker,
                                        Ship ship = null,
                                        MissionElement elem = null)
        {
            int x1, y1, x2, y2;
            double cx = rect.w / 2;
            double cy = rect.h / 2;

            c = (cx < cy) ? cx : cy;
            r = system.Radius() * zoom;

            if (view_mode == VIEW_REGION)
            {
                if (rgn == null) return;
                r = rgn.Radius() * zoom;
            }

            double scale = c / r;
            double ox = offset_x * scale;
            double oy = offset_y * scale;

            Point old_loc = new Point();
            double old_x = 0;
            double old_y = 0;
            bool old_in = false;

            Point first_loc = new Point();
            int first_x = 0;
            int first_y = 0;
            bool first_in = false;

            bool draw_route = true;
            bool draw_bold = false;

            if (ship != null && ship.GetElementIndex() > 1)
                draw_route = false;

            if (ship != null && ship == current_ship)
            {
                s_marker = s_marker * 1.5f;
                draw_bold = true;
            }

            else if (elem != null && elem == current_elem)
            {
                s_marker = s_marker * 1.5f;
                draw_bold = true;
            }

            for (int i = 0; i < s_route.Count; i++)
            {
                Instruction navpt = s_route[i];

                if (navpt.RegionName().Equals(rgn.Name(), StringComparison.InvariantCultureIgnoreCase))
                {
                    double nav_x = navpt.Location().X * scale;
                    double nav_y = navpt.Location().Y * scale;

                    int isx = (int)(cx + nav_x + ox);
                    int isy = (int)(cy + nav_y + oy);

                    if (old_in && draw_route)
                    {
                        int iox = (int)(cx + old_x + ox);
                        int ioy = (int)(cy + old_y + oy);
                        windowObj.DrawLine(iox, ioy, isx, isy, s_marker);

                        int xx1 = (iox - isx);
                        int yy1 = (ioy - isy);

                        if (draw_bold)
                        {
                            if (xx1 > yy1)
                            {
                                windowObj.DrawLine(iox, ioy + 1, isx, isy + 1, s_marker);
                            }
                            else
                            {
                                windowObj.DrawLine(iox + 1, ioy, isx + 1, isy, s_marker);
                            }
                        }

                        if ((xx1 * xx1 + yy1 * yy1) > 2000)
                        {
                            double dist = new Point(navpt.Location() - old_loc).Length;

                            int imx = (int)(cx + (old_x + nav_x) / 2 + ox);
                            int imy = (int)(cy + (old_y + nav_y) / 2 + oy);

                            string dist_txt ;
                            FormatUtil.FormatNumber(out dist_txt, dist);
                            font.SetColor(Color.gray);
                            windowObj.SetFont(font);
                            windowObj.Print(imx - 20, imy - 6, dist_txt);
                            font.SetColor(Color.white);
                        }
                    }

                    x1 = isx - 3;
                    y1 = isy - 3;
                    x2 = isx + 3;
                    y2 = isy + 3;

                    Color c = Color.white;
                    if (navpt.Status() > Instruction.STATUS.ACTIVE)
                    {
                        c = Color.gray;
                    }
                    else if (!first_in)
                    {
                        first_in = true;
                        first_loc = navpt.Location();
                        first_x = isx;
                        first_y = isy;
                    }

                    if (draw_route)
                    {
                        windowObj.DrawLine(x1, y1, x2, y2, c);
                        windowObj.DrawLine(x1, y2, x2, y1, c);

                        if (navpt == current_navpt)
                            windowObj.DrawRect(x1 - 2, y1 - 2, x2 + 2, y2 + 2, c);

                        string buf;
                        buf = string.Format("{0}", i + 1);
                        windowObj.SetFont(font);
                        windowObj.Print(x2 + 3, y1, buf);

                        if (navpt == current_navpt)
                        {
                            if (!string.IsNullOrEmpty(navpt.TargetName()))
                            {
                                buf = string.Format("{0} {1}", Game.GetText("MapView.item." + Instruction.ActionName(navpt.Action())), navpt.TargetName());
                                windowObj.Print(x2 + 3, y1 + 10, buf);
                            }
                            else
                            {
                                buf = string.Format("{0}", Game.GetText("MapView.item." + Instruction.ActionName(navpt.Action())));
                                windowObj.Print(x2 + 3, y1 + 10, buf);
                            }

                            buf = string.Format("{0}", Game.GetText("MapView.item." + Instruction.FormationName(navpt.Formation())));
                            windowObj.Print(x2 + 3, y1 + 20, buf);

                            buf = string.Format("{0}", navpt.Speed());
                            windowObj.Print(x2 + 3, y1 + 30, buf);

                            if (navpt.HoldTime() != 0)
                            {
                                string hold_time;
                                FormatUtil.FormatTime(out hold_time, navpt.HoldTime());

                                buf = string.Format("{0} {1}", Game.GetText("MapView.item.Hold"), hold_time);
                                windowObj.Print(x2 + 3, y1 + 40, buf);
                            }
                        }
                    }

                    old_loc = navpt.Location();
                    old_x = nav_x;
                    old_y = nav_y;
                    old_in = true;
                }

                else
                {
                    old_loc = navpt.Location();
                    old_x = 0;
                    old_y = 0;
                    old_in = false;
                }
            }

            // if the ship and the first active navpoint are both in the region,
            // draw a line from the ship to the first active navpoint:

            if (first_in)
            {
                old_in = false;

                if (ship != null && ship.GetRegion() != null)
                {
                    old_in = (ship.GetRegion().GetOrbitalRegion() == rgn);

                    if (old_in)
                    {
                        old_loc = ship.Location().OtherHand();
                        old_x = old_loc.X * scale;
                        old_y = old_loc.Y * scale;
                    }
                }

                else if (elem != null)
                {
                    old_in = (elem.Region() == rgn.Name()) ? true : false;

                    if (old_in)
                    {
                        old_loc = elem.Location();
                        old_x = old_loc.X * scale;
                        old_y = old_loc.Y * scale;
                    }
                }

                if (old_in)
                {
                    int iox = (int)(cx + old_x + ox);
                    int ioy = (int)(cy + old_y + oy);
                    windowObj.DrawLine(iox, ioy, first_x, first_y, s_marker);

                    int xx1 = (iox - first_x);
                    int yy1 = (ioy - first_y);

                    if (draw_bold)
                    {
                        if (xx1 > yy1)
                        {
                            windowObj.DrawLine(iox, ioy + 1, first_x, first_y + 1, s_marker);
                        }
                        else
                        {
                            windowObj.DrawLine(iox + 1, ioy, first_x + 1, first_y, s_marker);
                        }
                    }

                    if ((xx1 * xx1 + yy1 * yy1) > 2000)
                    {
                        double dist = new Point(first_loc - old_loc).Length;
                        double nav_x = first_loc.X * scale;
                        double nav_y = first_loc.Y * scale;

                        int imx = (int)(cx + (old_x + nav_x) / 2 + ox);
                        int imy = (int)(cy + (old_y + nav_y) / 2 + oy);

                        string dist_txt ;
                        FormatUtil.FormatNumber(out dist_txt, dist);
                        font.SetColor(Color.gray);
                        windowObj.SetFont(font);
                        windowObj.Print(imx - 20, imy - 6, dist_txt);
                        font.SetColor(Color.white);
                    }
                }
            }
        }



        public virtual void DrawCombatantSystem(Combatant c, Orbital rgn, int x, int y, int r)
        {
            int team = c.GetIFF();
            int x1 = 0;
            int x2 = 0;
            int y1 = y - r;
            TextFormat a = 0;

            switch (team)
            {
                case 0:
                    x1 = x - 64;
                    x2 = x + 64;
                    y1 = y + r + 4;
                    a = TextFormat.DT_CENTER;
                    break;

                case 1:
                    x1 = x - 200;
                    x2 = x - r - 4;
                    a = TextFormat.DT_RIGHT;
                    break;

                default:
                    x1 = x + r + 4;
                    x2 = x + 200;
                    a = TextFormat.DT_LEFT;
                    break;
            }

            DrawCombatGroupSystem(c.GetForce(), rgn, x1, x2, ref y1, a);
        }

        public virtual void DrawCombatGroupSystem(CombatGroup group, Orbital rgn, int x1, int x2, ref int y, TextFormat a)
        {
            if (group == null || group.IsReserve() || group.CalcValue() < 1)
                return;

            string txt;

            if (group.GetRegion() == rgn.Name())
            {
                switch (group.Type())
                {
                    case CombatGroup.GROUP_TYPE.CARRIER_GROUP:
                    case CombatGroup.GROUP_TYPE.BATTLE_GROUP:
                    case CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON:
                        txt = string.Format("{0} '{1}'", group.GetShortDescription(), group.Name());
                        active_window.SetFont(font);
                        active_window.DrawText(txt, 0, new Rect(x1, y, x2 - x1, 12), a);
                        y += 10;
                        break;

                    case CombatGroup.GROUP_TYPE.BATTALION:
                    case CombatGroup.GROUP_TYPE.STATION:
                    case CombatGroup.GROUP_TYPE.STARBASE:

                    case CombatGroup.GROUP_TYPE.MINEFIELD:
                    case CombatGroup.GROUP_TYPE.BATTERY:
                    case CombatGroup.GROUP_TYPE.MISSILE:

                        active_window.SetFont(font);
                        active_window.DrawText(group.GetShortDescription(), 0, new Rect(x1, y, x2 - x1, 12), a);
                        y += 10;
                        break;

                    default:
                        break;
                }
            }

            foreach (CombatGroup g in group.GetComponents())
            {
                DrawCombatGroupSystem(g, rgn, x1, x2, ref y, a);
            }
        }

        public virtual void DrawCombatGroup(CombatGroup group, int rep = 3)
        {
            // does group even exist yet?
            if (group == null || group.IsReserve() || group.CalcValue() < 1)
                return;

            // is group a squadron?  don't draw squadrons on map:
            if (group.Type() >= CombatGroup.GROUP_TYPE.WING && group.Type() < CombatGroup.GROUP_TYPE.FLEET)
                return;

            // has group been discovered yet?
            CombatGroup player_group = campaign.GetPlayerGroup();
            if (group.GetIFF() != 0 && player_group != null && player_group.GetIFF() != group.GetIFF())
                if (group.IntelLevel() <= Intel.INTEL_TYPE.KNOWN)
                    return;

            // has group been destroyed already?
            if (group.CalcValue() < 1)
                return;

            OrbitalRegion rgn = (OrbitalRegion)regions[current_region];
            if (rgn == null) return;

            POINT shiploc;

            double cx = rect.w / 2;
            double cy = rect.h / 2;

            c = (cx < cy) ? cx : cy;
            r = rgn.Radius() * zoom;

            double scale = c / r;

            double ox = offset_x * scale;
            double oy = offset_y * scale;

            double rlx = 0;
            double rly = 0;

            int sprite_width = 10;

            if (group.GetUnits().Count > 0)
            {
                CombatUnit unit = null;

                for (int i = 0; i < group.GetUnits().Count; i++)
                {
                    unit = group.GetUnits()[i];

                    if (unit.Count() - unit.DeadCount() > 0)
                        break;
                }

                // draw unit icon:
                if (unit.GetRegion() == rgn.Name() && unit.Type() > SimElements.CLASSIFICATION.LCA && unit.Count() > 0)
                {
                    double sx = (unit.Location().X + rlx) * scale;
                    double sy = (unit.Location().Y + rly) * scale;

                    shiploc.X = (int)(cx + sx + ox);
                    shiploc.Y = (int)(cy + sy + oy);

                    bool ship_visible = shiploc.X >= 0 && shiploc.X < rect.w &&
                    shiploc.Y >= 0 && shiploc.Y < rect.h;

                    if (ship_visible)
                    {
                        if (rep < 3)
                        {
                            windowObj.FillRect(shiploc.X - 2, shiploc.Y - 2, shiploc.X + 2, shiploc.Y + 2, unit.MarkerColor());
                            sprite_width = 2;

                            string buf;
                            buf = string.Format("{0}", unit.Name());
                            windowObj.SetFont(font);
                            windowObj.Print(shiploc.X - sprite_width, shiploc.Y + sprite_width + 2, buf);
                        }
                        else
                        {
                            int sprite_index = 2;
                            int nsprites = 0;

                            if (unit.GetDesign() != null)
                                nsprites = unit.GetDesign().map_sprites.Count;

                            if (nsprites != 0)
                            {
                                if (sprite_index >= nsprites)
                                    sprite_index = sprite_index % nsprites;

                                Bitmap map_sprite = unit.GetDesign().map_sprites[sprite_index];

                                Bitmap bmp = new Bitmap(map_sprite.Texture);
                                //bmp.CopyBitmap(map_sprite);
                                ColorizeBitmap(bmp, unit.MarkerColor());
                                sprite_width = bmp.Width() / 2;
                                int h = bmp.Height() / 2;

                                windowObj.DrawBitmap(shiploc.X - sprite_width,
                                shiploc.Y - h,
                                shiploc.X + sprite_width,
                                shiploc.Y + h,
                                bmp,
                                Video.BLEND_TYPE.BLEND_ALPHA);
                            }

                            else
                            {
                                if (unit.IsStatic())
                                {
                                    windowObj.FillRect(shiploc.X - 6, shiploc.Y - 6, shiploc.X + 6, shiploc.Y + 6, unit.MarkerColor());
                                    windowObj.DrawRect(shiploc.X - 6, shiploc.Y - 6, shiploc.X + 6, shiploc.Y + 6, Color.white);
                                }
                                else if (unit.IsStarship())
                                {
                                    windowObj.FillRect(shiploc.X - 4, shiploc.Y - 4, shiploc.X + 4, shiploc.Y + 4, unit.MarkerColor());
                                    windowObj.DrawRect(shiploc.X - 4, shiploc.Y - 4, shiploc.X + 4, shiploc.Y + 4, Color.white);
                                }
                                else
                                {
                                    windowObj.FillRect(shiploc.X - 3, shiploc.Y - 3, shiploc.X + 3, shiploc.Y + 3, unit.MarkerColor());
                                    windowObj.DrawRect(shiploc.X - 3, shiploc.Y - 3, shiploc.X + 3, shiploc.Y + 3, Color.white);
                                }
                            }

                            string label;
                            label = unit.GetDescription();
                            windowObj.SetFont(font);
                            windowObj.Print(shiploc.X - sprite_width, shiploc.Y + sprite_width + 2, label);
                        }
                    }
                }
            }

            // recurse
            foreach (CombatGroup g in group.GetComponents())
            {
                DrawCombatGroup(g, rep);
            }
        }


        public virtual int GetViewMode() { return view_mode; }
        public virtual void SetViewMode(int mode)
        {
            if (mode >= 0 && mode < 3)
            {
                // save state:
                view_zoom[view_mode] = zoom;
                view_offset_x[view_mode] = offset_x;
                view_offset_y[view_mode] = offset_y;

                // switch mode:
                view_mode = mode;

                // restore state:

                if (view_mode == VIEW_GALAXY)
                {
                    zoom = 1;
                    offset_x = 0;
                    offset_y = 0;
                }
                else
                {
                    zoom = view_zoom[view_mode];
                    offset_x = view_offset_x[view_mode];
                    offset_y = view_offset_y[view_mode];
                }

                scrolling = 0;
                scroll_x = 0;
                scroll_y = 0;
            }
        }

        public virtual void SetSelectionMode(int mode)
        {
            if (mode <= SELECT_NONE)
            {
                seln_mode = SELECT_NONE;
                return;
            }

            if (mode != seln_mode && mode <= SELECT_FIGHTER)
            {
                seln_mode = mode;

                // when changing mode, 
                // select the item closest to the current center:
                if (system != null && view_mode == VIEW_SYSTEM)
                    SelectAt(rect.x + rect.w / 2,
                    rect.y + rect.h / 2);
            }
        }

        public virtual int GetSelectionMode() { return seln_mode; }
        public virtual void SetSelection(int index)
        {
            if (scrolling != 0) return;
            Orbital s = null;

            switch (seln_mode)
            {
                case SELECT_SYSTEM:
                    if (index < system_list.Count)
                        SetSystem(system_list[index]);
                    s = stars[current_star];
                    break;

                default:
                case SELECT_PLANET:
                    if (index < planets.Count)
                        current_planet = index;
                    s = planets[current_planet];
                    break;

                case SELECT_REGION:
                    if (index < regions.Count)
                        current_region = index;
                    s = regions[current_region];
                    break;

                case SELECT_STATION:
                    {
                        if (mission != null)
                        {
                            MissionElement selected_elem = null;

                            foreach (MissionElement elem in mission.GetElements())
                            {
                                if (elem.IsStatic())
                                {
                                    if (elem.Identity() == index)
                                    {
                                        selected_elem = elem;
                                        break;
                                    }
                                }
                            }

                            SelectElem(selected_elem);

                            if (selected_elem != null && regions.Count != 0)
                            {
                                foreach (Orbital rgn in regions)
                                {
                                    if (selected_elem.Region().Equals(rgn.Name(), StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        Orbital elem_region = rgn;
                                        current_region = regions.IndexOf(elem_region);
                                    }
                                }
                            }
                        }

                        else
                        {
                            Ship selship = null;

                            if (ship != null)
                            {
                                SimRegion simrgn = ship.GetRegion();
                                if (simrgn != null)
                                {
                                    foreach (Ship ss in simrgn.Ships())
                                    {
                                        if (ss.IsStatic())
                                        {
                                            if (ss.Identity() == index)
                                            {
                                                selship = ss;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            SelectShip(selship);

                            if (selship != null)
                            {
                                s = selship.GetRegion().GetOrbitalRegion();
                                current_region = regions.IndexOf(s);
                            }
                        }
                    }
                    break;

                case SELECT_STARSHIP:
                    {
                        if (mission != null)
                        {
                            MissionElement selected_elem = null;

                            foreach (MissionElement elem in mission.GetElements())
                            {
                                if (elem.IsStarship())
                                {
                                    if (elem.Identity() == index)
                                    {
                                        selected_elem = elem;
                                        break;
                                    }
                                }
                            }

                            SelectElem(selected_elem);

                            if (selected_elem != null && regions.Count != 0)
                            {
                                foreach (Orbital rgn in regions)
                                {
                                    if (selected_elem.Region().Equals(rgn.Name(), StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        Orbital elem_region = rgn;
                                        current_region = regions.IndexOf(elem_region);
                                    }
                                }
                            }
                        }

                        else
                        {
                            Ship selship = null;

                            if (ship != null)
                            {
                                SimRegion simrgn = ship.GetRegion();
                                if (simrgn != null)
                                {
                                    foreach (Ship ss in simrgn.Ships())
                                    {
                                        if (ss.IsStarship())
                                        {
                                            if (ss.Identity() == index)
                                            {
                                                selship = ss;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            SelectShip(selship);

                            if (selship != null)
                            {
                                s = selship.GetRegion().GetOrbitalRegion();
                                current_region = regions.IndexOf(s);
                            }
                        }
                    }
                    break;

                case SELECT_FIGHTER:
                    {
                        if (mission != null)
                        {
                            MissionElement selected_elem = null;

                            foreach (MissionElement elem in mission.GetElements())
                            {
                                if (elem.IsDropship() && !elem.IsSquadron())
                                {
                                    if (elem.Identity() == index)
                                    {
                                        selected_elem = elem;
                                        break;
                                    }
                                }
                            }

                            SelectElem(selected_elem);

                            if (selected_elem != null && regions.Count != 0)
                            {
                                foreach (Orbital rgn in regions)
                                {
                                    if (selected_elem.Region().Equals(rgn.Name(), StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        Orbital elem_region = rgn;
                                        current_region = regions.IndexOf(elem_region);
                                    }
                                }
                            }
                        }

                        else
                        {
                            Ship selship = null;

                            if (ship != null)
                            {
                                SimRegion simrgn = ship.GetRegion();
                                if (simrgn != null)
                                {
                                    foreach (Ship ss in simrgn.Ships())
                                    {
                                        if (ss.IsDropship())
                                        {
                                            if (ss.Identity() == index)
                                            {
                                                selship = ss;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            SelectShip(selship);

                            if (selship != null)
                            {
                                s = selship.GetRegion().GetOrbitalRegion();
                                current_region = regions.IndexOf(s);
                            }
                        }
                    }
                    break;
            }

            SetupScroll(s);
        }

        public virtual void SetSelectedShip(Ship ship)
        {
            if (scrolling != 0) return;
            Orbital s = null;
            Ship selship = null;


            switch (seln_mode)
            {
                case SELECT_SYSTEM:
                case SELECT_PLANET:
                case SELECT_REGION:
                default:
                    break;

                case SELECT_STATION:
                case SELECT_STARSHIP:
                case SELECT_FIGHTER:
                    {
                        if (ship != null)
                        {
                            SimRegion simrgn = ship.GetRegion();

                            if (simrgn != null && simrgn.NumShips() != 0)
                            {
                                selship = simrgn.Ships().Find(x => x == ship);
                            }
                        }

                        SelectShip(selship);
                    }
                    break;
            }

            if (selship != null)
                SetupScroll(s);
        }

        public virtual void SetSelectedElem(MissionElement elem)
        {
            if (scrolling != 0) return;
            Orbital s = null;

            switch (seln_mode)
            {
                case SELECT_SYSTEM:
                case SELECT_PLANET:
                case SELECT_REGION:
                default:
                    break;

                case SELECT_STATION:
                case SELECT_STARSHIP:
                case SELECT_FIGHTER:
                    {
                        SelectElem(elem);
                    }
                    break;
            }

            if (current_elem != null)
                SetupScroll(s);
        }

        public virtual void SetRegion(OrbitalRegion rgn)
        {
            if (scrolling != 0 || rgn == null) return;

            int index = regions.IndexOf(rgn);

            if (index < 0 || index == current_region || index >= regions.Count)
                return;

            current_region = index;
            Orbital s = regions[current_region];

            if (s == null)
                return;

            switch (view_mode)
            {
                case VIEW_GALAXY:
                case VIEW_SYSTEM:
                    scroll_x = (offset_x + s.Location().X) / 5.0;
                    scroll_y = (offset_y + s.Location().Y) / 5.0;
                    scrolling = 5;
                    break;

                case VIEW_REGION:
                    offset_x = 0;
                    offset_y = 0;
                    scrolling = 0;
                    break;
            }
        }

        public virtual void SetRegionByName(string rgn_name)
        {
            OrbitalRegion rgn = null;

            for (int i = 0; i < regions.Count; i++)
            {
                Orbital r = regions[i];
                if (rgn_name == r.Name())
                {
                    rgn = (OrbitalRegion)r;
                    break;
                }
            }

            SetRegion(rgn);
        }
        public virtual void SelectAt(int x, int y)
        {
            if (scrolling != 0) return;
            if (c == 0) return;
            if (seln_mode < 0) return;

            Orbital s = null;

            double scale = r / c;
            double cx = rect.w / 2;
            double cy = rect.h / 2;
            double test_x = (x - rect.x - cx) * scale - offset_x;
            double test_y = (y - rect.y - cy) * scale - offset_y;
            double dist = 1.0e20;
            int closest = 0;

            if (view_mode == VIEW_GALAXY)
            {
                c = (cx > cy) ? cx : cy;
                r = 10;

                Galaxy g = Galaxy.GetInstance();
                if (g != null)
                    r = g.Radius();

                StarSystem closest_system = null;

                // draw the list of systems, and their connections:
                foreach (StarSystem ss in system_list)
                {
                    double dx = (ss.Location().X - test_x);
                    double dy = (ss.Location().Y - test_y);
                    double d = Math.Sqrt(dx * dx + dy * dy);

                    if (d < dist)
                    {
                        dist = d;
                        closest_system = ss;
                    }
                }

                if (closest_system != null)
                    SetSystem(closest_system);
            }

            else if (view_mode == VIEW_SYSTEM)
            {
                switch (seln_mode)
                {
                    case SELECT_SYSTEM:
                        {
                            if (stars.IsEmpty()) return;
                            int index = 0;
                            foreach (Orbital star in stars)
                            {
                                double dx = (star.Location().X - test_x);
                                double dy = (star.Location().Y - test_y);
                                double d = Math.Sqrt(dx * dx + dy * dy);

                                if (d < dist)
                                {
                                    dist = d;
                                    closest = index;
                                }

                                index++;
                            }

                            current_star = closest;
                        }
                        s = stars[current_star];
                        break;

                    case SELECT_PLANET:
                        {
                            if (planets.IsEmpty()) return;
                            int index = 0;
                            foreach (Orbital planet in planets)
                            {
                                double dx = (planet.Location().X - test_x);
                                double dy = (planet.Location().Y - test_y);
                                double d = Math.Sqrt(dx * dx + dy * dy);

                                if (d < dist)
                                {
                                    dist = d;
                                    closest = index;
                                }

                                index++;
                            }

                            current_planet = closest;
                        }
                        s = planets[current_planet];
                        break;

                    default:
                    case SELECT_REGION:
                        {
                            if (regions.IsEmpty()) return;
                            int index = 0;
                            foreach (Orbital region in regions)
                            {
                                double dx = (region.Location().X - test_x);
                                double dy = (region.Location().Y - test_y);
                                double d = Math.Sqrt(dx * dx + dy * dy);

                                if (d < dist)
                                {
                                    dist = d;
                                    closest = index;
                                }

                                index++;
                            }

                            current_region = closest;
                        }
                        s = regions[current_region];
                        break;
                }
            }

            else if (view_mode == VIEW_REGION)
            {
                dist = 5.0e3;

                if (mission != null)
                {
                    Orbital rgn = regions[current_region];
                    MissionElement sel_elem = null;
                    Instruction sel_nav = null;

                    if (rgn == null) return;

                    // check nav points:
                    foreach (MissionElement e in mission.GetElements())
                    {
                        if (!e.IsSquadron() && (editor || e.GetIFF() == mission.Team()))
                        {
                            foreach (Instruction n in e.NavList())
                            {
                                if (n.RegionName().Equals(rgn.Name(), StringComparison.InvariantCultureIgnoreCase))
                                {
                                    Point nloc = n.Location();
                                    double dx = nloc.X - test_x;
                                    double dy = nloc.Y - test_y;
                                    double d = Math.Sqrt(dx * dx + dy * dy);

                                    if (d < dist)
                                    {
                                        dist = d;
                                        sel_nav = n;
                                        sel_elem = e;
                                    }
                                }
                            }
                        }
                    }

                    if (sel_nav != null)
                    {
                        SelectElem(sel_elem);
                        SelectNavpt(sel_nav);
                    }

                    // check elements:
                    else
                    {
                        foreach (MissionElement e in mission.GetElements())
                        {
                            if (e.Region() == rgn.Name() && !e.IsSquadron())
                            {
                                Point sloc = e.Location();
                                double dx = sloc.X - test_x;
                                double dy = sloc.Y - test_y;
                                double d = Math.Sqrt(dx * dx + dy * dy);

                                if (d < dist)
                                {
                                    dist = d;
                                    sel_elem = e;
                                }
                            }
                        }

                        SelectElem(sel_elem);

                        if (sel_elem != null)
                            s = rgn;
                    }
                }
                else if (ship != null)
                {
                    Sim sim = Sim.GetSim();
                    Orbital rgn = regions[current_region];
                    SimRegion simrgn = null;
                    Ship sel_ship = null;
                    Instruction sel_nav = null;

                    if (sim != null && rgn != null)
                        simrgn = sim.FindRegion(rgn.Name());

                    // check nav points:
                    if (simrgn != null)
                    {
                        for (int r = 0; r < sim.GetRegions().Count; r++)
                        {
                            SimRegion simrgn2 = sim.GetRegions()[r];

                            for (int i = 0; i < simrgn2.Ships().Count; i++)
                            {
                                Ship ss = simrgn2.Ships()[i];

                                if (ss.GetIFF() == ship.GetIFF() && ss.GetElementIndex() == 1)
                                {
                                    foreach (Instruction n in ss.GetFlightPlan())
                                    {
                                        if (n.RegionName().Equals(rgn.Name(), StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            Point nloc = n.Location();
                                            double dx = nloc.X - test_x;
                                            double dy = nloc.Y - test_y;
                                            double d = Math.Sqrt(dx * dx + dy * dy);

                                            if (d < dist)
                                            {
                                                dist = d;
                                                sel_nav = n;
                                                sel_ship = ss;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (sel_nav != null)
                    {
                        SelectShip(sel_ship);
                        SelectNavpt(sel_nav);
                    }

                    // check ships:
                    else if (simrgn.NumShips() != 0)
                    {
                        foreach (Ship ss in simrgn.Ships())
                        {
                            if (!IsClutter(ss))
                            {
                                Point sloc = ss.Location().OtherHand();
                                double dx = sloc.X - test_x;
                                double dy = sloc.Y - test_y;
                                double d = Math.Sqrt(dx * dx + dy * dy);

                                if (d < dist)
                                {
                                    dist = d;
                                    sel_ship = ss;
                                }
                            }
                        }

                        SelectShip(sel_ship);
                    }
                    else
                    {
                        SelectShip(null);
                        SelectNavpt(null);
                    }
                }
            }

            if (s != null)
                SetupScroll(s);
        }
        public virtual Orbital GetSelection()
        {
            Orbital s = null;

            switch (seln_mode)
            {
                case SELECT_SYSTEM:
                    if (current_star < stars.Count)
                        s = stars[current_star];
                    break;

                default:
                case SELECT_PLANET:
                    if (current_planet < planets.Count)
                        s = planets[current_planet];
                    break;

                case SELECT_REGION:
                    if (current_region < regions.Count)
                        s = regions[current_region];
                    break;

                case SELECT_STATION:
                case SELECT_STARSHIP:
                case SELECT_FIGHTER:
                    break;
            }

            return s;
        }
        public virtual Ship GetSelectedShip()
        {
            return current_ship;
        }


        public virtual MissionElement GetSelectedElem()
        {
            return current_elem;
        }


        public virtual int GetSelectionIndex()
        {
            int s = 0;

            switch (seln_mode)
            {
                case SELECT_SYSTEM: s = current_star; break;
                default:
                case SELECT_PLANET: s = current_planet; break;
                case SELECT_REGION: s = current_region; break;

                case SELECT_STATION:
                case SELECT_STARSHIP:
                case SELECT_FIGHTER:
                    {
                        s = -1;
                        Sim sim = Sim.GetSim();
                        Orbital rgn = regions[current_region];
                        SimRegion simrgn = null;

                        if (sim != null && rgn != null)
                            simrgn = sim.FindRegion(rgn.Name());

                        if (simrgn != null)
                        {
                            if (current_ship != null && simrgn.NumShips() != 0)
                            {
                                s = simrgn.Ships().IndexOf(current_ship);
                            }
                        }
                    }
                    break;
            }

            return s;
        }
        public virtual void SetShipFilter(SimElements.CLASSIFICATION f) { ship_filter = f; }

        // Event Target Interface:
        public virtual int OnMouseMove(int x, int y)
        {
            if (captured)
            {
                IEventTarget test = null;
                EventDispatch dispatch = EventDispatch.GetInstance();
                if (dispatch != null)
                    test = dispatch.GetCapture();

                if (test != this)
                {
                    captured = false;
                    Mouse.Show(true);
                }

                else
                {
                    if (dragging)
                    {
                        int delta_x = x - mouse_x;
                        int delta_y = y - mouse_y;

                        offset_x += delta_x * r / c;
                        offset_y += delta_y * r / c;

                        Mouse.SetCursorPos(mouse_x, mouse_y);
                    }

                    else if (view_mode == VIEW_REGION)
                    {
                        double scale = r / c;
                        click_x = (x - rect.x - rect.w / 2) * scale - offset_x;
                        click_y = (y - rect.y - rect.h / 2) * scale - offset_y;

                        if ((adding_navpt || moving_navpt) && current_navpt != null)
                        {
                            Point loc = current_navpt.Location();
                            loc.X = click_x;
                            loc.Y = click_y;
                            current_navpt.SetLocation(loc);
                            current_navpt.SetStatus(current_status);
                        }

                        else if (editor && moving_elem && current_elem != null)
                        {
                            Point loc = current_elem.Location();
                            loc.X = click_x;
                            loc.Y = click_y;
                            current_elem.SetLocation(loc);
                        }
                    }
                }
            }

            return active_window.OnMouseMove(x, y);
        }
        public virtual int OnLButtonDown(int x, int y)
        {
#if TODO
            if (!captured)
                captured = SetCapture();

            if (captured)
            {
                dragging = true;
                mouse_x = x;
                mouse_y = y;
                Mouse.Show(false);
            }

            return active_window.OnRButtonDown(x, y);
#endif 
            throw new NotImplementedException();
        }
        public virtual int OnLButtonUp(int x, int y)
        {
#if TODO
          if (captured)
            {
                ReleaseCapture();
                captured = false;
                Mouse.Show(true);
            }

            dragging = false;

            return active_window.OnRButtonUp(x, y);
#endif
            throw new NotImplementedException();
        }
        public virtual int OnClick()
        {
            return active_window.OnClick();
        }
        public virtual int OnRButtonDown(int x, int y)
        {
            if (menu_view != null && menu_view.IsShown())
            {
                // ignore this event...
            }
            else
            {
                if (!captured)
                    captured = SetCapture();

                if (view_mode == VIEW_REGION)
                {
                    double scale = r / c;
                    click_x = (x - rect.x - rect.w / 2) * scale - offset_x;
                    click_y = (y - rect.y - rect.h / 2) * scale - offset_y;

                    if (current_navpt != null)
                    {
                        Point nloc = current_navpt.Location();
                        double dx = nloc.X - click_x;
                        double dy = nloc.Y - click_y;
                        double d = Math.Sqrt(dx * dx + dy * dy);

                        if (d < 5e3)
                        {
                            moving_navpt = true;

                            if (ship != null && current_ship != null && ship != current_ship)
                            {
                                if (ship.GetElement() != null && current_ship.GetElement() != null)
                                {
                                    if (!ship.GetElement().CanCommand(current_ship.GetElement()))
                                    {
                                        moving_navpt = false;
                                    }
                                }
                            }
                            else if (current_elem != null && NetLobby.GetInstance() != null)
                            {
                                moving_navpt = false;
                            }
                        }
                    }

                    else if (editor && current_elem != null)
                    {
                        Point nloc = current_elem.Location();
                        double dx = nloc.X - click_x;
                        double dy = nloc.Y - click_y;
                        double d = Math.Sqrt(dx * dx + dy * dy);

                        if (d < 5e3)
                        {
                            moving_elem = true;

                            if (current_elem != null && NetLobby.GetInstance() != null)
                            {
                                moving_elem = false;
                            }
                        }
                    }
                }
            }

            return active_window.OnLButtonDown(x, y);
        }
        public virtual int OnRButtonUp(int x, int y)
        {
            bool process_event = false;

            if (captured)
            {
                process_event = true;
                ReleaseCapture();
                captured = false;
                Mouse.Show(true);
            }

            if (process_event && !adding_navpt)
            {
                if (!moving_navpt && !moving_elem)
                {
                    Button.PlaySound();
                }

                if (view_mode == VIEW_REGION)
                {
                    double scale = r / c;
                    click_x = (x - rect.x - rect.w / 2) * scale - offset_x;
                    click_y = (y - rect.y - rect.h / 2) * scale - offset_y;

                    if (scrolling == 0)
                        SelectAt(x, y);

                    active_window.ClientEvent((ActiveWindow.ETD)NavDlg.EID_MAP_CLICK, x, y);
                }

                else if (scrolling == 0)
                {
                    SelectAt(x, y);

                    active_window.ClientEvent((ActiveWindow.ETD)NavDlg.EID_MAP_CLICK, x, y);
                }
            }

            if ((adding_navpt || moving_navpt) && current_navpt != null)
            {
                current_navpt.SetStatus(current_status);

                Sim sim = Sim.GetSim();
                if (sim != null)
                {
                    Ship s = current_ship;

                    if (s != null && s.GetElement() != null)
                    {
                        Element elem = s.GetElement();
                        int index = elem.GetNavIndex(current_navpt);

                        if (index >= 0)
                            NetUtil.SendNavData(false, elem, index - 1, current_navpt);
                    }
                }
            }

            adding_navpt = false;
            moving_navpt = false;
            moving_elem = false;

            return active_window.OnLButtonUp(x, y);
        }

        public virtual bool IsEnabled()
        {
            if (active_window != null)
                return active_window.IsEnabled();

            return false;
        }
        public virtual bool IsVisible()
        {
            if (active_window != null)
                return active_window.IsVisible();

            return false;
        }
        public virtual bool IsFormActive()
        {
            if (active_window != null)
                return active_window.IsFormActive();

            return false;
        }
        public virtual Rect TargetRect()
        {
            if (active_window != null)
                return active_window.TargetRect();

            return new Rect();
        }

        public void ZoomIn()
        {
            zoom *= 0.9;

            if (view_mode == VIEW_SYSTEM)
            {
                if (system != null && zoom * system.Radius() < 2e6)
                {
                    zoom = 2e6 / system.Radius();
                }
            }
            else if (view_mode == VIEW_REGION)
            {
                OrbitalRegion rgn = GetRegion();
                if (rgn != null && zoom * rgn.Radius() < 1e3)
                {
                    zoom = 1e3 / rgn.Radius();
                }
            }
        }
        public void ZoomOut()
        {
            zoom *= 1.1;

            if (view_mode == VIEW_SYSTEM)
            {
                if (system != null && zoom * system.Radius() > 500e9)
                {
                    zoom = 500e9 / system.Radius();
                }
            }
            else if (view_mode == VIEW_REGION)
            {
                OrbitalRegion rgn = GetRegion();
                if (rgn != null && zoom * rgn.Radius() > 1e6)
                {
                    zoom = 1e6 / rgn.Radius();
                }
            }
        }

        public void SetGalaxy(List<StarSystem> g)
        {
            system_list.Clear();
            system_list.AddRange(g);

            if (system_list.Count > 0)
            {
                SetSystem(system_list[0]);
            }
        }

        public void SetSystem(StarSystem s)
        {
            if (system != s)
            {
                system = s;

                // forget invalid selection:
                current_star = 0;
                current_planet = 0;
                current_region = 0;
                current_ship = null;
                current_elem = null;
                current_navpt = null;

                // flush old object pointers:
                stars.Clear();
                planets.Clear();
                regions.Clear();

                // insert objects from star system:
                if (system != null)
                {
                    foreach (OrbitalBody star in system.Bodies())
                    {
                        switch (star.Type())
                        {
                            case Orbital.OrbitalType.STAR:
                                stars.Add(star);
                                break;
                            case Orbital.OrbitalType.PLANET:
                            case Orbital.OrbitalType.MOON:
                                planets.Add(star);
                                break;
                        }
                    }
                    OrbitalBody laststar = system.Bodies().Last(); //TODO
                    foreach (OrbitalBody planet in laststar.Satellites())
                    {
                        planets.Add(planet);

                        foreach (OrbitalBody moon in planet.Satellites())
                        {
                            planets.Add(moon);
                        }
                    }

                    foreach (OrbitalRegion rgn in system.AllRegions())
                        regions.Add(rgn);

                    // sort region list by distance from the star:
                    regions.Sort();
                }

                BuildMenu();
            }
        }

        public void SetMission(Mission m)
        {
            if (mission != m)
            {
                mission = m;

                // forget invalid selection:
                current_star = 0;
                current_planet = 0;
                current_region = 0;
                current_ship = null;
                current_elem = null;
                current_navpt = null;

                if (mission != null && system_list.Count > 0)
                {
                    system = null;
                    SetSystem(mission.GetStarSystem());
                }

                BuildMenu();
            }
        }
        public void SetShip(Ship s)
        {
            if (ship != s)
            {
                ship = s;

                // forget invalid selection:
                current_star = 0;
                current_planet = 0;
                current_region = 0;
                current_ship = null;
                current_elem = null;
                current_navpt = null;

                if (ship != null && system_list.Count > 0)
                {
                    SimRegion rgn = ship.GetRegion();

                    if (rgn != null && rgn.System() != null)
                    {
                        system = null;
                        SetSystem(rgn.System());
                    }
                }

                BuildMenu();
            }
        }
        public void SetCampaign(Campaign c)
        {
            if (campaign != c)
            {
                campaign = c;

                // forget invalid selection:
                current_star = 0;
                current_planet = 0;
                current_region = 0;
                current_ship = null;
                current_elem = null;
                current_navpt = null;

                if (campaign != null)
                    SetGalaxy(campaign.GetSystemList());
            }
        }

        private const int MAP_SYSTEM = 1000;
        private const int MAP_SECTOR = 2000;
        private const int MAP_SHIP = 3000;
        private const int MAP_NAV = 4000;
        private const int MAP_ADDNAV = 4001;
        private const int MAP_DELETE = 4002;
        private const int MAP_CLEAR = 4003;
        private const int MAP_ACTION = 5000;
        private const int MAP_FORMATION = 6000;
        private const int MAP_SPEED = 7000;
        private const int MAP_HOLD = 8000;
        private const int MAP_FARCAST = 8500;
        private const int MAP_OBJECTIVE = 9000;


        public bool IsVisible(Point loc) { throw new NotImplementedException(); }

        // accessors:
        public virtual void GetClickLoc(out double x, out double y) { x = click_x; y = click_y; }
        public List<StarSystem> GetGalaxy() { return system_list; }
        public StarSystem GetSystem() { return system; }
        public OrbitalRegion GetRegion()
        {
            OrbitalRegion result = null;

            if (current_region < regions.Count)
                result = (OrbitalRegion)regions[current_region];

            return result;
        }


        public virtual bool Update(SimObject obj)
        {
            if (obj == current_ship)
            {
                current_ship = null;
                active_menu = map_menu;
            }

            return simObserver.Update(obj);
        }
        public virtual string GetObserverName() { return "MapWin"; }

        public bool GetEditorMode() { return editor; }
        public void SetEditorMode(bool b) { editor = b; }


        protected virtual void BuildMenu()
        {
            ClearMenu();

            map_system_menu = new Menu(Game.GetText("MapView.menu.STARSYSTEM"));

            if (system_list.Count > 0)
            {
                int i = 0;
                foreach (StarSystem s in system_list)
                {
                    map_system_menu.AddItem(s.Name(), MAP_SYSTEM + i);
                    i++;
                }
            }

            else if (system != null)
            {
                map_system_menu.AddItem(system.Name(), MAP_SYSTEM);
            }

            map_sector_menu = new Menu(Game.GetText("MapView.menu.SECTOR"));
            for (int i = 0; i < regions.Count; i++)
            {
                Orbital rgn = regions[i];
                map_sector_menu.AddItem(rgn.Name(), MAP_SECTOR + i);
            }

            map_menu = new Menu(Game.GetText("MapView.menu.MAP"));
            map_menu.AddMenu("System", map_system_menu);
            map_menu.AddMenu("Sector", map_sector_menu);

            if (ship != null || mission != null)
            {
                ship_menu = new Menu(Game.GetText("MapView.menu.SHIP"));
                ship_menu.AddMenu(Game.GetText("MapView.item.Starsystem"), map_system_menu);
                ship_menu.AddMenu(Game.GetText("MapView.item.Sector"), map_sector_menu);

                ship_menu.AddItem("", 0);
                ship_menu.AddItem(Game.GetText("MapView.item.Add-Nav"), MAP_ADDNAV);
                ship_menu.AddItem(Game.GetText("MapView.item.Clear-All"), MAP_CLEAR);

                action_menu = new Menu(Game.GetText("MapView.menu.ACTION"));
                for (Instruction.ACTION i = 0; i < Instruction.ACTION.NUM_ACTIONS; i++)
                {
                    action_menu.AddItem(Game.GetText("MapView.item." + Instruction.ActionName(i)), MAP_ACTION + i);
                }

                formation_menu = new Menu(Game.GetText("MapView.menu.FORMATION"));
                for (Instruction.FORMATION i = 0; i < Instruction.FORMATION.NUM_FORMATIONS; i++)
                {
                    formation_menu.AddItem(Game.GetText("MapView.item." + Instruction.FormationName(i)), MAP_FORMATION + i);
                }

                speed_menu = new Menu(Game.GetText("MapView.menu.SPEED"));
                speed_menu.AddItem("250", MAP_SPEED + 0);
                speed_menu.AddItem("500", MAP_SPEED + 1);
                speed_menu.AddItem("750", MAP_SPEED + 2);
                speed_menu.AddItem("1000", MAP_SPEED + 3);

                hold_menu = new Menu(Game.GetText("MapView.menu.HOLD"));
                hold_menu.AddItem(Game.GetText("MapView.item.None"), MAP_HOLD + 0);
                hold_menu.AddItem(Game.GetText("MapView.item.1-Minute"), MAP_HOLD + 1);
                hold_menu.AddItem(Game.GetText("MapView.item.5-Minutes"), MAP_HOLD + 2);
                hold_menu.AddItem(Game.GetText("MapView.item.10-Minutes"), MAP_HOLD + 3);
                hold_menu.AddItem(Game.GetText("MapView.item.15-Minutes"), MAP_HOLD + 4);

                farcast_menu = new Menu(Game.GetText("MapView.menu.FARCAST"));
                farcast_menu.AddItem(Game.GetText("MapView.item.Use-Quantum"), MAP_FARCAST + 0);
                farcast_menu.AddItem(Game.GetText("MapView.item.Use-Farcast"), MAP_FARCAST + 1);

                objective_menu = new Menu(Game.GetText("MapView.menu.OBJECTIVE"));

                nav_menu = new Menu(Game.GetText("MapView.menu.NAVPT"));
                nav_menu.AddMenu(Game.GetText("MapView.item.Action"), action_menu);
                nav_menu.AddMenu(Game.GetText("MapView.item.Objective"), objective_menu);
                nav_menu.AddMenu(Game.GetText("MapView.item.Formation"), formation_menu);
                nav_menu.AddMenu(Game.GetText("MapView.item.Speed"), speed_menu);
                nav_menu.AddMenu(Game.GetText("MapView.item.Hold"), hold_menu);
                nav_menu.AddMenu(Game.GetText("MapView.item.Farcast"), farcast_menu);
                nav_menu.AddItem("", 0);
                nav_menu.AddItem(Game.GetText("MapView.item.Add-Nav"), MAP_ADDNAV);
                nav_menu.AddItem(Game.GetText("MapView.item.Del-Nav"), MAP_DELETE);
            }

            else if (campaign != null)
            {
                ship_menu = null;
                speed_menu = null;
                hold_menu = null;
                farcast_menu = null;
                objective_menu = null;
                formation_menu = null;
                nav_menu = null;
            }

            active_menu = map_menu;
        }
        protected virtual void ClearMenu()
        {
            //delete map_menu;
            //delete map_system_menu;
            //delete map_sector_menu;
            //delete ship_menu;
            //delete nav_menu;
            //delete action_menu;
            //delete objective_menu;
            //delete formation_menu;
            //delete speed_menu;
            //delete hold_menu;
            //delete farcast_menu;

            map_menu = null;
            map_system_menu = null;
            map_sector_menu = null;
            ship_menu = null;
            nav_menu = null;
            action_menu = null;
            objective_menu = null;
            formation_menu = null;
            speed_menu = null;
            hold_menu = null;
            farcast_menu = null;
        }
        protected virtual void ProcessMenuItem(int action)
        {
            bool send_nav_data = false;
            bool can_command = true;

            if (ship != null && current_ship != null && ship != current_ship)
            {
                if (ship.GetElement() != null && current_ship.GetElement() != null)
                {
                    if (!ship.GetElement().CanCommand(current_ship.GetElement()))
                    {
                        can_command = false;
                    }
                }
            }

            else if (current_elem != null && NetLobby.GetInstance() != null)
            {
                can_command = false;
            }

            if (action >= MAP_OBJECTIVE)
            {
                int index = action - MAP_OBJECTIVE;

                if (current_navpt != null && can_command)
                {
                    current_navpt.SetTarget(objective_menu.GetItem(index).GetText());
                    send_nav_data = true;
                }
            }

            else if (action >= MAP_FARCAST)
            {
                if (current_navpt != null && can_command)
                {
                    current_navpt.SetFarcast(action - MAP_FARCAST);
                    send_nav_data = true;
                }
            }

            else if (action >= MAP_HOLD)
            {
                int hold_time = 0;
                switch (action)
                {
                    default:
                    case MAP_HOLD + 0: hold_time = 0; break;
                    case MAP_HOLD + 1: hold_time = 60; break;
                    case MAP_HOLD + 2: hold_time = 300; break;
                    case MAP_HOLD + 3: hold_time = 600; break;
                    case MAP_HOLD + 4: hold_time = 900; break;
                }

                if (current_navpt != null && can_command)
                {
                    current_navpt.SetHoldTime(hold_time);
                    send_nav_data = true;
                }
            }

            else if (action >= MAP_SPEED)
            {
                if (current_navpt != null && can_command)
                {
                    current_navpt.SetSpeed((action - MAP_SPEED + 1) * 250);
                    send_nav_data = true;
                }
            }

            else if (action >= MAP_FORMATION)
            {
                if (current_navpt != null && can_command)
                {
                    current_navpt.SetFormation((Instruction.FORMATION)(action - MAP_FORMATION));
                    send_nav_data = true;
                }
            }

            else if (action >= MAP_ACTION)
            {
                if (current_navpt != null && can_command)
                {
                    current_navpt.SetAction((Instruction.ACTION)(action - MAP_ACTION));
                    SelectNavpt(current_navpt);
                    send_nav_data = true;
                }
            }

            else if (action == MAP_ADDNAV)
            {
                string rgn_name = regions[current_region].Name();
                Instruction prior = current_navpt;
                Instruction n = null;

                if (current_ship != null && can_command)
                {
                    Sim sim2 = Sim.GetSim();
                    SimRegion rgn = sim2.FindRegion(rgn_name);
                    Point init_pt = new Point();

                    if (rgn != null)
                    {
                        if (rgn.IsAirSpace())
                            init_pt.Z = 10e3;

                        n = new Instruction(rgn, init_pt);
                    }
                    else
                    {
                        n = new Instruction(rgn_name, init_pt);
                    }

                    n.SetSpeed(500);

                    if (prior != null)
                    {
                        n.SetAction(prior.Action());
                        n.SetFormation(prior.Formation());
                        n.SetSpeed(prior.Speed());
                        n.SetTarget(prior.GetTarget());
                    }

                    current_ship.AddNavPoint(n, prior);
                }

                else if (current_elem != null && can_command)
                {
                    Point init_pt = new Point();

                    if (regions[current_region].Type() == Orbital.OrbitalType.TERRAIN)
                        init_pt.Z = 10e3;

                    n = new Instruction(rgn_name, init_pt);
                    n.SetSpeed(500);

                    if (prior != null)
                    {
                        n.SetAction(prior.Action());
                        n.SetFormation(prior.Formation());
                        n.SetSpeed(prior.Speed());
                        n.SetTarget(prior.GetTarget());
                    }

                    current_elem.AddNavPoint(n, prior);
                }

                if (can_command)
                {
                    current_navpt = n;
                    current_status = Instruction.STATUS.PENDING;
                    adding_navpt = true;
                    captured = SetCapture();
                }
            }

            else if (action == MAP_DELETE)
            {
                if (current_navpt != null && can_command)
                {
                    if (current_ship != null)
                        current_ship.DelNavPoint(current_navpt);
                    else if (current_elem != null && can_command)
                        current_elem.DelNavPoint(current_navpt);

                    SelectNavpt(null);
                }
            }

            else if (action == MAP_CLEAR)
            {
                if (current_ship != null && can_command)
                    current_ship.ClearFlightPlan();
                else if (current_elem != null && can_command)
                    current_elem.ClearFlightPlan();

                SelectNavpt(null);
            }

            else if (action >= MAP_NAV)
            {
            }

            else if (action >= MAP_SHIP)
            {
            }

            else if (action >= MAP_SECTOR)
            {
                int index = action - MAP_SECTOR;

                if (index < regions.Count)
                    current_region = index;

                if (view_mode == VIEW_SYSTEM)
                {
                    Orbital s = regions[current_region];
                    SetupScroll(s);
                }
            }

            else if (system_list.Count > 0 && action >= MAP_SYSTEM)
            {
                int index = action - MAP_SYSTEM;

                if (index < system_list.Count)
                    SetSystem(system_list[index]);
            }

            else
            {
            }

            Sim sim = Sim.GetSim();
            if (send_nav_data && sim != null)
            {
                Ship s = current_ship;

                if (s != null && s.GetElement() != null)
                {
                    Element elem = s.GetElement();
                    int index = elem.GetNavIndex(current_navpt);

                    if (index >= 0)
                        NetUtil.SendNavData(false, elem, index - 1, current_navpt);
                }
            }
        }
        protected virtual bool SetCapture()
        {
            EventDispatch dispatch = EventDispatch.GetInstance();
            if (dispatch != null)
                return dispatch.CaptureMouse(this) != 0 ? true : false;

            return false;
        }
        protected virtual bool ReleaseCapture()
        {
            EventDispatch dispatch = EventDispatch.GetInstance();
            if (dispatch != null)
                return dispatch.ReleaseMouse(this) != 0 ? true : false;

            return false;
        }

        protected virtual void DrawTabbedText(FontItem font, string text) { throw new NotImplementedException(); }

        protected bool IsClutter(Ship test)
        {
            // get leader:
            Ship lead = test.GetLeader();

            if (lead == test)   // this is the leader:
                return false;

            // too close?
            if (lead != null)
            {
                POINT testloc = new POINT(), leadloc = new POINT();

                GetShipLoc(test, ref testloc);
                GetShipLoc(lead, ref leadloc);

                double dx = testloc.X - leadloc.X;
                double dy = testloc.Y - leadloc.Y;
                double d = dx * dx + dy * dy;

                if (d <= 64)
                    return true;
            }

            return false;
        }
        protected bool IsCrowded(Ship test)
        {
            POINT testloc = new POINT(), refloc = new POINT();
            Sim sim = Sim.GetSim();
            Orbital rgn = regions[current_region];
            SimRegion simrgn = sim.FindRegion(rgn.Name());

            if (simrgn != null)
            {
                GetShipLoc(test, ref testloc);

                foreach (Ship ref_ in simrgn.Ships())
                {

                    // too close?
                    if (ref_ != null && ref_ != test)
                    {
                        GetShipLoc(ref_, ref refloc);

                        double dx = testloc.X - refloc.X;
                        double dy = testloc.Y - refloc.Y;
                        double d = dx * dx + dy * dy;

                        if (d <= 64)
                            return true;
                    }
                }
            }

            return false;
        }
        protected bool IsCrowded(MissionElement test)
        {
            POINT testloc = new POINT(), refloc = new POINT();
            Sim sim = Sim.GetSim();
            Orbital rgn = regions[current_region];

            if (mission != null)
            {
                GetElemLoc(test, ref testloc);

                foreach (MissionElement ref_ in mission.GetElements())
                {
                    if (ref_ != null && ref_ != test && ref_.Region().Equals(rgn.Name(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        GetElemLoc(ref_, ref refloc);

                        double dx = testloc.X - refloc.X;
                        double dy = testloc.Y - refloc.Y;
                        double d = dx * dx + dy * dy;

                        if (d <= 64)
                            return true;
                    }
                }
            }

            return false;
        }
        protected void GetShipLoc(Ship s, ref POINT shiploc)
        {
            double cx = rect.w / 2;
            double cy = rect.h / 2;

            c = (cx < cy) ? cx : cy;
            r = system.Radius() * zoom;

            OrbitalRegion rgn = (OrbitalRegion)regions[current_region];

            if (view_mode == VIEW_REGION)
            {
                if (rgn == null) return;
                r = rgn.Radius() * zoom;
            }

            double scale = c / r;

            double ox = offset_x * scale;
            double oy = offset_y * scale;

            double rlx = 0;
            double rly = 0;

            if (view_mode == VIEW_SYSTEM)
            {
                rgn = system.ActiveRegion();

                if (rgn != null)
                {
                    rlx = rgn.Location().X;
                    rly = rgn.Location().Y;
                }
            }

            if (view_mode == VIEW_SYSTEM ||
                    (view_mode == VIEW_REGION && rgn == s.GetRegion().GetOrbitalRegion()))
            {
                double sx = (s.Location().X + rlx) * scale;
                double sy = (s.Location().Y + rly) * scale;

                shiploc.X = (int)(cx + sx + ox);
                shiploc.Y = (int)(cy + sy + oy);
            }
            else
            {
                shiploc.X = -1;
                shiploc.Y = -1;
            }
        }
        protected void GetElemLoc(MissionElement s, ref POINT shiploc)
        {
            double cx = rect.w / 2;
            double cy = rect.h / 2;

            c = (cx < cy) ? cx : cy;
            r = system.Radius() * zoom;

            OrbitalRegion rgn = (OrbitalRegion)regions[current_region];

            if (view_mode == VIEW_REGION)
            {
                if (rgn == null) return;
                r = rgn.Radius() * zoom;
            }

            double scale = c / r;

            double ox = offset_x * scale;
            double oy = offset_y * scale;

            double rlx = 0;
            double rly = 0;

            if (view_mode == VIEW_SYSTEM)
            {
                rgn = system.ActiveRegion();

                if (rgn != null)
                {
                    rlx = rgn.Location().X;
                    rly = rgn.Location().Y;
                }
            }

            if (view_mode == VIEW_SYSTEM ||
                    (view_mode == VIEW_REGION && s.Region().Equals(rgn.Name(), StringComparison.InvariantCultureIgnoreCase)))
            {
                double sx = (s.Location().X + rlx) * scale;
                double sy = (s.Location().Y + rly) * scale;

                shiploc.X = (int)(cx + sx + ox);
                shiploc.Y = (int)(cy + sy + oy);
            }
            else
            {
                shiploc.X = -1;
                shiploc.Y = -1;
            }
        }
        protected void SelectShip(Ship selship)
        {
            if (selship != current_ship)
            {
                current_ship = selship;

                if (current_ship != null)
                {
                    if (current_ship.Life() == 0 || current_ship.IsDying() || current_ship.IsDead())
                    {
                        current_ship = null;
                    }
                    else
                    {
                        Observe(current_ship);
                    }
                }
            }

            SelectNavpt(null);
        }
        protected void SelectElem(MissionElement elem)
        {
            if (elem != current_elem)
            {
                current_elem = elem;

                if (current_elem != null)
                {
                    if (current_elem.IsStarship())
                    {
                        ship_menu.GetItem(3).SetEnabled(true);
                    }

                    else if (current_elem.IsDropship())
                    {
                        ship_menu.GetItem(3).SetEnabled(true);
                    }

                    else
                    {
                        ship_menu.GetItem(3).SetEnabled(false);
                    }
                }
            }

            SelectNavpt(null);
        }
        protected void SelectNavpt(Instruction navpt)
        {
            current_navpt = navpt;

            if (current_navpt != null)
            {
                current_status = current_navpt.Status();

                List<string> ships = new List<string>();
                objective_menu.ClearItems();

                switch (current_navpt.Action())
                {
                    case Instruction.ACTION.VECTOR:
                    case Instruction.ACTION.LAUNCH:
                    case Instruction.ACTION.PATROL:
                    case Instruction.ACTION.SWEEP:
                    case Instruction.ACTION.RECON:
                        objective_menu.AddItem(Game.GetText("MapView.item.not-available"), 0);
                        objective_menu.GetItem(0).SetEnabled(false);
                        break;

                    case Instruction.ACTION.DOCK:
                        FindShips(true, true, true, false, ships);
                        break;

                    case Instruction.ACTION.DEFEND:
                        FindShips(true, true, true, false, ships);
                        break;

                    case Instruction.ACTION.ESCORT:
                        FindShips(true, false, true, true, ships);
                        break;

                    case Instruction.ACTION.INTERCEPT:
                        FindShips(false, false, false, true, ships);
                        break;

                    case Instruction.ACTION.ASSAULT:
                        FindShips(false, false, true, false, ships);
                        break;

                    case Instruction.ACTION.STRIKE:
                        FindShips(false, true, false, false, ships);
                        break;
                }

                for (int i = 0; i < ships.Count; i++)
                    objective_menu.AddItem(ships[i], MAP_OBJECTIVE + i);

                ships.Destroy();
            }
            else
            {
                objective_menu.ClearItems();
                objective_menu.AddItem(Game.GetText("MapView.item.not-available"), 0);
                objective_menu.GetItem(0).SetEnabled(false);
            }
        }
        protected void FindShips(bool friendly, bool station, bool starship, bool dropship,
                                    List<string> result)
        {
            if (mission != null)
            {
                for (int i = 0; i < mission.GetElements().Count; i++)
                {
                    MissionElement elem = mission.GetElements()[i];

                    if (elem.IsSquadron()) continue;
                    if (!station && elem.IsStatic()) continue;
                    if (!starship && elem.IsStarship()) continue;
                    if (!dropship && elem.IsDropship()) continue;

                    if (!editor && friendly && elem.GetIFF() > 0 && elem.GetIFF() != mission.Team())
                        continue;

                    if (!editor && !friendly && (elem.GetIFF() == 0 || elem.GetIFF() == mission.Team()))
                        continue;

                    result.Add(elem.Name());
                }
            }

            else if (ship != null)
            {
                Sim sim = Sim.GetSim();

                if (sim != null)
                {
                    for (int r = 0; r < sim.GetRegions().Count; r++)
                    {
                        SimRegion rgn = sim.GetRegions()[r];

                        for (int i = 0; i < rgn.Ships().Count; i++)
                        {
                            Ship s = rgn.Ships()[i];

                            if (!station && s.IsStatic()) continue;
                            if (!starship && s.IsStarship()) continue;
                            if (!dropship && s.IsDropship()) continue;

                            if (friendly && s.GetIFF() > 0 && s.GetIFF() != ship.GetIFF())
                                continue;

                            if (!friendly && (s.GetIFF() == 0 || s.GetIFF() == ship.GetIFF()))
                                continue;

                            result.Add(s.Name());
                        }
                    }
                }
            }
        }
        protected void SetupScroll(Orbital s)
        {
            switch (view_mode)
            {
                case VIEW_GALAXY:
                    zoom = 1;
                    offset_x = 0;
                    offset_y = 0;
                    scrolling = 0;
                    break;

                case VIEW_SYSTEM:
                    if (s == null)
                    {
                        offset_x = 0;
                        offset_y = 0;
                        scrolling = 0;
                    }
                    else
                    {
                        scroll_x = (offset_x + s.Location().X) / 5.0;
                        scroll_y = (offset_y + s.Location().Y) / 5.0;
                        scrolling = 5;
                    }
                    break;

                case VIEW_REGION:
                    if (current_navpt != null)
                    {
                        // don't move the map
                        scrolling = 0;
                    }
                    else if (current_ship != null)
                    {
                        Point sloc = current_ship.Location().OtherHand();

                        if (!IsVisible(sloc))
                        {
                            scroll_x = (offset_x + sloc.X) / 5.0;
                            scroll_y = (offset_y + sloc.Y) / 5.0;
                            scrolling = 5;
                        }
                        else
                        {
                            scroll_x = 0;
                            scroll_y = 0;
                            scrolling = 0;
                        }
                    }
                    else if (current_elem != null)
                    {
                        Point sloc = current_elem.Location();

                        if (!IsVisible(sloc))
                        {
                            scroll_x = (offset_x + sloc.X) / 5.0;
                            scroll_y = (offset_y + sloc.Y) / 5.0;
                            scrolling = 5;
                        }
                        else
                        {
                            scroll_x = 0;
                            scroll_y = 0;
                            scrolling = 0;
                        }
                    }
                    else
                    {
                        offset_x = 0;
                        offset_y = 0;
                        scrolling = 0;
                    }
                    break;
            }
        }


        protected double GetMinRadius(Orbital.OrbitalType type)
        {
            switch (type)
            {
                case Orbital.OrbitalType.STAR: return 8;
                case Orbital.OrbitalType.PLANET: return 4;
                case Orbital.OrbitalType.MOON: return 2;
                case Orbital.OrbitalType.REGION: return 2;
            }

            return 0;
        }
        static void ColorizeBitmap(Bitmap img, Color color)
        {
            int w = img.Width();
            int h = img.Height();
#if TODO
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    Color c = img.GetColor(x, y);

                    if (c != Color.black && c != Color.white)
                    {
                        img.SetColor(x, y, color.ShadeColor(c.Blue() / 2));
                    }
                }
            }

            img.AutoMask();
#endif
            throw new NotImplementedException();
        }

        public string GetDescription()
        {
            throw new NotImplementedException();
        }

        public bool HasFocus()
        {
            throw new NotImplementedException();
        }

        public void KillFocus()
        {
            throw new NotImplementedException();
        }

        public int OnKeyDown(int vk, int flags)
        {
            throw new NotImplementedException();
        }

        public int OnMouseEnter(int x, int y)
        {
            throw new NotImplementedException();
        }

        public int OnMouseExit(int x, int y)
        {
            throw new NotImplementedException();
        }

        public int OnMouseWheel(int wheel)
        {
            throw new NotImplementedException();
        }

        public int OnSelect()
        {
            throw new NotImplementedException();
        }

        public void SetFocus()
        {
            throw new NotImplementedException();
        }

        public void Observe(SimObject obj)
        {
            simObserver.Observe(obj);
        }

        public void Ignore(SimObject obj)
        {
            simObserver.Ignore(obj);
        }
        protected SimObserver simObserver = new SimObserver();
        protected string title;
        protected Rect rect;
        protected Campaign campaign;
        protected Mission mission;
        protected List<StarSystem> system_list = new List<StarSystem>();
        protected StarSystem system;
        protected List<Orbital> stars = new List<Orbital>();
        protected List<Orbital> planets = new List<Orbital>();
        protected List<Orbital> regions = new List<Orbital>();
        protected Ship ship;
        protected Bitmap galaxy_image;
        protected bool editor;

        protected int current_star;
        protected int current_planet;
        protected int current_region;
        protected Ship current_ship;
        protected MissionElement current_elem;
        protected Instruction current_navpt;
        protected Instruction.STATUS current_status;

        protected int view_mode;
        protected int seln_mode;
        protected bool captured;
        protected bool dragging;
        protected bool adding_navpt;
        bool moving_navpt;
        protected bool moving_elem;
        protected int scrolling;
        protected int mouse_x;
        protected int mouse_y;
        protected SimElements.CLASSIFICATION ship_filter;

        protected double zoom;
        protected double[] view_zoom = new double[3];
        protected double offset_x;
        protected double offset_y;
        protected double[] view_offset_x = new double[3];
        protected double[] view_offset_y = new double[3];
        protected double c, r;
        protected double scroll_x;
        protected double scroll_y;
        protected double click_x;
        protected double click_y;

        protected FontItem font;
        protected FontItem title_font;

        protected ActiveWindow active_window;
        protected Menu active_menu;

        protected Menu map_menu;
        protected Menu map_system_menu;
        protected Menu map_sector_menu;
        protected Menu ship_menu;
        protected Menu nav_menu;
        protected Menu action_menu;
        protected Menu objective_menu;
        protected Menu formation_menu;
        protected Menu speed_menu;
        protected Menu hold_menu;
        protected Menu farcast_menu;

        protected MenuView menu_view;
        // Supported Selection Modes:

        private const int SELECT_NONE = -1;
        private const int SELECT_SYSTEM = 0;
        private const int SELECT_PLANET = 1;
        private const int SELECT_REGION = 2;
        private const int SELECT_STATION = 3;
        private const int SELECT_STARSHIP = 4;
        private const int SELECT_FIGHTER = 5;
        private const int SELECT_NAVPT = 6;

        private const int VIEW_GALAXY = 0;
        private const int VIEW_SYSTEM = 1;
        private const int VIEW_REGION = 2;

    }
}
