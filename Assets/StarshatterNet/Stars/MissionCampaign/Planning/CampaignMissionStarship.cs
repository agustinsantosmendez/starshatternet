﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CampaignMissionStarship.h/CampaignMissionStarship.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CampaignMissionStarship generates missions and mission
    info for the player's STARSHIP GROUP as part of a
    dynamic campaign.
*/

using System;
using System.Collections.Generic;
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.StarSystems;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign.Planning
{
    public class CampaignMissionStarship
    {
        public static bool dump_missions = false;

        public CampaignMissionStarship(Campaign campaign)
        {
            campaign = null; player_group = null; player_unit = null; mission = null; player = null;
            strike_group = null; strike_target = null; prime_target = null;
            ward = null; escort = null; ownside = 0; enemy = -1; mission_type = 0;

            if (campaign == null || campaign.GetPlayerGroup() == null)
            {
                ErrLogger.PrintLine("ERROR - CMS campaign=0x{0:X8} player_group=0x{1:X8} ",
                            campaign, campaign != null ? campaign.GetPlayerGroup().GetHashCode() : 0);
                return;
            }

            player_group = campaign.GetPlayerGroup();
            player_unit = campaign.GetPlayerUnit();
        }
        //virtual ~CampaignMissionStarship();

        static int id_key = 1;
        public virtual void CreateMission(CampaignMissionRequest req)
        {
            if (campaign == null || req == null)
                return;

            ErrLogger.PrintLine("\n-----------------------------------------------");
            if (!string.IsNullOrEmpty(req.Script()))
                ErrLogger.PrintLine("CMS CreateMission() request: {0} '{1}'",
                             Mission.RoleName(req.Type()), req.Script());

            else
                ErrLogger.PrintLine("CMS CreateMission() request: {0} {1}",
                            Mission.RoleName(req.Type()),
                            req.GetObjective() != null ? req.GetObjective().Name() : "(no target)");

            request = req;

            if (player_group == null)
                return;

            if (request.GetPrimaryGroup() != player_group)
            {
                player_group = request.GetPrimaryGroup();
                player_unit = null;
            }

            ownside = player_group.GetIFF();
            mission_info = null;

            for (int i = 0; i < campaign.GetCombatants().Count; i++)
            {
                int iff = campaign.GetCombatants()[i].GetIFF();
                if (iff > 0 && iff != ownside)
                {
                    enemy = iff;
                    break;
                }
            }

            GenerateMission(id_key++);
            DefineMissionObjectives();

            MissionInfo info = DescribeMission();

            if (info != null)
            {
                campaign.GetMissionList().Add(info);

                ErrLogger.PrintLine("CMS Created {0:D3} '{1}' {2}\n",
                                    info.id, info.name, Mission.RoleName(mission.Type()));

                if (dump_missions)
                {
                    string script = mission.Serialize();
                    string fname = string.Format("msn{0:D3}.def", info.id);
                    ReaderSupport.WriteAllDataText(fname, string.Format("{0}\n", script));
                }
            }
            else
            {
                ErrLogger.PrintLine("CMS failed to create mission.\n");
            }
        }


        protected virtual Mission GenerateMission(int id)
        {
            bool found = false;

            SelectType();

            if (request != null && !string.IsNullOrEmpty(request.Script()))
            {
                MissionTemplate mt = new MissionTemplate(id, request.Script(), campaign.Path());
                if (mt != null)
                    mt.SetPlayerSquadron(player_group);
                mission = mt;
                found = true;
            }

            else
            {
                mission_info = campaign.FindMissionTemplate(mission_type, player_group);

                found = mission_info != null;

                if (found)
                {
                    MissionTemplate mt = new MissionTemplate(id, mission_info.script, campaign.Path());
                    if (mt != null)
                        mt.SetPlayerSquadron(player_group);
                    mission = mt;
                }
                else
                {
                    mission = new Mission(id);
                    if (mission != null)
                        mission.SetType(mission_type);
                }
            }

            if (mission == null || player_group == null)
            {
                Exit();
                return null;
            }

            string name = string.Format("Starship Mission {0}", id);

            mission.SetName(name);
            mission.SetTeam(player_group.GetIFF());
            mission.SetStart(request.StartTime());

            SelectRegion();
            GenerateStandardElements();

            if (!found)
            {
                GenerateMissionElements();
                mission.SetOK(true);
                mission.Validate();
            }

            else
            {
                CreatePlayer();
                mission.Load();

                if (mission.IsOK())
                {
                    player = mission.GetPlayer();
                    prime_target = mission.GetTarget();
                    ward = mission.GetWard();
                }

                // if there was a problem, scrap the mission
                // and start over:
                else
                {
                    //delete mission;

                    mission = new Mission(id);

                    if (mission == null)
                    {
                        Exit();
                        return null;
                    }

                    mission.SetType(mission_type);
                    mission.SetName(name);
                    mission.SetTeam(player_group.GetIFF());
                    mission.SetStart(request.StartTime());

                    SelectRegion();
                    GenerateStandardElements();
                    GenerateMissionElements();

                    mission.SetOK(true);
                    mission.Validate();
                }
            }

            return mission;
        }
        protected virtual void SelectType()
        {
            if (request != null)
                mission_type = request.Type();

            else
                mission_type = Mission.TYPE.PATROL;

            if (player_unit != null && player_unit.GetShipClass() == SimElements.CLASSIFICATION.CARRIER)
                mission_type = Mission.TYPE.FLIGHT_OPS;
        }

        protected virtual void SelectRegion()
        {
            if (player_group == null)
            {
                ErrLogger.PrintLine("WARNING: CMS - no player group in SelectRegion");
                return;
            }

            CombatZone zone = player_group.GetAssignedZone();

            if (zone == null)
                zone = player_group.GetCurrentZone();

            if (zone != null)
            {
                mission.SetStarSystem(campaign.GetSystem(zone.System()));

                if (zone.HasRegion(player_group.GetRegion()))
                    mission.SetRegion(player_group.GetRegion());

                else
                    mission.SetRegion(zone.GetRegions()[0]);
            }

            else
            {
                ErrLogger.PrintLine("WARNING: CMS - No zone for '{0}'\n", player_group.Name());

                StarSystem s = campaign.GetSystemList()[0];

                mission.SetStarSystem(s);
                mission.SetRegion(s.Regions()[0].Name());
            }
        }

        protected virtual void GenerateStandardElements()
        {
            foreach (CombatZone z in campaign.GetZones())
            {
                foreach (ZoneForce force in z.GetForces())
                {
                    foreach (CombatGroup g in force.GetGroups())
                    {
                        switch (g.Type())
                        {
                            case CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON:
                            case CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON:
                            case CombatGroup.GROUP_TYPE.ATTACK_SQUADRON:
                            case CombatGroup.GROUP_TYPE.LCA_SQUADRON:
                                CreateSquadron(g);
                                break;

                            case CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON:
                            case CombatGroup.GROUP_TYPE.BATTLE_GROUP:
                            case CombatGroup.GROUP_TYPE.CARRIER_GROUP:
                                CreateElements(g);
                                break;

                            case CombatGroup.GROUP_TYPE.MINEFIELD:
                            case CombatGroup.GROUP_TYPE.BATTERY:
                            case CombatGroup.GROUP_TYPE.MISSILE:
                            case CombatGroup.GROUP_TYPE.STATION:
                            case CombatGroup.GROUP_TYPE.STARBASE:
                            case CombatGroup.GROUP_TYPE.SUPPORT:
                            case CombatGroup.GROUP_TYPE.COURIER:
                            case CombatGroup.GROUP_TYPE.MEDICAL:
                            case CombatGroup.GROUP_TYPE.SUPPLY:
                            case CombatGroup.GROUP_TYPE.REPAIR:
                                CreateElements(g);
                                break;

                            case CombatGroup.GROUP_TYPE.CIVILIAN:
                            case CombatGroup.GROUP_TYPE.WAR_PRODUCTION:
                            case CombatGroup.GROUP_TYPE.FACTORY:
                            case CombatGroup.GROUP_TYPE.REFINERY:
                            case CombatGroup.GROUP_TYPE.RESOURCE:
                            case CombatGroup.GROUP_TYPE.INFRASTRUCTURE:
                            case CombatGroup.GROUP_TYPE.TRANSPORT:
                            case CombatGroup.GROUP_TYPE.NETWORK:
                            case CombatGroup.GROUP_TYPE.HABITAT:
                            case CombatGroup.GROUP_TYPE.STORAGE:
                            case CombatGroup.GROUP_TYPE.NON_COM:
                                CreateElements(g);
                                break;
                        }
                    }
                }
            }
        }
        protected virtual void GenerateMissionElements()
        {
            CreatePlayer();
            CreateWards();
            CreateTargets();

            if (ward != null && player != null)
            {
                Instruction obj = new Instruction(Instruction.ACTION.ESCORT, ward.Name());

                if (obj != null)
                {
                    switch (mission.Type())
                    {
                        case Mission.TYPE.ESCORT_FREIGHT:
                            obj.SetTargetDesc("the star freighter " + ward.Name());
                            break;

                        case Mission.TYPE.ESCORT_SHUTTLE:
                            obj.SetTargetDesc("the shuttle " + ward.Name());
                            break;

                        case Mission.TYPE.ESCORT_STRIKE:
                            obj.SetTargetDesc("the " + ward.Name() + " strike package");
                            break;

                        default:
                            if (ward.GetCombatGroup() != null)
                            {
                                obj.SetTargetDesc("the " + ward.GetCombatGroup().GetDescription());
                            }
                            else
                            {
                                obj.SetTargetDesc("the " + ward.Name());
                            }
                            break;
                    }

                    player.AddObjective(obj);
                }
            }
        }
        protected virtual void CreateElements(CombatGroup g)
        {
            MissionElement elem = null;
            List<CombatUnit> units = g.GetUnits();

            CombatUnit cmdr = null;

            for (int i = 0; i < units.Count; i++)
            {
                elem = CreateSingleElement(g, units[i]);

                if (elem != null)
                {
                    if (cmdr != null)
                    {
                        cmdr = units[i];

                        if (player_group != null && player_group.GetIFF() == g.GetIFF())
                        {
                            // the grand admiral is all powerful!
                            Player player = Player.GetCurrentPlayer();
                            if (player != null && player.Rank() >= 10)
                            {
                                elem.SetCommander(player_group.Name());
                            }
                        }
                    }
                    else
                    {
                        elem.SetCommander(cmdr.Name());

                        if (g.Type() == CombatGroup.GROUP_TYPE.CARRIER_GROUP &&
                                elem.MissionRole() == Mission.TYPE.ESCORT)
                        {
                            Instruction obj = new Instruction(Instruction.ACTION.ESCORT, cmdr.Name());
                            if (obj != null)
                            {
                                obj.SetTargetDesc("the " + g.GetDescription());
                                elem.AddObjective(obj);
                            }
                        }
                    }

                    mission.AddElement(elem);
                }
            }
        }
        protected virtual void CreateSquadron(CombatGroup g)
        {
            if (g == null || g.IsReserve()) return;

            CombatUnit fighter = g.GetUnits()[0];
            CombatUnit carrier = FindCarrier(g);

            if (fighter == null || carrier == null) return;

            int live_count = fighter.LiveCount();
            int maint_count = (live_count > 4) ? live_count / 2 : 0;

            MissionElement elem = new MissionElement();

            if (elem == null)
            {
                Exit();
                return;
            }

            elem.SetName(g.Name());
            elem.SetElementID(pkg_id++);

            elem.SetDesign(fighter.GetDesign());
            elem.SetCount(fighter.Count());
            elem.SetDeadCount(fighter.DeadCount());
            elem.SetMaintCount(maint_count);
            elem.SetIFF(fighter.GetIFF());
            elem.SetIntelLevel(g.IntelLevel());
            elem.SetRegion(fighter.GetRegion());

            elem.SetCarrier(carrier.Name());
            elem.SetCommander(carrier.Name());
            elem.SetLocation(carrier.Location() + RandomHelper.RandomPoint());

            elem.SetCombatGroup(g);
            elem.SetCombatUnit(fighter);

            mission.AddElement(elem);
        }

        protected virtual void CreatePlayer()
        {
            // prepare elements for the player's group
            MissionElement elem = null;

            if (player_group != null)
            {
                foreach (MissionElement e in mission.GetElements())
                {
                    if (e.GetCombatGroup() == player_group)
                    {
                        player_group_elements.Add(e);

                        // match the player to the requested unit, if possible:
                        if ((player_unit == null && elem == null) || (player_unit == e.GetCombatUnit()))
                        {
                            elem = e;
                        }
                    }
                }
            }

            if (elem != null)
            {
                elem.SetPlayer(1);
                elem.SetCommandAI(0);
                player = elem;
            }
            else if (player_group != null)
            {
                ErrLogger.PrintLine("CMS GenerateMissionElements() could not find player element '{0}'", player_group.Name());
            }
            else
            {
                ErrLogger.PrintLine("CMS GenerateMissionElements() could not find player element (no player group)");
            }
        }

        protected virtual void CreateWards()
        {
            switch (mission.Type())
            {
                case Mission.TYPE.ESCORT_FREIGHT: CreateWardFreight(); break;
                default: break;
            }
        }

        protected virtual void CreateWardFreight()
        {
            if (mission == null || mission.GetStarSystem() == null || player_group == null) return;

            CombatGroup freight = null;

            if (request != null)
                freight = request.GetObjective();

            if (freight == null)
                freight = campaign.FindGroup(ownside, CombatGroup.GROUP_TYPE.FREIGHT);

            if (freight == null || freight.CalcValue() < 1) return;

            CombatUnit unit = freight.GetNextUnit();
            if (unit == null) return;

            MissionElement elem = CreateSingleElement(freight, unit);
            if (elem == null) return;

            elem.SetMissionRole(Mission.TYPE.CARGO);
            elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
            elem.SetRegion(player_group.GetRegion());

            ward = elem;
            mission.AddElement(elem);


            StarSystem system = mission.GetStarSystem();
            OrbitalRegion rgn1 = system.FindRegion(elem.Region());
            Point delta = rgn1.Location() - rgn1.Primary().Location();
            Point navpt_loc = elem.Location();
            Instruction n = null;

            delta.Normalize();
            delta *= 200.0e3;

            navpt_loc += delta;

            n = new Instruction(elem.Region(), navpt_loc, Instruction.ACTION.VECTOR);
            if (n != null)
            {
                n.SetSpeed(500);
                elem.AddNavPoint(n);
            }

            string rgn2 = elem.Region();
            List<CombatZone> zones = campaign.GetZones();
            if (zones[zones.Count - 1].HasRegion(rgn2))
                rgn2 = zones[0].GetRegions()[0];
            else
                rgn2 = zones[zones.Count - 1].GetRegions()[0];

            n = new Instruction(rgn2, new Point(0, 0, 0), Instruction.ACTION.VECTOR);
            if (n != null)
            {
                n.SetSpeed(750);
                elem.AddNavPoint(n);
            }
        }

        protected virtual void CreateEscorts()
        {
        }


        protected virtual void CreateTargets()
        {
            if (player_group != null && player_group.Type() == CombatGroup.GROUP_TYPE.CARRIER_GROUP)
            {
                CreateTargetsCarrier();
            }

            else
            {
                switch (mission.Type())
                {
                    default:
                    case Mission.TYPE.PATROL: CreateTargetsPatrol(); break;
                    case Mission.TYPE.ASSAULT:
                    case Mission.TYPE.STRIKE: CreateTargetsAssault(); break;
                    case Mission.TYPE.ESCORT_FREIGHT: CreateTargetsFreightEscort(); break;
                }
            }
        }
        protected virtual void CreateTargetsAssault()
        {
            if (player == null) return;

            CombatGroup assigned = null;

            if (request != null)
                assigned = request.GetObjective();

            if (assigned != null)
            {
                CreateElements(assigned);

                foreach (MissionElement elem in mission.GetElements())
                {
                    if (elem.GetCombatGroup() == assigned)
                    {
                        if (prime_target == null)
                        {
                            prime_target = elem;

                            MissionElement player_lead = player_group_elements[0];

                            if (player_lead == null) return;

                            Instruction obj = new Instruction(Instruction.ACTION.ASSAULT, prime_target.Name());
                            if (obj != null)
                            {
                                obj.SetTargetDesc("preplanned target '" + prime_target.Name() + "'");
                                player_lead.AddObjective(obj);
                            }

                            // create flight plan:
                            RLoc rloc = new RLoc();
                            RLoc ref_ = null;
                            Vector3D dummy = new Vector3D(0, 0, 0);
                            Instruction instr = null;
                            Point loc = player_lead.Location();
                            Point tgt = prime_target.Location();
                            Point mid;

                            mid = loc + (prime_target.Location() - loc) * 0.35;

                            rloc.SetReferenceLoc(null);
                            rloc.SetBaseLocation(mid);
                            rloc.SetDistance(50e3);
                            rloc.SetDistanceVar(5e3);
                            rloc.SetAzimuth(90 * ConstantsF.DEGREES);
                            rloc.SetAzimuthVar(45 * ConstantsF.DEGREES);

                            instr = new Instruction(prime_target.Region(), dummy, Instruction.ACTION.VECTOR);

                            if (instr == null)
                                return;

                            instr.SetSpeed(750);
                            instr.SetRLoc(rloc);

                            ref_ = instr.GetRLoc();

                            player_lead.AddNavPoint(instr);

                            for (int i = 1; i < player_group_elements.Count; i++)
                            {
                                MissionElement pge = player_group_elements[i];
                                RLoc rloc2 = new RLoc();

                                rloc2.SetReferenceLoc(ref_);
                                rloc2.SetDistance(50e3);
                                rloc2.SetDistanceVar(5e3);

                                instr = new Instruction(prime_target.Region(), dummy, Instruction.ACTION.VECTOR);

                                if (instr == null)
                                    return;

                                instr.SetSpeed(750);
                                instr.SetRLoc(rloc2);

                                pge.AddNavPoint(instr);
                            }

                            double extra = 10e3;

                            if (prime_target != null && prime_target.GetDesign() != null)
                            {
                                switch (prime_target.GetDesign().type)
                                {
                                    default: extra = 20e3; break;
                                    case SimElements.CLASSIFICATION.FRIGATE: extra = 25e3; break;
                                    case SimElements.CLASSIFICATION.DESTROYER: extra = 30e3; break;
                                    case SimElements.CLASSIFICATION.CRUISER: extra = 50e3; break;
                                    case SimElements.CLASSIFICATION.BATTLESHIP: extra = 70e3; break;
                                    case SimElements.CLASSIFICATION.DREADNAUGHT: extra = 80e3; break;
                                    case SimElements.CLASSIFICATION.SWACS: extra = 30e3; break;
                                    case SimElements.CLASSIFICATION.CARRIER: extra = 90e3; break;
                                }
                            }

                            rloc.SetReferenceLoc(null);
                            rloc.SetBaseLocation(tgt);
                            rloc.SetDistance(100e3 + extra);
                            rloc.SetDistanceVar(15e3);
                            rloc.SetAzimuth(90 * ConstantsF.DEGREES);
                            rloc.SetAzimuthVar(45 * ConstantsF.DEGREES);

                            instr = new Instruction(prime_target.Region(), dummy, Instruction.ACTION.ASSAULT);

                            if (instr == null)
                                return;

                            instr.SetSpeed(500);
                            instr.SetRLoc(rloc);
                            instr.SetTarget(prime_target.Name());

                            ref_ = instr.GetRLoc();

                            player_lead.AddNavPoint(instr);

                            for (int i = 1; i < player_group_elements.Count; i++)
                            {
                                MissionElement pge = player_group_elements[i];
                                RLoc rloc2 = new RLoc();

                                rloc2.SetReferenceLoc(ref_);
                                rloc2.SetDistance(50e3);
                                rloc2.SetDistanceVar(5e3);

                                instr = new Instruction(prime_target.Region(), dummy, Instruction.ACTION.ASSAULT);

                                if (instr == null)
                                    return;

                                instr.SetSpeed(500);
                                instr.SetRLoc(rloc2);
                                instr.SetTarget(prime_target.Name());

                                pge.AddNavPoint(instr);
                            }
                        }
                    }
                }
            }
        }

        protected virtual void CreateTargetsPatrol()
        {
            if (player_group == null || player == null) return;

            string region = player_group.GetRegion();
            Point base_loc = player.Location();
            Point patrol_loc = base_loc +
            RandomHelper.RandomDirection() * RandomHelper.Random(170e3, 250e3);

            Instruction n = new Instruction(region, patrol_loc, Instruction.ACTION.PATROL);
            player.AddNavPoint(n);

            for (int i = 1; i < player_group_elements.Count; i++)
            {
                MissionElement elem = player_group_elements[i];

                n = new Instruction(region,
                 patrol_loc + RandomHelper.RandomDirection() * RandomHelper.Random(20e3, 40e3),
                 Instruction.ACTION.PATROL);
                if (n != null)
                    elem.AddNavPoint(n);
            }

            Point loc2 = patrol_loc + RandomHelper.RandomDirection() * RandomHelper.Random(150e3, 200e3);

            n = new Instruction(region, loc2, Instruction.ACTION.PATROL);
            if (n != null)
                player.AddNavPoint(n);

            for (int i = 1; i < player_group_elements.Count; i++)
            {
                MissionElement elem = player_group_elements[i];

                n = new Instruction(region,
                    loc2 + RandomHelper.RandomDirection() * RandomHelper.Random(20e3, 40e3),
                 Instruction.ACTION.PATROL);

                if (n != null)
                    elem.AddNavPoint(n);
            }

            int ntargets = 2 + (RandomHelper.RandomChance() ? 1 : 0);
            int ntries = 8;

            while (ntargets > 0 && ntries > 0)
            {
                Point target_loc = RandomHelper.RandomChance() ? patrol_loc : loc2;
                int t = CreateRandomTarget(region, target_loc);
                ntargets -= t;
                if (t < 1) ntries--;
            }

            Instruction obj = new Instruction(n);
            if (obj != null)
            {
                obj.SetTargetDesc("inbound enemy units");
                player.AddObjective(obj);
            }
        }
        protected virtual void CreateTargetsCarrier()
        {
            if (player_group == null || player == null) return;

            string region = player_group.GetRegion();
            Point base_loc = player.Location();
            Point patrol_loc = base_loc +
            RandomHelper.RandomDirection() * RandomHelper.Random(75e3, 150e3);
            Point loc2 = patrol_loc +
            RandomHelper.RandomDirection() * RandomHelper.Random(50e3, 100e3);


            int ntargets = 2 + (RandomHelper.RandomChance() ? 1 : 0);
            int ntries = 8;

            while (ntargets > 0 && ntries > 0)
            {
                Point target_loc = RandomHelper.RandomChance() ? patrol_loc : loc2;
                int t = CreateRandomTarget(region, target_loc);
                ntargets -= t;
                if (t < 1) ntries--;
            }
        }
        protected virtual void CreateTargetsFreightEscort()
        {
            if (ward == null)
            {
                CreateTargetsPatrol();
                return;
            }

            CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.ATTACK_SQUADRON);
            CombatGroup s2 = FindSquadron(enemy, CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON);

            if (s == null || s2 == null) return;

            MissionElement elem = CreateFighterPackage(s, 2, Mission.TYPE.ASSAULT);
            if (elem != null)
            {
                elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);

                elem.SetLocation(ward.Location() + RandomHelper.RandomPoint() * 5);

                Instruction obj = new Instruction(Instruction.ACTION.ASSAULT, ward.Name());
                if (obj != null)
                    elem.AddObjective(obj);
                mission.AddElement(elem);

                MissionElement e2 = CreateFighterPackage(s2, 2, Mission.TYPE.ESCORT);
                if (e2 != null)
                {
                    e2.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                    e2.SetLocation(elem.Location() + RandomHelper.RandomPoint() * 0.25);

                    Instruction obj2 = new Instruction(Instruction.ACTION.ESCORT, elem.Name());
                    if (obj2 != null)
                        e2.AddObjective(obj2);
                    mission.AddElement(e2);
                }
            }

            Instruction obj3 = new Instruction(mission.GetRegion(), new Point(0, 0, 0), Instruction.ACTION.PATROL);
            if (player != null && obj3 != null)
            {
                obj3.SetTargetDesc("enemy patrols");
                player.AddObjective(obj3);
            }
        }
        protected virtual int CreateRandomTarget(string rgn, Point base_loc)
        {
            int ntargets = 0;
            int ttype = RandomHelper.RandomIndex();

            if (player_group != null && player_group.Type() == CombatGroup.GROUP_TYPE.CARRIER_GROUP)
            {
                switch (ttype)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3: ttype = 0; break;
                    case 4:
                    case 5: ttype = 1; break;
                    case 6:
                    case 7: ttype = 2; break;
                    case 8:
                    case 9: ttype = 3; break;
                    case 10:
                    case 11: ttype = 4; break;
                    case 12:
                    case 13:
                    case 14:
                    case 15: ttype = 5; break;
                }
            }
            else
            {
                switch (ttype)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5: ttype = 0; break;
                    case 6:
                    case 7:
                    case 8: ttype = 1; break;
                    case 9:
                    case 10: ttype = 4; break;
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15: ttype = 5; break;
                }
            }

            switch (ttype)
            {
                case 0:
                    {
                        CombatGroup s = null;

                        s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON);

                        if (s != null)
                        {
                            for (int i = 0; i < 2; i++)
                            {
                                CombatUnit u = s.GetRandomUnit();
                                MissionElement elem = CreateSingleElement(s, u);
                                if (elem != null)
                                {
                                    elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                                    elem.SetRegion(rgn);
                                    elem.SetLocation(base_loc + RandomHelper.RandomPoint() * 1.5);
                                    elem.SetMissionRole(Mission.TYPE.FLEET);
                                    mission.AddElement(elem);
                                    ntargets++;
                                }
                            }
                        }
                    }
                    break;

                case 1:
                    {
                        CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.LCA_SQUADRON);

                        if (s != null)
                        {
                            MissionElement elem = CreateFighterPackage(s, 2, Mission.TYPE.CARGO);
                            if (elem != null)
                            {
                                elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                                elem.SetRegion(rgn);
                                elem.SetLocation(base_loc + RandomHelper.RandomPoint() * 2);
                                mission.AddElement(elem);
                                ntargets++;

                                CombatGroup s2 = FindSquadron(enemy, CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON);

                                if (s2 != null)
                                {
                                    MissionElement e2 = CreateFighterPackage(s2, 2, Mission.TYPE.ESCORT);
                                    if (e2 != null)
                                    {
                                        e2.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                                        e2.SetRegion(rgn);
                                        e2.SetLocation(elem.Location() + RandomHelper.RandomPoint() * 0.5);

                                        Instruction obj = new Instruction(Instruction.ACTION.ESCORT, elem.Name());
                                        if (obj != null)
                                            e2.AddObjective(obj);

                                        mission.AddElement(e2);
                                    }
                                }
                            }
                        }
                    }
                    break;

                case 2:
                    {
                        CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON);

                        if (s != null)
                        {
                            MissionElement elem = CreateFighterPackage(s, 4, Mission.TYPE.PATROL);
                            if (elem != null)
                            {
                                elem.SetIntelLevel(Intel.INTEL_TYPE.SECRET);
                                elem.SetRegion(rgn);
                                elem.SetLocation(base_loc);
                                mission.AddElement(elem);
                                ntargets++;
                            }
                        }
                    }
                    break;

                case 3:
                    {
                        CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.FIGHTER_SQUADRON);

                        if (s != null)
                        {
                            MissionElement elem = CreateFighterPackage(s, 3, Mission.TYPE.ASSAULT);
                            if (elem != null)
                            {
                                elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                                elem.Loadouts().Destroy();
                                elem.Loadouts().Add(new MissionLoad(-1, "Ship Strike"));
                                elem.SetRegion(rgn);
                                elem.SetLocation(base_loc + RandomHelper.RandomPoint());
                                mission.AddElement(elem);

                                if (player != null)
                                {
                                    Instruction n = new Instruction(player.Region(),
                                     player.Location() + RandomHelper.RandomPoint(),
                                     Instruction.ACTION.ASSAULT);
                                    n.SetTarget(player.Name());
                                    elem.AddNavPoint(n);
                                }

                                ntargets++;
                            }
                        }
                    }
                    break;

                case 4:
                    {
                        CombatGroup s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.ATTACK_SQUADRON);

                        if (s != null)
                        {
                            MissionElement elem = CreateFighterPackage(s, 2, Mission.TYPE.ASSAULT);
                            if (elem != null)
                            {
                                elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                                elem.Loadouts().Destroy();
                                elem.Loadouts().Add(new MissionLoad(-1, "Hvy Ship Strike"));
                                elem.SetRegion(rgn);
                                elem.SetLocation(base_loc + RandomHelper.RandomPoint() * 1.3);
                                mission.AddElement(elem);

                                if (player != null)
                                {
                                    Instruction n = new Instruction(player.Region(),
                                     player.Location() + RandomHelper.RandomPoint(),
                                     Instruction.ACTION.ASSAULT);
                                    n.SetTarget(player.Name());
                                    elem.AddNavPoint(n);
                                }

                                ntargets++;
                            }
                        }
                    }
                    break;

                default:
                    {
                        CombatGroup s = null;

                        s = FindSquadron(enemy, CombatGroup.GROUP_TYPE.FREIGHT);

                        if (s != null)
                        {
                            CombatUnit u = s.GetRandomUnit();
                            MissionElement elem = CreateSingleElement(s, u);
                            if (elem != null)
                            {
                                elem.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                                elem.SetRegion(rgn);
                                elem.SetLocation(base_loc + RandomHelper.RandomPoint() * 2);
                                elem.SetMissionRole(Mission.TYPE.CARGO);
                                mission.AddElement(elem);
                                ntargets++;

                                CombatGroup s2 = FindSquadron(enemy, CombatGroup.GROUP_TYPE.INTERCEPT_SQUADRON);

                                if (s2 != null)
                                {
                                    MissionElement e2 = CreateFighterPackage(s2, 2, Mission.TYPE.ESCORT);
                                    if (e2 != null)
                                    {
                                        e2.SetIntelLevel(Intel.INTEL_TYPE.KNOWN);
                                        e2.SetRegion(rgn);
                                        e2.SetLocation(elem.Location() + RandomHelper.RandomPoint() * 0.5);

                                        Instruction obj = new Instruction(Instruction.ACTION.ESCORT, elem.Name());
                                        if (obj != null)
                                            e2.AddObjective(obj);
                                        mission.AddElement(e2);
                                        ntargets++;
                                    }
                                }
                            }
                        }
                    }
                    break;
            }

            return ntargets;
        }

        protected virtual MissionElement CreateSingleElement(CombatGroup g, CombatUnit u)
        {
            if (g == null || g.IsReserve()) return null;
            if (u == null || u.LiveCount() < 1) return null;
            if (mission.GetStarSystem() == null) return null;

            // no ground units in starship missions:
            StarSystem system = mission.GetStarSystem();
            OrbitalRegion rgn = system.FindRegion(u.GetRegion());

            if (rgn == null || rgn.Type() == Orbital.OrbitalType.TERRAIN)
                return null;

            // make sure this unit isn't already in the mission:
            foreach (MissionElement elem1 in mission.GetElements())
            {
                if (elem1.GetCombatUnit() == u)
                    return null;
            }

            MissionElement elem = new MissionElement();
            if (elem == null)
            {
                Exit();
                return null;
            }

            if (!string.IsNullOrEmpty(u.Name()))
                elem.SetName(u.Name());
            else
                elem.SetName(u.DesignName());

            elem.SetElementID(pkg_id++);

            elem.SetDesign(u.GetDesign());
            elem.SetCount(u.LiveCount());
            elem.SetIFF(u.GetIFF());
            elem.SetIntelLevel(g.IntelLevel());
            elem.SetRegion(u.GetRegion());
            elem.SetHeading(u.GetHeading());

            int unit_index = g.GetUnits().IndexOf(u);
            Point base_loc = u.Location();
            bool exact = u.IsStatic(); // exact unit-level placement

            if (base_loc.Length < 1)
            {
                base_loc = g.Location();
                exact = false;
            }

            if (unit_index < 0 || unit_index > 0 && !exact)
            {
                Point loc = RandomHelper.RandomDirection();

                if (!u.IsStatic())
                {
                    while (Math.Abs(loc.Y) > Math.Abs(loc.X))
                        loc = RandomHelper.RandomDirection();

                    loc *= 10e3 + 9e3 * unit_index;
                }
                else
                {
                    loc *= 2e3 + 2e3 * unit_index;
                }

                elem.SetLocation(base_loc + loc);
            }
            else
            {
                elem.SetLocation(base_loc);
            }

            if (g.Type() == CombatGroup.GROUP_TYPE.CARRIER_GROUP)
            {
                if (u.Type() == SimElements.CLASSIFICATION.CARRIER)
                {
                    elem.SetMissionRole(Mission.TYPE.FLIGHT_OPS);
                }
                else
                {
                    elem.SetMissionRole(Mission.TYPE.ESCORT);
                }
            }
            else if (u.Type() == SimElements.CLASSIFICATION.STATION || u.Type() == SimElements.CLASSIFICATION.FARCASTER)
            {
                elem.SetMissionRole(Mission.TYPE.OTHER);

                // link farcaster to other terminus:
                if (u.Type() == SimElements.CLASSIFICATION.FARCASTER)
                {
                    string name = u.Name();
                    int dash = name.IndexOf('-');

                    string src = name.Substring(0, dash);
                    string dst = name.Substring(dash + 1, name.Length - (dash + 1));

                    Instruction obj = new Instruction(Instruction.ACTION.VECTOR, dst + "-" + src);
                    if (obj != null)
                        elem.AddObjective(obj);
                }
            }
            else if ((u.Type() & SimElements.CLASSIFICATION.STARSHIPS) != 0)
            {
                elem.SetMissionRole(Mission.TYPE.FLEET);
            }

            elem.SetCombatGroup(g);
            elem.SetCombatUnit(u);

            return elem;
        }
        protected virtual MissionElement CreateFighterPackage(CombatGroup squadron, int count, Mission.TYPE role)
        {
            if (squadron == null || squadron.IsReserve())
                return null;

            CombatUnit fighter = squadron.GetUnits()[0];
            CombatUnit carrier = FindCarrier(squadron);

            if (fighter == null)
                return null;

            int avail = fighter.LiveCount();
            int actual = count;

            if (avail < actual)
                actual = avail;

            if (avail < 1)
            {
                ErrLogger.PrintLine("CMS - Insufficient fighters in squadron '{0}' - {1} required, {2} available",
                                        squadron.Name(), count, avail);
                return null;
            }

            MissionElement elem = new MissionElement();

            if (elem == null)
            {
                Exit();
                return null;
            }

            elem.SetName(Callsign.GetCallsign(fighter.GetIFF()));
            elem.SetElementID(pkg_id++);

            if (carrier != null)
            {
                elem.SetCommander(carrier.Name());
                elem.SetHeading(carrier.GetHeading());
            }
            else
            {
                elem.SetHeading(fighter.GetHeading());
            }

            elem.SetDesign(fighter.GetDesign());
            elem.SetCount(actual);
            elem.SetIFF(fighter.GetIFF());
            elem.SetIntelLevel(squadron.IntelLevel());
            elem.SetRegion(fighter.GetRegion());
            elem.SetSquadron(fighter.Name());
            elem.SetMissionRole(role);
            elem.Loadouts().Add(new MissionLoad(-1, "ACM Medium Range"));

            if (carrier != null)
                elem.SetLocation(carrier.Location() + RandomHelper.RandomPoint() * 0.3);
            else
                elem.SetLocation(fighter.Location() + RandomHelper.RandomPoint());

            elem.SetCombatGroup(squadron);
            elem.SetCombatUnit(fighter);

            return elem;
        }

        protected virtual CombatGroup FindSquadron(int iff, CombatGroup.GROUP_TYPE type)
        {
            if (player_group == null) return null;

            CombatGroup result = null;
            CombatZone zone = player_group.GetAssignedZone();
            if (zone == null) zone = player_group.GetCurrentZone();

            if (zone == null)
            {
                ErrLogger.PrintLine("CMS Warning: no zone for {0}", player_group.Name());
                return result;
            }

            ZoneForce force = zone.FindForce(iff);

            if (force != null)
            {
                List<CombatGroup> groups = new List<CombatGroup>();
                foreach (CombatGroup g in force.GetGroups())
                {
                    if (g.Type() == type && g.CountUnits() > 0)
                    {
                        result = g;
                        groups.Add(g);
                    }
                }

                if (groups.Count > 1)
                {
                    int index = (int)RandomHelper.Random(0, groups.Count);
                    if (index >= groups.Count) index = groups.Count - 1;
                    result = groups[index];
                }
            }

            return result;
        }
        protected virtual CombatUnit FindCarrier(CombatGroup g)
        {
            CombatGroup carrier = g.FindCarrier();

            if (carrier != null && carrier.GetUnits().Count != 0)
            {
                MissionElement carrier_elem = mission.FindElement(carrier.Name());

                if (carrier_elem != null)
                    return carrier.GetUnits()[0];
            }

            return null;
        }

        protected virtual void DefineMissionObjectives()
        {
            if (mission == null || player == null) return;

            if (prime_target != null) mission.SetTarget(prime_target);
            if (ward != null) mission.SetWard(ward);

            string objectives = "";

            if (player.Objectives().Count > 0)
            {
                for (int i = 0; i < player.Objectives().Count; i++)
                {
                    Instruction obj = player.Objectives()[i];
                    objectives += "* ";
                    objectives += obj.GetDescription();
                    objectives += ".\n";
                }
            }
            else
            {
                objectives += "* Perform standard fleet operations in the ";
                objectives += mission.GetRegion();
                objectives += " sector.\n";
            }

            mission.SetObjective(objectives);
        }
        protected virtual MissionInfo DescribeMission()
        {
            if (mission == null || player == null) return null;

            string name;
            string player_info = "";

            if (mission_info != null && !string.IsNullOrEmpty(mission_info.name))
                name = string.Format("MSN-{0:D3} {1}", mission.Identity(), mission_info.name);
            else if (ward != null)
                name = string.Format("MSN-{0:D3} {1} {2}", mission.Identity(), Game.GetText(mission.TypeName()), ward.Name());
            else if (prime_target != null)
                name = string.Format("MSN-{0:D3} {1} {2} {3}", mission.Identity(), Game.GetText(mission.TypeName()),
                    Ship.ClassName(prime_target.GetDesign().type),
                    prime_target.Name());
            else
                name = string.Format("MSN-{0:D3} {1}", mission.Identity(), Game.GetText(mission.TypeName()));

            if (player != null)
            {
                player_info = player.GetCombatGroup().GetDescription();
            }

            MissionInfo info = new MissionInfo();

            if (info != null)
            {
                info.id = mission.Identity();
                info.mission = mission;
                info.name = name;
                info.type = mission.Type();
                info.player_info = player_info;
                info.description = mission.Objective();
                info.start = mission.Start();

                if (mission.GetStarSystem() != null)
                    info.system = mission.GetStarSystem().Name();
                info.region = mission.GetRegion();
            }

            mission.SetName(name);

            return info;
        }
        protected virtual void Exit()
        {
            Starshatter stars = Starshatter.GetInstance();
            if (stars != null)
                stars.SetGameMode(Starshatter.MODE.MENU_MODE);
        }


        protected Campaign campaign;
        protected CampaignMissionRequest request;
        protected MissionInfo mission_info;

        protected CombatUnit player_unit;
        protected CombatGroup player_group;
        protected CombatGroup strike_group;
        protected CombatGroup strike_target;
        protected Mission mission;
        protected List<MissionElement> player_group_elements;
        protected MissionElement player;
        protected MissionElement ward;
        protected MissionElement prime_target;
        protected MissionElement escort;

        protected int ownside;
        protected int enemy;
        protected Mission.TYPE mission_type;

        static int pkg_id = 1000;
    }
}