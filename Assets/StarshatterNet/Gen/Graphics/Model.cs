﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Solid.h/Solid.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Classes for rendering solid meshes of polygons
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars;
using UnityEditor;
using UnityEngine;

namespace StarshatterNet.Gen.Graphics
{
    public class Model
    {
        public Model()
        {
            radius = 0; luminous = false; dynamic = false;
        }
        public Model(Model m)
        {
            radius = m.radius;
            luminous = m.luminous;
            dynamic = m.dynamic;
            mesh = m.mesh;
            name = m.name;
        }
        // ~Model();

        //Model& operator = (  Model& m);
        //int operator ==(  Model& that) const { return this == &that; }

        public bool Load(string file, double scale = 1.0)
        {
            modelObj = Resources.Load(file);
            mesh = Resources.Load<Mesh>(file);
            if (mesh != null)
            {
                Bounds bounds = mesh.bounds;
                Vector3 vec = bounds.size;
                radius = vec.sqrMagnitude;
                name = file;
                return true;
            }
            return false;
        }
        //public bool Load(ModelFile* loader, double scale = 1.0);

        public string Name() { return name; }
        //public int NumVerts() { return nverts; }
        //public int NumSurfaces() { return surfaces.size(); }
        public int NumMaterials() { return materials.Count; }
        //public int NumPolys() { return npolys; }
        //public int NumSegments();
        public double Radius() { return radius; }
        public bool IsDynamic() { return dynamic; }
        public void SetDynamic(bool d) { dynamic = d; }
        public bool IsLuminous() { return luminous; }
        public void SetLuminous(bool l) { luminous = l; }

        //public List<Surface> GetSurfaces() { return surfaces; }
        //public List<Material> GetMaterials() { return materials; }
        //public Material FindMaterial(string mtl_name);
        public Stars.Material ReplaceMaterial(Stars.Material mtl) { throw new NotImplementedException(); }
        //public void GetAllTextures(List<Bitmap> textures);

        //public Poly* AddPolys(int nsurf, int npolys, int nverts);
        public void ExplodeMesh() { throw new NotImplementedException(); }
        public void OptimizeMesh() { MeshUtility.Optimize(mesh); }
        public void OptimizeMaterials() { throw new NotImplementedException(); }
        public void ScaleBy(float factor)
        {
            //TODO Unity scale a game object not a mesh
            https://answers.unity.com/questions/523289/change-size-of-mesh-at-runtime.html
            scale *= factor;
            Vector3[] baseVertices = null;
            if (baseVertices == null)
                baseVertices = mesh.vertices;

            var vertices = new Vector3[baseVertices.Length];

            for (var i = 0; i < vertices.Length; i++)
            {
                var vertex = baseVertices[i];
                vertex.x = vertex.x * factor;
                vertex.y = vertex.y * factor;
                vertex.z = vertex.z * factor;

                vertices[i] = vertex;
            }

            mesh.vertices = vertices;

            if (recalculateNormals)
                mesh.RecalculateNormals();
            mesh.RecalculateBounds();
        }

        public void Normalize() { throw new NotImplementedException(); }

        //public void SelectPolys(List<Poly>&, Material* mtl);
        //public void SelectPolys(List<Poly>&, Vec3 loc);

        //public void AddSurface(Surface* s);
        //public void ComputeTangents();

        // buffer management
        //public void DeletePrivateData();


        //private bool LoadMag5(BYTE* block, int len, double scale);
        //private bool LoadMag6(BYTE* block, int len, double scale);

        public Mesh Mesh { get => mesh; }
        public UnityEngine.Object ModelObj { get => modelObj; }

        private string name;
        //private List<Surface> surfaces;
        private List<Stars.Material> materials = new List<Stars.Material>();
        //private int nverts;
        //private int npolys;

        private double scale = 1.0;
        private bool recalculateNormals = false;

        private UnityEngine.Object modelObj;
        private Mesh mesh;
        private float radius;
        private float[] extents = new float[6];
        private bool luminous;
        private bool dynamic;

        internal Stars.Material FindMaterial(string v)
        {
            throw new NotImplementedException();
        }
    }
}
