﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      BaseScreen.h/BaseScreen.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

 */
using System;

namespace StarshatterNet.Stars.Dialogs
{
    public class BaseScreen
    {

        public BaseScreen() { }
        //public virtual ~BaseScreen() { }

        public virtual void ShowNavDlg() { }
        public virtual void HideNavDlg() { }
        public virtual bool IsNavShown() { return false; }
        public virtual NavDlg GetNavDlg() { return null; }

        public virtual void ShowMsnElemDlg() { }
        public virtual void HideMsnElemDlg() { }
        public virtual MsnElemDlg GetMsnElemDlg() { return null; }

        public virtual AudDlg GetAudDlg() { return null; }
        public virtual VidDlg GetVidDlg() { return null; }
        public virtual ModDlg GetModDlg() { return null; }
        public virtual ModInfoDlg GetModInfoDlg() { return null; }
        public virtual OptDlg GetOptDlg() { return null; }
        public virtual CtlDlg GetCtlDlg() { return null; }
        public virtual JoyDlg GetJoyDlg() { return null; }
        public virtual KeyDlg GetKeyDlg() { return null; }

        public virtual void ShowAudDlg() { }
        public virtual void ShowVidDlg() { }
        public virtual void ShowModDlg() { }
        public virtual void ShowModInfoDlg() { }
        public virtual void ShowOptDlg() { }
        public virtual void ShowCtlDlg() { }
        public virtual void ShowJoyDlg() { }
        public virtual void ShowKeyDlg() { }

        public virtual void ShowMsgDlg() { }
        public virtual void HideMsgDlg() { }
        public virtual bool IsMsgShown() { return false; }
        //TODO public virtual MsgDlg GetMsgDlg() { return null; }

        public virtual void ApplyOptions() { }
        public virtual void CancelOptions() { }
    }
}
