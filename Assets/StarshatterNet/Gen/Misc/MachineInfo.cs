﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:             nGenEx.lib
    ORIGINAL FILE:         MachineInfo.h/MachineInfo.cpp
    ORIGINAL AUTHOR:       John DiCamillo
    .NET AUTHOR:           Agustin Santos


    OVERVIEW
    ========
    Collect and Display Machine, OS, and Driver Information
*/
using System;
using UnityEngine;

[assembly: System.Reflection.AssemblyVersion("0.0.1.*")]

namespace StarshatterNet.Gen.Misc
{
    public static class MachineInfo
    {
        public enum CPUTYPES { CPU_INVALID, CPU_P5 = 5, CPU_P6 = 6, CPU_P7 = 7, CPU_PLUS };
        public enum OSTYPES { OS_INVALID, OS_WIN95, OS_WIN98, OS_WINNT, OS_WIN2K, OS_WINXP, OS_WINXP64, OS_WINVISTA, OS_WINSEVEN, OS_WINFUTURE };
        public enum GRAPHICSTYPES { DX_NONE, DX_3 = 3, DX_5 = 5, DX_6 = 6, DX_7 = 7, DX_8 = 8, DX_9 = 9, DX_PLUS };

        public static CPUTYPES GetCpuClass()
        {
#if !DEBUG
            throw new NotImplementedException();
#else
            return CPUTYPES.CPU_INVALID;
#endif
        }

        public static Version GetRuntimeVersion()
        {
            return System.Environment.Version;
        }

        public static int GetCpuSpeed()
        {
#if DIGITALRUNE
            int cpuClockSpeed = 0;
            //create an instance of the Managemnet class with the
            //Win32_Processor class
            ManagementClass mgmt = new ManagementClass("Win32_Processor");
            //create a ManagementObjectCollection to loop through
            ManagementObjectCollection objCol = mgmt.GetInstances();
            //start our loop for all processors found
            foreach (ManagementObject obj in objCol)
            {
                if (cpuClockSpeed == 0)
                {
                    // only return cpuStatus from first CPU
                    cpuClockSpeed = Convert.ToInt32(obj.Properties["CurrentClockSpeed"].Value.ToString());
                }
            }
            //return the status
            return cpuClockSpeed;
#else
            throw new NotImplementedException();
#endif
        }

        public static int GetTotalRam()
        {
#if DIGITALRUNE
            int cpuClockSpeed = 0;
            //create an instance of the Managemnet class with the
            //Win32_Processor class
            ManagementClass mgmt = new ManagementClass("Win32_PhysicalMemory");
            //create a ManagementObjectCollection to loop through
            ManagementObjectCollection objCol = mgmt.GetInstances();
            //start our loop for all processors found
            foreach (ManagementObject obj in objCol)
            {
                if (cpuClockSpeed == 0)
                {
                    // only return cpuStatus from first CPU
                    cpuClockSpeed = Convert.ToInt32(obj.Properties["Capacity"].Value.ToString());
                }
            }
            //return the status
            return cpuClockSpeed;
#else
            throw new NotImplementedException();
#endif
        }
        public static OperatingSystemFamily GetPlatform()
        {
            return SystemInfo.operatingSystemFamily;
        }

        public static GRAPHICSTYPES GetDirectXVersion()
        {
            throw new NotImplementedException();
        }

        public static void DescribeMachine()
        {
            OperatingSystemFamily os = GetPlatform();
            GetCpuClass();

            ErrLogger.PrintLine("+====================================================================+");
            ErrLogger.PrintLine("| STARSHATTER       {0,-50:s}{1,-50:s} |", GetVersion().ToString(), DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffK"));
            ErrLogger.PrintLine("| OS                {0,-100:s} |", SystemInfo.operatingSystem);
            ErrLogger.PrintLine("| CPU               {0,-100:s} |", SystemInfo.processorType);
            ErrLogger.PrintLine("| RAM               {0,-100:d} |", SystemInfo.systemMemorySize);
            ErrLogger.PrintLine("| GPU               {0,-100:s} |", SystemInfo.graphicsDeviceName + " (version " + SystemInfo.graphicsDeviceVersion + ")");


            ErrLogger.PrintLine("+====================================================================+");
            ErrLogger.PrintLine();
        }

        public static string GetShortDescription()
        {
            string desc;
            // processor info using the following format - "Intel(R) Core(TM)2 Quad CPU Q6600 @ 2.40GHz"
            string cpu = SystemInfo.processorType;
            int ram = SystemInfo.systemMemorySize;
            string os = SystemInfo.operatingSystem;
            desc = cpu + ", " + ram + " RAM, " + os;
            return desc;
        }
        public static string GetSystemInfo()
        {
            string str = "<color=red>SYSTEM INFO</color>";

#if UNITY_IOS
     str += "\n[iphone generation]iPhone.generation.ToString()";
#endif

#if UNITY_ANDROID
     str += "\n[system info]" + SystemInfo.deviceModel;
#endif

            str += "\n[type]" + SystemInfo.deviceType;
            str += "\n[os version]" + SystemInfo.operatingSystem;
            str += "\n[system memory size]" + SystemInfo.systemMemorySize;
            str += "\n[graphic device name]" + SystemInfo.graphicsDeviceName + " (version " + SystemInfo.graphicsDeviceVersion + ")";
            str += "\n[graphic memory size]" + SystemInfo.graphicsMemorySize;
            //str += "\n[graphic pixel fill rate]" + SystemInfo.graphicsPixelFillrate;
            str += "\n[graphic max texSize]" + SystemInfo.maxTextureSize;
            str += "\n[graphic shader level]" + SystemInfo.graphicsShaderLevel;
            str += "\n[support compute shader]" + SystemInfo.supportsComputeShaders;

            str += "\n[processor count]" + SystemInfo.processorCount;
            str += "\n[processor type]" + SystemInfo.processorType;
            str += "\n[support 3d texture]" + SystemInfo.supports3DTextures;
            str += "\n[support shadow]" + SystemInfo.supportsShadows;

            str += "\n[platform] " + Application.platform;
            str += "\n[screen size] " + Screen.width + " x " + Screen.height;
            str += "\n[screen pixel density dpi] " + Screen.dpi;

            return str;
        }
        public static Version GetVersion()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
        }
    }
}
