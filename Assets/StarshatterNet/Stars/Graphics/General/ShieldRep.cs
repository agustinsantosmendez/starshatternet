﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars.exe
    ORIGINAL FILE:      ShieldRep.h/ShieldRep.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    ShieldRep Solid class
*/
using System;
using StarshatterNet.Stars.SimElements;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using DWORD = System.UInt32;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars.Graphics.General
{
    public class ShieldRep : Solid
    {

        public ShieldRep() { throw new NotImplementedException(); }
        //public virtual ~ShieldRep();

        // operations
        //public virtual void Render(Video video, DWORD flags) { throw new NotImplementedException(); }
        public virtual void Energize(double seconds, bool bubble = false) { throw new NotImplementedException(); }
        public int ActiveHits() { return nhits; }
        public virtual void Hit(Point impact, Shot shot, double damage = 0) { throw new NotImplementedException(); }
        public override void TranslateBy(Point r) { throw new NotImplementedException(); }
        public virtual void Illuminate() { throw new NotImplementedException(); }


        protected int nhits;
        protected ShieldHit hits;
        protected Point true_eye_point;
        protected bool bubble;
    }
    public class ShieldHit
    {
        public Point hitloc;
        public double damage;
        public double age;
        public Shot shot;

        public ShieldHit() { damage = 0; age = 0; shot = null; }
    }
}