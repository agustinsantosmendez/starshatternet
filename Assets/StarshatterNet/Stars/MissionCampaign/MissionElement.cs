﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Mission.h/Mission.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Universe and Region classes
*/
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.SimElements;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning MissionElement class is still in development and is not recommended for production.
    public class MissionElement
    {
        public MissionElement()
        {
            id = elem_idkey++;
            elem_id = 0; design = null; skin = null; count = 1; maint_count = 0; dead_count = 0;
            IFF_code = 0; player = 0; alert = false; playable = false; rogue = false; invulnerable = false;
            respawns = 0; hold_time = 0; zone_lock = 0; heading = 0; mission_role = Mission.TYPE.OTHER;
            intel = Intel.INTEL_TYPE.SECRET; command_ai = 1; combat_group = null; combat_unit = null;

        }
        // ~MissionElement();

        // int operator ==(  MissionElement& r)   { return id == r.id; }

        public int Identity() { return id; }
        public string Name() { return name; }
        public string Abbreviation()
        {
            if (design != null)
                return design.abrv;

            return "UNK";
        }

        public string Carrier() { return carrier; }
        public string Commander() { return commander; }
        public string Squadron() { return squadron; }
        public string Path() { return path; }
        public int ElementID() { return elem_id; }
        public ShipDesign GetDesign() { return design; }
        public Skin GetSkin() { return skin; }
        public int Count() { return count; }
        public int MaintCount() { return maint_count; }
        public int DeadCount() { return dead_count; }
        public int GetIFF() { return IFF_code; }
        public Intel.INTEL_TYPE IntelLevel() { return intel; }
        public Mission.TYPE MissionRole() { return mission_role; }
        public int Player() { return player; }
        public string RoleName()
        {
            return Mission.RoleName(mission_role);
        }
        public Color MarkerColor()
        {
            return Ship.IFFColor(IFF_code);
        }
        public bool IsStarship()
        {
            CLASSIFICATION design_type = 0;
            if (GetDesign() != null)
                design_type = GetDesign().type;

            return design_type.HasFlag(CLASSIFICATION.STARSHIPS);
        }
        public bool IsDropship()
        {
            CLASSIFICATION design_type = 0;
            if (GetDesign() != null)
                design_type = GetDesign().type;

            return design_type.HasFlag(CLASSIFICATION.DROPSHIPS);
        }
        public bool IsStatic()
        {
            CLASSIFICATION design_type = 0;
            if (GetDesign() != null)
                design_type = GetDesign().type;

            return design_type >= CLASSIFICATION.STATION;
        }
        public bool IsGroundUnit()
        {
            CLASSIFICATION design_type = 0;
            if (GetDesign() != null)
                design_type = GetDesign().type;

            return design_type.HasFlag(CLASSIFICATION.GROUND_UNITS);
        }
        public bool IsSquadron()
        {
            if (!string.IsNullOrEmpty(carrier))
                return true;

            return false;
        }
        public bool IsCarrier()
        {
            ShipDesign design = GetDesign();
            if (design != null && design.flight_decks.Count > 0)
                return true;

            return false;
        }
        public bool IsAlert() { return alert; }
        public bool IsPlayable() { return playable; }
        public bool IsRogue() { return rogue; }
        public bool IsInvulnerable() { return invulnerable; }
        public int RespawnCount() { return respawns; }
        public int HoldTime() { return hold_time; }
        public int CommandAI() { return command_ai; }
        public int ZoneLock() { return zone_lock; }

        public string Region() { return rgn_name; }
        public Point Location()
        {
            return this.rloc.Location();
        }
        public RLoc GetRLoc() { return rloc; }
        public double Heading() { return heading; }

        public string GetShipName(int index)
        {
            if (index < 0 || index >= ships.Count)
            {
                if (count > 1)
                {
                    string sname = string.Format("{0} {1}", name, index + 1);
                    return sname;
                }
                else
                {
                    return name;
                }
            }

            return ships[index].Name();
        }
        public string GetRegistry(int index)
        {
            if (index < 0 || index >= ships.Count)
            {
                return "";
            }

            return ships[index].RegNum();
        }

        public List<Instruction> Objectives() { return objectives; }
        public List<string> Instructions() { return instructions; }
        public List<Instruction> NavList() { return navlist; }
        public List<MissionLoad> Loadouts() { return loadouts; }
        public List<MissionShip> Ships() { return ships; }

        public void SetName(string n) { name = n; }
        public void SetCarrier(string c) { carrier = c; }
        public void SetCommander(string c) { commander = c; }
        public void SetSquadron(string s) { squadron = s; }
        public void SetPath(string p) { path = p; }
        public void SetElementID(int id) { elem_id = id; }
        public void SetDesign(ShipDesign d) { design = d; }
        public void SetSkin(Skin s) { skin = s; }
        public void SetCount(int n) { count = n; }
        public void SetMaintCount(int n) { maint_count = n; }
        public void SetDeadCount(int n) { dead_count = n; }
        public void SetIFF(int iff) { IFF_code = iff; }
        public void SetIntelLevel(Intel.INTEL_TYPE i) { intel = i; }
        public void SetMissionRole(Mission.TYPE r) { mission_role = r; }
        public void SetPlayer(int p) { player = p; }
        public void SetPlayable(bool p) { playable = p; }
        public void SetRogue(bool r) { rogue = r; }
        public void SetInvulnerable(bool n) { invulnerable = n; }
        public void SetAlert(bool a) { alert = a; }
        public void SetCommandAI(int a) { command_ai = a; }
        public void SetRegion(string rgn) { rgn_name = rgn; }
        public void SetLocation(Point p)
        {
            rloc.SetBaseLocation(p);
            rloc.SetReferenceLoc(null);
            rloc.SetDistance(0);
        }
        public void SetRLoc(RLoc r)
        {
            rloc = r;
        }
        public void SetHeading(double h) { heading = h; }
        public void SetRespawnCount(int r) { respawns = r; }
        public void SetHoldTime(int t) { hold_time = t; }
        public void SetZoneLock(int z) { zone_lock = z; }

        public void AddNavPoint(Instruction pt, Instruction afterPoint = null)
        {
            if (pt != null && !navlist.Contains(pt))
            {
                if (afterPoint != null)
                {
                    int index = navlist.IndexOf(afterPoint);

                    if (index > -1)
                        navlist.Insert(index + 1, pt);
                    else
                        navlist.Add(pt);
                }

                else
                {
                    navlist.Add(pt);
                }
            }
        }
        public void DelNavPoint(Instruction pt)
        {
            if (pt != null)
                navlist.Remove(pt);
        }
        public void ClearFlightPlan()
        {
            navlist.Clear();
        }
        public int GetNavIndex(Instruction n)
        {
            int index = 0;

            if (navlist.Count > 0)
            {
                foreach (Instruction navpt in navlist)
                {
                    index++;
                    if (navpt == n)
                        return index;
                }
            }

            return 0;
        }

        public void AddObjective(Instruction obj) { objectives.Add(obj); }
        public void AddInstruction(string i) { instructions.Add(i); }

        public CombatGroup GetCombatGroup() { return combat_group; }
        public void SetCombatGroup(CombatGroup g) { combat_group = g; }
        public CombatUnit GetCombatUnit() { return combat_unit; }
        public void SetCombatUnit(CombatUnit u) { combat_unit = u; }


        protected internal int id;
        protected internal string name;
        protected internal string carrier;
        protected internal string commander;
        protected internal string squadron;
        protected internal string path;
        protected internal int elem_id;
        protected internal ShipDesign design;
        protected internal Skin skin;
        protected internal int count;
        protected internal int maint_count;
        protected internal int dead_count;
        protected internal int IFF_code;
        protected internal Mission.TYPE mission_role;
        protected internal Intel.INTEL_TYPE intel;
        protected internal int respawns;
        protected internal int hold_time;
        protected internal int zone_lock;
        protected internal int player;
        protected internal int command_ai;
        protected internal bool alert;
        protected internal bool playable;
        protected internal bool rogue;
        protected internal bool invulnerable;

        protected internal string rgn_name;
        protected internal RLoc rloc = new RLoc();
        protected internal double heading;

        protected internal CombatGroup combat_group;
        protected internal CombatUnit combat_unit;

        protected internal List<Instruction> objectives = new List<Instruction>();
        protected internal List<string> instructions = new List<string>();
        protected internal List<Instruction> navlist = new List<Instruction>();
        protected internal List<MissionLoad> loadouts = new List<MissionLoad>();
        protected internal List<MissionShip> ships = new List<MissionShip>();

        private static int elem_idkey = 1;

    }
}