﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CmdTheaterDlg.h/CmdTheaterDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Operational Command Dialog (Order of Battle Tab)
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class CmdTheaterDlg : CmdDlg
    {
        public CmdTheaterDlg(Screen s, Camera c, FormDef def, CmpnScreen mgr, GameObject p) :
             base(s, mgr, 0, 0, s.Width(), s.Height(), c, p, "CmdTheaterDlg")
        {
            map_theater = null; map_view = null;

            Init(def);
        }
        //public virtual ~CmdTheaterDlg();
        public override void RegisterControls()
        {
            map_theater = FindControl(400);

            RegisterCmdControls(this);

            if (btn_save != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_save, OnSave);

            if (btn_exit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_exit, OnExit);

            for (int i = 0; i < 5; i++)
            {
                if (btn_mode[i] != null)
                    REGISTER_CLIENT(ETD.EID_CLICK, btn_mode[i], OnMode);
            }

            if (map_theater != null)
                map_view = new MapView(map_theater);

            for (int i = 0; i < 3; i++)
            {
                view_btn[i] = (Button)FindControl(401 + i);
                REGISTER_CLIENT(ETD.EID_CLICK, view_btn[i], OnView);
            }

            zoom_in_btn = (Button)FindControl(410);
            zoom_out_btn = (Button)FindControl(411);
        }
        public override void Show()
        {
            mode = MODE.MODE_THEATER;

            base.Show();
            ShowCmdDlg();

            campaign = Campaign.GetCampaign();

            if (campaign != null && map_theater != null)
            {
                map_view.SetCampaign(campaign);
            }
        }
        public override void ExecFrame()
        {
            base.ExecFrame();

            if (map_view == null)
                return;

            if (Keyboard.KeyDown(KeyMap.VK_ADD) ||
                    (zoom_in_btn != null && zoom_in_btn.GetButtonState() > 0))
            {
                map_view.ZoomIn();
            }
            else if (Keyboard.KeyDown(KeyMap.VK_SUBTRACT) ||
                    (zoom_out_btn != null && zoom_out_btn.GetButtonState() > 0))
            {
                map_view.ZoomOut();
            }

            else if (Mouse.Wheel() > 0)
            {
                map_view.ZoomIn();
                map_view.ZoomIn();
                map_view.ZoomIn();
            }

            else if (Mouse.Wheel() < 0)
            {
                map_view.ZoomOut();
                map_view.ZoomOut();
                map_view.ZoomOut();
            }
        }

        // Operations:
        public override void OnMode(ActiveWindow obj, AWEvent evnt)
        {
            base.OnMode(obj, evnt);
        }
        public override void OnSave(ActiveWindow obj, AWEvent evnt)
        {
            base.OnSave(obj, evnt);
        }


        public override void OnExit(ActiveWindow obj, AWEvent evnt)
        {
            base.OnExit(obj, evnt);
        }
        public virtual void OnView(ActiveWindow obj, AWEvent evnt)
        {
            int use_filter_mode = -1;

            view_btn[VIEW_GALAXY].SetButtonState(0);
            view_btn[VIEW_SYSTEM].SetButtonState(0);
            view_btn[VIEW_REGION].SetButtonState(0);

            if (view_btn[0] == evnt.window)
            {
                if (map_view != null) map_view.SetViewMode(VIEW_GALAXY);
                view_btn[VIEW_GALAXY].SetButtonState(1);
                use_filter_mode = SELECT_SYSTEM;
            }

            else if (view_btn[VIEW_SYSTEM] == evnt.window)
            {
                if (map_view != null) map_view.SetViewMode(VIEW_SYSTEM);
                view_btn[VIEW_SYSTEM].SetButtonState(1);
                use_filter_mode = SELECT_REGION;
            }

            else if (view_btn[VIEW_REGION] == evnt.window)
            {
                if (map_view != null) map_view.SetViewMode(VIEW_REGION);
                view_btn[VIEW_REGION].SetButtonState(1);
                use_filter_mode = SELECT_STARSHIP;
            }

            if (use_filter_mode >= 0)
            {
                if (map_view != null) map_view.SetSelectionMode(use_filter_mode);
            }
        }


        protected CmpnScreen manager;

        protected ActiveWindow map_theater;
        protected MapView map_view;
        protected Button[] view_btn = new Button[3];
        protected Button zoom_in_btn;
        protected Button zoom_out_btn;


        private const int SELECT_NONE = -1;
        private const int SELECT_SYSTEM = 0;
        private const int SELECT_PLANET = 1;
        private const int SELECT_REGION = 2;
        private const int SELECT_STATION = 3;
        private const int SELECT_STARSHIP = 4;
        private const int SELECT_FIGHTER = 5;

        private const int VIEW_GALAXY = 0;
        private const int VIEW_SYSTEM = 1;
        private const int VIEW_REGION = 2;

    }
}