﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Starshatter.h/Starshatter.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using StarshatterNet.Audio;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Dialogs;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.StarSystems;
using StarshatterNet.Stars.Views;
using UnityEngine;
using KeyMap = StarshatterNet.Config.KeyMap;
using Screen = StarshatterNet.Gen.Graphics.Screen;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using DigitalRune.Mathematics;

namespace StarshatterNet.Stars
{
    /// <summary>
    /// This is the main type for Starshatter Game.
    /// </summary>
#warning Starshatter class is still in development and is not recommended for production.
    public class Starshatter : Game
    {
        public static readonly string versionInfo = MachineInfo.GetVersion().ToString();

        public enum MODE
        {
            MENU_MODE,  // main menu
            CLOD_MODE,  // loading campaign
            CMPN_MODE,  // operational command for dynamic campaign
            PREP_MODE,  // loading mission info for planning
            PLAN_MODE,  // mission briefing
            LOAD_MODE,  // loading mission into simulator
            PLAY_MODE,  // active simulation
            EXIT_MODE   // shutting down
        };

        public enum LOBBY
        {
            NET_LOBBY_CLIENT,
            NET_LOBBY_SERVER
        };

        public Starshatter()
        {
            if (instance == null)
                instance = this;

            app_name = "Starshatter: The Gathering Storm";
            title_text = "STARSHATTER";

            MachineInfo.DescribeMachine();
            ErrLogger.PrintLine("  Initializing Game");
            LoadVideoConfig(VideoConfigStr);

            HUDfont = (Font)Resources.Load("Fonts/HUDfont"); //new Font("HUDfont");
            FontMgr.Register("HUD", HUDfont);

            GUIfont = (Font)Resources.Load("Fonts/Verdana"); //new Font("GUIfont");
            FontMgr.Register("GUI", GUIfont);

            GUI_small_font = (Font)Resources.Load("Fonts/Verdana"); //new Font("GUIsmall");
            FontMgr.Register("GUIsmall", GUI_small_font);

            limerick12 = (Font)Resources.Load("Fonts/Limerick12r"); //new Font("Limerick12");
            limerick18 = (Font)Resources.Load("Fonts/Limerick12r"); //new Font("Limerick18");
            terminal = (Font)Resources.Load("Fonts/Terminal"); //new Font("Terminal");
            verdana = (Font)Resources.Load("Fonts/Verdana"); //new Font("Verdana");x`
            ocrb = (Font)Resources.Load("Fonts/OCRB"); //new Font("OCRB");

            FontMgr.Register("Limerick12", limerick12, 12);
            FontMgr.Register("Limerick18", limerick18, 18);
            FontMgr.Register("Terminal", terminal);
            FontMgr.Register("Verdana", verdana);
            FontMgr.Register("OCRB", ocrb);
        }

        // public virtual bool Init(HINSTANCE hi, HINSTANCE hpi, LPSTR cmdline, int nCmdShow) { throw new NotImplementedException(); }

        public override bool InitGame()
        {
            if (!base.InitGame())
                return false;

            RandomHelper.RandomInit();

            AudioConfig.Initialize();
            ModConfig.Initialize();

            InitMouse();

            Button.Initialize();
            // TODO EventDispatch.Create();
            // TODO NetClientConfig.Initialize();
            Player.Initialize();
            HUDSounds.Initialize();

            int nkeys = keycfg.LoadKeyMap("key.cfg", 256);

            if (nkeys != 0)
            {
                ErrLogger.PrintLine();
                ErrLogger.PrintLine("  Loaded key.cfg");
            }
            // create the appropriate motion controller and player_ship
            input = new MultiController();
            Keyboard k = new Keyboard();
            input.AddController(k);
            // TODO ActivateKeyboardLayout(GetKeyboardLayout(0), 0);

            mouse_input = new MouseController();
            input.AddController(mouse_input);

            ErrLogger.PrintLine("\nStarshatter.InitGame() create joystick");
            Joystick j = new Joystick();
            j.SetSensitivity(15, 5000);
            input.AddController(j);

            Joystick.EnumerateDevices();
            ErrLogger.PrintLine("");

            head_tracker = new TrackIR();
            MapKeys();
            SystemDesign.Initialize();
            WeaponDesign.Initialize();
            MusicDirector.Initialize();

            // if no splashes, we need to initialize the campaign engine now
            if (no_splash)
            {
                Ship.Initialize();
                Galaxy.Initialize();
                CombatRoster.Initialize();
                Campaign.Initialize();
            }
            // otherwise, the campaign engine will get initialized during the splashes
            else
            {
                SetupSplash();
            }

            time_mark = (ulong)Game.GameTime();
            minutes = 0;

            return true;
        }
        public virtual bool ChangeVideo() { throw new NotImplementedException(); }
        static bool quick_splash = false;
        public override void GameState()
        {
            if (splash != null)
            {

                if (GetKey() != 0)
                    quick_splash = true;

                if (quick_splash)
                {
                    splash.FadeIn(0);
                    splash.StopHold();
                    splash.FadeOut(0);
                }

                if (splash.Done())
                {
                    splash = null; // this will get deleted along with gamewin
                    splash_index++;

                    if (gamewin != null)
                    {
                        screen.DelWindow(gamewin);
                        //delete gamewin;
                        gamewin = null;
                    }

                    if (splash_index < 2)
                    {
                        Ship.Initialize();
                        Galaxy.Initialize();
                        SetupSplash();
                    }
                    else
                    {
                        CombatRoster.Initialize();
                        Campaign.Initialize();
                        SetupMenuScreen();
                    }

                    FlushKeys();
                }
            }

            else if (game_mode == MODE.MENU_MODE)
            {
                bool campaign_select = false;

                if (cmpnscreen != null)
                {
                    campaign_select = cmpnscreen.IsShown();
                    cmpnscreen.Hide();
                }

                if (gamescreen != null)
                    gamescreen.Hide();

                if (planscreen != null)
                    planscreen.Hide();

                if (loadscreen != null)
                    loadscreen.Hide();

                if (menuscreen == null)
                {
                    SetupMenuScreen();
                }
                else
                {
                    menuscreen.Show();

                    if (campaign_select)
                        menuscreen.ShowCmpSelectDlg();
                }

                if (MusicDirector.GetInstance() != null &&
                        MusicDirector.GetInstance().GetMode() != MusicDirector.MODES.CREDITS)
                    MusicDirector.SetMode(MusicDirector.MODES.MENU);

                DoMenuScreenFrame();
            }

            else if (game_mode == MODE.CLOD_MODE ||
                    game_mode == MODE.PREP_MODE ||
                    game_mode == MODE.LOAD_MODE)
            {
                if (menuscreen != null)
                    menuscreen.Hide();

                if (planscreen != null)
                    planscreen.Hide();

                if (cmpnscreen != null)
                    cmpnscreen.Hide();

                if (loadscreen == null)
                    SetupLoadScreen();
                else
                    loadscreen.Show();

                if (game_mode == MODE.CLOD_MODE)
                    MusicDirector.SetMode(MusicDirector.MODES.MENU);
                else
                    MusicDirector.SetMode(MusicDirector.MODES.BRIEFING);

                DoLoadScreenFrame();
            }

            else if (game_mode == MODE.PLAN_MODE)
            {
                if (menuscreen != null)
                    menuscreen.Hide();

                if (cmpnscreen != null)
                    menuscreen.Hide();

                if (loadscreen != null)
                    loadscreen.Hide();

                if (gamescreen != null)
                    gamescreen.Hide();

                if (planscreen == null)
                    SetupPlanScreen();
                else
                    planscreen.Show();

                Player p = Player.GetCurrentPlayer();
                if (p != null && p.ShowAward())
                {
                    if (!planscreen.IsAwardShown())
                        planscreen.ShowAwardDlg();
                }

                else if (ShipStats.NumStats() != 0)
                {
                    if (!planscreen.IsDebriefShown())
                    {
                        planscreen.ShowDebriefDlg();
                        show_missions = true;
                    }
                }

                else
                {
                    if (!planscreen.IsMsnShown() && !planscreen.IsNavShown())
                        planscreen.ShowMsnDlg();
                }

                MusicDirector.SetMode(MusicDirector.MODES.BRIEFING);

                DoPlanScreenFrame();
            }


            else if (game_mode == MODE.CMPN_MODE)
            {
                if (menuscreen != null)
                    menuscreen.Hide();

                if (planscreen != null)
                    planscreen.Hide();

                if (loadscreen != null)
                    loadscreen.Hide();

                if (gamescreen != null)
                    gamescreen.Hide();

                if (cmpnscreen == null)
                    SetupCmpnScreen();
                else
                    cmpnscreen.Show();

                DoCmpnScreenFrame();
            }

            else if (game_mode == MODE.PLAY_MODE)
            {
                if (menuscreen != null)
                    menuscreen.Hide();

                if (cmpnscreen != null)
                    cmpnscreen.Hide();

                if (planscreen != null)
                    planscreen.Hide();

                if (loadscreen != null)
                    loadscreen.Hide();

                if (gamescreen == null)
                    SetupGameScreen();
                else
                    gamescreen.Show();

                DoGameScreenFrame();
            }

            if (game_mode == MODE.EXIT_MODE)
            {
                exit_time -= Game.GUITime();

                if (exit_time <= 0)
                    this.Exit();
            }

            if (net_lobby != null)
                net_lobby.ExecFrame();

            if (music_dir != null)
                music_dir.ExecFrame();

            Memory.Check();
        }
        public override void Exit() { }
        public virtual bool OnHelp() { throw new NotImplementedException(); }

        public MODE GetGameMode()
        {
            return MODE.MENU_MODE;
        }

        public void SetGameMode(MODE m)
        {
            if (game_mode == m)
                return;

            string[] mode_name = {
                            "MENU_MODE",  // main menu
                            "CLOD_MODE",  // loading campaign
                            "CMPN_MODE",  // operational command for dynamic campaign
                            "PREP_MODE",  // loading mission info for planning
                            "PLAN_MODE",  // mission briefing
                            "LOAD_MODE",  // loading mission into simulator
                            "PLAY_MODE",  // active simulation
                            "EXIT_MODE"   // shutting down
                        };

            if (m >= MODE.MENU_MODE && m <= MODE.EXIT_MODE)
                ErrLogger.PrintLine(">>> Starshatter.SetGameMode({0}) ({1})", m, mode_name[(int)m]);
            else
                ErrLogger.PrintLine(">>> Starshatter.SetGameMode({0}) (UNKNOWN MODE)", m);

            MouseController mouse_con = MouseController.GetInstance();
            if (mouse_con != null)
                mouse_con.SetActive(false);

            if (m == MODE.CLOD_MODE || m == MODE.PREP_MODE || m == MODE.LOAD_MODE)
            {
                load_step = 0;
                load_progress = 0;
                load_activity = Game.GetText("Starshatter.load.general");
                paused = true;
            }

            else if (m == MODE.CMPN_MODE)
            {
                load_step = 0;
                load_progress = 100;
                load_activity = Game.GetText("Starshatter.load.complete");
                paused = false;
            }

            else if (m == MODE.PLAY_MODE)
            {
                ErrLogger.PrintLine("  Starting Game...");

                player_ship = null;
                load_progress = 100;
                load_activity = Game.GetText("Starshatter.load.complete");

                if (world == null)
                {
                    CreateWorld();
                    InstantiateMission();
                }
                if (gamescreen != null)
                    gamescreen.SetFieldOfView(field_of_view);

                HUDView.ClearMessages();
                RadioView.ClearMessages();

                SetTimeCompression(1);
                Pause(false);

                ErrLogger.PrintLine("  Stardate: {0}", StarSystem.GetBaseTime());
            }

            else if (m == MODE.PLAN_MODE)
            {
#if TODO
                if (game_mode == MODE.PLAY_MODE)
                {
                    ErrLogger.PrintLine("  Returning to Plan Mode...");
                    if (soundcard != null)
                        soundcard.StopSoundEffects();

                    StopNetGame();
                    Pause(true);
                    ErrLogger.PrintLine("  Stardate: {0}", StarSystem.GetBaseTime());
                }
#endif
                ErrLogger.PrintLine("WARNING: Starshatter.SetGameMode is not fully implemented.");
            }

            else if (m == MODE.MENU_MODE)
            {
                ErrLogger.PrintLine("  Returning to Main Menu...");
#if TODO
                if (game_mode == MODE.PLAN_MODE || game_mode == MODE.PLAY_MODE)
                {
                    if (soundcard != null)
                        soundcard.StopSoundEffects();

                    StopNetGame();
                }
#endif
                ErrLogger.PrintLine("WARNING: Starshatter.SetGameMode is not fully implemented.");

                paused = true;
            }

            if (m == MODE.EXIT_MODE)
            {
                ErrLogger.PrintLine("  Shutting Down (Returning to Windows)...");

                if (game_mode == MODE.PLAN_MODE || game_mode == MODE.PLAY_MODE)
                {
#if TODO
                    if (soundcard != null)
                        soundcard.StopSoundEffects();

                    StopNetGame();
#endif
                    ErrLogger.PrintLine("WARNING: Starshatter.SetGameMode is not fully implemented.");

                }

                ErrLogger.PrintLine("  Stardate: {0}", StarSystem.GetBaseTime());
                //ErrLogger.PrintLine("  Bitmap Cache Footprint: {0} KB\n", Bitmap.CacheMemoryFootprint() / 1024);

                paused = true;
            }

            FlushKeys();
            game_mode = m;
        }
        public void LoadVideoConfig(string filename)
        {
            if (!File.Exists(filename))
            {
                ErrLogger.PrintLine("WARNING: invalid {0} file. Using defaults", filename);
                XmlWriterSettings writerSettings = new XmlWriterSettings()
                {
#if DEBUG
                    Indent = true,
                    NewLineOnAttributes = true,
#endif
                };

                videoSettings = new VideoSettings();
                using (XmlWriter writer = XmlWriter.Create(filename, writerSettings))
                    videoSettings.Save(writer);

            }
            else
            {
                using (XmlReader reader = XmlReader.Create(filename))
                    videoSettings = VideoSettings.Load(reader);
            }
        }
        public void SaveVideoConfig(string filename) { ErrLogger.PrintLine("WARNING: StarshatterGame SaveVideoConfig() is not yet Implemented"); }
        public void SetupSplash() { ErrLogger.PrintLine("WARNING: StarshatterGame SetupSplash() is not yet Implemented"); }
        public void SetupMenuScreen()
        {
            if (menuscreen != null)
            {
                //delete menuscreen;
                menuscreen = null;
            }

            menuscreen = new MenuScreen();
            menuscreen.Setup(screen, camera, gameObj);
        }

        public void SetupCmpnScreen()
        {
            if (cmpnscreen != null)
            {
                //delete cmpnscreen;
                cmpnscreen = null;
            }

            cmpnscreen = new CmpnScreen();
            cmpnscreen.Setup(screen, camera, gameObj);
        }

        public void SetupPlanScreen()
        {
            if (planscreen != null)
            {
                //delete planscreen;
                planscreen = null;
            }

            planscreen = new PlanScreen();
            planscreen.Setup(screen, camera, gameObj);
        }
        public void SetupLoadScreen()
        {
            if (loadscreen != null)
            {
                //delete loadscreen;
                loadscreen = null;
            }

            loadscreen = new LoadScreen();
            loadscreen.Setup(screen, camera, gameObj);
        }
        public void SetupGameScreen()
        {
            if (gamescreen != null)
            {
                //delete gamescreen;
                gamescreen = null;
            }

            gamescreen = new GameScreen();
            gamescreen.Setup(screen, camera, gameObj);
            gamescreen.SetFieldOfView(field_of_view);

            // initialize player_ship's MFD choices:
            Player.SelectPlayer(Player.GetCurrentPlayer());
        }
        public void OpenTacticalReference()
        {
            if (menuscreen != null && game_mode == MODE.MENU_MODE)
            {
                menuscreen.ShowLoadDlg();

                LoadDlg load_dlg = menuscreen.GetLoadDlg();

                if (load_dlg != null && load_dlg.IsShown())
                {
                    load_activity = Game.GetText("Starshatter.load.tac-ref");
                    load_progress = 1;
                    catalog_index = 0;
                }
                else
                {
                    menuscreen.ShowTacRefDlg();
                }
            }
        }
        public void CreateWorld()
        {
            RadioTraffic.Initialize();
            RadioView.Initialize();
            RadioVox.Initialize();
            QuantumView.Initialize();
            QuitView.Initialize();
            TacticalView.Initialize();

            // create world
            if (world == null)
            {
                Sim sim = new Sim(input);
                world = sim;
                ErrLogger.PrintLine("  World Created.");
            }

            cam_dir = CameraDirector.GetInstance();
        }

        public void PlayerCam(CameraDirector.CAM_MODE mode) { throw new NotImplementedException(); }
        public void ViewSelection() { throw new NotImplementedException(); }

        public void MapKeys()
        {
            int nkeys = keycfg.GetNumKeys();

            if (nkeys > 0)
            {
                Starshatter.MapKeys(keycfg, nkeys);
                input.MapKeys(keycfg.GetMapping(), nkeys);
            }
        }

        public static void MapKeys(KeyMap mapping, int nkeys)
        {
            for (int i = 0; i < nkeys; i++)
            {
                KeyMapEntry k = mapping.GetKeyMap(i);

                if (k.act >= KeyMap.KEY_MAP_FIRST && k.act <= KeyMap.KEY_MAP_LAST)
                    MapKey(k.act, k.key, k.alt);
            }
        }

        public static void MapKey(int act, int key, int alt = 0)
        {
            keymap[act] = key;
            keyalt[act] = alt;
#if TODO
            GetAsyncKeyState(key);
            GetAsyncKeyState(alt);
#endif
        }


        public void SetTestMode(bool t) { test_mode = t; }
        public new static Starshatter GetInstance() { return instance; }
        public override int GetScreenWidth()
        {
            if (video_settings != null)
                return video_settings.Width;

            return 0;
        }
        public override int GetScreenHeight()
        {
            if (video_settings != null)
                return video_settings.Height;

            return 0;
        }

        // graphic options:
        public int LensFlare() { return lens_flare; }
        public int Corona() { return corona; }
        public int Nebula() { return nebula; }
        public int Dust() { return dust; }

        public KeyMap GetKeyMap() { return keycfg; }

        public int GetLoadProgress() { return load_progress; }
        public string GetLoadActivity() { return load_activity; }

        public void InvalidateTextureCache()
        {
            if (video !=null )
                video.InvalidateCache();
        }


        public int GetChatMode() { return chat_mode; }
        public void SetChatMode(int c) { throw new NotImplementedException(); }
        public string GetChatText() { return chat_text; }

        public void StopNetGame() { throw new NotImplementedException(); }

        public LOBBY GetLobbyMode() { throw new NotImplementedException(); }
        public void SetLobbyMode(LOBBY mode = LOBBY.NET_LOBBY_CLIENT) { throw new NotImplementedException(); }
        public void StartLobby() { throw new NotImplementedException(); }
        public void StopLobby() { throw new NotImplementedException(); }

        public void ExecCutscene(string msn_file, string path)
        {
            if (InCutscene() || string.IsNullOrEmpty(msn_file)) return;

            if (world == null)
                CreateWorld();

            cutscene_mission = new Mission(0);
            cutscene_basetime = StarSystem.GetBaseTime();

            if (cutscene_mission.Load(msn_file, path))
            {
                Sim sim = (Sim)world;

                if (sim != null)
                {
                    bool dynamic = false;
                    Campaign campaign = Campaign.GetCampaign();

                    if (campaign != null && campaign.IsDynamic())
                        dynamic = true;

                    sim.UnloadMission();
                    sim.LoadMission(cutscene_mission, true); // attempt to preload the tex cache
                    sim.ExecMission();
                    sim.ShowGrid(false);
                    player_ship = sim.GetPlayerShip();

                    ErrLogger.PrintLine("  Cutscene Instantiated.\n");

                    UpdateWorld();
                }
            }
            else
            {
                //delete cutscene_mission;
                cutscene_mission = null;
                cutscene_basetime = 0;
            }
        }
        public void BeginCutscene()
        {
            Sim sim = Sim.GetSim();

            if (cutscene == 0 && !sim.IsNetGame())
            {
                HUDView hud_view = HUDView.GetInstance();
                if (hud_view != null)
                    hud_view.SetHUDMode(HUDView.HUDModes.HUD_MODE_OFF);

                if (sim.GetPlayerShip() != null)
                    sim.GetPlayerShip().SetControls(null);

                AudioConfig audio_cfg = AudioConfig.GetInstance();

                if (audio_cfg != null)
                {
                    cut_efx_volume = audio_cfg.GetEfxVolume();
                    cut_wrn_volume = audio_cfg.GetWrnVolume();
                }

                Ship.SetFlightModel(FLIGHT_MODEL.FM_ARCADE);
            }

            cutscene++;
        }
        public void EndCutscene()
        {
            cutscene--;

            if (cutscene == 0)
            {
                DisplayView disp_view = DisplayView.GetOrCreateInstance(gamewin);
                if (disp_view != null)
                    disp_view.ClearDisplay();

                HUDView hud_view = HUDView.GetInstance();
                if (hud_view != null)
                    hud_view.SetHUDMode(HUDView.HUDModes.HUD_MODE_TAC);

                Sim sim = Sim.GetSim();
                if (sim.GetPlayerShip() != null)
                    sim.GetPlayerShip().SetControls(sim.GetControls());

                if (cam_dir != null)
                {
                    cam_dir.SetViewOrbital(null);
                    CameraDirector.SetRangeLimits(10, CameraDirector.GetRangeLimit());
                    cam_dir.SetOrbitPoint(Math.PI / 4, Math.PI / 4, 1);
                    cam_dir.SetOrbitRates(0, 0, 0);
                }

                AudioConfig audio_cfg = AudioConfig.GetInstance();

                if (audio_cfg != null)
                {
                    audio_cfg.SetEfxVolume(cut_efx_volume);
                    audio_cfg.SetWrnVolume(cut_wrn_volume);
                }

                Player p = Player.GetCurrentPlayer();
                if (p != null)
                    Ship.SetFlightModel(p.FlightModel());
            }
        }
        public bool InCutscene() { return cutscene > 0; }
        public Mission GetCutsceneMission()
        {
            return cutscene_mission;
        }

        public string GetSubtitles()
        {
            if (cutscene_mission != null)
                return cutscene_mission.Subtitles();

            return "";
        }
        public void EndMission()
        {
            if (cutscene_mission != null)
            {
                Sim sim = Sim.GetSim();

                if (sim != null && sim.GetMission() == cutscene_mission)
                {
                    ShipStats.Initialize();
                    sim.UnloadMission();

                    // restore world clock (true => absolute time reference)
                    if (cutscene_basetime != 0)
                        StarSystem.SetBaseTime(cutscene_basetime, true);

                    //delete cutscene_mission;
                    cutscene_mission = null;
                    cutscene_basetime = 0;

                    return;
                }
            }

            SetGameMode(Starshatter.MODE.PLAN_MODE);
        }

        public void StartOrResumeGame()
        {
            if (game_mode != MODE.MENU_MODE && game_mode != MODE.CMPN_MODE)
                return;

            Player p = Player.GetCurrentPlayer();
            if (p == null)
                return;

            List<Campaign> list = Campaign.GetAllCampaigns();
            Campaign c = null;
            string saved = CampaignSaveGame.GetResumeFile();


            // resume saved game?
            if (!string.IsNullOrEmpty(saved))
            {
                CampaignSaveGame savegame = new CampaignSaveGame();
                savegame.Load(saved);
                c = savegame.GetCampaign();
            }

            // start training campaign?
            else if (p.Trained() < 255)
            {
                c = list[0];
                c.Load();
            }

            // start new dynamic campaign sequence?
            else
            {
                c = list[1];
                c.Load();
            }

            if (c != null)
                Campaign.SelectCampaign(c.Name());

            Mouse.Show(false);
            SetGameMode(MODE.CLOD_MODE);
        }
        protected void ParseArgs(string[] cmdline)
        {
            if (cmdline.Contains("-win") || cmdline.Contains("-dbg"))
            {
                videoSettings.is_windowed = true;
                ErrLogger.PrintLine("   STARSHATTER RUNNING IN WINDOW MODE");
            }
            if (cmdline.Contains("-nosplash"))
            {
                no_splash = true;
            }
        }

        public override bool Init(Camera cam, GameObject gameObject)
        {
            string[] args = System.Environment.GetCommandLineArgs();
            ParseArgs(args);
            return base.Init(cam, gameObject);
        }

        public static bool UseFileSystem()
        {
            return use_file_system;
        }
        public new bool Paused()
        {
            return Game.Paused();
        }
        protected virtual void DoMenuScreenFrame()
        {
            if (Mouse.RButton() == 0)
            {
                Mouse.SetCursor(Mouse.CURSOR.ARROW);
                Mouse.Show(true);
            }

            if (time_til_change > 0)
                time_til_change -= Game.GUITime();

            if (menuscreen == null)
                return;

            if (Keyboard.KeyDown(KeyMap.KEY_EXIT))
            {
                if (time_til_change <= 0)
                {
                    time_til_change = 0.5;

                    if (!exit_latch && !menuscreen.CloseTopmost())
                    {
                        menuscreen.ShowExitDlg();
                    }
                }

                exit_latch = true;
            }
            else
            {
                exit_latch = false;
            }
            LoadDlg load_dlg = menuscreen.GetLoadDlg();

            if (load_dlg != null && load_dlg.IsShown())
            {
                // load all ship designs in the standard catalog
                if (catalog_index < ShipDesign.StandardCatalogSize())
                {
                    ShipDesign.PreloadCatalog(catalog_index++);
                    load_activity = Game.GetText("Starshatter.load.tac-ref");

                    //if (load_progress < 95)
                    //    load_progress++;
                    load_progress = (int)(catalog_index * 100f / ShipDesign.StandardCatalogSize());
                }

                else
                {
                    menuscreen.ShowTacRefDlg();
                }
            }
            if (show_missions)
            {
                if (net_lobby != null)
                    menuscreen.ShowNetLobbyDlg();
                else
                    menuscreen.ShowMsnSelectDlg();

                show_missions = false;
            }

            menuscreen.ExecFrame();

#if TODO
            if (req_change_video)
            {
                ChangeVideo();
                SetupMenuScreen();
            }
#endif
        }

        protected virtual void DoCmpnScreenFrame()
        {
            {
                Mouse.SetCursor(Mouse.CURSOR.ARROW);

                if (time_til_change > 0)
                    time_til_change -= Game.GUITime();

                exit_latch = Keyboard.KeyDown(KeyMap.KEY_EXIT) ? true : false;

                if (InCutscene() && player_ship != null)
                {
                    // warp effect:
                    if (player_ship.WarpFactor() > 1)
                    {
                        if (player_ship.WarpFactor() > field_of_view)
                            cmpnscreen.SetFieldOfView(player_ship.WarpFactor());
                        else
                            cmpnscreen.SetFieldOfView(field_of_view);
                    }

                    else
                    {
                        if (cmpnscreen.GetFieldOfView() != field_of_view)
                            cmpnscreen.SetFieldOfView(field_of_view);
                    }
                }

                if (InCutscene() && exit_latch)
                {
                    time_til_change = 1;
                    EndCutscene();
                    EndMission();
                    cmpnscreen.SetFieldOfView(field_of_view);
                }

                else if (time_til_change <= 0 && exit_latch)
                {
                    time_til_change = 1;

                    if (cmpnscreen == null || !cmpnscreen.CloseTopmost())
                    {
                        SetGameMode(MODE.MENU_MODE);
                    }
                }

                // time control for campaign mode:
                else if (game_mode == MODE.CMPN_MODE)
                {
                    if (time_til_change <= 0)
                    {
                        if (Keyboard.KeyDown(KeyMap.KEY_PAUSE))
                        {
                            time_til_change = 1;
                            Pause(!paused);
                        }

                        else if (Keyboard.KeyDown(KeyMap.KEY_TIME_COMPRESS))
                        {
                            time_til_change = 1;

                            switch (TimeCompression())
                            {
                                case 1: SetTimeCompression(2); break;
                                case 2: SetTimeCompression(4); break;
                                case 4: SetTimeCompression(8); break;
                            }
                        }

                        else if (Keyboard.KeyDown(KeyMap.KEY_TIME_EXPAND))
                        {
                            time_til_change = 1;

                            switch (TimeCompression())
                            {
                                case 8: SetTimeCompression(4); break;
                                case 4: SetTimeCompression(2); break;
                                default: SetTimeCompression(1); break;
                            }
                        }
                    }
                }

                if (show_missions && !InCutscene())
                {
                    cmpnscreen.ShowCmdMissionsDlg();
                    show_missions = false;
                }

                cmpnscreen.ExecFrame();
            }
        }

        protected virtual void DoPlanScreenFrame()
        {
            Mouse.SetCursor(Mouse.CURSOR.ARROW);

            if (time_til_change > 0)
                time_til_change -= Game.GUITime();

            if (Keyboard.KeyDown(KeyMap.KEY_EXIT))
            {
                if (time_til_change <= 0)
                {
                    time_til_change = 1;

                    if (!exit_latch && !planscreen.CloseTopmost())
                    {
                        Campaign campaign = Campaign.GetCampaign();
                        if (campaign != null && (campaign.IsDynamic() || campaign.IsTraining()))
                            SetGameMode(MODE.CMPN_MODE);
                        else
                            SetGameMode(MODE.MENU_MODE);
                    }
                }

                exit_latch = true;
            }
            else
            {
                exit_latch = false;
            }

            planscreen.ExecFrame();
            show_missions = true;
        }

        protected virtual void DoLoadScreenFrame()
        {
            Mouse.Show(false);
            Mouse.SetCursor(Mouse.CURSOR.ARROW);

            if (game_mode == MODE.CLOD_MODE)
            {
                CmpLoadDlg dlg = loadscreen.GetCmpLoadDlg();

                switch (load_step)
                {
                    case 0:
                        load_activity = Game.GetText("Starshatter.load.campaign");
                        load_progress = 1;
                        catalog_index = 0;
                        break;

                    case 1:
                        // load all ship designs in the standard catalog
                        if (catalog_index < ShipDesign.StandardCatalogSize())
                        {
                            ShipDesign.PreloadCatalog(catalog_index++);
                            load_activity = Game.GetText("Starshatter.load.campaign");

                            if (load_progress < 80)
                                load_progress++;

                            load_step = 0; // force return to current step on next frame
                        }
                        else
                        {
                            load_activity = Game.GetText("Starshatter.load.start");
                            load_progress = 80;
                        }
                        break;

                    case 2:
                        if (Campaign.GetCampaign() != null)
                            Campaign.GetCampaign().Start();
                        break;

                    default:
                        if (dlg != null && load_progress < 100)
                        {
                            load_progress++;
                        }
                        else
                        {
                            load_activity = Game.GetText("Starshatter.load.ready");
                            load_progress = 100;
                            SetGameMode(MODE.CMPN_MODE);
                        }
                        break;
                }
            }
            else
            {
                switch (load_step)
                {
                    case 0:
                        load_activity = Game.GetText("Starshatter.load.drives");
                        load_progress = 0;
                        break;

                    case 1:
                        Drive.Initialize();
                        LandingGear.Initialize();
                        load_activity = Game.GetText("Starshatter.load.explosions");
                        load_progress = 5;
                        break;

                    case 2:
                        Explosion.Initialize();
                        load_activity = Game.GetText("Starshatter.load.systems");
                        load_progress = 10;
                        break;

                    case 3:
                        FlightDeck.Initialize();
                        NavLight.Initialize();
                        load_activity = Game.GetText("Starshatter.load.ships");
                        load_progress = 15;
                        break;

                    case 4:
                        Ship.Initialize();
                        Shot.Initialize();
                        load_activity = Game.GetText("Starshatter.load.hud");
                        load_progress = 20;
                        break;

                    case 5:
                        MFD.Initialize();
                        load_activity = Game.GetText("Starshatter.load.menus");
                        load_progress = 25;
                        break;

                    case 6:
                        RadioTraffic.Initialize();
                        RadioView.Initialize();
                        TacticalView.Initialize();

                        if (gamescreen == null && game_mode != MODE.PREP_MODE)
                            SetupGameScreen();

                        load_activity = Game.GetText("Starshatter.load.mission");
                        load_progress = 65;
                        break;

                    case 7:
                        if (game_mode == MODE.PREP_MODE)
                        {
                            if (Campaign.GetCampaign() != null)
                                Campaign.GetCampaign().GetMission();
                            SetGameMode(MODE.PLAN_MODE);
                            load_activity = Game.GetText("Starshatter.load.loaded");
                            load_progress = 100;
                            Button.PlaySound(Button.SOUNDS.SND_ACCEPT);
                        }
                        else
                        {
                            CreateWorld();
                            load_activity = Game.GetText("Starshatter.load.simulation");
                            load_progress = 75;
                        }
                        break;

                    case 8:
                        InstantiateMission();
                        load_activity = Game.GetText("Starshatter.load.viewscreen");
                        load_progress = 90;
                        break;

                    default:
                        SetGameMode(MODE.PLAY_MODE);
                        load_activity = Game.GetText("Starshatter.load.ready");
                        load_progress = 100;
                        break;
                }
            }

            load_step++;
            loadscreen.ExecFrame();
        }
        protected virtual void DoGameScreenFrame()
        {
            Sim sim = (Sim)world;

            if (gamescreen == null || sim == null)
                return;

            if (InCutscene())
            {
                if (player_ship != null)
                {
                    // warp effect:
                    if (player_ship.WarpFactor() > 1)
                    {
                        if (player_ship.WarpFactor() > field_of_view)
                            gamescreen.SetFieldOfView(player_ship.WarpFactor());
                        else
                            gamescreen.SetFieldOfView(field_of_view);
                    }

                    else
                    {
                        if (gamescreen.GetFieldOfView() != field_of_view)
                            gamescreen.SetFieldOfView(field_of_view);
                    }
                }

                gamescreen.FrameRate(FrameRate());
                gamescreen.ExecFrame();

                if (Keyboard.KeyDown(KeyMap.KEY_EXIT))
                {
                    gamescreen.SetFieldOfView(field_of_view);
                    exit_latch = true;
                    time_til_change = 1;

                    sim.SkipCutscene();
                }

                return;
            }

            if (time_til_change > 0)
                time_til_change -= Game.GUITime();

            if (exit_latch && !Keyboard.KeyDown(KeyMap.KEY_EXIT))
                exit_latch = false;

            DoMouseFrame();

            HUDView hud_view = HUDView.GetInstance();

            // changing to a new ship?
            if (player_ship != sim.GetPlayerShip())
            {
                gamescreen.HideNavDlg();
                gamescreen.HideFltDlg();
                gamescreen.HideEngDlg();

                Ship new_player = sim.GetPlayerShip();

                if (new_player != null)
                {
                    if (new_player.IsDropship() && Ship.GetFlightModel() < FLIGHT_MODEL.FM_ARCADE && Ship.GetControlModel() < 1)
                        input.SwapYawRoll(true);
                    else
                        input.SwapYawRoll(false);

                    if (hud_view != null)
                    {
                        hud_view.SetHUDMode(HUDView.HUDModes.HUD_MODE_TAC);
                        hud_view.HideHUDWarn();
                    }
                }
            }

            player_ship = sim.GetPlayerShip();

            if (player_ship != null)
            {
                // warp effect:
                if (player_ship.WarpFactor() > 1)
                {
                    if (player_ship.WarpFactor() > field_of_view)
                        gamescreen.SetFieldOfView(player_ship.WarpFactor());
                    else
                        gamescreen.SetFieldOfView(field_of_view);
                }

                else
                {
                    if (gamescreen.GetFieldOfView() != field_of_view)
                        gamescreen.SetFieldOfView(field_of_view);
                }

                gamescreen.ShowExternal();

                if (CameraDirector.GetCameraMode() >= CameraDirector.CAM_MODE.MODE_ORBIT && !player_ship.InTransition())
                    tactical = true;
                else
                    tactical = false;

                if (player_ship.InTransition())
                    gamescreen.CloseTopmost();
            }

            if (chat_mode != 0)
            {
                DoChatMode();
            }

            else
            {
                DoGameKeys();
            }

            gamescreen.FrameRate(FrameRate());
            gamescreen.ExecFrame();

            if (Game.GameTime() - time_mark > 60000)
            {
                time_mark = (ulong)Game.GameTime();
                minutes++;
                if (minutes > 60)
                    ErrLogger.PrintLine("  TIME %2d:%02d:00\n", minutes / 60, minutes % 60);
                else
                    ErrLogger.PrintLine("  TIME    %2d:00\n", minutes);
            }
        }

        static int old_mouse_x = 0;
        static int old_mouse_y = 0;

        // radio message hot keys:
        static bool comm_key = false;

        protected virtual void DoMouseFrame()
        {
            EventDispatch event_dispatch = EventDispatch.GetInstance();
            if (event_dispatch != null && event_dispatch.GetCapture() != null)
                return;

            mouse_dx = 0;
            mouse_dy = 0;


            if (!spinning && Mouse.RButton() != 0)
            {
                spinning = true;
                old_mouse_x = (int)Mouse.X();
                old_mouse_y = (int)Mouse.Y();
                mouse_x = (int)Mouse.X();
                mouse_y = (int)Mouse.Y();
                Mouse.Show(false);
            }

            else if (spinning)
            {
                if (Mouse.RButton() == 0)
                {
                    spinning = false;

                    if (tactical)
                    {
                        Mouse.Show(true);
                        Mouse.SetCursor(Mouse.CURSOR.ARROW);
                    }

                    Mouse.SetCursorPos(old_mouse_x, old_mouse_y);
                }

                else
                {
                    mouse_dx = (int)Mouse.X() - mouse_x;
                    mouse_dy = (int)Mouse.Y() - mouse_y;

                    Mouse.SetCursorPos(mouse_x, mouse_y);
                }
            }

            else if (cutscene != 0 || player_ship == null)
            {
                Mouse.Show(false);
                return;
            }

            // hide mouse cursor when mouse controller is actively steering:
            else if (mouse_input != null && mouse_input.Active())
            {
                if (mouse_input.Selector() == 1)
                {
                    Mouse.Show(false);
                }

                else if (mouse_input.Selector() == 2)
                {
                    Mouse.Show(true);
                    Mouse.SetCursor(Mouse.CURSOR.CROSS);
                }
            }

            else
            {
                HUDView hud_view = HUDView.GetInstance();

                if (hud_view != null && hud_view.GetHUDMode() != HUDView.HUDModes.HUD_MODE_OFF)
                {
                    Mouse.Show(true);
                    Mouse.SetCursor(Mouse.CURSOR.ARROW);
                }
            }

            if (gamescreen != null && gamescreen.IsFormShown())
                return;

            TacticalView tac_view = TacticalView.GetInstance();

            if (tac_view != null)
                tac_view.DoMouseFrame();
        }

        protected virtual void DoChatMode() { throw new NotImplementedException(); }
        protected virtual void DoGameKeys()
        {
            Sim sim = (Sim)world;
            HUDView hud_view = HUDView.GetInstance();

            if (time_til_change <= 0)
            {
                if (Keyboard.KeyDown(KeyMap.KEY_CAM_BRIDGE))
                {
                    PlayerCam(CameraDirector.CAM_MODE.MODE_COCKPIT);
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_CAM_VIRT))
                {
                    PlayerCam(CameraDirector.CAM_MODE.MODE_VIRTUAL);
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_CAM_CHASE))
                {
                    PlayerCam(CameraDirector.CAM_MODE.MODE_CHASE);
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_CAM_DROP))
                {
                    PlayerCam(CameraDirector.CAM_MODE.MODE_DROP);
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_CAM_EXTERN))
                {
                    PlayerCam(CameraDirector.CAM_MODE.MODE_ORBIT);
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_TARGET_PADLOCK))
                {
                    PlayerCam(CameraDirector.CAM_MODE.MODE_TARGET);
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_SWAP_ROLL_YAW))
                {
                    input.SwapYawRoll(!input.GetSwapYawRoll());
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_ZOOM_WIDE))
                {
                    time_til_change = 0.5;
                    if (gamescreen.GetFieldOfView() <= 2)
                        field_of_view = 3; // wide
                    else
                        field_of_view = 2; // normal

                    // don't mess with fov during warp:
                    if (player_ship != null && player_ship.WarpFactor() <= 1)
                        gamescreen.SetFieldOfView(field_of_view);
                }

                else if (!exit_latch && Keyboard.KeyDown(KeyMap.KEY_EXIT))
                {
                    exit_latch = true;
                    time_til_change = 0.5;

                    if (!gamescreen.CloseTopmost())
                    {
                        QuitView quit = QuitView.GetInstance();
                        if (quit != null)
                            quit.ShowMenu();
                        else
                            SetGameMode(Starshatter.MODE.PLAN_MODE);
                    }
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_PAUSE))
                {
                    Pause(!paused);
                    time_til_change = 0.5;
                }


                else if (Keyboard.KeyDown(KeyMap.KEY_TIME_COMPRESS))
                {
                    time_til_change = 0.5;
                    if (NetGame.IsNetGame())
                        SetTimeCompression(1);
                    else
                        switch (TimeCompression())
                        {
                            case 1: SetTimeCompression(2); break;
                            default: SetTimeCompression(4); break;
                        }
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_TIME_EXPAND))
                {
                    time_til_change = 0.5;

                    if (NetGame.IsNetGame())
                        SetTimeCompression(1);
                    else
                        switch (TimeCompression())
                        {
                            case 4: SetTimeCompression(2); break;
                            default: SetTimeCompression(1); break;
                        }
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_TIME_SKIP))
                {
                    time_til_change = 0.5;

                    if (player_ship != null && !NetGame.IsNetGame())
                    {
                        player_ship.TimeSkip();
                    }
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_COMMAND_MODE))
                {
                    if (player_ship != null)
                    {
                        time_til_change = 0.5;
                        player_ship.CommandMode();
                    }
                }

                /*** For Debug Convenience Only: ***/
                else if (Keyboard.KeyDown(KeyMap.KEY_INC_STARDATE))
                {
                    StarSystem.SetBaseTime(StarSystem.GetBaseTime() + 600, true);
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_DEC_STARDATE))
                {
                    StarSystem.SetBaseTime(StarSystem.GetBaseTime() - 600, true);
                }
                /***/
            }

            if (gamescreen != null && time_til_change <= 0)
            {
                if (Keyboard.KeyDown(KeyMap.KEY_MFD1))
                {
                    gamescreen.CycleMFDMode(0);
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_MFD2))
                {
                    gamescreen.CycleMFDMode((MFD.Modes)1);
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_MFD3))
                {
                    gamescreen.CycleMFDMode((MFD.Modes)2);
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_MFD4))
                {
                    gamescreen.CycleMFDMode((MFD.Modes)3);
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_RADIO_MENU))
                {
                    RadioView radio_view = RadioView.GetInstance();
                    if (radio_view != null)
                    {
                        if (radio_view.IsMenuShown())
                            radio_view.CloseMenu();
                        else
                            radio_view.ShowMenu();
                    }
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_QUANTUM_MENU))
                {
                    QuantumView quantum_view = QuantumView.GetInstance();
                    if (quantum_view != null)
                    {
                        if (quantum_view.IsMenuShown())
                            quantum_view.CloseMenu();
                        else
                            quantum_view.ShowMenu();
                    }
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_CAM_VIEW_SELECTION))
                {
                    time_til_change = 0.5;
                    ViewSelection();
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_HUD_MODE))
                {
                    time_til_change = 0.5;
                    if (hud_view != null)
                        hud_view.CycleHUDMode();
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_HUD_COLOR))
                {
                    time_til_change = 0.5;
                    if (hud_view != null)
                        hud_view.CycleHUDColor();
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_HUD_WARN))
                {
                    time_til_change = 0.5;
                    if (hud_view != null)
                        hud_view.CycleHUDWarn();
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_HUD_INST))
                {
                    time_til_change = 0.5;
                    if (hud_view != null)
                        hud_view.CycleHUDInst();
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_SELF_DESTRUCT))
                {
                    time_til_change = 0.5;

                    if (player_ship != null && !player_ship.InTransition())
                    {
                        double damage = player_ship.Design().scuttle;

                        if (NetGame.IsNetGameClient())
                        {
                            NetUtil.SendSelfDestruct(player_ship, damage);
                        }
                        else
                        {
                            Point scuttle_loc = player_ship.Location() + RandomHelper.RandomDirection() * player_ship.Radius();
                            player_ship.InflictDamage(damage, null, 1, scuttle_loc);
                        }

                        if (player_ship.Integrity() < 1)
                        {
                            ErrLogger.PrintLine("  {0} 0-0-0-Destruct-0\n", player_ship.Name());

                            ShipStats s = ShipStats.Find(player_ship.Name());
                            if (s != null)
                                s.AddEvent(SimEvent.EVENT.DESTROYED, player_ship.Name());

                            player_ship.DeathSpiral();
                        }
                    }
                }
            }

            if (gamescreen != null && player_ship != null &&
                time_til_change <= 0 && !Keyboard.GetAsyncKeyState(KeyMap.VK_SHIFT) && !Keyboard.GetAsyncKeyState(KeyMap.VK_MENU))
            {
                if (Keyboard.KeyDown(KeyMap.KEY_NAV_DLG))
                {
                    gamescreen.ShowNavDlg();
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_WEP_DLG))
                {
                    if (player_ship != null && player_ship.Design().wep_screen)
                        gamescreen.ShowWeaponsOverlay();
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_FLT_DLG))
                {
                    if (player_ship != null && player_ship.NumFlightDecks() > 0)
                        gamescreen.ShowFltDlg();
                    time_til_change = 0.5;
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_ENG_DLG))
                {
                    if (player_ship != null && player_ship.Design().repair_screen)
                        gamescreen.ShowEngDlg();
                    time_til_change = 0.5;
                }
            }

            if (cam_dir != null)
            {
                double spin = (Math.PI / 2) * Game.FrameTime(); // Game.GUITime();

                if (avi_file != null)
                    spin /= 6;

                if (Keyboard.KeyDown(KeyMap.KEY_CAM_EXT_PLUS_AZ))
                    cam_dir.ExternalAzimuth(spin);

                else if (Keyboard.KeyDown(KeyMap.KEY_CAM_EXT_MINUS_AZ))
                    cam_dir.ExternalAzimuth(-spin);

                if (Keyboard.KeyDown(KeyMap.KEY_CAM_EXT_PLUS_EL))
                    cam_dir.ExternalElevation(spin);

                else if (Keyboard.KeyDown(KeyMap.KEY_CAM_EXT_MINUS_EL))
                    cam_dir.ExternalElevation(-spin);

                if (Keyboard.KeyDown(KeyMap.KEY_CAM_VIRT_PLUS_AZ))
                    cam_dir.VirtualAzimuth(-spin);

                else if (Keyboard.KeyDown(KeyMap.KEY_CAM_VIRT_MINUS_AZ))
                    cam_dir.VirtualAzimuth(spin);

                if (Keyboard.KeyDown(KeyMap.KEY_CAM_VIRT_PLUS_EL))
                    cam_dir.VirtualElevation(spin);

                else if (Keyboard.KeyDown(KeyMap.KEY_CAM_VIRT_MINUS_EL))
                    cam_dir.VirtualElevation(-spin);

                if (Keyboard.KeyDown(KeyMap.KEY_CAM_EXT_PLUS_RANGE))
                {
                    if (!gamescreen.IsNavShown())
                    {
                        cam_dir.ExternalRange((float)(1 + 1.5 * Game.FrameTime())); // 1.1f);
                    }
                }

                else if (Keyboard.KeyDown(KeyMap.KEY_CAM_EXT_MINUS_RANGE))
                {
                    if (!gamescreen.IsNavShown())
                    {
                        cam_dir.ExternalRange((float)(1 - 1.5 * Game.FrameTime())); // 0.9f);
                    }
                }

                if (tactical && !gamescreen.IsFormShown())
                {
                    if (Mouse.Wheel() != 0)
                    {
                        int w = (int)Mouse.Wheel();

                        if (w < 0)
                        {
                            while (w < 0)
                            {
                                cam_dir.ExternalRange(1.25f);
                                w += 120;
                            }
                        }
                        else
                        {
                            while (w > 0)
                            {
                                cam_dir.ExternalRange(0.75f);
                                w -= 120;
                            }
                        }
                    }

                    else if (Mouse.LButton() != 0 && Mouse.RButton() != 0)
                    {
                        if (mouse_dy < 0)
                            cam_dir.ExternalRange(0.85f);
                        else if (mouse_dy > 0)
                            cam_dir.ExternalRange(1.15f);
                    }

                    else if (Mouse.MButton() != 0 && time_til_change <= 0)
                    {
                        time_til_change = 0.5;
                        ViewSelection();
                    }

                    else
                    {
                        if (mouse_dx != 0 || mouse_dy != 0)
                        {
                            if (CameraDirector.GetCameraMode() == CameraDirector.CAM_MODE.MODE_VIRTUAL)
                            {
                                cam_dir.VirtualAzimuth(mouse_dx * 0.2 * ConstantsF.DEGREES);
                                cam_dir.VirtualElevation(mouse_dy * 0.2 * ConstantsF.DEGREES);
                            }
                            else
                            {
                                cam_dir.ExternalAzimuth(mouse_dx * 0.2 * ConstantsF.DEGREES);
                                cam_dir.ExternalElevation(mouse_dy * 0.2 * ConstantsF.DEGREES);
                            }
                        }
                    }
                }
            }

            if (Keyboard.KeyDown(KeyMap.KEY_COMM_ATTACK_TGT))
            {
                if (!comm_key)
                    RadioTraffic.SendQuickMessage(player_ship, RadioMessage.ACTION.ATTACK);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_COMM_ESCORT_TGT))
            {
                if (!comm_key)
                    RadioTraffic.SendQuickMessage(player_ship, RadioMessage.ACTION.ESCORT);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_COMM_WEP_FREE))
            {
                if (!comm_key)
                    RadioTraffic.SendQuickMessage(player_ship, RadioMessage.ACTION.WEP_FREE);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_COMM_WEP_HOLD))
            {
                if (!comm_key)
                    RadioTraffic.SendQuickMessage(player_ship, RadioMessage.ACTION.FORM_UP);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_COMM_COVER_ME))
            {
                if (!comm_key)
                    RadioTraffic.SendQuickMessage(player_ship, RadioMessage.ACTION.COVER_ME);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_COMM_SKIP_NAV))
            {
                if (!comm_key)
                    RadioTraffic.SendQuickMessage(player_ship, RadioMessage.ACTION.SKIP_NAVPOINT);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_COMM_RETURN_TO_BASE))
            {
                if (!comm_key)
                    RadioTraffic.SendQuickMessage(player_ship, RadioMessage.ACTION.RTB);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_COMM_CALL_INBOUND))
            {
                if (!comm_key)
                    RadioTraffic.SendQuickMessage(player_ship, RadioMessage.ACTION.CALL_INBOUND);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_COMM_REQUEST_PICTURE))
            {
                if (!comm_key)
                    RadioTraffic.SendQuickMessage(player_ship, RadioMessage.ACTION.REQUEST_PICTURE);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_COMM_REQUEST_SUPPORT))
            {
                if (!comm_key)
                    RadioTraffic.SendQuickMessage(player_ship, RadioMessage.ACTION.REQUEST_SUPPORT);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_CHAT_BROADCAST))
            {
                if (!comm_key)
                    SetChatMode((int)CHAT_MODES.CHAT_BROADCAST);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_CHAT_TEAM))
            {
                if (!comm_key)
                    SetChatMode((int)CHAT_MODES.CHAT_TEAM);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_CHAT_WING))
            {
                if (!comm_key)
                    SetChatMode((int)CHAT_MODES.CHAT_WING);
                comm_key = true;
            }

            else if (Keyboard.KeyDown(KeyMap.KEY_CHAT_UNIT) && sim.GetSelection().Capacity != 0)
            {
                if (!comm_key)
                    SetChatMode((int)CHAT_MODES.CHAT_UNIT);
                comm_key = true;
            }

            else
            {
                comm_key = false;
            }
        }

        public override bool GameLoop()
        {
            cam_dir = CameraDirector.GetInstance();

            if (active && paused)
            {
#if TODO
                // Route Events to EventTargets
                EventDispatch ed = EventDispatch.GetInstance();
                if (ed != null)
                    ed.Dispatch();
#endif

                UpdateWorld();
                GameState();
                UpdateScreen();
                CollectStats();

                /***
                static DWORD vmf_time = 0;

                if (real_time() - vmf_time > 5000) {
                    vmf_time = real_time();
                    DWORD vmf = video.VidMemFree() / (1024 * 1024);
                    ::Print("\n###### %02d:%02d - Video Memory Free: %d MB\n\n",
                            vmf_time / 60000,
                            vmf_time /  1000,
                            vmf);
                }
                ***/
            }

            base.GameLoop();
            return false;  // must return false to keep processing
                           // true tells the outer loop to sleep until a
                           // windows event is available
        }
        protected override void UpdateWorld()
        {
            ulong new_time = real_time;
            double delta = new_time - (ulong)frame_time; // in milliseconds
            seconds = max_frame_length;      // in seconds
            gui_seconds = delta * 0.001;

            if (frame_time == 0)
                gui_seconds = 0;

            if (delta < time_comp * max_frame_length * 1000)
            {
                seconds = time_comp * delta * 0.001;
            }
            else
            {
                seconds = time_comp * max_frame_length;
            }

            frame_time = (int)new_time;

            Galaxy galaxy = Galaxy.GetInstance();
            if (galaxy != null) galaxy.ExecFrame();

            // Cutscene missions have a tendency to mess with the stardate
            // to manage time-of-day and camera effects.  It's a bad idea to
            // evaluate campaign actions and events while the cutscene is
            // changing the time base.
            if (cutscene_mission == null)
            {
                Campaign campaign = Campaign.GetCampaign();
                if (campaign != null) campaign.ExecFrame();
            }

            if (paused)
            {
                if (world != null)
                    world.ExecFrame(0);
            }

            else
            {
                game_time += (ulong)(seconds * 1000);

                Drive.StartFrame();

                if (world != null)
                    world.ExecFrame(seconds);
            }

            if (game_mode == MODE.PLAY_MODE || InCutscene())
            {
                if (cam_dir != null)
                {
                    if (head_tracker != null && head_tracker.IsRunning() && !InCutscene())
                    {
                        head_tracker.ExecFrame();
                        cam_dir.VirtualHead(head_tracker.GetAzimuth(), head_tracker.GetElevation());
                        cam_dir.VirtualHeadOffset(head_tracker.GetX(), head_tracker.GetY(), head_tracker.GetZ());
                    }

                    cam_dir.ExecFrame(gui_seconds);
                }

                Sim sim = Sim.GetSim();
                SimRegion rgn = sim != null ? sim.GetActiveRegion() : null;

                if (rgn != null)
                {
                    foreach (Ship s in rgn.Ships())
                    {
                        s.SelectDetail(seconds);
                    }
                }
            }
        }
        protected virtual void InstantiateMission()
        {
            //Memory::Check();

            current_mission = null;

            if (Campaign.GetCampaign() != null)
            {
                current_mission = Campaign.GetCampaign().GetMission();
            }

            Sim sim = (Sim)world;

            if (sim != null)
            {
                bool dynamic = false;
                Campaign campaign = Campaign.GetCampaign();

                if (campaign != null && campaign.IsDynamic())
                    dynamic = true;

                sim.UnloadMission();
                sim.LoadMission(current_mission);
                sim.ExecMission();
                sim.SetTestMode(test_mode && !dynamic ? true : false);

                ErrLogger.PrintLine("  Mission Instantiated.");
            }

            // Memory::Check();
        }
        protected virtual bool ResizeVideo() { throw new NotImplementedException(); }
        protected virtual void InitMouse()
        {
            Mouse.Create(screen);
            Mouse.Show(false);
            Mouse.LoadCursor(Mouse.CURSOR.ARROW, "MouseArrow", Mouse.HOTSPOT.HOTSPOT_NW);
            Mouse.LoadCursor(Mouse.CURSOR.CROSS, "MouseCross", Mouse.HOTSPOT.HOTSPOT_CTR);
            Mouse.LoadCursor(Mouse.CURSOR.DRAG, "MouseDrag", Mouse.HOTSPOT.HOTSPOT_NW);
            Mouse.SetCursor(Mouse.CURSOR.ARROW);
        }

        protected static Starshatter instance;

        protected Window gamewin;
        protected MenuScreen menuscreen;
        protected LoadScreen loadscreen;
        protected PlanScreen planscreen;
        protected GameScreen gamescreen;
        protected CmpnScreen cmpnscreen;

        protected FadeView splash;
        protected int splash_index;
        protected Bitmap splash_image;

        protected MultiController input;
        protected MouseController mouse_input;
        protected TrackIR head_tracker;

        protected Ship player_ship;
        protected CameraDirector cam_dir;
        protected MusicDirector music_dir;

        protected Font HUDfont;
        protected Font GUIfont;
        protected Font GUI_small_font;
        protected Font terminal;
        protected Font verdana;
        protected Font title_font;
        protected Font limerick18;
        protected Font limerick12;
        protected Font ocrb;

        protected ulong time_mark = 0;
        protected ulong minutes = 0;

        protected double field_of_view;
        protected double orig_fov;

        protected static int[] keymap = new int[256];
        protected static int[] keyalt = new int[256];
        protected KeyMap keycfg = new KeyMap();

        protected bool tactical;
        protected bool spinning;
        protected int mouse_x;
        protected int mouse_y;
        protected int mouse_dx;
        protected int mouse_dy;

        protected MODE game_mode;
        protected bool test_mode;
        protected VideoSettings videoSettings;
        private const string VideoConfigStr = "video.cfg";

        protected int lens_flare;
        protected int corona;
        protected int nebula;
        protected int dust;

        protected double exit_time;

        protected int load_step;
        protected int load_progress;
        protected string load_activity;
        protected int catalog_index;

        protected int cutscene;
        protected int lobby_mode;
        protected NetLobby net_lobby;
        protected int chat_mode;
        protected string chat_text;

        private int quick_mode = 0;
        private string quick_mission_name;
        private Mission quick_mission = null;

        private static Mission current_mission = null;
        private static Mission cutscene_mission = null;
        private static double cutscene_basetime = 0;
        private static int cut_efx_volume = 100;
        private static int cut_wrn_volume = 100;
        private static double time_til_change = 0;
        private static bool exit_latch = true;
        private static bool show_missions = false;
        private static bool use_file_system = false;
        private static bool no_splash = true; // TODO = false; in the original


        public enum CHAT_MODES
        {
            CHAT_BROADCAST = 1,
            CHAT_TEAM = 2,
            CHAT_WING = 3,
            CHAT_UNIT = 4
        };

    }
}
