﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      WeaponGroup.h/WeaponGroup.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Weapon Control Category (Group) class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.SimElements;

namespace StarshatterNet.Stars
{
#warning WeaponGroup class is still in development and is not recommended for production.
    public class WeaponGroup
    {
        public WeaponGroup(string name) { throw new System.NotImplementedException(); }
        // ~WeaponGroup();

        public void ExecFrame(double factor) { throw new System.NotImplementedException(); }

        // identification:
        public string Name() { return name; }
        public string Abbreviation() { return abrv; }
        public void SetName(string n) { throw new System.NotImplementedException(); }
        public void SetAbbreviation(string a) { throw new System.NotImplementedException(); }

        public bool IsPrimary() { throw new System.NotImplementedException(); }
        public bool IsDrone() { throw new System.NotImplementedException(); }
        public bool IsDecoy() { throw new System.NotImplementedException(); }
        public bool IsProbe() { throw new System.NotImplementedException(); }
        public bool IsMissile() { throw new System.NotImplementedException(); }
        public bool IsBeam() { throw new System.NotImplementedException(); }
        public int Value() { throw new System.NotImplementedException(); }

        // weapon list:
        public void AddWeapon(Weapon w) { throw new System.NotImplementedException(); }
        public int NumWeapons() { throw new System.NotImplementedException(); }
        public List<Weapon> GetWeapons() { throw new System.NotImplementedException(); }
        public bool Contains(Weapon w) { throw new System.NotImplementedException(); }

        // weapon selection:
        public void SelectWeapon(int n) { throw new System.NotImplementedException(); }
        public void CycleWeapon() { throw new System.NotImplementedException(); }
        public Weapon GetWeapon(int n) { throw new System.NotImplementedException(); }
        public Weapon GetSelected() { throw new System.NotImplementedException(); }

        // operations:
        public bool GetTrigger() { return trigger; }
        public void SetTrigger(bool t = true) { trigger = t; }
        public int Ammo() { return ammo; }
        public float Mass() { return mass; }
        public float Resistance() { return resist; }
        public void CheckAmmo() { throw new System.NotImplementedException(); }

        public void SetTarget(SimObject t, ShipSystem sub = null) { throw new System.NotImplementedException(); }
        public SimObject GetTarget() { throw new System.NotImplementedException(); }
        public ShipSystem GetSubTarget() { throw new System.NotImplementedException(); }
        public void DropTarget() { throw new System.NotImplementedException(); }
        public void SetFiringOrders(Weapon.Orders o) { throw new System.NotImplementedException(); }
        public Weapon.Orders GetFiringOrders() { return orders; }
        public void SetControlMode(int m) { throw new System.NotImplementedException(); }
        public int GetControlMode() { return control; }
        public void SetSweep(Weapon.Sweep s) { throw new System.NotImplementedException(); }
        public int GetSweep() { return sweep; }
        public int Status() { throw new System.NotImplementedException(); }

        public WeaponDesign GetDesign() { throw new System.NotImplementedException(); }
        public bool CanTarget(CLASSIFICATION tgt_class) { throw new System.NotImplementedException(); }

        public void PowerOn() { throw new System.NotImplementedException(); }
        public void PowerOff() { throw new System.NotImplementedException(); }


        // Displayable name:
        protected string name;
        protected string abrv;

        protected List<Weapon> weapons = new List<Weapon>();

        protected int selected;
        protected bool trigger;
        protected int ammo;

        protected Weapon.Orders orders;
        protected int control;
        protected int sweep;

        protected float mass;
        protected float resist;
    }
}