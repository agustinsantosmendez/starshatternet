﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Terrain.h/Terrain.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Test pseudo-random terrain heightfield, based on Solid
 */
using System;
using System.Collections.Generic;
using StarshatterNet.Stars.StarSystems;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars
{
    public struct Vec3B
    {
        public Vec3B(byte a, byte b, byte c) { x = a; y = b; z = c; }

        public byte x, y, z;
    }

#warning Terrain class is still in development and is not recommended for production.
    public class Terrain
    {
        public Terrain(TerrainRegion region) { throw new NotImplementedException(); }
        // public virtual ~Terrain();

        public virtual void Activate(Scene scene) { throw new NotImplementedException(); }
        public virtual void Deactivate(Scene scene) { throw new NotImplementedException(); }

        public virtual void SelectDetail(Projector proj) { throw new NotImplementedException(); }
        public virtual void BuildTerrain() { throw new NotImplementedException(); }
        public virtual void BuildNormals() { throw new NotImplementedException(); }

        public virtual void ExecFrame(double seconds) { throw new NotImplementedException(); }

        public double Height(double x, double y) { throw new NotImplementedException(); }

        public Vec3B[] Normals() { return terrain_normals; }
        public TerrainRegion GetRegion() { return region; }
        public double FogFade() { return fog_fade; }

        public Bitmap Texture() { return terrain_texture; }
        public Bitmap ApronTexture() { return apron_texture; }
        public Bitmap WaterTexture() { return water_texture; }
        public Bitmap[] EnvironmentTexture() { return env_texture; }
        public Bitmap TileTexture(int n) { return tiles[n]; }
        public Bitmap CloudTexture(int n) { return cloud_texture[n]; }
        public Bitmap ShadeTexture(int n) { return shade_texture[n]; }
        public Bitmap DetailTexture(int n) { return noise_texture[n]; }
        public Water GetWater(int level) { return water[level]; }
        public List<TerrainLayer> GetLayers() { return layers; }

        public bool IsFirstPatch(TerrainPatch p) { throw new NotImplementedException(); }

        public static int DetailLevel() { return detail_level; }
        public static void SetDetailLevel(int detail) { throw new NotImplementedException(); }


        protected TerrainRegion region;
        protected TerrainPatch[] patches;
        protected TerrainPatch[] water_patches;
        protected Water[] water;
        protected TerrainApron[] aprons;
        protected TerrainClouds[] clouds;
        protected int nclouds;

        protected Bitmap terrain_patch;
        protected Bitmap terrain_apron;
        protected Bitmap terrain_texture;
        protected Bitmap apron_texture;
        protected Bitmap water_texture;
        protected Bitmap[] env_texture = new Bitmap[6];
        protected Bitmap[] tiles = new Bitmap[256];
        protected Bitmap[] cloud_texture = new Bitmap[2];
        protected Bitmap[] shade_texture = new Bitmap[2];
        protected Bitmap[] noise_texture = new Bitmap[2];

        protected Vec3B[] terrain_normals;
        protected List<TerrainLayer> layers;

        protected string datapath;
        protected double scale;
        protected double mtnscale;
        protected int subdivisions;
        protected int patch_size;
        protected uint detail_frame;
        protected double fog_fade;

        protected static int detail_level;
    }
}
