﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Sky.h/Sky.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Celestial sphere, stars, planets, space dust...
*/
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using UnityEngine;
using UnityEngine.Assertions;

namespace StarshatterNet.Stars.Graphics.General
{
    public class Stars : Graphic
    {
        SkyNebula nebula;
        public Stars(int nstars)
        {
            name = "Stars";

            infinite = true;
            luminous = true;
            shadow = false;
            Assert.IsNotNull(gameObj, "Game Object is null!");
            Assert.IsNotNull(rectTransform, "RectTransform system missing from object!");

            gameObj.name = name;

            particles = gameObj.AddComponent<ParticleSystem>();
            Assert.IsNotNull(particles, "Particle system missing from object!");
            ParticleSystemRenderer renderer = gameObj.GetComponent<ParticleSystemRenderer>();
            Assert.IsNotNull(renderer, "Particle Renderer is missing from object!");

            ParticleSystem.MainModule mm = particles.main;
            ParticleSystem.EmissionModule em = particles.emission;
            ParticleSystem.ShapeModule sm = particles.shape;

            mm.playOnAwake = false;
            mm.loop = false;
            mm.maxParticles = nstars;
            em.enabled = false;
            sm.enabled = false;
            renderer.sharedMaterial = Resources.Load("Sky/Stars", typeof(UnityEngine.Material)) as UnityEngine.Material;

            stars = new ParticleSystem.Particle[nstars];

            for (int i = 0; i < nstars; i++)
            {
                // Randomize star size within parameters
                float randSize = Random.Range(starSize, starSize + 1f);

                //Vector3F pos = RandomHelper.RandomVector(sphereRadius);
                //stars[i].position = new Vector3(pos.X, pos.Y, pos.Z) + gameObj2.transform.position;
                stars[i].position = RandomHelper.GetRandomInSphere(sphereRadius) + gameObj.transform.position;
                stars[i].startSize = starSize * randSize * sphereRadius / 10;
                // If coloration is desired, get a random color. By default, Starshatter has true
                stars[i].startColor = colorize ? GetRandomColor(0.7f, 0.8f) : Color.white;
                stars[i].startLifetime = float.PositiveInfinity;
                stars[i].remainingLifetime = float.PositiveInfinity;
            }
            particles.SetParticles(stars, stars.Length); // Write data to the particle system
        }
        //virtual ~Stars();
        private Color GetRandomColor(float min, float max)
        {
            Color color = new Color(Random.Range(min, max), Random.Range(min, max), Random.Range(min, max), 1f);

            return color;
        }

        public virtual void Illuminate(double scale) { }
        public virtual void Render() { }


        protected ParticleSystem particles;
        protected ParticleSystem.Particle[] stars;

        public float starSize = 0.1f;
        public float starSizeRange = 0.5f;
        public float sphereRadius = 990f; // Far plane is 1000f by default
        public bool colorize = false;
    }
}
