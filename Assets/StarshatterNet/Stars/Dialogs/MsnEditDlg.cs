﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MsnEditDlg.h/MsnEditDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Mission Editor Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.StarSystems;
using UnityEngine;
using DWORD = System.Int32;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MsnEditDlg : FormWindow
    {
        public MsnEditDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
                base(s, 0, 0, s.Width(), s.Height(), c, p, "MsnEditDlg")
        {
            manager = mgr;
            btn_accept = null; btn_cancel = null;
            btn_elem_add = null; btn_elem_edit = null; btn_elem_del = null; btn_elem_inc = null; btn_elem_dec = null;
            btn_event_add = null; btn_event_edit = null; btn_event_del = null; btn_event_inc = null; btn_event_dec = null;
            btn_sit = null; btn_pkg = null; btn_map = null;
            txt_name = null; cmb_type = null; cmb_system = null; cmb_region = null;
            txt_description = null; txt_situation = null; txt_objective = null;
            lst_elem = null; lst_event = null; mission = null; mission_info = null; current_tab = 0;
            exit_latch = true;

            Init(def);
        }
        //public virtual ~MsnEditDlg();

        public override void RegisterControls()
        {
            btn_accept = (Button)FindControl(1);
            btn_cancel = (Button)FindControl(2);
            btn_sit = (Button)FindControl(301);
            btn_pkg = (Button)FindControl(302);
            btn_map = (Button)FindControl(303);

            btn_elem_add = (Button)FindControl(501);
            btn_elem_edit = (Button)FindControl(505);
            btn_elem_del = (Button)FindControl(502);
            btn_elem_inc = (Button)FindControl(503);
            btn_elem_dec = (Button)FindControl(504);

            btn_event_add = (Button)FindControl(511);
            btn_event_edit = (Button)FindControl(515);
            btn_event_del = (Button)FindControl(512);
            btn_event_inc = (Button)FindControl(513);
            btn_event_dec = (Button)FindControl(514);

            txt_name = (EditBox)FindControl(201);
            cmb_type = (ComboBox)FindControl(202);
            cmb_system = (ComboBox)FindControl(203);
            cmb_region = (ComboBox)FindControl(204);

            txt_description = (EditBox)FindControl(410);
            txt_situation = (EditBox)FindControl(411);
            txt_objective = (EditBox)FindControl(412);

            lst_elem = (ListBox)FindControl(510);
            lst_event = (ListBox)FindControl(520);

            if (btn_accept != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_accept, OnAccept);

            if (btn_cancel != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_cancel, OnCancel);

            if (btn_elem_add != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_elem_add, OnElemAdd);

            if (btn_elem_edit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_elem_edit, OnElemEdit);

            if (btn_elem_del != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_elem_del, OnElemDel);

            if (btn_elem_inc != null)
            {
                string up_arrow = char.ConvertFromUtf32(0x2191); // Font.ARROW_UP;
                btn_elem_inc.SetText(up_arrow);
                btn_elem_inc.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_elem_inc, OnElemInc);
            }

            if (btn_elem_dec != null)
            {
                string dn_arrow = char.ConvertFromUtf32(0x2193);  //Font.ARROW_DOWN;
                btn_elem_dec.SetText(dn_arrow);
                btn_elem_dec.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_elem_dec, OnElemDec);
            }

            if (btn_event_add != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_event_add, OnEventAdd);

            if (btn_event_edit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_event_edit, OnEventEdit);

            if (btn_event_del != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_event_del, OnEventDel);

            if (btn_event_inc != null)
            {
                string up_arrow = char.ConvertFromUtf32(0x2191); // Font.ARROW_UP;
                btn_event_inc.SetText(up_arrow);
                btn_event_inc.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_event_inc, OnEventInc);
            }

            if (btn_event_dec != null)
            {
                string dn_arrow = char.ConvertFromUtf32(0x2193);  //Font.ARROW_DOWN;
                btn_event_dec.SetText(dn_arrow);
                btn_event_dec.SetEnabled(false);
                REGISTER_CLIENT(ETD.EID_CLICK, btn_event_dec, OnEventDec);
            }

            if (btn_sit != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_sit, OnTabButton);

            if (btn_pkg != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_pkg, OnTabButton);

            if (btn_map != null)
                REGISTER_CLIENT(ETD.EID_CLICK, btn_map, OnTabButton);

            if (cmb_system != null)
                REGISTER_CLIENT(ETD.EID_SELECT, cmb_system, OnSystemSelect);

            if (lst_elem != null)
                REGISTER_CLIENT(ETD.EID_SELECT, lst_elem, OnElemSelect);

            if (lst_event != null)
                REGISTER_CLIENT(ETD.EID_SELECT, lst_event, OnEventSelect);
        }
        public override void Show()
        {
            base.Show();

            if (txt_name == null || cmb_type == null) return;

            txt_name.SetText("");

            if (cmb_system != null)
            {
                cmb_system.ClearItems();

                Galaxy galaxy = Galaxy.GetInstance();
                foreach (StarSystem iter in galaxy.GetSystemList())
                {
                    cmb_system.AddItem(iter.Name());
                }
            }

            if (mission != null)
            {
                int i;

                txt_name.SetText(mission.Name());
                cmb_type.SetSelection((int)mission.Type());

                StarSystem sys = mission.GetStarSystem();
                if (sys != null && cmb_system != null && cmb_region != null)
                {
                    for (i = 0; i < cmb_system.NumItems(); i++)
                    {
                        if (cmb_system.GetItem(i) == sys.Name())
                        {
                            cmb_system.SetSelection(i);
                            break;
                        }
                    }

                    cmb_region.ClearItems();
                    int sel_rgn = 0;

                    List<OrbitalRegion> regions = new List<OrbitalRegion>();
                    regions.AddRange(sys.AllRegions());
                    regions.Sort();

                    i = 0;
                    foreach (OrbitalRegion region in regions)
                    {
                        cmb_region.AddItem(region.Name());

                        if (mission.GetRegion() == region.Name())
                        {
                            sel_rgn = i;
                        }

                        i++;
                    }

                    cmb_region.SetSelection(sel_rgn);
                }

                if (txt_description != null && mission_info != null)
                    txt_description.SetText(mission_info.description);

                if (txt_situation != null)
                    txt_situation.SetText(mission.Situation());

                if (txt_objective != null)
                    txt_objective.SetText(mission.Objective());

                DrawPackages();
            }

            ShowTab(current_tab);

            exit_latch = true;
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                if (!exit_latch && btn_accept != null && btn_accept.IsEnabled())
                    OnAccept(null, null);
            }

            else if (Keyboard.KeyDown(KeyMap.VK_ESCAPE))
            {
                if (!exit_latch)
                    OnCancel(null, null);
            }

            else
            {
                exit_latch = false;
            }
        }

        public virtual void ScrapeForm()
        {
            if (mission != null)
            {
                if (txt_name != null)
                {
                    mission.SetName(txt_name.GetText());
                }

                if (cmb_type != null)
                {
                    mission.SetType((Mission.TYPE)cmb_type.GetSelectedIndex());

                    if (mission_info != null)
                        mission_info.type = (Mission.TYPE)cmb_type.GetSelectedIndex();
                }

                Galaxy galaxy = Galaxy.GetInstance();
                StarSystem system = null;

                if (galaxy != null && cmb_system != null)
                    system = galaxy.GetSystem(cmb_system.GetSelectedItem());

                if (system != null)
                {
                    mission.ClearSystemList();
                    mission.SetStarSystem(system);

                    if (mission_info != null)
                        mission_info.system = system.Name();
                }

                if (cmb_region != null)
                {
                    mission.SetRegion(cmb_region.GetSelectedItem());

                    if (mission_info != null)
                        mission_info.region = cmb_region.GetSelectedItem();
                }

                if (txt_description != null && mission_info != null)
                {
                    mission_info.description = txt_description.GetText();
                    mission.SetDescription(txt_description.GetText());
                }

                if (txt_situation != null)
                    mission.SetSituation(txt_situation.GetText());

                if (txt_objective != null)
                    mission.SetObjective(txt_objective.GetText());
            }
        }

        // Operations:
        public virtual void OnAccept(ActiveWindow obj, AWEvent evnt)
        {
            if (mission != null)
            {
                ScrapeForm();
                mission_info.name = mission.Name();
                mission.Save();
            }

            manager.ShowMsnSelectDlg();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            if (mission != null)
                mission.Load();

            manager.ShowMsnSelectDlg();
        }
        public virtual void OnTabButton(ActiveWindow obj, AWEvent evnt)
        {
            if (evnt == null) return;

            if (evnt.window == btn_sit)
                ShowTab(0);

            else if (evnt.window == btn_pkg)
                ShowTab(1);

            else if (evnt.window == btn_map)
                ShowTab(2);
        }
        public virtual void OnSystemSelect(ActiveWindow obj, AWEvent evnt)
        {
            StarSystem sys = null;

            if (cmb_system != null)
            {
                string name = cmb_system.GetSelectedItem();

                Galaxy galaxy = Galaxy.GetInstance();
                foreach (StarSystem s in galaxy.GetSystemList())
                {

                    if (s.Name()== name)
                    {
                        sys = s;
                        break;
                    }
                }
            }

            if (sys != null && cmb_region != null)
            {
                cmb_region.ClearItems();

                List<OrbitalRegion> regions = new List<OrbitalRegion>();
                regions.AddRange(sys.AllRegions());
                regions.Sort();

                foreach (OrbitalRegion region in regions)
                {
                    cmb_region.AddItem(region.Name());
                }
            }

            ScrapeForm();

            NavDlg navdlg = manager.GetNavDlg();

            if (navdlg != null)
                navdlg.SetMission(mission);
        }
        public virtual void OnElemAdd(ActiveWindow obj, AWEvent evnt)
        {
            if (lst_elem != null && mission != null)
            {
                List<MissionElement> elements = mission.GetElements();
                MissionElement elem = new MissionElement();

                if (elements.Count > 0)
                    elem.SetLocation(RandomHelper.RandomPoint());

                if (cmb_region != null)
                    elem.SetRegion(cmb_region.GetSelectedItem());

                elements.Add(elem);
                DrawPackages();

                MsnElemDlg msn_elem_dlg = manager.GetMsnElemDlg();

                if (msn_elem_dlg != null)
                {
                    ScrapeForm();
                    msn_elem_dlg.SetMission(mission);
                    msn_elem_dlg.SetMissionElement(elem);
                    manager.ShowMsnElemDlg();
                }
            }
        }
        public virtual void OnElemEdit(ActiveWindow obj, AWEvent evnt)
        {
            if (lst_elem != null && mission != null && lst_elem.GetSelCount() == 1)
            {
                int index = lst_elem.GetSelection();
                MissionElement elem = mission.GetElements()[index];
                MsnElemDlg msn_elem_dlg = manager.GetMsnElemDlg();

                if (elem != null && msn_elem_dlg != null)
                {
                    ScrapeForm();
                    msn_elem_dlg.SetMission(mission);
                    msn_elem_dlg.SetMissionElement(elem);
                    manager.ShowMsnElemDlg();
                }
            }
        }
        public virtual void OnElemDel(ActiveWindow obj, AWEvent evnt)
        {
            if (lst_elem != null && mission != null)
            {
                List<MissionElement> elements = mission.GetElements();
                //delete elements.removeIndex(lst_elem.GetSelection());
                DrawPackages();
            }
        }

        static ulong click_time = 0;
        public virtual void OnElemSelect(ActiveWindow obj, AWEvent evnt)
        {

            if (lst_elem != null && mission != null)
            {
                int selection = lst_elem.GetSelection();

                if (btn_elem_edit != null)
                    btn_elem_edit.SetEnabled(selection >= 0);

                if (btn_elem_del != null)
                    btn_elem_del.SetEnabled(selection >= 0);

                if (btn_elem_inc != null)
                    btn_elem_inc.SetEnabled(selection > 0);

                if (btn_elem_dec != null)
                    btn_elem_dec.SetEnabled(selection >= 0 && selection < lst_elem.NumItems() - 1);

                // double-click:
                if (Game.RealTime() - click_time < 350)
                {
                    if (lst_elem.GetSelCount() == 1)
                    {
                        int index = lst_elem.GetSelection();
                        MissionElement elem = mission.GetElements()[index];
                        MsnElemDlg msn_elem_dlg = manager.GetMsnElemDlg();

                        if (elem != null && msn_elem_dlg != null)
                        {
                            ScrapeForm();
                            msn_elem_dlg.SetMission(mission);
                            msn_elem_dlg.SetMissionElement(elem);
                            manager.ShowMsnElemDlg();
                        }
                    }
                }
            }

            click_time = Game.RealTime();
        }
        public virtual void OnElemInc(ActiveWindow obj, AWEvent evnt)
        {
            int index = lst_elem.GetSelection();
            mission.IncreaseElemPriority(index--);

            DrawPackages();
            lst_elem.SetSelected(index);
            btn_elem_edit.SetEnabled(true);
            btn_elem_del.SetEnabled(true);
            btn_elem_inc.SetEnabled(index > 0);
            btn_elem_dec.SetEnabled(index >= 0 && index < lst_elem.NumItems() - 1);
        }

        public virtual void OnElemDec(ActiveWindow obj, AWEvent evnt)
        {
            int index = lst_elem.GetSelection();
            mission.DecreaseElemPriority(index++);

            DrawPackages();
            lst_elem.SetSelected(index);
            btn_elem_edit.SetEnabled(true);
            btn_elem_del.SetEnabled(true);
            btn_elem_inc.SetEnabled(index > 0);
            btn_elem_dec.SetEnabled(index >= 0 && index < lst_elem.NumItems() - 1);
        }

        public virtual void OnEventAdd(ActiveWindow obj, AWEvent awevnt)
        {
            if (lst_event != null && mission != null)
            {
                List<MissionEvent> events = mission.GetEvents();
                MissionEvent evnt = new MissionEvent();

                int id = 1;
                for (int i = 0; i < events.Count; i++)
                {
                    MissionEvent e = events[i];
                    if (e.EventID() >= id)
                        id = e.EventID() + 1;
                }

                evnt.id = id;

                events.Add(evnt);
                DrawPackages();

                MsnEventDlg msn_event_dlg = manager.GetMsnEventDlg();

                if (msn_event_dlg != null)
                {
                    ScrapeForm();
                    msn_event_dlg.SetMission(mission);
                    msn_event_dlg.SetMissionEvent(evnt);
                    manager.ShowMsnEventDlg();
                }
            }
        }
        public virtual void OnEventEdit(ActiveWindow obj, AWEvent evnt) { throw new NotImplementedException(); }
        public virtual void OnEventDel(ActiveWindow obj, AWEvent evnt) { throw new NotImplementedException(); }

        static ulong click_time2 = 0;
        public virtual void OnEventSelect(ActiveWindow obj, AWEvent awevnt)
        {
            if (lst_event != null && mission != null)
            {
                int selection = lst_event.GetSelection();

                if (btn_event_edit != null)
                    btn_event_edit.SetEnabled(selection >= 0);

                if (btn_event_del != null)
                    btn_event_del.SetEnabled(selection >= 0);

                if (btn_event_inc != null)
                    btn_event_inc.SetEnabled(selection > 0);

                if (btn_event_dec != null)
                    btn_event_dec.SetEnabled(selection >= 0 && selection < lst_event.NumItems() - 1);

                // double-click:
                if (Game.RealTime() - click_time2 < 350)
                {
                    if (lst_event.GetSelCount() == 1)
                    {
                        int index = lst_event.GetSelection();
                        MissionEvent evnt = mission.GetEvents()[index];
                        MsnEventDlg msn_event_dlg = manager.GetMsnEventDlg();

                        if (evnt != null && msn_event_dlg != null)
                        {
                            ScrapeForm();
                            msn_event_dlg.SetMission(mission);
                            msn_event_dlg.SetMissionEvent(evnt);
                            manager.ShowMsnEventDlg();
                        }
                    }
                }
            }

            click_time2 = Game.RealTime();
        }
        public virtual void OnEventInc(ActiveWindow obj, AWEvent evnt)
        {
            int index = lst_event.GetSelection();
            mission.IncreaseEventPriority(index--);

            DrawPackages();
            lst_event.SetSelected(index);
            btn_event_edit.SetEnabled(true);
            btn_event_del.SetEnabled(true);
            btn_event_inc.SetEnabled(index > 0);
            btn_event_dec.SetEnabled(index >= 0 && index < lst_event.NumItems() - 1);
        }
        public virtual void OnEventDec(ActiveWindow obj, AWEvent evnt)
        {
            int index = lst_event.GetSelection();
            mission.DecreaseEventPriority(index++);

            DrawPackages();
            lst_event.SetSelected(index);
            btn_event_edit.SetEnabled(true);
            btn_event_del.SetEnabled(true);
            btn_event_inc.SetEnabled(index > 0);
            btn_event_dec.SetEnabled(index >= 0 && index < lst_event.NumItems() - 1);
        }

        public virtual Mission GetMission() { return mission; }
        public virtual void SetMission(Mission m)
        {
            mission = m;
            current_tab = 0;
        }

        public virtual void SetMissionInfo(MissionInfo m)
        {
            mission_info = m;
            current_tab = 0;
        }

        protected virtual void DrawPackages()
        {
            bool elem_del = false;
            bool elem_edit = false;
            bool elem_inc = false;
            bool elem_dec = false;

            bool event_del = false;
            bool event_edit = false;
            bool event_inc = false;
            bool event_dec = false;

            if (mission != null)
            {
                if (lst_elem != null)
                {
                    bool cleared = false;
                    int index = lst_elem.GetSelection();

                    if (lst_elem.NumItems() != mission.GetElements().Count)
                    {
                        if (lst_elem.NumItems() < mission.GetElements().Count)
                            index = lst_elem.NumItems();

                        lst_elem.ClearItems();
                        cleared = true;
                    }

                    int i = 0;
                    foreach (MissionElement elem in mission.GetElements())
                    {
                        string txt = string.Format("{0}", elem.Identity());

                        if (cleared)
                        {
                            lst_elem.AddItemWithData(txt, elem.ElementID());
                        }
                        else
                        {
                            lst_elem.SetItemText(i, txt);
                            lst_elem.SetItemData(i, elem.ElementID());
                        }

                        txt = string.Format("{0}", elem.GetIFF());
                        lst_elem.SetItemText(i, 1, txt);
                        lst_elem.SetItemText(i, 2, elem.Name());
                        lst_elem.SetItemText(i, 4, elem.RoleName());
                        lst_elem.SetItemText(i, 5, elem.Region());

                        ShipDesign design = elem.GetDesign();

                        if (design != null)
                        {
                            if (elem.Count() > 1)
                                txt = string.Format("{0} {1}", elem.Count(), design.abrv);
                            else
                                txt = string.Format("{0} {1}", design.abrv, design.name);
                        }
                        else
                        {
                            txt = string.Format("{0}", Game.GetText("MsnDlg.undefined"));
                        }

                        lst_elem.SetItemText(i, 3, txt);

                        i++;
                    }

                    int nitems = lst_elem.NumItems();
                    if (nitems != 0)
                    {
                        if (index >= nitems)
                            index = nitems - 1;

                        if (index >= 0)
                        {
                            lst_elem.SetSelected(index);
                            elem_del = true;
                            elem_edit = true;
                            elem_inc = index > 0;
                            elem_dec = index < nitems - 1;
                        }
                    }
                }

                if (lst_event != null)
                {
                    bool cleared = false;
                    int index = lst_event.GetSelection();

                    if (lst_event.NumItems() != mission.GetEvents().Count)
                    {
                        if (lst_event.NumItems() < mission.GetEvents().Count)
                            index = lst_event.NumItems();

                        lst_event.ClearItems();
                        cleared = true;
                    }

                    int i = 0;
                    foreach (MissionEvent evnt in mission.GetEvents())
                    {
                        string txt;

                        txt = string.Format("{0}", evnt.EventID());
                        if (cleared)
                        {
                            lst_event.AddItemWithData(txt, evnt.EventID());
                        }
                        else
                        {
                            lst_event.SetItemText(i, txt);
                            lst_event.SetItemData(i, evnt.EventID());
                        }

                        if (evnt.Time() != 0)
                        {
                            FormatUtil.FormatTime(out txt, evnt.Time());
                        }
                        else if (evnt.Delay() != 0)
                        {
                            FormatUtil.FormatTime(out txt, evnt.Delay());
                            txt  = "+ " + txt;
                        }
                        else
                        {
                            txt = " ";
                        }

                        lst_event.SetItemText(i, 1, txt);
                        lst_event.SetItemText(i, 2, evnt.EventName());
                        lst_event.SetItemText(i, 3, evnt.EventShip());
                        lst_event.SetItemText(i, 4, evnt.EventMessage());

                        i++;
                    }

                    int nitems = lst_event.NumItems();
                    if (nitems != 0)
                    {
                        if (index >= nitems)
                            index = nitems - 1;

                        if (index >= 0)
                        {
                            lst_event.SetSelected(index);
                            event_del = true;
                            event_edit = true;
                            event_inc = index > 0;
                            event_dec = index < nitems - 1;
                        }
                    }
                }
            }

            if (btn_elem_del != null) btn_elem_del.SetEnabled(elem_del);
            if (btn_elem_edit != null) btn_elem_edit.SetEnabled(elem_edit);
            if (btn_elem_inc != null) btn_elem_inc.SetEnabled(elem_inc);
            if (btn_elem_dec != null) btn_elem_dec.SetEnabled(elem_del);

            if (btn_event_del != null) btn_event_del.SetEnabled(event_del);
            if (btn_event_edit != null) btn_event_edit.SetEnabled(event_edit);
            if (btn_event_inc != null) btn_event_inc.SetEnabled(event_inc);
            if (btn_event_dec != null) btn_event_dec.SetEnabled(event_dec);
        }
        public virtual void ShowTab(int tab)
        {
            current_tab = tab;

            if (current_tab < 0 || current_tab > 2)
                current_tab = 0;

            if (btn_sit != null) btn_sit.SetButtonState(tab == 0 ? (short)1 : (short)0);
            if (btn_pkg != null) btn_pkg.SetButtonState(tab == 1 ? (short)1 : (short)0);
            if (btn_map != null) btn_map.SetButtonState(tab == 2 ? (short)1 : (short)0);

            DWORD low = 400 + tab * 100;
            DWORD high = low + 100;

            foreach (ActiveWindow a in Controls())
            {
                if (a.GetID() < 400)
                    a.Show();

                else if (a.GetID() >= low && a.GetID() < high)
                    a.Show();

                else
                    a.Hide();
            }

            if (tab == 2)
            {
                NavDlg navdlg = manager.GetNavDlg();

                if (navdlg != null)
                {
                    navdlg.SetMission(mission);
                    navdlg.SetEditorMode(true);
                }

                manager.ShowNavDlg();
            }
            else
            {
                manager.HideNavDlg();
            }
        }

        protected MenuScreen manager;

        protected Button btn_accept;
        protected Button btn_cancel;

        protected Button btn_elem_add;
        protected Button btn_elem_edit;
        protected Button btn_elem_del;
        protected Button btn_elem_inc;
        protected Button btn_elem_dec;

        protected Button btn_event_add;
        protected Button btn_event_edit;
        protected Button btn_event_del;
        protected Button btn_event_inc;
        protected Button btn_event_dec;

        protected Button btn_sit;
        protected Button btn_pkg;
        protected Button btn_map;

        protected EditBox txt_name;
        protected ComboBox cmb_type;
        protected ComboBox cmb_system;
        protected ComboBox cmb_region;

        protected EditBox txt_description;
        protected EditBox txt_situation;
        protected EditBox txt_objective;

        protected ListBox lst_elem;
        protected ListBox lst_event;

        protected Mission mission;
        protected MissionInfo mission_info;

        protected int current_tab;
        protected bool exit_latch;
    }
}