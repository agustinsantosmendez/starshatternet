﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      FormDef.h/FormDef.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Form and Control Definition Resources
*/
using System;
using System.Collections.Generic;
using System.Linq;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars;
using StarshatterNet.Stars.Views;
using UnityEngine;
using DWORD = System.Int32;
using WORD = System.UInt16;

namespace StarshatterNet.Gen.Forms
{
    public enum WinType
    {
        WIN_DEF_FORM,
        WIN_DEF_LABEL,
        WIN_DEF_BUTTON,
        WIN_DEF_COMBO,
        WIN_DEF_EDIT,
        WIN_DEF_IMAGE,
        WIN_DEF_SLIDER,
        WIN_DEF_LIST,
        WIN_DEF_RICH,
        WIN_DEF_BACKGROUND
    }

    // +--------------------------------------------------------------------+

    public class ColumnDef
    {
        public ColumnDef()
        {
            width = 10; align = 0; sort = 0; color = Color.white; use_color = false;
        }
        public ColumnDef(string t, int w, TextFormat a, int s)
        {
            title = t; width = w; align = a; sort = s;
            color = Color.white; use_color = false;
        }

        public string title;
        public int width;
        public TextFormat align;
        public int sort;
        public Color color;
        public bool use_color;
    }

    // +--------------------------------------------------------------------+

    public class LayoutDef
    {
        public List<DWORD> x_mins = new List<DWORD>();
        public List<DWORD> y_mins = new List<DWORD>();
        public List<float> x_weights = new List<float>();
        public List<float> y_weights = new List<float>();

        public bool IsEmpty()
        {
            return x_mins.Count == 0 && y_mins.Count == 0 && x_weights.Count == 0 && y_weights.Count == 0;
        }

    }

    // +--------------------------------------------------------------------+

    public class WinDef
    {
        public WinDef(DWORD a_id, WinType a_type, string a_text = null, DWORD a_style = 0)
        {
            id = a_id; pid = 0; type = a_type; text = a_text; style = a_style;
            rect = new Stars.Rect(0, 0, 0, 0);
            text_align = 0;
            single_line = false;
            enabled = true;
            transparent = false;
            hide_partial = true;
            back_color = Color.gray;
            base_color = Color.gray;
            fore_color = Color.black;
            fixed_width = 0;
            fixed_height = 0;
        }
        //public virtual ~WinDef() { }

        // int operator ==(WinDef w) { return id == w.id; }

        public DWORD GetID() { return id; }
        public void SetID(DWORD i) { id = i; }
        public DWORD GetParentID() { return pid; }
        public void SetParentID(DWORD id) { pid = id; }
        public WinType GetWinType() { return type; }
        public void SetType(WinType t) { type = t; }

        public void SetRect(Stars.Rect r) { rect = r; }
        public Stars.Rect GetRect() { return rect; }
        public int GetX() { return rect.x; }
        public int GetY() { return rect.y; }
        public int GetW() { return rect.w; }
        public int GetH() { return rect.h; }

        public void SetEnabled(bool e = true) { enabled = e; }
        public bool IsEnabled() { return enabled; }

        public void SetStyle(DWORD s) { style = s; }
        public DWORD GetStyle() { return style; }

        public void SetFont(string s) { font = s; }
        public string GetFont() { return font; }
        public void SetText(string t) { text = t; }
        public string GetText() { return text; }
        public void SetAltText(string t) { alt_text = t; }
        public string GetAltText() { return alt_text; }
        public void SetTexture(string t) { texture = t; }
        public string GetTexture() { return texture; }

        public void SetBackColor(Color c) { back_color = c; }
        public Color GetBackColor() { return back_color; }
        public void SetBaseColor(Color c) { base_color = c; }
        public Color GetBaseColor() { return base_color; }
        public void SetForeColor(Color c) { fore_color = c; }
        public Color GetForeColor() { return fore_color; }
        public void SetSingleLine(bool a) { single_line = a; }
        public bool GetSingleLine() { return single_line; }
        public void SetTextAlign(TextFormat a) { text_align = a; }
        public TextFormat GetTextAlign() { return text_align; }
        public void SetTransparent(bool t) { transparent = t; }
        public bool GetTransparent() { return transparent; }
        public void SetHidePartial(bool a) { hide_partial = a; }
        public bool GetHidePartial() { return hide_partial; }

        public void SetMargins(Insets m) { margins = m; }
        public Insets GetMargins() { return margins; }
        public void SetTextInsets(Insets t) { text_insets = t; }
        public Insets GetTextInsets() { return text_insets; }
        public void SetCellInsets(Insets t) { cell_insets = t; }
        public Insets GetCellInsets() { return cell_insets; }
        public void SetCells(Stars.Rect r) { cells = r; }
        public Stars.Rect GetCells() { return cells; }

        public void SetFixedWidth(int w) { fixed_width = w; }
        public int GetFixedWidth() { return fixed_width; }
        public void SetFixedHeight(int h) { fixed_height = h; }
        public int GetFixedHeight() { return fixed_height; }

        public LayoutDef GetLayout() { return layout; }


        protected DWORD id;
        protected DWORD pid;
        protected WinType type;
        protected Stars.Rect rect;
        protected string font;
        protected string text;
        protected string alt_text;
        protected string texture;
        protected string picture;
        protected DWORD style;
        protected TextFormat text_align;
        protected bool single_line;
        protected bool enabled;
        protected bool transparent;
        protected bool hide_partial;
        protected Color back_color;
        protected Color base_color;
        protected Color fore_color;

        protected internal Insets margins;
        protected internal Insets text_insets;
        protected internal Insets cell_insets;
        protected internal Stars.Rect cells;
        protected internal int fixed_width;
        protected internal int fixed_height;

        protected internal LayoutDef layout;
    }

    // +--------------------------------------------------------------------+

    public class CtrlDef : WinDef
    {
        [Flags]
        public enum CTRL_DEF
        {
            CTRL_DEF_ANIMATED = 0x0001,
            CTRL_DEF_BORDER = 0x0002,
            CTRL_DEF_DROP_SHADOW = 0x0004,
            CTRL_DEF_INDENT = 0x0008,
            CTRL_DEF_INVERT_LABEL = 0x0010,
            CTRL_DEF_GLOW = 0x0020,
            CTRL_DEF_SIMPLE = 0x0040,
            CTRL_DEF_STICKY = 0x0080
        }

        public CtrlDef(DWORD a_id = 0, WinType a_type = WinType.WIN_DEF_FORM, string a_text = null, DWORD a_style = 0) : base(a_id, a_type, a_text, a_style)
        {
            ctrl_flags = CTRL_DEF.CTRL_DEF_ANIMATED | CTRL_DEF.CTRL_DEF_BORDER | CTRL_DEF.CTRL_DEF_INDENT;
            bevel_width = 5;
            picture_loc = 1; // North
            picture_type = Bitmap.BMP_TYPES.BMP_SOLID;

            active = false;
            show_headings = false;

            leading = 0;
            line_height = 0;
            multiselect = false;
            dragdrop = false;
            orientation = 0;
            scroll_bar = 1;
            num_leds = 1;

            smooth_scroll = false;

            item_style = 0;
            selected_style = 0;
            pass_char = (char)0;

            items.Clear();

            //ZeroMemory(tabs, sizeof(tabs));
            tabs.Initialize();
            ntabs = 0;
        }
        // public virtual ~CtrlDef();

        //public virtual CtrlDef operator=(CtrlDef ctrl);

        public bool GetActive()
        {
            return active;
        }
        public void SetActive(bool c)
        {
            active = c;
        }
        public Color GetActiveColor()
        {
            return active_color;
        }
        public void SetActiveColor(Color c)
        {
            active_color = c;
        }
        public bool GetAnimated()
        {
            return ctrl_flags.HasFlag(CTRL_DEF.CTRL_DEF_ANIMATED);
        }

        public void SetAnimated(bool bNewValue)
        {
            if (bNewValue)
                ctrl_flags |= CTRL_DEF.CTRL_DEF_ANIMATED;
            else
                ctrl_flags &= ~CTRL_DEF.CTRL_DEF_ANIMATED;
        }
        public short GetBevelWidth()
        {
            return bevel_width;
        }
        public void SetBevelWidth(short nNewValue)
        {
            bevel_width = nNewValue;
        }
        public bool GetBorder()
        {
            return ctrl_flags.HasFlag(CTRL_DEF.CTRL_DEF_BORDER);
        }
        public void SetBorder(bool bNewValue)
        {
            if (bNewValue)
                ctrl_flags |= CTRL_DEF.CTRL_DEF_BORDER;
            else
                ctrl_flags &= ~CTRL_DEF.CTRL_DEF_BORDER;
        }
        public Color GetBorderColor()
        {
            return border_color;
        }
        public void SetBorderColor(Color c)
        {
            border_color = c;
        }
        public bool GetDropShadow()
        {
            return ctrl_flags.HasFlag(CTRL_DEF.CTRL_DEF_DROP_SHADOW);
        }
        public void SetDropShadow(bool bNewValue)
        {
            if (bNewValue)
                ctrl_flags |= CTRL_DEF.CTRL_DEF_DROP_SHADOW;
            else
                ctrl_flags &= ~CTRL_DEF.CTRL_DEF_DROP_SHADOW;
        }
        public bool GetIndent()
        {
            return ctrl_flags.HasFlag(CTRL_DEF.CTRL_DEF_INDENT);
        }
        public void SetIndent(bool bNewValue)
        {
            if (bNewValue)
                ctrl_flags |= CTRL_DEF.CTRL_DEF_INDENT;
            else
                ctrl_flags &= ~CTRL_DEF.CTRL_DEF_INDENT;
        }
        public bool GetInvertLabel()
        {
            return ctrl_flags.HasFlag(CTRL_DEF.CTRL_DEF_INVERT_LABEL);
        }
        public void SetInvertLabel(bool bNewValue)
        {
            if (bNewValue)
                ctrl_flags |= CTRL_DEF.CTRL_DEF_INVERT_LABEL;
            else
                ctrl_flags &= ~CTRL_DEF.CTRL_DEF_INVERT_LABEL;
        }
        public int GetOrientation()
        {
            return orientation;
        }
        public void SetOrientation(int o)
        {
            orientation = o;
        }
        public string GetPicture()
        {
            return picture;
        }
        public void SetPicture(string img_name)
        {
            picture = img_name;
        }
        public short GetPictureLocation()
        {
            return picture_loc;
        }
        public void SetPictureLocation(short nNewValue)
        {
            picture_loc = nNewValue;
        }
        public Bitmap.BMP_TYPES GetPictureType()
        {
            return picture_type;
        }
        public void SetPictureType(Bitmap.BMP_TYPES nNewValue)
        {
            picture_type = nNewValue;
        }
        public bool GetSticky()
        {
            return ctrl_flags.HasFlag(CTRL_DEF.CTRL_DEF_STICKY);
        }
        public void SetSticky(bool bNewValue)
        {
            if (bNewValue)
                ctrl_flags |= CTRL_DEF.CTRL_DEF_STICKY;
            else
                ctrl_flags &= ~CTRL_DEF.CTRL_DEF_STICKY;
        }
        public int GetNumLeds()
        {
            return num_leds;
        }
        public void SetNumLeds(int nNewValue)
        {
            if (nNewValue > 0)
                num_leds = nNewValue;
        }

        public int NumItems()
        {
            return items.Count;
        }
        public string GetItem(int i)
        {
            string result = null;

            if (i >= 0 && i < items.Count)
                result = items[i];

            return result;
        }
        public void AddItem(string t)
        {
            items.Add(t);
        }
        public void AddItems(List<string> ts)
        {
            foreach (var t in ts)
                items.Add(Game.GetText(t));
        }
        public int NumColumns()
        {
            return columns.Count;
        }
        public ColumnDef GetColumn(int i)
        {
            ColumnDef result = null;

            if (i >= 0 && i < columns.Count)
                result = columns[i];

            return result;
        }
        public void AddColumn(string t, int w, TextFormat a, int s)
        {
            columns.Add(new ColumnDef(t, w, a, s));
        }

        public int NumTabs()
        {
            return ntabs;
        }
        public int GetTab(int i)
        {
            if (i >= 0 && i < ntabs)
                return tabs[i];
            return 0;
        }
        public void SetTab(int i, int t)
        {
            if (i >= 0 && i < 10)
            {
                tabs[i] = t;
                if (i >= ntabs)
                    ntabs = i + 1;
            }
        }
        public void AddTab(int i)
        {
            if (ntabs < 10)
                tabs[ntabs++] = i;
        }

        public bool GetShowHeadings()
        {
            return show_headings;
        }
        public void SetShowHeadings(bool bNewValue)
        {
            show_headings = bNewValue;
        }
        public int GetLeading()
        {
            return leading;
        }
        public void SetLeading(int nNewValue)
        {
            leading = nNewValue;
        }
        public int GetLineHeight()
        {
            return line_height;
        }
        public void SetLineHeight(int nNewValue)
        {
            line_height = nNewValue;
        }
        public bool GetMultiSelect()
        {
            return multiselect;
        }
        public void SetMultiSelect(bool nNewValue)
        {
            multiselect = nNewValue;
        }
        public bool GetDragDrop()
        {
            return dragdrop;
        }
        public void SetDragDrop(bool nNewValue)
        {
            dragdrop = nNewValue;
        }
        public int GetScrollBarVisible()
        {
            return scroll_bar;
        }
        public void SetScrollBarVisible(int nNewValue)
        {
            scroll_bar = nNewValue;
        }
        public bool GetSmoothScroll()
        {
            return smooth_scroll;
        }
        public void SetSmoothScroll(bool bNewValue)
        {
            smooth_scroll = bNewValue;
        }

        public int GetItemStyle()
        {
            return item_style;
        }
        public void SetItemStyle(short nNewValue)
        {
            item_style = nNewValue;
        }
        public short GetSelectedStyle()
        {
            return selected_style;
        }
        public void SetSelectedStyle(short nNewValue)
        {
            selected_style = nNewValue;
        }

        public char GetPasswordChar()
        {
            return pass_char;
        }
        public void SetPasswordChar(char nNewValue)
        {
            pass_char = nNewValue;
        }

        public string GetStandardImage()
        {
            return standard_image;
        }
        public void SetStandardImage(string img_name)
        {
            standard_image = img_name;
        }
        public string GetActivatedImage()
        {
            return activated_image;
        }
        public void SetActivatedImage(string img_name)
        {
            activated_image = img_name;
        }
        public string GetTransitionImage()
        {
            return transition_image;
        }
        public void SetTransitionImage(string img_name)
        {
            transition_image = img_name;
        }


        protected CTRL_DEF ctrl_flags;
        protected short bevel_width;

        protected Color active_color;
        protected Color border_color;

        protected string picture;
        protected short picture_loc;
        protected Bitmap.BMP_TYPES picture_type;

        protected string standard_image;
        protected string activated_image;
        protected string transition_image;

        protected bool active;
        protected bool show_headings;
        protected int leading;
        protected int line_height;
        protected bool multiselect;
        protected bool dragdrop;
        protected int scroll_bar;
        protected int orientation;
        protected int num_leds;

        protected short item_style;
        protected short selected_style;

        protected bool smooth_scroll;

        protected List<string> items = new List<string>();
        protected List<ColumnDef> columns = new List<ColumnDef>();

        protected int ntabs;
        protected int[] tabs = new int[10];
        protected char pass_char;
    }

    // +--------------------------------------------------------------------+

    public class FormDef : WinDef
    {

        public FormDef(string a_text = null, DWORD a_style = 0) : base(0, WinType.WIN_DEF_FORM, a_text, a_style) { }
        //public virtual ~FormDef();


        public void AddCtrl(CtrlDef def)
        {
            if (def != null)
                controls.Add(def);
        }
        public CtrlDef FindCtrl(DWORD ctrl_id)
        {
            if (ctrl_id > 0)
            {
                foreach (var c in controls)
                    if (c.GetID() == ctrl_id) return c;
            }

            return null;
        }

        public List<CtrlDef> GetControls()
        {
            // cast away const
            FormDef f = (FormDef)this;
            return f.controls;
        }

        public void Load(string fname)
        {
            filename = fname;
            path_name = "Screens/";
            var formentry = ReaderSupport.DeserializeAsset<Serialization.FormEntry>(path_name + filename);
            if (formentry == null || formentry.FORM == null || formentry.FORM.form == null)
            {
                ErrLogger.PrintLine("ERROR: invalid form file '{0}'", path_name + filename);
                return;
            }
            var entry = formentry.FORM.form;
            if (!string.IsNullOrEmpty(entry.text))
                this.SetText(Game.GetText(entry.text));
            this.SetID(entry.id);
            this.SetParentID(entry.pid);
            if (entry.rect != null && entry.rect.Length == 4)
                this.SetRect(new Stars.Rect(entry.rect[0], entry.rect[1], entry.rect[2], entry.rect[3]));

            if (!string.IsNullOrEmpty(entry.font))
                this.SetFont(entry.font);
            if (entry.back_color != null && entry.back_color.Length >= 3)
            {
                var color = new Color32((byte)entry.back_color[0], (byte)entry.back_color[1], (byte)entry.back_color[2], 255);
                this.SetBackColor(color);
            }
            if (entry.base_color != null && entry.base_color.Length >= 3)
            {
                var color = new Color32((byte)entry.base_color[0], (byte)entry.base_color[1], (byte)entry.base_color[2], 255);
                this.SetBaseColor(color);
            }
            if (entry.fore_color != null && entry.fore_color.Length >= 3)
            {
                var color = new Color32((byte)entry.fore_color[0], (byte)entry.fore_color[1], (byte)entry.fore_color[2], 255);
                this.SetForeColor(color);
            }
            if (entry.margins != null)
                GetDefInsets(ref this.margins, entry.margins);
            if (entry.text_insets != null)
                GetDefInsets(ref this.text_insets, entry.text_insets);
            if (entry.cell_insets != null)
                GetDefInsets(ref this.cell_insets, entry.cell_insets);
            if (entry.cells != null)
                if (entry.cells != null && entry.cells.Length == 4)
                    this.cells = new Stars.Rect(entry.cells[0], entry.cells[1], entry.cells[2], entry.cells[3]);
            if (!string.IsNullOrEmpty(entry.texture))
                this.SetTexture(entry.texture);
            this.SetTransparent(entry.transparent);
            this.SetStyle(entry.style);
            if (!string.IsNullOrEmpty(entry.align))
            {
                switch (entry.align.ToLowerInvariant())
                {
                    case "left": this.SetTextAlign(TextFormat.DT_LEFT); break;
                    case "right": this.SetTextAlign(TextFormat.DT_RIGHT); break;
                    case "center": this.SetTextAlign(TextFormat.DT_CENTER); break;
                    default: this.SetTextAlign(TextFormat.DT_LEFT); break;
                }
            }
            if (entry.layout != null)
            {
                this.layout = new LayoutDef();
                ParseLayoutDef(ref this.layout, entry.layout);
            }
            if (entry.defctrls != null && entry.defctrls.Length > 0)
                for (int i = 1; i < entry.defctrls.Length; i++)
                {
                    var fromCtrl = entry.defctrls[i - 1].ctrl;
                    var toCtrl = entry.defctrls[i].ctrl;
                    Serialization.CtrlDef.CopyCtrlDef(fromCtrl, ref toCtrl);
                    entry.defctrls[i].ctrl = toCtrl;
                }
            foreach (var crtlval in entry.ctrls)
            {
                CtrlDef ctrl = new CtrlDef();
                if (!string.IsNullOrEmpty(crtlval.params_name) && entry.defctrls != null)
                {
                    var ctrlref = entry.defctrls.FirstOrDefault(s => s.name == crtlval.params_name);
                    if (ctrlref != null)
                        ParseCtrlDef(ref ctrl, ctrlref.ctrl); // copy default params
                }
                ParseCtrlDef(ref ctrl, crtlval);
                this.AddCtrl(ctrl);
            }
        }
        public void GetDefInsets(ref Insets dst, int[] vals)
        {
            if (vals == null || vals.Length != 4)
            {
                ErrLogger.PrintLine("WARNING: malformed Insets in '{0}'", filename);
                return;
            }
            dst.left = (WORD)(vals[0]);
            dst.right = (WORD)(vals[1]);
            dst.top = (WORD)(vals[2]);
            dst.bottom = (WORD)(vals[3]);
        }

        protected void ParseCtrlDef(ref CtrlDef ctrl, Serialization.CtrlDef val)
        {
            if (!string.IsNullOrEmpty(val.text))
                ctrl.SetText(Game.GetText(val.text));
            if (!string.IsNullOrEmpty(val.id))
                ctrl.SetID(DWORD.Parse(val.id));
            if (!string.IsNullOrEmpty(val.pid))
                ctrl.SetParentID(DWORD.Parse(val.pid));
            if (!string.IsNullOrEmpty(val.alt))
                ctrl.SetAltText(Game.GetText(val.alt));
            if (!string.IsNullOrEmpty(val.type))
            {
                WinType type = WinType.WIN_DEF_LABEL;
                switch (val.type.ToLowerInvariant())
                {
                    case "form": type = WinType.WIN_DEF_FORM; break;
                    case "label": type = WinType.WIN_DEF_LABEL; break;
                    case "button": type = WinType.WIN_DEF_BUTTON; break;
                    case "combo": type = WinType.WIN_DEF_COMBO; break;
                    case "edit": type = WinType.WIN_DEF_EDIT; break;
                    case "image": type = WinType.WIN_DEF_IMAGE; break;
                    case "slider": type = WinType.WIN_DEF_SLIDER; break;
                    case "list": type = WinType.WIN_DEF_LIST; break;
                    case "text": type = WinType.WIN_DEF_RICH; break;
                    case "background": type = WinType.WIN_DEF_BACKGROUND; break;
                    default: break;
                }
                ctrl.SetType(type);
            }
            if (val.rect != null && val.rect.Length == 4)
                ctrl.SetRect(new Stars.Rect(val.rect[0], val.rect[1], val.rect[2], val.rect[3]));

            if (!string.IsNullOrEmpty(val.font))
                ctrl.SetFont(val.font);
            if (val.active_color != null && val.active_color.Length >= 3)
            {
                var color = new Color32((byte)val.active_color[0], (byte)val.active_color[1], (byte)val.active_color[2], 255);
                ctrl.SetActiveColor(color);
            }
            if (val.back_color != null && val.back_color.Length >= 3)
            {
                var color = new Color32((byte)val.back_color[0], (byte)val.back_color[1], (byte)val.back_color[2], 255);
                ctrl.SetBackColor(color);
            }
            if (val.base_color != null && val.base_color.Length >= 3)
            {
                var color = new Color32((byte)val.base_color[0], (byte)val.base_color[1], (byte)val.base_color[2], 255);
                ctrl.SetBaseColor(color);
            }
            if (val.border_color != null && val.border_color.Length >= 3)
            {
                var color = new Color32((byte)val.border_color[0], (byte)val.border_color[1], (byte)val.border_color[2], 255);
                ctrl.SetBorderColor(color);
            }
            if (val.fore_color != null && val.fore_color.Length >= 3)
            {
                var color = new Color32((byte)val.fore_color[0], (byte)val.fore_color[1], (byte)val.fore_color[2], 255);
                ctrl.SetForeColor(color);
            }
            if (!string.IsNullOrEmpty(val.texture))
                ctrl.SetTexture(val.texture);
            if (val.margins != null)
                GetDefInsets(ref ctrl.margins, val.margins);
            if (val.text_insets != null)
                GetDefInsets(ref ctrl.text_insets, val.text_insets);
            if (val.cell_insets != null)
                GetDefInsets(ref ctrl.cell_insets, val.cell_insets);
            if (val.cells != null && val.cells.Length == 4)
                ctrl.cells = new Stars.Rect(val.cells[0], val.cells[1], val.cells[2], val.cells[3]);
            if (!string.IsNullOrEmpty(val.fixed_width))
                ctrl.fixed_width = ReaderSupport.IntParse(val.fixed_width);
            if (!string.IsNullOrEmpty(val.fixed_height))
                ctrl.fixed_height = ReaderSupport.IntParse(val.fixed_height);
            if (!string.IsNullOrEmpty(val.standard_image))
                ctrl.SetStandardImage(val.standard_image);
            if (!string.IsNullOrEmpty(val.activated_image))
                ctrl.SetActivatedImage(val.activated_image);
            if (!string.IsNullOrEmpty(val.transition_image))
                ctrl.SetTransitionImage(val.transition_image);
            if (!string.IsNullOrEmpty(val.picture))
                ctrl.SetPicture(val.picture);
            if (!string.IsNullOrEmpty(val.enabled))
                ctrl.SetEnabled(bool.Parse(val.enabled));
            if (val.items != null && val.items.Length > 0)
                ctrl.AddItems(val.items.ToList<string>());
            if (!string.IsNullOrEmpty(val.tab))
                ctrl.AddTab(ReaderSupport.IntParse(val.tab));
            if (val.columns != null && val.columns.Length > 0)
                foreach (var valcolumn in val.columns)
                    ParseColumnDef(ref ctrl, valcolumn);
            if (!string.IsNullOrEmpty(val.orientation))
                ctrl.SetOrientation(ReaderSupport.IntParse(val.orientation));
            if (val.leading != null)
                ctrl.SetLeading(ReaderSupport.IntParse(val.leading));
            if (val.line_height != null)
                ctrl.SetLineHeight(ReaderSupport.IntParse(val.line_height));
            if (val.multiselect != null)
                ctrl.SetMultiSelect(bool.Parse(val.multiselect));
            if (val.dragdrop != null)
                ctrl.SetDragDrop(bool.Parse(val.dragdrop));
            if (val.scroll_bar != null)
                ctrl.SetScrollBarVisible(ReaderSupport.IntParse(val.scroll_bar));
            if (val.smooth_scroll != null)
                ctrl.SetSmoothScroll(bool.Parse(val.smooth_scroll));
            if (val.picture_loc != null)
                ctrl.SetPictureLocation(short.Parse(val.picture_loc));
            if (val.picture_type != null)
                ctrl.SetPictureType((Bitmap.BMP_TYPES)ReaderSupport.IntParse(val.picture_type));
            if (val.style != null)
                ctrl.SetStyle(ReaderSupport.IntParse(val.style));
            if (!string.IsNullOrEmpty(val.align))
            {
                switch (val.align.ToLowerInvariant())
                {
                    case "left": ctrl.SetTextAlign(TextFormat.DT_LEFT); break;
                    case "right": ctrl.SetTextAlign(TextFormat.DT_RIGHT); break;
                    case "center": ctrl.SetTextAlign(TextFormat.DT_CENTER); break;
                    default: ctrl.SetTextAlign(TextFormat.DT_LEFT); break;
                }
            }
            if (val.single_line != null)
                ctrl.SetSingleLine(bool.Parse(val.single_line));
            if (val.bevel_width != null)
                ctrl.SetBevelWidth(ReaderSupport.ShortParse(val.bevel_width));
            if (val.active != null)
                ctrl.SetActive(bool.Parse(val.active));
            if (val.animated != null)
                ctrl.SetAnimated(bool.Parse(val.animated));
            if (val.border != null)
                ctrl.SetBorder(bool.Parse(val.border));
            if (val.drop_shadow != null)
                ctrl.SetDropShadow(bool.Parse(val.drop_shadow));
            if (val.show_headings != null)
                ctrl.SetShowHeadings(bool.Parse(val.show_headings));
            if (val.sticky != null)
                ctrl.SetSticky(bool.Parse(val.sticky));
            if (val.transparent != null)
                ctrl.SetTransparent(bool.Parse(val.transparent));
            if (val.hide_partial != null)
                ctrl.SetHidePartial(bool.Parse(val.hide_partial));
            if (val.num_leds != null)
                ctrl.SetNumLeds(ReaderSupport.IntParse(val.num_leds));
            if (val.item_style != null)
                ctrl.SetItemStyle(ReaderSupport.ShortParse(val.item_style));
            if (val.selected_style != null)
                ctrl.SetSelectedStyle(ReaderSupport.ShortParse(val.selected_style));
            if (!string.IsNullOrEmpty(val.password))
                ctrl.SetPasswordChar(val.password[0]);
            if (val.layout != null)
            {
                ctrl.layout = new LayoutDef();
                ParseLayoutDef(ref ctrl.layout, val.layout);
            }
        }


        protected void ParseColumnDef(ref CtrlDef ctrl, Serialization.ColumnDef val)
        {
            if (val.text == default(string) &&
                val.width == default(int) &&
                val.align == default(string) &&
                val.sort == default(int) &&
                val.color == null)
                return;

            string text = "";
            int width = 0;
            TextFormat align = TextFormat.DT_LEFT;
            int sort = 0;
            Color c = Color.black;
            bool use_color = false;
            if (!string.IsNullOrEmpty(val.text))
                text = val.text;
            width = val.width;
            if (!string.IsNullOrEmpty(val.align))
            {
                switch (val.align.ToLowerInvariant())
                {
                    case "left": align = TextFormat.DT_LEFT; break;
                    case "right": align = TextFormat.DT_RIGHT; break;
                    case "center": align = TextFormat.DT_CENTER; break;
                    default: align = TextFormat.DT_LEFT; break;
                }
            }
            sort = val.sort;
            if (val.color != null && val.color.Length >= 3)
            {
                var color = new Color32((byte)val.color[0], (byte)val.color[1], (byte)val.color[2], 255);
                use_color = true;
            }
            ctrl.AddColumn(text, width, align, sort);
            if (use_color)
            {
                int index = ctrl.NumColumns() - 1;
                ColumnDef column = ctrl.GetColumn(index);

                if (column != null)
                {
                    column.color = c;
                    column.use_color = true;
                }
            }
        }
        protected void ParseLayoutDef(ref LayoutDef def, Serialization.LayoutDef val)
        {
            if (val.x_mins != null)
                def.x_mins = val.x_mins.ToList();

            if (val.y_mins != null)
                def.y_mins = val.y_mins.ToList(); ;

            if (val.x_weights != null)
                def.x_weights = val.x_weights.ToList(); ;

            if (val.y_weights != null)
                def.y_weights = val.y_weights.ToList(); ;

        }
        private static string filename;
        private static string path_name;

        protected CtrlDef defctrl;
        protected List<CtrlDef> controls = new List<CtrlDef>();
    }

    public struct Insets
    {
        //public Insets() { left=0; right=0; top=0; bottom=0; }
        public Insets(WORD l, WORD r, WORD t, WORD b) { left = l; right = r; top = t; bottom = b; }

        public WORD left;
        public WORD right;
        public WORD top;
        public WORD bottom;
    }
}
namespace StarshatterNet.Gen.Serialization
{
    [Serializable]
    public class FormEntry
    {
        public FormDefEntry FORM;
    }
    [Serializable]
    public class FormDefEntry
    {
        public Form form;
    }
    [Serializable]
    public class Form
    {
        public string text;
        public DWORD id;
        public DWORD pid;
        public int[] rect;
        public string font;
        public int[] back_color;
        public int[] base_color;
        public int[] fore_color;
        public int[] margins; //Insets
        public int[] text_insets; //Insets
        public int[] cell_insets; //Insets
        public int[] cells;
        public string texture;
        public bool transparent;
        public DWORD style;
        public string align; // left, right, center
        public LayoutDef layout;
        public CtrlDefParams[] defctrls;
        public CtrlDef[] ctrls;
    }
    [Serializable]
    public class CtrlDefParams
    {
        public string name;
        public CtrlDef ctrl;
    }
    [Serializable]
    public class CtrlDef
    {
        public string params_name;
        public string text;
        public string id;
        public string pid;
        public string alt;
        public string type; // button, combo, edit, image, slider, list, text, 
        public int[] rect;
        public string font;
        public int[] active_color;
        public int[] back_color;
        public int[] base_color;
        public int[] border_color;
        public int[] fore_color;
        public string texture;
        public int[] margins; //Insets
        public int[] text_insets; //Insets
        public int[] cell_insets; //Insets
        public int[] cells;
        public string fixed_width;
        public string fixed_height;
        public string standard_image;
        public string activated_image;
        public string transition_image;
        public string picture;
        public string enabled;
        public string[] items;
        public string tab;
        public ColumnDef[] columns;
        public string orientation;
        public string leading;
        public string line_height;
        public string multiselect;
        public string dragdrop;
        public string scroll_bar;
        public string smooth_scroll;
        public string picture_loc;
        public string picture_type;
        public string style;
        public string align; // left, right, center
        public string single_line;
        public string bevel_width;
        public string active;
        public string animated;
        public string border;
        public string drop_shadow;
        public string show_headings;
        public string sticky;
        public string transparent;
        public string hide_partial;
        public string num_leds;
        public string item_style;
        public string selected_style;
        public string password;
        public LayoutDef layout;
        public static bool IsNullOrEmpty(Array array)
        {
            return (array == null || array.Length == 0);
        }
        public static void CopyIfEmpy(string from, ref string to)
        {
            if (string.IsNullOrEmpty(to) && !string.IsNullOrEmpty(from))
                to = from;
        }
        public static void CopyIfEmpy<T>(T[] from, ref T[] to)
        {
            if (IsNullOrEmpty(to) && !IsNullOrEmpty(from))
                to = from.Clone() as T[];
        }
        public static void CopyIfEmpy(LayoutDef from, ref LayoutDef to)
        {
            if (LayoutDef.IsNullOrEmpty(to) && !LayoutDef.IsNullOrEmpty(from))
                to = from;
        }
        public static void CopyCtrlDef(CtrlDef from, ref CtrlDef to)
        {
            //if (string.IsNullOrEmpty(from.params_name) && !string.IsNullOrEmpty(from.params_name)) to.params_name = from.params_name;
            CopyIfEmpy(from.text, ref to.text);
            CopyIfEmpy(from.pid, ref to.pid);
            CopyIfEmpy(from.alt, ref to.alt);
            CopyIfEmpy(from.type, ref to.type);
            CopyIfEmpy(from.rect, ref to.rect);
            CopyIfEmpy(from.font, ref to.font);
            CopyIfEmpy(from.active_color, ref to.active_color);
            CopyIfEmpy(from.back_color, ref to.back_color);
            CopyIfEmpy(from.base_color, ref to.base_color);
            CopyIfEmpy(from.border_color, ref to.border_color);
            CopyIfEmpy(from.fore_color, ref to.fore_color);
            CopyIfEmpy(from.texture, ref to.texture);
            CopyIfEmpy(from.margins, ref to.margins);
            CopyIfEmpy(from.text_insets, ref to.text_insets);
            CopyIfEmpy(from.cell_insets, ref to.cell_insets);
            CopyIfEmpy(from.cells, ref to.cells);
            CopyIfEmpy(from.fixed_width, ref to.fixed_width);
            CopyIfEmpy(from.fixed_height, ref to.fixed_height);
            CopyIfEmpy(from.standard_image, ref to.standard_image);
            CopyIfEmpy(from.activated_image, ref to.activated_image);
            CopyIfEmpy(from.transition_image, ref to.transition_image);
            CopyIfEmpy(from.picture, ref to.picture);
            CopyIfEmpy(from.enabled, ref to.enabled);
            CopyIfEmpy(from.items, ref to.items);
            CopyIfEmpy(from.tab, ref to.tab);
            CopyIfEmpy(from.columns, ref to.columns);
            CopyIfEmpy(from.orientation, ref to.orientation);
            CopyIfEmpy(from.leading, ref to.leading);
            CopyIfEmpy(from.line_height, ref to.line_height);
            CopyIfEmpy(from.multiselect, ref to.multiselect);
            CopyIfEmpy(from.dragdrop, ref to.dragdrop);
            CopyIfEmpy(from.scroll_bar, ref to.scroll_bar);
            CopyIfEmpy(from.smooth_scroll, ref to.smooth_scroll);
            CopyIfEmpy(from.picture_loc, ref to.picture_loc);
            CopyIfEmpy(from.picture_type, ref to.picture_type);
            CopyIfEmpy(from.style, ref to.style);
            CopyIfEmpy(from.align, ref to.align);
            CopyIfEmpy(from.bevel_width, ref to.bevel_width);
            CopyIfEmpy(from.active, ref to.active);
            CopyIfEmpy(from.animated, ref to.animated);
            CopyIfEmpy(from.border, ref to.border);
            CopyIfEmpy(from.drop_shadow, ref to.drop_shadow);
            CopyIfEmpy(from.show_headings, ref to.show_headings);
            CopyIfEmpy(from.sticky, ref to.sticky);
            CopyIfEmpy(from.transparent, ref to.transparent);
            CopyIfEmpy(from.hide_partial, ref to.hide_partial);
            CopyIfEmpy(from.num_leds, ref to.num_leds);
            CopyIfEmpy(from.item_style, ref to.item_style);
            CopyIfEmpy(from.selected_style, ref to.selected_style);
            CopyIfEmpy(from.password, ref to.password);
            CopyIfEmpy(from.layout, ref to.layout);
        }
    }

    [Serializable]
    public class ColumnDef
    {
        public string text;
        public int width;
        public string align; // left, right, center
        public int sort;
        public int[] color;
    }
    [Serializable]
    public class LayoutDef
    {
        public DWORD[] x_mins;
        public DWORD[] y_mins;
        public float[] x_weights;
        public float[] y_weights;
        public static bool IsNullOrEmpty(LayoutDef p)
        {
            return (p.x_mins == null || p.x_mins.Length == 0) &&
                    (p.y_mins == null || p.y_mins.Length == 0) &&
                    (p.x_weights == null || p.x_weights.Length == 0) &&
                    (p.y_weights == null || p.y_weights.Length == 0);
        }

    }
}