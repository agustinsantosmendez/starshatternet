﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Hangar.h/Hangar.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Everything needed to store and maintain space craft

    See Also: FlightDeck
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars
{
#warning Hangar class is still in development and is not recommended for production.
    public class Hangar : SimObserver
    {
        public Hangar()
        {
            ship = null; nsquadrons = 0; last_patrol_launch = 0;
            //ZeroMemory(nslots, sizeof(nslots));
            //ZeroMemory(squadrons, sizeof(squadrons));
        }
        public Hangar(Hangar s)
        {
            ship = null; nsquadrons = s.nsquadrons; last_patrol_launch = s.last_patrol_launch;
            //ZeroMemory(nslots, sizeof(nslots));
            //ZeroMemory(squadrons, sizeof(squadrons));
        }
        //virtual ~Hangar();

        public enum HANGAR_STATE
        {
            UNAVAIL = -2,
            MAINT = -1,
            STORAGE = 0,
            PREP,
            ALERT,
            QUEUED,
            LOCKED,
            LAUNCH,
            ACTIVE,
            APPROACH,
            RECOVERY
        };


        public const int MAX_SQUADRONS = 10;

        public virtual void ExecFrame(double seconds)
        {
            for (int n = 0; n < nsquadrons; n++)
            {
                if (squadrons[n] != null && nslots[n] > 0)
                {
                    for (int i = 0; i < nslots[n]; i++)
                    {
                        HangarSlot slot = squadrons[n][i];

                        switch (slot.state)
                        {
                            case HANGAR_STATE.UNAVAIL:
                            case HANGAR_STATE.STORAGE:
                            case HANGAR_STATE.APPROACH:
                                break;

                            case HANGAR_STATE.ACTIVE:
                                if (slot.ship != null && slot.ship.GetFlightPhase() == OP_MODE.APPROACH)
                                    slot.state = HANGAR_STATE.APPROACH;
                                break;

                            case HANGAR_STATE.MAINT:
                                if (slot.time > 0)
                                {
                                    slot.time -= seconds;
                                }
                                else
                                {
                                    slot.time = 0;
                                    slot.state = HANGAR_STATE.STORAGE;
                                }
                                break;


                            case HANGAR_STATE.PREP:
                                if (slot.time > 0)
                                {
                                    slot.time -= seconds;
                                }
                                else
                                {
                                    slot.time = 0;
                                    FinishPrep(slot);
                                }
                                break;

                            case HANGAR_STATE.ALERT:
                                if (slot.time > 0)
                                {
                                    slot.time -= seconds;
                                }
                                else if (slot.deck != null)
                                {
                                    slot.time = 0;

                                    // if package has specific objective, launch as soon as possible:
                                    if (!slot.alert_hold)
                                        slot.deck.Launch(slot.slot);

                                    switch (slot.deck.State(slot.slot))
                                    {
                                        case FlightDeck.FLIGHT_SLOT_STATE.READY: slot.state = HANGAR_STATE.ALERT; break;
                                        case FlightDeck.FLIGHT_SLOT_STATE.QUEUED: slot.state = HANGAR_STATE.QUEUED; break;
                                        case FlightDeck.FLIGHT_SLOT_STATE.LOCKED: slot.state = HANGAR_STATE.LOCKED; break;
                                        case FlightDeck.FLIGHT_SLOT_STATE.LAUNCH: slot.state = HANGAR_STATE.LAUNCH; break;
                                        default: slot.state = HANGAR_STATE.STORAGE; break;
                                    }
                                }
                                break;

                            case HANGAR_STATE.QUEUED:
                            case HANGAR_STATE.LOCKED:
                                if (slot.deck != null)
                                {
                                    switch (slot.deck.State(slot.slot))
                                    {
                                        case FlightDeck.FLIGHT_SLOT_STATE.READY: slot.state = HANGAR_STATE.ALERT; break;
                                        case FlightDeck.FLIGHT_SLOT_STATE.QUEUED: slot.state = HANGAR_STATE.QUEUED; break;
                                        case FlightDeck.FLIGHT_SLOT_STATE.LOCKED: slot.state = HANGAR_STATE.LOCKED; break;
                                        case FlightDeck.FLIGHT_SLOT_STATE.LAUNCH: slot.state = HANGAR_STATE.LAUNCH; break;
                                        default: slot.state = HANGAR_STATE.STORAGE; break;
                                    }

                                    slot.time = slot.deck.TimeRemaining(slot.slot);
                                }
                                break;

                            case HANGAR_STATE.LAUNCH:
                                if (slot.deck != null)
                                {
                                    slot.time = slot.deck.TimeRemaining(slot.slot);

                                    if (slot.ship != null && slot.ship.GetFlightPhase() > OP_MODE.LAUNCH)
                                    {
                                        slot.state = HANGAR_STATE.ACTIVE;
                                        slot.time = 0;
                                    }
                                }
                                break;

                            case HANGAR_STATE.RECOVERY:
                                break;
                        }
                    }
                }
            }
        }

        public void SetShip(Ship s) { ship = s; }

        public virtual bool CreateSquadron(string squadron, CombatGroup group, ShipDesign design, int count, int iff = -1, int[] def_load = null, int maint_count = 0, int dead_count = 0)
        {
            if (nsquadrons < MAX_SQUADRONS && count > 0)
            {
                HangarSlot[] s = new HangarSlot[count];

                for (int j = 0; j < count; j++)
                {
                    s[j].squadron = squadron;
                    s[j].group = group;
                    s[j].design = design;
                    s[j].iff = iff;

                    if (def_load != null)
                        //CopyMemory(s[i].loadout, def_load, sizeof(s[i].loadout));
                        Array.Copy(def_load, s[j].loadout, s[j].loadout.Length);
                }

                squadrons[nsquadrons] = s;
                nslots[nsquadrons] = count;
                names[nsquadrons] = squadron;

                int i = count - 1;
                while (dead_count-- > 0)
                    s[i--].state = HANGAR_STATE.UNAVAIL;

                while (maint_count-- > 0)
                {
                    s[i].state = HANGAR_STATE.MAINT;
                    s[i--].time = 600 + RandomHelper.Rand() / 15;
                }

                nsquadrons++;
                return true;
            }

            return false;
        }

        // used only by net client to expedite creation of already active ships:
        public virtual bool GotoActiveFlight(int squadron, int slot_index, Element elem, int[] loadout)
        {
            if (elem != null && squadron < nsquadrons && slot_index < nslots[squadron])
            {
                HangarSlot slot = squadrons[squadron][slot_index];

                if (slot.state == HANGAR_STATE.STORAGE)
                {
                    slot.deck = null;
                    slot.state = HANGAR_STATE.ACTIVE;
                    slot.time = 0;
                    slot.package = elem;
                    slot.alert_hold = false;

                    if (loadout != null)
                        //CopyMemory(slot.loadout, loadout, sizeof(slot.loadout));
                        Array.Copy(loadout, slot.loadout, slot.loadout.Length);

                    Sim sim = Sim.GetSim();

                    string ship_name = slot.squadron;

                    slot.ship = sim.CreateShip(ship_name, "",
                    (ShipDesign)slot.design,
                    ship.GetRegion().Name(),
                    ship.Location() + RandomHelper.RandomPoint(),
                    slot.iff,
                    ship.GetCommandAILevel(),
                    slot.loadout);

                    if (slot.ship != null)
                    {
                        Observe(slot.ship);

                        elem.SetCommander(ship.GetElement());
                        elem.AddShip(slot.ship);

                        if (slot.group != null)
                        {
                            elem.SetCombatGroup(slot.group);
                            elem.SetCombatUnit(slot.group.GetNextUnit());
                        }

                        string name = string.Format("{0} {1}", elem.Name(), slot.ship.GetElementIndex());

                        slot.ship.SetName(name);
                    }

                    return true;
                }
            }

            return false;
        }

        public virtual bool GotoAlert(int squadron, int slot, FlightDeck d, Element elem = null, int[] loadout = null, bool pkg = false, bool expedite = false)
        {
            if (squadron < nsquadrons && slot < nslots[squadron])
            {
                HangarSlot s = squadrons[squadron][slot];

                if (s.state == HANGAR_STATE.STORAGE)
                {
                    s.deck = d;
                    s.state = HANGAR_STATE.PREP;
                    s.time = expedite ? 3 : s.design.prep_time;
                    s.package = elem;
                    s.alert_hold = !pkg;

                    if (loadout != null)
                        // CopyMemory(s.loadout, loadout, sizeof(s.loadout));
                        Array.Copy(loadout, s.loadout, s.loadout.Length);

                    if (expedite)
                        FinishPrep(s);

                    return true;
                }
            }

            return false;
        }
        public virtual bool Ready(int squadron, int slot, FlightDeck d)
        {
            if (squadron < 0 || squadron >= nsquadrons || slot < 0 || slot >= nslots[squadron] || d == null)
                return false;

            HangarSlot s = squadrons[squadron][slot];

            s.time = 3;   // 5;
            s.deck = d;
            s.slot = -1;  // take first available slot

            if (d.Spot(s.ship, ref s.slot))
            {
                s.state = HANGAR_STATE.ALERT;
                s.alert_hold = false;
                return true;
            }

            return false;
        }
        public virtual bool Launch(int squadron, int slot)
        {
            if (squadron < nsquadrons && slot < nslots[squadron])
            {
                HangarSlot s = squadrons[squadron][slot];

                if (s.state == HANGAR_STATE.ALERT && s.deck != null)
                    return s.deck.Launch(s.slot);
            }

            return false;
        }

        public virtual bool StandDown(int squadron, int slot)
        {
            if (squadron < nsquadrons && slot < nslots[squadron])
            {
                HangarSlot s = squadrons[squadron][slot];

                Element package = null;
                bool clear_slot = false;

                if (s.state == HANGAR_STATE.ALERT && s.deck != null)
                {
                    if (s.deck.Clear(s.slot))
                    {
                        if (s.ship != null)
                        {
                            Sim sim = Sim.GetSim();

                            if (s.package != null)
                            {
                                package = s.package;
                                package.DelShip(s.ship);
                            }

                            sim.DestroyShip(s.ship);
                        }

                        clear_slot = true;
                    }
                }

                else if (s.state == HANGAR_STATE.PREP)
                {
                    clear_slot = true;
                    package = s.package;
                }

                if (clear_slot)
                {
                    s.state = HANGAR_STATE.STORAGE;
                    s.deck = null;
                    s.slot = 0;
                    s.ship = null;
                    s.package = null;

                    if (package != null)
                    {
                        int npkg = 0;
                        for (int i = 0; i < nslots[squadron]; i++)
                        {
                            if (squadrons[squadron][i].package == package)
                                npkg++;
                        }

                        if (npkg == 0)
                        {
                            Sim.GetSim().DestroyElement(package);
                        }
                    }

                    return true;
                }
            }

            return false;
        }
        public virtual bool CanStow(Ship incoming)
        {
            int squadron = -1;
            int slot = -1;

            if (FindSlot(incoming, ref squadron, ref slot))
                return true;

            return false;
        }

        public virtual bool Stow(Ship incoming)
        {
            int squadron = -1;
            int slot = -1;

            if (FindSlot(incoming, ref squadron, ref slot))
            {
                HangarSlot s = squadrons[squadron][slot];
                s.state = HANGAR_STATE.MAINT;
                s.design = incoming.Design();
                s.time = 2400;
                s.package = null;   // XXX MEMORY LEAK?

                // extra maintenance time?
                if (incoming.Integrity() < incoming.Design().integrity)
                {
                    double damage = 100 * ((double)incoming.Design().integrity - (double)incoming.Integrity()) / (double)incoming.Design().integrity;

                    if (damage < 10) s.time *= 1.2;
                    else if (damage < 25) s.time *= 2;
                    else if (damage < 50) s.time *= 4;
                    else s.time *= 10;
                }

                // quicker turnaround during network play:
                Sim sim = Sim.GetSim();
                if (sim != null && sim.IsNetGame())
                    s.time /= 40;

                return true;
            }

            return false;
        }
        public virtual bool FindSlot(Ship test, ref int squadron, ref int slot, HANGAR_STATE desired_state = HANGAR_STATE.UNAVAIL)
        {
            if (test != null)
            {
                // if test is already inbound to this carrier,
                // keep the inbound squadron and slot selections:
                if (desired_state == HANGAR_STATE.UNAVAIL && test.GetInbound() != null)
                {
                    InboundSlot inbound = test.GetInbound();
                    FlightDeck deck = inbound.GetDeck();

                    if (deck != null && deck.GetCarrier() == ship && deck.IsPowerOn())
                    {
                        squadron = inbound.Squadron();
                        slot = inbound.Index();
                        return true;
                    }
                }

                int avail_squadron = -1;
                int avail_slot = -1;

                for (int i = 0; i < nsquadrons; i++)
                {
                    if (squadron < 0 || squadron == i)
                    {
                        for (int j = 0; j < nslots[i]; j++)
                        {
                            HangarSlot s = squadrons[i][j];
                            if (s.ship == test)
                            {
                                squadron = i;
                                slot = j;
                                return true;
                            }

                            else if (avail_slot < 0 && s.ship == null)
                            {
                                if ((desired_state > HANGAR_STATE.STORAGE && s.state == HANGAR_STATE.STORAGE) ||
                                        (desired_state < HANGAR_STATE.STORAGE && s.state == HANGAR_STATE.UNAVAIL))
                                {
                                    avail_squadron = i;
                                    avail_slot = j;
                                }
                            }
                        }
                    }
                }

                if (avail_squadron >= 0 && avail_slot >= 0)
                {
                    squadron = avail_squadron;
                    slot = avail_slot;

                    if (desired_state > HANGAR_STATE.STORAGE)
                    {
                        HangarSlot s = squadrons[squadron][slot];

                        s.ship = test;
                        s.design = test.Design();
                        s.state = desired_state;
                        s.deck = null;
                        s.slot = 0;
                        s.package = test.GetElement();
                        s.time = 0;

                        Observe(s.ship);
                    }

                    return true;
                }
            }

            return false;
        }
        public virtual bool FindSquadronAndSlot(Ship test, ref int squadron, ref int slot)
        {
            if (test != null)
            {
                for (int i = 0; i < nsquadrons; i++)
                {
                    if (squadron < 0 || squadron == i)
                    {
                        for (int j = 0; j < nslots[i]; j++)
                        {
                            HangarSlot s = squadrons[i][j];
                            if (s.ship == test)
                            {
                                squadron = i;
                                slot = j;
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        public virtual bool FindAvailSlot(ShipDesign design, ref int squadron, ref int slot)
        {
            if (design != null)
            {
                for (int i = 0; i < nsquadrons; i++)
                {
                    if (nslots[i] > 0 && squadrons[i][0].design == design)
                    {
                        for (int j = 0; j < nslots[i]; j++)
                        {
                            HangarSlot s = squadrons[i][j];

                            if (s.state == HANGAR_STATE.STORAGE)
                            {
                                squadron = i;
                                slot = j;
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }
        public virtual bool FinishPrep(HangarSlot slot)
        {
            if (slot.deck.SpaceLeft(slot.design.type) != 0)
            {
                Sim sim = Sim.GetSim();

                string ship_name = slot.squadron;

                slot.ship = sim.CreateShip(ship_name, "",
                                            (ShipDesign)slot.design,
                                            ship.GetRegion().Name(),
                                            new Point(0, 0, 0),
                                            slot.iff,
                                            ship.GetCommandAILevel(),
                                            slot.loadout);

                Observe(slot.ship);

                if (slot.package != null)
                {
                    slot.package.SetCommander(ship.GetElement());
                    slot.package.AddShip(slot.ship);

                    if (slot.group != null)
                    {
                        slot.package.SetCombatGroup(slot.group);
                        slot.package.SetCombatUnit(slot.group.GetNextUnit());
                    }

                    string name = string.Format("{0} {1}", slot.package.Name(), slot.ship.GetElementIndex());
                    slot.ship.SetName(name);
                }

                slot.slot = -1;  // take first available slot
                if (slot.deck.Spot(slot.ship, ref slot.slot))
                {
                    slot.state = HANGAR_STATE.ALERT;
                    return true;
                }

                ErrLogger.PrintLine("WARNING: Could not spot alert ship - carrier: '{0}' ship '{1}'", ship.Name(), slot.ship.Name());
            }

            return false;
        }
        public virtual void SetAllIFF(int iff)
        {
            for (int n = 0; n < nsquadrons; n++)
            {
                if (squadrons[n] != null && nslots[n] > 0)
                {
                    for (int i = 0; i < nslots[n]; i++)
                    {
                        HangarSlot slot = squadrons[n][i];

                        if (slot.ship != null)
                            slot.ship.SetIFF(iff);
                    }
                }
            }
        }


        public override bool Update(SimObject obj)
        {
            bool found = false;

            for (int n = 0; !found && n < nsquadrons; n++)
            {
                if (squadrons[n] != null && nslots[n] > 0)
                {
                    for (int i = 0; !found && i < nslots[n]; i++)
                    {
                        HangarSlot slot = squadrons[n][i];

                        if (slot.ship == obj)
                        {
                            // was ship destroyed in combat,
                            // or did it just dock here?
                            if (slot.state != HANGAR_STATE.MAINT)
                            {
                                slot.state = HANGAR_STATE.UNAVAIL;
                                slot.ship = null;
                                slot.deck = null;
                                slot.time = 0;
                                slot.package = null;
                            }

                            found = true;
                        }
                    }
                }
            }

            return base.Update(obj);
        }
        public override string GetObserverName()
        {
            string name;
            if (ship != null)
                name = string.Format("Hangar({0})", ship.Name());
            else
                name = "Hangar";
            return name;
        }


        // accessors:
        public int NumSquadrons() { return nsquadrons; }
        public string SquadronName(int n)
        {
            if (n >= 0 && n < nsquadrons)
                return names[n];

            return localeManager.GetText("Unknown");
        }

        public int SquadronSize(int n)
        {
            if (n >= 0 && n < nsquadrons)
                return nslots[n];

            return 0;
        }

        public int SquadronIFF(int n)
        {
            if (n >= 0 && n < nsquadrons)
                return squadrons[n][0].iff;

            return 0;
        }

        public ShipDesign SquadronDesign(int n)
        {
            if (n >= 0 && n < nsquadrons && nslots[n] != 0)
                return squadrons[n][0].design;

            return null;
        }


        public int NumShipsReady(int n)
        {
            int result = 0;

            if (n >= 0 && n < nsquadrons && squadrons[n] != null && nslots[n] > 0)
            {
                for (int i = 0; i < nslots[n]; i++)
                {
                    HangarSlot slot = squadrons[n][i];

                    if (slot.state == HANGAR_STATE.STORAGE)
                        result++;
                }
            }

            return result;
        }

        public int NumShipsMaint(int n)
        {
            int result = 0;

            if (n >= 0 && n < nsquadrons && squadrons[n] != null && nslots[n] > 0)
            {
                for (int i = 0; i < nslots[n]; i++)
                {
                    HangarSlot slot = squadrons[n][i];

                    if (slot.state == HANGAR_STATE.MAINT)
                        result++;
                }
            }

            return result;
        }
        public int NumShipsDead(int n)
        {
            int result = 0;

            if (n >= 0 && n < nsquadrons && squadrons[n] != null && nslots[n] > 0)
            {
                for (int i = 0; i < nslots[n]; i++)
                {
                    HangarSlot slot = squadrons[n][i];

                    if (slot.state == HANGAR_STATE.UNAVAIL)
                        result++;
                }
            }

            return result;
        }


        public int NumSlotsEmpty()
        {
            int result = 0;

            for (int n = 0; n < nsquadrons; n++)
            {
                if (squadrons[n] != null && nslots[n] > 0)
                {
                    for (int i = 0; i < nslots[n]; i++)
                    {
                        HangarSlot slot = squadrons[n][i];

                        if (slot.state == HANGAR_STATE.UNAVAIL)
                            result++;
                    }
                }
            }

            return result;
        }



        public int GetActiveElements(List<Element> active_list)
        {
            active_list.Clear();

            for (int n = 0; n < nsquadrons; n++)
            {
                if (squadrons[n] !=null && nslots[n] > 0)
                {
                    for (int i = 0; i < nslots[n]; i++)
                    {
                        HangarSlot slot = squadrons[n][i];

                        if (slot.package != null && !active_list.Contains(slot.package))
                            active_list.Add(slot.package);
                    }
                }
            }

            return active_list.Count;
        }


        public HangarSlot GetSlot(int squadron, int index)
        {
            if (squadron >= 0 && squadron < nsquadrons)
                if (index >= 0 && index < nslots[squadron])
                    return squadrons[squadron][index];

            return null;
        }

        public Ship GetShip(HangarSlot s)
        {
            if (s != null) return s.ship;
            return null;
        }

        public ShipDesign GetDesign(HangarSlot s)
        {
            if (s != null) return s.design;
            return null;
        }


        public FlightDeck GetFlightDeck(HangarSlot s)
        {
            if (s != null) return s.deck;
            return null;
        }
        public int GetFlightDeckSlot(HangarSlot s)
        {
            if (s != null) return s.slot;
            return 0;
        }
        public Hangar.HANGAR_STATE GetState(HangarSlot s)
        {
            if (s != null) return s.state;
            return 0;
        }
        public double TimeRemaining(HangarSlot s)
        {
            if (s != null) return s.time;
            return 0;
        }
        public Element GetPackageElement(HangarSlot s)
        {
            if (s != null) return s.package;
            return null;
        }
        public int[] GetLoadout(HangarSlot s)
        {
            if (s != null) return s.loadout;
            return null;
        }
        public string StatusName(HangarSlot s)
        {
            switch (s.state)
            {
                default:
                case HANGAR_STATE.UNAVAIL: return localeManager.GetText("hangar.UNAVAIL");
                case HANGAR_STATE.MAINT: return localeManager.GetText("hangar.MAINT");
                case HANGAR_STATE.STORAGE: return localeManager.GetText("hangar.STORAGE");
                case HANGAR_STATE.PREP: return localeManager.GetText("hangar.PREP");
                case HANGAR_STATE.ALERT: return localeManager.GetText("hangar.ALERT");
                case HANGAR_STATE.QUEUED:
                    {
                        string state = localeManager.GetText("hangar.QUEUED");
                        string seq = string.Format(" {1}", s.deck.Sequence(s.slot));
                        return state + seq;
                    }
                case HANGAR_STATE.LOCKED: return localeManager.GetText("hangar.LOCKED");
                case HANGAR_STATE.LAUNCH: return localeManager.GetText("hangar.LAUNCH");
                case HANGAR_STATE.ACTIVE: return localeManager.GetText("hangar.ACTIVE");
                case HANGAR_STATE.APPROACH: return localeManager.GetText("hangar.APPROACH");
                case HANGAR_STATE.RECOVERY: return localeManager.GetText("hangar.RECOVERY");
            }
        }

        public int PreflightQueue(FlightDeck d)
        {
            int result = 0;

            for (int n = 0; n < nsquadrons; n++)
            {
                if (squadrons[n] != null && nslots[n] > 0)
                {
                    for (int i = 0; i < nslots[n]; i++)
                    {
                        HangarSlot slot = squadrons[n][i];

                        if (slot.deck == d)
                            result++;
                    }
                }
            }

            return result;
        }

        public uint GetLastPatrolLaunch()
        {
            return last_patrol_launch;
        }

        public void SetLastPatrolLaunch(uint t)
        {
            last_patrol_launch = t;
        }


        protected Ship ship;
        protected int nsquadrons;
        protected int[] nslots = new int[MAX_SQUADRONS];
        protected string[] names = new string[MAX_SQUADRONS];
        protected HangarSlot[][] squadrons = new HangarSlot[MAX_SQUADRONS][];
        protected uint last_patrol_launch;
        protected ContentBundle localeManager;
    }
}