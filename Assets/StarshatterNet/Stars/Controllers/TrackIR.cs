﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    FILE:         TrackIR.h/TrackIR.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    TrackIR head tracker interface class
*/
using DWORD = System.UInt32;
namespace StarshatterNet.Controllers
{
#warning TrackIR class is still in development and is not recommended for production.
    public class TrackIR
    {
        public TrackIR()
        {
            running = false; frame_signature = 0;
            az = 0; el = 0; x = 0; y = 0; z = 0;
        }
        //public ~TrackIR();

        public DWORD ExecFrame() { return 0; }

        public bool IsRunning() { return running; }
        public double GetAzimuth() { return az; }
        public double GetElevation() { return el; }

        public double GetX() { return x; }
        public double GetY() { return y; }
        public double GetZ() { return z; }



        protected bool running;
        protected DWORD stale_frames;
        protected DWORD frame_signature;

        protected double az;
        protected double el;

        protected double x; // vrt
        protected double y; // vup
        protected double z; // vpn (i think)

        private const double TRACK_TOP = -8000;
        private const double TRACK_BOTTOM = 8000;
        private const double TRACK_LEFT = 16000;
        private const double TRACK_RIGHT = -16000;
        private const double TRACK_XYZ = 24000;
    }
}