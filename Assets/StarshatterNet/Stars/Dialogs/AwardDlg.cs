﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      AwardDlg.h/AwardDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using System;
using StarshatterNet.Audio;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class AwardDlg : FormWindow
    {
        public AwardDlg(Screen s, Camera c, FormDef def, PlanScreen mgr, GameObject p) :
               base(s, 0, 0, s.Width(), s.Height(), c, p, "AwardDlg")
        {
            manager = mgr;
            lbl_name = null; lbl_info = null; img_rank = null; btn_close = null; exit_latch = true;

            Init(def);
        }

        public override void RegisterControls()
        {
            lbl_name = FindControl(203);
            lbl_info = FindControl(201);
            img_rank = (ImageBox)FindControl(202);

            btn_close = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, btn_close, OnClose);
        }

        public override void Show()
        {
            base.Show();
            ShowPlayer();

            exit_latch = true;
        }
        public virtual void ExecFrame()
        {
            if (Keyboard.KeyDown(KeyMap.VK_RETURN))
            {
                if (!exit_latch)
                    OnClose(null, null);
            }

            else if (Keyboard.KeyDown(KeyMap.VK_ESCAPE))
            {
                if (!exit_latch)
                    OnClose(null, null);
            }

            else
            {
                exit_latch = false;
            }
        }


        // Operations:
        public virtual void OnClose(ActiveWindow obj, AWEvent evnt)
        {
            Player player = Player.GetCurrentPlayer();
            if (player != null)
                player.ClearShowAward();

            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                Mouse.Show(false);

                Campaign campaign = Campaign.GetCampaign();
                if (campaign != null && campaign.GetCampaignId() < (int)Campaign.CONSTANTS.SINGLE_MISSIONS)
                    stars.SetGameMode(Starshatter.MODE.CMPN_MODE);
                else
                    stars.SetGameMode(Starshatter.MODE.MENU_MODE);
            }

            else
                ErrLogger.PrintLine("PANIC: AwardDlg.OnClose() - Game instance not found");
        }
        public virtual void ShowPlayer()
        {
            Player p = Player.GetCurrentPlayer();

            if (p != null)
            {
                if (lbl_name != null)
                {
                    lbl_name.SetText(p.AwardName());
                }

                if (lbl_info != null)
                {
                    lbl_info.SetText(p.AwardDesc());
                }

                if (img_rank != null)
                {
                    img_rank.SetPicture(p.AwardImage().Texture);
                    img_rank.Show();
                }

                Sound congrats = p.AwardSound();
                if (congrats != null)
                {
                    congrats.Play();
                }
            }
            else
            {
                if (lbl_info != null) lbl_info.SetText("");
                if (img_rank != null) img_rank.Hide();
            }
        }


        protected PlanScreen manager;

        protected ActiveWindow lbl_name;
        protected ActiveWindow lbl_info;
        protected ImageBox img_rank;
        protected Button btn_close;

        protected bool exit_latch;

    }
}
