﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;

namespace StarshatterNet.Stars.Network
{
#warning NetObjKill class is still in development and is not recommended for production.
    public class NetUtil
    {
        internal static void SendSysStatus(Ship ship, ShipSystem system)
        {
            throw new NotImplementedException();
        }

        internal static void SendObjKill(Ship slot_ship, Ship carrier, NetObjKill.KillType kILL_DOCK, int index = 0)
        {
            throw new NotImplementedException();
        }

        internal static void SendElemCreate(Element elem, int squadron, int[] slots, bool v)
        {
            throw new NotImplementedException();
        }

        internal static void SendShipLaunch(Ship ship, int squadron, int slot)
        {
            throw new NotImplementedException();
        }

        internal static void SendObjHyper(Ship jumpship, string v, Vector3D loc, Ship fc_src, Ship fc_dst, TRAN_TYPE type)
        {
            throw new NotImplementedException();
        }

        internal static void SendWepDestroy(SimObject drone)
        {
            throw new NotImplementedException();
        }

        internal static void SendWepTrigger(Weapon weapon, int nbarrels)
        {
            throw new NotImplementedException();
        }

        internal static void SendWepRelease(Weapon weapon, Shot shot)
        {
            throw new NotImplementedException();
        }

        internal static void SendObjDamage(Ship ship, double hull_damage)
        {
            throw new NotImplementedException();
        }

        internal static void SendNavData(bool v, Element element, int index, Instruction pt)
        {
            throw new NotImplementedException();
        }

        internal static void SendNavDelete(Element element, int index)
        {
            throw new NotImplementedException();
        }

        internal static void SendObjTarget(Ship ship)
        {
            throw new NotImplementedException();
        }

        internal static void SendSysDamage(Ship ship, ShipSystem system, double sys_damage)
        {
            throw new NotImplementedException();
        }

        internal static void SendObjDamage(Ship ship, double damage_applied, Shot shot)
        {
            throw new NotImplementedException();
        }

        internal static void SendObjEmcon(Ship ship)
        {
            throw new NotImplementedException();
        }

        internal static void SendSelfDestruct(Ship player_ship, double damage)
        {
            throw new NotImplementedException();
        }
    }
}