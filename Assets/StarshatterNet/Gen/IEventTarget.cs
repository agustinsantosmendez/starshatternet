﻿using StarshatterNet.Stars;

namespace StarshatterNet.Gen
{
    public interface IEventTarget
    {
        string GetDescription();
        bool HasFocus();
        bool IsEnabled();
        bool IsFormActive();
        bool IsVisible();
        void KillFocus();
        int OnClick();
        int OnKeyDown(int vk, int flags);
        int OnLButtonDown(int x, int y);
        int OnLButtonUp(int x, int y);
        int OnMouseEnter(int x, int y);
        int OnMouseExit(int x, int y);
        int OnMouseMove(int x, int y);
        int OnMouseWheel(int wheel);
        int OnRButtonDown(int x, int y);
        int OnRButtonUp(int x, int y);
        int OnSelect();
        void SetFocus();
        Rect TargetRect();
    }
}