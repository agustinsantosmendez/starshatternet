﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CombatGroup.h/CombatGroup.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
 */
using System;
using System.Collections.Generic;
using DigitalRune.Mathematics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.SimElements;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning CombatGroup class is still in development and is not recommended for production.
    public class CombatGroup : IComparable<CombatGroup>
    {
        public enum GROUP_TYPE
        {
            FORCE = 1,           // Commander In Chief

            WING,                // Air Force
            INTERCEPT_SQUADRON,  // a2a fighter
            FIGHTER_SQUADRON,    // multi-role fighter
            ATTACK_SQUADRON,     // strike / attack
            LCA_SQUADRON,        // landing craft

            FLEET,               // Navy
            DESTROYER_SQUADRON,  // destroyer
            BATTLE_GROUP,        // heavy cruiser(s)
            CARRIER_GROUP,       // fleet carrier

            BATTALION,           // Army
            MINEFIELD,
            BATTERY,
            MISSILE,
            STATION,             // orbital station
            STARBASE,            // planet-side base

            C3I,                 // Command, Control, Communications, Intelligence
            COMM_RELAY,
            EARLY_WARNING,
            FWD_CONTROL_CTR,
            ECM,

            SUPPORT,
            COURIER,
            MEDICAL,
            SUPPLY,
            REPAIR,

            CIVILIAN,            // root for civilian groups

            WAR_PRODUCTION,
            FACTORY,
            REFINERY,
            RESOURCE,

            INFRASTRUCTURE,
            TRANSPORT,
            NETWORK,
            HABITAT,
            STORAGE,

            NON_COM,             // other civilian traffic
            FREIGHT,
            PASSENGER,
            PRIVATE
        };

        public CombatGroup(GROUP_TYPE t, int n, string s, int iff_code, Intel.INTEL_TYPE e, CombatGroup p = null)
        {
            type = t; id = n; name = s; iff = iff_code; enemy_intel = e;
            parent = p; value = 0; plan_value = 0; unit_index = 0; combatant = null;
            expanded = false; sorties = 0; kills = 0; points = 0;
            current_zone = null; assigned_zone = null; zone_lock = false;
            if (parent != null)
                parent.AddComponent(this);
        }

        // ~CombatGroup();

        // comparison operators are used to sort combat groups into a priority list
        // in DESCENDING order, so the sense of the comparison is backwards from
        // usual...
        //int operator <(  CombatGroup& g)    { return value >  g.value; }
        //int operator <=(  CombatGroup& g)    { return value >= g.value; }
        //int operator ==(  CombatGroup& g)    { return this  == &g;      }
        public int CompareTo(CombatGroup other)
        {
            // If other is not a valid object reference, this instance is greater.
            if (other == null) return 1;

            // comparison operators are used to sort combat groups into a priority list
            // in DESCENDING order, so the sense of the comparison is backwards from
            // usual...
            return -value.CompareTo(other.value);
        }

        // Define the is greater than operator.
        public static bool operator >(CombatGroup l, CombatGroup r)
        {
            return l.CompareTo(r) > 0;
        }

        // Define the is less than operator.
        public static bool operator <(CombatGroup l, CombatGroup r)
        {
            return l.CompareTo(r) < 0;
        }

        // Define the is greater than or equal to operator.
        public static bool operator >=(CombatGroup l, CombatGroup r)
        {
            return l.CompareTo(r) >= 0;
        }

        // Define the is less than or equal to operator.
        public static bool operator <=(CombatGroup l, CombatGroup r)
        {
            return l.CompareTo(r) <= 0;
        }
        // operations:
        public static CombatGroup LoadOrderOfBattle(string filename, int team, Combatant combatant)
        {
            CombatGroup force = null;
            var combatGroups = ReaderSupport.DeserializeAsset<CombatGroups>(filename);
            if (combatGroups == null)
            {
                ErrLogger.PrintLine("ERROR: could not parse order of battle {0}", filename);
                return null;
            }
            if (combatGroups.groups.Count == 0)
            {
                ErrLogger.PrintLine("ERROR: group struct missing in {0}", filename);
                return null;
            }
            foreach (var cgi in combatGroups.groups)
            {
                string name = null;
                string type = null;
                string intel = "KNOWN";
                string region = null;
                string system = null;
                string parent_type = null;
                int parent_id = 0;
                int id = 0;
                int iff = -1;
                Point loc = new Point(1.0e9f, 0.0f, 0.0f);
                int unit_index = 0;

                List<CombatUnit> unit_list = new List<CombatUnit>();

                // all groups in this OOB default to the IFF of the main force
                if (force != null)
                    iff = force.GetIFF();

                if (cgi != null && (iff < 0 || team < 0 || iff == team))
                {
                    if (!string.IsNullOrEmpty(cgi.name))
                        name = cgi.name;
                    if (!string.IsNullOrEmpty(cgi.type))
                        type = cgi.type;
                    if (!string.IsNullOrEmpty(cgi.region))
                        region = cgi.region;
                    if (!string.IsNullOrEmpty(cgi.system))
                        system = cgi.system;
                    if (cgi.loc != null)
                        loc = cgi.loc;
                    if (!string.IsNullOrEmpty(cgi.parent_type))
                        parent_type = cgi.parent_type;
                    if (!string.IsNullOrEmpty(cgi.parent_id))
                        parent_id = int.Parse(cgi.parent_id);
                    if (!string.IsNullOrEmpty(cgi.iff))
                        iff = int.Parse(cgi.iff);
                    if (!string.IsNullOrEmpty(cgi.id))
                        id = int.Parse(cgi.id);
                    if (!string.IsNullOrEmpty(cgi.unit_index))
                        unit_index = int.Parse(cgi.unit_index);

                    foreach (var unit in cgi.units)
                    {
                        string unit_name = null;
                        string unit_regnum = null;
                        string unit_design = null;
                        string unit_skin = null;
                        CLASSIFICATION unit_class = (CLASSIFICATION)0;
                        int unit_count = 1;
                        int unit_dead = 0;
                        int unit_damage = 0;
                        int unit_heading = 0;

                        string unit_region = null;
                        string design = null;
                        Point unit_loc = new Point(1.0e9f, 0.0f, 0.0f);
                        unit_count = 1;
                        if (!string.IsNullOrEmpty(unit.name))
                            unit_name = unit.name;
                        if (!string.IsNullOrEmpty(unit.regnum))
                            unit_regnum = unit.regnum;
                        if (!string.IsNullOrEmpty(unit.region))
                            unit_region = unit.region;
                        if (unit.loc != null)
                            unit_loc = unit.loc;
                        if (!string.IsNullOrEmpty(unit.type))
                            unit_class = ShipDesign.ClassForName(unit.type);
                        if (!string.IsNullOrEmpty(unit.design))
                            unit_design = unit.design;
                        if (!string.IsNullOrEmpty(unit.skin))
                            unit_skin = unit.skin;
                        if (unit.count != default(int))
                            unit_count = unit.count;
                        if (unit.dead_count != default(int))
                            unit_dead = unit.dead_count;
                        if (unit.damage != default(int))
                            unit_damage = unit.damage;
                        if (unit.heading != default(int))
                            unit_heading = (int)unit.heading;

                        if (!ShipDesign.CheckName(unit.design))
                        {
                            ErrLogger.PrintLine("ERROR: invalid design '{0}' for unit '{1}' in '{2}'", unit.design, unit.name, filename);
                            return null;
                        }

                        CombatUnit cu = new CombatUnit(unit.name, unit.regnum, unit_class, unit.design, unit.count, iff);
                        cu.SetRegion(unit.region);
                        cu.SetSkin(unit.skin);
                        cu.MoveTo(unit.loc);
                        cu.Kill(unit.dead_count);
                        cu.SetSustainedDamage(unit.damage);
                        cu.SetHeading(unit.heading * ConstantsF.DEGREES);
                        unit_list.Add(cu);
                    }
                }

                if (iff >= 0 && (iff == team || team < 0))
                {
                    CombatGroup parent_group = null;

                    if (force != null)
                    {
                        parent_group = force.FindGroup(TypeFromName(parent_type), parent_id);
                    }

                    CombatGroup g = new CombatGroup(TypeFromName(type), id, name, iff, Intel.IntelFromName(intel), parent_group);
                    g.region = region;
                    g.combatant = combatant;
                    g.unit_index = unit_index;

                    if (loc.X >= 1e9)
                    {
                        if (parent_group != null)
                            g.location = parent_group.location;
                        else
                            g.location = new Point(0, 0, 0);
                    }
                    else
                    {
                        g.location = loc;
                    }

                    if (unit_list.Count != 0)
                    {
                        unit_list[0].SetLeader(true);

                        foreach (CombatUnit u in unit_list)
                        {
                            u.SetCombatGroup(g);

                            if (string.IsNullOrEmpty(u.GetRegion()))
                            {
                                u.SetRegion(g.GetRegion());
                                u.MoveTo(g.Location());
                            }

                            if (parent_group != null &&
                               (u.Type() == CLASSIFICATION.FIGHTER ||
                                u.Type() == CLASSIFICATION.ATTACK))
                            {

                                CombatUnit carrier = null;
                                CombatGroup p = parent_group;

                                while (p != null && carrier == null)
                                {
                                    if (p.units.Count != 0 && p.units[0].Type() == CLASSIFICATION.CARRIER)
                                    {
                                        carrier = p.units[0];
                                        u.SetCarrier(carrier);
                                        u.SetRegion(carrier.GetRegion());
                                    }

                                    p = p.parent;
                                }
                            }
                        }

                        g.units.AddRange(unit_list);
                    }
                    if (force == null)
                        force = g;
                }  // iff == team?
            } // group

            if (force != null)
                force.CalcValue();

            return force;
        }

        public static void SaveOrderOfBattle(string fname, CombatGroup force)
        {
            throw new System.NotImplementedException();
        }

        public static void MergeOrderOfBattle(byte[] block, string fname, int iff, Combatant combatant, Campaign campaign)
        {
            throw new System.NotImplementedException();
        }

        public void AddComponent(CombatGroup g)
        {
            if (g != null)
            {
                g.parent = this;
                components.Add(g);
            }
        }

        public CombatGroup FindGroup(GROUP_TYPE t, int n = -1)
        {
            CombatGroup result = null;

            if (type == t && (n < 0 || id == n))
                result = this;

            foreach (CombatGroup group in components)
            {
                result = group.FindGroup(t, n);
                if (result != null)
                    return result;
            }

            return result;
        }

        public CombatGroup Clone(bool deep = true)
        {
            CombatGroup clone = new CombatGroup(type, id, name, iff, enemy_intel);

            clone.combatant = combatant;
            clone.region = region;
            clone.location = location;
            clone.value = value;
            clone.expanded = expanded;

            for (int i = 0; i < units.Count; i++)
            {
                CombatUnit u = new CombatUnit(units[i]);
                u.SetCombatGroup(clone);
                clone.units.Add(u);
            }

            if (deep)
            {
                for (int i = 0; i < components.Count; i++)
                {
                    CombatGroup g = components[i].Clone(deep);
                    clone.AddComponent(g);

                    if (g.Type() == GROUP_TYPE.FIGHTER_SQUADRON ||
                            g.Type() == GROUP_TYPE.INTERCEPT_SQUADRON ||
                            g.Type() == GROUP_TYPE.ATTACK_SQUADRON ||
                            g.Type() == GROUP_TYPE.LCA_SQUADRON)
                    {

                        if (units.Count > 0)
                        {
                            CombatUnit carrier = units[0];

                            for (int u = 0; u < g.GetUnits().Count; u++)
                            {
                                CombatUnit unit = g.GetUnits()[u];

                                if (unit.Type() >= CLASSIFICATION.FIGHTER ||
                                        unit.Type() <= CLASSIFICATION.LCA)
                                {
                                    unit.SetCarrier(carrier);
                                    unit.SetRegion(carrier.GetRegion());
                                }
                            }
                        }
                    }
                }
            }

            return clone;
        }

        // accessors and mutators:
        public string GetDescription()
        {
            string desc;
            string name_desc;

            if (!string.IsNullOrEmpty(name))
                name_desc = string.Format("\"{0}\"", name);
            else
                name_desc = "";

            switch (type)
            {
                case GROUP_TYPE.FORCE:
                    desc = name; break;

                case GROUP_TYPE.FLEET: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.FLEET"), name_desc); break;
                case GROUP_TYPE.CARRIER_GROUP: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.CARRIER_GROUP"), name_desc); break;
                case GROUP_TYPE.BATTLE_GROUP: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.BATTLE_GROUP"), name_desc); break;
                case GROUP_TYPE.DESTROYER_SQUADRON: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.DESTROYER_SQUADRON"), name_desc); break;

                case GROUP_TYPE.WING: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.WING"), name_desc); break;
                case GROUP_TYPE.ATTACK_SQUADRON: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.ATTACK_SQUADRON"), name_desc); break;
                case GROUP_TYPE.FIGHTER_SQUADRON: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.FIGHTER_SQUADRON"), name_desc); break;
                case GROUP_TYPE.INTERCEPT_SQUADRON: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.INTERCEPT_SQUADRON"), name_desc); break;
                case GROUP_TYPE.LCA_SQUADRON: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.LCA_SQUADRON"), name_desc); break;

                case GROUP_TYPE.BATTALION: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.BATTALION"), name_desc); break;
                case GROUP_TYPE.STATION: desc = string.Format("{0} {1}{2}", ContentBundle.Instance.GetText("CombatGroup.STATION"), name); break;
                case GROUP_TYPE.STARBASE: desc = string.Format("{0} {1}{2}", ContentBundle.Instance.GetText("CombatGroup.STARBASE"), id, name_desc); break;
                case GROUP_TYPE.MINEFIELD: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.MINEFIELD"), name_desc); break;
                case GROUP_TYPE.BATTERY: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.BATTERY"), name_desc); break;
                case GROUP_TYPE.MISSILE: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.MISSILE"), name_desc); break;

                case GROUP_TYPE.C3I: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.C3I"), name_desc); break;
                case GROUP_TYPE.COMM_RELAY: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.COMM_RELAY"), name_desc); break;
                case GROUP_TYPE.EARLY_WARNING: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.EARLY_WARNING"), name_desc); break;
                case GROUP_TYPE.FWD_CONTROL_CTR: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.FWD_CONTROL_CTR"), name_desc); break;
                case GROUP_TYPE.ECM: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.ECM"), name_desc); break;

                case GROUP_TYPE.SUPPORT: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.SUPPORT"), name_desc); break;
                case GROUP_TYPE.COURIER: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.COURIER"), name_desc); break;
                case GROUP_TYPE.SUPPLY: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.SUPPLY"), name_desc); break;
                case GROUP_TYPE.REPAIR: desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.REPAIR"), name_desc); break;
                case GROUP_TYPE.MEDICAL:
                    desc = string.Format("{0} {1}{2}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.MEDICAL"), name_desc); break;

                case GROUP_TYPE.CIVILIAN:
                case GROUP_TYPE.WAR_PRODUCTION:
                case GROUP_TYPE.FACTORY:
                case GROUP_TYPE.REFINERY:
                case GROUP_TYPE.RESOURCE:
                    desc = name; break;

                case GROUP_TYPE.INFRASTRUCTURE:
                case GROUP_TYPE.TRANSPORT:
                case GROUP_TYPE.NETWORK:
                case GROUP_TYPE.HABITAT:
                case GROUP_TYPE.STORAGE:
                case GROUP_TYPE.FREIGHT:
                case GROUP_TYPE.PASSENGER:
                case GROUP_TYPE.PRIVATE:
                    desc = name; break;

                default: desc = string.Format("{0}{1}", ContentBundle.Instance.GetText("CombatGroup.default"), name_desc); break;
            }

            return desc;
        }
        public string GetShortDescription()
        {
            string desc;

            switch (type)
            {
                case GROUP_TYPE.FORCE: desc = name; break;

                case GROUP_TYPE.FLEET: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.FLEET")); break;
                case GROUP_TYPE.CARRIER_GROUP: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.CARRIER_GROUP")); break;
                case GROUP_TYPE.BATTLE_GROUP: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.BATTLE_GROUP")); break;
                case GROUP_TYPE.DESTROYER_SQUADRON: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.DESTROYER_SQUADRON")); break;

                case GROUP_TYPE.WING: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.WING")); break;
                case GROUP_TYPE.ATTACK_SQUADRON: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.ATTACK_SQUADRON")); break;
                case GROUP_TYPE.FIGHTER_SQUADRON: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.FIGHTER_SQUADRON")); break;
                case GROUP_TYPE.INTERCEPT_SQUADRON: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.INTERCEPT_SQUADRON")); break;
                case GROUP_TYPE.LCA_SQUADRON: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.LCA_SQUADRON")); break;

                case GROUP_TYPE.BATTALION: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.BATTALION")); break;
                case GROUP_TYPE.STATION: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.STATION")); break;
                case GROUP_TYPE.STARBASE: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.STARBASE")); break;
                case GROUP_TYPE.MINEFIELD: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.MINEFIELD")); break;
                case GROUP_TYPE.BATTERY: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.BATTERY")); break;

                case GROUP_TYPE.C3I: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.C3I")); break;
                case GROUP_TYPE.COMM_RELAY: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.COMM_RELAY")); break;
                case GROUP_TYPE.EARLY_WARNING: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.EARLY_WARNING")); break;
                case GROUP_TYPE.FWD_CONTROL_CTR: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.FWD_CONTROL_CTR")); break;
                case GROUP_TYPE.ECM: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.ECM")); break;

                case GROUP_TYPE.SUPPORT: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.SUPPORT")); break;
                case GROUP_TYPE.COURIER: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.COURIER")); break;
                case GROUP_TYPE.MEDICAL: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.MEDICAL")); break;
                case GROUP_TYPE.SUPPLY: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.SUPPLY")); break;
                case GROUP_TYPE.REPAIR: desc = string.Format("{0} {1}", GetOrdinal(), ContentBundle.Instance.GetText("CombatGroup.abrv.REPAIR")); break;

                case GROUP_TYPE.CIVILIAN:
                case GROUP_TYPE.WAR_PRODUCTION:
                case GROUP_TYPE.FACTORY:
                case GROUP_TYPE.REFINERY:
                case GROUP_TYPE.RESOURCE: desc = name; break;

                case GROUP_TYPE.INFRASTRUCTURE:
                case GROUP_TYPE.TRANSPORT:
                case GROUP_TYPE.NETWORK:
                case GROUP_TYPE.HABITAT:
                case GROUP_TYPE.STORAGE:
                case GROUP_TYPE.FREIGHT:
                case GROUP_TYPE.PASSENGER:
                case GROUP_TYPE.PRIVATE: desc = name; break;

                default: desc = string.Format("{0}", ContentBundle.Instance.GetText("CombatGroup.abrv.default")); break;
            }

            return desc;
        }

        public void SetCombatant(Combatant c) { combatant = c; }

        public Combatant GetCombatant() { return combatant; }
        public CombatGroup GetParent() { return parent; }
        public List<CombatGroup> GetComponents() { return components; }
        public List<CombatGroup> GetLiveComponents() { return live_comp; }
        public List<CombatUnit> GetUnits() { return units; }
        public CombatUnit GetRandomUnit()
        {
            CombatUnit result = null;
            List<CombatUnit> live = new List<CombatUnit>();

            foreach (CombatUnit unit in units)
            {
                if (unit.Count() - unit.DeadCount() > 0)
                    live.Add(unit);
            }

            if (live.Count > 0)
            {
                int ntries = 5;
                while (result == null && ntries-- > 0)
                {
                    int index = (int)RandomHelper.Random(0, live.Count);
                    result = live[index];

                    CLASSIFICATION ship_class = result.GetShipClass();
                    if (ship_class >= CLASSIFICATION.CRUISER &&
                            ship_class <= CLASSIFICATION.FARCASTER)
                        result = null;
                }
            }

            if (result == null)
            {
                foreach (CombatGroup comp in components)
                {
                    CombatUnit u = comp.GetRandomUnit();
                    if (u != null)
                        result = u;

                    if (result != null) return result;
                }
            }

            return result;
        }

        public CombatUnit GetFirstUnit()
        {
            int tmp_index = unit_index;
            unit_index = 0;
            CombatUnit result = GetNextUnit();
            unit_index = tmp_index;

            return result;
        }

        public CombatUnit GetNextUnit()
        {
            if (units.Count > 0)
            {
                List<CombatUnit> live = new List<CombatUnit>();

                foreach (CombatUnit unit in units)
                {
                    if (unit.Count() - unit.DeadCount() > 0)
                        live.Add(unit);
                }

                if (live.Count > 0)
                {
                    return live[unit_index++ % live.Count];
                }
            }

            if (components.Count > 0)
            {
                return components[unit_index % components.Count].GetNextUnit();
            }

            return null;
        }

        public CombatUnit FindUnit(string name)
        {
            if (units.Count > 0)
            {
                foreach (CombatUnit unit in units)
                {
                    if (unit.Name() == name)
                    {
                        if (unit.Count() - unit.DeadCount() > 0)
                            return unit;
                        else
                            return null;
                    }
                }
            }

            return null;
        }

        public CombatGroup FindCarrier()
        {
            CombatGroup p = GetParent();

            while (p != null &&
                    p.Type() != GROUP_TYPE.CARRIER_GROUP &&
                    p.Type() != GROUP_TYPE.STATION &&
                    p.Type() != GROUP_TYPE.STARBASE)
                p = p.GetParent();

            if (p != null && p.GetUnits().Count != 0)
                return p;

            return null;
        }


        public string Name() { return name; }
        public GROUP_TYPE Type() { return type; }
        public int CountUnits()
        {
            int n = 0;

            CombatGroup g = this;

            foreach (CombatUnit unit in g.units)
                n += unit.Count() - unit.DeadCount();

            this.live_comp.Clear();

            foreach (CombatGroup comp in g.components)
            {
                if (!comp.IsReserve())
                {
                    int unit_count = comp.CountUnits();
                    if (unit_count > 0)
                        this.live_comp.Add(comp);

                    n += unit_count;
                }
            }

            return n;
        }

        public Intel.INTEL_TYPE IntelLevel() { return enemy_intel; }
        public int GetID() { return id; }
        public int GetIFF() { return iff; }
        public Point Location() { return location; }
        public void MoveTo(Point loc)
        {
            location = loc;
        }

        public string GetRegion() { return region; }
        public void SetRegion(string rgn) { region = rgn; }
        public void AssignRegion(string rgn)
        {
            region = rgn;

            foreach (CombatGroup comp in components)
                comp.AssignRegion(rgn);

            foreach (CombatUnit unit in units)
                unit.SetRegion(rgn);
        }

        public int Value() { return value; }
        public int Sorties() { return sorties; }
        public void SetSorties(int n) { sorties = n; }
        public int Kills() { return kills; }
        public void SetKills(int n) { kills = n; }
        public int Points() { return points; }
        public void SetPoints(int n) { points = n; }
        public int UnitIndex() { return unit_index; }

        public double GetNextJumpTime()
        {
            double t = 0;

            foreach (CombatUnit unit in this.units)
                if (unit.GetNextJumpTime() > t)
                    t = unit.GetNextJumpTime();

            return t;
        }


        public double GetPlanValue() { return plan_value; }
        public void SetPlanValue(double v) { plan_value = v; }

        public bool IsAssignable()
        {
            switch (type)
            {
                case GROUP_TYPE.CARRIER_GROUP:
                case GROUP_TYPE.BATTLE_GROUP:
                case GROUP_TYPE.DESTROYER_SQUADRON:
                case GROUP_TYPE.ATTACK_SQUADRON:
                case GROUP_TYPE.FIGHTER_SQUADRON:
                case GROUP_TYPE.INTERCEPT_SQUADRON:
                case GROUP_TYPE.LCA_SQUADRON:
                    return this.CalcValue() > 0;
            }

            return false;
        }

        public bool IsTargetable()
        {
            // neutral / non-combatants are not *strategic* targets
            // for any combatant:
            if (iff < 1 || iff >= 100)
                return false;

            // civilian / non-combatant are not strategic targets:
            if (type == GROUP_TYPE.PASSENGER ||
                    type == GROUP_TYPE.PRIVATE ||
                    type == GROUP_TYPE.MEDICAL ||
                    type == GROUP_TYPE.HABITAT)
                return false;

            // must have units of our own to be targetable:
            if (units.Count < 1)
                return false;

            return this.CalcValue() > 0;
        }

        public bool IsDefensible()
        {
            if (type >= GROUP_TYPE.SUPPORT)
                return this.CalcValue() > 0;

            return false;
        }

        public bool IsStrikeTarget()
        {
            if (type < GROUP_TYPE.BATTALION ||
                    type == GROUP_TYPE.MINEFIELD ||   // assault, not strike
                    type == GROUP_TYPE.PASSENGER ||
                    type == GROUP_TYPE.PRIVATE ||
                    type == GROUP_TYPE.MEDICAL ||
                    type == GROUP_TYPE.HABITAT)
                return false;

            return this.CalcValue() > 0;
        }

        public bool IsMovable()
        {
            switch (type)
            {
                case GROUP_TYPE.CARRIER_GROUP:
                case GROUP_TYPE.BATTLE_GROUP:
                case GROUP_TYPE.DESTROYER_SQUADRON:
                case GROUP_TYPE.ATTACK_SQUADRON:
                case GROUP_TYPE.FIGHTER_SQUADRON:
                case GROUP_TYPE.INTERCEPT_SQUADRON:
                case GROUP_TYPE.LCA_SQUADRON:
                case GROUP_TYPE.COURIER:
                case GROUP_TYPE.MEDICAL:
                case GROUP_TYPE.SUPPLY:
                case GROUP_TYPE.REPAIR:
                case GROUP_TYPE.FREIGHT:
                case GROUP_TYPE.PASSENGER:
                case GROUP_TYPE.PRIVATE:
                    return true;
            }

            return false;
        }

        public bool IsFighterGroup()
        {
            switch (type)
            {
                case GROUP_TYPE.WING:
                case GROUP_TYPE.INTERCEPT_SQUADRON:
                case GROUP_TYPE.FIGHTER_SQUADRON:
                case GROUP_TYPE.ATTACK_SQUADRON:
                    return true;
            }

            return false;
        }

        public bool IsStarshipGroup()
        {
            switch (type)
            {
                case GROUP_TYPE.DESTROYER_SQUADRON:
                case GROUP_TYPE.BATTLE_GROUP:
                case GROUP_TYPE.CARRIER_GROUP:
                    return true;
            }

            return false;
        }

        public bool IsReserve()
        {
            if (enemy_intel <= Intel.INTEL_TYPE.RESERVE)
                return true;

            if (parent != null)
                return parent.IsReserve();

            return false;
        }


        // these two methods return zero terminated arrays of
        // integers identifying the preferred assets for attack
        // or defense in priority order:
        public static GROUP_TYPE[] PreferredAttacker(GROUP_TYPE t)
        {
            GROUP_TYPE[] p = new GROUP_TYPE[8];

            switch (t)
            {
                //case FLEET:
                case GROUP_TYPE.DESTROYER_SQUADRON:
                    p[0] = GROUP_TYPE.DESTROYER_SQUADRON;
                    p[1] = GROUP_TYPE.BATTLE_GROUP;
                    p[2] = GROUP_TYPE.CARRIER_GROUP;
                    p[3] = GROUP_TYPE.ATTACK_SQUADRON;
                    break;

                case GROUP_TYPE.BATTLE_GROUP:
                    p[0] = GROUP_TYPE.BATTLE_GROUP;
                    p[1] = GROUP_TYPE.DESTROYER_SQUADRON;
                    p[2] = GROUP_TYPE.CARRIER_GROUP;
                    p[3] = GROUP_TYPE.ATTACK_SQUADRON;
                    break;

                case GROUP_TYPE.CARRIER_GROUP:
                    p[0] = GROUP_TYPE.ATTACK_SQUADRON;
                    p[1] = GROUP_TYPE.BATTLE_GROUP;
                    p[2] = GROUP_TYPE.DESTROYER_SQUADRON;
                    p[3] = GROUP_TYPE.CARRIER_GROUP;
                    break;

                //case WING:
                case GROUP_TYPE.LCA_SQUADRON:
                case GROUP_TYPE.ATTACK_SQUADRON:
                case GROUP_TYPE.INTERCEPT_SQUADRON:
                case GROUP_TYPE.FIGHTER_SQUADRON:
                    p[0] = GROUP_TYPE.INTERCEPT_SQUADRON;
                    p[1] = GROUP_TYPE.FIGHTER_SQUADRON;
                    break;

                //case BATTALION:
                case GROUP_TYPE.STATION:
                    p[0] = GROUP_TYPE.BATTLE_GROUP;
                    p[1] = GROUP_TYPE.CARRIER_GROUP;
                    break;

                case GROUP_TYPE.STARBASE:
                case GROUP_TYPE.BATTERY:
                case GROUP_TYPE.MISSILE:
                    p[0] = GROUP_TYPE.ATTACK_SQUADRON;
                    p[1] = GROUP_TYPE.FIGHTER_SQUADRON;
                    break;

                //case C3I:
                case GROUP_TYPE.MINEFIELD:
                case GROUP_TYPE.COMM_RELAY:
                case GROUP_TYPE.EARLY_WARNING:
                case GROUP_TYPE.FWD_CONTROL_CTR:
                case GROUP_TYPE.ECM:
                    p[0] = GROUP_TYPE.ATTACK_SQUADRON;
                    p[1] = GROUP_TYPE.FIGHTER_SQUADRON;
                    p[2] = GROUP_TYPE.DESTROYER_SQUADRON;
                    break;

                //case SUPPORT:
                case GROUP_TYPE.COURIER:
                case GROUP_TYPE.MEDICAL:
                case GROUP_TYPE.SUPPLY:
                case GROUP_TYPE.REPAIR:
                    p[0] = GROUP_TYPE.DESTROYER_SQUADRON;
                    p[1] = GROUP_TYPE.BATTLE_GROUP;
                    p[2] = GROUP_TYPE.ATTACK_SQUADRON;
                    break;

                //case CIVILIAN:

                //case WAR_PRODuCTION:
                case GROUP_TYPE.FACTORY:
                case GROUP_TYPE.REFINERY:
                case GROUP_TYPE.RESOURCE:
                    p[0] = GROUP_TYPE.ATTACK_SQUADRON;
                    p[1] = GROUP_TYPE.FIGHTER_SQUADRON;
                    break;

                //case INFRASTRUCTURE:
                case GROUP_TYPE.TRANSPORT:
                case GROUP_TYPE.NETWORK:
                case GROUP_TYPE.HABITAT:
                case GROUP_TYPE.STORAGE:
                    p[0] = GROUP_TYPE.ATTACK_SQUADRON;
                    p[1] = GROUP_TYPE.FIGHTER_SQUADRON;
                    break;

                //case NON_COM:
                case GROUP_TYPE.FREIGHT:
                case GROUP_TYPE.PASSENGER:
                case GROUP_TYPE.PRIVATE:
                    p[0] = GROUP_TYPE.DESTROYER_SQUADRON;
                    p[1] = GROUP_TYPE.ATTACK_SQUADRON;
                    break;
            }

            return p;
        }
        public static GROUP_TYPE[] PreferredDefender(GROUP_TYPE t)
        {
            GROUP_TYPE[] p = new GROUP_TYPE[8];

            switch (t)
            {
                //case FLEET:
                case GROUP_TYPE.CARRIER_GROUP:
                case GROUP_TYPE.BATTLE_GROUP:
                case GROUP_TYPE.DESTROYER_SQUADRON:

                //case WING:
                case GROUP_TYPE.LCA_SQUADRON:
                case GROUP_TYPE.ATTACK_SQUADRON:
                case GROUP_TYPE.INTERCEPT_SQUADRON:
                case GROUP_TYPE.FIGHTER_SQUADRON: break;

                //case BATTALION:
                case GROUP_TYPE.STATION:
                    p[0] = GROUP_TYPE.BATTLE_GROUP;
                    p[1] = GROUP_TYPE.CARRIER_GROUP;
                    p[2] = GROUP_TYPE.DESTROYER_SQUADRON;
                    break;
                case GROUP_TYPE.STARBASE:
                case GROUP_TYPE.MINEFIELD:
                case GROUP_TYPE.BATTERY:
                case GROUP_TYPE.MISSILE:
                    p[0] = GROUP_TYPE.FIGHTER_SQUADRON;
                    p[1] = GROUP_TYPE.INTERCEPT_SQUADRON;
                    break;

                //case C3I:
                case GROUP_TYPE.COMM_RELAY:
                case GROUP_TYPE.EARLY_WARNING:
                case GROUP_TYPE.FWD_CONTROL_CTR:
                case GROUP_TYPE.ECM:
                    p[0] = GROUP_TYPE.FIGHTER_SQUADRON;
                    p[1] = GROUP_TYPE.INTERCEPT_SQUADRON;
                    break;

                //case SUPPORT:
                case GROUP_TYPE.COURIER:
                case GROUP_TYPE.MEDICAL:
                case GROUP_TYPE.SUPPLY:
                case GROUP_TYPE.REPAIR:
                    p[0] = GROUP_TYPE.DESTROYER_SQUADRON;
                    p[1] = GROUP_TYPE.BATTLE_GROUP;
                    p[2] = GROUP_TYPE.ATTACK_SQUADRON;
                    break;

                //case CIVILIAN:

                //case WAR_PRODuCTION:
                case GROUP_TYPE.FACTORY:
                case GROUP_TYPE.REFINERY:
                case GROUP_TYPE.RESOURCE:
                    p[0] = GROUP_TYPE.FIGHTER_SQUADRON;
                    p[1] = GROUP_TYPE.INTERCEPT_SQUADRON;
                    break;

                //case INFRASTRUCTURE:
                case GROUP_TYPE.TRANSPORT:
                case GROUP_TYPE.NETWORK:
                case GROUP_TYPE.HABITAT:
                case GROUP_TYPE.STORAGE:
                    p[0] = GROUP_TYPE.FIGHTER_SQUADRON;
                    p[1] = GROUP_TYPE.INTERCEPT_SQUADRON;
                    break;

                //case NON_COM:
                case GROUP_TYPE.FREIGHT:
                case GROUP_TYPE.PASSENGER:
                case GROUP_TYPE.PRIVATE:
                    p[0] = GROUP_TYPE.DESTROYER_SQUADRON;
                    p[1] = GROUP_TYPE.BATTLE_GROUP;
                    break;
            }

            return p;
        }


        public bool IsExpanded() { return expanded; }
        public void SetExpanded(bool e) { expanded = e; }

        public string GetAssignedSystem() { return assigned_system; }

        public void SetAssignedSystem(string s)
        {
            assigned_system = s;
            assigned_zone = null;
            zone_lock = false;

            foreach (CombatGroup g in components)
            {
                g.SetAssignedSystem(s);
            }
        }

        public CombatZone GetCurrentZone() { return current_zone; }
        public void SetCurrentZone(CombatZone z) { current_zone = z; }
        public CombatZone GetAssignedZone() { return assigned_zone; }
        public void SetAssignedZone(CombatZone z)
        {
            assigned_zone = z;

            if (assigned_zone == null)
                zone_lock = false;

            foreach (CombatGroup g in components)
            {
                g.SetAssignedZone(z);
            }
        }

        public void ClearUnlockedZones()
        {
            if (!zone_lock)
                assigned_zone = null;

            foreach (CombatGroup g in components)
            {
                g.ClearUnlockedZones();
            }
        }

        public bool IsZoneLocked() { return assigned_zone != null && zone_lock; }
        public void SetZoneLock(bool lock_ = true)
        {
            if (assigned_zone == null)
                zone_lock = false;
            else
                zone_lock = lock_;

            if (zone_lock)
                assigned_system = "";

            foreach (CombatGroup g in components)
            {
                g.SetZoneLock(lock_);
            }
        }

        public bool IsSystemLocked() { return assigned_system.Length > 0; }

        public string GetStrategicDirection() { return strategic_direction; }
        public void SetStrategicDirection(string dir) { strategic_direction = dir; }

        public void SetIntelLevel(Intel.INTEL_TYPE n)
        {
            if (n < Intel.INTEL_TYPE.RESERVE || n > Intel.INTEL_TYPE.TRACKED) return;

            enemy_intel = n;

            // if this group has been discovered, the entire
            // branch of the OOB tree must be exposed.  Otherwise,
            // no missions would ever be planned against this
            // combat group.

            if (n > Intel.INTEL_TYPE.SECRET)
            {
                CombatGroup p = parent;
                while (p != null)
                {
                    if (p.enemy_intel < Intel.INTEL_TYPE.KNOWN)
                        p.enemy_intel = Intel.INTEL_TYPE.KNOWN;

                    p = p.parent;
                }
            }
        }


        public int CalcValue()
        {
            int val = 0;

            foreach (CombatUnit unit in units)
                val += unit.GetValue();

            foreach (CombatGroup comp in components)
                val += comp.CalcValue();

            value = val;
            return value;
        }


        public List<CombatAssignment> GetAssignments() { return assignments; }
        public void ClearAssignments()
        {
            assignments.Clear();

            foreach (CombatGroup comp in components)
                comp.ClearAssignments();
        }


        public static GROUP_TYPE TypeFromName(string type_name)
        {
            for (int i = (int)GROUP_TYPE.FORCE; i < (int)GROUP_TYPE.PRIVATE; i++)
                if (type_name == group_name[i])
                    return (GROUP_TYPE)i;

            return (GROUP_TYPE)0;
        }
        public static GROUP_TYPE GroupTypeFromName(string name)
        {
            GROUP_TYPE rst;
            if (EnumExtensions.TryParse<GROUP_TYPE>(name, true, out rst))
                return rst;
            else
                return (GROUP_TYPE)0;
        }

        public static string NameFromType(int type)
        {
            return group_name[type];
        }

        private CLASSIFICATION ShipClassFromName(string type_name)
        {
            return Ship.ClassForName(type_name);
        }

        private string GetOrdinal()
        {
            string ordinal;

            int last_two_digits = id % 100;

            if (last_two_digits > 10 && last_two_digits < 20)
            {
                ordinal = string.Format("ordinal.{0}", last_two_digits);
                string suffix = ContentBundle.Instance.GetText(ordinal);

                if (suffix != ordinal)
                    ordinal = string.Format("{0}{1}", id, suffix);
                else
                    ordinal = string.Format("{0}th", id);
            }
            else
            {
                int last_digit = last_two_digits % 10;
                ordinal = string.Format("ordinal.{0}", last_digit);
                string suffix = ContentBundle.Instance.GetText(ordinal);
                if (suffix != ordinal)
                    ordinal = string.Format("{0}{1}", id, suffix);
                else if (last_digit == 1)
                    ordinal = string.Format("{0}st", id);
                else if (last_digit == 2)
                    ordinal = string.Format("{0}nd", id);
                else if (last_digit == 3)
                    ordinal = string.Format("{0}rd", id);
                else
                    ordinal = string.Format("{0}th", id);
            }

            return ordinal;
        }


        // attributes:
        private GROUP_TYPE type;
        private int id;
        private string name;
        private int iff;
        private Intel.INTEL_TYPE enemy_intel;

        private double plan_value; // scratch pad for plan modules

        private List<CombatUnit> units = new List<CombatUnit>();
        private List<CombatGroup> components = new List<CombatGroup>();
        private List<CombatGroup> live_comp = new List<CombatGroup>();
        private Combatant combatant;
        private CombatGroup parent;
        internal string region;
        internal Point location;
        private int value;
        internal int unit_index;

        private int sorties;
        private int kills;
        private int points;

        private bool expanded;   // for tree control

        private string assigned_system;
        private CombatZone current_zone;
        private CombatZone assigned_zone;
        private bool zone_lock;
        private List<CombatAssignment> assignments;

        private string strategic_direction;

        private static readonly string[] group_name = new string[] {
                "",
                "force",
                "wing",
                "intercept_squadron",
                "fighter_squadron",
                "attack_squadron",
                "lca_squadron",
                "fleet",
                "destroyer_squadron",
                "battle_group",
                "carrier_group",
                "battalion",
                "minefield",
                "battery",
                "missile",
                "station",
                "starbase",
                "c3i",
                "comm_relay",
                "early_warning",
                "fwd_control_ctr",
                "ecm",
                "support",
                "courier",
                "medical",
                "supply",
                "repair",
                "civilian",
                "war_production",
                "factory",
                "refinery",
                "resource",
                "infrastructure",
                "transport",
                "network",
                "habitat",
                "storage",
                "non_com",
                "freight",
                "passenger",
                "private"
            };
    }


    [Serializable]
    public class CombatGroups
    {
        public List<CombatGroupInfo> groups;
    }

    [Serializable]
    public class CombatGroupInfo
    {
        public string name;
        public string type;
        public string intel;
        public string region;
        public string system;
        public Point loc;
        public string parent_type;
        public string parent_id;
        public string iff;
        public string id;
        public string unit_index;

        public List<CombatUnitInfo> units;

        // User-defined conversion from CombatGroupInfo to CombatGroup
        public static implicit operator CombatGroup(CombatGroupInfo cgi)
        {
            throw new System.NotImplementedException();
        }

        //  User-defined conversion from CombatGroup to CombatGroupInfo
        public static implicit operator CombatGroupInfo(CombatGroup cg)
        {
            throw new NotImplementedException();
        }

    }

}
