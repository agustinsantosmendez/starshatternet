﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      JoyDlg.h/JoyDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Navigation Active Window class
*/
using System;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Forms;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class JoyDlg : FormWindow
    {
        public JoyDlg(Screen s, Camera c, FormDef def, BaseScreen mgr, GameObject p) :
                base(s, 0, 0, s.Width(), s.Height(), c, p, "JoyDlg")
        {
            manager = mgr;
            apply = null; cancel = null; message = null;

            Init(def);
        }
        //public virtual ~JoyDlg();
        public override void RegisterControls()
        {
            if (apply != null)
                return;

            for (int i = 0; i < 4; i++)
            {
                axis_button[i] = (Button)FindControl(201 + i);
                invert_checkbox[i] = (Button)FindControl(301 + i);

                if (axis_button[i] != null)
                    REGISTER_CLIENT(ETD.EID_CLICK, axis_button[i], OnAxis);
            }

            message = FindControl(11);

            apply = (Button)FindControl(1);
            REGISTER_CLIENT(ETD.EID_CLICK, apply, OnApply);

            cancel = (Button)FindControl(2);
            REGISTER_CLIENT(ETD.EID_CLICK, cancel, OnCancel);
        }
        public override void Show()
        {
            base.Show();

            for (int i = 0; i < 4; i++)
            {
                Button b = axis_button[i];
                if (b != null)
                {
                    int map = Joystick.GetAxisMap(i) - KeyMap.KEY_JOY_AXIS_X;
                    int inv = Joystick.GetAxisInv(i);

                    if (map >= 0 && map < 8)
                    {
                        b.SetText(Game.GetText(joy_axis_names[map]));
                        map_axis[i] = map;
                    }
                    else
                    {
                        b.SetText(Game.GetText("JoyDlg.unmapped"));
                    }

                    b.SetButtonState(0);

                    invert_checkbox[i].SetButtonState(inv != 0 ? (short)1 : (short)0);
                }
            }

            //TODO SetFocus();
        }
        // Operations:
        public virtual void ExecFrame()
        {
            if (selected_axis >= 0 && selected_axis < 4)
            {
                Joystick joystick = Joystick.GetInstance();
                if (joystick != null)
                {
                    joystick.Acquire();

                    int delta = 1000;

                    for (int i = 0; i < 8; i++)
                    {
                        int a = Joystick.ReadRawAxis(i + KeyMap.KEY_JOY_AXIS_X);

                        int d = a - samples[i];
                        if (d < 0) d = -d;

                        if (d > delta && samples[i] < 1e6)
                        {
                            delta = d;
                            sample_axis = i;
                        }

                        samples[i] = a;
                    }

                    Button b = axis_button[selected_axis];

                    if (sample_axis >= 0)
                    {
                        b.SetText(Game.GetText(joy_axis_names[sample_axis]));
                        map_axis[selected_axis] = sample_axis;
                    }

                    else
                        b.SetText(Game.GetText("JoyDlg.select"));
                }
            }
        }

        public virtual void OnApply(ActiveWindow obj, AWEvent evnt)
        {
            Starshatter stars = Starshatter.GetInstance();

            if (stars != null)
            {
                KeyMap keymap = stars.GetKeyMap();

                keymap.Bind(KeyMap.KEY_AXIS_YAW, map_axis[0] + KeyMap.KEY_JOY_AXIS_X, 0);
                keymap.Bind(KeyMap.KEY_AXIS_PITCH, map_axis[1] + KeyMap.KEY_JOY_AXIS_X, 0);
                keymap.Bind(KeyMap.KEY_AXIS_ROLL, map_axis[2] + KeyMap.KEY_JOY_AXIS_X, 0);
                keymap.Bind(KeyMap.KEY_AXIS_THROTTLE, map_axis[3] + KeyMap.KEY_JOY_AXIS_X, 0);

                keymap.Bind(KeyMap.KEY_AXIS_YAW_INVERT, invert_checkbox[0].GetButtonState(), 0);
                keymap.Bind(KeyMap.KEY_AXIS_PITCH_INVERT, invert_checkbox[1].GetButtonState(), 0);
                keymap.Bind(KeyMap.KEY_AXIS_ROLL_INVERT, invert_checkbox[2].GetButtonState(), 0);
                keymap.Bind(KeyMap.KEY_AXIS_THROTTLE_INVERT, invert_checkbox[3].GetButtonState(), 0);

                keymap.SaveKeyMap("key.cfg", 256);

                stars.MapKeys();
            }

            if (manager != null)
                manager.ShowCtlDlg();
        }
        public virtual void OnCancel(ActiveWindow obj, AWEvent evnt)
        {
            if (manager != null)
                manager.ShowCtlDlg();
        }

        public virtual void OnAxis(ActiveWindow obj, AWEvent evnt)
        {
            for (int i = 0; i < 4; i++)
            {
                int map = map_axis[i];
                string name = Game.GetText("JoyDlg.unmapped");
                Button b = axis_button[i];

                if (map >= 0 && map < 8)
                    name = Game.GetText(joy_axis_names[map]);

                if (b != null)
                {
                    if (b == evnt.window)
                    {
                        if (selected_axis == i)
                        {
                            b.SetText(name);
                            b.SetButtonState(0);
                            selected_axis = -1;
                        }
                        else
                        {
                            b.SetText(Game.GetText("JoyDlg.select"));
                            b.SetButtonState(1);
                            selected_axis = i;
                        }
                    }
                    else
                    {
                        b.SetText(name);
                        b.SetButtonState(0);
                    }
                }
            }

            for (int i = 0; i < 8; i++)
            {
                samples[i] = 10000000;
            }
        }

        protected BaseScreen manager;

        protected ActiveWindow message;
        protected Button[] axis_button = new Button[4];
        protected Button[] invert_checkbox = new Button[4];

        protected Button apply;
        protected Button cancel;

        private static string[] joy_axis_names = {
                    "JoyDlg.axis.0",
                    "JoyDlg.axis.1",
                    "JoyDlg.axis.2",
                    "JoyDlg.axis.3",
                    "JoyDlg.axis.4",
                    "JoyDlg.axis.5",
                    "JoyDlg.axis.6",
                    "JoyDlg.axis.7"
                };

        private static int selected_axis = -1;
        private static int sample_axis = -1;
        private static int[] samples = new int[8];

        private static int[] map_axis = new int[4];
    }
}