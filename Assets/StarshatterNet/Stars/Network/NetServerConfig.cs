﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetServerConfig.h/NetServerConfig.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
  */
using System;
using System.Collections.Generic;
using WORD = System.Int32;

namespace StarshatterNet.Stars.Network
{
    public class NetServerConfig
    {
        public NetServerConfig()
        {
            instance = this;

            name = "Starshatter ";
            admin_name = "system";
            admin_pass = "manager";
            admin_port = 11111;
            lobby_port = 11100;
            game_port = 11101;
            game_type = GAME_TYPE.NET_GAME_PUBLIC;
            auth_level = NetAuth.AUTH_LEVEL.NET_AUTH_STANDARD;
            poolsize = 8;
            session_timeout = 300;

            name += Starshatter.versionInfo;

            Load();
        }
        //~NetServerConfig();

        public enum GAME_TYPE
        {
            NET_GAME_LAN,
            NET_GAME_PRIVATE,
            NET_GAME_PUBLIC
        };

        public string Name() { return name; }
        public string GetAdminName() { return admin_name; }
        public string GetAdminPass() { return admin_pass; }
        public string GetGamePass() { return game_pass; }
        public string GetMission() { return mission; }
        public WORD GetAdminPort() { return admin_port; }
        public WORD GetLobbyPort() { return lobby_port; }
        public WORD GetGamePort() { return game_port; }
        public int GetPoolsize() { return poolsize; }
        public int GetSessionTimeout() { return session_timeout; }
        public GAME_TYPE GetGameType() { return game_type; }
        public NetAuth.AUTH_LEVEL GetAuthLevel() { return auth_level; }

        public void SetName(string s) { name = Clean(s); }
        public void SetAdminName(string s) { admin_name = Clean(s); }
        public void SetAdminPass(string s) { admin_pass = Clean(s); }
        public void SetGamePass(string s) { game_pass = Clean(s); }
        public void SetMission(string s) { mission = Clean(s); }
        public void SetGameType(GAME_TYPE t) { game_type = t; }
        public void SetAdminPort(WORD p) { admin_port = p; }
        public void SetLobbyPort(WORD p) { lobby_port = p; }
        public void SetGamePort(WORD p) { game_port = p; }
        public void SetPoolsize(int s) { poolsize = s; }
        public void SetSessionTimeout(int t) { session_timeout = t; }
        public void SetAuthLevel(NetAuth.AUTH_LEVEL n) { auth_level = n; }

        public void Load() { throw new NotImplementedException(); }
        public void Save() { throw new NotImplementedException(); }

        public bool IsUserBanned(NetUser user) { throw new NotImplementedException(); }
        public void BanUser(NetUser user) { throw new NotImplementedException(); }

        public static void Initialize() { throw new NotImplementedException(); }
        public static void Close() { throw new NotImplementedException(); }
        public static NetServerConfig GetInstance() { return instance; }


        private void LoadBanList() { throw new NotImplementedException(); }
        private string Clean(string s) { throw new NotImplementedException(); }

        private string name;
        private string admin_name;
        private string admin_pass;
        private string game_pass;
        private string mission;

        private WORD admin_port;
        private WORD lobby_port;
        private WORD game_port;
        private int poolsize;
        private int session_timeout;
        private GAME_TYPE game_type;
        private NetAuth.AUTH_LEVEL auth_level;

        private List<NetAddr> banned_addrs;
        private List<string> banned_names;

        private static NetServerConfig instance = null;
     }
}