﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      SimEvent.h/SimEvent.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Universe and Region classes
 */
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Stars.Simulator
{
    public class SimEvent
    {
        public enum EVENT
        {
            LAUNCH = 1, DOCK, LAND, EJECT, CRASH, COLLIDE, DESTROYED,
            MAKE_ORBIT, BREAK_ORBIT, QUANTUM_JUMP,
            LAUNCH_SHIP, RECOVER_SHIP,
            FIRE_GUNS, FIRE_MISSILE, DROP_DECOY,
            GUNS_KILL, MISSILE_KILL,
            LAUNCH_PROBE, SCAN_TARGET
        };

        public SimEvent(EVENT e, string tgt = null, string info = null)
        {
            evnt = e;
            count = 0;
            Sim sim = Sim.GetSim();
            if (sim != null)
            {
                time = (int)sim.MissionClock();
            }
            else
            {
                time = (int)(Game.GameTime() / 1000);
            }

            SetTarget(tgt);
            SetInfo(info);
        }

        // ~SimEvent();

        public EVENT GetEvent() { return evnt; }
        public int GetTime() { return time; }
        public string GetEventDesc()
        {
            switch (evnt)
            {
                case EVENT.LAUNCH: return localeManager.GetText("sim.event.Launch");
                case EVENT.DOCK: return localeManager.GetText("sim.event.Dock");
                case EVENT.LAND: return localeManager.GetText("sim.event.Land");
                case EVENT.EJECT: return localeManager.GetText("sim.event.Eject");
                case EVENT.CRASH: return localeManager.GetText("sim.event.Crash");
                case EVENT.COLLIDE: return localeManager.GetText("sim.event.Collision With");
                case EVENT.DESTROYED: return localeManager.GetText("sim.event.Destroyed By");
                case EVENT.MAKE_ORBIT: return localeManager.GetText("sim.event.Make Orbit");
                case EVENT.BREAK_ORBIT: return localeManager.GetText("sim.event.Break Orbit");
                case EVENT.QUANTUM_JUMP: return localeManager.GetText("sim.event.Quantum Jump");
                case EVENT.LAUNCH_SHIP: return localeManager.GetText("sim.event.Launch Ship");
                case EVENT.RECOVER_SHIP: return localeManager.GetText("sim.event.Recover Ship");
                case EVENT.FIRE_GUNS: return localeManager.GetText("sim.event.Fire Guns");
                case EVENT.FIRE_MISSILE: return localeManager.GetText("sim.event.Fire Missile");
                case EVENT.DROP_DECOY: return localeManager.GetText("sim.event.Drop Decoy");
                case EVENT.GUNS_KILL: return localeManager.GetText("sim.event.Guns Kill");
                case EVENT.MISSILE_KILL: return localeManager.GetText("sim.event.Missile Kill");
                case EVENT.LAUNCH_PROBE: return localeManager.GetText("sim.event.Launch Probe");
                case EVENT.SCAN_TARGET: return localeManager.GetText("sim.event.Scan Target");
                default: return localeManager.GetText("sim.event.no event");
            }
        }
        public string GetTarget() { return target; }
        public string GetInfo() { return info; }
        public int GetCount() { return count; }

        public void SetTarget(string tgt)
        {
            if (!string.IsNullOrWhiteSpace(tgt))
                target = tgt;
        }

        public void SetInfo(string info)
        {
            if (!string.IsNullOrWhiteSpace(info))
                this.info = info;
        }

        public void SetCount(int count)
        {
            this.count = count;
        }

        public void SetTime(int time)
        {
            this.time = time;
        }

        private EVENT evnt;
        private int time;
        private string target;
        private string info;
        private int count;
        protected ContentBundle localeManager;
    }
}
