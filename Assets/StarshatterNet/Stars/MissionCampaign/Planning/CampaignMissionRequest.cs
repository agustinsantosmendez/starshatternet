﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CampaignMissionRequest.h/CampaignMissionRequest.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CampaignMissionRequest
*/
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign.Planning
{
#warning CampaignMissionRequest class is still in development and is not recommended for production.
    public class CampaignMissionRequest
    {
        public CampaignMissionRequest(Campaign c, Mission.TYPE type, int start, CombatGroup primary, CombatGroup tgt = null)
        {
            campaign = c;
            this.type = type;
            opp_type = -1;
            this.start = start;
            primary_group = primary;
            secondary_group = null;
            objective = tgt;
            use_loc = false;
        }

        public Campaign GetCampaign() { return campaign; }
        public Mission.TYPE Type() { return type; }
        public int OpposingType() { return opp_type; }
        public int StartTime() { return start; }
        public CombatGroup GetPrimaryGroup() { return primary_group; }
        public CombatGroup GetSecondaryGroup() { return secondary_group; }
        public CombatGroup GetObjective() { return objective; }

        public bool IsLocSpecified() { return use_loc; }
        public string RegionName() { return region; }
        public Point Location() { return location; }
        public string Script() { return script; }

        public void SetType(Mission.TYPE t) { type = t; }
        public void SetOpposingType(int t) { opp_type = t; }
        public void SetStartTime(int s) { start = s; }
        public void SetPrimaryGroup(CombatGroup g) { primary_group = g; }
        public void SetSecondaryGroup(CombatGroup g) { secondary_group = g; }
        public void SetObjective(CombatGroup g) { objective = g; }

        public void SetRegionName(string rgn) { region = rgn; use_loc = true; }
        public void SetLocation(Point loc) { location = loc; use_loc = true; }
        public void SetScript(string s) { script = s; }


        private Campaign campaign;

        private Mission.TYPE type;    // type of mission
        private int opp_type;         // opposing mission type
        private int start;            // start time
        private CombatGroup primary_group;    // player's group
        private CombatGroup secondary_group;  // optional support group
        private CombatGroup objective;        // target or ward

        private bool use_loc;          // use the specified location
        private string region;
        private Point location;
        private string script;
    }
}
