﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MenuDlg.h/MenuDlg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Main Menu Dialog Active Window class
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Stars.MissionCampaign;
using UnityEngine;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Stars.Dialogs
{
    public class MenuDlg : FormWindow
    {
        public MenuDlg(Screen s, Camera c, FormDef def, MenuScreen mgr, GameObject p) :
            base(s, 0, 0, s.Width(), s.Height(), c, p, "MenuDlg")
        {
            manager = mgr; btn_start = null;
            btn_campaign = null; btn_mission = null; btn_player = null; btn_multi = null;
            btn_mod = null; btn_tac = null;
            btn_options = null; btn_controls = null; btn_quit = null;
            version = null; description = null; stars = null; campaign = null;

            stars = Starshatter.GetInstance();
            campaign = Campaign.GetCampaign();

            Init(def);
        }

        public override void RegisterControls()
        {
            if (btn_start != null)
                return;

            btn_start = (Button)FindControl(120);
            btn_campaign = (Button)FindControl(101);
            btn_mission = (Button)FindControl(102);
            btn_player = (Button)FindControl(103);
            btn_multi = (Button)FindControl(104);
            btn_video = (Button)FindControl(111);
            btn_options = (Button)FindControl(112);
            btn_controls = (Button)FindControl(113);
            btn_quit = (Button)FindControl(114);
            btn_mod = (Button)FindControl(115);
            btn_tac = (Button)FindControl(116);

            if (btn_start != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_start, OnStart);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_start, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_start, OnButtonExit);
            }

            if (btn_campaign != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_campaign, OnCampaign);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_campaign, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_campaign, OnButtonExit);
            }

            if (btn_mission != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_mission, OnMission);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_mission, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_mission, OnButtonExit);
            }

            if (btn_player != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_player, OnPlayer);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_player, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_player, OnButtonExit);
            }

            if (btn_multi != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_multi, OnMultiplayer);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_multi, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_multi, OnButtonExit);
            }

            if (btn_video != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_video, OnVideo);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_video, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_video, OnButtonExit);
            }

            if (btn_options != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_options, OnOptions);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_options, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_options, OnButtonExit);
            }

            if (btn_controls != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_controls, OnControls);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_controls, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_controls, OnButtonExit);
            }

            if (btn_mod != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_mod, OnMod);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_mod, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_mod, OnButtonExit);
            }

            if (btn_tac != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_tac, OnTacReference);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_tac, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_tac, OnButtonExit);
            }

            if (btn_quit != null)
            {
                REGISTER_CLIENT(ETD.EID_CLICK, btn_quit, OnQuit);
                REGISTER_CLIENT(ETD.EID_MOUSE_ENTER, btn_quit, OnButtonEnter);
                REGISTER_CLIENT(ETD.EID_MOUSE_EXIT, btn_quit, OnButtonExit);
            }

            version = FindControl(100);

            if (version != null)
            {
                version.SetText(Starshatter.versionInfo);
            }

            description = FindControl(202);
        }
        public override void Show()
        {
            base.Show();

            if (btn_multi != null && Starshatter.UseFileSystem())
                btn_multi.SetEnabled(false);
        }
        public virtual void ExecFrame() { }

        // Operations:
        public virtual void OnStart(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            stars.StartOrResumeGame();
        }
        public virtual void OnCampaign(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            manager.ShowCmpSelectDlg();
        }
        public virtual void OnMission(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            manager.ShowMsnSelectDlg();
        }
        public virtual void OnPlayer(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            manager.ShowPlayerDlg();
        }
        public virtual void OnMultiplayer(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            manager.ShowNetClientDlg();
        }
        public virtual void OnMod(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            manager.ShowModDlg();
        }
        public virtual void OnTacReference(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            stars.OpenTacticalReference();
        }

        public virtual void OnVideo(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            manager.ShowVidDlg();
        }
        public virtual void OnOptions(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            manager.ShowOptDlg();
        }
        public virtual void OnControls(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            manager.ShowCtlDlg();
        }
        public virtual void OnQuit(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null) description.SetText("");
            manager.ShowExitDlg();
        }

        public virtual void OnButtonEnter(ActiveWindow obj, AWEvent evnt)
        {
            ActiveWindow src = evnt.window;

            if (src != null && description != null)
                description.SetText(src.GetAltText());
        }
        public virtual void OnButtonExit(ActiveWindow obj, AWEvent evnt)
        {
            if (description != null)
                description.SetText("");
        }

        protected MenuScreen manager;

        protected Button btn_start;
        protected Button btn_campaign;
        protected Button btn_mission;
        protected Button btn_player;
        protected Button btn_multi;
        protected Button btn_mod;
        protected Button btn_tac;

        protected Button btn_video;
        protected Button btn_options;
        protected Button btn_controls;
        protected Button btn_quit;

        protected ActiveWindow version;
        protected ActiveWindow description;

        protected Starshatter stars;
        protected Campaign campaign;
    }
}
