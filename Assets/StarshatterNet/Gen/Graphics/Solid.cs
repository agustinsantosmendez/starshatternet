﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Solid.h/Solid.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Classes for rendering solid meshes of polygons
*/
using System;
using System.Collections.Generic;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Gen.Graphics
{
    public class Solid : Graphic
    {
        public static bool IsCollisionEnabled() { return use_collision_detection; }
        public static void EnableCollision(bool enable) { use_collision_detection = enable; }

        public Solid(string name = null, bool createGameObjet = true) : base(name, createGameObjet)
        {
            model = null; own_model = true; roll = 0.0f; pitch = 0.0f; yaw = 0.0f;

            shadow = true;
            if (name == null)
                this.name = string.Format("Solid {0}", id);
        }
        //public virtual ~Solid();

        // operations
        //public virtual void Render(Video* video, DWORD flags);
        //public virtual void SelectDetail(Projector p) { throw new NotImplementedException(); }
        //public virtual void ProjectScreenRect(Projector p) { throw new NotImplementedException(); }
        public override void Update() { }

        // accessors / mutators
        public Model GetModel() { return model; }
        public void GetAllTextures(List<Bitmap> textures) { throw new NotImplementedException(); }

        public virtual bool IsDynamic() { throw new NotImplementedException(); }
        public virtual void SetDynamic(bool d) { throw new NotImplementedException(); }
        public override void SetLuminous(bool l)
        {
            luminous = l;

            if (model != null && luminous)
            {
                ErrLogger.PrintLine("Solid.SetLuminous is not implemented.");
            }
        }
        public override void SetOrientation(Quaternion d)
        { throw new NotImplementedException(); }
        public override void SetOrientation(Matrix33D o)
        {
            orientation = o; throw new NotImplementedException();
        }
        public virtual void SetOrientation(Solid match)
        {
            if (model == null || infinite)
                return;

            // copy the orientation matrix from the solid we are matching:
            orientation = match.Orientation(); throw new NotImplementedException();
        }
        public Matrix33D Orientation() { return orientation; }
        public float Roll() { return roll; }
        public float Pitch() { return pitch; }
        public float Yaw() { return yaw; }
        public override bool IsSolid() { return true; }

        // stencil shadows
        public virtual void CreateShadows(int nlights = 1) { throw new NotImplementedException(); }
        public virtual void UpdateShadows(List<LightNode> lights) { throw new NotImplementedException(); }
        //public List<Shadow> GetShadows() { return shadows; }


        public bool Load(string mag_file, double scale = 1.0)
        {
            // get ready to load, delete existing model:
            ClearModel();

            // loading our own copy, so we own the model:
            model = new Model();
            own_model = true;

            // now load the model:
            if (model.Load(mag_file, scale))
            {
                gameObj = (GameObject)GameObject.Instantiate(model.ModelObj, Vector3.zero, Quaternion.identity);

                radius = (float)model.Radius();
                gameObj.transform.localScale = new Vector3(radius, radius, radius);
                name = model.Name();
                return true;
            }

            // load failed:
            ClearModel();
            return false;
        }
        //public bool Load(ModelFile loader, double scale = 1.0) { throw new NotImplementedException(); }
        public void UseModel(Model m)
        {
            // get rid of the existing model:
            ClearModel();

            // point to the new model:
            own_model = false;
            model = m;
            radius = (float)m.Radius();
        }
        public void ClearModel()
        {
            if (own_model && model != null)
            {
                //delete model;
                model = null;
            }
            if (gameObj != null)
            {
                GameObject.Destroy(gameObj);
                gameObj = null;
            }

            radius = 0.0f;
        }
        public bool Rescale(double scale)
        {
            if (!own_model || gameObj == null)
                return false;

            radius *= (float)scale;

            gameObj.transform.localScale = new Vector3((float)scale, (float)scale, (float)scale);
            //radius = gameObj.transform.GetComponent<SphereCollider>().radius;
            Debug.Log("Cube Rescale radius: " + radius);
            return true;
        }

        // collision detection
        public override bool CollidesWith(Graphic o) { throw new NotImplementedException(); }
        public override bool CheckRayIntersection(Point pt, Point vpn, double len, Point ipt,
                                                  bool treat_translucent_polys_as_solid = true)
        { throw new NotImplementedException(); }

        //public virtual Poly* GetIntersectionPoly()   { return intersection_poly; }

        // buffer management
        //public virtual void DeletePrivateData();
        //public virtual void InvalidateSurfaceData();
        //public virtual void InvalidateSegmentData();


        protected Model model;
        protected bool own_model;

        protected float roll, pitch, yaw;
        protected Matrix33D orientation;
        //protected Poly* intersection_poly;

        //protected List<Shadow> shadows;
        protected MeshFilter meshFilter;
        private static bool use_collision_detection = true;
    }
}