﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx.lib
    ORIGINAL FILE:      ContentBundle.h/ContentBundle.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Chained collection of localized strings
*/
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;

namespace StarshatterNet.Gen.Misc
{
    public class ContentBundle
    {
        public static ContentBundle Instance = new ContentBundle("content", CultureInfo.CurrentCulture);
        public ContentBundle(string bundle, CultureInfo locale)
        {
            Stream file = FindFile(bundle, locale);
            if (file != null)
            {
                LoadBundle(file);
            }
        }

        //public virtual ~ContentBundle();

        //int operator ==(const ContentBundle& that)  const { return this == &that; }

        public string GetName() { return name; }
        public string GetText(string key)
        {
            string val;
            if (values.TryGetValue(key, out val))
                return val;
            else
                return key;
        }
        public bool IsLoaded() { return values.Count != 0; }


        protected void LoadBundle(Stream file)
        {
            string key;
            string val;
            string line;
            try
            {
                using (var streamReader = new StreamReader(file))
                {
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        if (string.IsNullOrWhiteSpace(line) || line.StartsWith("#")) continue;
                        string[] parts = line.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                        if (parts.Length != 2) continue;
                        key = parts[0].Trim();
                        val = parts[1].Trim().Replace(@"\n", Environment.NewLine);
                        if (!values.ContainsKey(key))
                            values.Add(key, val);
                        else
                            ErrLogger.PrintLine("WARNING: A duplicate key-value exists in localization file '{0}={1}'", key, val);

                    }
                }
            }
            finally
            { file.Close(); }
        }
        protected Stream FindFile(string basename, CultureInfo locale)
        {
            Stream result;
            var filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "Content");
            string filename;

            if (filePath != null && !string.IsNullOrWhiteSpace(basename))
            {
                if (locale != null)
                {
                    filename = System.IO.Path.Combine(filePath, basename + "_" + locale.Name + ".txt");

                    if (File.Exists(filename))
                        try
                        {
                            result = File.Open(filename, FileMode.Open);
                            return result;
                        }
                        catch
                        {
                            ErrLogger.PrintLine("WARNING: Not found localization file '{0}'", filename);
                        }

                    filename = System.IO.Path.Combine(filePath, basename + "_" + locale.TwoLetterISOLanguageName + ".txt");

                    if (File.Exists(filename))
                        try
                        {
                            result = File.Open(filename, FileMode.Open);
                            return result;
                        }
                        catch
                        {
                            ErrLogger.PrintLine("WARNING: Not found localization file '{0}'", filename);
                        }
                }

                filename = System.IO.Path.Combine(filePath, basename + ".txt");

                try
                {
                    result = File.Open(filename, FileMode.Open);
                    return result;
                }
                catch
                {
                    ErrLogger.PrintLine("FATAL ERROR: Not found localization file '{0}'", filename);
                }
            }

            return null;
        }

        protected string name;
        protected Dictionary<string, string> values = new Dictionary<string, string>();

    }
}
