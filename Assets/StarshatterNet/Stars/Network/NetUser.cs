﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetLobby.h/NetLobby.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

    OVERVIEW
    ========
    This class represents a user connecting to the multiplayer lobby
 */
using StarshatterNet.Gen.Misc;
using UnityEngine;
using DWORD = System.UInt32;

namespace StarshatterNet.Stars.Network
{
    public class NetUser
    {
        public NetUser(string name) { throw new System.NotSupportedException(); }
        public NetUser(Player player) { throw new System.NotSupportedException(); }
        //public virtual ~NetUser();

        //int operator ==(  NetUser  u) const { return this == &u; }

        public string Name() { return name; }
        public string Pass() { return pass; }
        public NetAddr GetAddress() { return addr; }
        public Color GetColor() { return color; }
        public string GetSessionID() { return session_id; }
        public DWORD GetNetID() { return netid; }
        public bool IsHost() { return host; }

        public int AuthLevel() { return auth_level; }
        public int AuthState() { return auth_state; }
        public string Salt() { return salt; }
        public bool IsAuthOK() { throw new System.NotSupportedException(); }

        public string Squadron() { return squadron; }
        public string Signature() { return signature; }
        public int Rank() { return rank; }
        public int FlightTime() { return flight_time; }
        public int Missions() { return missions; }
        public int Kills() { return kills; }
        public int Losses() { return losses; }

        public void SetName(string n) { name = n; }
        public void SetPass(string p) { pass = p; }
        public void SetAddress(NetAddr a) { addr = a; }

        public void SetColor(Color c) { color = c; }
        public void SetNetID(DWORD id) { netid = id; }
        public void SetSessionID(string s) { session_id = s; }
        public void SetHost(bool h) { host = h; }

        public void SetAuthLevel(int n) { auth_level = n; }
        public void SetAuthState(int n) { auth_state = n; }
        public void SetSalt(string s) { salt = s; }

        public void SetSquadron(string s) { squadron = s; }
        public void SetSignature(string s) { signature = s; }
        public void SetRank(int n) { rank = n; }
        public void SetFlightTime(int n) { flight_time = n; }
        public void SetMissions(int n) { missions = n; }
        public void SetKills(int n) { kills = n; }
        public void SetLosses(int n) { losses = n; }

        public string GetDescription()
        {
            string content = "";

            content += "name \"";
            content += FormatUtil.SafeQuotes(name);
            content += "\" sig \"";
            content += FormatUtil.SafeQuotes(signature);
            content += "\" squad \"";
            content += FormatUtil.SafeQuotes(squadron);

            string buffer = string.Format("\" rank {0} time {1} miss {2} kill {3} loss {4} host {5} ",
            rank, flight_time, missions, kills, losses,
            host ? "true" : "false");

            content += buffer;

            return content;
        }


        protected string name;
        protected string pass;
        protected string session_id;
        protected NetAddr addr;
        protected DWORD netid;
        protected Color color;
        protected bool host;

        // authentication:
        protected int auth_state;
        protected int auth_level;
        protected string salt;

        // stats:
        protected string squadron;
        protected string signature;
        protected int rank;
        protected int flight_time;
        protected int missions;
        protected int kills;
        protected int losses;
    }
}