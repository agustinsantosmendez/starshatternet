﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetAddr.h/NetAddr.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

     OVERVIEW
     ========
     Network Address (specifically, Internet Protocol)
     Represents a network endpoint as an IP address and a port number. 
     The equivalent in .Net could be the IPEndPoint class 
 */
using System;
using System.Collections.Generic;
using System.Net;

namespace StarshatterNet.Stars.Network
{
    public class NetAddr : IEquatable<NetAddr>
    {
        /// <summary>
        /// Initializes a new instance of the IPEndPoint class with the specified address and port number.
        /// </summary>
        /// <param name="hostNameOrAddress">A string representation of a host or that contains an IP address in dotted-quad notation for IPv4 and in colon-hexadecimal notation for IPv6.</param>
        /// <param name="port">The port number associated with the address, or 0 to specify any available port. port is in host order.</param>
        public NetAddr(string hostNameOrAddress, Int32 port = 0)
        {
            this.port = port;
            if (!string.IsNullOrEmpty(hostNameOrAddress))
            {
                // Resolves a host name or IP address to an IPHostEntry instance using the dns class
                IPHostEntry iphost = System.Net.Dns.GetHostEntry(hostNameOrAddress);
                // get all of the possible IP addresses for this host
                IPAddress[] addresses = iphost.AddressList;

                if (addresses.Length > 0)
                    addr = addresses[0];
            }
            Init();
        }

        /// <summary>
        /// Initializes a new instance of the IPEndPoint class with the specified address and port number.
        /// </summary>
        /// <param name="address">An IPAddress.</param>
        /// <param name="port">The port number associated with the address, or 0 to specify any available port. port is in host order.</param>
        public NetAddr(Int64 a = 0, Int32 port = 0)
        {
            this.port = port;
            Init();
        }

        /// <summary>
        /// Initializes a new instance of the IPEndPoint class with the specified address and port number.
        /// </summary>
        /// <param name="address">An IPAddress.</param>
        /// <param name="port">The port number associated with the address, or 0 to specify any available port. port is in host order.</param>
        public NetAddr(IPAddress address, Int32 port = 0)
        {
            this.port = port;
            Init();
        }


        public IPAddress IPAddr() { return addr; }
        public byte B4() { return addr.GetAddressBytes()[4]; }
        public byte B3() { return addr.GetAddressBytes()[4]; }
        public byte B2() { return addr.GetAddressBytes()[4]; }
        public byte B1() { return addr.GetAddressBytes()[4]; }

        public Int32 Port() { return port; }
        public void SetPort(Int32 p) { port = p; }

        public IPEndPoint GetSockAddr()
        {
            return sadr;
        }
        public int GetSockAddrLength()
        {
            throw new NotImplementedException();
            //return  sizeof(IPEndPoint);
        }

        public void SetSockAddr(IPEndPoint s, int size)
        {
            if (s != null)
            {
                sadr = s;
                InitFromSockAddr();
            }
        }

        public void InitFromSockAddr()
        {
            addr = sadr.Address;
            port = sadr.Port;
        }

        private void Init()
        {
            sadr = new IPEndPoint(addr, port);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as NetAddr);
        }

        public bool Equals(NetAddr other)
        {
            return other != null &&
                   EqualityComparer<IPAddress>.Default.Equals(addr, other.addr) &&
                   port == other.port;
        }

        public override int GetHashCode()
        {
            var hashCode = -1947388440;
            hashCode = hashCode * -1521134295 + EqualityComparer<IPAddress>.Default.GetHashCode(addr);
            hashCode = hashCode * -1521134295 + port.GetHashCode();
            return hashCode;
        }


        public static bool operator ==(NetAddr addr1, NetAddr addr2)
        {
            return EqualityComparer<NetAddr>.Default.Equals(addr1, addr2);
        }

        public static bool operator !=(NetAddr addr1, NetAddr addr2)
        {
            return !(addr1 == addr2);
        }

        private IPAddress addr;    // IP addr in host byte order
        private Int32 port;         // IP port in host byte order
        private IPEndPoint sadr;

    }
}