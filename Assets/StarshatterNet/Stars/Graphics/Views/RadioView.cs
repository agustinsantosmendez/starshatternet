﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      RadioView.h/RadioView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    View class for Radio Communications HUD Overlay
*/
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Simulator;
using System;
using UnityEngine;

namespace StarshatterNet.Stars.Views
{
#warning RadioView class is still in development and is not recommended for production.
    public class RadioView : View, ISimObserver
    {
        public RadioView(Window c) : base(c, "RadioView")
        {
#if TODO
            View = c;
#endif
            sim = null; ship = null; font = null; dst_elem = null;

            radio_view = this;
            sim = Sim.GetSim();

            width = window.Width();
            height = window.Height();
            xcenter = (width / 2.0) - 0.5;
            ycenter = (height / 2.0) + 0.5;
            font = FontMgr.Find("HUD");

            HUDView hud = HUDView.GetInstance();
            if (hud != null)
                SetColor(hud.GetTextColor());

            for (int i = 0; i < MAX_MSG; i++)
                msg_time[i] = 0;
        }
        // ~RadioView();

        // Operations:
        public override void Refresh()
        {
        }
        public override void OnWindowMove()
        {
            width = window.Width();
            height = window.Height();
            xcenter = (width / 2.0) - 0.5;
            ycenter = (height / 2.0) + 0.5;
        }

        static int current_key = 0;
        public virtual void ExecFrame()
        {
            HUDView hud = HUDView.GetInstance();
            if (hud != null)
            {
                if (txt_color != hud.GetTextColor())
                {
                    txt_color = hud.GetTextColor();
                    SetColor(txt_color);
                }

                hud_color = hud.GetHUDColor();
            }


            if (current_key > 0 && Keyboard.KeyDown(current_key))
                return;

            current_key = 0;

            Menu menu = history.GetCurrent();
            if (menu != null)
            {
                int max_items = menu.NumItems();

                if (menu == starship_menu && Keyboard.KeyDown('0'))
                {
                    current_key = '0';
                    if (++starship_page >= num_pages)
                        starship_page = 0;

                    history.Pop();
                    history.Push(GetRadioMenu(ship));
                }
                else
                {
                    for (int i = 0; i < max_items; i++)
                    {
                        if (Keyboard.KeyDown('1' + i))
                        {
                            current_key = '1' + i;
                            MenuItem item = menu.GetItem(i);
                            if (item != null && item.GetEnabled())
                            {
                                if (item.GetSubmenu() != null)
                                {
                                    if (history.GetCurrent() == starship_menu)
                                        dst_elem = (Element)item.GetData();

                                    history.Push(item.GetSubmenu());
                                }
                                else
                                {
                                    // execute radio message:
                                    SendRadioMessage(ship, item);

                                    // clear radio menu:
                                    history.Clear();
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }

        public virtual bool IsMenuShown()
        {
            return history.GetCurrent() != null;
        }

        public virtual void ShowMenu()
        {
            if (ship == null) return;

            if (history.GetCurrent() == null)
            {
                history.Push(GetRadioMenu(ship));

                for (int i = 0; i < 10; i++)
                {
                    if (Keyboard.KeyDown('1' + i))
                    {
                        // just need to clear the key down flag
                        // so we don't process old keystrokes
                        // as valid menu picks...
                    }
                }
            }
        }

        public virtual void CloseMenu()
        {
            history.Clear();
            dst_elem = null;
            starship_page = 0;
            num_pages = 0;
        }

        public static void Message(string msg)
        {
            //AutoThreadSync a(sync);

            if (radio_view != null)
            {
                int index = -1;

                for (int i = 0; i < MAX_MSG; i++)
                {
                    if (radio_view.msg_time[i] <= 0)
                    {
                        index = i;
                        break;
                    }
                }

                // no space; advance pipeline:
                if (index < 0)
                {
                    for (int i = 0; i < MAX_MSG - 1; i++)
                    {
                        radio_view.msg_text[i] = radio_view.msg_text[i + 1];
                        radio_view.msg_time[i] = radio_view.msg_time[i + 1];
                    }

                    index = MAX_MSG - 1;
                }

                radio_view.msg_text[index] = msg;
                radio_view.msg_time[index] = 10;
            }
        }
        public static void ClearMessages()
        {
            if (radio_view != null)
            {
                for (int i = 0; i < MAX_MSG - 1; i++)
                {
                    radio_view.msg_text[i] = "";
                    radio_view.msg_time[i] = 0;
                }
            }
        }

        public bool Update(SimObject obj)
        {
            if (obj == ship)
            {
                ship = null;
                history.Clear();
            }

            return simObserver.Update(obj);
        }

        public string GetObserverName()
        {
            return "RadioView";
        }


        public static void SetColor(Color c)
        {
            HUDView hud = HUDView.GetInstance();

            if (hud != null)
            {
                hud_color = hud.GetHUDColor();
                txt_color = hud.GetTextColor();
            }
            else
            {
                hud_color = c;
                txt_color = c;
            }
        }


        static bool initialized = false;
        public static void Initialize()
        {
            if (initialized) return;

            target_menu = new Menu(Game.GetText("RadioView.menu.TARGET"));
            target_menu.AddItem(Game.GetText("RadioView.item.attack"), (int)RadioMessage.ACTION.ATTACK);
            target_menu.AddItem(Game.GetText("RadioView.item.bracket"), (int)RadioMessage.ACTION.BRACKET);
            target_menu.AddItem(Game.GetText("RadioView.item.escort"), (int)RadioMessage.ACTION.ESCORT);

            combat_menu = new Menu(Game.GetText("RadioView.menu.COMBAT"));
            combat_menu.AddItem(Game.GetText("RadioView.item.cover"), (int)RadioMessage.ACTION.COVER_ME);
            combat_menu.AddItem(Game.GetText("RadioView.item.break-attack"), (int)RadioMessage.ACTION.WEP_FREE);
            combat_menu.AddItem(Game.GetText("RadioView.item.form-up"), (int)RadioMessage.ACTION.FORM_UP);

            formation_menu = new Menu(Game.GetText("RadioView.menu.FORMATION"));
            formation_menu.AddItem(Game.GetText("RadioView.item.diamond"), (int)RadioMessage.ACTION.GO_DIAMOND);
            formation_menu.AddItem(Game.GetText("RadioView.item.spread"), (int)RadioMessage.ACTION.GO_SPREAD);
            formation_menu.AddItem(Game.GetText("RadioView.item.box"), (int)RadioMessage.ACTION.GO_BOX);
            formation_menu.AddItem(Game.GetText("RadioView.item.trail"), (int)RadioMessage.ACTION.GO_TRAIL);

            sensors_menu = new Menu(Game.GetText("RadioView.menu.SENSORS"));
            sensors_menu.AddItem(Game.GetText("RadioView.item.emcon-1"), (int)RadioMessage.ACTION.GO_EMCON1);
            sensors_menu.AddItem(Game.GetText("RadioView.item.emcon-2"), (int)RadioMessage.ACTION.GO_EMCON2);
            sensors_menu.AddItem(Game.GetText("RadioView.item.emcon-3"), (int)RadioMessage.ACTION.GO_EMCON3);
            sensors_menu.AddItem(Game.GetText("RadioView.item.probe"), (int)RadioMessage.ACTION.LAUNCH_PROBE);

            mission_menu = new Menu(Game.GetText("RadioView.menu.MISSION"));
            mission_menu.AddItem(Game.GetText("RadioView.item.skip-navpt"), (int)RadioMessage.ACTION.SKIP_NAVPOINT);
            mission_menu.AddItem(Game.GetText("RadioView.item.resume"), (int)RadioMessage.ACTION.RESUME_MISSION);
            mission_menu.AddItem(Game.GetText("RadioView.item.rtb"), (int)RadioMessage.ACTION.RTB);

            wing_menu = new Menu(Game.GetText("RadioView.menu.WINGMAN"));
            wing_menu.AddMenu(Game.GetText("RadioView.item.target"), target_menu);
            wing_menu.AddMenu(Game.GetText("RadioView.item.combat"), combat_menu);
            wing_menu.AddMenu(Game.GetText("RadioView.item.formation"), formation_menu);
            wing_menu.AddMenu(Game.GetText("RadioView.item.mission"), mission_menu);
            wing_menu.AddMenu(Game.GetText("RadioView.item.sensors"), sensors_menu);

            elem_menu = new Menu(Game.GetText("RadioView.menu.ELEMENT"));
            elem_menu.AddMenu(Game.GetText("RadioView.item.target"), target_menu);
            elem_menu.AddMenu(Game.GetText("RadioView.item.combat"), combat_menu);
            elem_menu.AddMenu(Game.GetText("RadioView.item.formation"), formation_menu);
            elem_menu.AddMenu(Game.GetText("RadioView.item.mission"), mission_menu);
            elem_menu.AddMenu(Game.GetText("RadioView.item.sensors"), sensors_menu);

            control_menu = new Menu(Game.GetText("RadioView.menu.CONTROL"));
            control_menu.AddItem(Game.GetText("RadioView.item.picture"), (int)RadioMessage.ACTION.REQUEST_PICTURE);
            control_menu.AddItem(Game.GetText("RadioView.item.backup"), (int)RadioMessage.ACTION.REQUEST_SUPPORT);
            control_menu.AddItem(Game.GetText("RadioView.item.call-inbound"), (int)RadioMessage.ACTION.CALL_INBOUND);
            control_menu.AddItem(Game.GetText("RadioView.item.call-finals"), (int)RadioMessage.ACTION.CALL_FINALS);

            fighter_menu = new Menu(Game.GetText("RadioView.menu.RADIO"));
            fighter_menu.AddMenu(Game.GetText("RadioView.item.wingman"), wing_menu);
            fighter_menu.AddMenu(Game.GetText("RadioView.item.element"), elem_menu);
            fighter_menu.AddMenu(Game.GetText("RadioView.item.control"), control_menu);

            starship_menu = new Menu(Game.GetText("RadioView.menu.RADIO"));

            initialized = true;
        }
        public static void Close()
        {
            history.Clear();

            //delete fighter_menu;
            //delete starship_menu;
            //delete target_menu;
            //delete combat_menu;
            //delete formation_menu;
            //delete sensors_menu;
            //delete mission_menu;
            //delete wing_menu;
            //delete elem_menu;
            //delete control_menu;
        }

        public static RadioView GetInstance() { return radio_view; }
        static bool TargetRequired(MenuItem item)
        {
            if (item != null)
            {
                switch ((RadioMessage.ACTION)item.GetData())
                {
                    case RadioMessage.ACTION.ATTACK:
                    case RadioMessage.ACTION.BRACKET:
                    case RadioMessage.ACTION.ESCORT:
                        return true;

                    default:
                        if (item.GetData() == target_menu)
                            return true;
                        break;
                }
            }

            return false;
        }


        protected void SendRadioMessage(Ship ship, MenuItem item)
        {
            if (ship == null || item == null) return;
            Element elem = ship.GetElement();
            if (elem == null) return;

            // check destination:
            if (dst_elem != null)
            {
                RadioMessage msg = new RadioMessage(dst_elem, ship, (RadioMessage.ACTION)item.GetData());

                if (TargetRequired(item))
                    msg.AddTarget(ship.GetTarget());

                RadioTraffic.Transmit(msg);
                dst_elem = null;
            }

            else if (history.Find(Game.GetText("RadioView.menu.WINGMAN")) != null)
            { // wingman menu
                int index = ship.GetElementIndex();
                int wing = 0;

                switch (index)
                {
                    case 1: wing = 2; break;
                    case 2: wing = 1; break;
                    case 3: wing = 4; break;
                    case 4: wing = 3; break;
                }

                if (wing != 0)
                {
                    Ship dst = elem.GetShip(wing);
                    if (dst != null)
                    {
                        RadioMessage msg = new RadioMessage(dst, ship, (RadioMessage.ACTION)item.GetData());

                        if (TargetRequired(item))
                            msg.AddTarget(ship.GetTarget());

                        RadioTraffic.Transmit(msg);
                    }
                }
            }

            else if (history.Find(Game.GetText("RadioView.menu.ELEMENT")) != null)
            { // element menu
                RadioMessage msg = new RadioMessage(elem, ship, (RadioMessage.ACTION)item.GetData());

                if (TargetRequired(item))
                    msg.AddTarget(ship.GetTarget());

                RadioTraffic.Transmit(msg);
            }

            else if (history.Find(Game.GetText("RadioView.menu.CONTROL")) != null)
            { // control menu
                RadioMessage msg = null;
                Ship controller = ship.GetController();

                if (controller != null)
                {
                    msg = new RadioMessage(controller, ship, (RadioMessage.ACTION)item.GetData());
                    RadioTraffic.Transmit(msg);
                }
            }
        }

        public virtual Menu GetRadioMenu(Ship s)
        {
            dst_elem = null;

            if (s != null && sim != null)
            {
                if (s.IsStarship())
                {
                    starship_menu.ClearItems();

                    int n = 0;
                    int page_offset = starship_page * PAGE_SIZE;

                    if (num_pages == 0)
                    {
                        foreach (Element elem in sim.GetElements())
                        {
                            if (elem.IsFinished() || elem.IsSquadron() || elem.IsStatic())
                                continue;

                            if (ship.GetIFF() == elem.GetIFF() && ship.GetElement() != elem)
                                n++;
                        }

                        num_pages = (n / PAGE_SIZE) + (n % PAGE_SIZE > 0 ? 1 : 0);
                        n = 0;
                    }

                    foreach (Element elem in sim.GetElements())
                    {
                        if (elem.IsFinished() || elem.IsSquadron() || elem.IsStatic())
                            continue;

                        if (ship.GetIFF() == elem.GetIFF() && ship.GetElement() != elem)
                        {
                            if (n >= page_offset && n < page_offset + PAGE_SIZE)
                            {
                                string text = string.Format("{0}. {1}", n + 1 - page_offset, elem.Name());

                                if (elem.IsActive())
                                {
                                    starship_menu.AddMenu(text, elem_menu, elem);
                                }
                                else
                                {
                                    text += " ";
                                    text += Game.GetText("RadioView.item.not-avail");
                                    starship_menu.AddItem(text, 0, false);
                                }
                            }
                            n++;
                        }
                    }

                    if (num_pages > 1)
                    {
                        string text = string.Format(Game.GetText("RadioView.item.next-page"), starship_page + 1, num_pages);
                        starship_menu.AddItem(text);
                    }

                    return starship_menu;
                }
                else if (s.IsDropship())
                {
                    return fighter_menu;
                }
            }

            return null;
        }


        public void Observe(SimObject obj)
        {
            simObserver.Observe(obj);
        }

        public void Ignore(SimObject obj)
        {
            simObserver.Ignore(obj);
        }

        protected SimObserver simObserver = new SimObserver();

        protected FontItem font;

        protected int width, height;
        protected double xcenter, ycenter;

        protected Sim sim;
        protected Ship ship;
        protected Element dst_elem;

        protected const int MAX_MSG = 6;
        protected string[] msg_text = new string[MAX_MSG];
        protected double[] msg_time = new double[MAX_MSG];

        protected static RadioView radio_view;
        protected static object sync;

        private static Menu fighter_menu = null;
        private static Menu starship_menu = null;
        private static Menu target_menu = null;
        private static Menu combat_menu = null;
        private static Menu formation_menu = null;
        private static Menu sensors_menu = null;
        private static Menu mission_menu = null;
        private static Menu wing_menu = null;
        private static Menu elem_menu = null;
        private static Menu control_menu = null;

        private static int starship_page = 0;
        private static int num_pages = 0;
        private const int PAGE_SIZE = 9;

        private static MenuHistory history;

        private static Color hud_color = Color.black;
        private static Color txt_color = Color.white;

    }
}