﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Sensor.h/Sensor.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Integrated (Passive and Active) Sensor Package class
*/
using DigitalRune.Mathematics;
using StarshatterNet.Stars.Simulator;
using System;
using System.Collections.Generic;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Gen.Graphics;

namespace StarshatterNet.Stars.SimElements
{
    public class Sensor : ShipSystem, ISimObserver
    {

        public enum Mode
        {
            PAS, STD, ACM, GM,   // fighter modes
            PST, CST             // starship modes
        };

        public Sensor() : base(CATEGORY.SENSOR, 1, "Dual Sensor Pkg", 1, 10, 10, 10)
        {
            mode = Mode.STD; target = null;
            nsettings = 0; range_index = 0;

            name = Game.GetText("sys.sensor");
            abrv = Game.GetText("sys.sensor.abrv");

            SetMode(mode);
            power_flags = POWER_FLAGS.POWER_WATTS;

            //Array.Clear(range_settings, 0, range_settings.Length);
        }

        public Sensor(Sensor s) : base(s)
        {
            mode = Mode.STD; target = null;
            nsettings = s.nsettings; range_index = 0;
            Mount(s);

            SetMode(mode);
            power_flags = POWER_FLAGS.POWER_WATTS;

            Array.Copy(s.range_settings, range_settings, s.range_settings.Length);
            if (nsettings != 0)
                range_index = nsettings - 1;
        }

        ~Sensor()
        {
            ClearAllContacts();
        }

        public override void ExecFrame(double seconds)
        {
            if (SimulationTime.IsPaused)
                return;

            base.ExecFrame(seconds);

            if (!IsPowerOn() || energy <= 0)
            {
                ClearAllContacts();
                return;
            }

            if (ship != null && ship.GetAIMode() < 2)
            {
                // just pretend to work this frame:
                energy = 0.0f;
                return;
            }

            if (ship != null && ship.GetRegion() != null)
            {
                CameraNode cam = ship.Cam();
                double az1 = MathHelper.ToRadians(-45);
                double az2 = MathHelper.ToRadians(45);

                if (mode > Mode.GM)
                {
                    az1 = MathHelper.ToRadians(-175);
                    az2 = MathHelper.ToRadians(175);
                }

                foreach (Ship c_ship in ship.GetRegion().Ships())
                {
                    if (c_ship != ship)
                    {
                        ProcessContact(c_ship, az1, az2);
                    }
                    else
                    {
                        Contact c = FindContact(c_ship);

                        if (c == null)
                        {
                            c = new Contact(c_ship, 0.0f, 0.0f);
                            contacts.Add(c);
                        }

                        // update track:
                        if (c != null)
                        {
                            c.loc = c_ship.Location();
                            c.d_pas = 2.0f;
                            c.d_act = 2.0f;

                            c.UpdateTrack();
                        }
                    }
                }

                foreach (var threat_iter in ship.GetThreatList())
                {
                    ProcessContact(threat_iter, az1, az2);
                }

                foreach (var drone_iter in ship.GetRegion().Drones())
                {
                    ProcessContact(drone_iter, az1, az2);
                }

                List<Contact> track_list = ship.GetRegion().TrackList(ship.GetIFF());

                for (int i = contacts.Count - 1; i >= 0; i--)
                {
                    Contact contact = contacts[i];
                    Ship c_ship = contact.GetShip();
                    Shot c_shot = contact.GetShot();
                    double c_life = -1;

                    if (c_ship != null)
                    {
                        c_life = c_ship.Life();

                        // look for quantum jumps and orbit transitions:
                        if (c_ship.GetRegion() != ship.GetRegion())
                            c_life = 0;
                    }

                    else if (c_shot != null)
                    {
                        c_life = c_shot.Life();
                    }

                    else
                    {
                        c_life = 0;
                    }

                    if (contact.Age() < 0 || c_life == 0)
                    {
                        //delete c_iter.removeItem();
                        contacts.Remove(contact);
                    }

                    else if (ship != null && ship.GetIFF() >= 0 && ship.GetIFF() < 5)
                    {
                        // update shared track database:
                        Contact t = track_list.Find(e => e == contact);

                        if (c_ship != null)
                        {
                            if (t == null)
                            {
                                Contact track = new Contact(c_ship, contact.d_pas, contact.d_act);
                                track.loc = c_ship.Location();
                                track_list.Add(track);
                            }

                            else
                            {
                                t.loc = c_ship.Location();
                                t.Merge(contact);
                                t.UpdateTrack();
                            }
                        }

                        else if (c_shot != null)
                        {
                            if (t == null)
                            {
                                Contact track = new Contact(c_shot, contact.d_pas, contact.d_act);
                                track.loc = c_shot.Location();
                                track_list.Add(track);
                            }

                            else
                            {
                                t.loc = c_shot.Location();
                                t.Merge(contact);
                                t.UpdateTrack();
                            }
                        }
                    }
                }


                if (mode == Mode.ACM)
                {
                    if (ship.GetTarget() == null)
                        ship.LockTarget(SimObject.TYPES.SIM_SHIP, true, true);
                }
            }

            energy = 0.0f;
        }

        public virtual SimObject LockTarget(SimObject.TYPES type = SimObject.TYPES.SIM_SHIP, bool closest = false, bool hostile = false)
        {
            if (ship == null || ship.GetEMCON() < 3)
            {
                Ignore(target);
                target = null;
                return target;
            }

            SimObject test = null;
            List<TargetOffset> targets = new List<TargetOffset>();

            foreach (var contact in ship.ContactList())
            {
                if (type == SimObject.TYPES.SIM_SHIP)
                    test = contact.GetShip();
                else
                    test = contact.GetShot();

                if (test == null)
                    continue;

                // do not target own missiles:
                if (contact.GetShot() != null && contact.GetShot().Owner() == ship)
                    continue;

                double tgt_range = contact.Range(ship);

                // do not target ships outside of detet range:
                if (contact.GetShip() != null && contact.GetShip().Design().detet < tgt_range)
                    continue;

                double d_pas = contact.PasReturn();
                double d_act = contact.ActReturn();
                bool vis = contact.Visible(ship) || contact.Threat(ship);

                if (!vis && d_pas < sensor_lock_threshold && d_act < sensor_lock_threshold)
                    continue;

                if (closest)
                {
                    if (hostile && (contact.GetIFF(ship) == 0 || contact.GetIFF(ship) == ship.GetIFF()))
                        continue;

                    targets.Add(new TargetOffset(test, tgt_range));
                }

                // clip:
                else if (contact.InFront(ship))
                {
                    double az, el, rng;

                    contact.GetBearing(ship, out az, out el, out rng);
                    az = Math.Abs(az / Math.PI);
                    el = Math.Abs(el / Math.PI);

                    if (az <= 0.2 && el <= 0.2)
                        targets.Add(new TargetOffset(test, az + el));
                }
            }

            targets.Sort();

            if (target != null)
            {
                int index = 100000;
                int i = 0;

                if (targets.Count > 0)
                {
                    foreach (var iter in targets)
                    {
                        if (iter.target == target)
                        {
                            index = i;
                            break;
                        }

                        i++;
                    }

                    if (index < targets.Count - 1)
                        index++;
                    else
                        index = 0;

                    target = targets[index].target;
                    Observe(target);
                }
            }
            else if (targets.Count > 0)
            {
                target = targets[0].target;
                Observe(target);
            }
            else
            {
                target = null;
            }

            targets.Clear();

            if (target != null && mode < Mode.STD)
                mode = Mode.STD;

            return target;
        }
        public virtual SimObject LockTarget(SimObject candidate)
        {
            Ignore(target);
            target = null;

            if (ship.GetEMCON() < 3)
                return target;

            if (candidate == null)
                return target;

            SimObject.TYPES type = (SimObject.TYPES)candidate.Type();
            SimObject test = null;

            foreach (var contact in ship.ContactList())
            {
                if (type == SimObject.TYPES.SIM_SHIP)
                    test = contact.GetShip();
                else
                    test = contact.GetShot();

                if (test == candidate)
                {
                    double d_pas = contact.PasReturn();
                    double d_act = contact.ActReturn();
                    bool vis = contact.Visible(ship) || contact.Threat(ship);

                    if (vis || d_pas > sensor_lock_threshold || d_act > sensor_lock_threshold)
                    {
                        target = test;
                        Observe(target);
                    }

                    break;
                }
            }

            if (target != null && mode < Mode.STD)
                mode = Mode.STD;

            return target;
        }

        public virtual bool IsTracking(SimObject tgt)
        {
            if (tgt != null && mode != Mode.GM && mode != Mode.PAS && mode != Mode.PST && IsPowerOn())
            {
                if (tgt == target)
                    return true;

                Contact c = null;

                if ((SimObject.TYPES)tgt.Type() == SimObject.TYPES.SIM_SHIP)
                {
                    c = FindContact((Ship)tgt);
                }
                else
                {
                    c = FindContact((Shot)tgt);
                }

                return (c != null && c.ActReturn() > SENSOR_THRESHOLD && !c.IsProbed());
            }

            return false;
        }

        public override void DoEMCON(int index)
        {
            int e = GetEMCONPower(index);

            if (power_level * 100 > e || emcon != index)
            {
                if (e == 0)
                {
                    PowerOff();
                }
                else if (emcon != index)
                {
                    PowerOn();

                    if (power_level * 100 > e)
                    {
                        SetPowerLevel(e);
                    }

                    if (emcon == 3)
                    {
                        if (GetMode() < Mode.PST)
                            SetMode(Mode.STD);
                        else
                            SetMode(Mode.CST);
                    }
                    else
                    {
                        Mode m = GetMode();
                        if (m < Mode.PST && m > Mode.PAS)
                            SetMode(Mode.PAS);
                        else if (m == Mode.CST)
                            SetMode(Mode.PST);
                    }
                }
            }

            emcon = index;
        }

        public virtual void ClearAllContacts()
        {
            contacts.Clear();
        }


        public virtual Mode GetMode() { return mode; }
        public virtual void SetMode(Mode m)
        {
            if (mode != m)
            {
                // dump the contact list when changing in/out of GM:
                if (mode == Mode.GM || m == Mode.GM)
                    ClearAllContacts();

                // dump the current target on mode changes:
                if (m <= Mode.GM)
                {
                    if (ship != null)
                        ship.DropTarget();

                    Ignore(target);
                    target = null;
                }
            }

            mode = m;
        }

        public virtual double GetBeamLimit()
        {
            if (mode == Mode.ACM)
                return MathHelper.ToRadians(15);

            if (mode <= Mode.GM)
                return MathHelper.ToRadians(45);

            return MathHelper.ToRadians(175);
        }

        public virtual double GetBeamRange()
        {
            return (double)range_settings[range_index];
        }

        public virtual void IncreaseRange()
        {
            if (range_index < nsettings - 1)
                range_index++;
            else
                range_index = nsettings - 1;
        }

        public virtual void DecreaseRange()
        {
            if (range_index > 0)
                range_index--;
            else
                range_index = 0;
        }

        public virtual void AddRange(double r)
        {
            if (nsettings < 8)
                range_settings[nsettings++] = (float)r;

            range_index = nsettings - 1;
        }

        public Contact FindContact(Ship s)
        {
            foreach (var contact in contacts)
            {
                if (contact.GetShip() == s)
                    return contact;
            }

            return null;
        }

        public Contact FindContact(Shot s)
        {
            foreach (var contact in contacts)
            {
                if (contact.GetShot() == s)
                    return contact;
            }

            return null;
        }

        // borrow this sensor for missile seeker
        public SimObject AcquirePassiveTargetForMissile()
        {
            SimObject pick = null;
            double min_off = 2;


            foreach (var contact in ship.ContactList())
            {
                SimObject test = contact.GetShip();
                double d = contact.PasReturn();

                if (d < 1) continue;

                // clip:
                if (contact.InFront(ship))
                {
                    double az, el, rng;

                    contact.GetBearing(ship, out az, out el, out rng);
                    az = Math.Abs(az / Math.PI);
                    el = Math.Abs(el / Math.PI);

                    if (az + el < min_off)
                    {
                        min_off = az + el;
                        pick = test;
                    }
                }
            }

            return pick;
        }

        public SimObject AcquireActiveTargetForMissile()
        {
            SimObject pick = null;
            double min_off = 2;


            foreach (var contact in ship.ContactList())
            {
                SimObject test = contact.GetShip();
                double d = contact.ActReturn();

                if (d < 1) continue;

                if (contact.InFront(ship))
                {
                    double az, el, rng;

                    contact.GetBearing(ship, out az, out el, out rng);
                    az = Math.Abs(az / Math.PI);
                    el = Math.Abs(el / Math.PI);

                    if (az + el < min_off)
                    {
                        min_off = az + el;
                        pick = test;
                    }
                }
            }

            return pick;
        }

        // SimObserver:
        public virtual bool Update(SimObject obj)
        {
            if (obj == target)
            {
                target = null;
            }

            return simObserver.Update(obj);
        }

        public virtual string GetObserverName()
        {
            return "Sensor";
        }


        protected void ProcessContact(Ship c_ship, double az1, double az2)
        {
#if TODO
            if (c_ship.IsNetObserver())
                return;

            double sensor_range = GetBeamRange();

            // translate:
            CameraNode cam = ship.Cam();
            Point targ_pt = c_ship.Location() - ship.Location();

            // rotate:
            double tx = Point.Dot(targ_pt, cam.vrt());
            double ty = Point.Dot(targ_pt, cam.vup());
            double tz = Point.Dot(targ_pt, cam.vpn());

            // convert to spherical coords:
            double rng = targ_pt.Length;
            double az = Math.Asin(Math.Abs(tx) / rng);
            double el = Math.Asin(Math.Abs(ty) / rng);
            if (tx < 0) az = -az;
            if (ty < 0) el = -el;

            double min_range = rng;
            Drone probe = ship.GetProbe();
            bool probescan = false;

            if (ship.GetIFF() == c_ship.GetIFF())
            {
                min_range = 1;
            }

            else if (probe != null)
            {
                Point probe_pt = c_ship.Location() - probe.Location();
                double prng = probe_pt.Length;

                if (prng < probe.Design().lethal_radius && prng < rng)
                {
                    min_range = prng;
                    probescan = true;
                }
            }

            bool vis = tz > 1 && (c_ship.Radius() / rng > 0.001);
            bool threat = (c_ship.Life() != 0 &&
                           c_ship.GetIFF() != 0 &&
                            c_ship.GetIFF() != ship.GetIFF() &&
                            c_ship.GetEMCON() > 2 &&
                            c_ship.IsTracking(ship));

            if (!threat)
            {
                if (mode == Mode.GM && !c_ship.IsGroundUnit())
                    return;

                if (mode != Mode.GM && c_ship.IsGroundUnit())
                    return;

                if (min_range > sensor_range || min_range > c_ship.Design().detet)
                {
                    if (c_ship.Equals(target))
                    {
                        ship.DropTarget();
                        Ignore(target);
                        target = null;
                    }

                    return;
                }
            }

            // clip:
            if (threat || vis || mode >= Mode.PST || tz > 1)
            {

                // correct az/el for back hemisphere:
                if (tz < 0)
                {
                    if (az < 0) az = -Math.PI - az;
                    else az = Math.PI - az;
                }

                double d_pas = 0;
                double d_act = 0;
                double effectivity = energy / capacity * availability;

                // did this contact get scanned this frame?
                if (effectivity > SENSOR_THRESHOLD)
                {
                    if (az >= az1 && az <= az2 && (mode >= Mode.PST || Math.Abs(el) < MathHelper.ToRadians(45)))
                    {
                        double passive_range_limit = 500e3;
                        if (c_ship.Design().detet > passive_range_limit)
                            passive_range_limit = c_ship.Design().detet;

                        d_pas = c_ship.PCS() * effectivity * (1 - min_range / passive_range_limit);

                        if (d_pas < 0)
                            d_pas = 0;

                        if (probescan)
                        {
                            double max_range = probe.Design().lethal_radius;
                            d_act = c_ship.ACS() * (1 - min_range / max_range);
                        }

                        else if (mode != Mode.PAS && mode != Mode.PST)
                        {
                            double max_range = sensor_range;
                            d_act = c_ship.ACS() * effectivity * (1 - min_range / max_range);
                        }

                        if (d_act < 0)
                            d_act = 0;
                    }
                }

                // yes, update or add new contact:
                if (threat || vis || d_pas > SENSOR_THRESHOLD || d_act > SENSOR_THRESHOLD)
                {
                    Element elem = c_ship.GetElement();
                    CombatUnit unit = c_ship.GetCombatUnit();

                    if (elem != null && ship && elem.GetIFF() != ship.GetIFF() && elem.IntelLevel() < Intel.LOCATED)
                    {
                        elem.SetIntelLevel(Intel.LOCATED);
                    }

                    if (unit && ship && unit.GetIFF() != ship.GetIFF())
                    {
                        CombatGroup group = unit.GetCombatGroup();

                        if (group && group.IntelLevel() < Intel.LOCATED &&
                                group.IntelLevel() > Intel.RESERVE)
                        {
                            group.SetIntelLevel(Intel.LOCATED);
                        }
                    }

                    Contact c = FindContact(c_ship);

                    if (c == null)
                    {
                        c = new Contact(c_ship, 0.0f, 0.0f);
                        contacts.Add(c);
                    }

                    // update track:
                    if (c != null)
                    {
                        c.loc = c_ship.Location();
                        c.d_pas = (float)d_pas;
                        c.d_act = (float)d_act;
                        c.probe = probescan;

                        c.UpdateTrack();
                    }
                }
            }
#endif
            throw new NotImplementedException();
        }

        protected void ProcessContact(Shot c_shot, double az1, double az2)
        {
            double sensor_range = GetBeamRange();

            if (c_shot.IsPrimary() || c_shot.IsDecoy())
                return;

            // translate:
            CameraNode cam = ship.Cam();
            Point targ_pt = c_shot.Location() - ship.Location();

            // rotate:
            double tx = Point.Dot(targ_pt, cam.vrt());
            double ty = Point.Dot(targ_pt, cam.vup());
            double tz = Point.Dot(targ_pt, cam.vpn());

            // convert to spherical coords:
            double rng = targ_pt.Length;
            double az = Math.Asin(Math.Abs(tx) / rng);
            double el = Math.Asin(Math.Abs(ty) / rng);
            if (tx < 0) az = -az;
            if (ty < 0) el = -el;

            bool vis = tz > 1 && (c_shot.Radius() / rng > 0.001);
            bool threat = (c_shot.IsTracking(ship));

            // clip:
            if (threat || vis || ((mode >= Mode.PST || tz > 1) && rng <= sensor_range))
            {

                // correct az/el for back hemisphere:
                if (tz < 0)
                {
                    if (az < 0) az = -Math.PI - az;
                    else az = Math.PI - az;
                }

                double d_pas = 0;
                double d_act = 0;
                double effectivity = energy / capacity * availability;

                // did this contact get scanned this frame?
                if (effectivity > SENSOR_THRESHOLD)
                {
                    if (az >= az1 && az <= az2 && (mode >= Mode.PST || Math.Abs(el) < MathHelper.ToRadians(45)))
                    {
                        if (rng < sensor_range / 2)
                            d_pas = 1.5;
                        else
                            d_pas = 0.5;

                        if (mode != Mode.PAS && mode != Mode.PST)
                            d_act = effectivity * (1 - rng / sensor_range);

                        if (d_act < 0)
                            d_act = 0;
                    }
                }

                // yes, update or add new contact:
                if (threat || vis || d_pas > SENSOR_THRESHOLD || d_act > SENSOR_THRESHOLD)
                {
                    Contact c = FindContact(c_shot);

                    if (c == null)
                    {
                        c = new Contact(c_shot, 0.0f, 0.0f);
                        contacts.Add(c);
                    }

                    // update track:
                    if (c != null)
                    {
                        c.loc = c_shot.Location();
                        c.d_pas = (float)d_pas;
                        c.d_act = (float)d_act;

                        c.UpdateTrack();
                    }
                }
            }
        }

        public void Observe(SimObject obj)
        {
            simObserver.Observe(obj);
        }

        public void Ignore(SimObject obj)
        {
            simObserver.Ignore(obj);
        }

        protected Mode mode;
        protected int nsettings;
        protected int range_index;
        protected float[] range_settings = new float[8];
        protected SimObject target;

        protected List<Contact> contacts;
        protected ISimObserver simObserver = new SimObserver();

        private const double sensor_lock_threshold = 0.5;
        private const double SENSOR_THRESHOLD = 0.25; // TODO Duplicate definition at Contact.cs

        private class TargetOffset
        {

            public SimObject target = null;
            public double offset = 10;

            public TargetOffset() { }
            public TargetOffset(SimObject t, double o) { target = t; offset = o; }

            // public int operator <(const TargetOffset& o) const { return offset<o.offset; }
            // public int operator <=(const TargetOffset& o) const { return offset <= o.offset; }
            // public int operator ==(const TargetOffset& o) const { return offset == o.offset; }
        }

    }
}