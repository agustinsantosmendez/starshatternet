﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    nGenEx.lib
    ORIGINAL FILE:       Bitmap.h/Bitmap.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Bitmap Resource class
*/
using System;
using System.IO;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars;
using UnityEngine;

namespace StarshatterNet.Gen.Graphics
{
    public class Bitmap
    {
        public enum BMP_TYPES { BMP_SOLID, BMP_TRANSPARENT, BMP_TRANSLUCENT };
        public Bitmap(Texture2D texture2D)
        {
            texture = texture2D;
            if (texture2D != null)
                filename = texture2D.name;
        }
        public Bitmap(string fname, BMP_TYPES t = BMP_TYPES.BMP_SOLID)
        {
            string extension = Path.GetExtension(fname);
            if (extension.Equals("pcx", StringComparison.InvariantCultureIgnoreCase))
                fname.Replace(extension, "png");
            filename = fname;
            texture = Resources.Load<Texture2D>(fname);

            SetType(t);
        }
        public Bitmap() { ErrLogger.PrintLine("Bitmap constructor is not yet implemented"); }
        public Bitmap(int w, int h, Color p, BMP_TYPES t = BMP_TYPES.BMP_SOLID) { throw new NotImplementedException(); }

        public void SetType(BMP_TYPES t) { type = t; }
        public BMP_TYPES Type() { return type; }
        public bool IsSolid() { return type == BMP_TYPES.BMP_SOLID; }
        public bool IsTransparent() { return type == BMP_TYPES.BMP_TRANSPARENT; }
        public bool IsTranslucent() { return type == BMP_TYPES.BMP_TRANSLUCENT; }

        internal void ClearImage()
        {
            ErrLogger.PrintLine("WARNING: Bitmap.ClearImage is not yet implemented.");
        }
        public Texture2D Texture { get => texture; set => texture = value; }

        public string GetFilename() { return filename; }
        public void SetFilename(string s) { filename = s; }

        public int Width()
        {
            if (texture != null)
                return texture.width;
            return 0;
        }
        public int Height()
        {
            if (texture != null)
                return texture.height;
            return 0;
        }
        protected BMP_TYPES type;
        protected string filename;
        private Texture2D texture;

        internal void DrawRect(Stars.Rect warn_rect, Color darkGray)
        {
            throw new NotImplementedException();
        }

        internal void FillRect( Stars.Rect r2, Color tc)
        {
            throw new NotImplementedException();
        }
        internal void FillRect(int x, int y, int w, int h, Color tc)
        {
            throw new NotImplementedException();
        }
        internal void DrawEllipse(int x, int y, int w, int h, Color tc)
        {
            throw new NotImplementedException();
        }
        internal void DrawRect(int x, int y, int w, int h, Color tc)
        {
            throw new NotImplementedException();
        }
        internal void DrawLine(int x1, int y1, int x2, int y2, Color tc)
        {
            throw new NotImplementedException();
        }

        internal void BitBlt(int v1, int v2, Bitmap hud_bmp, int v3, int v4, int bmp_w, int bmp_h)
        {
            throw new NotImplementedException();
        }
    }
}
