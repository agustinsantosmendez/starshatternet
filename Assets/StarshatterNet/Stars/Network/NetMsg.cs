﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetMsg.h/NetMsg.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

     OVERVIEW
     ========
     User level network message
 */
using System;
using System.Text;

namespace StarshatterNet.Stars.Network
{
    public class NetMsg : IEquatable<NetMsg>, IComparable<NetMsg>
    {
        [Flags]
        public enum FLAGS : byte { RELIABLE = 0x01, PRIORITY = 0x02, SCATTER = 0x04 };
        public enum TYPES : byte
        {
            INVALID = 0,
            RESERVED = 0xF0,
            MULTIPART = 0xF1
        };
        public const int MAX_SIZE = 250;

        public NetMsg(uint nid, byte[] d, int l, FLAGS f = 0)
        {
            msgid = net_msg_sequence++;
            netid = nid;
            len = l;
            flags = f;
            data = new byte[len];

            Array.Copy(d, data, len);

            if (len < MAX_SIZE)
                data[1] = (byte)len;
            else
                data[1] = 0;
        }

        public NetMsg(uint nid, byte type, string text, int l, FLAGS f = 0)
        {
            msgid = net_msg_sequence++;
            netid = nid;
            len = 2 + l;
            flags = f;
            data = new byte[len];

            data[0] = type;

            if (len < MAX_SIZE)
                data[1] = (byte)len;
            else
                data[1] = 0;

            byte[] bytes = Encoding.UTF8.GetBytes(text);
            if (len > 2)
                Array.Copy(bytes, 0, data, 2, len);

        }

        public uint Sequence() { return msgid; }
        public uint NetID() { return netid; }
        public byte[] Data() { return data; }
        public TYPES Type() { return data != null && data.Length > 0 ? (TYPES)data[0] : (TYPES)0; }
        public int Length() { return len; }
        public FLAGS Flags() { return flags; }

        public bool IsReliable() { return flags.HasFlag(FLAGS.RELIABLE); }
        public bool IsPriority() { return flags.HasFlag(FLAGS.PRIORITY); }
        public bool IsScatter() { return flags.HasFlag(FLAGS.SCATTER); }

        public void SetSequence(uint s) { msgid = s; }

        public override bool Equals(object obj)
        {
            return Equals(obj as NetMsg);
        }

        public bool Equals(NetMsg other)
        {
            return other != null &&
                   msgid == other.msgid &&
                   netid == other.netid;
        }

        public override int GetHashCode()
        {
            var hashCode = -843829972;
            hashCode = hashCode * -1521134295 + msgid.GetHashCode();
            hashCode = hashCode * -1521134295 + netid.GetHashCode();
            return hashCode;
        }

        public int CompareTo(NetMsg other)
        {
            if (this.data[0] == (byte)TYPES.MULTIPART && other.data[0] == (byte)TYPES.MULTIPART)
            {
                NetMsgMultipart p1 = (NetMsgMultipart)this.data;
                NetMsgMultipart p2 = (NetMsgMultipart)other.data;

                if (p1.msgid == p2.msgid)
                    return p1.partno.CompareTo(p2.partno);

                return  p1.msgid.CompareTo(p2.msgid);
            }
            return this.msgid.CompareTo(other.msgid);
        }

        public static bool operator ==(NetMsg msg1, NetMsg msg2)
        {
            return msg1.CompareTo(msg2) == 0;
        }

        public static bool operator !=(NetMsg msg1, NetMsg msg2)
        {
            return msg1.CompareTo(msg2) != 0;
        }
        public static bool operator <(NetMsg msg1, NetMsg msg2)
        {
            return msg1.CompareTo(msg2) < 0;
        }

        public static bool operator >(NetMsg msg1, NetMsg msg2)
        {
            return msg1.CompareTo(msg2) > 0;
        }
        public static bool operator <=(NetMsg msg1, NetMsg msg2)
        {
            return msg1.CompareTo(msg2) <= 0;
        }

        public static bool operator >=(NetMsg msg1, NetMsg msg2)
        {
            return msg1.CompareTo(msg2) >= 0;
        }


        private uint msgid;
        private uint netid;
        private byte[] data;
        private int len;
        private FLAGS flags;

        private static uint net_msg_sequence = 1;

        public class NetMsgMultipart
        {
            public TYPES type;
            public byte len;
            public uint msgid;
            public uint partno;
            public uint nparts;
            public byte[] payload = new byte[256];


            public static implicit operator NetMsgMultipart(byte[] d)
            {
                NetMsgMultipart msg = new NetMsgMultipart();
                int index = 0;
                msg.type = (TYPES)d[index]; index += sizeof(TYPES);
                msg.len = (byte)d[index]; index += sizeof(byte);
                msg.msgid = BitConverter.ToUInt32(d, index); index += sizeof(uint);
                msg.partno = BitConverter.ToUInt32(d, index); index += sizeof(uint);
                msg.nparts = BitConverter.ToUInt32(d, index); index += sizeof(uint);
                msg.payload = new byte[msg.len];
                // TODO Array.Copy(d, index, msg.payload, index, msg.len);
                return msg;
            }
            public static implicit operator byte[] (NetMsgMultipart msg)
            {
                byte[] d = new byte[msg.len + HeaderSize()] ;
                int index = 0;
                Array.Copy(BitConverter.GetBytes((byte)msg.type), 0, d, index, sizeof(TYPES)); index += sizeof(TYPES);
                Array.Copy(BitConverter.GetBytes(msg.len), 0, d, index, sizeof(byte)); index += sizeof(byte);
                Array.Copy(BitConverter.GetBytes(msg.msgid), 0, d, index, sizeof(uint)); index += sizeof(uint);
                Array.Copy(BitConverter.GetBytes(msg.partno), 0, d, index, sizeof(uint)); index += sizeof(uint);
                Array.Copy(BitConverter.GetBytes(msg.nparts), 0, d, index, sizeof(uint)); index += sizeof(uint);
                Array.Copy(msg.payload, 0, d, index, msg.len); index += msg.len;
                return d;
            }

            public static uint HeaderSize()
            {
                return sizeof(TYPES) + sizeof(byte) + 3 * sizeof(uint);
            }
        }
    }
}