﻿
using System.Linq;
using StarshatterNet.Gen.Forms;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars;
using StarshatterNet.Stars.Dialogs;
using UnityEditor;
using UnityEngine;

public class MenuScreenTesting : MonoBehaviour
{
    public Camera cameraObj;
    private Starshatter stars;

    void Awake()
    {
        stars = new Starshatter();
        stars.Init(cameraObj, this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        stars.Run();
        stars.Activate(true);
    }

    // Update is called once per frame
    void Update()
    {
        stars.GameLoop();

        if (Input.GetKeyDown(KeyCode.I))
            stars.ScreenCapture();
    }

}
