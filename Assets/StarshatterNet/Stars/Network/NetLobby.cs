﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      NetLobby.h/NetLobby.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos

    OVERVIEW
    ========
    Base Class for Multiplayer Game Lobby classes
 */
using System;
using System.Collections.Generic;
using StarshatterNet.Config;
using StarshatterNet.Stars.MissionCampaign;
using DWORD = System.UInt32;
using WORD = System.UInt16;

namespace StarshatterNet.Stars.Network
{
    public class NetLobby
    {

        public NetLobby(bool temporary = false) { throw new NotImplementedException(); }
        //public virtual ~NetLobby();

        public virtual bool IsClient()
        {
            return false;
        }
        public virtual bool IsServer() { return false; }
        public virtual bool IsActive() { return active; }

        public virtual void ExecFrame() { throw new NotImplementedException(); }
        public virtual void Recv() { throw new NotImplementedException(); }
        public virtual void Send() { throw new NotImplementedException(); }
        public virtual int GetLastError() { return 0; }

        public virtual NetUser FindUserByAddr(NetAddr addr) { throw new NotImplementedException(); }
        public virtual NetUser FindUserByName(string name) { throw new NotImplementedException(); }
        public virtual NetUser FindUserByNetID(DWORD id) { throw new NotImplementedException(); }

        public virtual void BanUser(NetUser user) { throw new NotImplementedException(); }
        public virtual void AddUser(NetUser user) { throw new NotImplementedException(); }
        public virtual void DelUser(NetUser user) { throw new NotImplementedException(); }
        public virtual bool SetUserHost(NetUser user, bool host) { throw new NotImplementedException(); }
        public virtual int NumUsers() { throw new NotImplementedException(); }
        public virtual NetUser GetHost() { throw new NotImplementedException(); }
        public virtual bool HasHost() { throw new NotImplementedException(); }
        public virtual bool IsHost() { return false; }
        public virtual List<NetUser> GetUsers() { throw new NotImplementedException(); }

        public virtual List<ModInfo> GetServerMods() { throw new NotImplementedException(); }

        public virtual NetUser GetLocalUser() { throw new NotImplementedException(); }
        public virtual void SetLocalUser(NetUser user) { throw new NotImplementedException(); }

        public virtual int GetStatus() { return status; }
        public virtual void SetStatus(int s) { status = s; }

        public virtual void AddChat(NetUser user, string msg, bool route = true) { throw new NotImplementedException(); }
        public virtual List<NetChatEntry> GetChat() { throw new NotImplementedException(); }
        public virtual void ClearChat() { throw new NotImplementedException(); }
        public virtual void SaveChat() { }
        public virtual DWORD GetStartTime() { return start_time; }

        public virtual List<NetCampaignInfo> GetCampaigns() { throw new NotImplementedException(); }

        public virtual void AddUnitMap(MissionElement elem, int index = 0) { throw new NotImplementedException(); }
        public virtual List<NetUnitEntry> GetUnitMap() { throw new NotImplementedException(); }
        public virtual void ClearUnitMap() { throw new NotImplementedException(); }
        public virtual void MapUnit(int n, string user, bool lock_ = false) { throw new NotImplementedException(); }
        public virtual void UnmapUnit(string user) { throw new NotImplementedException(); }
        public virtual bool IsMapped(string user) { throw new NotImplementedException(); }

        public virtual Mission GetSelectedMission() { return mission; }
        public virtual DWORD GetSelectedMissionID() { return selected_mission; }
        public virtual void SelectMission(DWORD id) { throw new NotImplementedException(); }

        public virtual string GetMachineInfo() { return machine_info; }
        public virtual WORD GetGamePort() { return 0; }

        // actions:
        public virtual bool Ping() { throw new NotImplementedException(); }
        public virtual void GameStart() { }
        public virtual void GameStop() { }
        public virtual DWORD GetLag() { throw new NotImplementedException(); }

        // instance management:
        public static NetLobby GetInstance() { return null; /* throw new NotImplementedException(); */ }
        public static bool IsNetLobbyClient() { throw new NotImplementedException(); }
        public static bool IsNetLobbyServer() { throw new NotImplementedException(); }


        protected virtual void DoPing(NetPeer peer, string msg) { }
        protected virtual void DoServerInfo(NetPeer peer, string msg) { }
        protected virtual void DoServerMods(NetPeer peer, string msg) { }
        protected virtual void DoLogin(NetPeer peer, string msg) { }
        protected virtual void DoLogout(NetPeer peer, string msg) { }
        protected virtual void DoAuthUser(NetPeer peer, string msg) { }
        protected virtual void DoUserAuth(NetPeer peer, string msg) { }
        protected virtual void DoChat(NetPeer peer, string msg) { }
        protected virtual void DoUserList(NetPeer peer, string msg) { }
        protected virtual void DoBanUser(NetPeer peer, string msg) { }
        protected virtual void DoMissionList(NetPeer peer, string msg) { }
        protected virtual void DoMissionSelect(NetPeer peer, string msg) { }
        protected virtual void DoMissionData(NetPeer peer, string msg) { }
        protected virtual void DoUnitList(NetPeer peer, string msg) { }
        protected virtual void DoMapUnit(NetPeer peer, string msg) { }
        protected virtual void DoGameStart(NetPeer peer, string msg) { }
        protected virtual void DoGameStop(NetPeer peer, string msg) { }
        protected virtual void DoExit(NetPeer peer, string msg) { }

        protected virtual void ParseMsg(string msg, List<NetLobbyParam> params_) { throw new NotImplementedException(); }

        protected NetLink link;
        protected NetUser local_user;
        protected List<NetUser> users;
        protected List<NetChatEntry> chat_log;
        protected List<NetCampaignInfo> campaigns;
        protected List<NetUnitEntry> unit_map;
        protected string machine_info;
        protected List<ModInfo> server_mods;

        bool active;
        DWORD last_send_time;
        DWORD start_time;
        DWORD selected_mission;
        Mission mission;
        int status;
    }
    public class NetLobbyParam
    {
        public NetLobbyParam(string n, string v)
        {
            name = n; value = v;
        }

        //TODO public int operator ==(const NetLobbyParam& p)   { return name == p.name; }

        public string name;
        public string value;
    }

    // +-------------------------------------------------------------------+

    public class NetUnitEntry
    {


        public NetUnitEntry(MissionElement elem, int index = 0) { throw new NotImplementedException(); }
        public NetUnitEntry(string elem, string design, int iff, int index = 0) { throw new NotImplementedException(); }
        // public ~NetUnitEntry();

        //TODO public int operator ==(const NetUnitEntry& e) const { return (elem == e.elem) && (index == e.index); }

        public string GetDescription() { throw new NotImplementedException(); }
        public string GetUserName() { return user; }
        public string GetElemName() { return elem; }
        public string GetDesign() { return design; }
        public int GetIFF() { return iff; }
        public int GetIndex() { return index; }
        public int GetLives() { return lives; }
        public int GetIntegrity() { return hull; }
        public int GetMissionRole() { return role; }
        public bool GetLocked() { return lock_; }

        public void SetUserName(string u) { user = u; }
        public void SetElemName(string e) { elem = e; }
        public void SetDesign(string d) { design = d; }
        public void SetIFF(int i) { iff = i; }
        public void SetIndex(int i) { index = i; }
        public void SetLives(int i) { lives = i; }
        public void SetIntegrity(int i) { hull = i; }
        public void SetMissionRole(int i) { role = i; }
        public void SetLock(bool l) { lock_ = l; }


        private string user;
        private string elem;
        private string design;
        private int iff;
        private int index;
        private int lives;
        private int hull;
        private int role;
        private bool lock_;
    }

    // +--------------------------------------------------------------------+

    public class NetServerInfo
    {
        public enum STATUS { OFFLINE, LOBBY, BRIEFING, ACTIVE, DEBRIEFING, PERSISTENT };

        public NetServerInfo() { throw new NotImplementedException(); }

        public string name;
        public string hostname;
        public string password;
        public string type;
        public NetAddr addr;
        public WORD port;
        public WORD gameport;
        public bool save;

        public string version;
        public string machine_info;
        public int nplayers;
        public int hosted;
        public int status;

        public DWORD ping_time;
    }

    // +--------------------------------------------------------------------+

    public class NetCampaignInfo
    {

        public NetCampaignInfo() { id = 0;  }
        // public ~NetCampaignInfo() { }

        public int id;
        public string name;

        public List<MissionInfo> missions;
    }

    // +--------------------------------------------------------------------+

    public enum NET_LOBBY_MESSAGES
    {
        NET_LOBBY_PING = 0x10,
        NET_LOBBY_SERVER_INFO,
        NET_LOBBY_SERVER_MODS,
        NET_LOBBY_LOGIN,
        NET_LOBBY_LOGOUT,
        NET_LOBBY_CHAT,
        NET_LOBBY_USER_LIST,
        NET_LOBBY_BAN_USER,
        NET_LOBBY_MISSION_LIST,
        NET_LOBBY_MISSION_SELECT,
        NET_LOBBY_MISSION_DATA,
        NET_LOBBY_UNIT_LIST,
        NET_LOBBY_MAP_UNIT,

        NET_LOBBY_AUTH_USER,
        NET_LOBBY_USER_AUTH,

        NET_LOBBY_GAME_START,
        NET_LOBBY_GAME_STOP,
        NET_LOBBY_EXIT
    }
}