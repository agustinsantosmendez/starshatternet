﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CampaignPlanEvent.h/CampaignPlanEvent.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CampaignPlanEvent generates simulated combat
    events based on a statistical analysis of the
    combatants within the context of a dynamic
    campaign.
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Stars.MissionCampaign.Planning
{
#warning CampaignPlanEvent class is still in development and is not recommended for production.
    public class CampaignPlanEvent : CampaignPlan
    {
        public CampaignPlanEvent(Campaign c) : base(c)
        {
            event_time = 0;

            if (campaign != null)
            {
                event_time = (int)campaign.GetTime();
            }
        }
        // ~CampaignPlanEvent();

        // operations:
        public override void ExecFrame()
        {
            if (campaign != null && campaign.IsActive())
            {
                if (campaign.GetPlayerGroup() == null)
                    return;

                // once every twenty minutes is plenty:
                if (Campaign.Stardate() - exec_time < 1200)
                    return;

                if (!ExecScriptedEvents())
                    ExecStatisticalEvents();

                exec_time = Campaign.Stardate();
                event_time = (int)campaign.GetTime();
            }
        }
        public override void SetLockout(int seconds)
        {
            exec_time = Campaign.Stardate() + seconds;
        }


        public virtual bool ExecScriptedEvents()
        {
            bool scripted_event = false;

            if (campaign != null)
            {
                foreach (CombatAction action in campaign.GetActions())
                {

                    if (action.IsAvailable())
                    {

                        switch (action.Type())
                        {
                            case CombatAction.TYPE.COMBAT_EVENT:
                                {
                                    CombatEvent evnt = new CombatEvent(campaign,
                                                                        (CombatEvent.EVENT_TYPE)action.Subtype(),
                                                                        (int)campaign.GetTime(),
                                                                        action.GetIFF(),
                                                                        action.Source(),
                                                                        action.Region());

                                    if (evnt == null)
                                        return false;

                                    evnt.SetTitle(action.GetText());

                                    if (!string.IsNullOrEmpty(action.Filename()))
                                        evnt.SetFilename(action.Filename());

                                    if (!string.IsNullOrEmpty(action.ImageFile()))
                                        evnt.SetImageFile(action.ImageFile());

                                    if (!string.IsNullOrEmpty(action.SceneFile()))
                                        evnt.SetSceneFile(action.SceneFile());

                                    evnt.Load();

                                    ProsecuteKills(action);
                                    campaign.GetEvents().Add(evnt);

                                    action.FireAction();
                                    scripted_event = true;

                                    if (action.Subtype() == (int)CombatEvent.EVENT_TYPE.CAMPAIGN_END)
                                    {
                                        ErrLogger.PrintLine(">>>>> CAMPAIGN {0} END  (Action {1:d3}) <<<<<\n", campaign.GetCampaignId(), action.Identity());
                                        campaign.SetStatus(Campaign.STATUS.CAMPAIGN_SUCCESS);
                                    }

                                    else if (action.Subtype() == (int)CombatEvent.EVENT_TYPE.CAMPAIGN_FAIL)
                                    {
                                        ErrLogger.PrintLine(">>>>> CAMPAIGN {0} FAIL (Action {1:d3}) <<<<<\n", campaign.GetCampaignId(), action.Identity());
                                        campaign.SetStatus(Campaign.STATUS.CAMPAIGN_FAILED);
                                    }
                                }
                                break;

                            case CombatAction.TYPE.STRATEGIC_DIRECTIVE:
                                {
                                    CombatGroup g = campaign.FindGroup(action.GetIFF(),
                                                                        (CombatGroup.GROUP_TYPE)action.AssetType(),
                                                                        action.AssetId());

                                    if (g != null)
                                    {
                                        g.SetStrategicDirection(action.GetText());
                                        action.FireAction();
                                    }
                                    else
                                    {
                                        action.FailAction();
                                    }

                                    scripted_event = true;
                                }
                                break;

                            case CombatAction.TYPE.CAMPAIGN_SITUATION:
                                {
                                    campaign.SetSituation(action.GetText());
                                    action.FireAction();
                                    scripted_event = true;
                                }
                                break;

                            case CombatAction.TYPE.CAMPAIGN_ORDERS:
                                {
                                    campaign.SetOrders(action.GetText());
                                    action.FireAction();
                                    scripted_event = true;
                                }
                                break;

                            case CombatAction.TYPE.INTEL_EVENT:
                                {
                                    CombatGroup g = campaign.FindGroup(action.GetIFF(),
                                                                        (CombatGroup.GROUP_TYPE)action.AssetType(),
                                                                        action.AssetId());

                                    if (g != null)
                                    {
                                        g.SetIntelLevel((Intel.INTEL_TYPE)action.Subtype());
                                        action.FireAction();
                                    }
                                    else
                                    {
                                        ErrLogger.PrintLine("WARNING: Action {0} (intel level) Could not find group (IFF:{1}, type:{2}, id:{3})\n",
                                                               action.Identity(),
                                                               action.GetIFF(),
                                                               action.AssetType(),
                                                               action.AssetId());

                                        action.FailAction();
                                    }

                                    scripted_event = true;
                                }
                                break;

                            case CombatAction.TYPE.ZONE_ASSIGNMENT:
                                {
                                    CombatGroup g = campaign.FindGroup(action.GetIFF(),
                                                                        (CombatGroup.GROUP_TYPE)action.AssetType(),
                                                                        action.AssetId());

                                    if (g != null)
                                    {
                                        bool found = false;

                                        if (action.Region() != null)
                                        {
                                            CombatZone zone = campaign.GetZone(action.Region());

                                            if (zone != null)
                                            {
                                                g.SetAssignedZone(zone);
                                                g.SetZoneLock(true);
                                                found = true;

                                                // don't announce the move unless it's for the player's team:
                                                if (action.GetIFF() == campaign.GetPlayerIFF() && !action.GetText().Equals("do-not-display", StringComparison.InvariantCultureIgnoreCase))
                                                {
                                                    CombatEvent evnt = new CombatEvent(campaign,
                                                                                    CombatEvent.EVENT_TYPE.MOVE_TO,
                                                                                    (int)campaign.GetTime(),
                                                                                    action.GetIFF(),
                                                                                    CombatEvent.EVENT_SOURCE.FORCOM,
                                                                                    action.Region());

                                                    if (evnt == null)
                                                        return false;

                                                    string title = g.Name() + " Orders: Proceed to " + action.Region() + " Sector";
                                                    evnt.SetTitle(title);

                                                    double eta = campaign.GetTime() + 3600;
                                                    eta -= eta % 1800;

                                                    string text;
                                                    FormatUtil.FormatDayTime(out text, eta);

                                                    string info = "ORDERS:\n\nEffective immediately, ";
                                                    info += g.GetDescription();
                                                    info += " and all associated units shall proceed to ";
                                                    info += action.Region();
                                                    info += " sector and commence spaceborne operations in that area.  ETA rendevous point ";
                                                    info += text;
                                                    info += ".\n\nFleet Admiral A. Evars FORCOM\nCommanding";

                                                    evnt.SetInformation(info);

                                                    if (!string.IsNullOrEmpty(action.ImageFile()))
                                                        evnt.SetImageFile(action.ImageFile());

                                                    if (!string.IsNullOrEmpty(action.SceneFile()))
                                                        evnt.SetSceneFile(action.SceneFile());

                                                    evnt.Load();
                                                    campaign.GetEvents().Add(evnt);
                                                }
                                            }
                                        }

                                        if (!found)
                                        {
                                            ErrLogger.PrintLine("WARNING: Action {0} Could not find assigned zone '{1}' for '{2}'\n",
                                                                action.Identity(),
                                                                action.Region() != null ? action.Region() : "NULL",
                                                                g.Name());

                                            g.SetAssignedZone(null);
                                        }

                                        action.FireAction();
                                    }
                                    else
                                    {
                                        ErrLogger.PrintLine("WARNING: Action {0} (zone assignment) Could not find group (IFF:{1}, type:{2}, id:{3})\n",
                                                            action.Identity(),
                                                            action.GetIFF(),
                                                            action.AssetType(),
                                                            action.AssetId());

                                        action.FailAction();
                                    }

                                    scripted_event = true;
                                }
                                break;

                            case CombatAction.TYPE.SYSTEM_ASSIGNMENT:
                                {
                                    CombatGroup g = campaign.FindGroup(action.GetIFF(),
                                                                       (CombatGroup.GROUP_TYPE)action.AssetType(),
                                                                        action.AssetId());

                                    if (g != null)
                                    {
                                        bool found = false;

                                        if (action.System() != null)
                                        {
                                            string system = action.System();

                                            if (campaign.GetSystem(system) != null)
                                            {
                                                g.SetAssignedSystem(system);
                                                found = true;

                                                // don't announce the move unless it's for the player's team:
                                                if (action.GetIFF() == campaign.GetPlayerIFF() && !action.GetText().Equals("do-not-display", StringComparison.InvariantCultureIgnoreCase))
                                                {
                                                    CombatEvent evnt = new CombatEvent(campaign,
                                                                                    CombatEvent.EVENT_TYPE.MOVE_TO,
                                                                                    (int)campaign.GetTime(),
                                                                                    action.GetIFF(),
                                                                                    CombatEvent.EVENT_SOURCE.FORCOM,
                                                                                    action.Region());

                                                    if (evnt == null)
                                                        return false;

                                                    string title = g.Name() + " Orders: Proceed to " + action.System() + " System";
                                                    evnt.SetTitle(title);

                                                    double eta = campaign.GetTime() + 3600;
                                                    eta -= eta % 1800;

                                                    string text;
                                                    FormatUtil.FormatDayTime(out text, eta);

                                                    string info = "ORDERS:\n\nEffective immediately, ";
                                                    info += g.GetDescription();
                                                    info += " and all associated units shall proceed to the ";
                                                    info += action.System();
                                                    info += " star system and commence spaceborne operations in that area.  ETA rendevous point ";
                                                    info += text;
                                                    info += ".\n\nFleet Admiral A. Evars FORCOM\nCommanding";

                                                    evnt.SetInformation(info);

                                                    if (!string.IsNullOrEmpty(action.ImageFile()))
                                                        evnt.SetImageFile(action.ImageFile());

                                                    if (!string.IsNullOrEmpty(action.SceneFile()))
                                                        evnt.SetSceneFile(action.SceneFile());

                                                    evnt.Load();
                                                    campaign.GetEvents().Add(evnt);
                                                }
                                            }
                                        }

                                        if (!found)
                                        {
                                            ErrLogger.PrintLine("WARNING: Action {0} Could not find assigned system '{1}' for '{2}'\n",
                                                            action.Identity(),
                                                            action.System() != null ? action.System() : "NULL",
                                                            g.Name());

                                            g.SetAssignedSystem("");
                                        }

                                        action.FireAction();
                                    }
                                    else
                                    {
                                        ErrLogger.PrintLine("WARNING: Action {0} (system assignment) Could not find group (IFF:{1}, type:{2}, id:{3})\n",
                                                            action.Identity(),
                                                            action.GetIFF(),
                                                            action.AssetType(),
                                                            action.AssetId());

                                        action.FailAction();
                                    }

                                    scripted_event = true;
                                }
                                break;

                            case CombatAction.TYPE.NO_ACTION:
                                action.FireAction();
                                scripted_event = true;
                                break;

                            default:
                                break;
                        }
                    }
                }
            }

            return scripted_event;
        }
        public virtual bool ExecStatisticalEvents()
        {
            bool result = false;

            if (campaign != null)
            {
                foreach (Combatant c in campaign.GetCombatants())
                {
                    if (result) break;
                    CombatAssignment a = ChooseAssignment(c.GetForce());

                    // prefer assignments not in player's zone:
                    if (a != null)
                    {
                        CombatGroup objective = a.GetObjective();
                        CombatGroup player = campaign.GetPlayerGroup();

                        if (objective != null && player != null &&
                                objective.GetCurrentZone() == player.GetCurrentZone())
                            a = ChooseAssignment(c.GetForce());
                    }

                    if (a != null)
                    {
                        result = CreateEvent(a);
                    }
                }
            }

            return result;
        }


        protected virtual void ProsecuteKills(CombatAction action)
        {
            if (action.AssetKills().Count > 0)
            {
                CombatGroup g = campaign.FindGroup(action.GetIFF(),
                                                    (CombatGroup.GROUP_TYPE)action.AssetType(),
                                                    action.AssetId());

                if (g != null)
                {
                    foreach (string name in action.AssetKills())
                    {
                        CombatUnit asset = g.FindUnit(name);

                        if (asset != null)
                        {
                            int value_killed = asset.Kill(1);

                            foreach (Combatant c in campaign.GetCombatants())
                            {
                                if (c.GetIFF() > 0 && c.GetIFF() != asset.GetIFF())
                                {
                                    // damage to neutral assets must be scored to bad guys:
                                    if (asset.GetIFF() > 0 || c.GetIFF() > 1)
                                    {
                                        c.AddScore(value_killed);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (action.TargetKills().Count > 0)
            {
                CombatGroup g = campaign.FindGroup(action.TargetIFF(),
                                                    (CombatGroup.GROUP_TYPE)action.TargetType(),
                                                    action.TargetId());

                if (g != null)
                {
                    foreach (string name in action.TargetKills())
                    {
                        CombatUnit target = g.FindUnit(name);

                        if (target != null)
                        {
                            int value_killed = target.Kill(1);

                            foreach (Combatant c in campaign.GetCombatants())
                            {
                                if (c.GetIFF() > 0 && c.GetIFF() != target.GetIFF())
                                {
                                    // damage to neutral assets must be scored to bad guys:
                                    if (target.GetIFF() > 0 || c.GetIFF() > 1)
                                    {
                                        c.AddScore(value_killed);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        static void FindAssignments(CombatGroup g, List<CombatAssignment> alist)
        {
            if (g == null) return;

            alist.AddRange(g.GetAssignments());

            foreach (CombatGroup iter in g.GetComponents())
                FindAssignments(iter, alist);
        }

        protected virtual CombatAssignment ChooseAssignment(CombatGroup g)
        {
            List<CombatAssignment> alist = new List<CombatAssignment>();
            FindAssignments(g, alist);

            int tries = 5;

            if (alist.Count > 0)
            {
                while (tries-- > 0)
                {
                    int index = (int)RandomHelper.Random(0, alist.Count);

                    if (index >= alist.Count)
                        index = 0;

                    CombatAssignment a = alist[index];

                    if (a == null) continue;

                    CombatGroup resource = a.GetResource();
                    CombatGroup objective = a.GetObjective();

                    if (resource == null || objective == null)
                        continue;

                    if (resource.IsReserve() || objective.IsReserve())
                        continue;

                    if (resource.CalcValue() < 50 || objective.CalcValue() < 50)
                        continue;

                    if (resource == campaign.GetPlayerGroup() || objective == campaign.GetPlayerGroup())
                        continue;

                    return a;
                }
            }

            return null;
        }
        protected virtual bool CreateEvent(CombatAssignment a)
        {
            CombatEvent evnt = null;

            if (campaign != null && a != null && a.GetResource() != null && RandomHelper.RandomChance(1, 2))
            {
                event_time = (int)RandomHelper.Random(event_time, campaign.GetTime());

                CombatGroup group = a.GetResource();

                if (group == campaign.GetPlayerGroup())
                {

                    if (group.Type() == CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON ||
                            group.Type() == CombatGroup.GROUP_TYPE.BATTLE_GROUP ||
                            group.Type() == CombatGroup.GROUP_TYPE.CARRIER_GROUP)
                    {

                        return false;
                    }
                }

                CombatGroup target = a.GetObjective();

                if (target != null && target == campaign.GetPlayerGroup())
                {

                    if (target.Type() == CombatGroup.GROUP_TYPE.DESTROYER_SQUADRON ||
                            target.Type() == CombatGroup.GROUP_TYPE.BATTLE_GROUP ||
                            target.Type() == CombatGroup.GROUP_TYPE.CARRIER_GROUP)
                    {

                        return false;
                    }
                }

                switch (a.Type())
                {
                    case Mission.TYPE.DEFEND:
                        evnt = CreateEventDefend(a);
                        break;

                    case Mission.TYPE.ASSAULT:
                        if (group.IsStarshipGroup())
                            evnt = CreateEventStarship(a);
                        else
                            evnt = CreateEventFighterAssault(a);
                        break;

                    case Mission.TYPE.STRIKE:
                        if (group.IsStarshipGroup())
                            evnt = CreateEventStarship(a);
                        else
                            evnt = CreateEventFighterStrike(a);
                        break;

                    case Mission.TYPE.SWEEP:
                        evnt = CreateEventFighterSweep(a);
                        break;
                }

                if (evnt != null)
                {
                    campaign.GetEvents().Add(evnt);
                    return true;
                }
            }

            return false;
        }

        protected virtual CombatEvent CreateEventDefend(CombatAssignment a)
        {
            bool friendly = IsFriendlyAssignment(a);

            if (!friendly)
                return null;

            CombatEvent evnt = null;
            CombatGroup group = a.GetResource();
            CombatGroup obj = a.GetObjective();
            CombatUnit unit = group.GetRandomUnit();
            CombatUnit tgt = obj.GetRandomUnit();

            if (unit == null || tgt == null)
                return null;

            bool success = Success(a);
            string rgn = group.GetRegion();
            string title = group.Name() + " in Defensive Engagement";
            string info;

            evnt = new CombatEvent(campaign,
             CombatEvent.EVENT_TYPE.DEFEND,
             event_time,
             group.GetIFF(),
             CombatEvent.EVENT_SOURCE.TACNET,
             rgn);

            if (evnt == null)
                return null;

            int tgt_count = 0;
            int unit_count = 0;

            if (!success)
            {
                if (tgt != null)
                {
                    if (tgt.Kill(1) > 0)
                        tgt_count++;
                    Combatant c = group.GetCombatant();
                    if (c != null) c.AddScore(tgt.GetSingleValue());
                }

                if (unit != null && RandomHelper.RandomChance(1, 5))
                {
                    if (unit.Kill(1) > 0)
                        unit_count++;
                    Combatant c = obj.GetCombatant();
                    if (c != null) c.AddScore(unit.GetSingleValue());
                }
            }

            CombatGroup us = group;
            CombatGroup them = obj;
            int us_count = unit_count;
            int them_count = tgt_count;

            if (obj.IsStrikeTarget())
            {
                info = "EVENT:  " + rgn + " Sector\n\n";
            }

            else
            {
                info = "MISSION:  Escort " + obj.Name() + ", " + rgn + " Sector\n\n";
            }

            info += GetTeamName(group);
            info += " " + group.GetDescription();

            if (success)
                info += " successfully defended ";
            else
                info += " was unable to defend ";

            info += GetTeamName(obj);
            info += " " + obj.GetDescription() + ".\n\n";

            // need to find an enemy group to do the attacking...

            evnt.SetTitle(title);
            evnt.SetInformation(info);
            return evnt;
        }
        protected virtual CombatEvent CreateEventFighterAssault(CombatAssignment a)
        {
            CombatEvent evnt = null;
            CombatGroup group = a.GetResource();
            CombatGroup obj = a.GetObjective();
            CombatUnit unit = group.GetRandomUnit();
            CombatUnit tgt = obj.GetRandomUnit();

            if (unit == null || tgt == null)
                return null;

            bool success = Success(a);
            string rgn = group.GetRegion();
            string title = group.Name();
            string info;

            evnt = new CombatEvent(campaign,
             CombatEvent.EVENT_TYPE.ATTACK,
             event_time,
             group.GetIFF(),
             CombatEvent.EVENT_SOURCE.TACNET,
             rgn);

            if (evnt == null)
                return null;

            title += " Assault " + obj.Name();

            int tgt_count = 0;
            int unit_count = 0;

            if (success)
            {
                if (tgt != null)
                {
                    int killed = tgt.Kill(1 + tgt.Count() / 2);
                    if (killed > 0)
                        tgt_count += killed / tgt.GetSingleValue();
                    Combatant c = group.GetCombatant();
                    if (c != null) c.AddScore(tgt.GetSingleValue());
                }

                if (unit != null && RandomHelper.RandomChance(1, 5))
                {
                    if (unit.Kill(1) > 0)
                        unit_count++;
                    Combatant c = obj.GetCombatant();
                    if (c != null) c.AddScore(unit.GetSingleValue());
                }
            }
            else
            {
                for (int i = 0; i < 2; i++)
                {
                    if (unit != null && RandomHelper.RandomChance(1, 4))
                    {
                        if (unit.Kill(1) > 0)
                            unit_count++;
                        Combatant c = obj.GetCombatant();
                        if (c != null) c.AddScore(unit.GetSingleValue());
                    }
                }
            }

            CombatGroup us = group;
            CombatGroup them = obj;
            int us_count = unit_count;
            int them_count = tgt_count;

            bool friendly = IsFriendlyAssignment(a);

            if (friendly)
            {
                info = "MISSION:  Strike, " + rgn + " Sector\n\n";
            }

            else
            {
                info = "EVENT:  " + rgn + " Sector\n\n";

                us = obj;
                them = group;
                us_count = tgt_count;
                them_count = unit_count;
            }

            info += GetTeamName(group);
            info += " " + group.GetDescription();

            if (success)
                info += " successfully assault ";
            else if (!friendly)
                info += " assault averted against ";
            else
                info += " attempted assault on ";

            info += GetTeamName(obj);
            info += " " + obj.GetDescription() + ".\n\n";

            string text;

            if (them_count != 0)
            {
                if (friendly)
                {
                    if (them_count > 1)
                        text = string.Format("ENEMY KILLED:\t {0} {1} destroyed\n", them_count, tgt.Name());
                    else
                        text = string.Format("ENEMY KILLED:\t {0} destroyed\n", tgt.Name());
                }
                else
                {
                    text = string.Format("ENEMY KILLED:\t {0} {1} destroyed\n", them_count, them.Name());
                }

                info += text;
            }
            else
            {
                info += "ENEMY KILLED:\t 0\n";
            }

            if (us_count != 0)
            {
                if (!friendly)
                    text = string.Format("ALLIED LOSSES:\t {0} destroyed\n", tgt.Name());
                else
                    text = string.Format("ALLIED LOSSES:\t {0} {1} destroyed", us_count, us.Name());

                info += text;
            }
            else
            {
                info += "ALLIED LOSSES:\t 0";
            }


            evnt.SetTitle(title);
            evnt.SetInformation(info);
            return evnt;
        }
        protected virtual CombatEvent CreateEventFighterStrike(CombatAssignment a)
        {
            CombatEvent evnt = null;
            CombatGroup group = a.GetResource();
            CombatGroup obj = a.GetObjective();
            CombatUnit unit = group.GetRandomUnit();
            CombatUnit tgt = obj.GetRandomUnit();

            if (unit == null || tgt == null)
                return null;

            bool success = Success(a);
            string rgn = group.GetRegion();
            string title = group.Name();
            string info;

            evnt = new CombatEvent(campaign,
             CombatEvent.EVENT_TYPE.ATTACK,
             event_time,
             group.GetIFF(),
             CombatEvent.EVENT_SOURCE.TACNET,
             rgn);

            if (evnt == null)
                return null;

            if (unit != null)
                title += " " + unit.GetDesign().abrv + "s";

            if (success)
            {
                title += " Successfully Strike " + obj.Name();
            }
            else
            {
                title += " Attempt Strike on " + obj.Name();
            }

            int tgt_count = 0;
            int unit_count = 0;

            if (success)
            {
                if (tgt != null)
                {
                    int killed = tgt.Kill(1 + tgt.Count() / 2);
                    if (killed > 0)
                        tgt_count += killed / tgt.GetSingleValue();
                    Combatant c = group.GetCombatant();
                    if (c != null) c.AddScore(tgt.GetSingleValue());
                }

                if (unit != null && RandomHelper.RandomChance(1, 5))
                {
                    if (unit.Kill(1) > 0)
                        unit_count++;
                    Combatant c = obj.GetCombatant();
                    if (c != null) c.AddScore(unit.GetSingleValue());
                }
            }
            else
            {
                for (int i = 0; i < 2; i++)
                {
                    if (unit != null && RandomHelper.RandomChance(1, 4))
                    {
                        if (unit.Kill(1) > 0)
                            unit_count++;
                        Combatant c = obj.GetCombatant();
                        if (c != null) c.AddScore(unit.GetSingleValue());
                    }
                }
            }

            CombatGroup us = group;
            CombatGroup them = obj;
            int us_count = unit_count;
            int them_count = tgt_count;

            bool friendly = IsFriendlyAssignment(a);

            if (friendly)
            {
                info = "MISSION:  Strike, " + rgn + " Sector\n\n";
            }

            else
            {
                info = "EVENT:  " + rgn + " Sector\n\n";

                us = obj;
                them = group;
                us_count = tgt_count;
                them_count = unit_count;
            }

            info += GetTeamName(group);
            info += " " + group.GetDescription();

            if (success)
                info += " successfully strike ";
            else if (!friendly)
                info += " strike against ";
            else
                info += " attempted strike on ";

            info += GetTeamName(obj);
            info += " " + obj.GetDescription();

            if (!success && !friendly)
                info += " averted.\n\n";
            else
                info += ".\n\n";

            string text;

            if (them_count != 0)
            {
                if (friendly)
                {
                    if (them_count > 1)
                        text = string.Format("ENEMY KILLED:\t {0} {1} destroyed\n", them_count, tgt.Name());
                    else
                        text = string.Format("ENEMY KILLED:\t {0} destroyed\n", tgt.Name());
                }
                else
                {
                    text = string.Format("ENEMY KILLED:\t {0} {1} destroyed\n", them_count, them.Name());
                }

                info += text;
            }
            else
            {
                info += "ENEMY KILLED:\t 0\n";
            }

            if (us_count != 0)
            {
                if (!friendly)
                    text = string.Format("ALLIED LOSSES:\t {0} destroyed\n", tgt.Name());
                else
                    text = string.Format("ALLIED LOSSES:\t {0} {1} destroyed", us_count, us.Name());

                info += text;
            }
            else
            {
                info += "ALLIED LOSSES:\t 0";
            }

            evnt.SetTitle(title);
            evnt.SetInformation(info);
            return evnt;
        }
        protected virtual CombatEvent CreateEventFighterSweep(CombatAssignment a)
        {
            CombatEvent evnt = null;
            CombatGroup group = a.GetResource();
            CombatGroup obj = a.GetObjective();
            CombatUnit unit = group.GetRandomUnit();
            CombatUnit tgt = obj.GetRandomUnit();

            if (unit == null || tgt == null)
                return null;

            bool success = Success(a);
            string rgn = group.GetRegion();
            string title = group.Name();
            string info;

            evnt = new CombatEvent(campaign,
             CombatEvent.EVENT_TYPE.ATTACK,
             event_time,
             group.GetIFF(),
             CombatEvent.EVENT_SOURCE.TACNET,
             rgn);

            if (evnt == null)
                return null;

            if (unit != null)
                title += " " + unit.GetDesign().abrv + "s";
            else
                title += " Fighters";

            if (RandomHelper.RandomChance(1, 4)) title += " Clash with ";
            else if (RandomHelper.RandomChance(1, 4)) title += " Engage ";
            else if (RandomHelper.RandomChance(1, 4)) title += " Intercept ";
            else title += " Encounter ";

            title += obj.Name();

            int tgt_count = 0;
            int unit_count = 0;

            if (success)
            {
                for (int i = 0; i < 2; i++)
                {
                    if (tgt != null && RandomHelper.RandomChance(3, 4))
                    {
                        if (tgt.Kill(1) > 0)
                            tgt_count++;
                        Combatant c = group.GetCombatant();
                        if (c != null) c.AddScore(tgt.GetSingleValue());
                    }
                }

                if (tgt_count > 1)
                {
                    if (tgt != null && RandomHelper.RandomChance(1, 4))
                    {
                        if (tgt.Kill(1) > 0)
                            tgt_count++;
                        Combatant c = group.GetCombatant();
                        if (c != null) c.AddScore(tgt.GetSingleValue());
                    }
                }

                else
                {
                    if (unit != null && RandomHelper.RandomChance(1, 5))
                    {
                        if (unit.Kill(1) > 0)
                            unit_count++;
                        Combatant c = obj.GetCombatant();
                        if (c != null) c.AddScore(unit.GetSingleValue());
                    }
                }
            }
            else
            {
                for (int i = 0; i < 2; i++)
                {
                    if (unit != null && RandomHelper.RandomChance(3, 4))
                    {
                        if (unit.Kill(1) > 0)
                            unit_count++;
                        Combatant c = obj.GetCombatant();
                        if (c != null) c.AddScore(unit.GetSingleValue());
                    }
                }

                if (tgt != null && RandomHelper.RandomChance(1, 4))
                {
                    if (tgt.Kill(1) > 0)
                        tgt_count++;
                    Combatant c = group.GetCombatant();
                    if (c != null) c.AddScore(tgt.GetSingleValue());
                }
            }

            CombatGroup us = group;
            CombatGroup them = obj;
            int us_count = unit_count;
            int them_count = tgt_count;

            bool friendly = IsFriendlyAssignment(a);

            if (!friendly)
            {
                us = obj;
                them = group;
                us_count = tgt_count;
                them_count = unit_count;
            }

            if (friendly)
            {
                if (RandomHelper.RandomChance())
                    info = "MISSION:  OCA Sweep, " + rgn + " Sector\n\n";
                else
                    info = "MISSION:  FORCAP, " + rgn + " Sector\n\n";

                info += GetTeamName(group);
                info += " " + group.GetDescription();
                info += " engaged " + GetTeamName(obj);
                info += " " + obj.GetDescription() + ".\n\n";
            }
            else
            {
                info = "MISSION:  Patrol, " + rgn + " Sector\n\n";

                info += GetTeamName(obj);
                info += " " + obj.GetDescription();
                info += " engaged " + GetTeamName(group);
                info += " " + group.GetDescription() + ".\n\n";
            }

            string text;

            if (them_count != 0)
            {
                text = string.Format("ENEMY KILLED:\t {0} {1} destroyed\n", them_count, them.Name());

                info += text;
            }
            else
            {
                info += "ENEMY KILLED:\t 0\n";
            }

            if (us_count != 0)
            {
                text = string.Format("ALLIED LOSSES:\t {0} {1} destroyed", us_count, us.Name());
                info += text;
            }
            else
            {
                info += "ALLIED LOSSES:\t 0";
            }

            evnt.SetTitle(title);
            evnt.SetInformation(info);
            return evnt;
        }
        protected virtual CombatEvent CreateEventStarship(CombatAssignment a)
        {
            CombatEvent evnt = null;
            CombatGroup group = a.GetResource();
            CombatGroup obj = a.GetObjective();
            CombatUnit unit = group.GetRandomUnit();
            CombatUnit tgt = obj.GetRandomUnit();

            if (unit == null || tgt == null)
                return null;

            bool success = Success(a);
            string rgn = group.GetRegion();
            string title = group.Name();
            string info;

            evnt = new CombatEvent(campaign,
             CombatEvent.EVENT_TYPE.ATTACK,
             event_time,
             group.GetIFF(),
             CombatEvent.EVENT_SOURCE.TACNET,
             group.GetRegion());

            if (evnt == null)
                return null;

            title += " Assaults " + a.GetObjective().Name();

            int tgt_count = 0;
            int unit_count = 0;

            if (success)
            {
                if (tgt != null)
                {
                    if (tgt.Kill(1) > 0)
                        tgt_count++;
                    Combatant c = group.GetCombatant();
                    if (c != null) c.AddScore(tgt.GetSingleValue());
                }

                if (unit != null && RandomHelper.RandomChance(1, 5))
                {
                    if (unit.Kill(1) > 0)
                        unit_count++;
                    Combatant c = obj.GetCombatant();
                    if (c != null) c.AddScore(unit.GetSingleValue());
                }
            }
            else
            {
                for (int i = 0; i < 2; i++)
                {
                    if (unit != null && RandomHelper.RandomChance(1, 4))
                    {
                        if (unit.Kill(1) > 0)
                            unit_count++;
                        Combatant c = obj.GetCombatant();
                        if (c != null) c.AddScore(unit.GetSingleValue());
                    }
                }
            }

            CombatGroup us = group;
            CombatGroup them = obj;
            int us_count = unit_count;
            int them_count = tgt_count;

            bool friendly = IsFriendlyAssignment(a);

            if (friendly)
            {
                info = "MISSION:  Fleet Action, " + rgn + " Sector\n\n";
            }

            else
            {
                info = "EVENT:  " + rgn + " Sector\n\n";

                us = obj;
                them = group;
                us_count = tgt_count;
                them_count = unit_count;
            }

            info += GetTeamName(group);
            info += " " + group.GetDescription();

            if (success)
                info += " successfully assaulted ";
            else if (!friendly)
                info += " assault against ";
            else
                info += " attempted assault on ";

            info += GetTeamName(obj);
            info += " " + obj.GetDescription();

            if (!success && !friendly)
                info += " failed.\n\n";
            else
                info += ".\n\n";

            string text;

            if (them_count != 0)
            {
                if (friendly)
                {
                    if (tgt.Count() > 1)
                    {
                        text = string.Format("ENEMY KILLED:\t {0} {1} destroyed\n", them_count, tgt.Name());
                    }
                    else
                    {
                        text = string.Format("ENEMY KILLED:\t {0} destroyed\n", tgt.Name());
                    }
                }
                else
                {
                    if (unit.Count() > 1)
                    {
                        text = string.Format("ENEMY KILLED:\t {0} {1} destroyed\n", them_count, unit.Name());
                    }
                    else
                    {
                        text = string.Format("ENEMY KILLED:\t {0} destroyed\n", unit.Name());
                    }
                }

                info += text;
            }
            else
            {
                info += "ENEMY KILLED:\t 0\n";
            }

            if (us_count != 0)
            {
                if (!friendly)
                    text = string.Format("ALLIED LOSSES:\t {0} destroyed\n", tgt.Name());
                else
                    text = string.Format("ALLIED LOSSES:\t {0} destroyed", unit.Name());

                info += text;
            }
            else
            {
                info += "ALLIED LOSSES:\t 0";
            }

            evnt.SetTitle(title);
            evnt.SetInformation(info);
            return evnt;
        }

        protected virtual bool IsFriendlyAssignment(CombatAssignment a)
        {
            if (campaign == null || a == null || a.GetResource() == null)
                return false;

            int a_team = a.GetResource().GetIFF();
            CombatGroup player = campaign.GetPlayerGroup();

            if (player != null && (player.GetIFF() == a_team))
                return true;

            return false;
        }


        protected virtual bool Success(CombatAssignment a)
        {
            if (campaign == null || a == null || a.GetResource() == null)
                return false;

            int odds = 6 - campaign.GetCampaignId();

            if (odds < 1)
                odds = 1;

            bool success = RandomHelper.RandomChance(odds, 5);

            if (!IsFriendlyAssignment(a))
                success = !success;

            return success;
        }

        protected virtual string GetTeamName(CombatGroup g)
        {
            while (g.GetParent() != null)
                g = g.GetParent();

            return g.Name();
        }


        // attributes:
        protected int event_time;
    }
}
