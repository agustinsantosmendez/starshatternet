﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      RadioTraffic.h/RadioTraffic.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    RadioTraffic maintains a history of all messages sent between ships
    in the simulation.  This class also handles displaying relevant
    traffic to the player.
*/
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.Views;
using System.Collections.Generic;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning RadioTraffic class is still in development and is not recommended for production.
    public class RadioTraffic
    {
        public RadioTraffic()
        {
            radio_traffic = this;
        }

        //~RadioTraffic();

        // accessors:
        public static void Initialize()
        {
            if (radio_traffic == null)
                radio_traffic = new RadioTraffic();
        }

        public static void Close()
        {
            //delete radio_traffic;
            radio_traffic = null;
        }


        public static RadioTraffic GetInstance() { return radio_traffic; }

        public static void SendQuickMessage(Ship ship, RadioMessage.ACTION action)
        {
            if (ship == null) return;

            Element elem = ship.GetElement();

            if (elem != null)
            {
                if (action >= RadioMessage.ACTION.REQUEST_PICTURE)
                {
                    Ship controller = ship.GetController();

                    if (controller != null && !ship.IsStarship())
                    {
                        RadioMessage msg = new RadioMessage(controller, ship, action);
                        Transmit(msg);
                    }
                }
                else if (action >= RadioMessage.ACTION.SPLASH_1 && action <= RadioMessage.ACTION.DISTRESS)
                {
                    RadioMessage msg = new RadioMessage((Element)null, ship, action);
                    Transmit(msg);
                }
                else
                {
                    RadioMessage msg = new RadioMessage(elem, ship, action);

                    if (action == RadioMessage.ACTION.ATTACK || action == RadioMessage.ACTION.ESCORT)
                        msg.AddTarget(ship.GetTarget());

                    Transmit(msg);
                }
            }
        }


        public static void Transmit(RadioMessage msg)
        {
#if TODO
            if (msg != null && radio_traffic != null)
            {
                NetGame net_game = NetGame.GetInstance();
                if (net_game != null)
                {
                    NetCommMsg net_msg = new NetCommMsg();
                    net_msg.SetRadioMessage(msg);
                    net_game.SendData(net_msg);
                }

                radio_traffic.SendMessage(msg);
            }
#endif
            throw new System.NotImplementedException();
        }

        public static void DiscardMessages()
        {
            if (radio_traffic != null)
                radio_traffic.traffic.Clear();
        }

        public static string TranslateVox(string phrase)
        {
            string vox = "vox.";
            vox += phrase;
            vox = localeManager.GetText(vox.ToLower());

            if (vox.Contains("vox."))  // failed to translate
                return phrase;    // return the original text

            return vox;
        }


        public void SendMessage(RadioMessage msg)
        {
#if TODO
            if (msg == null) return;

            Sim sim = Sim.GetSim();
            Ship player = sim.GetPlayerShip();
            int iff = 0;

            if (player != null)
                iff = player.GetIFF();

            if (msg.DestinationShip() != null)
            {
                traffic.Add(msg);

                if (msg.Channel() == 0 || msg.Channel() == iff)
                    DisplayMessage(msg);

                if (NetGame.IsNetGameClient())
                    msg.DestinationShip().HandleRadioMessage(msg);
            }

            else if (msg.DestinationElem() != null)
            {
                traffic.Add(msg);

                if (msg.Channel() == 0 || msg.Channel() == iff)
                    DisplayMessage(msg);

                if (!NetGame.IsNetGameClient())
                    msg.DestinationElem().HandleRadioMessage(msg);
            }

            else
            {
                if (msg.Channel() == 0 || msg.Channel() == iff)
                    DisplayMessage(msg);

                //delete msg;
            }
#endif
            throw new System.NotImplementedException();
        }

        public void DisplayMessage(RadioMessage msg)
        {
            if (msg == null) return;

            string txt_buf;
            string msg_buf;
            string src_buf = null;
            string dst_buf = null;
            string act_buf;
            int vox_channel = 0;

            Ship dst_ship = msg.DestinationShip();
            Element dst_elem = msg.DestinationElem();

            // BUILD SRC AND DST BUFFERS -------------------

            if (msg.Sender() != null)
            {
                Ship sender = msg.Sender();

                // orders to self?
                if (dst_elem != null && dst_elem.NumShips() == 1 && dst_elem.GetShip(1) == sender)
                {
                    if (msg.Action() >= RadioMessage.ACTION.CALL_ENGAGING)
                    {
                        src_buf = sender.Name();

                        if (sender.IsStarship())
                            vox_channel = (sender.Identity() % 3) + 5;
                    }
                }

                // orders to other ships:
                else
                {
                    if (sender.IsStarship())
                    {
                        vox_channel = (sender.Identity() % 3) + 5;
                    }
                    else
                    {
                        vox_channel = sender.GetElementIndex();
                    }

                    if (msg.Action() >= RadioMessage.ACTION.CALL_ENGAGING)
                    {
                        src_buf = sender.Name();
                    }
                    else
                    {
                        src_buf = string.Format("This is {0}", sender.Name());

                        if (dst_ship != null)
                        {
                            // internal announcement
                            if (dst_ship.GetElement() == sender.GetElement())
                            {
                                dst_elem = sender.GetElement();
                                int index = sender.GetElementIndex();

                                if (index > 1 && dst_elem != null)
                                {
                                    dst_buf = string.Format("{0} Leader", dst_elem.Name());
                                    src_buf = string.Format("this is {0} {1}", dst_elem.Name(), index);
                                }
                                else
                                {
                                    src_buf = string.Format("this is {0} leader", dst_elem.Name());
                                }
                            }

                            else
                            {
                                dst_buf = dst_ship.Name();
                                src_buf = FirstLetterToLower(src_buf);
                            }
                        }

                        else if (dst_elem != null)
                        {
                            // flight
                            if (dst_elem.NumShips() > 1)
                            {
                                dst_buf = string.Format("{0} Flight", dst_elem.Name());

                                // internal announcement
                                if (sender.GetElement() == dst_elem)
                                {
                                    int index = sender.GetElementIndex();

                                    if (index > 1)
                                    {
                                        dst_buf = string.Format("{0} Leader", (string)dst_elem.Name());
                                        src_buf = string.Format("this is {0} {1}", (string)dst_elem.Name(), index);
                                    }
                                    else
                                    {
                                        src_buf = string.Format("this is {0} leader", (string)dst_elem.Name());
                                    }
                                }
                            }

                            // solo
                            else
                            {
                                dst_buf = dst_elem.Name();
                                src_buf = FirstLetterToLower(src_buf);
                            }
                        }
                    }
                }
            }

            // BUILD ACTION AND TARGET BUFFERS -------------------

            SimObject target = null;

            act_buf = RadioMessage.ActionName(msg.Action());

            if (msg.TargetList().Count > 0)
                target = msg.TargetList()[0];

            if (msg.Action() == RadioMessage.ACTION.ACK ||
                    msg.Action() == RadioMessage.ACTION.NACK)
            {

                if (dst_ship == msg.Sender())
                {
                    src_buf = null;
                    dst_buf = null;

                    if (msg.Action() == RadioMessage.ACTION.ACK)
                        msg_buf = string.Format("{0}.", TranslateVox("Acknowledged"));
                    else
                        msg_buf = string.Format("{0}.", TranslateVox("Unable"));
                }
                else if (msg.Sender() != null)
                {
                    dst_buf = null;

                    if (!string.IsNullOrWhiteSpace(msg.Info()))
                    {
                        msg_buf = string.Format("{0}. {1}", TranslateVox(act_buf), msg.Info());
                    }
                    else
                    {
                        msg_buf = string.Format("{0}.", TranslateVox(act_buf));
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(msg.Info()))
                    {
                        msg_buf = string.Format("{0}. {1}", TranslateVox(act_buf), msg.Info());
                    }
                    else
                    {
                        msg_buf = string.Format("{0}.", TranslateVox(act_buf));
                    }
                }
            }

            else if (msg.Action() == RadioMessage.ACTION.MOVE_PATROL)
            {
                msg_buf = string.Format("{0}.", TranslateVox("Move patrol."));
            }

            else if (target != null && dst_ship != null && msg.Sender() != null)
            {
                Contact c = msg.Sender().FindContact(target);

                if (c != null && c.GetIFF(msg.Sender()) > 10)
                {
                    msg_buf = string.Format("{0} {1}.", TranslateVox(act_buf), TranslateVox("unknown contact"));
                }

                else
                {
                    msg_buf = string.Format("{0} {1}.", TranslateVox(act_buf), target.Name());
                }
            }

            else if (target != null)
            {
                msg_buf = string.Format("{0} {1}.", TranslateVox(act_buf), target.Name());
            }

            else if (!string.IsNullOrWhiteSpace(msg.Info()))
            {
                msg_buf = string.Format("{0} {1}", TranslateVox(act_buf), (string)msg.Info());
            }

            else
            {
                msg_buf = TranslateVox(act_buf);
            }

            char last_char = msg_buf[msg_buf.Length - 1];
            if (last_char != '!' && last_char != '.' && last_char != '?')
                msg_buf += ".";

            // final format:
            if (!string.IsNullOrWhiteSpace(dst_buf) && !string.IsNullOrWhiteSpace(src_buf))
            {
                txt_buf = string.Format("{0} {1}. {2}", TranslateVox(dst_buf), TranslateVox(src_buf), msg_buf);
                txt_buf = FirstLetterToUpper(txt_buf);
            }

            else if (!string.IsNullOrWhiteSpace(src_buf))
            {
                txt_buf = string.Format("{0}. {1}", TranslateVox(src_buf), msg_buf);
                txt_buf = FirstLetterToUpper(txt_buf);
            }

            else if (!string.IsNullOrWhiteSpace(dst_buf))
            {
                txt_buf = string.Format("{0} {1}", TranslateVox(dst_buf), msg_buf);
                txt_buf = FirstLetterToUpper(txt_buf);
            }

            else
            {
                txt_buf = msg_buf;
            }

            // vox:
            string[] path = new string[8] { "1", "1", "2", "3", "4", "5", "6", "7" };

            RadioVox vox = new RadioVox(vox_channel, path[vox_channel], txt_buf);

            if (vox != null)
            {
                vox.AddPhrase(dst_buf);
                vox.AddPhrase(src_buf);
                vox.AddPhrase(act_buf);

                if (!vox.Start())
                {
                    RadioView.Message(txt_buf);
                    //delete vox;
                }
            }
        }

        private string FirstLetterToUpper(string str)
        {
            return char.ToUpper(str[0]) + str.Substring(1);
        }
        private string FirstLetterToLower(string str)
        {
            return char.ToLower(str[0]) + str.Substring(1);
        }


        protected List<RadioMessage> traffic = new List<RadioMessage>();
        protected static RadioTraffic radio_traffic;
        protected static ContentBundle localeManager;
    }
}