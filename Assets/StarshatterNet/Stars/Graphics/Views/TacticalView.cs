﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      TacticalView.h/TacticalView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    View class for Radio Communications HUD Overlay
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Audio;
using StarshatterNet.Config;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.AI;
using StarshatterNet.Stars.Dialogs;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;
using Screen = UnityEngine.Screen;
namespace StarshatterNet.Stars.Views
{
    public class TacticalView : View, ISimObserver
    {
        public TacticalView(Window parent, string name) : base(parent, name)
        {
            ship = null; camview = null; projector = null;
            mouse_down = false; right_down = false; shift_down = false;
            show_move = false; show_action = (RadioMessage.ACTION)0; active_menu = null; menu_view = null;
            msg_ship = null; base_alt = 0; move_alt = 0;


            tac_view = this;
            sim = Sim.GetSim();

            width = Screen.width;   // TODO
            height = Screen.height; // TODO
            xcenter = (width / 2.0) - 0.5;
            ycenter = (height / 2.0) + 0.5;
            font = FontMgr.Find("HUD");

            SetColor(Color.white);

            mouse_start.X = 0;
            mouse_start.Y = 0;
            mouse_action.X = 0;
            mouse_action.Y = 0;

            menu_view = new MenuView(window, name);
        }
        //public virtual ~TacticalView();

        // Operations:
        public override void Refresh()
        {
            sim = Sim.GetSim();

            if (sim != null)
            {
                bool rebuild = false;

                if (ship != sim.GetPlayerShip())
                {
                    ship = sim.GetPlayerShip();

                    if (ship != null)
                    {
                        if (ship.Life() == 0 || ship.IsDying() || ship.IsDead())
                        {
                            ship = null;
                        }
                        else
                        {
                            Observe(ship);
                        }
                    }

                    rebuild = true;
                }

                if (ship != null)
                {
                    if (current_sector != ship.GetRegion().Name())
                        rebuild = true;

                    if (rebuild)
                    {
                        BuildMenu();
                        current_sector = ship.GetRegion().Name();
                    }
                }
            }

            if (ship == null || ship.InTransition())
                return;

            DrawMouseRect();

            if (sim != null)
            {
                if (sim.GetSelection() != null && sim.GetSelection().Count != 0)
                {
                    foreach (Ship selection in sim.GetSelection())
                    {

                        // draw selection rect on selected ship:
                        if (selection != null && selection.Rep() != null)
                            DrawSelection(selection);
                    }

                    RadioView rv = RadioView.GetInstance();
                    QuantumView qv = QuantumView.GetInstance();

                    if ((rv == null || !rv.IsMenuShown()) && (qv == null || !qv.IsMenuShown()))
                    {
                        if (sim.GetSelection().Count == 1)
                        {
                            DrawSelectionInfo(sim.GetSelection()[1]);
                        }
                        else
                        {
                            DrawSelectionList(sim.GetSelection());
                        }
                    }
                }
            }

            DrawMenu();

            if (show_move)
            {
                Mouse.Show(false);
                DrawMove();
            }
            else if (show_action != 0)
            {
                Mouse.Show(false);
                DrawAction();
            }
        }

        public override void OnWindowMove()
        {
            width = Screen.width;   // TODO
            height = Screen.height; // TODO
            xcenter = (width / 2.0) - 0.5;
            ycenter = (height / 2.0) + 0.5;
        }
        public virtual void ExecFrame()
        {
            HUDView hud = HUDView.GetInstance();
            if (hud != null)
            {
                if (hud_color != hud.GetTextColor())
                {
                    hud_color = hud.GetTextColor();
                    SetColor(hud_color);
                }
            }
        }
        public virtual void UseProjector(Projector p)
        {
            projector = p;
        }

        static ulong rbutton_latch = 0;
        static ulong click_time = 0;
        public virtual void DoMouseFrame()
        {
            Starshatter stars = Starshatter.GetInstance();

            if (stars.InCutscene())
                return;

            if (Mouse.RButton() != 0)
            {
                MouseController mouse_con2 = MouseController.GetInstance();
                if (!right_down && (mouse_con2 == null || !mouse_con2.Active()))
                {
                    rbutton_latch = Game.RealTime();
                    right_down = true;
                }
            }
            else
            {
                if (sim != null && right_down && (Game.RealTime() - rbutton_latch < 250))
                {
                    Ship seln = WillSelectAt((int)Mouse.X(), (int)Mouse.Y());

                    if (seln != null && sim.IsSelected(seln) &&
                            seln.GetIFF() == ship.GetIFF() &&
                            ship.GetElement().CanCommand(seln.GetElement()))
                    {

                        msg_ship = seln;
                        Observe(msg_ship);
                    }

                    else if (ship != null && seln == ship &&
                            (ship.GetDirector() == null ||
                                 (int)ship.GetDirector().Type() != ShipCtrl.DIR_TYPE))
                    {

                        msg_ship = seln;
                    }

                    else
                    {
                        msg_ship = null;
                    }
                }

                right_down = false;
            }

            if (menu_view != null)
                menu_view.DoMouseFrame();

            MouseController mouse_con = MouseController.GetInstance();

            if (mouse_con == null || !mouse_con.Active())
            {
                if (Mouse.LButton() != 0)
                {
                    if (!mouse_down)
                    {
                        mouse_start.X = Mouse.X();
                        mouse_start.Y = Mouse.Y();

                        shift_down = Keyboard.KeyDown(KeyMap.VK_SHIFT);
                    }

                    else
                    {
                        if (Mouse.X() < mouse_start.X)
                        {
                            mouse_rect.x = (int)Mouse.X();
                            mouse_rect.w = (int)(mouse_start.X - Mouse.X());
                        }
                        else
                        {
                            mouse_rect.x = (int)mouse_start.X;
                            mouse_rect.w = (int)(Mouse.X() - mouse_start.X);
                        }

                        if (Mouse.Y() < mouse_start.Y)
                        {
                            mouse_rect.y = (int)Mouse.Y();
                            mouse_rect.h = (int)(mouse_start.Y - Mouse.Y());
                        }
                        else
                        {
                            mouse_rect.y = (int)mouse_start.Y;
                            mouse_rect.h = (int)(Mouse.Y() - mouse_start.Y);
                        }

                        // don't draw seln rectangle while zooming:
                        if (Mouse.RButton() != 0 || show_move || show_action != 0)
                        {
                            mouse_rect.w = 0;
                            mouse_rect.h = 0;
                        }

                        else
                        {
                            SelectRect(mouse_rect);
                        }
                    }

                    mouse_down = true;
                }

                else
                {
                    if (mouse_down)
                    {
                        int mouse_x = (int)Mouse.X();
                        int mouse_y = (int)Mouse.Y();

                        if (menu_view != null && menu_view.GetAction() != 0)
                        {
                            ProcessMenuItem(menu_view.GetAction());
                            Mouse.Show(true);
                        }
                        else if (show_move)
                        {
                            SendMove();
                            show_move = false;
                            Mouse.Show(true);
                        }
                        else if (show_action != 0)
                        {
                            SendAction();
                            show_action = 0;
                            Mouse.Show(true);
                        }
                        else
                        {
                            if (!HUDView.IsMouseLatched() && !WepView.IsMouseLatched())
                            {
                                int dx = (int)Math.Abs((double)(mouse_x - mouse_start.X));
                                int dy = (int)Math.Abs((double)(mouse_y - mouse_start.Y));


                                if (dx < 3 && dy < 3)
                                {
                                    bool hit = SelectAt(mouse_x, mouse_y);

                                    if (ship.IsStarship() && Game.RealTime() - click_time < 350)
                                        SetHelm(hit);

                                    click_time = Game.RealTime();
                                }
                            }
                        }

                        mouse_rect = new Rect();
                        mouse_down = false;
                    }
                }
            }

            if (show_action != 0 && !mouse_down && !right_down)
            {
                mouse_action.X = Mouse.X();
                mouse_action.Y = Mouse.Y();
            }
        }

        public virtual bool Update(SimObject obj)
        {
            //TODO Update must be called when simObserver object is updated. 
            if (obj == ship)
            {
                ship = null;
            }

            if (obj == msg_ship)
            {
                msg_ship = null;
            }

            return simObserver.Update(obj);
        }

        public virtual string GetObserverName()
        {
            return "TacticalView";
        }

        public static void SetColor(Color c)
        {
            HUDView hud = HUDView.GetInstance();

            if (hud != null)
            {
                hud_color = hud.GetHUDColor();
                txt_color = hud.GetTextColor();
            }
            else
            {
                hud_color = c;
                txt_color = c;
            }
        }

        static bool initialized = false;
        public static void Initialize()
        {
            if (initialized) return;

            view_menu = new Menu(Game.GetText("TacView.menu.view"));
            view_menu.AddItem(Game.GetText("TacView.item.forward"), (int)VIEW_MENU.VIEW_FORWARD);
            view_menu.AddItem(Game.GetText("TacView.item.chase"), (int)VIEW_MENU.VIEW_CHASE);
            view_menu.AddItem(Game.GetText("TacView.item.orbit"), (int)VIEW_MENU.VIEW_ORBIT);
            view_menu.AddItem(Game.GetText("TacView.item.padlock"), (int)VIEW_MENU.VIEW_PADLOCK);

            emcon_menu = new Menu(Game.GetText("TacView.menu.emcon"));

            quantum_menu = new Menu(Game.GetText("TacView.menu.quantum"));
            farcast_menu = new Menu(Game.GetText("TacView.menu.farcast"));

            main_menu = new Menu(Game.GetText("TacView.menu.main"));

            action_menu = new Menu(Game.GetText("TacView.menu.action"));
            action_menu.AddItem(Game.GetText("TacView.item.engage"), (int)RadioMessage.ACTION.ATTACK);
            action_menu.AddItem(Game.GetText("TacView.item.bracket"), (int)RadioMessage.ACTION.BRACKET);
            action_menu.AddItem(Game.GetText("TacView.item.escort"), (int)RadioMessage.ACTION.ESCORT);
            action_menu.AddItem(Game.GetText("TacView.item.identify"), (int)RadioMessage.ACTION.IDENTIFY);
            action_menu.AddItem(Game.GetText("TacView.item.hold"), (int)RadioMessage.ACTION.WEP_HOLD);

            formation_menu = new Menu(Game.GetText("TacView.menu.formation"));
            formation_menu.AddItem(Game.GetText("TacView.item.diamond"), (int)RadioMessage.ACTION.GO_DIAMOND);
            formation_menu.AddItem(Game.GetText("TacView.item.spread"), (int)RadioMessage.ACTION.GO_SPREAD);
            formation_menu.AddItem(Game.GetText("TacView.item.box"), (int)RadioMessage.ACTION.GO_BOX);
            formation_menu.AddItem(Game.GetText("TacView.item.trail"), (int)RadioMessage.ACTION.GO_TRAIL);

            sensors_menu = new Menu(Game.GetText("TacView.menu.emcon"));
            sensors_menu.AddItem(Game.GetText("TacView.item.emcon-1"), (int)RadioMessage.ACTION.GO_EMCON1);
            sensors_menu.AddItem(Game.GetText("TacView.item.emcon-2"), (int)RadioMessage.ACTION.GO_EMCON2);
            sensors_menu.AddItem(Game.GetText("TacView.item.emcon-3"), (int)RadioMessage.ACTION.GO_EMCON3);
            sensors_menu.AddItem(Game.GetText("TacView.item.probe"), (int)RadioMessage.ACTION.LAUNCH_PROBE);

            fighter_menu = new Menu(Game.GetText("TacView.menu.context"));
            fighter_menu.AddMenu(Game.GetText("TacView.item.action"), action_menu);
            fighter_menu.AddMenu(Game.GetText("TacView.item.formation"), formation_menu);
            fighter_menu.AddMenu(Game.GetText("TacView.item.sensors"), sensors_menu);
            fighter_menu.AddItem(Game.GetText("TacView.item.patrol"), (int)RadioMessage.ACTION.MOVE_PATROL);
            fighter_menu.AddItem(Game.GetText("TacView.item.cancel"), (int)RadioMessage.ACTION.RESUME_MISSION);
            fighter_menu.AddItem("", 0);
            fighter_menu.AddItem(Game.GetText("TacView.item.rtb"), (int)RadioMessage.ACTION.RTB);
            fighter_menu.AddItem(Game.GetText("TacView.item.dock"), (int)RadioMessage.ACTION.DOCK_WITH);
            fighter_menu.AddMenu(Game.GetText("TacView.item.farcast"), farcast_menu);

            starship_menu = new Menu(Game.GetText("TacView.menu.context"));
            starship_menu.AddMenu(Game.GetText("TacView.item.action"), action_menu);
            starship_menu.AddMenu(Game.GetText("TacView.item.sensors"), sensors_menu);
            starship_menu.AddItem(Game.GetText("TacView.item.patrol"), (int)RadioMessage.ACTION.MOVE_PATROL);
            starship_menu.AddItem(Game.GetText("TacView.item.cancel"), (int)RadioMessage.ACTION.RESUME_MISSION);
            starship_menu.AddItem("", 0);
            starship_menu.AddMenu(Game.GetText("TacView.item.quantum"), quantum_menu);
            starship_menu.AddMenu(Game.GetText("TacView.item.farcast"), farcast_menu);

            initialized = true;
        }
        public static void Close()
        {
            //delete view_menu;
            //delete emcon_menu;
            //delete main_menu;
            //delete fighter_menu;
            //delete starship_menu;
            //delete action_menu;
            //delete formation_menu;
            //delete sensors_menu;
            //delete quantum_menu;
            //delete farcast_menu;
        }


        public static TacticalView GetInstance() { return tac_view; }


        protected virtual bool SelectAt(int x, int y)
        {
            if (ship == null) return false;

            Ship selection = WillSelectAt(x, y);

            if (selection != null && shift_down)
                ship.SetTarget(selection);

            else if (sim != null && selection != null)
                sim.SetSelection(selection);

            return selection != null;
        }
        protected virtual bool SelectRect(Rect rect)
        {
            bool result = false;
#if TODO
            if (ship == null || sim == null) return result;

            if (rect.w > 8 || rect.h > 8)
                sim.ClearSelection();

            // check distance to each contact:
            List<Contact> contact_list = ship.ContactList();

            for (int i = 0; i < ship.NumContacts(); i++)
            {
                Ship test = contact_list[i].GetShip();

                if (test != null && test != ship)
                {

                    Point test_loc = test.Location();
                    projector.Transform(test_loc);

                    if (test_loc.Z > 1)
                    {
                        projector.Project(test_loc);

                        if (rect.Contains((int)test_loc.X, (int)test_loc.Y))
                        {
                            // shift-select targets:
                            if (shift_down)
                            {
                                if (test.GetIFF() == 0 || test.GetIFF() == ship.GetIFF())
                                    continue;

                                ship.SetTarget(test);
                                result = true;
                            }
                            else
                            {
                                sim.AddSelection(test);
                                result = true;
                            }
                        }
                    }
                }
            }

            // select self only in orbit cam
            if (!shift_down && CameraDirector.GetCameraMode() == CameraDirector.CAM_MODE.MODE_ORBIT)
            {
                Point test_loc = ship.Location();
                projector.Transform(test_loc);

                if (test_loc.Z > 1)
                {
                    projector.Project(test_loc);

                    if (rect.Contains((int)test_loc.X, (int)test_loc.Y))
                    {
                        sim.AddSelection(ship);
                        result = true;
                    }
                }
            }

            return result;
#endif
            throw new NotImplementedException();
        }
        protected virtual Ship WillSelectAt(int x, int y)
        {
#if TODO
            Ship selection = null;

            if (ship != null)
            {
                // check distance to each contact:
                List<Contact> contact_list = ship.ContactList();

                for (int i = 0; i < ship.NumContacts(); i++)
                {
                    Ship test = contact_list[i].GetShip();

                    if (test != null)
                    {
                        // shift-select targets:
                        if (shift_down)
                        {
                            if (test.GetIFF() == 0 || test.GetIFF() == ship.GetIFF())
                                continue;
                        }

                        Graphic g = test.Rep();
                        if (g != null)
                        {
                            Rect r = g.ScreenRect();

                            if (r.x == 2000 && r.y == 2000 && r.w == 0 && r.h == 0)
                            {
                                if (projector != null)
                                {
                                    Point loc = test.Location();
                                    projector.Transform(loc);
                                    projector.Project(loc);

                                    r.x = (int)loc.X;
                                    r.y = (int)loc.Y;
                                }
                            }

                            if (r.w < 20 || r.h < 20)
                                r.Inflate(20, 20);
                            else
                                r.Inflate(10, 10);

                            if (r.Contains(x, y))
                            {
                                selection = test;
                                break;
                            }
                        }
                    }
                }

                if (selection == null && !shift_down)
                {
                    Graphic g = ship.Rep();
                    if (g != null)
                    {
                        Rect r = g.ScreenRect();

                        if (r.Contains(x, y))
                        {
                            selection = ship;
                        }
                    }
                }
            }

            if (selection == ship && CameraDirector.GetCameraMode() != CameraDirector.CAM_MODE.MODE_ORBIT)
                selection = null;

            return selection;
#endif
            throw new NotImplementedException();
        }
        protected virtual void SetHelm(bool approach)
        {
            Point delta = new Point();

            // double-click on ship: set helm to approach
            if (sim != null && approach)
            {
                //ListIter<Ship> iter = sim.GetSelection();
                //++iter;
                Ship selection = sim.GetSelection()[0];

                if (selection != ship)
                {
                    delta = selection.Location() - ship.Location();
                    delta.Normalize();
                }
            }

            // double-click on space: set helm in direction
            if (delta.Length < 1)
            {
                float mx = Mouse.X();
                float my = Mouse.Y();

                if (projector != null)
                {
#if TODO
                    double focal_dist = width / Math.Tan(projector.XAngle());

                    delta = projector.vpn() * focal_dist +
                    projector.vup() * -1 * (my - height / 2) +
                    projector.vrt() * (mx - width / 2);

                    delta.Normalize();
#endif
                    throw new NotImplementedException();
                }

                else
                {
                    return;
                }
            }

            double az = Math.Atan2(Math.Abs(delta.X), delta.Z);
            double el = Math.Asin(delta.Y);

            if (delta.X < 0)
                az *= -1;

            az += Math.PI;

            if (az >= 2 * Math.PI)
                az -= 2 * Math.PI;

            ship.SetHelmHeading(az);
            ship.SetHelmPitch(el);
        }


        protected virtual void DrawMouseRect()
        {
#if TODO
            if (mouse_rect.w > 0 && mouse_rect.h > 0)
            {
                Color c = hud_color * 0.66f;

                if (shift_down)
                    c = Color.yellow;

                window.DrawRect(mouse_rect, c);
            }
#endif
            throw new NotImplementedException();
        }

        protected virtual void DrawSelection(Ship seln)
        {
            if (seln == null)
                return;
#if TODO
            Graphic g = seln.Rep();
            Rect r = g.ScreenRect();

            Point mark_pt = seln.Location();

            projector.Transform(mark_pt);

            // clip:
            if (mark_pt.Z > 1.0)
            {
                projector.Project(mark_pt);

                int x = (int)mark_pt.X;
                int y = r.y;

                if (y >= 2000)
                    y = (int)mark_pt.Y;

                if (x > 4 && x < width - 4 &&
                        y > 4 && y < height - 4)
                {

                    const int BAR_LENGTH = 40;

                    // life bars:
                    int sx = x - BAR_LENGTH / 2;
                    int sy = y - 8;

                    double hull_strength = seln.HullStrength() / 100.0;

                    int hw = (int)(BAR_LENGTH * hull_strength);
                    int sw = (int)(BAR_LENGTH * (seln.ShieldStrength() / 100.0));

                    if (hw < 0) hw = 0;
                    if (sw < 0) sw = 0;

                    ShipSystem.STATUS s = ShipSystem.STATUS.NOMINAL;

                    if (hull_strength < 0.30) s = ShipSystem.STATUS.CRITICAL;
                    else if (hull_strength < 0.60) s = ShipSystem.STATUS.DEGRADED;

                    Color hc = HUDView.GetStatusColor(s);
                    Color sc = hud_color;

                    window.FillRect(sx, sy, sx + hw, sy + 1, hc);
                    window.FillRect(sx, sy + 3, sx + sw, sy + 4, sc);
                }
            }
#endif
            throw new NotImplementedException();
        }
        protected virtual void DrawSelectionInfo(Ship seln)
        {
#if TODO
            if (ship == null || seln == null) return;

            Rect label_rect = new Rect(width - 140, 10, 90, 12);
            Rect info_rect = new Rect(width - 100, 10, 90, 12);

            if (width >= 800)
            {
                label_rect.x -= 20;
                info_rect.x -= 20;
                info_rect.w += 20;
            }

            string name;
            string design;
            string shield;
            string hull;
            string range = "";
            string heading;
            string speed = "";
            string orders;
            string psv;
            string act;

            bool show_labels = width > 640;
            bool full_info = true;
            int shield_val = seln.ShieldStrength();
            int hull_val = seln.HullStrength();

            if (shield_val < 0) shield_val = 0;
            if (hull_val < 0) hull_val = 0;

            name = string.Format("{0}", seln.Name());

            if (show_labels)
            {
                shield = string.Format("{0} {1:D3}", Game.GetText("HUDView.symbol.shield"), shield_val);
                hull = string.Format("{0} {1:D3}", Game.GetText("HUDView.symbol.hull"), hull_val);
            }
            else
            {
                shield = string.Format("{0:D3}", shield_val);
                hull = string.Format("{0:D3}", hull_val);
            }

            FormatUtil.FormatNumberExp(ref range, new Point(seln.Location() - ship.Location()).Length / 1000);
            range += " km";
            heading = string.Format("{0:D3} {1}", (int)(seln.CompassHeading() / ConstantsF.DEGREES), Game.GetText("HUDView.symbol.degrees").data());

            double ss = seln.Velocity().Length;
            if (seln.Velocity() * seln.Heading() < 0)
                ss = -ss;

            FormatUtil.FormatNumberExp(ref speed, ss);
            speed += " m/s";

            Contact contact = null;

            // always recognize ownside:
            if (seln.GetIFF() != ship.GetIFF())
            {
                foreach (Contact c in ship.ContactList())
                {
                    if (c.GetShip() == seln)
                    {
                        contact = c;
                        if (c.GetIFF(ship) > seln.GetIFF())
                        {
                            name = string.Format("{0} {1:D4}", Game.GetText("TacView.contact"), seln.GetContactID());
                            full_info = false;
                        }

                        break;
                    }
                }
            }

            if (show_labels)
            {
                font.SetColor(txt_color);
                font.SetAlpha(1);

                font.DrawText(Game.GetText("TacView.name"), 5, label_rect, TextFormat.DT_LEFT);
                label_rect.y += 10;
                font.DrawText(Game.GetText("TacView.type"), 5, label_rect, TextFormat.DT_LEFT);
                label_rect.y += 10;

                if (full_info)
                {
                    font.DrawText(Game.GetText("TacView.shield"), 5, label_rect, TextFormat.DT_LEFT);
                    label_rect.y += 10;
                    font.DrawText(Game.GetText("TacView.hull"), 5, label_rect, TextFormat.DT_LEFT);
                    label_rect.y += 10;
                }

                font.DrawText(Game.GetText("TacView.range"), 4, label_rect, TextFormat.DT_LEFT);
                label_rect.y += 10;

                if (full_info)
                {
                    font.DrawText(Game.GetText("TacView.speed"), 4, label_rect, TextFormat.DT_LEFT);
                    label_rect.y += 10;
                    font.DrawText(Game.GetText("TacView.heading"), 4, label_rect, TextFormat.DT_LEFT);
                    label_rect.y += 10;
                }
                else
                {
                    font.DrawText(Game.GetText("TacView.passive"), 4, label_rect, TextFormat.DT_LEFT);
                    label_rect.y += 10;
                    font.DrawText(Game.GetText("TacView.active"), 4, label_rect, TextFormat.DT_LEFT);
                    label_rect.y += 10;
                }
            }

            font.DrawText(name, 0, info_rect, TextFormat.DT_LEFT);
            info_rect.y += 10;

            if (full_info)
            {
                 design= string.Format("{0} {1}", seln.Abbreviation(), seln.Design().display_name);
                font.DrawText(design, 0, info_rect, TextFormat.DT_LEFT);
                info_rect.y += 10;
            }
            else
            {
                if (seln.IsStarship())
                    font.DrawText(Game.GetText("TacView.starship"), 8, info_rect, TextFormat.DT_LEFT);
                else
                    font.DrawText(Game.GetText("TacView.fighter"), 7, info_rect, TextFormat.DT_LEFT);

                info_rect.y += 10;
            }

            if (full_info)
            {
                font.DrawText(shield, 0, info_rect, TextFormat.DT_LEFT);
                info_rect.y += 10;

                font.DrawText(hull, 0, info_rect, TextFormat.DT_LEFT);
                info_rect.y += 10;
            }

            font.DrawText(range, 0, info_rect, TextFormat.DT_LEFT);
            info_rect.y += 10;

            if (full_info)
            {
                font.DrawText(speed, 0, info_rect, TextFormat.DT_LEFT);
                info_rect.y += 10;

                font.DrawText(heading, 0, info_rect, TextFormat.DT_LEFT);
                info_rect.y += 10;

                if (seln.GetIFF() == ship.GetIFF())
                {
                    Instruction instr = seln.GetRadioOrders();
                    if (instr != null && instr.Action() != 0)
                    {
                        orders = Instruction.ActionName(instr.Action());

                        if ((int)instr.Action() == (int)RadioMessage.ACTION.QUANTUM_TO)
                        {
                            orders += " ";
                            orders += instr.RegionName();
                        }
                    }
                    else
                    {
                        orders = null;
                    }

                    if (orders != null)
                    {
                        if (show_labels)
                        {
                            font.DrawText(Game.GetText("TacView.orders"), 5, label_rect, TextFormat.DT_LEFT);
                            label_rect.y += 10;
                        }

                        font.DrawText(orders, 0, info_rect, TextFormat.DT_LEFT);
                        info_rect.y += 10;
                    }
                }
            }
            else
            {
                psv = string.Format("{0:D3}", (int)(contact.PasReturn() * 100.0));
                act = string.Format("{0:D3}", (int)(contact.ActReturn() * 100.0));

                if (contact.Threat(ship))
                    psv += " !";

                font.DrawText(psv, 0, info_rect, TextFormat.DT_LEFT);
                info_rect.y += 10;
                font.DrawText(act, 0, info_rect, TextFormat.DT_LEFT);
                info_rect.y += 10;
            }

            /*** XXX DEBUG
        font.DrawText(seln.GetDirectorInfo(), 0, info_rect, DT_LEFT);
        info_rect.y += 10;
        /***/
#endif
            throw new NotImplementedException();

        }
        protected virtual void DrawSelectionList(List<Ship> selnList)
        {
#if TODO
            int index = 0;
            Rect info_rect = new Rect(width - 100, 10, 90, 12);

            foreach (Ship seln in selnList)
            {
                string name;
                name = string.Format("{0}", seln.Name());

                // always recognize ownside:
                if (seln.GetIFF() != ship.GetIFF())
                {
                    foreach (Contact c in ship.ContactList())
                    {
                        if (c.GetShip() == seln)
                        {
                            if (c.GetIFF(ship) > seln.GetIFF())
                            {
                                name = string.Format("{0} {1:D4}", Game.GetText("TacView.contact"), seln.GetContactID());
                            }

                            break;
                        }
                    }
                }

                font.DrawText(name, 0, info_rect, TextFormat.DT_LEFT);
                info_rect.y += 10;
                index++;

                if (index >= 10)
                    break;
            }
#endif
            throw new NotImplementedException();

        }

        protected virtual void BuildMenu()
        {
            main_menu.ClearItems();
            quantum_menu.ClearItems();
            farcast_menu.ClearItems();
            emcon_menu.ClearItems();

            if (ship == null)
                return;

            // prepare quantum and farcast menus:
            foreach (SimRegion rgn in sim.GetRegions())
            {
                if (rgn != ship.GetRegion() && rgn.Type() != SimRegion.TYPES.AIR_SPACE)
                    quantum_menu.AddItem(rgn.Name(), QUANTUM);
            }

            if (ship.GetRegion() != null)
            {
                foreach (Ship s in ship.GetRegion().Ships())
                {
                    if (s != null && s.GetFarcaster() != null)
                    {
                        Farcaster farcaster = s.GetFarcaster();

                        // ensure that the farcaster is connected:
                        farcaster.ExecFrame(0);

                        // now find the destination
                        Ship dest = farcaster.GetDest();

                        if (dest != null && dest.GetRegion() != null)
                        {
                            SimRegion rgn = dest.GetRegion();
                            farcast_menu.AddItem(rgn.Name(), FARCAST);
                        }
                    }
                }
            }

            // build the main menu:
            main_menu.AddMenu(Game.GetText("TacView.item.camera"), view_menu);
            main_menu.AddItem("", 0);
            main_menu.AddItem(Game.GetText("TacView.item.instructions"), (int)VIEW_MENU.VIEW_INS);
            main_menu.AddItem(Game.GetText("TacView.item.navigation"), (int)VIEW_MENU.VIEW_NAV);

            if (ship.Design().repair_screen)
                main_menu.AddItem(Game.GetText("TacView.item.engineering"), (int)VIEW_MENU.VIEW_ENG);

            if (ship.Design().wep_screen)
                main_menu.AddItem(Game.GetText("TacView.item.weapons"), (int)VIEW_MENU.VIEW_WEP);

            if (ship.NumFlightDecks() > 0)
                main_menu.AddItem(Game.GetText("TacView.item.flight"), (int)VIEW_MENU.VIEW_FLT);

            emcon_menu.AddItem(Game.GetText("TacView.item.emcon-1"), (int)RadioMessage.ACTION.GO_EMCON1);
            emcon_menu.AddItem(Game.GetText("TacView.item.emcon-2"), (int)RadioMessage.ACTION.GO_EMCON2);
            emcon_menu.AddItem(Game.GetText("TacView.item.emcon-3"), (int)RadioMessage.ACTION.GO_EMCON3);

            if (ship.GetProbeLauncher() != null)
                emcon_menu.AddItem(Game.GetText("TacView.item.probe"), (int)RadioMessage.ACTION.LAUNCH_PROBE);

            main_menu.AddItem("", 0);
            main_menu.AddMenu(Game.GetText("TacView.item.sensors"), emcon_menu);

            if (sim != null && ship.GetQuantumDrive() != null)
            {
                main_menu.AddItem("", 0);
                main_menu.AddMenu(Game.GetText("TacView.item.quantum"), quantum_menu);
            }

            if (ship.IsStarship())
            {
                main_menu.AddItem("", 0);
                main_menu.AddItem(Game.GetText("TacView.item.command"), (int)VIEW_MENU.VIEW_CMD);
            }
        }
        protected virtual void DrawMenu()
        {
            active_menu = null;

            if (ship != null)
                active_menu = main_menu;

            if (msg_ship != null)
            {
                if (msg_ship.IsStarship())
                    active_menu = starship_menu;
                else if (msg_ship.IsDropship())
                    active_menu = fighter_menu;
            }

            if (menu_view != null)
            {
                menu_view.SetBackColor(hud_color);
                menu_view.SetTextColor(txt_color);
                menu_view.SetMenu(active_menu);
                menu_view.Refresh();
            }
        }
        protected virtual void ProcessMenuItem(int action)
        {
            Starshatter stars = Starshatter.GetInstance();

            switch (action)
            {
                case (int)RadioMessage.ACTION.MOVE_PATROL:
                    show_move = true;
                    base_alt = 0;
                    move_alt = 0;

                    if (msg_ship != null) base_alt = msg_ship.Location().Y;
                    break;

                case (int)RadioMessage.ACTION.ATTACK:
                case (int)RadioMessage.ACTION.BRACKET:
                case (int)RadioMessage.ACTION.ESCORT:
                case (int)RadioMessage.ACTION.IDENTIFY:
                case (int)RadioMessage.ACTION.DOCK_WITH:
                    show_action = (RadioMessage.ACTION)action;
                    break;

                case (int)RadioMessage.ACTION.WEP_HOLD:
                case (int)RadioMessage.ACTION.RESUME_MISSION:
                case (int)RadioMessage.ACTION.RTB:
                case (int)RadioMessage.ACTION.GO_DIAMOND:
                case (int)RadioMessage.ACTION.GO_SPREAD:
                case (int)RadioMessage.ACTION.GO_BOX:
                case (int)RadioMessage.ACTION.GO_TRAIL:
                case (int)RadioMessage.ACTION.GO_EMCON1:
                case (int)RadioMessage.ACTION.GO_EMCON2:
                case (int)RadioMessage.ACTION.GO_EMCON3:
                case (int)RadioMessage.ACTION.LAUNCH_PROBE:
                    if (msg_ship != null)
                    {
                        Element elem = msg_ship.GetElement();
                        RadioMessage msg = new RadioMessage(elem, ship, (RadioMessage.ACTION)action);
                        if (msg != null)
                            RadioTraffic.Transmit(msg);
                    }
                    else if (ship != null)
                    {
                        if (action == (int)RadioMessage.ACTION.GO_EMCON1)
                            ship.SetEMCON(1);
                        else if (action == (int)RadioMessage.ACTION.GO_EMCON2)
                            ship.SetEMCON(2);
                        else if (action == (int)RadioMessage.ACTION.GO_EMCON3)
                            ship.SetEMCON(3);
                        else if (action == (int)RadioMessage.ACTION.LAUNCH_PROBE)
                            ship.LaunchProbe();
                    }
                    break;

                case (int)VIEW_MENU.VIEW_FORWARD: stars.PlayerCam(CameraDirector.CAM_MODE.MODE_COCKPIT); break;
                case (int)VIEW_MENU.VIEW_CHASE: stars.PlayerCam(CameraDirector.CAM_MODE.MODE_CHASE); break;
                case (int)VIEW_MENU.VIEW_PADLOCK: stars.PlayerCam(CameraDirector.CAM_MODE.MODE_TARGET); break;
                case (int)VIEW_MENU.VIEW_ORBIT: stars.PlayerCam(CameraDirector.CAM_MODE.MODE_ORBIT); break;

                case (int)VIEW_MENU.VIEW_NAV: gamescreen.ShowNavDlg(); break;
                case (int)VIEW_MENU.VIEW_WEP: gamescreen.ShowWeaponsOverlay(); break;
                case (int)VIEW_MENU.VIEW_ENG: gamescreen.ShowEngDlg(); break;
                case (int)VIEW_MENU.VIEW_INS: HUDView.GetInstance().CycleHUDInst(); break;
                case (int)VIEW_MENU.VIEW_FLT: gamescreen.ShowFltDlg(); break;

                case (int)VIEW_MENU.VIEW_CMD:
                    if (ship != null && ship.IsStarship())
                    {
                        ship.CommandMode();
                    }
                    break;

                case QUANTUM:
                    if (sim != null)
                    {
                        Ship s = msg_ship;

                        if (s == null)
                            s = ship;

                        if (s != null && s.GetQuantumDrive() != null)
                        {
                            QuantumDrive quantum = s.GetQuantumDrive();
                            if (quantum != null)
                            {
                                MenuItem menu_item = menu_view.GetMenuItem();
                                string rgn_name = menu_item.GetText();
                                SimRegion rgn = sim.FindRegion(rgn_name);

                                if (rgn != null)
                                {
                                    if (s == ship)
                                    {
                                        quantum.SetDestination(rgn, new Point(0, 0, 0));
                                        quantum.Engage();
                                    }

                                    else
                                    {
                                        Element elem = msg_ship.GetElement();
                                        RadioMessage msg = new RadioMessage(elem, ship, RadioMessage.ACTION.QUANTUM_TO);
                                        if (msg != null)
                                        {
                                            msg.SetInfo(rgn_name);
                                            RadioTraffic.Transmit(msg);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;

                case FARCAST:
                    if (sim != null && msg_ship != null)
                    {
                        MenuItem menu_item = menu_view.GetMenuItem();
                        string rgn_name = menu_item.GetText();
                        SimRegion rgn = sim.FindRegion(rgn_name);

                        if (rgn != null)
                        {
                            Element elem = msg_ship.GetElement();
                            RadioMessage msg = new RadioMessage(elem, ship, RadioMessage.ACTION.FARCAST_TO);
                            if (msg != null)
                            {
                                msg.SetInfo(rgn_name);
                                RadioTraffic.Transmit(msg);
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }


        protected virtual void DrawMove()
        {
#if TODO
            if (projector == null || !show_move || msg_ship == null) return;

            Point origin = msg_ship.Location();

            if (GetMouseLoc3D())
            {
                Point dest = move_loc;

                double distance = (dest - origin).Length;

                projector.Transform(origin);
                projector.Project(origin);

                int x0 = (int)origin.X;
                int y0 = (int)origin.Y;

                projector.Transform(dest);
                projector.Project(dest);

                int x = (int)dest.X;
                int y = (int)dest.Y;

                window.DrawEllipse(x - 10, y - 10, x + 10, y + 10, Color.white);
                window.DrawLine(x0, y0, x, y, Color.white);

                string range = "";
                Rect range_rect = new Rect(x + 12, y - 8, 120, 20);

                if (Math.Abs(move_alt) > 1)
                {
                    dest = move_loc;
                    dest.Y += move_alt;
                    distance = (dest - msg_ship.Location()).Length;

                    projector.Transform(dest);
                    projector.Project(dest);

                    int x1 = (int)dest.X;
                    int y1 = (int)dest.Y;

                    window.DrawEllipse(x1 - 10, y1 - 10, x1 + 10, y1 + 10, Color.white);
                    window.DrawLine(x0, y0, x1, y1, Color.white);
                    window.DrawLine(x1, y1, x, y, Color.white);

                    range_rect.x = x1 + 12;
                    range_rect.y = y1 - 8;
                }

                FormatUtil.FormatNumber(ref range, distance);
                font.SetColor(Color.white);
                font.DrawText(range, 0, range_rect, TextFormat.DT_LEFT | TextFormat.DT_SINGLELINE);
                font.SetColor(txt_color);
            }
#endif
            throw new NotImplementedException();

        }
        protected virtual void SendMove()
        {
            if (projector == null || !show_move || msg_ship == null) return;

            if (GetMouseLoc3D())
            {
                Element elem = msg_ship.GetElement();
                RadioMessage msg = new RadioMessage(elem, ship, RadioMessage.ACTION.MOVE_PATROL);
                Point dest = move_loc;
                dest.Y += move_alt;
                msg.SetLocation(dest);
                RadioTraffic.Transmit(msg);
                HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_TAC_ACCEPT);
            }
        }


        protected virtual bool GetMouseLoc3D()
        {
#if TODO
            int mx = (int)Mouse.X();
            int my = (int)Mouse.Y();

            if (projector != null)
            {
                double focal_dist = width / Math.Tan(projector.XAngle());
                Point focal_vect = projector.vpn() * focal_dist +
                projector.vup() * -1 * (my - height / 2) +
                projector.vrt() * (mx - width / 2);

                focal_vect.Normalize();

                if (Keyboard.KeyDown(KeyMap.VK_SHIFT))
                {
                    if (Mouse.RButton()!= 0)
                        return true;

                    if (Math.Abs(focal_vect.X) > Math.Abs(focal_vect.Z))
                    {
                        double dx = move_loc.X - projector.Pos().x;
                        double t = -1 * ((projector.Pos().x - dx) / focal_vect.X);

                        if (t > 0)
                        {
                            Point p = projector.Pos() + focal_vect * t;
                            move_alt = p.Y - base_alt;
                        }
                    }
                    else
                    {
                        double dz = move_loc.z - projector.Pos().z;
                        double t = -1 * ((projector.Pos().z - dz) / focal_vect.Z);
                        Point p = projector.Pos() + focal_vect * t;

                        if (t > 0)
                        {
                            Point p = projector.Pos() + focal_vect * t;
                            move_alt = p.Y - base_alt;
                        }
                    }

                    if (move_alt > 25e3)
                        move_alt = 25e3;
                    else if (move_alt < -25e3)
                        move_alt = -25e3;

                    return true;
                }
                else
                {
                    if (Math.Abs(focal_vect.Y) > 1e-5)
                    {
                        if (Mouse.RButton()!=0)
                            return true;

                        bool clamp = false;
                        double t = -1 * ((projector.Pos().y - base_alt) / focal_vect.Y);

                        while (t <= 0 && my < height - 1)
                        {
                            my++;
                            clamp = true;

                            focal_vect = projector.vpn() * focal_dist +
                            projector.vup() * -1 * (my - height / 2) +
                            projector.vrt() * (mx - width / 2);

                            focal_vect.Normalize();
                            t = -1 * ((projector.Pos().Y - base_alt) / focal_vect.Y);
                        }

                        if (t > 0)
                        {
                            if (clamp)
                                Mouse.SetCursorPos(mx, my);

                            move_loc = projector.Pos() + focal_vect * t;
                        }

                        return true;
                    }
                }
            }

            return false;
#endif
            throw new NotImplementedException();

        }

        static bool invalid_action = false;

        protected virtual void DrawAction()
        {
#if TODO
            if (projector == null || show_action == 0 || msg_ship == null) return;

            Point origin = msg_ship.Location();
            projector.Transform(origin);
            projector.Project(origin);

            int x0 = (int)origin.X;
            int y0 = (int)origin.Y;

            int mx = mouse_action.X;
            int my = mouse_action.Y;
            int r = 10;

            int enemy = 2;
            if (ship.GetIFF() > 1)
                enemy = 1;

            Ship tgt = WillSelectAt(mx, my);
            int tgt_iff = 0;

            if (tgt != null)
                tgt_iff = tgt.GetIFF();

            Color c = Color.white;

            switch (show_action)
            {
                case RadioMessage.ACTION.ATTACK:
                case RadioMessage.ACTION.BRACKET:
                    c = Ship.IFFColor(enemy);
                    if (tgt != null)
                    {
                        if (tgt_iff == ship.GetIFF() || tgt_iff == 0)
                            r = 0;
                    }
                    break;

                case RadioMessage.ACTION.ESCORT:
                case RadioMessage.ACTION.DOCK_WITH:
                    c = ship.MarkerColor();
                    if (tgt != null)
                    {
                        if (tgt_iff == enemy)
                            r = 0;

                        // must have a hangar to dock with...
                        if (show_action == RadioMessage.ACTION.DOCK_WITH && tgt.GetHangar() == null)
                            r = 0;
                    }
                    break;

                default:
                    if (tgt != null)
                    {
                        if (tgt_iff == ship.GetIFF())
                            r = 0;
                    }
                    break;
            }

            if (tgt != null && r != 0)
            {
                if (((Game.RealTime() / 200) & 1) !=0)
                    r = 20;
                else
                    r = 15;
            }

            if (r)
            {
                invalid_action = false;
                window.DrawEllipse(mx - r, my - r, mx + r, my + r, c);
            }

            else
            {
                invalid_action = true;
                window.DrawLine(mx - 10, my - 10, mx + 10, my + 10, c);
                window.DrawLine(mx + 10, my - 10, mx - 10, my + 10, c);
            }

            window.DrawLine(x0, y0, mx, my, c);
#endif
            throw new NotImplementedException();
        }

        protected virtual void SendAction()
        {
            if (show_action == 0 || msg_ship == null || invalid_action)
            {
                HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_TAC_REJECT);
                return;
            }

            int mx = (int)mouse_action.X;
            int my = (int)mouse_action.Y;

            Ship tgt = WillSelectAt(mx, my);

            if (tgt != null)
            {
                Element elem = msg_ship.GetElement();
                RadioMessage msg = new RadioMessage(elem, ship, show_action);

                /***
                Element*       tgt_elem = tgt.GetElement();

                if (tgt_elem) {
                    for (int i = 1; i <= tgt_elem.NumShips(); i++)
                        msg.AddTarget(tgt_elem.GetShip(i));
                }
                else {
                    msg.AddTarget(tgt);
                }
                ***/

                msg.AddTarget(tgt);

                RadioTraffic.Transmit(msg);
                HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_TAC_ACCEPT);
            }
            else
            {
                HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_TAC_REJECT);
            }
        }


        public void Observe(SimObject obj)
        {
            simObserver.Observe(obj);
        }

        public void Ignore(SimObject obj)
        {
            simObserver.Ignore(obj);
        }

        protected GameScreen gamescreen;
        protected CameraView camview;
        protected Projector projector;

        protected int width, height;
        protected double xcenter, ycenter;

        protected bool shift_down;
        protected bool mouse_down;
        protected bool right_down;
        protected bool show_move;
        protected RadioMessage.ACTION show_action;

        protected Point move_loc;
        protected double base_alt;
        protected double move_alt;

        protected Point mouse_action;
        protected Point mouse_start;
        protected Rect mouse_rect;

        protected FontItem font;
        protected Sim sim;
        protected Ship ship;
        protected Ship msg_ship;
        protected string current_sector;

        protected Menu active_menu;
        protected MenuView menu_view;

        protected SimObserver simObserver = new SimObserver();
        protected static TacticalView tac_view;
        private static Color hud_color = Color.black;
        private static Color txt_color = Color.black;


        // +====================================================================+
        //
        // TACTICAL COMMUNICATIONS MENU:
        //

        private static Menu main_menu = null;
        private static Menu view_menu = null;
        private static Menu emcon_menu = null;

        private static Menu fighter_menu = null;
        private static Menu starship_menu = null;
        private static Menu action_menu = null;
        private static Menu formation_menu = null;
        private static Menu sensors_menu = null;
        private static Menu quantum_menu = null;
        private static Menu farcast_menu = null;

        private static Element dst_elem = null;

        private enum VIEW_MENU
        {
            VIEW_FORWARD = 1000,
            VIEW_CHASE,
            VIEW_PADLOCK,
            VIEW_ORBIT,
            VIEW_NAV,
            VIEW_WEP,
            VIEW_ENG,
            VIEW_FLT,
            VIEW_INS,
            VIEW_CMD
        };

        private const int QUANTUM = 2000;
        private const int FARCAST = 2001;
    }
}