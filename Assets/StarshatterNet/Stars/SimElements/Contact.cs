﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Contact.h/Contact.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Sensor Contact class
*/
using System;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using DWORD = System.UInt32;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars
{
    public class Contact : SimObserver
    {
        public Contact()
        {
            ship = null; shot = null; d_pas = 0.0f; d_act = 0.0f;
            track = null; ntrack = 0; time = 0; track_time = 0; probe = false;
            acquire_time = (uint)Game.GameTime();
        }
        public Contact(Ship s, float p, float a)
        {
            ship = s; shot = null; d_pas = p; d_act = a;
            track = null; ntrack = 0; time = 0; track_time = 0; probe = false;
            acquire_time = (uint)Game.GameTime();
            Observe(ship);
        }

        public Contact(Shot s, float p, float a)
        {
            ship = null; shot = s; d_pas = p; d_act = a;
            track = null; ntrack = 0; time = 0; track_time = 0; probe = false;
            acquire_time = (uint)Game.GameTime();
            Observe(shot);
        }

        // ~Contact();

        public static bool operator ==(Contact a, Contact b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
             if (a.ship != null)
                return a.ship == b.ship;
            else if (a.shot != null)
                return a.shot == b.shot;
            else
                return false;
        }

        public static bool operator !=(Contact a, Contact b)
        {
            return !(a == b);
        }
        public override bool Equals(System.Object obj)
        {
            // If parameter cannot be cast to Contact return false:
            Contact c = obj as Contact;
            if ((object)c == null)
            {
                return false;
            }

            if (ship != null)
                return ship == c.ship;
            else if (shot != null)
                return shot == c.shot;
            else
                return false;
        }

        public bool Equals(Contact c)
        {
            // Return true if the fields match:
            if (ship != null)
                return ship == c.ship;
            else if (shot != null)
                return shot == c.shot;
            else
                return false;
        }

        public override int GetHashCode()
        {
            if (ship != null)
                return ship.GetHashCode();
            else if (shot != null)
                return shot.GetHashCode();
            else
                return base.GetHashCode();
        }


        public Ship GetShip() { return ship; }
        public Shot GetShot() { return shot; }
        public Point Location() { return loc; }

        public double PasReturn() { return d_pas; }
        public double ActReturn() { return d_act; }
        public bool PasLock()
        {
            return d_pas >= SENSOR_THRESHOLD;
        }


        public bool ActLock()
        {
            return d_act >= SENSOR_THRESHOLD;
        }

        public double Age()
        {
            double age = 0;

            if (ship == null && shot == null)
                return age;

            double seconds = (Game.GameTime() - time) / 1000.0;

            age = 1.0 - seconds / DEFAULT_TRACK_AGE;

            if (age < 0)
                age = 0;

            return age;
        }

        public bool IsProbed() { return probe; }

        public DWORD AcquisitionTime() { return acquire_time; }

        public int GetIFF(Ship observer)
        {
            int i = 0;

            if (ship != null)
            {
                i = ship.GetIFF();

                // if the contact is on our side or has locked us up,
                // we know whose side he's on.

                // Otherwise:
                if (i != observer.GetIFF() && !Threat(observer))
                {
                    if (d_pas < 2 * SENSOR_THRESHOLD && d_act < SENSOR_THRESHOLD && !Visible(observer))
                        i = -1000;   // indeterminate iff reading
                }
            }

            else if (shot != null && shot.Owner() != null)
            {
                i = shot.Owner().GetIFF();
            }

            return i;
        }
        public void GetBearing(Ship observer, out double az, out double el, out double rng)
        {
            // translate:
            Point targ_pt = loc - observer.Location();

            // rotate:
            CameraNode cam = observer.Cam();
            double tx = Point.Dot(targ_pt, cam.vrt());
            double ty = Point.Dot(targ_pt, cam.vup());
            double tz = Point.Dot(targ_pt, cam.vpn());

            // convert to spherical coords:
            rng = targ_pt.Length;
            az = Math.Asin(Math.Abs(tx) / rng);
            el = Math.Asin(Math.Abs(ty) / rng);

            if (tx < 0) az = -az;
            if (ty < 0) el = -el;

            // correct az/el for back hemisphere:
            if (tz < 0)
            {
                if (az < 0) az = -Math.PI - az;
                else az = Math.PI - az;
            }
        }
        public double Range(Ship observer, double limit = 75e3)
        {
            double r = (loc - observer.Location()).Length;

            // if passive only, return approximate range:
            if (!ActLock())
            {
                const int chunk = 25000;

                if (!PasLock())
                {
                    r = (int)limit;
                }

                else if (r <= chunk)
                {
                    r = chunk;
                }

                else
                {
                    int r1 = (int)(r + chunk / 2) / chunk;
                    r = r1 * chunk;
                }
            }

            return r;
        }

        public bool InFront(Ship observer)
        {
            // translate:
            Point targ_pt = loc - observer.Location();

            // rotate:
            CameraNode cam = observer.Cam();
            double tz = Point.Dot(targ_pt, cam.vpn());

            if (tz > 1.0)
                return true;

            return false;
        }
        public bool Threat(Ship observer)
        {
            bool threat = false;

            if (observer != null && observer.Life() != 0)
            {
                if (ship != null && ship.Life() != 0)
                {
                    threat = (ship.GetIFF() != 0 &&
                    ship.GetIFF() != observer.GetIFF() &&
                    ship.GetEMCON() > 2 &&
                    ship.IsTracking(observer) &&
                    ship.Weapons().Count > 0);

                    if (threat && observer.GetIFF() == 0)
                        threat = ship.GetIFF() > 1;
                }

                else if (shot != null)
                {
                    threat = shot.IsTracking(observer);

                    if (!threat && shot.Design().probe && shot.GetIFF() != observer.GetIFF())
                    {
                        Point probe_pt = shot.Location() - observer.Location();
                        double prng = probe_pt.Length;

                        threat = (prng < shot.Design().lethal_radius);
                    }
                }
            }

            return threat;
        }
        public bool Visible(Ship observer)
        {
            // translate:
            Point targ_pt = loc - observer.Location();
            double radius = 0;

            if (ship != null)
                radius = ship.Radius();

            else if (shot != null)
                radius = shot.Radius();

            // rotate:
            CameraNode cam = observer.Cam();
            double rng = targ_pt.Length;

            return radius / rng > 0.002;
        }

        public void Reset()
        {
            if (SimulationTime.IsPaused) return;

            float step_down = (float)(SimulationTime.FrameTime() / 10);

            if (d_pas > 0)
                d_pas -= step_down;

            if (d_act > 0)
                d_act -= step_down;
        }

        public void Merge(Contact c)
        {
            if (c.GetShip() == ship && c.GetShot() == shot)
            {
                if (c.d_pas > d_pas)
                    d_pas = c.d_pas;

                if (c.d_act > d_act)
                    d_act = c.d_act;
            }
        }

        public void ClearTrack()
        {
            //delete[] track;
            track = null;
            ntrack = 0;
        }

        public void UpdateTrack()
        {
            time = (uint)Game.GameTime();

            if (shot != null || (ship != null && ship.IsGroundUnit()))
                return;

            if (track == null)
            {
                track = new Point[DEFAULT_TRACK_LENGTH];
                track[0] = loc;
                ntrack = 1;
                track_time = time;
            }

            else if (time - track_time > DEFAULT_TRACK_UPDATE)
            {
                if (loc != track[0])
                {
                    for (int i = DEFAULT_TRACK_LENGTH - 2; i >= 0; i--)
                        track[i + 1] = track[i];

                    track[0] = loc;
                    if (ntrack < DEFAULT_TRACK_LENGTH) ntrack++;
                }

                track_time = time;
            }
        }

        public int TrackLength() { return ntrack; }

        public Point TrackPoint(int i)
        {
            if (track != null && i < ntrack)
                return track[i];

            return new Point();
        }


        public override bool Update(SimObject obj)
        {
            if (obj == ship || obj == shot)
            {
                ship = null;
                shot = null;
                d_act = 0;
                d_pas = 0;

                ClearTrack();
            }

            return base.Update(obj);
        }

        public override string GetObserverName()
        {
            string name;

            if (ship != null)
                name = string.Format("Contact Ship='{0}'", ship.Name());
            else if (shot != null)
                name = string.Format("Contact Shot='{0}' {1}", shot.Name(), shot.DesignName());
            else
                name = string.Format("Contact (unknown)");

            return name;
        }


        internal Ship ship;
        private Shot shot;
        internal Point loc;
        private DWORD acquire_time;
        private DWORD time;

        private Point[] track;
        private int ntrack;
        private DWORD track_time;

        internal float d_pas;   // power output
        internal float d_act;   // mass, size
        internal bool probe;   // scanned by probe

        private const int DEFAULT_TRACK_UPDATE = 500; // milliseconds
        private const int DEFAULT_TRACK_LENGTH = 20; // 10 seconds
        private const double DEFAULT_TRACK_AGE = 10; // 10 seconds
        private const double SENSOR_THRESHOLD = 0.25;
    }
}
