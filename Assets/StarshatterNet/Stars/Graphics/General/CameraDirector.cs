﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:    Stars.exe
    FILE:         CameraDirector.h/CameraDirector.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Camera Director singleton manages the main view camera based on the current ship
*/
using System;
using System.Collections.Generic;
using DigitalRune.Mathematics;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using StarshatterNet.Stars.StarSystems;
using StarshatterNet.Stars.Views;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars
{
#warning CameraDirector class is still in development and is not recommended for production.
    public class CameraDirector : SimObserver
    {
        public enum CAM_MODE
        {
            MODE_NONE,

            MODE_COCKPIT,
            MODE_CHASE,
            MODE_TARGET,
            MODE_THREAT,
            MODE_ORBIT,
            MODE_VIRTUAL,
            MODE_TRANSLATE,
            MODE_ZOOM,
            MODE_DOCKING,
            MODE_DROP,

            MODE_LAST
        }
        // CONSTRUCTORS:
        public CameraDirector()
        {
            mode = CAM_MODE.MODE_COCKPIT; requested_mode = CAM_MODE.MODE_NONE; old_mode = CAM_MODE.MODE_NONE;
            sim = null; ship = null; region = null; external_ship = null; external_body = null;
            virt_az = 0; virt_el = 0; virt_x = 0; virt_y = 0; virt_z = 0;
            azimuth = Math.PI / 4; elevation = Math.PI / 4; az_rate = 0; el_rate = 0; range_rate = 0;
            range = 0; range_min = 100; range_max = range_max_limit;
            base_range = 0; transition = 0; hud = null;

            instance = this;
        }
        //public virtual ~CameraDirector=);

        //public int operator ==(const CameraDirector& that)   { return this == &that; }

        public static CameraDirector GetInstance()
        {
            if (instance == null)
                instance = new CameraDirector();

            return instance;
        }


        // CAMERA:
        public static CAM_MODE GetCameraMode()
        {
            if (instance != null)
            {
                CAM_MODE op_mode = instance.mode;
                if (instance.requested_mode > instance.mode)
                    op_mode = instance.requested_mode;
                return op_mode;
            }

            return CAM_MODE.MODE_NONE;
        }
        public static void SetCameraMode(CAM_MODE m, double trans_time = 1)
        {
            if (instance != null)
                instance.SetMode(m, trans_time);
        }

        public static string GetModeName()
        {
            CAM_MODE m = GetCameraMode();

            if (m != CAM_MODE.MODE_VIRTUAL)
            {
                return get_camera_mode_name(m);
            }
            else if (instance != null)
            {
                if (instance.virt_az > 30 * ConstantsF.DEGREES && instance.virt_az < 100 * ConstantsF.DEGREES)
                    return "RIGHT";

                else if (instance.virt_az >= 100 * ConstantsF.DEGREES)
                    return "RIGHT-AFT";

                else if (instance.virt_az < -30 * ConstantsF.DEGREES && instance.virt_az > -100 * ConstantsF.DEGREES)
                    return "LEFT";

                else if (instance.virt_az <= -100 * ConstantsF.DEGREES)
                    return "LEFT-AFT";

                else if (instance.virt_el > 15 * ConstantsF.DEGREES)
                    return "UP";

                else if (instance.virt_el < -15 * ConstantsF.DEGREES)
                    return "DOWN";

                return get_camera_mode_name(m);
            }

            return "";
        }

        public static double GetRangeLimit()
        {
            return range_max_limit;
        }
        public static void SetRangeLimit(double r)
        {
            if (r >= 1e3)
                range_max_limit = r;
        }
        public static void SetRangeLimits(double min, double max)
        {
            if (instance != null)
            {
                instance.range_min = min;
                instance.range_max = max;
            }
        }

        public virtual void Reset()
        {
            mode = CAM_MODE.MODE_COCKPIT;
            requested_mode = CAM_MODE.MODE_NONE;
            old_mode = CAM_MODE.MODE_NONE;

            sim = null;
            ship = null;
            region = null;
            external_ship = null;
            external_body = null;
            transition = 0;
            hud = null;
        }

        public virtual void SetMode(CAM_MODE m, double trans_time = 1)
        {
            if (requested_mode == m)
                return;

            external_point = new Point();

            // save current mode for after transition:
            if (m == CAM_MODE.MODE_DROP && mode != CAM_MODE.MODE_DROP)
                old_mode = mode;

            // if manually leaving drop mode, forget about
            // restoring the previous mode when the drop
            // expires...
            else if (m != CAM_MODE.MODE_DROP && mode == CAM_MODE.MODE_DROP)
                old_mode = CAM_MODE.MODE_NONE;

            if (m == CAM_MODE.MODE_VIRTUAL && ship != null && ship.Cockpit() == null)
                return;

            if (mode == m)
            {
                if (mode == CAM_MODE.MODE_TARGET || mode == CAM_MODE.MODE_ORBIT)
                    CycleViewObject();

                return;
            }

            if (m > CAM_MODE.MODE_NONE && m < CAM_MODE.MODE_LAST)
            {
                if (m <= CAM_MODE.MODE_VIRTUAL)
                {
                    requested_mode = m;
                    transition = trans_time;
                    external_ship = null;
                    ClearGroup();

                    // no easy way to do a smooth transition between
                    // certain modes, so just go immediately:
                    if ((mode == CAM_MODE.MODE_TARGET && m == CAM_MODE.MODE_CHASE) ||
                            (mode == CAM_MODE.MODE_COCKPIT && m == CAM_MODE.MODE_VIRTUAL) ||
                            (mode == CAM_MODE.MODE_VIRTUAL && m == CAM_MODE.MODE_COCKPIT))
                    {
                        mode = m;
                        requested_mode = 0;
                        transition = 0;
                    }
                }

                else if (m == CAM_MODE.MODE_TRANSLATE || m == CAM_MODE.MODE_ZOOM)
                {
                    requested_mode = m;
                    transition = trans_time;
                    base_range = range;
                }

                else if (m >= CAM_MODE.MODE_DOCKING)
                {
                    mode = m;
                    transition = 0;
                    external_ship = null;
                    ClearGroup();
                }

                virt_az = 0;
                virt_el = 0;
                virt_x = 0;
                virt_y = 0;
                virt_z = 0;
            }
        }
        public virtual CAM_MODE GetMode() { return requested_mode > CAM_MODE.MODE_NONE ? requested_mode : mode; }
        public virtual CameraNode GetCamera() { return camera; }
        public virtual void SetCamera(CameraNode cam) { camera = cam; }
        public virtual Ship GetShip() { return ship; }
        public virtual void SetShip(Ship s)
        {
            sim = Sim.GetSim();
            hud = HUDView.GetInstance();

            // can't take control of a dead ship:
            if (s != null && (s.Life() == 0 || s.IsDying() || s.IsDead()))
                return;

            // leaving the old ship, so make sure it is visible:
            if (ship != null && ship != s)
            {
                ship.ShowRep();
                ship.HideCockpit();
            }

            // taking control of the new ship:
            ship = s;

            if (ship != null)
            {
                Observe(ship);
                region = ship.GetRegion();

                if (sim != null && ship.GetRegion() != sim.GetActiveRegion())
                    sim.ActivateRegion(ship.GetRegion());

                range = ship.Radius() * 4;

                if (mode == CAM_MODE.MODE_COCKPIT)
                    mode = CAM_MODE.MODE_CHASE;

                SetMode(CAM_MODE.MODE_COCKPIT);
                ExecFrame(0);
            }
        }

        public virtual void VirtualHead(double az, double el)
        {
            if (mode == CAM_MODE.MODE_VIRTUAL || mode == CAM_MODE.MODE_TARGET || mode == CAM_MODE.MODE_COCKPIT)
            {
                const double alimit = 3 * Math.PI / 4;
                const double e_lo = Math.PI / 8;
                const double e_hi = Math.PI / 3;
                const double escale = e_hi;

                virt_az = az * alimit;
                virt_el = el * escale;

                if (virt_az > alimit)
                    virt_az = alimit;

                else if (virt_az < -alimit)
                    virt_az = -alimit;

                if (virt_el > e_hi)
                    virt_el = e_hi;

                else if (virt_el < -e_lo)
                    virt_el = -e_lo;
            }
        }
        public virtual void VirtualHeadOffset(double x, double y, double z)
        {
            if (mode == CAM_MODE.MODE_VIRTUAL || mode == CAM_MODE.MODE_TARGET || mode == CAM_MODE.MODE_COCKPIT)
            {
                virt_x = x;
                virt_y = y;
                virt_z = z;
            }
        }
        public virtual void VirtualAzimuth(double delta)
        {
            if (mode == CAM_MODE.MODE_VIRTUAL || mode == CAM_MODE.MODE_TARGET || mode == CAM_MODE.MODE_COCKPIT)
            {
                virt_az += delta;

                const double alimit = 3 * Math.PI / 4;

                if (virt_az > alimit)
                    virt_az = alimit;

                else if (virt_az < -alimit)
                    virt_az = -alimit;
            }
        }
        public virtual void VirtualElevation(double delta)
        {
            if (mode == CAM_MODE.MODE_VIRTUAL || mode == CAM_MODE.MODE_TARGET || mode == CAM_MODE.MODE_COCKPIT)
            {
                virt_el += delta;

                const double e_lo = Math.PI / 8;
                const double e_hi = Math.PI / 3;

                if (virt_el > e_hi)
                    virt_el = e_hi;

                else if (virt_el < -e_lo)
                    virt_el = -e_lo;
            }
        }
        public virtual void ExternalAzimuth(double delta)
        {
            azimuth += delta;

            if (azimuth > Math.PI)
                azimuth = -2 * Math.PI + azimuth;

            else if (azimuth < -Math.PI)
                azimuth = 2 * Math.PI + azimuth;
        }
        public virtual void ExternalElevation(double delta)
        {
            elevation += delta;

            const double limit = (0.43 * Math.PI);

            if (elevation > limit)
                elevation = limit;
            else if (elevation < -limit)
                elevation = -limit;
        }
        public virtual void ExternalRange(double delta)
        {
            range *= delta;

            if (ship != null && ship.IsAirborne())
                range_max = 30e3;
            else
                range_max = range_max_limit;

            if (range < range_min)
                range = range_min;

            else if (range > range_max)
                range = range_max;
        }
        public virtual void SetOrbitPoint(double az, double el, double r)
        {
            azimuth = az;
            elevation = el;
            range = r;

            if (external_body != null)
            {
                if (range < external_body.Radius() * 2)
                    range = external_body.Radius() * 2;

                else if (range > external_body.Radius() * 6)
                    range = external_body.Radius() * 6;
            }

            else
            {
                if (range < range_min)
                    range = range_min;

                else if (range > range_max)
                    range = range_max;
            }
        }
        public virtual void SetOrbitRates(double ar, double er, double rr)
        {
            az_rate = ar;
            el_rate = er;

            range_rate = rr;
        }

        public static bool IsViewCentered()
        {
            if (instance != null)
            {
                double fvaz = Math.Abs(instance.virt_az);
                double fvel = Math.Abs(instance.virt_el);

                return fvaz < 15 * ConstantsF.DEGREES && fvel < 15 * ConstantsF.DEGREES;
            }

            return true;
        }

        public virtual void CycleViewObject()
        {
            if (ship == null) return;

            Ship current = external_ship;
            external_ship = null;

            foreach (Contact c in ship.ContactList())
            {
                if (external_ship == null) break;
                Ship c_ship = c.GetShip();

                if (c_ship != null && current == null)
                {
                    external_ship = c_ship;
                }
                else if (current != null && c_ship == current)
                {
                    if (c.ActLock())
                    {
                        external_ship = c.GetShip();
                    }
                }
            }

            if (external_ship != current)
            {
                if (external_ship != null)
                {
                    if (external_ship.Life() == 0 || external_ship.IsDying() || external_ship.IsDead())
                    {
                        external_point = external_ship.Location();
                        external_ship = null;
                    }
                    else
                    {
                        Observe(external_ship);
                    }
                }

                if (mode == CAM_MODE.MODE_ORBIT)
                {
                    SetMode(CAM_MODE.MODE_TRANSLATE);
                    ExternalRange(1);
                }
            }
        }

        public virtual Orbital GetViewOrbital() { return external_body; }
        public virtual Ship GetViewObject() { return external_ship; }

        public virtual void SetViewOrbital(Orbital orb)
        {
            external_body = orb;

            if (external_body != null)
            {
                range_min = external_body.Radius() * 2.5;
                ClearGroup();
                external_ship = null;

                if (sim != null)
                {
                    region = sim.FindNearestSpaceRegion(orb);
                    if (region != null)
                        sim.ActivateRegion(region);
                }

                if (ship != null && region == null)
                    region = ship.GetRegion();
            }
        }
        public virtual void SetViewObject(Ship obj, bool quick = false)
        {
            if (ship == null) return;
            if (obj == null)
            {
                obj = ship;
                region = ship.GetRegion();
            }

            external_body = null;
            external_point = new Point();

            Starshatter stars = Starshatter.GetInstance();

            if (obj.GetIFF() != ship.GetIFF() && !stars.InCutscene())
            {
                // only view solid contacts:
                Contact c = ship.FindContact(obj);
                if (c == null || !c.ActLock())
                    return;
            }

            if (mode == CAM_MODE.MODE_TARGET)
            {
                ClearGroup();
                if (external_ship != null)
                {
                    external_ship = null;
                }
            }
            else if (mode >= CAM_MODE.MODE_ORBIT)
            {
                if (quick)
                {
                    mode = CAM_MODE.MODE_ORBIT;
                    transition = 0;
                }
                else
                {
                    SetMode(CAM_MODE.MODE_TRANSLATE);
                }

                if (external_group.Count != 0)
                {
                    ClearGroup();

                    if (external_ship != null)
                    {
                        external_ship = null;
                    }
                }

                else
                {
                    if ((obj == external_ship) || (obj == ship && external_ship == null))
                    {
                        if (!quick)
                            SetMode(CAM_MODE.MODE_ZOOM);
                    }

                    else if (external_ship != null)
                    {
                        external_ship = null;
                    }
                }
            }

            if (external_ship != obj)
            {
                external_ship = obj;

                if (external_ship != null)
                {
                    region = external_ship.GetRegion();

                    if (external_ship.Life() == 0 || external_ship.IsDying() || external_ship.IsDead())
                    {
                        external_ship = null;
                        range_min = 100;
                    }
                    else
                    {
                        Observe(external_ship);

                        if (sim != null)
                            sim.ActivateRegion(external_ship.GetRegion());

                        range_min = external_ship.Radius() * 1.5;
                    }
                }

                Observe(external_ship);
                ExternalRange(1);
            }
        }
        public virtual void SetViewObjectGroup(List<Ship> group, bool quick = false)
        {
            if (ship == null) return;

            Starshatter stars = Starshatter.GetInstance();

            if (!stars.InCutscene())
            {
                // only view solid contacts:
                foreach (Ship s in group)
                {
                    if (s.GetIFF() != ship.GetIFF())
                    {
                        Contact c = ship.FindContact(s);
                        if (c == null || !c.ActLock())
                            return;
                    }

                    if (s.Life() == 0 || s.IsDying() || s.IsDead())
                        return;
                }
            }

            //group.reset();

            if (external_group.Count > 1 &&
                    external_group.Count == group.Count)
            {

                bool same = true;

                for (int i = 0; same && i < external_group.Count; i++)
                {
                    if (external_group[i] != group[i])
                        same = false;
                }

                if (same)
                {
                    SetMode(CAM_MODE.MODE_ZOOM);
                    return;
                }
            }

            ClearGroup();

            if (quick)
            {
                mode = CAM_MODE.MODE_ORBIT;
                transition = 0;
            }
            else
            {
                SetMode(CAM_MODE.MODE_TRANSLATE);
            }

            external_group.AddRange(group);

            foreach (Ship s in external_group)
            {
                region = s.GetRegion();
                Observe(s);
            }
        }
        public virtual void ClearGroup()
        {
            external_group.Clear();
        }

        // BEHAVIORS:
        public virtual void ExecFrame(double seconds)
        {
            if (ship == null)
                return;

            hud = HUDView.GetInstance();

            OP_MODE flight_phase = ship.GetFlightPhase();

            if (flight_phase < OP_MODE.LOCKED)
                SetMode(CAM_MODE.MODE_DOCKING);

            if (ship.IsAirborne())
            {
                if (flight_phase >= OP_MODE.DOCKING)
                    SetMode(CAM_MODE.MODE_DOCKING);
            }
            else
            {
                if (flight_phase >= OP_MODE.RECOVERY)
                    SetMode(CAM_MODE.MODE_DOCKING);
            }

            if (flight_phase >= OP_MODE.LOCKED && flight_phase < OP_MODE.ACTIVE)
            {
                CAM_MODE m = GetMode();
                if (m != CAM_MODE.MODE_COCKPIT && m != CAM_MODE.MODE_VIRTUAL)
                    SetMode(CAM_MODE.MODE_COCKPIT);
            }

            if (ship.InTransition())
            {
                SetMode(CAM_MODE.MODE_DROP);
            }
            // automatically restore mode after transition:
            else if (old_mode != CAM_MODE.MODE_NONE)
            {
                mode = old_mode;
                requested_mode = old_mode;
                old_mode = CAM_MODE.MODE_NONE;
            }

            CAM_MODE op_mode = mode;
            if (requested_mode > mode)
                op_mode = requested_mode;

            Starshatter stars = Starshatter.GetInstance();

            // if we are in padlock, and have not locked a ship
            // try to padlock the current target:
            if (op_mode == CAM_MODE.MODE_TARGET && external_ship == null)
            {
                SimObject tgt = ship.GetTarget();
                if (tgt != null && tgt.Type() == (int)SimObject.TYPES.SIM_SHIP)
                    SetViewObject((Ship)tgt);
            }

            // if in an external mode, check the external ship:
            else if (op_mode >= CAM_MODE.MODE_TARGET && op_mode <= CAM_MODE.MODE_ZOOM)
            {
                if (external_ship != null && external_ship != ship && !stars.InCutscene())
                {
                    Contact c = ship.FindContact(external_ship);
                    if (c == null || !c.ActLock())
                    {
                        SetViewObject(ship);
                    }
                }
            }

            if (ship.Rep() != null)
            {
                if (op_mode == CAM_MODE.MODE_COCKPIT)
                {
                    ship.HideRep();
                    ship.HideCockpit();
                }
                else if (op_mode == CAM_MODE.MODE_VIRTUAL || op_mode == CAM_MODE.MODE_TARGET)
                {
                    if (ship.Cockpit() != null)
                    {
                        ship.HideRep();
                        ship.ShowCockpit();
                    }
                    else
                    {
                        ship.ShowRep();
                    }
                }
                else
                {
                    ship.Rep().SetForeground(op_mode == CAM_MODE.MODE_DOCKING);
                    ship.ShowRep();
                    ship.HideCockpit();
                }
            }

            if (hud != null && hud.Ambient() != Color.black)
                sim.GetScene().SetAmbient(hud.Ambient());
            else
                sim.GetScene().SetAmbient(sim.GetStarSystem().Ambient());

            switch (op_mode)
            {
                default:
                case CAM_MODE.MODE_COCKPIT: Cockpit(seconds); break;
                case CAM_MODE.MODE_CHASE: Chase(seconds); break;
                case CAM_MODE.MODE_TARGET: Target(seconds); break;
                case CAM_MODE.MODE_THREAT: Threat(seconds); break;
                case CAM_MODE.MODE_VIRTUAL: Virtual(seconds); break;
                case CAM_MODE.MODE_ORBIT:
                case CAM_MODE.MODE_TRANSLATE:
                case CAM_MODE.MODE_ZOOM: Orbit(seconds); break;
                case CAM_MODE.MODE_DOCKING: Docking(seconds); break;
                case CAM_MODE.MODE_DROP: Drop(seconds); break;
            }

            if (ship.Shake() > 0 &&
                    (op_mode < CAM_MODE.MODE_ORBIT ||
                        (op_mode == CAM_MODE.MODE_VIRTUAL && ship.Cockpit() != null)))
            {

                Point vib = ship.Vibration() * 0.2;
                camera.MoveBy(vib);
                camera.Aim(0, vib.Y, vib.Z);
            }

            Transition(seconds);
            DetailSet.SetReference(region, camera.Pos());
        }
        public virtual void Transition(double seconds)
        {
            if (transition > 0)
                transition -= seconds * 1.5;

            if (transition <= 0)
            {
                transition = 0;

                if (requested_mode != CAM_MODE.MODE_NONE && requested_mode != mode)
                    mode = requested_mode;

                requested_mode = 0;

                if (mode == CAM_MODE.MODE_TRANSLATE || mode == CAM_MODE.MODE_ZOOM)
                {
                    if (mode == CAM_MODE.MODE_ZOOM)
                        range = range_min;

                    mode = CAM_MODE.MODE_ORBIT;
                }
            }
        }

        // SimObserver:
        public override bool Update(SimObject obj)
        {
            if (obj.Type() == (int)SimObject.TYPES.SIM_SHIP)
            {
                Ship s = (Ship)obj;
                if (ship == s)
                    ship = null;

                if (external_ship == s)
                {
                    external_point = s.Location();
                    external_ship = null;
                }

                if (external_group.Contains(s))
                    external_group.Remove(s);
            }

            return this.Update(obj);
        }
        public override string GetObserverName()
        {
            return "CameraDirector";
        }


        protected virtual void Cockpit(double s)
        {
            camera.Clone(ship.Cam());

            Point bridge = ship.BridgeLocation();
            Point cpos = camera.Pos() +
            camera.vrt() * bridge.X +
            camera.vpn() * bridge.Y +
            camera.vup() * bridge.Z;

            camera.MoveTo(cpos);
        }
        protected virtual void Chase(double s)
        {
            double step = 1;

            if (requested_mode == CAM_MODE.MODE_COCKPIT)
                step = transition;

            else if (requested_mode == CAM_MODE.MODE_CHASE)
                step = 1 - transition;

            camera.Clone(ship.Cam());
            Point velocity = camera.vpn();

            if (ship.Velocity().Length > 10)
            {
                velocity = ship.Velocity();
                velocity.Normalize();
                velocity *= 0.25;
                velocity += camera.vpn() * 2;
                velocity.Normalize();
            }

            Point chase = ship.ChaseLocation();
            Point bridge = ship.BridgeLocation();
            Point cpos = camera.Pos() +
            camera.vrt() * bridge.X * (1 - step) +
            camera.vpn() * bridge.Y * (1 - step) +
            camera.vup() * bridge.Z * (1 - step) +
            velocity * chase.Y * step +
            camera.vup() * chase.Z * step;

            camera.MoveTo(cpos);
        }
        protected virtual void Target(double seconds)
        {
            Point target_loc = external_point;

            if (external_ship != null)
                target_loc = external_ship.Location();

            if (external_ship == null || external_ship == ship)
            {
                if (external_point == null)
                {
                    if (ship.Cockpit() != null)
                        Virtual(seconds);
                    else
                        Orbit(seconds);

                    return;
                }
            }

            double step = 1;

            if (requested_mode == CAM_MODE.MODE_COCKPIT)
                step = transition;

            else if (requested_mode == CAM_MODE.MODE_TARGET)
                step = 1 - transition;

            if (ship.Cockpit() != null)
            {
                // internal padlock:
                Cockpit(seconds);
                camera.Padlock(target_loc, 3 * Math.PI / 4, Math.PI / 8, Math.PI / 3);
            }
            else
            {
                // external padlock:
                Point delta = target_loc - ship.Location();
                delta.Normalize();
                delta *= -5 * ship.Radius() * step;
                delta.Y += ship.Radius() * step;

                camera.MoveTo(ship.Location() + delta);
                camera.LookAt(target_loc);
            }
        }
        protected virtual void Threat(double seconds)
        {
            Chase(seconds);
        }

        protected virtual void Virtual(double seconds)
        {
            camera.Clone(ship.Cam());

            Point bridge = ship.BridgeLocation();
            Point cpos = camera.Pos() +
            camera.vrt() * (bridge.X + virt_x) +
            camera.vpn() * (bridge.Y + virt_z) +
            camera.vup() * (bridge.Z + virt_y);

            camera.MoveTo(cpos);

            camera.Yaw(virt_az);
            camera.Pitch(-virt_el);

            double fvaz = Math.Abs(virt_az);
            double fvel = Math.Abs(virt_el);

            if (fvaz > 0.01 * ConstantsF.DEGREES && fvaz < 15 * ConstantsF.DEGREES)
            {
                double bleed = fvaz * 2;

                if (virt_az > 0)
                    virt_az -= bleed * seconds;
                else
                    virt_az += bleed * seconds;
            }

            if (fvel > 0.01 * ConstantsF.DEGREES && fvel < 15 * ConstantsF.DEGREES)
            {
                double bleed = fvel;

                if (virt_el > 0)
                    virt_el -= bleed * seconds;
                else
                    virt_el += bleed * seconds;
            }
        }
        protected virtual void Orbit(double seconds)
        {
            Point cpos = ship.Location();
            CAM_MODE op_mode = GetCameraMode();

            if (seconds < 0) seconds = 0;
            else if (seconds > 0.2) seconds = 0.2;

            // auto rotate
            azimuth += az_rate * seconds;
            elevation += el_rate * seconds;
            range *= 1 + range_rate * seconds;

            if (external_body != null && external_body.Rep() != null)
            {
                range_min = external_body.Radius() * 2.5;
                cpos = external_body.Rep().Location();
            }

            else if (external_group.Count != 0)
            {
                Point neg = new Point(1e9, 1e9, 1e9);
                Point pos = new Point(-1e9, -1e9, -1e9);

                foreach (var ship in external_group)
                {
                    Point loc = ship.Location();

                    if (loc.X < neg.X) neg.X = loc.X;
                    if (loc.X > pos.X) pos.X = loc.X;
                    if (loc.Y < neg.Y) neg.Y = loc.Y;
                    if (loc.Y > pos.Y) pos.Y = loc.Y;
                    if (loc.Z < neg.Z) neg.Z = loc.Z;
                    if (loc.Z > pos.Z) pos.Z = loc.Z;
                }

                double dx = pos.X - neg.X;
                double dy = pos.Y - neg.Y;
                double dz = pos.Z - neg.Z;

                if (dx > dy)
                {
                    if (dx > dz) range_min = dx * 1.2;
                    else range_min = dz * 1.2;
                }
                else
                {
                    if (dy > dz) range_min = dy * 1.2;
                    else range_min = dz * 1.2;
                }

                // focus on median location:
                cpos = neg + new Point(dx / 2, dy / 2, dz / 2);
            }
            else if (external_ship != null)
            {
                range_min = external_ship.Radius() * 1.5;
                cpos = external_ship.Location();
            }
            else
            {
                range_min = ship.Radius() * 1.5;
                cpos = ship.Location();
            }

            if (range < range_min)
                range = range_min;

            double er = range;
            double az = azimuth;
            double el = elevation;

            if (requested_mode == CAM_MODE.MODE_TRANSLATE)
            {
                cpos = cpos * (1 - transition) + base_loc * (transition);
            }

            else if (requested_mode == CAM_MODE.MODE_ZOOM)
            {
                er = base_range * transition;

                if (er < range_min)
                {
                    er = range_min;
                    range = range_min;
                    transition = 0;
                    requested_mode = 0;
                }
            }

            // transitions:
            else if (mode < CAM_MODE.MODE_ORBIT || requested_mode > 0 && requested_mode < CAM_MODE.MODE_ORBIT)
            {
                double az0 = ship.CompassHeading();
                if (Math.Abs(az - az0) > Math.PI) az0 -= 2 * Math.PI;

                double r0 = 0;
                double z0 = 0;

                if (mode == CAM_MODE.MODE_CHASE || requested_mode == CAM_MODE.MODE_CHASE ||
                        mode == CAM_MODE.MODE_TARGET || requested_mode == CAM_MODE.MODE_TARGET)
                {
                    r0 = ship.ChaseLocation().Length;
                    z0 = 20 * ConstantsF.DEGREES;
                }

                // pull out:
                if (mode < CAM_MODE.MODE_ORBIT)
                {
                    er *= (1 - transition);
                    az *= (1 - transition);
                    el *= (1 - transition);

                    er += r0 * transition;
                    az += az0 * transition;
                    el += z0 * transition;
                }

                // push in:
                else
                {
                    er *= transition;
                    az *= transition;
                    el *= transition;

                    er += r0 * (1 - transition);
                    az += az0 * (1 - transition);
                    el += z0 * (1 - transition);
                }
            }

            else
            {
                // save base location for next time we re-focus
                base_loc = cpos;
            }

            double dxtmp = er * Math.Sin(az) * Math.Cos(el);
            double dytmp = er * Math.Cos(az) * Math.Cos(el);
            double dztmp = er * Math.Sin(el);

            Point cloc = cpos + new Point(dxtmp, dztmp, dytmp);

            Terrain terrain = ship.GetRegion().GetTerrain();

            if (terrain != null)
            {
                double cam_agl = cloc.Y - terrain.Height(cloc.X, cloc.Z);

                if (cam_agl < 100)
                    cloc.Y = terrain.Height(cloc.X, cloc.Z) + 100;
            }

            if (external_ship == null && er < 0.5 * ship.Radius())
                ship.Rep().Hide();

            camera.MoveTo(cloc.X, cloc.Y, cloc.Z);
            camera.LookAt(cpos);
        }

        protected virtual void Docking(double seconds)
        {
            FlightDeck dock = ship.GetDock();

            if (dock == null)
            {
                Cockpit(seconds);
                return;
            }

            if (!ship.IsAirborne())
                sim.GetScene().SetAmbient(new Color(120f / 256f, 130f / 256f, 140f / 256f));
            OP_MODE flight_phase = ship.GetFlightPhase();

            Point bridge = ship.BridgeLocation();
            Point cloc = ship.Location() +
            ship.Cam().vrt() * bridge.X +
            ship.Cam().vpn() * bridge.Y +
            ship.Cam().vup() * bridge.Z;

            Point cpos = dock.CamLoc();

            // preflight:
            if (flight_phase < OP_MODE.LOCKED)
            {
                base_loc = cpos;
            }

            else if (flight_phase == OP_MODE.LOCKED)
            {
                if (hud != null)
                    hud.SetHUDMode(HUDView.HUDModes.HUD_MODE_TAC);
                cpos = base_loc * transition +
                cloc * (1 - transition);
            }

            // recovery:
            else if (flight_phase > OP_MODE.APPROACH)
            {
                if (hud != null)
                    hud.SetTacticalMode(true);
            }

            camera.MoveTo(cpos);
            camera.LookAt(cloc);
        }
        protected virtual void Drop(double s)
        {
            // orbital transitions use "drop cam" at transition_loc
            camera.MoveTo(ship.TransitionLocation());
            camera.LookAt(ship.Location());
        }

        protected CAM_MODE mode;
        protected CAM_MODE requested_mode;
        protected CAM_MODE old_mode;
        protected CameraNode camera;

        protected Ship ship;

        protected SimRegion region;
        protected Orbital external_body;
        protected Ship external_ship;
        protected List<Ship> external_group;
        protected Point external_point;

        protected Point base_loc;

        protected double virt_az;
        protected double virt_el;
        protected double virt_x;
        protected double virt_y;
        protected double virt_z;
        protected double azimuth;
        protected double elevation;
        protected double az_rate;
        protected double el_rate;
        protected double range_rate;
        protected double range;
        protected double range_min;
        protected double range_max;
        protected double base_range;
        protected double transition;

        protected Sim sim;
        protected HUDView hud;

        protected static CameraDirector instance = null;
        private static double range_max_limit = 300e3;
        private static string get_camera_mode_name(CAM_MODE m)
        {
            switch (m)
            {
                default:
                case CAM_MODE.MODE_NONE: return "";
                case CAM_MODE.MODE_COCKPIT: return "";
                case CAM_MODE.MODE_CHASE: return "Chase Cam";
                case CAM_MODE.MODE_TARGET: return "Padlock";
                case CAM_MODE.MODE_THREAT: return "Threatlock";
                case CAM_MODE.MODE_VIRTUAL: return "Virtual";
                case CAM_MODE.MODE_ORBIT:
                case CAM_MODE.MODE_TRANSLATE:
                case CAM_MODE.MODE_ZOOM: return "Orbit Cam";
                case CAM_MODE.MODE_DOCKING: return "Dock Cam";
                case CAM_MODE.MODE_DROP: return "";
            }
        }

    }
}