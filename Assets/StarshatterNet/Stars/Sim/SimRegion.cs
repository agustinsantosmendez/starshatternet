﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Sim.h/Sim.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Universe and Region classes
 */
using System;
using System.Collections.Generic;
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.AI;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.Network;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.StarSystems;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.Simulator
{
#warning SimRegion class is still in development and is not recommended for production.
    public class SimRegion
    {
        public enum TYPES { REAL_SPACE, AIR_SPACE };

        public SimRegion(Sim sim, string name, TYPES type)
        {
            this.sim = sim; this.name = name; this.type = type; orbital_region = null; star_system = null; player_ship = null;
            grid = null; active = false; current_view = 0; sim_time = 0; ai_index = 0; terrain = null;
            {
                if (sim != null)
                {
                    star_system = sim.GetStarSystem();
                }
            }
        }
        public SimRegion(Sim sim, OrbitalRegion rgn)
        {
            this.sim = sim; orbital_region = rgn; type = TYPES.REAL_SPACE; star_system = null; player_ship = null;
            grid = null; active = false; current_view = 0; sim_time = 0; ai_index = 0; terrain = null;

            if (rgn != null)
            {
                star_system = rgn.StarSystem();
            }

            if (orbital_region != null)
            {
                name = orbital_region.Name();
                grid = new GridNode((int)orbital_region.Radius(),
                                    (int)orbital_region.GridSpace());


                if (orbital_region.Type() == Orbital.OrbitalType.TERRAIN)
                {
                    TerrainRegion trgn = (TerrainRegion)orbital_region;
                    terrain = new Terrain(trgn);

                    type = TYPES.AIR_SPACE;
                }

                else if (orbital_region.Asteroids() > 0)
                {
                    int asteroids = orbital_region.Asteroids();

                    for (int i = 0; i < asteroids; i++)
                    {
                        Point init_loc = new Point(rand.Next(-16384, 16383) * 30,
                                            rand.Next(-16384, 16383) * 3,
                                            rand.Next(-16384, 16383) * 30);
                        sim.CreateAsteroid(init_loc, i, RandomHelper.Random(1e7, 1e8), this);
                    }
                }
            }
            else
            {
                name = ContentBundle.Instance.GetText("Unknown");
            }
        }
        // ~SimRegion();

        public bool Equals(SimRegion other)
        {
            return this.sim.Equals(other.sim) && (this.name == other.name);
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            SimRegion other = (SimRegion)obj;
            return this.sim.Equals(other.sim) && (this.name == other.name);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + this.sim.GetHashCode();
            hash = hash * 23 + this.name.GetHashCode();
            return hash;
        }

        public static bool operator ==(SimRegion l, SimRegion r)
        {
            if (object.ReferenceEquals(l, r))
            {
                return true;     // if both the same object, or both null
            }
            // If one is null, but not both, return false.
            if (((object)l == null) || ((object)r == null))
            {
                return false;
            }

            return (l.sim == r.sim) && (l.name == r.name);
        }

        public static bool operator !=(SimRegion l, SimRegion r)
        {
            return !(l == r);
        }
        public static bool operator <(SimRegion l, SimRegion r) { return (l.orbital_region != null && r.orbital_region != null && l.orbital_region < r.orbital_region); }

        public static bool operator >(SimRegion l, SimRegion r) { return (l.orbital_region != null && r.orbital_region != null && l.orbital_region > r.orbital_region); }

        public static bool operator <=(SimRegion l, SimRegion r) { return (l < r) || (l == r); }

        public static bool operator >=(SimRegion l, SimRegion r) { return (l > r) || (l == r); }

        //public int operator ==(  SimRegion  r)   { return (sim==r.sim) && (name==r.name); }
        //public int operator <(  SimRegion r)  ;
        //public int operator <=(  SimRegion  r)  ;

        public virtual void Activate()
        {
            if (sim == null) return;

            foreach (var ship in ships)
                ship.Activate(sim.scene);

            foreach (var shot in shots)
                shot.Activate(sim.scene);

            foreach (var exp in explosions)
                exp.Activate(sim.scene);

            foreach (var deb in debris)
                deb.Activate(sim.scene);

            foreach (var a in asteroids)
                a.Activate(sim.scene);

            if (grid != null)
                sim.scene.AddGraphic(grid);

            if (terrain != null)
                terrain.Activate(sim.scene);

            player_ship = null;
            active = true;
        }
        public virtual void Deactivate()
        {
            if (sim == null) return;

            foreach (var ship in ships)
                ship.Deactivate(sim.scene);

            foreach (var shot in shots)
                shot.Deactivate(sim.scene);

            foreach (var exp in explosions)
                exp.Deactivate(sim.scene);

            foreach (var deb in debris)
                deb.Deactivate(sim.scene);

            foreach (var a in asteroids)
                a.Deactivate(sim.scene);

            if (grid != null)
                sim.scene.DelGraphic(grid);

            if (terrain != null)
                terrain.Deactivate(sim.scene);

            player_ship = null;
            active = false;

            for (int i = 0; i < 5; i++)
                track_database[i].Destroy();
        }
        public virtual void ExecFrame(double secs)
        {
#if TODO
            if (sim == null) return;

            double seconds = secs;

            // DON'T REALLY KNOW WHAT PURPOSE THIS SERVES....
            if (!active)
            {
                double max_frame = 3 * SimulationTime.GetMaxFrameLength();
                long new_time = Game.GameTime();
                double delta = new_time - sim_time;
                seconds = delta / 1000.0;

                if (seconds > max_frame)
                    seconds = max_frame;
            }

            sim_time = Game.GameTime();

            if (orbital_region != null)
                location = orbital_region.Location();

            CameraDirector cam_dir = CameraDirector.GetInstance();

            Point ref_ = new Point();

            if (active && cam_dir != null)
            {
                ref_ = cam_dir.GetCamera().Pos();
                UpdateSky(seconds, ref_);
            }

            if (terrain != null)
                terrain.ExecFrame(seconds);

            UpdateTracks(seconds);
            UpdateShips(seconds);
            UpdateShots(seconds);
            UpdateExplosions(seconds);

            if (!Game.Paused())
            {
                DamageShips();
                DockShips();

                if (active)
                {
                    CollideShips();
                    CrashShips();
                }

                DestroyShips();
            }

            if (active && cam_dir != null && player_ship != null)
            {
                Sound.SetListener(cam_dir.GetCamera(), player_ship.Velocity());
            }
#endif 
            throw new NotImplementedException();
        }
        public void ShowGrid(bool show = true)
        {
#if TODO
            if (grid != null)
            {
                if (show)
                    grid.Show();
                else
                    grid.Hide();
            }
#endif
            throw new NotImplementedException();
        }
        public void NextView()
        {
            if (ships.Count != 0)
            {
                int original_view = current_view;

                do
                {
                    current_view++;
                    if (current_view >= ships.Count)
                    {
                        current_view = 0;
                    }
                }
                while (ships[current_view].Life() == 0 && current_view != original_view);

                if (current_view != original_view)
                {
                    ClearSelection();

                    if (!sim.IsTestMode())
                        player_ship.SetControls(null);

                    if (player_ship.Rep() != null)
                        player_ship.Rep().Show();

                    AttachPlayerShip(current_view);
                }
            }
        }
        public Ship FindShip(string ship_name)
        {
            Ship ship = null;

            if (!string.IsNullOrEmpty(ship_name))
            {
                int name_len = ship_name.Length;

                foreach (Ship test in ships)
                {
                    if (test.Name().StartsWith(ship_name))
                    {
                        int test_len = test.Name().Length;

                        // The only fuzzy match is for element indices.
                        // The desired name "Alpha" matches "Alpha 1" and "Alpha 2"
                        // but not "Alpha-Centauri"

                        if (test_len > name_len && test.Name()[name_len] != ' ')
                            continue;

                        ship = test;
                        break;
                    }
                }
            }

            return ship;
        }
        public Ship GetPlayerShip() { return player_ship; }
        public void SetPlayerShip(Ship ship)
        {
            // there can only be a player ship when playing the game locally
            if (Starshatter.GetInstance() != null)
            {
                int player_index = ships.IndexOf(ship);

                if (player_index >= 0)
                {
                    if (sim.GetActiveRegion() != this)
                        sim.ActivateRegion(this);

                    AttachPlayerShip(player_index);
                }

                else
                {
                    ErrLogger.PrintLine("SimRegion {0} could not set player ship '{1}' - not in region",
                                            name, ship != null ? ship.Name() : "(null)");
                }
            }

            // if this is a stand-alone server, set player ship to null
            else
            {
                if (player_ship != null)
                    player_ship.SetControls(null);

                current_view = -1;
                player_ship = null;
            }
        }
        public OrbitalRegion GetOrbitalRegion() { return orbital_region; }
        public Terrain GetTerrain() { return terrain; }
        public bool IsActive() { return active; }
        public bool IsAirSpace() { return type == TYPES.AIR_SPACE; }
        public bool IsOrbital() { return type == TYPES.REAL_SPACE; }
        public bool CanTimeSkip()
        {
            bool ok = false;

            if (player_ship != null)
            {
                ok = true;

                for (int i = 0; ok && i < ships.Count; i++)
                {
                    Ship s = ships[i];

                    if (s != player_ship && s.GetIFF() != 0 && s.GetIFF() != player_ship.GetIFF())
                    {
                        double dist = new Point(s.Location() - player_ship.Location()).Length;

                        if (s.IsStarship())
                            ok = dist > 60e3;
                        else
                            ok = dist > 30e3;
                    }
                }
            }

            return ok;
        }

        public virtual Ship FindShipByObjID(uint objid)
        {
            foreach (var ship in ships)
            {
                if (ship.GetObjID() == objid)
                    return ship;
            }

            return null;
        }
        public virtual Shot FindShotByObjID(uint objid)
        {
            Shot shot = null;

            foreach (var test in shots)
            {
                if (shot.GetObjID() == objid)
                {
                    shot = test;
                    break;
                }
            }

            if (shot == null)
            {
                foreach (var test in drones)
                {
                    if (test.GetObjID() == objid)
                    {
                        shot = test;
                        break;
                    }
                }
            }

            return shot;
        }

        public virtual void InsertObject(Ship ship)
        {
            if (ship == null) return;

            SimRegion orig = ship.GetRegion();

            if (orig != this)
            {
                if (orig != null)
                {
                    if (orig.active)
                        ship.Deactivate(sim.scene);

                    orig.ships.Remove(ship);
                    orig.carriers.Remove(ship);
                    orig.selection.Remove(ship);
                }

                ships.Add(ship);

                if (ship.NumFlightDecks() != 0)
                    carriers.Add(ship);

                TranslateObject(ship);
                ship.SetRegion(this);

                if (active)
                    ship.Activate(sim.scene);
            }
        }
        public virtual void InsertObject(Shot shot)
        {
            if (shot == null) return;

            SimRegion orig = shot.GetRegion();

            if (orig != this)
            {
                if (orig != null)
                    orig.shots.Remove(shot);

                shots.Add(shot);
                if (shot.IsDrone())
                    drones.Add((Drone)shot);

                TranslateObject(shot);
                shot.SetRegion(this);

                if (active)
                    shot.Activate(sim.scene);
            }
        }
        public virtual void InsertObject(Explosion exp)
        {
            if (exp == null) return;

            SimRegion orig = exp.GetRegion();

            if (orig != this)
            {
                if (orig != null)
                    orig.explosions.Remove(exp);

                explosions.Add(exp);
                TranslateObject(exp);
                exp.SetRegion(this);

                if (active)
                    exp.Activate(sim.scene);
            }
        }
        public virtual void InsertObject(Debris d)
        {
            if (d == null) return;

            SimRegion orig = d.GetRegion();

            if (orig != this)
            {
                if (orig != null)
                    orig.debris.Remove(d);

                debris.Add(d);
                TranslateObject(d);
                d.SetRegion(this);

                if (active)
                    d.Activate(sim.scene);
            }
        }
        public virtual void InsertObject(Asteroid a)
        {
            if (a == null) return;

            SimRegion orig = a.GetRegion();

            if (orig != this)
            {
                if (orig != null)
                    orig.asteroids.Remove(a);

                asteroids.Add(a);
                TranslateObject(a);
                a.SetRegion(this);

                if (active)
                    a.Activate(sim.scene);
            }
        }

        public string Name() { return name; }
        public TYPES Type() { return type; }
        public int NumShips() { return ships.Count; }
        public List<Ship> Ships() { return ships; }
        public List<Ship> Carriers() { return carriers; }
        public List<Shot> Shots() { return shots; }
        public List<Drone> Drones() { return drones; }
        public List<Debris> Rocks() { return debris; }
        public List<Asteroid> Roids() { return asteroids; }
        public List<Explosion> Explosions() { return explosions; }
        public List<SimRegion> Links() { return links; }
        public StarSystem System() { return star_system; }

        public Point Location() { return location; }

        public void SetSelection(Ship newsel)
        {
            selection.Clear();
            selection.Add(newsel);
        }
        public bool IsSelected(Ship s)
        {
            return selection.Contains(s);
        }
        public List<Ship> GetSelection()
        {
            return selection;
        }
        public void ClearSelection()
        {
            selection.Clear();
        }
        public void AddSelection(Ship newsel)
        {
            if (newsel == null ||
                newsel.GetFlightPhase() < OP_MODE.ACTIVE ||
                newsel.GetFlightPhase() >= OP_MODE.RECOVERY)
                return;

            if (!selection.Contains(newsel))
                selection.Add(newsel);
        }

        public List<Contact> TrackList(int iff)
        {
            if (iff >= 0 && iff < 5)
                return track_database[iff];

            List<Contact> empty = new List<Contact>();
            return empty;
        }

        public void ResolveTimeSkip(double seconds)
        {
            for (int i = 0; i < ships.Count; i++)
            {
                Ship ship = ships[i];
                Ship ward = ship.GetWard();

                // remember to burn fuel and fix stuff...
                ship.ExecSystems(seconds);
                ship.ExecMaintFrame(seconds);

                ship.ClearTrack();
                foreach (var contact in ship.ContactList())
                    contact.ClearTrack();

                if (ship.IsStatic())
                    continue;

                // if ship is cleared inbound, land him:
                InboundSlot inbound = ship.GetInbound();
                if (inbound != null)
                {
                    if (inbound.Cleared())
                    {
                        FlightDeck deck = inbound.GetDeck();

                        if (deck != null)
                        {
                            ship.SetCarrier((Ship)deck.GetCarrier(), deck);
                            ship.SetFlightPhase(OP_MODE.DOCKED);
                            ship.Stow();
                            deck.Clear(inbound.Index());
                        }
                    }

                    // cleared or not, once you're inbound, don't seek navpoints:
                    continue;
                }

                if (ship.GetHangar() != null)
                {
                    ship.GetHangar().ExecFrame(seconds);

                    List<FlightDeck> flight_decks = ship.FlightDecks();
                    for (int n = 0; n < flight_decks.Count; n++)
                        flight_decks[n].ExecFrame(seconds);
                }

                Instruction navpt = ship.GetNextNavPoint();
                Point dest = ship.Location();
                double speed = 500;
                double space = 2.0e3 * (ship.GetElementIndex() - 1);

                if (ship.IsStarship())
                    space *= 5;

                if (navpt != null && navpt.Action() == Instruction.ACTION.LAUNCH)
                {
                    ship.SetNavptStatus(navpt, Instruction.STATUS.COMPLETE);
                    navpt = ship.GetNextNavPoint();
                }

                if (navpt != null)
                {
                    dest = navpt.Location().OtherHand();
                    speed = navpt.Speed();
                }

                else if (ward != null)
                {
                    Point delta2 = ship.Location() - ward.Location();
                    delta2.Y = 0;

                    if (delta2.Length > 25e3)
                    {
                        delta2.Normalize();
                        dest = ward.Location() + delta2 * 25e3;
                    }
                }

                Point delta = dest - ship.Location();
                Point unit = delta;
                double dist = unit.Normalize() - space;

                if (dist > 1e3)
                {
                    if (speed < 50)
                        speed = 500;

                    double etr = dist / speed;

                    if (etr > seconds)
                        etr = seconds;

                    Point trans = unit * (speed * etr);

                    if (ship.GetFuelLevel() > 1)
                    {
                        ship.MoveTo(ship.Location() + trans);
                        ship.SetVelocity(unit * speed);
                    }
                    ship.LookAt(dest);

                    if (ship.IsStarship())
                    {
                        ship.SetFLCSMode(FLCS_MODE.FLCS_HELM);
                        ship.SetHelmHeading(ship.CompassHeading());
                        ship.SetHelmPitch(ship.CompassPitch());
                    }
                }

                else if (navpt != null && navpt.Status() <= Instruction.STATUS.ACTIVE)
                {
                    ship.SetNavptStatus(navpt, Instruction.STATUS.COMPLETE);
                }

                if (ward != null)
                {
                    Point ward_heading = ward.Heading();
                    ward_heading.Y = 0;
                    ward_heading.Normalize();

                    if (ship.GetFuelLevel() > 1)
                    {
                        ship.SetVelocity(ward.Velocity());
                    }
                    ship.LookAt(ship.Location() + ward_heading * 1e6);

                    if (ship.IsStarship())
                    {
                        ship.SetFLCSMode(FLCS_MODE.FLCS_HELM);
                        ship.SetHelmHeading(ship.CompassHeading());
                        ship.SetHelmPitch(ship.CompassPitch());
                    }
                }

                if (dist > 1 || ward != null)
                {
                    for (int j = 0; j < ships.Count; j++)
                    {
                        Ship test = ships[j];

                        if (ship != test && test.Mass() >= ship.Mass())
                        {
                            Point delta2 = ship.Location() - test.Location();

                            if (delta2.Length < ship.Radius() * 2 + test.Radius() * 2)
                            {
                                ship.MoveTo(test.Location() + RandomHelper.RandomPoint().OtherHand());
                            }
                        }
                    }
                }
            }

            DockShips();
        }


        public void CommitMission()
        {
            for (int i = 0; i < dead_ships.Count; i++)
            {
                Ship s = dead_ships[i];

                if (s.GetCombatUnit() != null && s.GetFlightPhase() != OP_MODE.DOCKED)
                    s.GetCombatUnit().Kill(1);
            }

            for (int i = 0; i < ships.Count; i++)
            {
                Ship s = ships[i];
                CombatUnit u = s.GetCombatUnit();

                if (u != null)
                {
                    Point u_loc = s.Location().OtherHand();
                    if (u_loc.Z > 20e3)
                        u_loc.Z = 20e3;
                    else if (u_loc.Z < -20e3)
                        u_loc.Z = -20e3;

                    if (u.IsStarship())
                    {
                        u.SetRegion(s.GetRegion().Name());
                        u.MoveTo(u_loc);
                    }

                    if (!u.IsDropship())
                    {
                        if (s.Integrity() < 1)
                            u.Kill(1);
                        else
                            u.SetSustainedDamage(s.Design().integrity - s.Integrity());
                    }

                    CombatGroup g = u.GetCombatGroup();
                    if (g != null && g.Type() > CombatGroup.GROUP_TYPE.FLEET && g.GetFirstUnit() == u)
                    {
                        if (!g.IsZoneLocked())
                            g.SetRegion(s.GetRegion().Name());
                        else
                            u.SetRegion(g.GetRegion());

                        g.MoveTo(u_loc);
                    }
                }
            }
        }
        protected void TranslateObject(SimObject obj)
        {
            if (orbital_region != null)
                location = orbital_region.Location();

            if (obj != null)
            {
                SimRegion orig = obj.GetRegion();
                if (orig != null)
                {
                    Point delta = Location() - orig.Location();
                    delta = delta.OtherHand();
                    obj.TranslateBy(delta);
                }
            }
        }

        protected void AttachPlayerShip(int index)
        {
            if (player_ship != null)
                player_ship.SetControls(null);

            current_view = index;
            player_ship = ships[current_view];

            CameraDirector cam_dir = CameraDirector.GetInstance();
            if (cam_dir != null)
                cam_dir.SetShip(player_ship);

            if (sim.dust != null)
                sim.dust.Reset(player_ship.Location());

            if (!sim.IsTestMode())
                player_ship.SetControls(sim.ctrl);

            MouseController mouse_con = MouseController.GetInstance();
            if (mouse_con != null)
                mouse_con.SetActive(false);
        }
        protected void DestroyShips()
        {
            // Classical pattern to remove elements from a generic list while iterating over it
            // Iterate  list in reverse with a for loop
            for (int i = ships.Count - 1; i >= 0; i--)
            {
                Ship ship = ships[i];

                if (ship.IsDead())
                {
                    // must use the iterator to remove the current
                    // item from the container:
                    ships.RemoveAt(i);
                    DestroyShip(ship);
                }
            }
        }
        public void DestroyShip(Ship ship)
        {
            if (ship == null) return;

            Ship spawn = null;

            ships.Remove(ship);
            carriers.Remove(ship);
            selection.Remove(ship);

            string rgn_name = null;
            if (ship.GetRegion() != null)
                rgn_name = ship.GetRegion().Name();

            bool player_destroyed = (player_ship == ship);

            string ship_name = ship.Name();
            string ship_reg = ship.Registry();

            ShipDesign ship_design = ship.Design();
            int ship_iff = ship.GetIFF();
            int cmd_ai = ship.GetCommandAILevel();
            bool respawn = sim.IsTestMode() && !ship.IsGroundUnit();
            bool observe = false;

            if (!respawn)
                respawn = ship.RespawnCount() > 0;

            if (sim.netgame != null)
            {
                if (!respawn)
                    observe = player_destroyed;
            }

            if (respawn || observe)
            {
                if (sim.netgame == null || !respawn)
                    ship.SetRespawnLoc(RandomHelper.RandomPoint() * 2);

                Point spawn_loc = ship.RespawnLoc();

                if (ship.IsAirborne() && spawn_loc.Z < 5e3)
                    spawn_loc.Z = RandomHelper.Random(8e3, 10e3);

                spawn = sim.CreateShip(ship_name, ship_reg, ship_design, rgn_name, spawn_loc, ship_iff, cmd_ai, observe ? null : ship.GetLoadout());
                spawn.SetRespawnCount(ship.RespawnCount() - 1);
                spawn.SetNetObserver(observe);

                if (sim.netgame != null && respawn)
                    sim.netgame.Respawn(ship.GetObjID(), spawn);

                int n = ship_name.Length;
                if (n > 2)
                {
                    if (ship_name[n - 2] == ' ' && Char.IsDigit(ship_name[n - 1]))
                        ship_name = ship_name.Substring(0, n - 2);
                }

                Element elem = sim.FindElement(ship_name);
                if (elem != null)
                    elem.AddShip(spawn, ship.GetOrigElementIndex());
                else
                    ErrLogger.PrintLine("Warning: No Element found for '{0}' on respawn.", ship_name);

                if (player_destroyed)
                    SetPlayerShip(spawn);
            }
            else
            {
                // close mission, return to menu:
                if (player_destroyed)
                {
                    Starshatter stars = Starshatter.GetInstance();
                    if (stars != null)
                        stars.SetGameMode(Starshatter.MODE.PLAN_MODE);
                }
            }

            sim.ProcessEventTrigger(MissionEvent.EVENT_TRIGGER.TRIGGER_DESTROYED, 0, ship.Name());

            dead_ships.Add(ship);
            ship.Destroy(); if (ship == null) return;

            //Ship spawn = null;

            ships.Remove(ship);
            carriers.Remove(ship);
            selection.Remove(ship);

            //string rgn_name = null;
            if (ship.GetRegion() != null)
                rgn_name = ship.GetRegion().Name();

            player_destroyed = (player_ship == ship);

            ship_name = ship.Name();
            ship_reg = ship.Registry();

            ship_design = ship.Design();
            ship_iff = ship.GetIFF();
            cmd_ai = ship.GetCommandAILevel();
            respawn = sim.IsTestMode() && !ship.IsGroundUnit();
            observe = false;

            if (!respawn)
                respawn = ship.RespawnCount() > 0;

            if (sim.netgame != null)
            {
                if (!respawn)
                    observe = player_destroyed;
            }

            if (respawn || observe)
            {
                if (sim.netgame == null || !respawn)
                    ship.SetRespawnLoc(RandomHelper.RandomPoint() * 2);

                Point spawn_loc = ship.RespawnLoc();

                if (ship.IsAirborne() && spawn_loc.Z < 5e3)
                    spawn_loc.Z = RandomHelper.Random(8e3, 10e3);

                spawn = sim.CreateShip(ship_name, ship_reg, ship_design, rgn_name, spawn_loc, ship_iff, cmd_ai, observe ? null : ship.GetLoadout());
                spawn.SetRespawnCount(ship.RespawnCount() - 1);
                spawn.SetNetObserver(observe);

                if (sim.netgame != null && respawn)
                    sim.netgame.Respawn(ship.GetObjID(), spawn);

                int n = ship_name.Length;
                if (n > 2)
                {
                    if (ship_name[n - 2] == ' ' && Char.IsDigit(ship_name[n - 1]))
                        ship_name = ship_name.Substring(0, n - 2);
                }

                Element elem = sim.FindElement(ship_name);
                if (elem != null)
                    elem.AddShip(spawn, ship.GetOrigElementIndex());
                else
                    ErrLogger.PrintLine("Warning: No Element found for '{0}' on respawn.", ship_name);

                if (player_destroyed)
                    SetPlayerShip(spawn);
            }
            else
            {
                // close mission, return to menu:
                if (player_destroyed)
                {
                    Starshatter stars = Starshatter.GetInstance();
                    if (stars != null)
                        stars.SetGameMode(Starshatter.MODE.PLAN_MODE);
                }
            }

            sim.ProcessEventTrigger(MissionEvent.EVENT_TRIGGER.TRIGGER_DESTROYED, 0, ship.Name());

            dead_ships.Add(ship);
            ship.Destroy();
        }
        public void NetDockShip(Ship ship, Ship carrier, FlightDeck deck)
        {
            if (ship == null || carrier == null || deck == null) return;

            deck.Dock(ship);
        }

        protected void UpdateSky(double seconds, Point ref_)
        {
            Dust dust = sim.dust;

            if (dust != null)
            {
                if (orbital_region != null && orbital_region.Type() == Orbital.OrbitalType.TERRAIN)
                {
                    dust.Hide();
                }
                else
                {
                    dust.Show();

                    dust.ExecFrame(seconds, ref_);
#if TODO
                    if (player_ship != null && dust.Hidden())
                    {
                        dust.Reset(player_ship.Location());
                        dust.Show();
                    }
#endif 
                    throw new NotImplementedException();
                }
            }

            //foreach (Asteroid a in asteroids)
            //{
            //    a.ExecFrame(seconds);
            //}
            asteroids.ForEach(i => i.ExecFrame(seconds));
        }
        protected void UpdateShips(double seconds)
        {
            int ship_index = 0;
            if (ai_index > ships.Count)
                ai_index = 0;

            foreach (Ship ship in ships)
            {
                if (ship_index == ai_index || ship == player_ship)
                    ship.SetAIMode(2);
                else
                    ship.SetAIMode(1);

                ship.ExecFrame(seconds);
                ship_index++;
            }

            ai_index++; ;
        }
        protected void UpdateShots(double seconds)
        {
            for (int i = shots.Count - 1; i >= 0; i--)
            {
                Shot shot = shots[i];

                shot.ExecFrame(seconds);

                if (shot.Design().flak)
                {
                    SeekerAI seeker = (SeekerAI)shot.GetDirector();

                    if (shot.Life() < 0.02 || seeker != null && seeker.Overshot())
                    {
                        shot.SetFuse(0.001); // set lifetime to ~zero
                        sim.CreateSplashDamage(shot);
                    }
                }

                if (shot.Life() < 0.01)
                {  // died of old age
                    NetUtil.SendWepDestroy(shot);

                    if (shot.IsDrone())
                        drones.Remove((Drone)shot);

                    shots.RemoveAt(i);
                    // delete shot;
                    shot = null;
                }
            }
        }
        protected void UpdateExplosions(double seconds)
        {
            for (int i = explosions.Count - 1; i >= 0; i--)
            {
                Explosion exp = explosions[i];
                exp.ExecFrame(seconds);

                if (exp.Life() < 0.01)
                {  // died of old age
                    explosions.RemoveAt(i);
                    // delete exp;
                }
            }

            for (int i = debris.Count - 1; i >= 0; i--)
            {
                Debris d = debris[i];
                d.ExecFrame(seconds);

                if (d.Life() < 0.01)
                {  // died of old age
                    debris.RemoveAt(i);
                    // delete d;
                }
            }
        }
        protected void UpdateTracks(double seconds)
        {
            for (int i = 0; i < 5; i++)
            {
                List<Contact> track_list = track_database[i];

                for (int j = track_list.Count - 1; j >= 0; j--)

                {
                    Contact t = track_list[j];
                    Ship c_ship = t.GetShip();
                    Shot c_shot = t.GetShot();
                    double c_life = 0;

                    if (c_ship != null)
                    {
                        c_life = c_ship.Life();

                        // look for quantum jumps and orbit transitions:
                        if (c_ship.GetRegion() != this || c_ship.IsNetObserver())
                            c_life = 0;
                    }

                    else if (c_shot != null)
                        c_life = c_shot.Life();

                    if (t.Age() < 0 || c_life == 0)
                    {
                        track_list.RemoveAt(j);
                        // delete t;
                    }

                    else
                    {
                        t.Reset();
                    }
                }
            }
        }

        protected void DamageShips()
        {
            if (ships.Count == 0 || shots.Count == 0)
                return;

            Point impact = new Point();

            // FOR EACH SHOT IN THE REGION:
            for (int i = shots.Count - 1; i >= 0; i--)
            {
                Shot shot = shots[i];
                Ship owner = shot.Owner();
                string owner_name;

                if (owner != null)
                    owner_name = owner.Name();
                else
                    owner_name = "[KIA]";

                // CHECK FOR COLLISION WITH A SHIP:
                foreach (Ship ship in ships)
                {
                    if (shot == null) break;
                    int hit = ship.HitBy(shot, ref impact);

                    if (hit != 0)
                    {
                        // recon imager:
                        if (shot.Damage() < 0)
                        {
                            ShipStats shooter = ShipStats.Find(owner_name);
                            if (shooter != null)
                            {
                                shooter.AddEvent(SimEvent.EVENT.SCAN_TARGET, ship.Name());
                            }
                        }

                        // live round:
                        else if (shot.Damage() > 0)
                        {
                            bool ship_destroyed = (!ship.InTransition() && ship.Integrity() < 1.0f);

                            // then delete the ship:
                            if (ship_destroyed)
                            {
                                NetUtil.SendObjKill(ship, owner, shot.IsMissile() ? NetObjKill.KillType.KILL_SECONDARY : NetObjKill.KillType.KILL_PRIMARY);
                                Director director = null;

                                ErrLogger.PrintLine("    {0} Killed {1} ({2})", owner_name, ship.Name(), Sim.FormatGameTime());
#if TODO
                                if (owner != null)
                                    director = owner.GetDirector();

                                // alert the killer
                                if (director != null && director.Type() > SteerAI.SteerType.SEEKER && director.Type() < SteerAI.SteerType.GROUND)
                                {
                                    ShipAI shipAI = (ShipAI)director;
                                    shipAI.Splash(ship);
                                }
#endif
                                throw new NotImplementedException();

                                // record the kill
                                ShipStats killer = ShipStats.Find(owner_name);
                                if (killer != null)
                                {
                                    if (shot.IsMissile())
                                        killer.AddEvent(SimEvent.EVENT.MISSILE_KILL, ship.Name());
                                    else
                                        killer.AddEvent(SimEvent.EVENT.GUNS_KILL, ship.Name());
                                }

                                if (owner != null && owner.GetIFF() != ship.GetIFF())
                                {
                                    if (ship.GetIFF() > 0 || owner.GetIFF() > 1)
                                    {
                                        killer.AddPoints(ship.Value());

                                        Element elem = owner.GetElement();
                                        if (elem != null)
                                        {
                                            if (owner.GetElementIndex() > 1)
                                            {
                                                Ship s = elem.GetShip(1);

                                                if (s != null)
                                                {
                                                    ShipStats cmdr_stats = ShipStats.Find(s.Name());
                                                    if (cmdr_stats != null)
                                                    {
                                                        cmdr_stats.AddCommandPoints(ship.Value() / 2);
                                                    }
                                                }
                                            }

                                            Element cmdr = elem.GetCommander();
                                            if (cmdr != null)
                                            {
                                                Ship s = cmdr.GetShip(1);

                                                if (s != null)
                                                {
                                                    ShipStats cmdr_stats = ShipStats.Find(s.Name());
                                                    if (cmdr_stats != null)
                                                    {
                                                        cmdr_stats.AddCommandPoints(ship.Value() / 2);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                ShipStats killee = ShipStats.Find(ship.Name());
                                if (killee != null)
                                    killee.AddEvent(SimEvent.EVENT.DESTROYED, owner_name);

                                ship.DeathSpiral();
                            }
                        }

                        // finally, consume the shot:
                        if (!shot.IsBeam())
                        {
                            if (owner != null)
                            {
                                ShipStats stats = ShipStats.Find(owner_name);
                                if (shot.Design().primary)
                                    stats.AddGunHit();
                                else if (shot.Damage() > 0)
                                    stats.AddMissileHit();
                            }

                            NetUtil.SendWepDestroy(shot);

                            if (shot.IsDrone())
                                drones.Remove((Drone)shot);

                            shots.RemoveAt(i);
                            //delete shot;
                            shot = null;
                        }
                        else if (!shot.HitTarget())
                        {
                            shot.SetHitTarget(true);

                            if (owner != null)
                            {
                                ShipStats stats = ShipStats.Find(owner_name);
                                if (shot.Design().primary)
                                    stats.AddGunHit();
                            }
                        }
                    }
                }

                // CHECK FOR COLLISION WITH A DRONE:
                if (shot != null && shot.Design().target_type.HasFlag(CLASSIFICATION.DRONE))
                {
                    foreach (Drone d in drones)
                    {
                        if (shot == null) break;

                        if (d == shot || d.Owner() == owner)
                            continue;

                        int hit = d.HitBy(shot, ref impact);
                        if (hit != 0)
                        {
                            bool destroyed = (d.Integrity() < 1.0f);

                            // then mark the drone for deletion:
                            if (destroyed)
                            {
                                NetUtil.SendWepDestroy(d);
                                sim.CreateExplosion(d.Location(), d.Velocity(), (Explosion.ExplosionType)21, 1.0f, 1.0f, this);
                                d.SetLife(0);
                            }

                            // finally, consume the shot:
                            if (!shot.IsBeam())
                            {
                                if (owner != null)
                                {
                                    ShipStats stats = ShipStats.Find(owner_name);
                                    if (shot.Design().primary)
                                        stats.AddGunHit();
                                    else
                                        stats.AddMissileHit();
                                }

                                NetUtil.SendWepDestroy(shot);

                                if (shot.IsDrone())
                                    drones.Remove((Drone)shot);

                                shots.RemoveAt(i);
                                // delete shot;
                                shot = null;
                            }
                        }
                    }
                }

                // CHECK FOR COLLISION WITH DEBRIS:
                for (int j = debris.Count - 1; j >= 0; j--)
                {
                    if (shot == null) break;
                    Debris d = debris[i];

                    if (d.Radius() < 50)
                        continue;

                    int hit = d.HitBy(shot, out impact);
                    if (hit != 0)
                    {
                        bool destroyed = (d.Integrity() < 1.0f);

                        // then delete the debris:
                        if (destroyed)
                        {
                            sim.CreateExplosion(d.Location(), d.Velocity(), Explosion.ExplosionType.LARGE_EXPLOSION, 1.0f, 1.0f, this);
                            debris.RemoveAt(j);
                            //delete d;
                        }

                        // finally, consume the shot:
                        if (!shot.IsBeam())
                        {
                            NetUtil.SendWepDestroy(shot);
                            if (shot.IsDrone())
                                drones.Remove((Drone)shot);

                            shots.RemoveAt(i);
                            //delete shot;
                            shot = null;
                        }
                    }
                }

                // CHECK FOR COLLISION WITH ASTEROIDS:
                foreach (Asteroid a in asteroids)
                {
                    if (shot == null) break;

                    int hit = a.HitBy(shot, out impact);
                    if (hit != 0)
                    {
                        if (!shot.IsBeam())
                        {
                            if (shot.IsDrone())
                                drones.Remove((Drone)shot);

                            shots.RemoveAt(i);
                            //delete shot;
                            shot = null;
                        }
                    }
                }
            }
        }
        protected void CollideShips()
        {
            if (ships.Count < 2 && debris.Count < 1)
                return;

            List<Ship> kill_list = new List<Ship>();

            int s_index = 0;

            foreach (Ship ship in ships)
            {

                if (ship.InTransition() ||
                        ship.GetFlightPhase() < OP_MODE.ACTIVE ||
                        ship.MissionClock() < 10000 ||
                        ship.IsNetObserver())
                    continue;

                int t_index = 0;
                foreach (Ship targ in ships)
                {

                    if (t_index++ <= s_index) continue;

                    if (targ == ship) continue;

                    if (targ.InTransition() ||
                            targ.GetFlightPhase() < OP_MODE.ACTIVE ||
                            targ.MissionClock() < 10000 ||
                            targ.IsNetObserver())
                        continue;

                    // ignore AI fighter collisions:
                    if (ship.IsDropship() &&
                            ship != player_ship &&
                            targ.IsDropship() &&
                            targ != player_ship)
                        continue;

                    // don't collide with own runway!
                    if (ship.IsAirborne() && ship.GetCarrier() == targ)
                        continue;
                    if (targ.IsAirborne() && targ.GetCarrier() == ship)
                        continue;

                    // impact:
                    if (ship.CollidesWith(targ))
                    {
                        Vector3D tv1 = targ.Velocity();
                        Vector3D sv1 = ship.Velocity();

                        Physical.SemiElasticCollision(ship, targ);

                        Vector3D tv2 = targ.Velocity();
                        Vector3D sv2 = ship.Velocity();

                        double dvs = (sv2 - sv1).Length;
                        double dvt = (tv2 - tv1).Length;

                        if (dvs > 20) dvs *= dvs;
                        if (dvt > 20) dvt *= dvt;

                        if (!NetGame.IsNetGameClient())
                        {
                            double old_integrity = ship.Integrity();
                            ship.InflictDamage(dvs);
                            double hull_damage = old_integrity - ship.Integrity();
                            NetUtil.SendObjDamage(ship, hull_damage);

                            old_integrity = targ.Integrity();
                            targ.InflictDamage(dvt);
                            hull_damage = old_integrity - targ.Integrity();
                            NetUtil.SendObjDamage(targ, hull_damage);
                        }

                        // then delete the ship:
                        if (targ.Integrity() < 1.0f)
                        {
                            NetUtil.SendObjKill(targ, ship, NetObjKill.KillType.KILL_COLLISION);
                            ErrLogger.PrintLine("   ship {0} died in collision with {1} ({2})", targ.Name(), ship.Name(), Sim.FormatGameTime());
                            if (!kill_list.Contains(targ))
                            {
                                ShipStats r = ShipStats.Find(targ.Name());
                                if (r != null) r.AddEvent(SimEvent.EVENT.COLLIDE, ship.Name());

                                if (targ.GetIFF() > 0 && ship.GetIFF() != targ.GetIFF())
                                {
                                    r = ShipStats.Find(ship.Name());
                                    if (r != null) r.AddPoints(targ.Value());
                                }

                                kill_list.Add(targ);
                            }
                        }

                        if (ship.Integrity() < 1.0f)
                        {
                            NetUtil.SendObjKill(ship, targ, NetObjKill.KillType.KILL_COLLISION);
                            ErrLogger.PrintLine("   ship {0} died in collision with {1} ({2})", ship.Name(), targ.Name(), Sim.FormatGameTime());
                            if (!kill_list.Contains(ship))
                            {
                                ShipStats r = ShipStats.Find(ship.Name());
                                if (r != null) r.AddEvent(SimEvent.EVENT.COLLIDE, targ.Name());

                                if (ship.GetIFF() > 0 && ship.GetIFF() != targ.GetIFF())
                                {
                                    r = ShipStats.Find(targ.Name());
                                    if (r != null) r.AddPoints(ship.Value());
                                }

                                kill_list.Add(ship);
                            }
                        }
                    }
                }

                for (int i = debris.Count - 1; i >= 0; i--)
                {
                    Debris d = debris[i];

                    if (d.Radius() < 50)
                        continue;

                    if (ship.CollidesWith(d))
                    {
                        Vector3D tv1 = d.Velocity();
                        Vector3D sv1 = ship.Velocity();

                        Physical.SemiElasticCollision(ship, d);

                        Vector3D tv2 = d.Velocity();
                        Vector3D sv2 = ship.Velocity();

                        if (!NetGame.IsNetGameClient())
                        {
                            ship.InflictDamage((sv2 - sv1).Length);
                        }

                        d.InflictDamage((tv2 - tv1).Length);

                        // then delete the debris:
                        if (d.Integrity() < 1.0f)
                        {
                            sim.CreateExplosion(d.Location(), d.Velocity(), Explosion.ExplosionType.LARGE_EXPLOSION, 1.0f, 1.0f, this);
                            debris.RemoveAt(i);
                            // delete d;
                        }

                        if (ship.Integrity() < 1.0f)
                        {
                            if (!kill_list.Contains(ship))
                            {
                                ShipStats r = ShipStats.Find(ship.Name());
                                if (r != null) r.AddEvent(SimEvent.EVENT.COLLIDE, ContentBundle.Instance.GetText("DEBRIS"));

                                kill_list.Add(ship);
                            }
                        }
                    }
                }

                foreach (Asteroid a in asteroids)
                {

                    if (ship.CollidesWith(a))
                    {
                        Vector3D sv1 = ship.Velocity();
                        Physical.SemiElasticCollision(ship, a);
                        Vector3D sv2 = ship.Velocity();

                        if (!NetGame.IsNetGameClient())
                        {
                            ship.InflictDamage((sv2 - sv1).Length * 10);
                        }

                        if (ship.Integrity() < 1.0f)
                        {
                            if (!kill_list.Contains(ship))
                            {
                                ShipStats r = ShipStats.Find(ship.Name());
                                if (r != null) r.AddEvent(SimEvent.EVENT.COLLIDE, ContentBundle.Instance.GetText("ASTEROID"));

                                kill_list.Add(ship);
                            }
                        }
                    }
                }

                s_index++;
            }

            foreach (Ship kill in kill_list)
            {
                kill.DeathSpiral();
            }
        }
        protected void CrashShips()
        {
            if (type != TYPES.AIR_SPACE || NetGame.IsNetGameClient())
                return;

            foreach (Ship ship in ships)
            {
                if (!ship.IsGroundUnit() &&
                        !ship.InTransition() &&
                        ship.Class() != CLASSIFICATION.LCA &&
                        ship.AltitudeAGL() < ship.Radius() / 2)
                {
                    if (ship.GetFlightPhase() == OP_MODE.ACTIVE || ship.GetFlightPhase() == OP_MODE.APPROACH)
                    {
                        ship.InflictDamage(1e6);

                        if (ship.Integrity() < 1.0f)
                        {
                            ErrLogger.PrintLine("    ship destroyed by crash: {0} ({1})", ship.Name(), Sim.FormatGameTime());
                            ShipStats r = ShipStats.Find(ship.Name());
                            if (r != null) r.AddEvent(SimEvent.EVENT.CRASH);

                            ship.DeathSpiral();
                        }
                    }
                }
            }

            for (int i = shots.Count - 1; i >= 0; i--)
            {
                Shot shot = shots[i];

                if (shot.IsBeam() || shot.IsDecoy())
                    continue;

                if (shot.AltitudeMSL() < 5e3 &&
                        shot.AltitudeAGL() < 5)
                {

                    // shot hit the ground, destroy it:
                    NetUtil.SendWepDestroy(shot);

                    if (shot.IsDrone())
                        drones.Remove((Drone)shot);

                    shots.RemoveAt(i);
                    // delete shot;
                }
            }
        }
        protected void DockShips()
        {
            if (ships.Count == 0)
                return;

            for (int i = ships.Count - 1; i >= 0; i--)
            {
                Ship ship = ships[i];
                bool docked = (ship.GetFlightPhase() == OP_MODE.DOCKED);

                if (docked)
                {
                    sim.ProcessEventTrigger(MissionEvent.EVENT_TRIGGER.TRIGGER_DOCK, 0, ship.Name());

                    // who did this ship dock with?
                    Ship carrier = ship.GetCarrier();

                    if (carrier != null)
                    {
                        ShipStats s = ShipStats.Find(ship.Name());
                        if (s != null)
                        {
                            if (ship.IsAirborne())
                                s.AddEvent(SimEvent.EVENT.LAND, carrier.Name());
                            else
                                s.AddEvent(SimEvent.EVENT.DOCK, carrier.Name());
                        }

                        ShipStats c = ShipStats.Find(carrier.Name());
                        if (c != null) c.AddEvent(SimEvent.EVENT.RECOVER_SHIP, ship.Name());
                    }

                    // then delete the ship:
                    bool player_docked = (player_ship == ship);
                    string ship_name = ship.Name();

                    selection.Remove(ship);
                    ships.RemoveAt(i);
                    dead_ships.Add(ship);
                    ship.Destroy();

                    if (player_docked)
                    {
                        // close mission, return to menu:
                        Starshatter stars = Starshatter.GetInstance();
                        if (stars != null)
                            stars.SetGameMode(Starshatter.MODE.PLAN_MODE);
                    }

                    if (carrier != null)
                        ErrLogger.PrintLine("    {0} Docked with {1}", ship_name, carrier.Name());
                }
            }
        }

        protected Sim sim;
        public string name;
        protected TYPES type;
        protected StarSystem star_system;
        public OrbitalRegion orbital_region;
        protected Point location;
        protected GridNode grid;
        protected Terrain terrain;
        protected bool active;

        protected Ship player_ship;
        protected int current_view;
        protected List<Ship> ships;
        protected List<Ship> carriers;
        protected List<Ship> selection;
        protected List<Ship> dead_ships;
        protected List<Shot> shots;
        protected List<Drone> drones;
        protected List<Explosion> explosions;
        protected List<Debris> debris;
        protected List<Asteroid> asteroids;
        protected List<Contact>[] track_database = new List<Contact>[5];
        protected List<SimRegion> links;

        protected uint sim_time;
        protected int ai_index;

        protected Random rand = new Random();
    }
}
