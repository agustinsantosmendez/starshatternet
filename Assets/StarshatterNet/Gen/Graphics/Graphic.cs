﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Graphic.h/Graphic.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Abstract 3D Graphic Object
*/
using System;
using DigitalRune.Mathematics.Algebra;
using UnityEngine;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Gen.Graphics
{
    public class Graphic : IDisposable
    {
        public enum TYPE { OTHER, SOLID, SPRITE, BOLT, QUAD };

        [Flags]
        public enum RENDER_FLAGS
        {
            RENDER_SOLID = 0x0001,
            RENDER_ALPHA = 0x0002,
            RENDER_ADDITIVE = 0x0004,
            RENDER_FIRST_LIGHT = 0x1000,
            RENDER_ADD_LIGHT = 0x2000
        };

        public Graphic(string name = null, bool createGameObjet = true)
        {
            id = id_key++;
            if (name == null)
                name = "Graphic:" + id;
            if (createGameObjet)
            {
                gameObj = new GameObject(name);
                rectTransform = gameObj.AddComponent<RectTransform>();
                rectTransform.anchorMin = Vector2.up;
                rectTransform.anchorMax = Vector2.up;
            }
        }

        //public virtual ~Graphic();

        //int operator ==(const Graphic& g)   { return id == g.id; }
        //int operator <(const Graphic& g);
        //int operator <=(const Graphic& g);

        // operations
        //virtual void Render(Video  video, DWORD flags) { }
        public virtual void Update() { }
        public virtual void SetOrientation(Matrix33D o) { }
        public virtual void SetOrientation(Quaternion d) { }

        public virtual bool CollidesWith(Graphic o)
        {
            Point delta_loc = loc - o.loc;

            // bounding spheres test:
            if (delta_loc.Length > radius + o.radius)
                return false;

            return true;
        }

        // accessors / mutators
        public int Identity() { return id; }
        public string Name() { return (gameObj != null ? gameObj.name : name); }
        public bool IsVisible() { return visible; }
        public void SetVisible(bool v) { visible = v; }
        public float Radius() { return radius; }

        public Point Location() { return loc; }
        public void MoveTo(Point p)
        {
            loc = p;
            if (rectTransform != null)
                rectTransform.anchoredPosition = new Vector3((float)p.X, -(float)p.Y, (float)p.Z);
        }
        public void MoveTo(float x, float y, float z)
        {
            if (rectTransform != null)
                rectTransform.anchoredPosition = new Vector3(x, -y, z);
        }
        public virtual void TranslateBy(Point ref_)
        {
            MoveTo(loc - ref_);
        }
        public virtual void TranslateBy(float x, float y, float z)
        {
            MoveTo(loc - new Point(x, y, z));
        }

        public virtual float Depth() { return depth; }
        public virtual void SetDepth(float d) { depth = d; }
        public static int Nearer(Graphic a, Graphic b)
        {
            if (a.depth < b.depth) return -1;
            else if (a.depth == b.depth) return 0;
            else return 1;
        }
        public static int Farther(Graphic a, Graphic b)
        {
            if (a.depth > b.depth) return -1;
            else if (a.depth == b.depth) return 0;
            else return 1;
        }

        public virtual bool IsInfinite() { return infinite; }
        public virtual void SetInfinite(bool b)
        {
            infinite = b;

            if (infinite)
                depth = 1.0e9f;
        }
        public virtual bool IsForeground() { return foreground; }
        public virtual void SetForeground(bool f) { foreground = f; }
        public virtual bool IsBackground() { return background; }
        public virtual void SetBackground(bool b) { background = b; }

        public virtual bool Hidden() { return hidden; }
        public virtual int Life() { return life; }
        public virtual void Destroy()
        {
            if (scene != null)
                scene.DelGraphic(this);

            // delete this;
        }
        public virtual void Hide()
        {
            hidden = true;
            if (gameObj != null)
                gameObj.SetActive(!hidden);
        }
        public virtual void Show()
        {
            hidden = false;
            if (gameObj != null)
                gameObj.SetActive(!hidden);
        }
        public virtual bool Luminous() { return luminous; }
        public virtual void SetLuminous(bool l) { luminous = l; }
        public virtual bool Translucent() { return trans; }
        public virtual bool CastsShadow() { return shadow; }
        public virtual void SetShadow(bool s) { shadow = s; }

        public virtual bool IsSolid() { return false; }
        public virtual bool IsSprite() { return false; }
        public virtual bool IsBolt() { return false; }
        public virtual bool IsQuad() { return false; }

        public virtual void ProjectScreenRect(Projector p)
        {
            screen_rect.x = 2000;
            screen_rect.y = 2000;
            screen_rect.width = 0;
            screen_rect.height = 0;
        }

        public Rect ScreenRect() { return screen_rect; }
        public virtual Scene GetScene() { return scene; }
        public virtual void SetScene(Scene s) { scene = s; }
        public virtual bool CheckRayIntersection(Point pt, Point vpn, double len, Point ipt,
                                                bool treat_translucent_polys_as_solid = true)
        { return false; }

        public virtual bool CheckVisibility(Projector projector)
        {
#if TODO
            if (projector.IsVisible(Location(), Radius()) &&
                    projector.ApparentRadius(Location(), Radius()) > 1)
            {

                visible = true;
            }
            else
            {
                visible = false;
                screen_rect.x = 2000;
                screen_rect.y = 2000;
                screen_rect.w = 0;
                screen_rect.h = 0;
            }

            return visible;
#endif
            throw new NotImplementedException();
        }


        public override bool Equals(object obj)
        {
            var graphic = obj as Graphic;
            return graphic != null &&
                   id == graphic.id;
        }

        public override int GetHashCode()
        {
            return 1877310944 + id.GetHashCode();
        }
        public static bool operator ==(Graphic l, Graphic r)
        {
            if (object.ReferenceEquals(l, r))
            {
                return true;     // if both the same object, or both null
            }
            // If one is null, but not both, return false.
            if (((object)l == null) || ((object)r == null))
            {
                return false;
            }

            return (l.id == r.id);
        }

        public static bool operator !=(Graphic l, Graphic r)
        {
            return !(l == r);
        }
        public static bool operator <(Graphic l, Graphic r)
        {
            if (!l.infinite && r.infinite)
                return true;

            else if (l.infinite && !r.infinite)
                return false;

            double za = Math.Abs(l.Depth());
            double zb = Math.Abs(r.Depth());

            return (za < zb);
        }

        public static bool operator >(Graphic l, Graphic r)
        {
            if (!l.infinite && r.infinite)
                return true;

            else if (l.infinite && !r.infinite)
                return false;

            double za = Math.Abs(l.Depth());
            double zb = Math.Abs(r.Depth());

            return (za > zb);
        }

        public static bool operator <=(Graphic l, Graphic r)
        {
            if (!l.infinite && r.infinite)
                return true;

            else if (l.infinite && !r.infinite)
                return false;

            double za = Math.Abs(l.Depth());
            double zb = Math.Abs(r.Depth());

            return (za <= zb);
        }

        public static bool operator >=(Graphic l, Graphic r)
        {
            if (!l.infinite && r.infinite)
                return true;

            else if (l.infinite && !r.infinite)
                return false;

            double za = Math.Abs(l.Depth());
            double zb = Math.Abs(r.Depth());

            return (za >= zb);
        }

        protected static int id_key = 0;

        protected int id;
        protected Point loc = new Point(0.0f, 0.0f, 0.0f);
        protected float depth = 0.0f;
        protected float radius = 0.0f;
        protected int life = -1;

        protected bool visible = true;
        protected bool infinite = false;
        protected bool foreground = false;
        protected bool background;
        protected bool hidden = false;
        protected bool trans = false;
        protected bool shadow = false;
        protected bool luminous = false;

        protected Rect screen_rect = new Rect(0, 0, 0, 0);
        protected Scene scene = null;
        protected string name = "Graphic";

        public GameObject gameObj;
        protected RectTransform rectTransform;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Graphic() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
