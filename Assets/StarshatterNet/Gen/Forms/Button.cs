﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      Button.h/Button.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Button class
*/
using System;
using StarshatterNet.Audio;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Views;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.Int32;

namespace StarshatterNet.Gen.Forms
{
    public class Button : ActiveWindow
    {
        public enum SOUNDS
        {
            SND_BUTTON,
            SND_CLICK,
            SND_SWISH,
            SND_CHIRP,
            SND_ACCEPT,
            SND_REJECT,
            SND_CONFIRM,
            SND_LIST_SELECT,
            SND_LIST_SCROLL,
            SND_LIST_DROP,
            SND_COMBO_OPEN,
            SND_COMBO_CLOSE,
            SND_COMBO_HILITE,
            SND_COMBO_SELECT,
            SND_MENU_OPEN,
            SND_MENU_CLOSE,
            SND_MENU_SELECT,
            SND_MENU_HILITE
        };

        public Button(ActiveWindow p, int ax, int ay, int aw, int ah, DWORD aid = 0) : base(p.GetScreen(), ax, ay, aw, ah, aid, 0, p)
        {
            animated = true;
            bevel_width = 5;
            border = false;
            button_state = 0;
            drop_shadow = false;
            sticky = false;
            picture_loc = 1;
            captured = false;
            pre_state = 0;
            text_align = TextFormat.DT_CENTER;

            standard_image = null;
            activated_image = null;
            transition_image = null;

            desc = "Button " + id;
        }

        private void OnClickEvent()
        {
            Debug.Log("OnClickEvent");
            OnClick();
        }

        //public Button(ActiveWindow* p, int ax, int ay, int aw, int ah, DWORD id = 0);
        //virtual ~Button();

        public static void Initialize()
        {
            string loaderPath = "Sounds/";
            LoadInterfaceSound(loaderPath, "button", ref button_sound);
            LoadInterfaceSound(loaderPath, "click", ref click_sound);
            LoadInterfaceSound(loaderPath, "swish", ref swish_sound);
            LoadInterfaceSound(loaderPath, "chirp", ref chirp_sound);
            LoadInterfaceSound(loaderPath, "accept", ref accept_sound);
            LoadInterfaceSound(loaderPath, "reject", ref reject_sound);
            LoadInterfaceSound(loaderPath, "confirm", ref confirm_sound);
            LoadInterfaceSound(loaderPath, "list_select", ref list_select_sound);
            LoadInterfaceSound(loaderPath, "list_scroll", ref list_scroll_sound);
            LoadInterfaceSound(loaderPath, "list_drop", ref list_drop_sound);
            LoadInterfaceSound(loaderPath, "combo_open", ref combo_open_sound);
            LoadInterfaceSound(loaderPath, "combo_close", ref combo_close_sound);
            LoadInterfaceSound(loaderPath, "combo_hilite", ref combo_hilite_sound);
            LoadInterfaceSound(loaderPath, "combo_select", ref combo_select_sound);
            LoadInterfaceSound(loaderPath, "menu_open", ref menu_open_sound);
            LoadInterfaceSound(loaderPath, "menu_close", ref menu_close_sound);
            LoadInterfaceSound(loaderPath, "menu_select", ref menu_select_sound);
            LoadInterfaceSound(loaderPath, "menu_hilite", ref menu_hilite_sound);
        }
        public static void Close() { throw new NotImplementedException(); }
        public static void PlaySound(SOUNDS n = 0)
        {
            Sound sound = null;

            switch (n)
            {
                default:
                case SOUNDS.SND_BUTTON: if (button_sound != null) sound = button_sound; break;
                case SOUNDS.SND_CLICK: if (click_sound != null) sound = click_sound; break;
                case SOUNDS.SND_SWISH: if (swish_sound != null) sound = swish_sound; break;
                case SOUNDS.SND_CHIRP: if (chirp_sound != null) sound = chirp_sound; break;
                case SOUNDS.SND_ACCEPT: if (accept_sound != null) sound = accept_sound; break;
                case SOUNDS.SND_REJECT: if (reject_sound != null) sound = reject_sound; break;
                case SOUNDS.SND_CONFIRM: if (confirm_sound != null) sound = confirm_sound; break;
                case SOUNDS.SND_LIST_SELECT: if (list_select_sound != null) sound = list_select_sound; break;
                case SOUNDS.SND_LIST_SCROLL: if (list_scroll_sound != null) sound = list_scroll_sound; break;
                case SOUNDS.SND_LIST_DROP: if (list_drop_sound != null) sound = list_drop_sound; break;
                case SOUNDS.SND_COMBO_OPEN: if (combo_open_sound != null) sound = combo_open_sound; break;
                case SOUNDS.SND_COMBO_CLOSE: if (combo_close_sound != null) sound = combo_close_sound; break;
                case SOUNDS.SND_COMBO_HILITE: if (combo_hilite_sound != null) sound = combo_hilite_sound; break;
                case SOUNDS.SND_COMBO_SELECT: if (combo_select_sound != null) sound = combo_select_sound; break;
                case SOUNDS.SND_MENU_OPEN: if (menu_open_sound != null) sound = menu_open_sound; break;
                case SOUNDS.SND_MENU_CLOSE: if (menu_close_sound != null) sound = menu_close_sound; break;
                case SOUNDS.SND_MENU_SELECT: if (menu_select_sound != null) sound = menu_select_sound; break;
                case SOUNDS.SND_MENU_HILITE: if (menu_hilite_sound != null) sound = menu_hilite_sound; break;
            }

            if (sound != null)
            {
                sound.SetVolume(gui_volume);
                sound.Play();
            }
        }
        public static void SetVolume(int vol)
        {
            if (vol >= -10000 && vol <= 0)
                gui_volume = vol;
        }

        // Operations:
        public virtual void Draw() { throw new NotImplementedException(); }   // refresh backing store

        // Event Target Interface:
        public virtual void OnMouseMove(int x, int y) { }
        public virtual void OnLButtonDown(int x, int y) { }
        public virtual void OnLButtonUp(int x, int y) { }
        public override int OnClick()
        {
            PlaySound(SOUNDS.SND_BUTTON);

            if (sticky)
                button_state = (short)(pre_state == 0 ? 1 : 0);

            pre_state = button_state;

            return base.OnClick();
        }
        public override int OnMouseEnter(int x, int y)
        {
            if (button_state >= 0)
                pre_state = button_state;

            if (button_state == 0)
                button_state = -1;

            if (IsEnabled() && IsShown())
                PlaySound(SOUNDS.SND_SWISH);

            return base.OnMouseEnter(x, y);
        }
        public override int OnMouseExit(int x, int y)
        {
            if (button_state == -1)
                button_state = pre_state;

            return base.OnMouseExit(x, y);
        }

        // Property accessors:
        public Color GetActiveColor() { throw new NotImplementedException(); }
        public void SetActiveColor(Color c) { throw new NotImplementedException(); }
        public bool GetAnimated() { throw new NotImplementedException(); }
        public void SetAnimated(bool bNewValue) { throw new NotImplementedException(); }
        public short GetBevelWidth()
        {
            return bevel_width;
        }
        public void SetBevelWidth(short nNewValue)
        {
            if (nNewValue < 0) nNewValue = 0;
            if (nNewValue > rect.w / 2) nNewValue = (short)(rect.w / 2);
            bevel_width = nNewValue;
        }
        public bool GetBorder()
        {
            return border;
        }
        public void SetBorder(bool bNewValue)
        {
            border = bNewValue;
        }
        public Color GetBorderColor()
        {
            return border_color;
        }
        public void SetBorderColor(Color newValue)
        {
            border_color = newValue;
        }
        public short GetButtonState()
        {
            return button_state;
        }
        public void SetButtonState(short n)
        {
            if (button_state != n && n >= -2 && n <= 2)
            {
                button_state = n;
                pre_state = n;
            }
        }

        public bool GetDropShadow()
        {
            return drop_shadow;
        }
        public void SetDropShadow(bool bNewValue)
        {
            drop_shadow = bNewValue;
        }
        public void GetPicture(out Texture2D img)
        {
            img = picture;
        }
        public void SetPicture(Texture2D img)
        {
            picture = img;
            //picture.CopyBitmap(img);
            //picture.AutoMask();
        }
        public short GetPictureLocation()
        {
            return picture_loc;
        }
        public void SetPictureLocation(short n)
        {
            if (picture_loc != n && n >= 0 && n <= 8)
            {
                picture_loc = n;
            }
        }
        public bool GetSticky()
        {
            return sticky;
        }
        public void SetSticky(bool n)
        {
            if (sticky != n)
                sticky = n;
        }

        public void SetStandardImage(Texture2D img)
        {
            standard_image = img;
            texture = standard_image;
            SetStandardImage();
        }
        protected void SetStandardImage()
        {
            if (buttonCmp != null && standard_image != null)
            {
                Insets c = this.GetMargins();
                buttonCmp.image.type = Image.Type.Sliced;
                Vector4 border = new Vector4(c.left, c.bottom, c.right, c.top); //X=left, Y=bottom, Z=right, W=top.
                UnityEngine.Sprite s = UnityEngine.Sprite.Create(standard_image, new UnityEngine.Rect(0, 0, standard_image.width, standard_image.height),
                                                                 new Vector2(.5f, .5f), 100f, 0, SpriteMeshType.FullRect, border);
                s.name = standard_image.name;
                buttonCmp.image.sprite = s;

                var disable_image = new Texture2D(standard_image.width, standard_image.height, TextureFormat.ARGB32, false);
                Color[] pix = standard_image.GetPixels(); // get pixel colors
                for (int i = 0; i < pix.Length; i++)
                    pix[i].a = disableAlpha;    // set the alpha of each pixel
                disable_image.SetPixels(pix);   // set changed pixel alphas
                disable_image.Apply();          // upload texture to GPU
                UnityEngine.Sprite ds = UnityEngine.Sprite.Create(disable_image, new UnityEngine.Rect(0, 0, disable_image.width, disable_image.height),
                                                 new Vector2(.5f, .5f), 100f, 0, SpriteMeshType.FullRect, border);

                SpriteState spriteState = buttonCmp.spriteState;
                spriteState.disabledSprite = ds;
                buttonCmp.spriteState = spriteState;

            }
        }
        public void SetActivatedImage(Texture2D img)
        {
            activated_image = img;
            SetActivatedImage();
        }
        protected void SetActivatedImage()
        {
            if (buttonCmp != null && activated_image != null)
            {
                Insets c = this.GetMargins();
                Vector4 border = new Vector4(c.left, c.bottom, c.right, c.top); //X=left, Y=bottom, Z=right, W=top.
                UnityEngine.Sprite s = UnityEngine.Sprite.Create(activated_image, new UnityEngine.Rect(0, 0, activated_image.width, activated_image.height),
                                                                 new Vector2(.5f, .5f), 100f, 0, SpriteMeshType.FullRect, border);
                s.name = activated_image.name;
                SpriteState spriteState = buttonCmp.spriteState;
                spriteState.highlightedSprite = s;
                buttonCmp.spriteState = spriteState;
            }

        }

        public void SetTransitionImage(Texture2D img)
        {
            transition_image = img;
            SetTransitionImage();
        }
        protected void SetTransitionImage()
        {
            if (buttonCmp != null && transition_image != null)
            {
                Insets c = this.GetMargins();
                Vector4 border = new Vector4(c.left, c.bottom, c.right, c.top); //X=left, Y=bottom, Z=right, W=top.
                UnityEngine.Sprite s = UnityEngine.Sprite.Create(transition_image, new UnityEngine.Rect(0, 0, transition_image.width, transition_image.height),
                                                                 new Vector2(.5f, .5f), 100f, 0, SpriteMeshType.FullRect, border);
                s.name = transition_image.name;
                SpriteState spriteState = buttonCmp.spriteState;
                spriteState.pressedSprite = s;
                buttonCmp.spriteState = spriteState;
            }
        }


        public override void SetText(string t)
        {
            base.SetText(t);
            SetText();
        }
        protected void SetText()
        {
            if (textCmp != null)
                textCmp.text = this.GetText();
        }

        public void SetFont(FontItem f)
        {
            base.SetFont(f);
            SetFont();
        }
        public override void SetFontSize(int s)
        {
            base.SetFontSize(s);
            SetFont();
        }

        protected void SetFont()
        {
            if (textCmp == null) return;
            textCmp.font = this.GetFont().font;
            textCmp.fontSize = this.GetFontSize();
        }
        public override void SetTextAlign(TextFormat a)
        {
            base.SetTextAlign(a);
            SetTextAlign();
        }
        protected void SetTextAlign()
        {
            if (textCmp != null)
                if (text_align.HasFlag(TextFormat.DT_LEFT))
                    textCmp.alignment = TextAnchor.MiddleLeft;
                else if (text_align.HasFlag(TextFormat.DT_RIGHT))
                    textCmp.alignment = TextAnchor.MiddleRight;
                else if (text_align.HasFlag(TextFormat.DT_CENTER))
                    textCmp.alignment = TextAnchor.UpperCenter;
        }
        public override void SetForeColor(Color c)
        {
            base.SetForeColor(c);
            SetForeColor();
        }
        protected void SetForeColor()
        {
            if (textCmp != null)
                textCmp.color = fore_color;
        }

        public override void SetEnabled(bool e = true)
        {
            base.SetEnabled(e);
            SetEnabled();
        }
        protected void SetEnabled()
        {
            if (obj == null) return;
            if (enabled)
            {
                obj.GetComponent<UnityEngine.UI.Button>().interactable = true;
                textCmp.color = fore_color;
            }
            else
            {
                obj.GetComponent<UnityEngine.UI.Button>().interactable = false;
                Color c = fore_color;
                c.a = disableAlpha;
                textCmp.color = c;
            }
        }

        protected Stars.Rect CalcLabelRect(int img_w, int img_h) { throw new NotImplementedException(); }
        protected Stars.Rect CalcPictureRect() { throw new NotImplementedException(); }
        protected void DrawImage(Bitmap bmp, Stars.Rect irect) { throw new NotImplementedException(); }
        protected static void LoadInterfaceSound(string path, string wave, ref Sound s)
        {
            s.AudioClip = Resources.Load<AudioClip>(path + wave);
            if (s != null)
                s.SetFlags(s.GetFlags() | Sound.FlagEnum.INTERFACE);
        }
        protected internal override void BuildGamebjects()
        {
            base.BuildGamebjects();

            DefaultControls.Resources uiResources = new DefaultControls.Resources();
            obj = DefaultControls.CreateButton(uiResources);
            buttonCmp = obj.GetComponent<UnityEngine.UI.Button>();
            textCmp = buttonCmp.GetComponentInChildren<Text>();
            textCmp.GetComponent<RectTransform>().anchoredPosition = new Vector3(6, 0, 0);
            buttonCmp.transition = Selectable.Transition.SpriteSwap;
            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            buttonCmp.onClick.AddListener(OnClickEvent);
            ActiveWindowEventHandler eh = obj.AddComponent<ActiveWindowEventHandler>();
            eh.activewindow = this;
            rectTransform.SetParent(this.parentObj.transform); //Assign the newly created Image GameObject as a Child of the Parent Panel.
            obj.name = desc;

            SetText();
            SetFont();
            SetTextAlign();
            SetForeColor();
            SetStandardImage();
            SetActivatedImage();
            SetTransitionImage();
            SetEnabled();
        }
        protected bool animated;
        protected bool drop_shadow;
        protected bool sticky;
        protected bool border;

        protected Color active_color;
        protected Color border_color;

        protected bool captured;
        protected short pre_state;
        protected short bevel_width;
        protected short button_state;

        protected short picture_loc;
        protected Text textCmp;

        protected static Sound button_sound = new Sound();
        protected static Sound click_sound = new Sound();
        protected static Sound swish_sound = new Sound();
        protected static Sound chirp_sound = new Sound();
        protected static Sound accept_sound = new Sound();
        protected static Sound reject_sound = new Sound();
        protected static Sound confirm_sound = new Sound();
        protected static Sound list_select_sound = new Sound();
        protected static Sound list_scroll_sound = new Sound();
        protected static Sound list_drop_sound = new Sound();
        protected static Sound combo_open_sound = new Sound();
        protected static Sound combo_close_sound = new Sound();
        protected static Sound combo_hilite_sound = new Sound();
        protected static Sound combo_select_sound = new Sound();
        protected static Sound menu_open_sound = new Sound();
        protected static Sound menu_close_sound = new Sound();
        protected static Sound menu_select_sound = new Sound();
        protected static Sound menu_hilite_sound = new Sound();

        protected static int gui_volume = 0;
        protected Texture2D picture;
        protected Texture2D standard_image;   // state = 0
        protected Texture2D activated_image;  // state = 1 (if sticky)
        protected Texture2D transition_image; // state = 2 (if sticky)

        protected UnityEngine.UI.Button buttonCmp;
        protected const float disableAlpha = 1f - 0.35f;
    }
}
