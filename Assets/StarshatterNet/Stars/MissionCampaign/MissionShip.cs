﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Mission.h/Mission.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Simulation Universe and Region classes
*/
using StarshatterNet.Stars.Graphics.General;
using StarshatterNet.Stars.SimElements;
using System.Collections.Generic;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning MissionShip class is still in development and is not recommended for production.
    public class MissionShip
    {
        public MissionShip()
        {
            loc = new Point(-1e9, -1e9, -1e9); respawns = 0; heading = 0; integrity = 100;
            decoys = -10; probes = -10; skin = null;
            for (int i = 0; i < 16; i++)
                ammo[i] = -10;

            for (int i = 0; i < 4; i++)
                fuel[i] = -10;
        }
        // ~MissionShip() { }

        public string Name() { return name; }
        public string RegNum() { return regnum; }
        public string Region() { return region; }
        public Skin GetSkin() { return skin; }
        public Point Location() { return loc; }
        public Point Velocity() { return velocity; }
        public int Respawns() { return respawns; }
        public double Heading() { return heading; }
        public double Integrity() { return integrity; }
        public int Decoys() { return decoys; }
        public int Probes() { return probes; }
        public int[] Ammo() { return ammo; }
        public int[] Fuel() { return fuel; }

        public void SetName(string n) { name = n; }
        public void SetRegNum(string n) { regnum = n; }
        public void SetRegion(string n) { region = n; }
        public void SetSkin(Skin s) { skin = s; }
        public void SetLocation(Point p) { loc = p; }
        public void SetVelocity(Point p) { velocity = p; }
        public void SetRespawns(int r) { respawns = r; }
        public void SetHeading(double h) { heading = h; }
        public void SetIntegrity(double n) { integrity = n; }
        public void SetDecoys(int d) { decoys = d; }
        public void SetProbes(int p) { probes = p; }
        public void SetAmmo(int[] a)
        {
            if (a != null)
            {
                for (int i = 0; i < 16; i++)
                    ammo[i] = a[i];
            }
        }
        public void SetFuel(int[] f)
        {
            if (f != null)
            {
                for (int i = 0; i < 4; i++)
                    fuel[i] = f[i];
            }
        }


        protected internal string name;
        protected internal string regnum;
        protected internal string region;
        protected internal Skin skin;
        protected internal Point loc;
        protected internal Point velocity;
        protected internal int respawns;
        protected internal double heading;
        protected internal double integrity;
        protected internal int decoys;
        protected internal int probes;
        protected internal int[] ammo = new int[16];
        protected internal int[] fuel = new int[4];

    }
}