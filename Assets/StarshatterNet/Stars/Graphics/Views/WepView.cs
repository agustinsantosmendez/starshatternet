﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      WepView.h/WepView.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    View class for Tactical HUD Overlay
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Audio;
using StarshatterNet.Controllers;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Views;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using UnityEngine;

namespace StarshatterNet.Stars.Views
{
    public class WepView : View, ISimObserver
    {
        public WepView(Window c, string name = "WepView") : base(c, name)
        {
            sim = null; ship = null; target = null; active_region = null;
            transition = false; mode = 0; mouse_down = false;

            wep_view = this;

            sim = Sim.GetSim();

            HUDView.PrepareBitmap("TAC_left", out tac_left);
            HUDView.PrepareBitmap("TAC_right", out tac_right);
            HUDView.PrepareBitmap("TAC_button", out tac_button);
            HUDView.PrepareBitmap("MAN", out tac_man);
            HUDView.PrepareBitmap("AUTO", out tac_aut);
            HUDView.PrepareBitmap("DEF", out tac_def);

            tac_left.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            tac_right.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            tac_man.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            tac_aut.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);
            tac_def.SetType(Bitmap.BMP_TYPES.BMP_TRANSLUCENT);

            tac_left_sprite = new SpriteGraphic(tac_left, window.Canvas);
            tac_right_sprite = new SpriteGraphic(tac_right, window.Canvas);
            tac_button_sprite = new SpriteGraphic(tac_button, window.Canvas);
            tac_man_sprite = new SpriteGraphic(tac_man, window.Canvas);
            tac_aut_sprite = new SpriteGraphic(tac_aut, window.Canvas);
            tac_def_sprite = new SpriteGraphic(tac_def, window.Canvas);

            OnWindowMove();

            hud_font = FontMgr.Find("HUD");
            big_font = FontMgr.Find("GUI");

            hud = HUDView.GetInstance();
            if (hud != null)
                SetColor(hud.GetHUDColor());
        }
        //public virtual ~WepView();

        // Operations:
        public override void Refresh()
        {
            sim = Sim.GetSim();
            if (sim == null || hud == null || hud.GetHUDMode() == HUDView.HUDModes.HUD_MODE_OFF)
                return;

            if (ship != sim.GetPlayerShip())
            {
                ship = sim.GetPlayerShip();

                if (ship != null)
                {
                    if (ship.Life() == 0 || ship.IsDying() || ship.IsDead())
                    {
                        ship = null;
                    }
                    else
                    {
                        Observe(ship);
                    }
                }
            }

            if (mode < 1)
                return;

            if (ship != null)
            {
                // no tactical overlay for fighters:
                if (ship.Design() != null && !ship.Design().wep_screen)
                {
                    mode = 0;
                    return;
                }

                // no hud in transition:
                if (ship.InTransition())
                {
                    transition = true;
                    return;
                }

                else if (transition)
                {
                    transition = false;
                    RestoreOverlay();
                }

                if (target != ship.GetTarget())
                {
                    target = ship.GetTarget();
                    if (target != null) Observe(target);
                }

                DrawOverlay();
            }
            else
            {
                if (target != null)
                {
                    target = null;
                }
            }
        }

        public override void OnWindowMove()
        {
            width = window.Width();
            height = window.Height();
            xcenter = (width / 2.0) - 0.5;
            ycenter = (height / 2.0) + 0.5;

            int btn_loc = width / 2 - 147 - 45;
            int man_loc = width / 2 - 177 - 16;
            int aut_loc = width / 2 - 145 - 16;
            int def_loc = width / 2 - 115 - 16;

            int index = 0;

            for (int i = 0; i < MAX_WEP; i++)
            {
                btn_rect[index++] = new Rect(btn_loc, 30, 90, 20);
                btn_rect[index++] = new Rect(man_loc, 56, 32, 8);
                btn_rect[index++] = new Rect(aut_loc, 56, 32, 8);
                btn_rect[index++] = new Rect(def_loc, 56, 32, 8);

                btn_loc += 98;
                man_loc += 98;
                aut_loc += 98;
                def_loc += 98;
            }
        }
        public virtual void ExecFrame()
        {
            int hud_mode = 1;

            // update the position of HUD elements that are
            // part of the 3D scene (like fpm and lcos sprites)

            if (hud != null)
            {
                if (hud_color != hud.GetHUDColor())
                {
                    hud_color = hud.GetHUDColor();
                    SetColor(hud_color);
                }

                if (hud.GetHUDMode() == HUDView.HUDModes.HUD_MODE_OFF)
                    hud_mode = 0;
            }

            if (ship != null && !transition && mode > 0 && hud_mode > 0)
            {
                if (mode > 0)
                {
                    DoMouseFrame();
                }
            }
        }
        public virtual void SetOverlayMode(int m)
        {
            if (mode != m)
            {
                mode = m;

                if (hud != null)
                    hud.SetOverlayMode(mode);

                RestoreOverlay();
            }
        }
        public virtual int GetOverlayMode() { return mode; }
        public virtual void CycleOverlayMode()
        {
            SetOverlayMode(mode == 0 ? 1 : 0);
        }


        public virtual void RestoreOverlay()
        {
            if (mode > 0)
            {
                HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_WEP_DISP);
            }

            else
            {
                HUDSounds.PlaySound(HUDSounds.SOUNDS.SND_WEP_MODE);
            }
        }

        public virtual bool Update(SimObject obj)
        {
            if (obj == ship)
            {
                ship = null;
                target = null;
            }
            else if (obj == target)
            {
                target = null;
            }

            return simObserver.Update(obj);
        }

        public virtual string GetObserverName()
        {
            return "WepView";
        }


        public static WepView GetInstance() { return wep_view; }
        public static void SetColor(Color c)
        {
            HUDView hud = HUDView.GetInstance();

            if (hud != null)
            {
                hud_color = hud.GetHUDColor();
                txt_color = hud.GetTextColor();
            }
            else
            {
                hud_color = c;
                txt_color = c;
            }

            HUDView.ColorizeBitmap(tac_left_sprite, hud_color);
            HUDView.ColorizeBitmap(tac_right_sprite, hud_color);
            HUDView.ColorizeBitmap(tac_button_sprite, hud_color);
            HUDView.ColorizeBitmap(tac_man_sprite, hud_color);
            HUDView.ColorizeBitmap(tac_aut_sprite, hud_color);
            HUDView.ColorizeBitmap(tac_def_sprite, hud_color);
        }

        public static bool IsMouseLatched()
        {
            return mouse_in;
        }


        protected void DrawOverlay()
        {
            int cx = width / 2;
            int cy = 0;
            int w = tac_left.Width();
            int h = tac_left.Height();

            window.DrawBitmap(cx - w, cy, cx, cy + h, tac_left, Video.BLEND_TYPE.BLEND_ALPHA);
            window.DrawBitmap(cx, cy, cx + w, cy + h, tac_right, Video.BLEND_TYPE.BLEND_ALPHA);

            if (ship != null)
            {
                List<WeaponGroup> weapons = ship.Weapons();
                for (int i = 0; i < MAX_WEP; i++)
                {
                    if (weapons.Count > i)
                    {
                        // draw main fire button:
                        Rect r = btn_rect[i * 4];

                        w = tac_button.Width();
                        h = tac_button.Height();
                        cx = r.x + r.w / 2 - w / 2;
                        cy = r.y + r.h / 2 - h / 2;

                        r.Deflate(5, 5);

                        big_font.SetColor(txt_color);
                        window.SetFont(big_font);
                        window.DrawBitmap(cx, cy, cx + w, cy + h, tac_button, Video.BLEND_TYPE.BLEND_ALPHA);
                        window.DrawText(weapons[i].Name(), 0, ref r, TextFormat.DT_SINGLELINE | TextFormat.DT_CENTER);

                        r.Inflate(5, 5);

                        // draw firing orders:
                        Weapon.Orders o = weapons[i].GetFiringOrders();
                        w = tac_man.Width();
                        h = tac_man.Height();

                        Color c0 = Color.gray;
                        Color c1 = Color.white;

                        r = btn_rect[i * 4 + 1];
                        cx = r.x + r.w / 2 - w / 2;
                        cy = r.y + r.h / 2 - h / 2;
                        window.FadeBitmap(cx, cy, cx + w, cy + h, tac_man, (o == (Weapon.Orders)0 ? c1 : c0), Video.BLEND_TYPE.BLEND_ALPHA);

                        r = btn_rect[i * 4 + 2];
                        cx = r.x + r.w / 2 - w / 2;
                        cy = r.y + r.h / 2 - h / 2;
                        window.FadeBitmap(cx, cy, cx + w, cy + h, tac_aut, (o == (Weapon.Orders)1 ? c1 : c0), Video.BLEND_TYPE.BLEND_ALPHA);

                        r = btn_rect[i * 4 + 3];
                        cx = r.x + r.w / 2 - w / 2;
                        cy = r.y + r.h / 2 - h / 2;
                        window.FadeBitmap(cx, cy, cx + w, cy + h, tac_def, (o == (Weapon.Orders)2 ? c1 : c0), Video.BLEND_TYPE.BLEND_ALPHA);
                    }
                }

                Rect tgt_rect = new Rect();
                tgt_rect.x = width / 2 + 73;
                tgt_rect.y = 74;
                tgt_rect.w = 100;
                tgt_rect.h = 15;

                string subtxt;
                Color stat = hud_color;
                ulong blink = Game.RealTime();

                if (ship.GetTarget() != null)
                {
                    if (ship.GetSubTarget() != null)
                    {
                        ulong blink_delta = Game.RealTime() - blink;

                        ShipSystem sys = ship.GetSubTarget();
                        subtxt = sys.Abbreviation();
                        switch (sys.Status())
                        {
                            case ShipSystem.STATUS.DEGRADED: stat = new Color32(255, 255, 0, 255); break;
                            case ShipSystem.STATUS.CRITICAL:
                            case ShipSystem.STATUS.DESTROYED: stat = new Color32(255, 0, 0, 255); break;
                            case ShipSystem.STATUS.MAINT:
                                if (blink_delta < 250)
                                    stat = new Color32(8, 8, 8, 255);
                                break;
                        }

                        if (blink_delta > 500)
                            blink = Game.RealTime();
                    }

                    else
                        subtxt = ship.GetTarget().Name();
                }
                else
                {
                    subtxt = "NO TGT";
                }

                subtxt.ToUpper();

                hud_font.SetColor(stat);
                window.SetFont(hud_font);
                window.DrawText(subtxt, subtxt.Length, ref tgt_rect, TextFormat.DT_SINGLELINE | TextFormat.DT_CENTER);
            }
        }

        static bool mouse_down = false;
        static int mouse_down_x = 0;
        static int mouse_down_y = 0;

        protected void DoMouseFrame()
        {

            int x = (int)Mouse.X();
            int y = (int)Mouse.Y();

            // coarse-grained test: is mouse in overlay at all?
            if (x < width / 2 - 256 || x > width / 2 + 256 || y > 90)
            {
                mouse_in = false;
                return;
            }

            mouse_in = true;

            if (Mouse.LButton() != 0)
            {
                if (!mouse_down)
                {
                    mouse_down = true;
                    mouse_down_x = x;
                    mouse_down_y = y;
                }

                // check weapons buttons:
                int max_wep = ship.Weapons().Count;

                if (max_wep > MAX_WEP)
                    max_wep = MAX_WEP;

                for (int i = 0; i < max_wep; i++)
                {
                    int index = i * 4;

                    if (CheckButton(index, mouse_down_x, mouse_down_y))
                    {
                        ship.FireWeapon(i);
                        return;
                    }

                    else if (CheckButton(index + 1, mouse_down_x, mouse_down_y))
                    {
                        ship.Weapons()[i].SetFiringOrders(Weapon.Orders.MANUAL);
                        return;
                    }

                    else if (CheckButton(index + 2, mouse_down_x, mouse_down_y))
                    {
                        ship.Weapons()[i].SetFiringOrders(Weapon.Orders.AUTO);
                        return;
                    }

                    else if (CheckButton(index + 3, mouse_down_x, mouse_down_y))
                    {
                        ship.Weapons()[i].SetFiringOrders(Weapon.Orders.POINT_DEFENSE);
                        return;
                    }
                }
            }

            else if (mouse_down)
            {
                mouse_down = false;
                mouse_down_x = 0;
                mouse_down_y = 0;

                // check subtarget buttons:
                if (ship.GetTarget() != null)
                {
                    Rect r = new Rect(width / 2 + 50, 70, 20, 20);
                    if (r.Contains(x, y))
                    {
                        CycleSubTarget(-1);
                        return;
                    }

                    r.x = width / 2 + 180;
                    if (r.Contains(x, y))
                    {
                        CycleSubTarget(1);
                        return;
                    }
                }
            }
        }
        protected bool CheckButton(int index, int x, int y)
        {
            if (index >= 0 && index < MAX_BTN)
            {
                return btn_rect[index].Contains(x, y) ? true : false;
            }

            return false;
        }

        protected void CycleSubTarget(int direction)
        {
            if (ship.GetTarget() == null || ship.GetTarget().Type() != (int)SimObject.TYPES.SIM_SHIP)
                return;

            ship.CycleSubTarget(direction);
        }

        public void Observe(SimObject obj)
        {
            simObserver.Observe(obj);
        }

        public void Ignore(SimObject obj)
        {
            simObserver.Ignore(obj);
        }

        protected ISimObserver simObserver = new SimObserver();
        protected int mode;
        protected bool transition;
        protected int width, height, aw, ah;
        protected double xcenter, ycenter;

        protected Sim sim;
        protected Ship ship;
        protected SimObject target;
        protected HUDView hud;

        protected const int MAX_WEP = 4, MAX_BTN = 16;
        protected Rect[] btn_rect = new Rect[MAX_BTN];

        protected SimRegion active_region;

        protected static WepView wep_view;

        protected static Bitmap tac_left;
        protected static Bitmap tac_right;
        protected static Bitmap tac_button;
        protected static Bitmap tac_man;
        protected static Bitmap tac_aut;
        protected static Bitmap tac_def;

        protected static SpriteGraphic tac_left_sprite;
        protected static SpriteGraphic tac_right_sprite;
        protected static SpriteGraphic tac_button_sprite;
        protected static SpriteGraphic tac_man_sprite;
        protected static SpriteGraphic tac_aut_sprite;
        protected static SpriteGraphic tac_def_sprite;

        protected static Color hud_color = Color.black;
        protected static Color txt_color = Color.black;

        protected static bool mouse_in = false;

        protected static FontItem hud_font = null;
        protected static FontItem big_font = null;
    }
}