﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          nGenEx
    ORIGINAL FILE:      FormWindow.h/FormWindow.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Form Window class (a window that manages controls)
*/
using System.Collections.Generic;
using StarshatterNet.Audio;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Graphics;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.Int32;
using Screen = StarshatterNet.Gen.Graphics.Screen;

namespace StarshatterNet.Gen.Forms
{
    public class FormWindow : ActiveWindow
    {
        public FormWindow(Screen screen, int ax, int ay, int aw, int ah, Camera camera, GameObject pParent, string name,
                            DWORD aid = 0, DWORD s = 0) : base(screen, ax, ay, aw, ah, aid, s, null)
        {
            if (!string.IsNullOrEmpty(name))
                desc = "Form " + name + " " + id;
            else
                desc = "Form " + id;
            this.camera = camera;
            this.parentObj = pParent;
        }

        // public virtual ~FormWindow();

        // operations:
        public virtual void Init()
        {
        }
        public virtual void Init(FormDef def)
        {
            if (def == null) return;

            if (def.GetRect().w > 0 && def.GetRect().h > 0)
            {
                // if form size is specified in def, and it is 
                // smaller than the current screen size,
                // center the form on the display:

                Stars.Rect r = def.GetRect();
                if (r.w < screen.Height())
                {
                    r.x = (screen.Height() - r.w) / 2;
                }
                else
                {
                    r.x = 0;
                    r.w = screen.Height();
                }

                if (r.h < screen.Height())
                {
                    r.y = (screen.Height() - r.h) / 2;
                }
                else
                {
                    r.y = 0;
                    r.h = screen.Height();
                }

                MoveTo(r);
            }

            SetMargins(def.GetMargins());
            SetTextInsets(def.GetTextInsets());
            SetCellInsets(def.GetCellInsets());
            SetCells(def.GetCells());
            SetFixedWidth(def.GetFixedWidth());
            SetFixedHeight(def.GetFixedHeight());
            UseLayout(def.GetLayout().x_mins, def.GetLayout().y_mins, def.GetLayout().x_weights, def.GetLayout().y_weights);

            if (!string.IsNullOrEmpty(def.GetTexture()))
            {
                var path = "Screens/";
                texture = Resources.Load<Texture2D>(path + def.GetTexture());
            }

            SetBackColor(def.GetBackColor());
            SetForeColor(def.GetForeColor());

            FontItem f = FontMgr.Find(def.GetFont());
            if (f == null) f = FontMgr.GetDefault();
            if (f != null)
            {
                SetFont(f);
                SetFontSize(f.size);
            }
            SetTransparent(def.GetTransparent());

            foreach (CtrlDef ctrl in def.GetControls())
            {
                switch (ctrl.GetWinType())
                {
                    case WinType.WIN_DEF_FORM:
                    case WinType.WIN_DEF_LABEL:
                    default:
                        CreateDefLabel(ctrl);
                        break;

                    case WinType.WIN_DEF_BUTTON:
                        CreateDefButton(ctrl);
                        break;

                    case WinType.WIN_DEF_COMBO:
                        CreateDefCombo(ctrl);
                        break;

                    case WinType.WIN_DEF_BACKGROUND:
                    case WinType.WIN_DEF_IMAGE:
                        CreateDefImage(ctrl);
                        break;

                    case WinType.WIN_DEF_EDIT:
                        CreateDefEdit(ctrl);
                        break;

                    case WinType.WIN_DEF_LIST:
                        CreateDefList(ctrl);
                        break;

                    case WinType.WIN_DEF_SLIDER:
                        CreateDefSlider(ctrl);
                        break;

                    case WinType.WIN_DEF_RICH:
                        CreateDefRichText(ctrl);
                        break;
                }
            }

            RegisterControls();
            BuildGamebjects();
            DoLayout();
            CalcGrid();
        }
        public virtual void Destroy()
        {
            Hide();
            children.Destroy();
        }
        public virtual ActiveWindow FindControl(DWORD id)
        {
            return FindChild(id);
        }
        public override ActiveWindow FindControl(int x, int y)
        {
            ActiveWindow mouse_tgt = null;

            foreach (ActiveWindow iter in children)
            {
                ActiveWindow test = iter;
                if (test.TargetRect().Contains(x, y))
                {
                    mouse_tgt = test;

                    while (test != null)
                    {
                        test = test.FindChild(x, y);

                        if (test != null)
                            mouse_tgt = test;
                    }
                }
            }

            return mouse_tgt;
        }
        public virtual void RegisterControls() { }

        public virtual void AdoptFormDef(FormDef def)
        {
            Destroy();
            Init(def);
        }
        public virtual void AddControl(ActiveWindow ctrl)
        {
            if (ctrl != null)
            {
                if (!children.Contains(ctrl))
                    children.Add(ctrl);

                ctrl.SetForm(this);

                if (!shown)
                    ctrl.Hide();
            }
        }
        public virtual ActiveWindow CreateLabel(string label_text, int ax, int ay, int aw, int ah, DWORD aid = 0, DWORD pid = 0, DWORD astyle = 0)
        {
            TextLabelBox label = null;
            ActiveWindow parent = this;

            if (pid != 0)
                parent = FindControl(pid);

            label = new TextLabelBox(parent, ax, ay, aw, ah, aid);

            if (label != null)
            {
                label.SetForm(this);
                label.SetText(label_text);

                if (!shown)
                    label.Hide();
            }

            return label;
        }
        public virtual Button CreateButton(string lbl_text, int ax, int ay, int aw, int ah, DWORD aid = 0, DWORD pid = 0)
        {
            Button button = null;
            ActiveWindow parent = this;

            if (pid != 0)
                parent = FindControl(pid);

            button = new Button(parent, ax, ay, aw, ah, aid);

            if (button != null)
            {
                button.SetForm(this);
                button.SetText(lbl_text);

                if (!shown)
                    button.Hide();
            }

            return button;
        }

        public virtual ImageBox CreateImageBox(string lbl_text, int ax, int ay, int aw, int ah, DWORD aid = 0, DWORD pid = 0)
        {
            ImageBox image = null;
            ActiveWindow parent = this;

            if (pid != 0)
                parent = FindControl(pid);

            image = new ImageBox(parent, ax, ay, aw, ah, aid);

            if (image != null)
            {
                image.SetForm(this);
                image.SetText(lbl_text);

                if (!shown)
                    image.Hide();
            }

            return image;
        }

        public virtual ListBox CreateListBox(string text, int ax, int ay, int aw, int ah, DWORD aid = 0, DWORD pid = 0)
        {
            ListBox list = null;
            ActiveWindow parent = this;

            if (pid != 0)
                parent = FindControl(pid);

            list = new ListBox(parent, ax, ay, aw, ah, aid);

            if (list != null)
            {
                list.SetForm(this);

                if (!shown)
                    list.Hide();
            }

            return list;
        }
        public virtual ComboBox CreateComboBox(string lbl_text, int ax, int ay, int aw, int ah, DWORD aid = 0, DWORD pid = 0)
        {
            ComboBox combo = null;
            ActiveWindow parent = this;

            if (pid != 0)
                parent = FindControl(pid);

            combo = new ComboBox(parent, ax, ay, aw, ah, aid);

            if (combo != null)
            {
                combo.SetForm(this);
                combo.SetLabel(lbl_text);

                if (!shown)
                    combo.Hide();
            }

            return combo;
        }
        public virtual EditBox CreateEditBox(string lbl_text, int ax, int ay, int aw, int ah, DWORD aid = 0, DWORD pid = 0)
        {
            EditBox edit = null;
            ActiveWindow parent = this;

            if (pid != 0)
                parent = FindControl(pid);

            edit = new EditBox(parent, ax, ay, aw, ah, aid);

            if (edit != null)
            {
                edit.SetForm(this);
                edit.SetText(lbl_text);

                if (!shown)
                    edit.Hide();
            }

            return edit;
        }

        public virtual RichTextBox CreateRichTextBox(string label_text, int ax, int ay, int aw, int ah, DWORD aid = 0, DWORD pid = 0, DWORD astyle = 0)
        {
            RichTextBox rtb = null;
            ActiveWindow parent = this;

            if (pid != 0)
                parent = FindControl(pid);

            rtb = new RichTextBox(parent, ax, ay, aw, ah, aid, astyle);

            if (rtb != null)
            {
                rtb.SetForm(this);
                rtb.SetText(label_text);

                if (!shown)
                    rtb.Hide();
            }

            return rtb;
        }
        public virtual Slider CreateSlider(string text, int ax, int ay, int aw, int ah, DWORD aid = 0, DWORD pid = 0, DWORD style = 0)
        {
            Slider slider = null;
            ActiveWindow parent = this;

            if (pid != 0)
                parent = FindControl(pid);

            slider = new Slider(parent, ax, ay, aw, ah, aid);

            if (slider != null)
            {
                slider.SetForm(this);

                if (!shown)
                    slider.Hide();
            }

            return slider;
        }

        // property accessors:
        public List<ActiveWindow> Controls() { return children; }

        protected virtual void CreateDefLabel(CtrlDef def)
        {
            ActiveWindow ctrl = CreateLabel(def.GetText(),
                                            def.GetX(),
                                            def.GetY(),
                                            def.GetW(),
                                            def.GetH(),
                                            def.GetID(),
                                            def.GetParentID(),
                                            def.GetStyle());

            ctrl.SetAltText(def.GetAltText());
            ctrl.SetBackColor(def.GetBackColor());
            ctrl.SetForeColor(def.GetForeColor());
            ctrl.SetTextAlign(def.GetTextAlign());
            ctrl.SetSingleLine(def.GetSingleLine());
            ctrl.SetTransparent(def.GetTransparent());
            ctrl.SetHidePartial(def.GetHidePartial());

            ctrl.SetMargins(def.GetMargins());
            ctrl.SetTextInsets(def.GetTextInsets());
            ctrl.SetCellInsets(def.GetCellInsets());
            ctrl.SetCells(def.GetCells());
            ctrl.SetFixedWidth(def.GetFixedWidth());
            ctrl.SetFixedHeight(def.GetFixedHeight());

            ctrl.UseLayout(def.GetLayout().x_mins,
                            def.GetLayout().y_mins,
                            def.GetLayout().x_weights,
                            def.GetLayout().y_weights);

            if (!string.IsNullOrEmpty(def.GetTexture()))
            {
                Texture2D ctrl_tex = null;
                var path = "Screens/";
                ctrl_tex = Resources.Load<Texture2D>(path + def.GetTexture());

                ctrl.SetTexture(ctrl_tex);
            }

            FontItem f = FontMgr.Find(def.GetFont());
            if (f == null) f = FontMgr.GetDefault();
            if (f != null)
            {
                ctrl.SetFont(f);
                ctrl.SetFontSize(f.size);
            }
        }
        protected virtual void CreateDefButton(CtrlDef def)
        {
            Button ctrl = CreateButton(def.GetText(),
                                def.GetX(),
                                def.GetY(),
                                def.GetW(),
                                def.GetH(),
                                def.GetID(),
                                def.GetParentID());

            var path = "Screens/";
            Texture2D ctrl_tex = null;

            ctrl.SetAltText(def.GetAltText());
            ctrl.SetEnabled(def.IsEnabled());
            ctrl.SetBackColor(def.GetBackColor());
            ctrl.SetForeColor(def.GetForeColor());
            ctrl.SetTextAlign(def.GetTextAlign());
            ctrl.SetSingleLine(def.GetSingleLine());

            ctrl.SetBevelWidth(def.GetBevelWidth());
            ctrl.SetDropShadow(def.GetDropShadow());
            ctrl.SetSticky(def.GetSticky());
            ctrl.SetTransparent(def.GetTransparent());
            ctrl.SetHidePartial(def.GetHidePartial());
            ctrl.SetPictureLocation(def.GetPictureLocation());

            ctrl.SetMargins(def.GetMargins());
            ctrl.SetTextInsets(def.GetTextInsets());
            ctrl.SetCellInsets(def.GetCellInsets());
            ctrl.SetCells(def.GetCells());
            ctrl.SetFixedWidth(def.GetFixedWidth());
            ctrl.SetFixedHeight(def.GetFixedHeight());


            if (!string.IsNullOrEmpty(def.GetPicture()))
            {
                ctrl_tex = Resources.Load<Texture2D>(path + def.GetPicture());
                ctrl.SetPicture(ctrl_tex);
            }

            if (!string.IsNullOrEmpty(def.GetStandardImage()))
            {
                ctrl_tex = Resources.Load<Texture2D>(path + def.GetStandardImage());
                ctrl.SetStandardImage(ctrl_tex);

                if (!string.IsNullOrEmpty(def.GetActivatedImage()))
                {
                    ctrl_tex = Resources.Load<Texture2D>(path + def.GetActivatedImage());
                    ctrl.SetActivatedImage(ctrl_tex);
                }

                if (!string.IsNullOrEmpty(def.GetTransitionImage()))
                {
                    ctrl_tex = Resources.Load<Texture2D>(path + def.GetTransitionImage());
                    ctrl.SetTransitionImage(ctrl_tex);
                }
            }

            FontItem f = FontMgr.Find(def.GetFont());
            if (f == null) f = FontMgr.GetDefault();
            if (f != null)
            {
                ctrl.SetFont(f);
                ctrl.SetFontSize(f.size);
            }
        }
        protected virtual void CreateDefImage(CtrlDef def)
        {
            ImageBox ctrl = CreateImageBox(def.GetText(),
                                            def.GetX(),
                                            def.GetY(),
                                            def.GetW(),
                                            def.GetH(),
                                            def.GetID(),
                                            def.GetParentID());

            ctrl.SetAltText(def.GetAltText());
            ctrl.SetBackColor(def.GetBackColor());
            ctrl.SetForeColor(def.GetForeColor());
            ctrl.SetStyle(def.GetStyle());
            ctrl.SetTextAlign(def.GetTextAlign());
            ctrl.SetSingleLine(def.GetSingleLine());
            ctrl.SetTransparent(def.GetTransparent());
            ctrl.SetHidePartial(def.GetHidePartial());

            ctrl.SetMargins(def.GetMargins());
            ctrl.SetTextInsets(def.GetTextInsets());
            ctrl.SetCellInsets(def.GetCellInsets());
            ctrl.SetCells(def.GetCells());
            ctrl.SetFixedWidth(def.GetFixedWidth());
            ctrl.SetFixedHeight(def.GetFixedHeight());

            if (def.GetLayout() != null && !def.GetLayout().IsEmpty())
                ctrl.UseLayout(def.GetLayout().x_mins,
                    def.GetLayout().y_mins,
                    def.GetLayout().x_weights,
                    def.GetLayout().y_weights);

            Texture2D ctrl_tex = null;
            var path = "Screens/";
            if (!string.IsNullOrEmpty(def.GetPicture()))
            {
                ctrl_tex = Resources.Load<Texture2D>(path + def.GetPicture());
                ctrl.SetPicture(ctrl_tex);
            }
            else if (!string.IsNullOrEmpty(def.GetTexture()))
            {
                ctrl_tex = Resources.Load<Texture2D>(path + def.GetTexture());
                ctrl.SetPicture(ctrl_tex);
            }
            FontItem f = FontMgr.Find(def.GetFont());
            if (f == null) f = FontMgr.GetDefault();
            if (f != null)
            {
                ctrl.SetFont(f);
                ctrl.SetFontSize(f.size);
            }
        }
        protected virtual void CreateDefList(CtrlDef def)
        {
            ListBox ctrl = CreateListBox(def.GetText(),
                                        def.GetX(),
                                        def.GetY(),
                                        def.GetW(),
                                        def.GetH(),
                                        def.GetID(),
                                        def.GetParentID());

            ctrl.SetAltText(def.GetAltText());
            ctrl.SetEnabled(def.IsEnabled());
            ctrl.SetBackColor(def.GetBackColor());
            ctrl.SetForeColor(def.GetForeColor());
            ctrl.SetStyle(def.GetStyle());
            ctrl.SetTextAlign(def.GetTextAlign());
            ctrl.SetTransparent(def.GetTransparent());
            ctrl.SetHidePartial(def.GetHidePartial());

            ctrl.SetLineHeight(def.GetLineHeight());
            ctrl.SetShowHeadings(def.GetShowHeadings());
            ctrl.SetLeading(def.GetLeading());
            ctrl.SetMultiSelect(def.GetMultiSelect());
            ctrl.SetDragDrop(def.GetDragDrop());
            ctrl.SetScrollBarVisible((ScrollWindow.POLICY)def.GetScrollBarVisible());
            ctrl.SetSmoothScroll(def.GetSmoothScroll());
            ctrl.SetItemStyle((ListBox.STYLE)def.GetItemStyle());
            ctrl.SetSelectedStyle((ListBox.STYLE)def.GetSelectedStyle());

            ctrl.SetMargins(def.GetMargins());
            ctrl.SetTextInsets(def.GetTextInsets());
            ctrl.SetCellInsets(def.GetCellInsets());
            ctrl.SetCells(def.GetCells());
            ctrl.SetFixedWidth(def.GetFixedWidth());
            ctrl.SetFixedHeight(def.GetFixedHeight());

            if (!string.IsNullOrEmpty(def.GetTexture()))
            {
                Texture2D ctrl_tex = null;
                var path = "Screens/";
                ctrl_tex = Resources.Load<Texture2D>(path + def.GetTexture());
                ctrl.SetTexture(ctrl_tex);
            }

            int ncols = def.NumColumns();
            for (int i = 0; i < ncols; i++)
            {
                ColumnDef col = def.GetColumn(i);
                ctrl.AddColumn(col.title, col.width, (ListBox.ALIGN)col.align, (ListBox.SORT)col.sort);

                if (col.use_color)
                    ctrl.SetColumnColor(i, col.color);
            }

            int nitems = def.NumItems();
            for (int i = 0; i < nitems; i++)
                ctrl.AddItem(def.GetItem(i));

            FontItem f = FontMgr.Find(def.GetFont());
            if (f == null) f = FontMgr.GetDefault();
            if (f != null)
            {
                ctrl.SetFont(f);
                ctrl.SetFontSize(f.size);
            }
        }
        protected virtual void CreateDefCombo(CtrlDef def)
        {
            ComboBox ctrl = CreateComboBox(def.GetText(),
                                            def.GetX(),
                                            def.GetY(),
                                            def.GetW(),
                                            def.GetH(),
                                            def.GetID(),
                                            def.GetParentID());

            ctrl.SetAltText(def.GetAltText());
            ctrl.SetEnabled(def.IsEnabled());
            ctrl.SetBackColor(def.GetBackColor());
            ctrl.SetForeColor(def.GetForeColor());
            ctrl.SetTextAlign(def.GetTextAlign());

            ctrl.SetActiveColor(def.GetActiveColor());
            ctrl.SetBorderColor(def.GetBorderColor());
            ctrl.SetBorder(def.GetBorder());
            ctrl.SetBevelWidth(def.GetBevelWidth());
            ctrl.SetTransparent(def.GetTransparent());
            ctrl.SetHidePartial(def.GetHidePartial());

            ctrl.SetMargins(def.GetMargins());
            ctrl.SetTextInsets(def.GetTextInsets());
            ctrl.SetCellInsets(def.GetCellInsets());
            ctrl.SetCells(def.GetCells());
            ctrl.SetFixedWidth(def.GetFixedWidth());
            ctrl.SetFixedHeight(def.GetFixedHeight());

            int nitems = def.NumItems();
            for (int i = 0; i < nitems; i++)
                ctrl.AddItem(def.GetItem(i));

            FontItem f = FontMgr.Find(def.GetFont());
            if (f == null) f = FontMgr.GetDefault();
            if (f != null)
            {
                ctrl.SetFont(f);
                ctrl.SetFontSize(f.size);
            }
        }
        protected virtual void CreateDefEdit(CtrlDef def)
        {
            EditBox ctrl = CreateEditBox(def.GetText(),
                                        def.GetX(),
                                        def.GetY(),
                                        def.GetW(),
                                        def.GetH(),
                                        def.GetID(),
                                        def.GetParentID());

            ctrl.SetAltText(def.GetAltText());
            ctrl.SetEnabled(def.IsEnabled());
            ctrl.SetBackColor(def.GetBackColor());
            ctrl.SetForeColor(def.GetForeColor());
            ctrl.SetStyle(def.GetStyle());
            ctrl.SetSingleLine(def.GetSingleLine());
            ctrl.SetTextAlign(def.GetTextAlign());
            ctrl.SetTransparent(def.GetTransparent());
            ctrl.SetHidePartial(def.GetHidePartial());
            ctrl.SetPasswordChar(def.GetPasswordChar());

            ctrl.SetMargins(def.GetMargins());
            ctrl.SetTextInsets(def.GetTextInsets());
            ctrl.SetCellInsets(def.GetCellInsets());
            ctrl.SetCells(def.GetCells());
            ctrl.SetFixedWidth(def.GetFixedWidth());
            ctrl.SetFixedHeight(def.GetFixedHeight());

            ctrl.SetLineHeight(def.GetLineHeight());
            ctrl.SetScrollBarVisible((ScrollWindow.POLICY)def.GetScrollBarVisible());
            ctrl.SetSmoothScroll(def.GetSmoothScroll());

            if (!string.IsNullOrEmpty(def.GetTexture()))
            {
                Texture2D ctrl_tex = null;
                var path = "Screens/";
                ctrl_tex = Resources.Load<Texture2D>(path + def.GetTexture());
                ctrl.SetTexture(ctrl_tex);
            }

            FontItem f = FontMgr.Find(def.GetFont());
            if (f == null) f = FontMgr.GetDefault();
            if (f != null)
            {
                ctrl.SetFont(f);
                ctrl.SetFontSize(f.size);
            }
        }
        protected virtual void CreateDefSlider(CtrlDef def)
        {
            Slider ctrl = CreateSlider(def.GetText(),
                                    def.GetX(),
                                    def.GetY(),
                                    def.GetW(),
                                    def.GetH(),
                                    def.GetID(),
                                    def.GetParentID());


            ctrl.SetAltText(def.GetAltText());
            ctrl.SetEnabled(def.IsEnabled());
            ctrl.SetBackColor(def.GetBackColor());
            ctrl.SetForeColor(def.GetForeColor());

            ctrl.SetActive(def.GetActive());
            ctrl.SetOrientation(def.GetOrientation());
            ctrl.SetFillColor(def.GetActiveColor());
            ctrl.SetBorderColor(def.GetBorderColor());
            ctrl.SetBorder(def.GetBorder());
            ctrl.SetStyle(def.GetStyle());
            ctrl.SetTransparent(def.GetTransparent());
            ctrl.SetHidePartial(def.GetHidePartial());
            ctrl.SetNumLeds(def.GetNumLeds());

            ctrl.SetMargins(def.GetMargins());
            ctrl.SetTextInsets(def.GetTextInsets());
            ctrl.SetCellInsets(def.GetCellInsets());
            ctrl.SetCells(def.GetCells());
            ctrl.SetFixedWidth(def.GetFixedWidth());
            ctrl.SetFixedHeight(def.GetFixedHeight());

            FontItem f = FontMgr.Find(def.GetFont());
            if (f == null) f = FontMgr.GetDefault();
            if (f != null)
            {
                ctrl.SetFont(f);
                ctrl.SetFontSize(f.size);
            }
        }
        protected virtual void CreateDefRichText(CtrlDef def)
        {
            RichTextBox ctrl = CreateRichTextBox(def.GetText(),
                                                def.GetX(),
                                                def.GetY(),
                                                def.GetW(),
                                                def.GetH(),
                                                def.GetID(),
                                                def.GetParentID(),
                                                def.GetStyle());

            ctrl.SetAltText(def.GetAltText());
            ctrl.SetBackColor(def.GetBackColor());
            ctrl.SetForeColor(def.GetForeColor());
            ctrl.SetLineHeight(def.GetLineHeight());
            ctrl.SetLeading(def.GetLeading());
            ctrl.SetScrollBarVisible((ScrollWindow.POLICY)def.GetScrollBarVisible());
            ctrl.SetSmoothScroll(def.GetSmoothScroll());
            ctrl.SetTextAlign(def.GetTextAlign());
            ctrl.SetTransparent(def.GetTransparent());
            ctrl.SetHidePartial(def.GetHidePartial());

            ctrl.SetMargins(def.GetMargins());
            ctrl.SetTextInsets(def.GetTextInsets());
            ctrl.SetCellInsets(def.GetCellInsets());
            ctrl.SetCells(def.GetCells());
            ctrl.SetFixedWidth(def.GetFixedWidth());
            ctrl.SetFixedHeight(def.GetFixedHeight());

            if (!string.IsNullOrEmpty(def.GetTexture()))
            {
                Texture2D ctrl_tex = null;
                var path = "Screens/";
                ctrl_tex = Resources.Load<Texture2D>(path + def.GetTexture());
                ctrl.SetTexture(ctrl_tex);
            }
            FontItem f = FontMgr.Find(def.GetFont());
            if (f == null) f = FontMgr.GetDefault();
            if (f != null)
            {
                ctrl.SetFont(f);
                ctrl.SetFontSize(f.size);
            }

            for (int i = 0; i < def.NumTabs(); i++)
                ctrl.SetTabStop(i, def.GetTab(i));
        }
        protected void REGISTER_CLIENT(ETD eid, ActiveWindow ctrl, PFVAWE fname)
        {
            if (ctrl != null) ctrl.RegisterClient(eid, this, fname);
        }

        protected internal override void BuildGamebjects()
        {
            base.BuildGamebjects();

            obj = new GameObject();
            obj.AddComponent<RectTransform>();
            obj.transform.SetParent(this.parentObj.transform, false);
            obj.name = desc;

            canvas = obj.AddComponent<Canvas>();
            canvas.pixelPerfect = true;
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            canvas.worldCamera = camera;

            CanvasScaler c = obj.AddComponent<CanvasScaler>();
            c.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            //c.referenceResolution = new Vector2(this.rect.w, this.rect.h);
            c.referenceResolution = new Vector2(screen.Width(), screen.Height());


            GraphicRaycaster gr = obj.AddComponent<GraphicRaycaster>();
            AudioSource src = this.parentObj.GetComponent<AudioSource>();
            if (src == null)
                ErrLogger.PrintLine("WARNING: FormWindow.BuildGamebjects: Parent has no AudioSource component");
            Sound.AudioSource = src;

            if (canvas.worldCamera != null)
                canvas.worldCamera.backgroundColor = this.GetBackColor();

            GameObject panel = null;
            if (this.rect.w != screen.Width() || this.rect.h != screen.Height())
            {
                isModal = true;
                rect.x = rect.x + rect.w;

                panel = new GameObject();
                RectTransform rectTransform = panel.AddComponent<RectTransform>();
                rectTransform.SetParent(obj.transform);
                panel.name = desc + " panel";
                panel.SetActive(true);

                rectTransform.pivot = new Vector2(0, 0);
                rectTransform.anchorMin = new Vector2(0, 1);
                rectTransform.anchorMax = new Vector2(0, 1);
                rectTransform.localScale = new Vector3(1, 1, 1);
                rectTransform.sizeDelta = new Vector2(rect.w, rect.h);
                rectTransform.anchoredPosition = new Vector3(rect.x, -(rect.y + rect.h), 0);
            }
            if (!transparent)
            {
                if (texture != null && texture.width > 0)
                {
                    Image img;
                    if (panel != null)
                        img = panel.AddComponent<Image>();
                    else
                        img = obj.AddComponent<Image>();
                    Insets cm = this.GetMargins();
                    img.type = Image.Type.Sliced;
                    Vector4 border = new Vector4(cm.left, cm.bottom, cm.right, cm.top); //X=left, Y=bottom, Z=right, W=top.

                    Sprite s = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f), 100f, 0, SpriteMeshType.FullRect, border);
                    s.name = texture.name;
                    img.sprite = s;
                }
            }

            // update children windows:
            foreach (var child in children)
                child.BuildGamebjects();
        }

        protected Canvas canvas;
        protected Camera camera;
    }

    // https://stackoverflow.com/questions/41194515/unity-create-ui-control-from-script

}
