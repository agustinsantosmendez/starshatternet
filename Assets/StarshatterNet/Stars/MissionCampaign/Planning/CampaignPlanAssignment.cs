﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CampaignPlanAssignment.h/CampaignPlanAssignment.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CampaignPlanAssignment creates combat assignments for
    assets within each combat zone as the third step in
    force tasking.
*/
using System.Collections.Generic;

namespace StarshatterNet.Stars.MissionCampaign.Planning
{
#warning CampaignPlanAssignment class is still in development and is not recommended for production.
    public class CampaignPlanAssignment : CampaignPlan
    {
        public CampaignPlanAssignment(Campaign c) : base(c) { }
        // ~CampaignPlanAssignment() { }

        // operations:
        public override void ExecFrame()
        {
            if (campaign != null && campaign.IsActive())
            {
                // once every few minutes is plenty:
                if (Campaign.Stardate() - exec_time < 300)
                    return;

                foreach (Combatant iter in campaign.GetCombatants())
                {
                    ProcessCombatant(iter);
                }

                exec_time = Campaign.Stardate();
            }
        }

        protected virtual void ProcessCombatant(Combatant c)
        {
            CombatGroup force = c.GetForce();
            if (force != null)
            {
                force.CalcValue();
                force.ClearAssignments();
            }

            foreach (CombatZone zone in campaign.GetZones())
            {
                ProcessZone(c, zone);
            }
        }
        protected virtual void ProcessZone(Combatant c, CombatZone zone)
        {
            List<CombatGroup> groups = new List<CombatGroup>();
            BuildZoneList(c.GetForce(), zone, groups);

            ZoneForce force = zone.FindForce(c.GetIFF());

            // defensive assignments:
            foreach (CombatGroup def in force.GetDefendList())
            {
                List<CombatGroup> assets = new List<CombatGroup>();
                BuildAssetList(CombatGroup.PreferredDefender(def.Type()), groups, assets);

                foreach (CombatGroup g in assets)
                {
                    CombatAssignment a = new CombatAssignment(Mission.TYPE.DEFEND, def, g);

                    if (a != null)
                        g.GetAssignments().Add(a);
                }
            }

            // offensive assignments:
            foreach (CombatGroup target in force.GetTargetList())
            {
                List<CombatGroup> assets = new List<CombatGroup>();
                BuildAssetList(CombatGroup.PreferredAttacker(target.Type()), groups, assets);

                foreach (CombatGroup asset in assets)
                {
                    Mission.TYPE mtype = Mission.TYPE.ASSAULT;

                    if (target.IsStrikeTarget())
                        mtype = Mission.TYPE.STRIKE;

                    else if (target.IsFighterGroup())
                        mtype = Mission.TYPE.SWEEP;

                    else if (target.Type() == CombatGroup.GROUP_TYPE.LCA_SQUADRON)
                        mtype = Mission.TYPE.INTERCEPT;

                    CombatAssignment a = new CombatAssignment(mtype, target, asset);

                    if (a != null)
                        asset.GetAssignments().Add(a);
                }
            }
        }


        protected virtual void BuildZoneList(CombatGroup g, CombatZone zone, List<CombatGroup> groups)
        {
            if (g == null)
                return;

            if (g.GetAssignedZone() == zone)
                groups.Add(g);

            foreach (CombatGroup iter in g.GetComponents())
                BuildZoneList(iter, zone, groups);
        }

        protected virtual void BuildAssetList(CombatGroup.GROUP_TYPE[] pref, List<CombatGroup> groups, List<CombatGroup> assets)
        {
            if (pref == null)
                return;

            foreach (var gt in pref)
            {
                foreach (CombatGroup g in groups)
                {
                    if (g.Type() == gt && g.CountUnits() > 0)
                        assets.Add(g);
                }
            }
        }
    }
}
