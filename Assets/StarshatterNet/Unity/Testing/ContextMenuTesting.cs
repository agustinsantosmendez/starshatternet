﻿using System;
using System.Collections.Generic;
using StarshatterNet.Stars.Graphics;
using StarshatterNet.Stars.Graphics.Views;
using StarshatterNet.Stars.Views;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ContextMenuTesting : MonoBehaviour
{
    public GameObject canvas;
    private int width, height;

    private GameObject menuObj;
    private StarshatterNet.Stars.Views.Menu menu;

    void Awake()
    {
        width = Screen.width;
        height = Screen.height;

        menu = new StarshatterNet.Stars.Views.Menu("Context Menu");

        StarshatterNet.Stars.Views.Menu menu1 = new StarshatterNet.Stars.Views.Menu("Context Menu for Option 1");
        menu1.AddItem("Option 1-1");

        StarshatterNet.Stars.Views.Menu menu11 = new StarshatterNet.Stars.Views.Menu("Context Menu for Option 1-1");
        menu11.AddItem("Option 1-1-1");
        menu1.AddMenu("Option 1-2", menu11);

        StarshatterNet.Stars.Views.Menu menu2 = new StarshatterNet.Stars.Views.Menu("Context Menu for Option 2");
        menu2.AddItem("Option 2-1");
        menu2.AddItem("Option 2-2");

        StarshatterNet.Stars.Views.Menu menu3 = new StarshatterNet.Stars.Views.Menu("Context Menu for Option 3");
        menu3.AddItem("Option 3-1");
        menu3.AddItem("Option 3-2");
        menu3.AddItem("Option 3-3");

        menu.AddMenu("Option 1", menu1);
        menu.AddMenu("Option 2", menu2);
        menu.AddMenu("Option 3", menu3);
        menu.AddItem("Option 4");
    }

    // Start is called before the first frame update
    void Start()
    {
        //stars.Run();
        //stars.Activate(true);
    }

    // Update is called once per frame
    void Update()
    {
        //Check if the left Mouse button is clicked
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Debug.Log("Hit " + Input.mousePosition);
            if (menuObj != null)
                GameObject.Destroy(menuObj);

            menuObj = PopupMenuCreator.CreatePopupMenu(canvas, Input.mousePosition, menu);
            PopupMenu popupItem = menuObj.GetComponent<PopupMenu>();
            popupItem.onValueSelected.AddListener(OnSelectItem);
        }
        //if (Input.GetKeyDown(KeyCode.I))
        //    stars.ScreenCapture();
    }
    private void OnSelectItem(GameObject gameObj)
    {
        // Output to console the clicked GameObject's name and the following message. 
        // You can replace this with your own actions for when clicking the GameObject.
        Debug.Log("ContextMenuTesting " + gameObj.name + " Game Object Clicked!");
    }

}