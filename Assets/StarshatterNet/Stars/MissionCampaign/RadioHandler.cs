﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      RadioHandler.h/RadioHandler.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    RadioHandler (radio comms) class declaration
*/
using System;
using StarshatterNet.Stars.AI;
using StarshatterNet.Stars.MissionCampaign;
using StarshatterNet.Stars.SimElements;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars
{
    public class RadioHandler
    {
        public RadioHandler() { }
        // public virtual ~RadioHandler();

        public virtual bool ProcessMessage(RadioMessage msg, Ship s)
        {
            if (s == null || msg == null || msg.Sender() == null)
                return false;

            if (s.Class() >= SimElements.CLASSIFICATION.FARCASTER && s.Class() <= SimElements.CLASSIFICATION.C3I)
                return false;

            if (msg.Sender().IsRogue())
            {
                Ship sender = (Ship)msg.Sender();  // cast-away const
                RadioMessage nak = new RadioMessage(sender, s, RadioMessage.ACTION.NACK);
                RadioTraffic.Transmit(nak);
                return false;
            }

            bool respond = (s != msg.Sender());

            // SPECIAL CASE:
            // skip navpoint must be processed by elem leader,
            // even if the elem leader sent the message:

            if (msg.Action() == RadioMessage.ACTION.SKIP_NAVPOINT && !respond)
                ProcessMessageAction(msg, s);

            if (!ProcessMessageOrders(msg, s))
                respond = respond && ProcessMessageAction(msg, s);

            return respond;
        }
        public virtual void AcknowledgeMessage(RadioMessage msg, Ship s)
        {
            if (s != null && msg != null && msg.Sender() != null && msg.Action() != 0)
            {
                if (msg.Action() >= RadioMessage.ACTION.ACK && msg.Action() <= RadioMessage.ACTION.NACK)
                    return;  // nothing to say here

                Ship sender = (Ship)msg.Sender();  // cast-away const
                RadioMessage ack = new RadioMessage(sender, s, RadioMessage.ACTION.ACK);
                RadioTraffic.Transmit(ack);
            }
        }



        protected virtual bool IsOrder(RadioMessage.ACTION action)
        {
            bool result = false;

            switch (action)
            {
                default:
                case RadioMessage.ACTION.NONE:
                case RadioMessage.ACTION.ACK:
                case RadioMessage.ACTION.NACK:
                    result = false;
                    break;

                // target mgt:
                case RadioMessage.ACTION.ATTACK:
                case RadioMessage.ACTION.ESCORT:
                case RadioMessage.ACTION.BRACKET:
                case RadioMessage.ACTION.IDENTIFY:
                    result = true;
                    break;

                // combat mgt:
                case RadioMessage.ACTION.COVER_ME:
                case RadioMessage.ACTION.WEP_HOLD:
                case RadioMessage.ACTION.FORM_UP:
                    result = true;
                    break;

                case RadioMessage.ACTION.WEP_FREE:
                case RadioMessage.ACTION.SAY_POSITION:
                case RadioMessage.ACTION.LAUNCH_PROBE:
                    result = false;
                    break;

                // formation mgt:
                case RadioMessage.ACTION.GO_DIAMOND:
                case RadioMessage.ACTION.GO_SPREAD:
                case RadioMessage.ACTION.GO_BOX:
                case RadioMessage.ACTION.GO_TRAIL:
                    result = true;
                    break;

                // mission mgt:
                case RadioMessage.ACTION.MOVE_PATROL:
                    result = true; break;
                case RadioMessage.ACTION.SKIP_NAVPOINT:
                    result = false; break;
                case RadioMessage.ACTION.RESUME_MISSION:
                    result = true; break;

                case RadioMessage.ACTION.RTB:
                case RadioMessage.ACTION.DOCK_WITH:
                case RadioMessage.ACTION.QUANTUM_TO:
                case RadioMessage.ACTION.FARCAST_TO:
                    result = true; break;

                // sensor mgt:
                case RadioMessage.ACTION.GO_EMCON1:
                case RadioMessage.ACTION.GO_EMCON2:
                case RadioMessage.ACTION.GO_EMCON3:
                    result = true; break;

                // support:
                case RadioMessage.ACTION.REQUEST_PICTURE:
                case RadioMessage.ACTION.REQUEST_SUPPORT:
                case RadioMessage.ACTION.PICTURE:
                    result = false; break;

                // traffic control:
                case RadioMessage.ACTION.CALL_INBOUND:
                case RadioMessage.ACTION.CALL_APPROACH:
                case RadioMessage.ACTION.CALL_CLEARANCE:
                case RadioMessage.ACTION.CALL_FINALS:
                case RadioMessage.ACTION.CALL_WAVE_OFF:
                    result = false; break;
            }

            return result;
        }
        protected virtual bool ProcessMessageOrders(RadioMessage msg, Ship ship)
        {
            Instruction instruction = ship.GetRadioOrders();
            RadioMessage.ACTION action = 0;

            if (msg != null && msg.Action() == RadioMessage.ACTION.RESUME_MISSION)
            {
                instruction.SetAction((Instruction.ACTION)0);
                instruction.SetFormation((Instruction.FORMATION)(-1));
                instruction.SetWeaponsFree(true);
                if (instruction.GetTarget() != null)
                {
                    instruction.ClearTarget();
                    ship.DropTarget();
                }
                return true;
            }

            if (msg != null && IsOrder(msg.Action()))
            {
                bool posture_only = false;

                action = msg.Action();

                if (action == RadioMessage.ACTION.FORM_UP)
                    action = RadioMessage.ACTION.WEP_HOLD;

                // target orders => drop current target:
                if (action >= RadioMessage.ACTION.ATTACK &&
                        action <= RadioMessage.ACTION.COVER_ME ||
                        action == RadioMessage.ACTION.WEP_HOLD ||
                        action >= RadioMessage.ACTION.DOCK_WITH &&
                        action <= RadioMessage.ACTION.FARCAST_TO)
                {

                    if (ship != msg.Sender())
                        ship.DropTarget();

                    IDirector dir = ship.GetDirector();
                    if (dir != null && dir.Type() >= SteerAI.SteerType.SEEKER && dir.Type() <= SteerAI.SteerType.GROUND)
                    {
                        SteerAI ai = (SteerAI)dir;
                        ai.SetTarget(null);
                    }

                    // farcast and quantum jump radio messages:
                    if (action >= RadioMessage.ACTION.QUANTUM_TO)
                    {
                        Sim sim = Sim.GetSim();

                        if (sim != null)
                        {
                            SimRegion rgn = sim.FindRegion(msg.Info());

                            if (rgn != null)
                            {
                                instruction.SetAction((Instruction.ACTION)action);
                                instruction.SetLocation(new Point(0, 0, 0));
                                instruction.SetRegion(rgn);
                                instruction.SetFarcast(action == RadioMessage.ACTION.FARCAST_TO ? 1 : 0);
                                instruction.SetWeaponsFree(false);
                                return true;
                            }
                        }
                    }
                }

                // formation orders => set formation:
                if (action >= RadioMessage.ACTION.GO_DIAMOND &&
                        action <= RadioMessage.ACTION.GO_TRAIL)
                {

                    switch (action)
                    {
                        case RadioMessage.ACTION.GO_DIAMOND: instruction.SetFormation(Instruction.FORMATION.DIAMOND); break;
                        case RadioMessage.ACTION.GO_SPREAD: instruction.SetFormation(Instruction.FORMATION.SPREAD); break;
                        case RadioMessage.ACTION.GO_BOX: instruction.SetFormation(Instruction.FORMATION.BOX); break;
                        case RadioMessage.ACTION.GO_TRAIL: instruction.SetFormation(Instruction.FORMATION.TRAIL); break;
                    }

                    posture_only = true;
                }

                // emcon orders => set emcon:
                if (action >= RadioMessage.ACTION.GO_EMCON1 &&
                        action <= RadioMessage.ACTION.GO_EMCON3)
                {

                    switch (msg.Action())
                    {
                        case RadioMessage.ACTION.GO_EMCON1: instruction.SetEMCON(1); break;
                        case RadioMessage.ACTION.GO_EMCON2: instruction.SetEMCON(2); break;
                        case RadioMessage.ACTION.GO_EMCON3: instruction.SetEMCON(3); break;
                    }

                    posture_only = true;
                }

                if (!posture_only)
                {
                    instruction.SetAction((Instruction.ACTION)action);
                    instruction.ClearTarget();

                    if (msg.TargetList().Count > 0)
                    {
                        SimObject msg_tgt = msg.TargetList()[0];
                        instruction.SetTarget(msg_tgt);
                        instruction.SetLocation(msg_tgt.Location());
                    }

                    else if (action == RadioMessage.ACTION.COVER_ME)
                    {
                        instruction.SetTarget((Ship)msg.Sender());
                        instruction.SetLocation(msg.Sender().Location());
                    }

                    else if (action == RadioMessage.ACTION.MOVE_PATROL)
                    {
                        instruction.SetLocation(msg.Location());
                    }

                    // handle element engagement:
                    if (action == RadioMessage.ACTION.ATTACK && msg.TargetList().Count > 0)
                    {
                        Element elem = msg.DestinationElem();

                        if (elem == null && msg.DestinationShip() != null)
                            elem = msg.DestinationShip().GetElement();

                        if (elem != null)
                        {
                            SimObject msg_tgt = msg.TargetList()[0];
                            if (msg_tgt != null && msg_tgt.Type() == (int)SimObject.TYPES.SIM_SHIP)
                            {
                                Element tgt = ((Ship)msg_tgt).GetElement();
                                elem.SetAssignment(tgt);

                                if (msg.TargetList().Count > 1)
                                    instruction.SetTarget(tgt.Name());
                                else
                                    instruction.SetTarget(msg_tgt);
                            }
                            else
                            {
                                elem.ResumeAssignment();
                            }
                        }
                    }

                    else if (action == RadioMessage.ACTION.RESUME_MISSION)
                    {
                        Element elem = msg.DestinationElem();

                        if (elem == null && msg.DestinationShip() != null)
                            elem = msg.DestinationShip().GetElement();

                        if (elem != null)
                        {
                            elem.ResumeAssignment();
                        }
                    }
                }

                instruction.SetWeaponsFree(action <= RadioMessage.ACTION.WEP_FREE);
                return true;
            }

            return false;
        }
        protected virtual bool ProcessMessageAction(RadioMessage msg, Ship ship)
        {
            if (msg == null) return false;

            if (msg.Action() == RadioMessage.ACTION.CALL_INBOUND)
                return Inbound(msg, ship);

            if (msg.Action() == RadioMessage.ACTION.CALL_FINALS)
                return true;   // acknowledge

            if (msg.Action() == RadioMessage.ACTION.REQUEST_PICTURE)
                return Picture(msg, ship);

            if (msg.Action() == RadioMessage.ACTION.REQUEST_SUPPORT)
                return Support(msg, ship);

            if (msg.Action() == RadioMessage.ACTION.SKIP_NAVPOINT)
                return SkipNavpoint(msg, ship);

            if (msg.Action() == RadioMessage.ACTION.LAUNCH_PROBE)
                return LaunchProbe(msg, ship);

            return false;
        }

        protected virtual bool Inbound(RadioMessage msg, Ship ship)
        {
            Ship inbound = (Ship)msg.Sender();
            Hangar hangar = ship.GetHangar();
            FlightDeck deck = null;
            int squadron = -1;
            int slot = -1;
            bool same_rgn = false;

            if (inbound != null && inbound.GetRegion() == ship.GetRegion())
                same_rgn = true;

            // is the sender already inbound to us?
            if (inbound.GetInbound() != null &&
                    inbound.GetInbound().GetDeck() != null &&
                    inbound.GetInbound().GetDeck().GetCarrier() == ship)
            {
                InboundSlot islot = inbound.GetInbound();
                deck = islot.GetDeck();
                squadron = islot.Squadron();
                slot = islot.Index();
            }

            // otherwise, find space for sender:
            else
            {
                if (hangar != null && same_rgn)
                {
                    if (hangar.FindSlot(inbound, ref squadron, ref slot))
                    {
                        int shortest_queue = 1000;

                        for (int i = 0; i < ship.NumFlightDecks(); i++)
                        {
                            FlightDeck d = ship.GetFlightDeck(i);
                            if (d.IsRecoveryDeck())
                            {
                                int nwaiting = d.GetRecoveryQueue().Count;

                                if (nwaiting < shortest_queue)
                                {
                                    deck = d;
                                    shortest_queue = nwaiting;
                                }
                            }
                        }
                    }
                }
            }

            // if no space (or not a carrier!) wave sender off:
            if (deck == null || !same_rgn || squadron < 0 || slot < 0)
            {
                RadioMessage wave_off = new RadioMessage(inbound, ship, RadioMessage.ACTION.NACK);
                if (hangar == null)
                    wave_off.SetInfo(Game.GetText("RadioHandler.no-hangar"));

                else if (!same_rgn)
                {
                    string info = string.Format(Game.GetText("RadioHandler.too-far-away"), ship.GetRegion().Name());
                    wave_off.SetInfo(info);
                }

                else
                    wave_off.SetInfo(Game.GetText("RadioHandler.all-full"));

                RadioTraffic.Transmit(wave_off);
                return false;
            }

            // put sender in recovery queue, if not already there:
            InboundSlot inbound_slot = inbound.GetInbound();
            int sequence = 0;

            if (inbound_slot == null)
            {
                inbound_slot = new InboundSlot(inbound, deck, squadron, slot);
                sequence = deck.Inbound(inbound_slot);
            }
            else
            {
                sequence = inbound_slot.Index();
            }

            // inform sender of status:
            RadioMessage approach = new RadioMessage(inbound, ship, RadioMessage.ACTION.CALL_APPROACH);

            if (inbound_slot.Cleared())
            {
                string info = string.Format(Game.GetText("RadioHandler.cleared"), deck.Name());
                approach.SetInfo(info);
            }
            else if (sequence != 0)
            {
                string info = string.Format(Game.GetText("RadioHandler.sequenced"), sequence, deck.Name());
                approach.SetInfo(info);
            }

            RadioTraffic.Transmit(approach);

            return false;
        }
        protected virtual bool Picture(RadioMessage msg, Ship ship)
        {
            if (ship == null) return false;

            // try to find some enemy fighters in the area:
            Ship tgt = null;
            double range = 1e9;

            foreach (Contact c in ship.ContactList())
            {
                int iff = c.GetIFF(ship);
                Ship s = c.GetShip();

                if (s != null && s.IsDropship() && s.IsHostileTo(ship))
                {
                    double s_range = new Point(msg.Sender().Location() - s.Location()).Length;
                    if (tgt == null || s_range < range)
                    {
                        tgt = s;
                        range = s_range;
                    }
                }
            }

            // found some:
            if (tgt != null)
            {
                Element sender = msg.Sender().GetElement();
                Element tgt_elem = tgt.GetElement();
                RadioMessage response = new RadioMessage(sender, ship, RadioMessage.ACTION.ATTACK);

                if (tgt_elem != null)
                {
                    for (int i = 1; i <= tgt_elem.NumShips(); i++)
                        response.AddTarget(tgt_elem.GetShip(i));
                }
                else
                {
                    response.AddTarget(tgt);
                }

                RadioTraffic.Transmit(response);
            }

            // nobody worth killin':
            else
            {
                Ship sender = (Ship)msg.Sender();  // cast-away const
                RadioMessage response = new RadioMessage(sender, ship, RadioMessage.ACTION.PICTURE);
                RadioTraffic.Transmit(response);
            }

            return false;
        }
        protected virtual bool Support(RadioMessage msg, Ship ship)
        {
            if (ship == null) return false;

            // try to find some fighters with time on their hands...
            Element help = null;
            Element cmdr = ship.GetElement();
            Element baby = msg.Sender().GetElement();
            SimRegion rgn = msg.Sender().GetRegion();

            for (int i = 0; i < rgn.Ships().Count; i++)
            {
                Ship s = rgn.Ships()[i];
                Element e = s.GetElement();

                if (e != null && s.IsDropship() &&
                        e.Type() == Mission.TYPE.PATROL &&
                        e != baby &&
                        cmdr.CanCommand(e) &&
                        s.GetRadioOrders().Action() == (Instruction.ACTION)0)
                {
                    help = e;
                    break;
                }
            }

            // found some:
            if (help != null)
            {
                RadioMessage escort = new RadioMessage(help, ship, RadioMessage.ACTION.ESCORT);
                escort.TargetList().Add(msg.Sender());
                RadioTraffic.Transmit(escort);

                string ok = Game.GetText("RadioHandler.help-enroute");
                Ship sender = (Ship)msg.Sender();  // cast-away const
                RadioMessage response = new RadioMessage(sender, ship, RadioMessage.ACTION.ACK);
                response.SetInfo(ok);
                RadioTraffic.Transmit(response);
            }

            // no help in sight:
            else
            {
                string nope = Game.GetText("RadioHandler.no-help-for-you");
                Ship sender = (Ship)msg.Sender();  // cast-away const
                RadioMessage response = new RadioMessage(sender, ship, RadioMessage.ACTION.NACK);
                response.SetInfo(nope);
                RadioTraffic.Transmit(response);
            }

            return false;
        }
        protected virtual bool SkipNavpoint(RadioMessage msg, Ship ship)
        {
            // Find next Instruction:
            Instruction navpt = ship.GetNextNavPoint();
            int elem_index = ship.GetElementIndex();

            if (navpt != null && elem_index < 2)
            {
                ship.SetNavptStatus(navpt, Instruction.STATUS.SKIPPED);
            }

            return true;
        }
        protected virtual bool LaunchProbe(RadioMessage msg, Ship ship)
        {
            if (ship != null && ship.GetProbeLauncher() != null)
            {
                ship.LaunchProbe();
                return ship.GetProbe() != null;
            }

            return false;
        }

    }
}
