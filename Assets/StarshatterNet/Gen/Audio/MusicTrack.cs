﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MusicTrack.h/MusicTrack.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    MusicTrack class
*/
using StarshatterNet.Config;
using StarshatterNet.Stars;
using System;
using UnityEngine;

namespace StarshatterNet.Audio
{
    public class MusicTrack
    {
        public enum STATE { NONE, FADE_IN, PLAY, FADE_OUT, STOP };
        public MusicTrack(string txt, MusicDirector.MODES m = 0, int n = 0)
        {
            name = txt; sound = null; state = STATE.NONE; mode = m; index = n;
            fade = 0; fade_time = FADE_TIME;

            long max_vol = 0;

            if (mode >= MusicDirector.MODES.FLIGHT)
                max_vol = AudioConfig.GameMusic();
            else
                max_vol = AudioConfig.MenuMusic();

            if (max_vol <= AudioConfig.Silence())
                return;

            sound = Sound.CreateStream(name);

            if (name.Contains("-loop"))
            {
                sound.SetFlags(Sound.FlagEnum.STREAMED |
                                Sound.FlagEnum.OGGVORBIS |
                                Sound.FlagEnum.LOOP |
                                Sound.FlagEnum.LOCKED);
            }

            else
            {
                sound.SetFlags(Sound.FlagEnum.STREAMED |
                                Sound.FlagEnum.OGGVORBIS |
                                Sound.FlagEnum.LOCKED);
            }

            sound.SetVolume((long)SILENCE);
            starsGame = Starshatter.GetInstance();
        }

        public virtual void ExecFrame()
        {
            bool music_pause = false;

            if (starsGame != null)
            {
                music_pause = (starsGame.GetGameMode() == Starshatter.MODE.PLAY_MODE) &&
                starsGame.Paused();
            }

            if (sound != null && !music_pause)
            {
                double fvol = 1;
                long volume = 0;

                switch (state)
                {
                    case STATE.PLAY:
                        if (IsReady())
                            sound.Play();
                        SetVolume(volume);
                        break;

                    case STATE.FADE_IN:
                        if (IsReady())
                            sound.Play();

                        if (fade > 0)
                        {
                            fvol = fade / fade_time;
                            volume = (long)(fvol * SILENCE);
                            SetVolume(volume);
                        }

                        if (fade < 0.01)
                            state = STATE.PLAY;
                        break;

                    case STATE.FADE_OUT:
                        if (IsReady())
                            sound.Play();

                        if (fade > 0)
                        {
                            fvol = 1 - fade / fade_time;
                            volume = (long)(fvol * SILENCE);
                            SetVolume(volume);
                        }

                        if (fade < 0.01)
                            state = STATE.STOP;
                        break;

                    case STATE.STOP:
                        if (IsPlaying())
                        {
                            sound.Stop();
                            sound.Release();
                            sound = null;
                        }
                        break;
                }

                if (fade > 0)
                    fade -= Game.GUITime();

                if (fade < 0)
                    fade = 0;
            }
        }

        public virtual void Play()
        {
            state = STATE.PLAY;
            fade = 0;
        }

        public virtual void Stop()
        {
            state = STATE.STOP;
            fade = 0;
        }

        public virtual void FadeIn()
        {
            if (state != STATE.FADE_IN && state != STATE.PLAY)
            {
                state = STATE.FADE_IN;
                fade = fade_time;
            }
        }

        public virtual void FadeOut()
        {
            if (state != STATE.FADE_OUT && state != STATE.STOP)
            {
                state = STATE.FADE_OUT;
                fade = fade_time;
            }
        }

        // accessors / mutators
        public string Name() { return name; }
        //public Song GetSound() { return sound; }
        public STATE GetState() { return state; }
        public MusicDirector.MODES GetMode() { return mode; }
        public int GetIndex() { return index; }

        public bool IsReady()
        {
             if (sound != null)
                return sound.IsReady();

            return false;
         }

        public bool IsPlaying()
        {
            if (sound != null)
                return sound.IsPlaying();

            return false;
        }

        public bool IsDone()
        {
            if (sound != null)
                return sound.IsDone() || sound.LoopCount() >= 5;

            return true;
        }

        public bool IsLooped()
        {
            if (sound != null)
                return sound.IsDone() || sound.LoopCount() >= 4;

            return true;
        }


        public virtual float GetVolume()
        {
            if (sound != null)
                return sound.GetVolume();

            return 0;
        }

        public virtual void SetVolume(long v)
        {
            if (sound != null)
            {
                long max_vol = 0;

                if (mode >= MusicDirector.MODES.FLIGHT)
                    max_vol = AudioConfig.GameMusic();
                else
                    max_vol = AudioConfig.MenuMusic();

                if (v > max_vol)
                    v = max_vol;

                sound.SetVolume(v);
            }
        }


        public virtual double GetTotalTime()
        {
            if (sound != null)
                return sound.GetTotalTime();

            return 0;
        }

        public virtual double GetTimeRemaining()
        {
            if (sound != null)
                return sound.GetTimeRemaining();

            return 0;
        }


        public virtual double GetTimeElapsed()
        {
            if (sound != null)
                return sound.GetTimeElapsed();

            return 0;
        }

        public virtual double GetFadeTime() { return fade_time; }
        public virtual void SetFadeTime(double t) { fade_time = t; }

        protected string name;
        protected Sound sound;
        protected STATE state;
        protected MusicDirector.MODES mode;
        protected int index;
        protected double fade;
        protected double fade_time = FADE_TIME;

        private readonly Starshatter starsGame;

        private const double FADE_TIME = 1.5;
        private const float SILENCE = -5000;
    }
}
