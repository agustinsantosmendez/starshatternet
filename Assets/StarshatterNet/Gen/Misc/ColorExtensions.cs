﻿using UnityEngine;

namespace StarshatterNet.Gen.Misc
{
    public static class ColorExtensions
    {
        public static double fade_level;
        public static void SetFade(double val)
        {
            fade_level = val;
        }
        public static void SetFade(double f, Color c, int build_shade)
        {
            // TODO
        }

        public static byte Red(this Color32 c)
        {
            return c.r;
        }
        public static byte Green(this Color32 c)
        {
            return c.g;
        }
        public static byte Blue(this Color32 c)
        {
            return c.b;
        }
        public static byte Alpha(this Color32 c)
        {
            return ((Color32)c).a;
        }
        public static byte Red(this Color c)
        {
            return ((Color32)c).r;
        }
        public static byte Green(this Color c)
        {
            return ((Color32)c).g;
        }
        public static byte Blue(this Color c)
        {
            return ((Color32)c).b;
        }
        public static byte Alpha(this Color c)
        {
            return ((Color32)c).a;
        }

        public static Color dim(this Color32 c, double scale)
        {
            int r = (int)(c.Red() * scale);
            int g = (int)(c.Green() * scale);
            int b = (int)(c.Blue() * scale);

            return new Color32((byte)r, (byte)g, (byte)b, (byte)c.Alpha());
        }
        public static Color dim(this Color c, double scale)
        {
            int r = (int)(c.Red() * scale);
            int g = (int)(c.Green() * scale);
            int b = (int)(c.Blue() * scale);

            return new Color((byte)r, (byte)g, (byte)b, (byte)c.Alpha());
        }


        public static Color ToTransparent(Color c)
        {
            return new Color(c.r, c.g, c.b, 0);
        }

        public static Color ToOpaque(Color c)
        {
            return new Color(c.r, c.g, c.b, 255);
        }


        public static Color Scale(Color c1, Color c2, double scale)
        {
            float r = (float)((c1.r + (c2.r - c1.r) * scale));
            float g = (float)((c1.g + (c2.g - c1.g) * scale));
            float b = (float)((c1.b + (c2.b - c1.b) * scale));
            float a = (float)((c1.a + (c2.a - c1.a) * scale));

            return new Color(r, g, b, a);
        }

        public const int SHADE_LEVELS = 64;
        public static Color ShadeColor(Color c, double shade)
        {
            int ishade = (int)(shade * SHADE_LEVELS);
            return c.ShadeColor(ishade);
        }
        public static Color ShadeColor(this Color c, int shade)
        {
            float fr = c.r, sr = fr;
            float fg = c.g, sg = fg;
            float fb = c.b, sb = fb;
            float range = SHADE_LEVELS;

            // first shade:
            if (shade < SHADE_LEVELS)
            {          // shade towards black
                sr = fr * (shade / range);
                sg = fg * (shade / range);
                sb = fb * (shade / range);
            }
            else if (shade > SHADE_LEVELS)
            {     // shade towards white
                float step = (shade - range) / range;

                sr = fr - (fr - 1.0f) * step;
                sg = fg - (fg - 1.0f) * step;
                sb = fb - (fb - 1.0f) * step;
            }
            return new Color(sr, sg, sb, c.a);
            //return new Color32((byte)(sr * 255.0), (byte)(sg * 255.0), (byte)(sb * 255.0), (byte)c.a);
        }

        public static readonly Color White = new Color32(255, 255, 255,255);
        public static readonly Color Black = new Color32(0, 0, 0, 255);
        public static readonly Color Gray = new Color32(128, 128, 128, 255);
        public static readonly Color LightGray = new Color32(192, 192, 192, 255);
        public static readonly Color DarkGray = new Color32(64, 64, 64, 255);
        public static readonly Color BrightRed = new Color32(255, 0, 0, 255);
        public static readonly Color BrightBlue = new Color32(0, 0, 255, 255);
        public static readonly Color BrightGreen = new Color32(0, 255, 0, 255);
        public static readonly Color DarkRed = new Color32(128, 0, 0, 255);
        public static readonly Color DarkBlue = new Color32(0, 0, 128, 255);
        public static readonly Color DarkGreen = new Color32(0, 128, 0, 255);
        public static readonly Color Yellow = new Color32(255, 255, 0, 255);
        public static readonly Color Cyan = new Color32(0, 255, 255, 255);
        public static readonly Color Magenta = new Color32(255, 0, 255, 255);
        public static readonly Color Tan = new Color32(180, 150, 120, 255);
        public static readonly Color Brown = new Color32(128, 100, 80, 255);
        public static readonly Color Violet = new Color32(128, 0, 128, 255);
        public static readonly Color Orange = new Color32(255, 150, 20, 255);
    }
}