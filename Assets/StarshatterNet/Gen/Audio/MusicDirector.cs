﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      MusicDirector.h/MusicDirector.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Music Director class to manage selection, setup, and playback
    of background music tracks for both menu and game modes
*/
using System;
using System.Collections.Generic;
using System.IO;
using StarshatterNet.Gen.Misc;
using UnityEditor;

namespace StarshatterNet.Audio
{
    public class MusicDirector
    {
        public enum MODES
        {
            NONE,

            // menu modes:
            MENU,
            INTRO,
            BRIEFING,
            DEBRIEFING,
            PROMOTION,
            VICTORY,
            DEFEAT,
            CREDITS,

            // in game modes:
            FLIGHT,
            COMBAT,
            LAUNCH,
            RECOVERY,

            // special modes:
            SHUTDOWN
        }

        public enum TRANSITIONS
        {
            CUT,
            FADE_OUT,
            FADE_IN,
            FADE_BOTH,
            CROSS_FADE
        }
        public MusicDirector()
        {
            ScanTracks();

            if (!no_music)
                StartThread();
        }

        // Operations:
        public MODES CheckMode(MODES req_mode)
        {
            if (req_mode == MODES.RECOVERY && recovery_tracks.Count == 0)
                req_mode = MODES.LAUNCH;

            if (req_mode == MODES.LAUNCH && launch_tracks.Count == 0)
                req_mode = MODES.FLIGHT;

            if (req_mode == MODES.COMBAT && combat_tracks.Count == 0)
                req_mode = MODES.FLIGHT;

            if (req_mode == MODES.FLIGHT && flight_tracks.Count == 0)
                req_mode = MODES.NONE;

            if (req_mode == MODES.PROMOTION && promote_tracks.Count == 0)
                req_mode = MODES.VICTORY;

            if (req_mode == MODES.DEBRIEFING && debrief_tracks.Count == 0)
                req_mode = MODES.BRIEFING;

            if (req_mode == MODES.BRIEFING && brief_tracks.Count == 0)
                req_mode = MODES.MENU;

            if (req_mode == MODES.INTRO && intro_tracks.Count == 0)
                req_mode = MODES.MENU;

            if (req_mode == MODES.VICTORY && victory_tracks.Count == 0)
                req_mode = MODES.MENU;

            if (req_mode == MODES.DEFEAT && defeat_tracks.Count == 0)
                req_mode = MODES.MENU;

            if (req_mode == MODES.CREDITS && credit_tracks.Count == 0)
                req_mode = MODES.MENU;

            if (req_mode == MODES.MENU && menu_tracks.Count == 0)
                req_mode = MODES.NONE;

            return req_mode;
        }

        public MODES GetMode() { return mode; }

        public static void Initialize()
        {
            if (music_mgr != null) music_mgr = null;
            music_mgr = new MusicDirector();
        }
        public static void Close()
        {
            music_mgr = null;
        }
        public static MusicDirector GetInstance()
        {
            return music_mgr;
        }

        public static void SetMode(MODES mode)
        {
            if (music_mgr == null || music_mgr.no_music) return;

            lock (music_mgr)
            {

                // stay in intro mode until it is complete:
                if (mode == MODES.MENU && (music_mgr.GetMode() == MODES.NONE ||
                            music_mgr.GetMode() == MODES.INTRO))
                    mode = MODES.INTRO;

                mode = music_mgr.CheckMode(mode);

                if (mode != music_mgr.mode)
                {
                    ErrLogger.PrintLine("MusicManager.SetMode() old: {0}  new: {1}", music_mgr.mode, mode);

                    music_mgr.mode = mode;

                    MusicTrack t = music_mgr.track;
                    if (t != null && t.GetState() != MusicTrack.STATE.NONE && !t.IsDone())
                    {
                        if (mode == MODES.NONE || mode == MODES.SHUTDOWN)
                            t.SetFadeTime(0.5);

                        t.FadeOut();
                    }

                    t = music_mgr.next_track;
                    if (t != null && t.GetState() != MusicTrack.STATE.NONE && !t.IsDone())
                    {
                        if (mode == MODES.NONE || mode == MODES.SHUTDOWN)
                            t.SetFadeTime(0.5);
                        t.FadeOut();

                        music_mgr.track = t;
                        music_mgr.next_track = null;
                    }

                    music_mgr.ShuffleTracks();
                    music_mgr.GetNextTrack(0);

                    if (music_mgr.next_track != null)
                        music_mgr.next_track.FadeIn();
                }
            }
        }
        public static string GetModeName(MODES mode)
        {
            switch (mode)
            {
                case MODES.NONE: return "NONE";
                case MODES.MENU: return "MENU";
                case MODES.INTRO: return "INTRO";
                case MODES.BRIEFING: return "BRIEFING";
                case MODES.DEBRIEFING: return "DEBRIEFING";
                case MODES.PROMOTION: return "PROMOTION";
                case MODES.FLIGHT: return "FLIGHT";
                case MODES.COMBAT: return "COMBAT";
                case MODES.LAUNCH: return "LAUNCH";
                case MODES.RECOVERY: return "RECOVERY";
                case MODES.VICTORY: return "VICTORY";
                case MODES.DEFEAT: return "DEFEAT";
                case MODES.CREDITS: return "CREDITS";
                case MODES.SHUTDOWN: return "SHUTDOWN";
            }

            return "UNKNOWN?";
        }

        public static bool IsNoMusic()
        {
            if (music_mgr != null)
                return music_mgr.no_music;

            return true;
        }

        private void StartThread()
        {
            // Nothing to do. Unity3d will take care of this. Really??
        }
        private void StopThread()
        {
            // Nothing to do. Unity3d will take care of this. Really??
        }

        private static MusicDirector music_mgr = null;

        public static MusicDirector Instance
        {
            get
            {
                return music_mgr;
            }
        }

        public void ExecFrame()
        {
            if (no_music) return;

            if (next_track != null && track == null)
            {
                track = next_track;
                next_track = null;
            }

            if (track != null)
            {
                if (track.IsDone())
                {
                    if (mode != MODES.NONE && mode != MODES.SHUTDOWN && next_track == null)
                    {
                        GetNextTrack(track.GetIndex() + 1);
                    }

                    track = next_track;
                    next_track = null;
                }

                else if (track.IsLooped())
                {
                    if (mode != MODES.NONE && mode != MODES.SHUTDOWN && next_track == null)
                    {
                        GetNextTrack(track.GetIndex() + 1);
                    }

                    track.FadeOut();
                    track.ExecFrame();
                }

                else
                {
                    track.ExecFrame();
                }
            }

            if (next_track != null)
            {
                if (next_track.IsDone())
                {
                    next_track = null;
                }

                else if (next_track.IsLooped())
                {
                    next_track.FadeOut();
                    next_track.ExecFrame();
                }

                else
                {
                    next_track.ExecFrame();
                }
            }
        }

        public void ScanTracks()
        {
            string respath = "Assets/Resources/";
            string audiopath = respath + "Audio/Music/";

            // Find all AudioClips that are placed in 'Assets/Resources/Audio/Music' folder
            string[] guids2 = AssetDatabase.FindAssets("t:AudioClip", new[] { "Assets/Resources/Audio/Music" });
            string[] fileEntries = new string[guids2.Length];
            for (int i = 0; i < guids2.Length; i++)
            {
                string guid2 = guids2[i];
                fileEntries[i] = AssetDatabase.GUIDToAssetPath(guid2);
            }

            //string respath = Application.dataPath + "/Resources/";
            //string audiopath = respath + "Audio/Music/";
            //string[] fileEntries = Directory.GetFiles(audiopath, "*.ogg", SearchOption.AllDirectories);
            no_music = true;
            foreach (var path in fileEntries)
            {
                var file = path.Replace(respath, "").Replace('\\', '/');
                file = Path.ChangeExtension(file, null);
                no_music = false;

                if (file.IndexOf("Menu", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    menu_tracks.Add(file);
                else if (file.IndexOf("Intro", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    intro_tracks.Add(file);
                else if (file.IndexOf("Brief", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    brief_tracks.Add(file);
                else if (file.IndexOf("Debrief", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    debrief_tracks.Add(file);
                else if (file.IndexOf("Promot", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    promote_tracks.Add(file);
                else if (file.IndexOf("Flight", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    flight_tracks.Add(file);
                else if (file.IndexOf("Combat", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    combat_tracks.Add(file);
                else if (file.IndexOf("Launch", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    launch_tracks.Add(file);
                else if (file.IndexOf("Recovery", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    recovery_tracks.Add(file);
                else if (file.IndexOf("Victory", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    victory_tracks.Add(file);
                else if (file.IndexOf("Defeat", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    defeat_tracks.Add(file);
                else if (file.IndexOf("Credit", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    credit_tracks.Add(file);
            }
        }
        protected void GetNextTrack(int index)
        {
            List<string> tracks;

            switch (mode)
            {
                case MODES.MENU: tracks = menu_tracks; break;
                case MODES.INTRO: tracks = intro_tracks; break;
                case MODES.BRIEFING: tracks = brief_tracks; break;
                case MODES.DEBRIEFING: tracks = debrief_tracks; break;
                case MODES.PROMOTION: tracks = promote_tracks; break;
                case MODES.FLIGHT: tracks = flight_tracks; break;
                case MODES.COMBAT: tracks = combat_tracks; break;
                case MODES.LAUNCH: tracks = launch_tracks; break;
                case MODES.RECOVERY: tracks = recovery_tracks; break;
                case MODES.VICTORY: tracks = victory_tracks; break;
                case MODES.DEFEAT: tracks = defeat_tracks; break;
                case MODES.CREDITS: tracks = credit_tracks; break;
                default: tracks = null; break;
            }

            if (tracks != null && tracks.Count > 0)
            {
                if (index < 0 || index >= tracks.Count)
                {
                    index = 0;

                    if (mode == MODES.INTRO)
                    {
                        mode = MODES.MENU;
                        ShuffleTracks();
                        tracks = menu_tracks;

                        ErrLogger.PrintLine("MusicManager: INTRO mode complete, switching to MENU");

                        if (tracks.Count == 0)
                            return;
                    }
                }

                next_track = new MusicTrack(tracks[index], mode, index);
                next_track.FadeIn();
            }

            else if (next_track != null)
            {
                next_track.FadeOut();
            }
        }

        // +-------------------------------------------------------------------+

        protected void ShuffleTracks()
        {
            List<string> tracks = null;

            switch (mode)
            {
                case MODES.MENU: tracks = menu_tracks; break;
                case MODES.INTRO: tracks = intro_tracks; break;
                case MODES.BRIEFING: tracks = brief_tracks; break;
                case MODES.DEBRIEFING: tracks = debrief_tracks; break;
                case MODES.PROMOTION: tracks = promote_tracks; break;
                case MODES.FLIGHT: tracks = flight_tracks; break;
                case MODES.COMBAT: tracks = combat_tracks; break;
                case MODES.LAUNCH: tracks = launch_tracks; break;
                case MODES.RECOVERY: tracks = recovery_tracks; break;
                case MODES.VICTORY: tracks = victory_tracks; break;
                case MODES.DEFEAT: tracks = defeat_tracks; break;
                case MODES.CREDITS: tracks = credit_tracks; break;
                default: tracks = null; break;
            }

            if (tracks != null && tracks.Count > 1)
            {
                tracks.Sort();

                string t = tracks[0];

                if (!Char.IsDigit(t[0]))
                    tracks.Shuffle();
            }
        }


        protected MODES mode;
        protected int transition;

        protected MusicTrack track;
        protected MusicTrack next_track;

        protected List<string> menu_tracks = new List<string>();
        protected List<string> intro_tracks = new List<string>();
        protected List<string> brief_tracks = new List<string>();
        protected List<string> debrief_tracks = new List<string>();
        protected List<string> promote_tracks = new List<string>();
        protected List<string> flight_tracks = new List<string>();
        protected List<string> combat_tracks = new List<string>();
        protected List<string> launch_tracks = new List<string>();
        protected List<string> recovery_tracks = new List<string>();
        protected List<string> victory_tracks = new List<string>();
        protected List<string> defeat_tracks = new List<string>();
        protected List<string> credit_tracks = new List<string>();

        protected bool no_music;

    }
}
