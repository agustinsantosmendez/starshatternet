﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Explosion.h/Explosion.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Explosion Sprite class
 */
using DigitalRune.Mathematics.Algebra;
using StarshatterNet.Gen.Graphics;
using StarshatterNet.Gen.Misc;
using StarshatterNet.Stars.Simulator;
using Point = DigitalRune.Mathematics.Algebra.Vector3D;

namespace StarshatterNet.Stars.Graphics.General
{
#warning Explosion class is still in development and is not recommended for production.
    public class Explosion : SimObject, ISimObserver
    {
        public enum ExplosionType
        {
            SHIELD_FLASH = 1,
            HULL_FLASH = 2,
            BEAM_FLASH = 3,
            SHOT_BLAST = 4,
            HULL_BURST = 5,
            HULL_FIRE = 6,
            PLASMA_LEAK = 7,
            SMOKE_TRAIL = 8,
            SMALL_FIRE = 9,
            SMALL_EXPLOSION = 10,
            LARGE_EXPLOSION = 11,
            LARGE_BURST = 12,
            NUKE_EXPLOSION = 13,
            QUANTUM_FLASH = 14,
            HYPER_FLASH = 15
        }
        public Explosion(ExplosionType type, Point pos, Vector3D vel,
                        float exp_scale, float part_scale,
                        SimRegion rgn = null, SimObject source = null)
        { throw new System.NotImplementedException(); }
        // ~Explosion();

        public static void Initialize()
        {
            if (initialized) return;

            string filename = "Explosions.json";
            ErrLogger.PrintLine("Loading Explosion Defs '{0}' ", filename);

            ErrLogger.PrintLine("WARNING: Explosion.Initialize() is not implemented");

            initialized = true;
        }
        public static void Close() { throw new System.NotImplementedException(); }

        public override void ExecFrame(double seconds) { throw new System.NotImplementedException(); }
        public Particles GetParticles() { return particles; }

        public void Activate(Scene scene) { throw new System.NotImplementedException(); }
        public void Deactivate(Scene scene) { throw new System.NotImplementedException(); }

        public virtual bool Update(SimObject obj) { throw new System.NotImplementedException(); }
        public virtual string GetObserverName() { throw new System.NotImplementedException(); }

        public void Observe(SimObject obj) { throw new System.NotImplementedException(); }

        public void Ignore(SimObject obj) { throw new System.NotImplementedException(); }

        protected int type;
        protected Particles particles;

        protected float scale;
        protected float scale1;
        protected float scale2;

        protected SimObject source;
        protected Point mount_rel;
        static bool initialized = false;

    }
}
