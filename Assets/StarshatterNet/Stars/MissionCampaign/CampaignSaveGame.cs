﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      CampaignSaveGame.h/CampaignSaveGame.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    CampaignSaveGame contains the logic needed to save and load
    campaign games in progress.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using StarshatterNet.Gen.Misc;

namespace StarshatterNet.Stars.MissionCampaign
{
#warning CampaignSaveGame class is still in development and is not recommended for production.
    public class CampaignSaveGame
    {
        public CampaignSaveGame(Campaign c = null)
        {
            campaign = c;
        }
        //public virtual ~CampaignSaveGame();

        public virtual Campaign GetCampaign() { return campaign; }

        public virtual void Load(string name)
        {
            ErrLogger.PrintLine("WARNING: CampaignSaveGame.Load is not yet implemented.");
        }

        public virtual void Save(string name)
        {
            if (campaign == null) return;

            CreateSaveDirectory();

            string fname = Path.Combine(GetSaveDirectory(), name);
            string timestr;
            FormatUtil.FormatDayTime(out timestr, campaign.GetTime());

            CombatGroup player_group = campaign.GetPlayerGroup();
            CombatUnit player_unit = campaign.GetPlayerUnit();
            Serialization.SaveGame saveGame = new Serialization.SaveGame();
            saveGame.campaign = campaign.Name();
            saveGame.grp_iff = player_group.GetIFF().ToString();
            saveGame.grp_type = player_group.Type().ToString();
            saveGame.grp_id = player_group.GetID().ToString();
            if (player_unit != null)
                saveGame.unit = player_unit.Name();
            saveGame.status = campaign.GetStatus().ToString();
            saveGame.basetime = campaign.GetStartTime().ToString();
            saveGame.camptime = campaign.GetTime().ToString();
            saveGame.time = timestr;
            saveGame.sitrep = campaign.Situation();
            saveGame.orders = campaign.Orders();
            saveGame.combatants = new Serialization.CombatantData[campaign.GetCombatants().Count];
            for (int i = 0; i < campaign.GetCombatants().Count; i++)
            {
                Combatant c = campaign.GetCombatants()[i];
                Serialization.CombatantData a = new Serialization.CombatantData();
                a.name = c.Name();
                a.iff = c.GetIFF().ToString();
                a.score = c.Score().ToString();
                saveGame.combatants[i] = a;
            }

            saveGame.actions = new Serialization.ActionData[campaign.GetActions().Count];
            for (int i = 0; i < campaign.GetActions().Count; i++)
            {
                CombatAction c = campaign.GetActions()[i];
                Serialization.ActionData a = new Serialization.ActionData();
                a.id = c.Identity().ToString();
                a.stat = c.Status().ToString();
                if (c.Count() != 0)
                    a.count = c.Count().ToString();
                if (c.StartAfter() != 0)
                    a.after = c.StartAfter().ToString();
                saveGame.actions[i] = a;
            }

            saveGame.events = new Serialization.EventData[campaign.GetEvents().Count];
            for (int i = 0; i < campaign.GetEvents().Count; i++)
            {
                CombatEvent e = campaign.GetEvents()[i];
                Serialization.EventData a = new Serialization.EventData();
                a.type = e.TypeName();
                a.time = e.Time().ToString();
                a.team = e.GetIFF().ToString();
                a.points = e.Points().ToString();
                a.source = e.SourceName();
                a.region = e.Region();
                a.title = e.Title();
                if (!string.IsNullOrEmpty(e.Filename()))
                    a.file = e.Filename();
                if (!string.IsNullOrEmpty(e.ImageFile()))
                    a.image = e.ImageFile();
                if (!string.IsNullOrEmpty(e.SceneFile()))
                    a.scene = e.SceneFile();
                if (string.IsNullOrEmpty(e.Filename()))
                    a.info = e.Information();
                saveGame.events[i] = a;
            }
            ReaderSupport.SerializeData<Serialization.SaveGame>(saveGame, fname);
            ErrLogger.PrintLine("WARNING: CampaignSaveGame.Save is not fully implemented.");
            foreach (Combatant c in campaign.GetCombatants())
            {
                CombatGroup g = c.GetForce();
                CombatGroup.SaveOrderOfBattle(fname, g);
            }

        }

        public static void Delete(string name)
        {
            File.Delete(Path.Combine(GetSaveDirectory(), name));
        }

        public static void RemovePlayer(Player p)
        {
            string save_dir = GetSaveDirectory(p);
            var files = ReaderSupport.GetDataFileList(save_dir, "*.*");
            foreach (string f in files)
                File.Delete(f);

        }

        public virtual void LoadAuto()
        {
            Load("AutoSave");
        }
        public virtual void SaveAuto()
        {
            Save("AutoSave");
        }

        public static string GetResumeFile()
        {
            // check for auto save game:
            if (ReaderSupport.DoesDataFileExist(Path.Combine(GetSaveDirectory(), "AutoSave")))
            {
                return "AutoSave";
            }

            return "";
        }
        public static int GetSaveGameList(out List<string> save_list)
        {
            var dirs = ReaderSupport.GetDataFileList(GetSaveDirectory(), "*.*");
            save_list = dirs.ToList();
            return save_list.Count;
        }


        private static string GetSaveDirectory()
        {
            return GetSaveDirectory(Player.GetCurrentPlayer());
        }

        private static string GetSaveDirectory(Player p)
        {
            if (p != null)
            {
                string save_dir = string.Format("{0}/{1:2}", SAVE_DIR, p.Identity());
                save_dir = ReaderSupport.GetDataPath(save_dir);
                return save_dir;
            }

            return SAVE_DIR;
        }
        private static void CreateSaveDirectory()
        {
            ReaderSupport.CreateDataDirectory(GetSaveDirectory());
        }

        private Campaign campaign;
        private static string SAVE_DIR = "SaveGame";

    }
}
namespace StarshatterNet.Stars.MissionCampaign.Serialization
{
    public class SaveGame
    {
        public string campaign;
        public string grp_iff;
        public string grp_type;
        public string grp_id;
        public string unit;
        public string status;
        public string basetime;
        public string camptime;
        public string time;
        public string sitrep;
        public string orders;
        public CombatantData[] combatants;
        public ActionData[] actions;
        public EventData[] events;
    }
    public class CombatantData
    {
        public string name;
        public string iff;
        public string score;
    }
    public class ActionData
    {
        public string id;
        public string stat;
        public string count;
        public string after;
    }
    public class EventData
    {
        public string type;
        public string time;
        public string team;
        public string points;
        public string source;
        public string region;
        public string title;
        public string file;
        public string image;
        public string scene;
        public string info;
    }
    public class CombatGroupData
    {
        public string type;
        public string id;
        public string name;
        public string intel;
        public string iff;
        public string unit_index;
        public string region;
        public string system;
        public string zone;
        public string zone_locked;
        public string loc;
        public string parent_type;
        public string parent_id;
        public string sorties;
        public string kills;
        public string points;
        public CombatUnitData[] combatunits;
        public CombatGroupData[] combatgroups;
    }
    public class CombatUnitData
    {
        public string name;
        public string type;
        public string design;
        public string count;
        public string regnum;
        public string region;
        public string loc;
        public string dead_count;
        public string damage;
        public string heading;
    }
}