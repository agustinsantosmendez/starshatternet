﻿/*  Starshatter OpenSource Distribution
    Copyright (c) 1997-2004, Destroyer Studios LLC.
    All Rights Reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name "Destroyer Studios" nor the names of its contributors
      may be used to endorse or promote products derived from this software
      without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    SUBSYSTEM:          Stars
    ORIGINAL FILE:      Solid.h/Solid.cpp
    ORIGINAL AUTHOR:    John DiCamillo
    .NET AUTHOR:        Agustin Santos


    OVERVIEW
    ========
    Sprite Object
*/
using System;
using System.Collections.Generic;
using StarshatterNet.Stars;
using UnityEngine;
using UnityEngine.UI;
using DWORD = System.UInt32;
using Rect = UnityEngine.Rect;

namespace StarshatterNet.Gen.Graphics
{
    public class SpriteGraphic : Graphic
    {
        public SpriteGraphic()
        {
        }
        public SpriteGraphic(Bitmap animation, Canvas canvas, int length = 1, bool repeat = true, bool share = true) :
            base("Sprite:" + animation.GetFilename())
        {
            if (animation == null) return;
            trans = true;
            SetAnimation(animation, length, repeat, share);

            texture = animation.Texture;
            w = texture.width;
            h = texture.height;
            spriteRenderer = gameObj.AddComponent<RawImage>();
            spriteRenderer.texture = texture;
            rectTransform.SetParent(canvas.transform);
            rectTransform.sizeDelta = new Vector2(w, h);
            loop = repeat;
        }

        public virtual void Render(Video video, DWORD flags) { throw new NotImplementedException(); }
        public virtual void Render2D(Video video) { throw new NotImplementedException(); }
        public override void Update()
        {
            if (life > 0 || loop)
            {
                ulong time = Game.RealTime();
                while (time - last_time > frame_time)
                {
                    life--;
                    frame_index++;
                    if (frame_index >= nframes)
                        frame_index = 0;

                    last_time += frame_time;
                }

                if (life < 0 && !loop)
                    life = 0;
            }
        }
        public virtual void Scale(double scale)
        {
            if (scale >= 0)
            {
                w = (int)(scale * w);
                h = (int)(scale * h);

                radius = (float)((w > h) ? w : h) / 2.0f;
                rectTransform.sizeDelta = new Vector2(w, h);
            }
        }

        public virtual void Rescale(double scale)
        {
            if (scale >= 0 && Frame() != null)
            {
                w = (int)(scale * Frame().Width());
                h = (int)(scale * Frame().Height());

                radius = (float)((w > h) ? w : h) / 2.0f;
                rectTransform.sizeDelta = new Vector2(w, h);
            }
        }

        public virtual void Reshape(int w1, int h1)
        {
            if (w1 >= 0 && h1 >= 0 && Frame() != null)
            {
                w = w1;
                h = h1;

                radius = (float)((w > h) ? w : h) / 2.0f;
                rectTransform.sizeDelta = new Vector2(w, h);
            }
        }

        // accessors / mutators
        public int Width() { return w; }
        public int Height() { return h; }
        public bool Looping() { return loop; }
        public int NumFrames() { return nframes; }
        public double FrameRate() { throw new NotImplementedException(); }
        public void SetFrameRate(double rate) { throw new NotImplementedException(); }

        public double Shade() { return shade; }
        public void SetShade(double s) { shade = s; }
        public double Angle() { return angle; }
        public void SetAngle(double a) { angle = a; }
        public int BlendMode() { return blend_mode; }
        public void SetBlendMode(int a) { blend_mode = a; }
        public int Filter() { return filter; }
        public void SetFilter(int f) { filter = f; }
        public virtual void SetAnimation(Bitmap animation, int length = 1, bool repeat = true, bool share = true)
        {
            radius = (float)((w > h) ? w : h) / 2.0f;

            own_frames = !share;
            nframes = length;
            frames = new Bitmap[length];
            frames[0] = animation;
            frame_index = 0;
            if (repeat)
            {
                loop = true;
                life = -1;
            }
            else
            {
                loop = false;
                life = nframes;
            }

            last_time = Game.RealTime() - frame_time;
        }
        public virtual void SetTexCoords(float x, float y, float w, float h)
        {
            spriteRenderer.uvRect = new Rect(x, y, w, h);
        }

        public virtual void Colorize(Color color, bool force_alpha = false)
        {
            if (spriteRenderer != null)
            {
                spriteRenderer.color = color;
            }
        }

        public Bitmap Frame()
        {
            return frames[frame_index];
        }
        public void SetFrameIndex(int n) { throw new NotImplementedException(); }

        public override bool IsSprite() { return true; }


        protected int w, h;
        protected bool loop;

        protected int nframes = 0;
        protected bool own_frames;
        protected Bitmap[] frames;
        protected int frame_index;
        protected ulong frame_time;
        protected ulong last_time;
        protected double shade;
        protected double angle;
        protected int blend_mode;
        protected int filter;

        protected Sprite sprite;
        public Texture2D texture;
        private RawImage spriteRenderer;
    }
}
